/**
 * @directive - DraggableDirective
 * @type - Attribute
 * Trigered with load of the element
 * Makes the attached element draggable
 */
import { Directive, ElementRef, Input, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';

import * as $ from 'jquery';
import 'jqueryui';

@Directive({
    selector: '[isDraggable]'
})

export class DraggableDirective implements OnInit, OnChanges {
    // Properties
    //tslint:disable-next-line:no-input-rename
    @Input('isDraggable') isDraggable: boolean = true;
    // tslint:disable-next-line: no-input-rename
    @Input('setIndex') setIndex: number = -1;
    @Output() onDrag: EventEmitter<IDraggedSet>;

    // Constructor
    constructor(private elemRef: ElementRef) {
        this.onDrag = new EventEmitter();
    }

    // Lifecycle Hooks
    ngOnInit(): void {
        this.makeElementDraggable();
    }

    ngOnChanges(): void {
        this.makeElementDraggable();
    }

    // Private Methods
    private makeElementDraggable(): void {
        if (this.isDraggable) {
            $(this.elemRef.nativeElement).draggable({
                zIndex: 9999999,
                revertDuration: 0,
                appendTo: 'body',
                helper: 'clone',
                revert: 'invalid',
                start: () => {
                    this.onDrag.emit({
                        draggedSetIndex: this.setIndex
                    });
                }
            });
        }
    }
}

export interface IDraggedSet {
    draggedSetIndex: number;
}
