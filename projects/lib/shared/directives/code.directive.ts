import { Directive, ElementRef } from '@angular/core';
import { NgControl, ControlContainer } from '@angular/forms';
import { Utils } from './../services/utility';
import { BaseDirective } from '@app/base/BaseDirective';

@Directive({
    selector: '[eTypeCode]'
})
export class CodeDirective extends BaseDirective {

    constructor(el: ElementRef, control: NgControl, controlContainer: ControlContainer, private utils: Utils) {
        super(el, control, controlContainer);
    }

    updateFormControl(value: string): void {
        if (typeof value !== 'undefined' && value !== null && value !== '') {
            let isWhiteSpace = /^\s+$/.test(value);
            let commonValidationResult = this.utils.commonValidate({ value: value });
            if ((isWhiteSpace === true && this.required(this.controlContainer['form'].controls[this.control.name])) || (commonValidationResult && commonValidationResult['invalidValue'] === true)) {
                this.setError();
            } else if (this.control && this.control.name) {
                this.controlContainer['form'].controls[this.control.name].setValue(value.toString().toUpperCase());
            } else {
                this.el.nativeElement.value = value.toString().toUpperCase();
            }
        }
    }
}
