
export interface ITaskWorker {
    process: (params: any) => any;
}

export class AppWebWorker {
    public delegate(params: any): any {
        let task: any = new this['TaskWorker']();
        return task.process(params);
    }

    public delegatePromise(): any {
        /** Place Holder - Function Body Not Required */
    }
}
