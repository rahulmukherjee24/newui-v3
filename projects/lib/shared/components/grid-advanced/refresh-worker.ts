import { ITaskWorker } from '../../web-worker/app-worker';

export class RefreshWorker implements ITaskWorker {
    constructor() {
    }

    public process = (): any => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(true);
            }, 180000);
        });
    }
}
