import { CustomAlertConstants } from './../alert/customalert.constants';
import { MessageConstant } from './../../constants/message.constant';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { ExportConfig } from './../../../app/base/ExportConfig';
import { Utils } from '@shared/services/utility';
import { HttpService } from '@shared/services/http-service';
import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { GlobalConstant } from '../../constants/global.constant';
import { RiExchange } from '../../../shared/services/riExchange';
import { SessionStorageService } from 'ngx-webstorage';
import { IExportOptions } from '@app/base/ExportConfig';

// webpack html imports
//let template = require('./pagination.html');

@Component({
    selector: 'icabs-pagination',
    templateUrl: 'pagination.html'
})
export class PaginationComponent implements OnInit, OnDestroy {
    public boundaryLinks: boolean;
    public previousText: string;
    public nextText: string;
    public firstText: string;
    public lastText: string;
    public defaultStartPage: number;

    private _config: any;

    @Input() public totalItems: number;
    @Input() public maxSize: number;
    @Input() public itemsPerPage: number;
    @Input() public currentPage: number;
    @Input() public disabled: boolean = false;
    @Input() public canExport: boolean = false;
    @Input() public exportOptions: IExportOptions;
    @Output() getCurrentPage = new EventEmitter<any>();

    constructor(private _global: GlobalConstant,
        private riExchange: RiExchange,
        private http: HttpService,
        private utils: Utils,
        private sessionStorageService: SessionStorageService,
        private notification: GlobalNotificationsService) { }

    ngOnInit(): void {
        this._config = this._global.AppConstants().paginationConfig;
        this.boundaryLinks = this._config.boundaryLinks;
        if (this.maxSize !== 0) {
            this.maxSize = this._config.maxSize;
        }
        this.previousText = this._config.previousText;
        this.nextText = this._config.nextText;
        this.firstText = this._config.firstText;
        this.lastText = this._config.lastText;
        this.defaultStartPage = this._config.defaultStartPage;
        this.currentPage = this.currentPage || this._config.defaultStartPage;
        this.totalItems = this.totalItems || 100;
        this.itemsPerPage = this.itemsPerPage || this._global.AppConstants().tableConfig.itemsPerPage;
    }

    ngOnDestroy(): void {
        this.riExchange.releaseReference(this);
    }

    public setPage(pageNo: number): void {
        this.currentPage = pageNo;
    }

    public currentPageChanged(event: any): void {
        this.getCurrentPage.emit({ value: event.page });
    }

    public onExportClick(event: any): void {
        event.preventDefault();
        let pageTitleElem: any = document.querySelector('.screen-header h1') || document.querySelector('.page-title h1') || document.querySelector('.page-header small');
        let pageTitle: string = pageTitleElem.innerHTML;
        this.utils.trackGAEvent('click', 'Grid Export', pageTitle);
        let lastGridRequest: any = this.sessionStorageService.retrieve('last-grid');

        ExportConfig.setExportConfig(this.exportOptions);

        this.notification.displayMessage(
            MessageConstant.Message.ExportMessage,
            CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        if (lastGridRequest) {
            this.http.makeExportRequest(null, pageTitle).then((_data: any) => {
            }).catch((error: any) => {
                console.log('error occurred', error);
            });
        }
    }
}
