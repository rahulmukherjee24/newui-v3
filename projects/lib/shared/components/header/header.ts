import { PushNotificationsService, PushEmitData } from './../../services/push.notification.service';
import { environment } from './../../../environments/environment';
import { QueryParams } from './../../services/http-params-wrapper';
import { NGXLogger } from 'ngx-logger';
import { LocalStorageService } from 'ngx-webstorage';
import { Store } from '@ngrx/store';
import { CBBConstants } from './../../constants/cbb.constants';
import { CBBService } from './../../services/cbb.service';
import { HttpService } from './../../services/http-service';
import { ServiceConstants } from './../../constants/service.constants';
import { ModalComponent } from './../modal/modal';
import { Component, OnInit, AfterViewInit, OnDestroy, Input, ViewChild, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../../services/auth.service';
import { VariableService } from '../../services/variable.service';
import { MessageConstant } from '../../constants/message.constant';
import { Utils } from '../../services/utility';

@Component({
    selector: 'icabs-header',
    templateUrl: 'header.html'
})

export class HeaderComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('promptModal') public promptModal;
    @ViewChild('mainnavref') public mainnavref;
    @ViewChild('employeeDetailsModal') public employeeDetailsModal: ModalComponent;

    @Input() displayUser: boolean;
    public name: string;
    public errorMessage: string;
    public displayUserMenu: boolean = false;
    public displayNav: boolean = false;
    public displayBackLink: boolean = true;
    private routerSubscription: any;
    public promptConfirmTitle: string = 'Confirm';
    public promptConfirmContent: any = [];
    private isClassic: boolean = false;

    public isStaging: boolean = false;
    public isTraining: boolean = false;

    public visibleDiv: string = '';
    public isMenuVisible: boolean = true;
    public timeoutConst: any;
    public menuTitle: string = '';
    public queryLookUp: QueryParams = new QueryParams();
    public storeData: any;
    public userInformation: any = {};
    public employeeInformation: any = {};
    public setupInfo: any;

    // For Notifications
    public notificationTypes: Array<PushEmitData> = [];
    public hasNewNotification: boolean = false;

    constructor(private _authService: AuthService, private _ls: LocalStorageService, private store: Store<any>, private location: Location, private router: Router, private variableService: VariableService, private utils: Utils, private serviceConstants: ServiceConstants, private httpService: HttpService, private cbbService: CBBService, private logger: NGXLogger, private pushNotification: PushNotificationsService
    ) {
        this.routerSubscription = router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.displayBackLink = !(event['url'].indexOf('/postlogin') !== -1 || event['url'] === '/');
            }
            if (event['url'] && (event['url'].indexOf('/postlogin') !== -1 || event['url'] === '/')) {
                this.promptConfirmContent = [MessageConstant.Message.SignOutMessage];
            } else {
                this.promptConfirmContent = [MessageConstant.Message.SignOutMessage, MessageConstant.Message.SignOutUnsavedMessage];
            }
        });

        this.menuTitle = MessageConstant.Message.expandMenu;

        this.setupInfo = this._ls.retrieve('SETUP_INFO');

        this.pushNotification.notify.subscribe(data => {
            this.notificationTypes = data;
            if (data.length) {
                this.hasNewNotification = true;
            }
        });
    }

    @HostListener('document:click', ['$event']) onClick($event: any): void {
        if (!$event.srcElement.dataset['dropdownLinks']) {
            this.visibleDiv = '';
        }
    }

    @HostListener('document:mouseover', ['$event']) onMenuExpandBlur(): void {
        if (document.querySelector('body').className.indexOf('menu-expanded') < 0) {
            this.isMenuVisible = false;
        }
        if (this.timeoutConst) {
            clearTimeout(this.timeoutConst);
        }
    }

    ngOnInit(): void {
        // For page reload scenario
        if (window.location.hash.indexOf('/postlogin') !== -1 || window.location.hash === '/') {
            this.displayBackLink = false;
            this.promptConfirmContent = [MessageConstant.Message.SignOutMessage];
        } else {
            this.promptConfirmContent = [MessageConstant.Message.SignOutMessage, MessageConstant.Message.SignOutUnsavedMessage];
        }
        if (typeof this.displayUser === 'undefined') {
            this.displayUser = true;
        }
        this.name = this._authService.displayName;
        if (!this.name) {
            this.name = this._ls.retrieve('DISPLAYNAME');
        }
        this.mainnavref.closeNavEmit.subscribe((data) => {
            this.closeNav(data);
        });
        this.isStaging = (environment['NODE_ENV'] === 'STAGING' || environment['NODE_ENV'] === 'DEVONSG');
        this.isTraining = (environment['NODE_ENV'] === 'TRAINING');
    }

    ngAfterViewInit(): void {
        // statement
        this.getEmployeDetails();
        this.cbbService.changeEmitter.subscribe(
            data => {
                if (data[CBBConstants.c_s_CHANGED_PROPERTY] === CBBConstants.c_s_CHANGE_EMPLOYEE_DATA) {
                    /**
                     * Logic Added To Retrieve The Latest Token
                     * setTimeout Is Used Intentionally TO Delay The Call To Get Latest Token
                     * This Scenario Will Be In Effect When User Open Browser After 1 Hour
                     */
                    if (this._authService.getToken()) {
                        this.getEmployeDetails();
                    } else {
                        setTimeout(() => {
                            this.getEmployeDetails();
                        }, 5000);
                    }
                }
            });
    }

    ngOnDestroy(): void {
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
    }

    public signOut(e: any): void {
        e.preventDefault();
        this.promptModal.show();
    }

    public UserMenu(e: any): void {
        e.preventDefault();
        this.displayUserMenu = true;
    }

    public closeUserMenu(e: any): void {
        e.preventDefault();
        this.displayUserMenu = false;
    }

    public hamburger(e: any): void {
        e.preventDefault();
        this.displayNav = true;
    }

    public closeNav(e: any): void {
        e.preventDefault();
        this.displayNav = false;
    }

    public promptConfirm(event: any): void {
        this.variableService.setLogoutClick(true);
        this._authService.signOut();
        setTimeout(() => {
            window.location.reload();
        }, 500);
    }

    public settings(e: any): void {
        e.preventDefault();
    }

    public backLinkOnClick(event: any): void {
        event.preventDefault();
        this.variableService.setBackClick(true);
        this.location.back();
    }

    public onLinkClick(value: any): void {
        this.utils.trackGAEvent('click', value);
    }

    public onOptionsClick(event: any, value: any): void {
        event.preventDefault();
        this.visibleDiv = value === this.visibleDiv ? '' : value;
        if (value === 'Notifications') {
            this.hasNewNotification = false;
        }
    }

    public onMenuExpandClick($event: any): void {
        $event.preventDefault();
        this.visibleDiv = '';
        this.isMenuVisible = !this.isMenuVisible;
        this.menuTitle = this.isMenuVisible ? MessageConstant.Message.collapseMenu : MessageConstant.Message.expandMenu;
        this.pushPageContentDown();

    }

    public onMenuExpandHover($event: any): void {
        $event.stopPropagation();
        if (!this.isMenuVisible) {
            this.timeoutConst = setTimeout(() => {
                this.visibleDiv = '';
                this.isMenuVisible = true;
            }, 500);
        }
    }

    public onMenuExpandBlur1(): void {
        if (document.querySelector('body').className.indexOf('menu-expanded') < 0) {
            this.isMenuVisible = false;
        }
        if (this.timeoutConst) {
            clearTimeout(this.timeoutConst);
        }
    }

    public pushPageContentDown(): void {
        let bodyElem: any = document.querySelector('body');
        if (this.isMenuVisible) {
            bodyElem.classList.add('menu-expanded');
        } else {
            bodyElem.classList.remove('menu-expanded');
        }
    }

    public getEmployeDetails(): any {
        let userData = [{
            'table': 'UserInformation',
            'query': { 'UserCode': this._ls.retrieve('RIUserCode') },
            'fields': ['UserCode', 'UserName', 'Email', 'UserTypeCode']
        },
        {
            'table': 'Employee',
            'query': { 'BusinessCode': this.utils.getBusinessCode(), 'UserCode': this._ls.retrieve('RIUserCode') },
            'fields': ['EmployeeCode', 'BranchNumber', 'OccupationCode', 'WorkEmail', 'SMSMessageNumber']
        }];
        if (!this.utils.getBusinessCode()) {
            return;
        }
        this.lookUpRecord(userData, 10).subscribe(
            (data) => {
                if (data['results'] && data['results'].length && data.results[0][0].UserTypeCode) {
                    let description: any = [{
                        'table': 'UserType',
                        'query': {
                            'BusinessCode': this.utils.getBusinessCode(), 'UserTypeCode': data.results[0][0].UserTypeCode
                        },
                        'fields': ['UserTypeDescription']
                    }];
                    if (data['results'][0].length) {
                        this.userInformation = data.results[0][0];
                    }
                    if (data['results'][1].length) {
                        this.employeeInformation = data.results[1][0];
                        this.employeeInformation.BranchName = this.utils.getBranchTextOnly(this.employeeInformation.BranchNumber);
                        // Section Added For NonLive Branches
                        if (!this.employeeInformation.BranchName) {
                            let branchNameQuery: Array<any> = [{
                                'table': 'Branch',
                                'query': {
                                    'BusinessCode': this.utils.getBusinessCode(), 'BranchNumber': this.employeeInformation.BranchNumber
                                },
                                'fields': ['BranchName']
                            }];
                            this.lookUpRecord(branchNameQuery, 10).subscribe(
                                (branchData) => {
                                    if (branchData['results'] && branchData['results'].length && branchData['results'][0][0]) {
                                        this.employeeInformation.BranchName = branchData['results'][0][0].BranchName;
                                    }
                                });
                        }
                    } else {
                        this.employeeInformation = { 'errorMessage': MessageConstant.Message.employeeLinkedMessage };
                    }
                    if (this.employeeInformation.OccupationCode) {
                        description.push({
                            'table': 'Occupation',
                            'query': {
                                'BusinessCode': this.utils.getBusinessCode(), 'OccupationCode': data.results[1][0].OccupationCode
                            },
                            'fields': ['OccupationDesc', 'BranchName']
                        });
                    }
                    this.lookUpRecord(description, 10).subscribe(
                        (descData) => {
                            if (descData['results'] && descData['results'].length) {
                                if (descData['results'][0].length) {
                                    this.userInformation.UserTypeDescription = descData['results'][0][0].UserTypeDescription;
                                }
                                if (descData['results'][1] && descData['results'][1].length) {
                                    this.employeeInformation.OccupationDesc = descData['results'][1][0].OccupationDesc;
                                }
                            }
                        });
                }
            },
            (error) => {
                this.logger.log(error);
            }
        );
    }

    public lookUpRecord(data: any, maxresults: number): any {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }

    public openEmployeeModal(event?: any): void {
        event.preventDefault();
        this.employeeDetailsModal.show({}, false);
    }

    public onNotificationCick(event: any, id: string, type: string): void {
        event.preventDefault();
        if (!id) {
            return;
        }
        this.pushNotification.onNotifcationClick(id);
    }
}
