import { Component, ViewChild, Input, Output, OnChanges, EventEmitter, OnInit } from '@angular/core';
import { PageDataService } from '../../services/page-data.service';
import { RiExchange } from '@shared/services/riExchange';
import { TableComponent } from '../table/table';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceConstants } from '@shared/constants/service.constants';
import { Utils } from '@shared/services/utility';
import { MntConst } from '@shared/services/riMaintenancehelper';

export interface IGenericTableColumn {
    [index: number]: {
        title: string;
        name: string;
        type?: string;
    };
}

export interface IGenericEllipsisControl {
    autoOpen?: boolean;
    configParams: IGenericEllipsisConfigParams;
    disable?: boolean;
    ellipsisTitle: string;
    httpConfig?: IGenericEllipsisHttpParams;
    tableColumn: IGenericTableColumn;
}

export interface IGenericEllipsisConfigParams {
    shouldShowAdd?: boolean;
    table: string;
}

export interface IGenericEllipsisHttpParams {
    method: string;
    module: string;
    operation: string;
}

export enum GenericEllipsisEvent {
    add = 'isAddEvent'
}

@Component({
    selector: 'icabs-ellipsis-generic',
    templateUrl: 'ellipsis-generic.html',
    styles: [`
        .ellipsis.disabled {
            opacity: 0.4;
        }
    `]
})

export class EllipsisGenericComponent implements OnInit, OnChanges {

    @ViewChild('childModal') public childModal;
    @ViewChild('ellipsisGenericTable') ellipsisGenericTable: TableComponent;

    @Input() showCloseButton: boolean;
    @Input() showHeader: boolean;
    @Input() closeModalManual: boolean;
    @Input() disabled: boolean;
    @Input() title: any;
    @Input() configParams: Object = null;
    @Input() httpConfig: Object = null;
    @Input() autoOpen: boolean;
    @Input() ellipsisIdentifier: any;
    @Input() hideIcon: any;
    @Input() ellipsisTitle: any;
    @Input() ellipsisColumn: any;

    @Output() ellipsisData = new EventEmitter();
    @Output() modalHidden = new EventEmitter();
    @Output() addModeOn = new EventEmitter();

    public search: QueryParams = new QueryParams();
    public inputParams: any = {};
    public shouldShowAdd: boolean = false;
    public addNew: Object = { [GenericEllipsisEvent.add]: true };

    private xhrParams: Record<string, string> = {
        method: 'generic/search',
        module: 'report',
        operation: 'GenericSearch'
    };

    constructor(
        private pageData: PageDataService,
        private riExchange: RiExchange,
        private serviceConstants: ServiceConstants,
        private utils: Utils
    ) {
    }

    public ngOnInit(): void {
        if (this.httpConfig) {
            this.xhrParams.method = this.httpConfig['method'];
            this.xhrParams.module = this.httpConfig['module'];
            this.xhrParams.operation = this.httpConfig['operation'];
        }

        if (this.configParams['shouldShowAdd']) {
            this.shouldShowAdd = this.configParams['shouldShowAdd'];
        }

        this.buildTableColumns();
    }

    public ngOnChanges(): void {
        if (this.autoOpen === true) {
            this.openModal();
        }
    }

    public openModal(): void {
        setTimeout(() => {
            if (!this.disabled) {
                this.updateView();
                this.childModal.show();
            }
        }, 0);
    }

    public modalClose(data?: any): void {
        this.modalHidden.emit(data);
    }

    public closeModal(): void {
        this.childModal.hide();
    }

    public sendDataToParent(valueReceive: any): void {
        this.closeModal();
        this.ellipsisData.emit(valueReceive);
    }


    public updateView(): void {
        this.inputParams.module = this.xhrParams.module;
        this.inputParams.method = this.xhrParams.method;
        this.inputParams.operation = this.xhrParams.operation;

        this.search.set(this.serviceConstants.Action, '0');
        this.search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.search.set('table', this.configParams['table']);

        this.inputParams.search = this.search;

        this.ellipsisGenericTable.loadTableData(this.inputParams);
    }

    private buildTableColumns(): void {
        this.ellipsisGenericTable.clearTable();
        this.ellipsisColumn.forEach((item: any) => {
            this.ellipsisGenericTable.AddTableField(item.name,
                item.type || MntConst.eTypeText,
                'Key', item.title || item.name, item.size,
                item.alignment || MntConst.eAlignmentCenter);
        });
    }

    public selectedData(event: any): void {
        if (event.row) {
            this.closeModal();
            this.ellipsisData.emit(event.row);
        }
    }

    public onAddNew(): void {
        this.ellipsisData.emit(this.addNew);
        this.closeModal();
    }
}
