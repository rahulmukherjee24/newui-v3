import { Component } from '@angular/core';


@Component({
    template: 'This function is not currently available to use and may be implemented in a future update.'
})
export class ScreenNotReadyComponent { }
