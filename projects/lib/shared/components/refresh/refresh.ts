import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'icabs-refresh',
    templateUrl: 'refresh.html'
})

export class RefreshComponent {
    @Input() disabled: Boolean = false;
    @Output() onRefresh = new EventEmitter<any>();

    public refresh(event: any): void {
        event.preventDefault();
        if (!this.disabled) {
            this.onRefresh.emit();
        }
    }
}
