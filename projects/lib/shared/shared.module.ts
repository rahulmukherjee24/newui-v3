import { DateDirective } from './directives/date.directive';
import { CodeDirective } from './directives/code.directive';
import { TextDirective } from './directives/text.directive';
import { TextFreeDirective } from './directives/text-free.directive';
import { NumberDirective } from './directives/number.directive';
import { TimeDirective } from './directives/time.directive';
import { CurrencyDirective } from './directives/currency.directive';
import { DecimalType6Directive } from './directives/decimal-type-6.directive';
import { DecimalType5Directive } from './directives/decimal-type-5.directive';
import { DecimalType4Directive } from './directives/decimal-type-4.directive';
import { DecimalType3Directive } from './directives/decimal-type-3.directive';
import { DecimalType2Directive } from './directives/decimal-type-2.directive';
import { DecimalType1Directive } from './directives/decimal-type-1.directive';
import { IntegerDirective } from './directives/integer.directive';
import { CapitalFirstLtrDirective } from './directives/capitalFirstLtr';
import { ZeroPadDirective } from './directives/zeroPad';
import { ConfirmOkComponent } from './components/confirm-ok/confirm-ok';
import { ReplaceTextPipe } from './pipes/replaceText';
import { GridColumnComponent } from './components/grid-advanced/grid-cell';
import { GridAdvancedComponent } from './components/grid-advanced/grid-advanced';
import { CommonDropdownComponent } from './components/common-dropdown/common-dropdown.component';
import { RouterModule } from '@angular/router';
import { CapitalizePipe } from './pipes/capitalize';
import { AutocompleteComponent } from './components/autocomplete/autocomplete';
import { RouteAwayComponent } from './components/route-away/route-away';
import { DropdownStaticComponent } from './components/dropdown-static/dropdownstatic';
import { CBBComponent } from './components/cbb/cbb';
import { DropdownStaticGroupedComponent } from './components/dropdown-static-grouped/dropdown-static-grouped';
import { GridComponent } from './components/grid/grid';
import { RefreshComponent } from './components/refresh/refresh';
import { DropdownComponent } from './components/dropdown/dropdown';
import { TableComponent } from './components/table/table';
import { EllipsisComponent } from './components/ellipsis/ellipsis';
import { EllipsisGenericComponent } from './components/ellipsis-generic/ellipsis-generic';
import { PaginationComponent } from './components/pagination/pagination';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { TabsComponent } from './components/tabs/tabs';
import { AccordionComponent } from './components/accordion/accordion';
import { PromptModalComponent } from './components/prompt-modal/prompt-modal';
import { DatepickerComponent } from './components/datepicker/datepicker';
import { HeaderComponent } from './components/header/header';
import { ModalAdvComponent } from './components/modal-adv/modal-adv';
import { CommonModule } from '@angular/common';
import { ModalModule, DatepickerModule, AccordionModule, TabsModule, PaginationModule } from 'ngx-bootstrap';
import { SpinnerComponent } from './components/spinner/spinner';
import { ModalComponent } from './components/modal/modal';
import { OrderBy } from './pipes/orderBy';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select/ng2-select';
import { CustomAlertComponent } from './components/alert/customalert';
import { FocusDirective } from './directives/focus';
import { DraggableDirective } from './directives/draggable.directive';
import { DroppableDirective } from './directives/droppable.directive';
import { SetTabToSmallDeviceDirective } from './directives/setTabForSmallDevice.directive';
import { GridToSlideComponent } from './components/gridToSlide/gridToSlide.component';
import { MobileHeaderComponent } from './components/mobile-header/mobile-header.component';
import { ResetFormStateComponent } from './components/reset-form-state/reset-form-state';
import { FetchDetailsDirective } from './directives/fetch-description.directive';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        TranslateModule,
        DatepickerModule.forRoot(),
        AccordionModule.forRoot(),
        TabsModule.forRoot(),
        RouterModule,
        PaginationModule.forRoot(),
        SelectModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        PaginationModule,
        AccordionModule,
        SelectModule,
        ModalModule,
        HeaderComponent,
        DatepickerComponent,
        ModalComponent,
        PromptModalComponent,
        AccordionComponent,
        TabsComponent,
        MainNavComponent,
        SpinnerComponent,
        PaginationComponent,
        EllipsisComponent,
        EllipsisGenericComponent,
        TableComponent,
        DropdownComponent,
        RefreshComponent,
        GridComponent,
        DropdownStaticGroupedComponent,
        CBBComponent,
        DropdownStaticComponent,
        RouteAwayComponent,
        AutocompleteComponent,
        CommonDropdownComponent,
        GridAdvancedComponent,
        GridColumnComponent,
        ModalAdvComponent,
        ConfirmOkComponent,
        CustomAlertComponent,
        OrderBy,
        CapitalizePipe,
        ReplaceTextPipe,
        FocusDirective,
        ZeroPadDirective,
        CapitalFirstLtrDirective,
        IntegerDirective,
        DecimalType1Directive,
        DecimalType2Directive,
        DecimalType3Directive,
        DecimalType4Directive,
        DecimalType5Directive,
        DecimalType6Directive,
        CurrencyDirective,
        TimeDirective,
        NumberDirective,
        TextFreeDirective,
        TextDirective,
        CodeDirective,
        DateDirective,
        DraggableDirective,
        DroppableDirective,
        SetTabToSmallDeviceDirective,
        FetchDetailsDirective,
        GridToSlideComponent,
        MobileHeaderComponent,
        ResetFormStateComponent
    ],
    declarations: [
        HeaderComponent,
        DatepickerComponent,
        ModalComponent,
        PromptModalComponent,
        AccordionComponent,
        TabsComponent,
        MainNavComponent,
        SpinnerComponent,
        PaginationComponent,
        EllipsisComponent,
        EllipsisGenericComponent,
        TableComponent,
        DropdownComponent,
        RefreshComponent,
        GridComponent,
        DropdownStaticGroupedComponent,
        CBBComponent,
        DropdownStaticComponent,
        RouteAwayComponent,
        AutocompleteComponent,
        CommonDropdownComponent,
        GridAdvancedComponent,
        GridColumnComponent,
        ModalAdvComponent,
        ConfirmOkComponent,
        CustomAlertComponent,
        OrderBy,
        CapitalizePipe,
        ReplaceTextPipe,
        FocusDirective,
        ZeroPadDirective,
        CapitalFirstLtrDirective,
        IntegerDirective,
        DecimalType1Directive,
        DecimalType2Directive,
        DecimalType3Directive,
        DecimalType4Directive,
        DecimalType5Directive,
        DecimalType6Directive,
        CurrencyDirective,
        TimeDirective,
        NumberDirective,
        TextFreeDirective,
        TextDirective,
        CodeDirective,
        DateDirective,
        DraggableDirective,
        DroppableDirective,
        SetTabToSmallDeviceDirective,
        FetchDetailsDirective,
        GridToSlideComponent,
        MobileHeaderComponent,
        ResetFormStateComponent
    ],
    providers: [
        OrderBy
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                OrderBy
            ]
        };
    }
}
