import { LocalStorageService } from 'ngx-webstorage';
import { AuthService } from './auth.service';
import { LocationStrategy } from '@angular/common';
import { NGXLogger } from 'ngx-logger';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { SubjectService } from './subject.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    //Inject classes
    private logger: NGXLogger;
    private location: LocationStrategy;
    private authService: AuthService;
    private ls: LocalStorageService;
    private subjectService: SubjectService;

    constructor(private injector: Injector) {
        this.logger = this.injector.get(NGXLogger);
        this.location = this.injector.get(LocationStrategy);
        this.authService = this.injector.get(AuthService);
        this.ls = this.injector.get(LocalStorageService);
        this.subjectService = this.injector.get(SubjectService);
    }

    handleError(error: any): void {
        this.logger.log('Global Error', error);
        let obj: any = {}, dt: any, user: any, message: any, stack: any;

        message = error.message ? error.message : error.toString();
        stack = error.stack ? error.stack : '';
        if (error.rejection && error.rejection instanceof Error) {
            message = error.rejection.message;
            stack = error.rejection.stack;
        }
        dt = new Date().toISOString();
        user = this.authService.displayName;
        if (!user) {
            user = this.ls.retrieve('DISPLAYNAME');
        }

        obj.msg = message;
        obj.stack = stack;
        obj.dt = dt;
        obj.user = user;
        this.logger.error(obj);
        if (this.subjectService.getDataEvent())
            this.subjectService.getDataEvent().next(obj);

        // IMPORTANT: Rethrow the error otherwise it gets swallowed
        throw error;
    }
}
