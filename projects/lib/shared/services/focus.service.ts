import { Injectable } from '@angular/core';

@Injectable()
export class FocusService {
    private elemToBeFocused: Element = null;
    constructor() { }

    public resetElemToBeFocused(): void {
        this.elemToBeFocused = null;
    }

    public setFocusableElement(): void {
        if (
            document.activeElement.tagName.toLowerCase() !== 'body' &&
            !document.activeElement.parentElement.classList.contains('module-title') &&
            !document.activeElement.parentElement.classList.contains('autocomplete-holder')
        ) {
            this.elemToBeFocused = document.activeElement;
        }
    }

    public focusElement(): void {
        if (this.elemToBeFocused && !(this.elemToBeFocused.getAttribute('disabled') || this.elemToBeFocused.classList.contains('disabled'))) {
            this.elemToBeFocused['focus']();
        } else {
            this.setFocusToFirstElement();
        }
    }

    private setFocusToFirstElement(): void {
        setTimeout(() => {
            const formElementList = document.querySelectorAll('form .ui-select-toggle,form input:not([disabled]),form select:not([disabled])');
            for (let l = 0; l < formElementList.length; l++) {
                const el = formElementList[l];
                if (el) {
                    const type: string = el.getAttribute('type');
                    if (
                        (type === undefined) ||
                        (type === null) ||
                        (type && type.toLowerCase() !== 'hidden' && type.toLowerCase() !== 'button')
                    ) {
                        setTimeout(() => {
                            this.elemToBeFocused = el;
                            el['focus']();
                        }, 90);
                        break;
                    }
                }
            }
        }, 100);
    }
}
