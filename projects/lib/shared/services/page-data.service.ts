import { Injectable } from '@angular/core';

@Injectable()
export class PageDataService {
    private data: any;
    private secondaryData: any;
    private ellipsis: string;
    public dt: Date = new Date();
    public sharedData: Date = new Date(this.dt.getFullYear(), this.dt.getMonth() - 6, this.dt.getDate());
    public grid_CurrentPage: string = '1';
    public toData: string;
    public isReturn: boolean = false;
    public PremiseNumber_copy: string;
    public PremiseName_copy: string;
    public ProductCode_copy: string;
    public ProductDesc_copy: string;
    public HistoryTypeCode_copy: string;
    public HistoryTypeCodeDesc_copy: string;
    public viewSelected_copy: string;
    public sortBy_copy: string;
    public parentMode_copy: string;
    public current_Year: any = this.dt.getFullYear();

    getData(): any {
        return this.data;
    }

    getSecondaryData(): any {
        return this.secondaryData;
    }

    saveData(data: any): void {
        this.data = data;
    }

    saveSecondaryData(data: any): void {
        this.secondaryData = data;
    }

    getEllipsisIdentifier(): any {
        return this.ellipsis;
    }

    saveEllipsisIdentifier(data: any): any {
        this.ellipsis = data;
    }

    clearData(): void {
        this.data = null;
    }

    clearSecondaryData(): void {
        this.secondaryData = null;
    }
}
