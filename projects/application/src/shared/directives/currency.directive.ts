import { Directive, ElementRef } from '@angular/core';
import { NgControl, ControlContainer } from '@angular/forms';
import { GlobalizeService } from './../services/globalize.service';
import { BaseDirective } from '@app/base/BaseDirective';

@Directive({
    selector: '[eTypeCurrency]'
})
export class CurrencyDirective extends BaseDirective {

    constructor(el: ElementRef, control: NgControl, controlContainer: ControlContainer, private globalize: GlobalizeService) {
        super(el, control, controlContainer);
    }

    updateFormControl(value: any, calledFromChange?: boolean): void {
        if (typeof value !== 'undefined' && value !== null && value !== '') {
            value = this.sanitizeValue(value, calledFromChange);
            let formattedValue: any = this.globalize.formatCurrencyToLocaleFormat(value, calledFromChange);
            if (formattedValue === false) {
                this.setError();
            } else {
                this.controlContainer['form'].controls[this.control.name].setValue(formattedValue);
            }
        }
    }
}
