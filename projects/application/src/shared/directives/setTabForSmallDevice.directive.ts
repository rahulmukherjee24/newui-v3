import { Directive, OnChanges, Input } from '@angular/core';

@Directive({
    selector: '[setTabForSmallDevice]'
})
export class SetTabToSmallDeviceDirective implements OnChanges {
    @Input() processTabsForMobile: boolean = false;
    private totalWidth: number = 0;

    ngOnChanges(e: any): void {
        if (e.processTabsForMobile.currentValue) { this.init(); }
    }

    private init(): void {
        setTimeout(() => {
            const screenBody: any = document.getElementsByClassName('screen-body')[0];
            const sdTabWrapper: any = screenBody.getElementsByClassName('sd-tab-wrapper')[0];
            const ulTabContainer: any = screenBody.getElementsByClassName('nav-tabs')[0];
            const elemList: any = document.querySelectorAll('.screen-body .nav-tabs li:not(.hidden)'); // document.querySelectorAll('.screen-body .nav-tabs li');
            const activeTab: any = document.querySelector('.screen-body .nav-tabs li.nav-item.active');
            let activeTabIdx: number = Array.prototype.indexOf.call(elemList, activeTab) || 0;
            this.totalWidth = 0;

            elemList.forEach((item: any) => { // fetching width to all LIs
                if (item['offsetWidth'] > 0) {
                    this.totalWidth += item['offsetWidth'];
                }
                this.totalWidth += 5;
            });
            ulTabContainer.style.width = this.totalWidth + 'px';

            if (activeTabIdx >= 1 && activeTabIdx < elemList.length) {  // if not first one and last one
                let distanceToScroll: number = 0;

                elemList.forEach((ele: any, i: number) => { // i=0, activeTabIdx=1
                    if (i === (activeTabIdx - 1)) {
                        distanceToScroll += ele['offsetWidth'] / 2;  //half width of just before one
                    } else if (i < (activeTabIdx - 1)) { // only for elem which are prev
                        if ((i + 1) < (activeTabIdx - 1)) {
                            distanceToScroll += ele['offsetWidth']; //full width
                        }
                        if ((i + 1) === (activeTabIdx - 1)) { // just prev sibiling, then take half
                            distanceToScroll += ele['offsetWidth'] / 2;  //half width of just before one
                        }
                    }
                });
                sdTabWrapper['scrollLeft'] = 0; // reset

                setTimeout(() => {
                    sdTabWrapper['scrollLeft'] += distanceToScroll;
                }, 500);
            }
        }, 1000);
    }

}



