/**
 * @directive - DraggableDirective
 * @type - Attribute
 * Trigered with load of the element
 * Makes the attached element draggable
 */
import { Directive, ElementRef, OnInit, OnChanges, Input } from '@angular/core';

import * as $ from 'jquery';
import 'jqueryui';

@Directive({
    selector: '[isDroppable]'
})

export class DroppableDirective implements OnInit, OnChanges {
    // Properties
    //tslint:disable-next-line:no-input-rename
    @Input('isDroppable') isDroppable: boolean = true;

    // Constructor
    constructor(private elemRef: ElementRef) { }

    // Lifecycle Hooks
    ngOnInit(): void {
        this.makeElementDropable();
    }

    ngOnChanges(): void {
        this.makeElementDropable();
    }

    // Private Methods
    private makeElementDropable(): void {
        if (this.isDroppable) {
            $(this.elemRef.nativeElement).droppable();
        }
    }
}
