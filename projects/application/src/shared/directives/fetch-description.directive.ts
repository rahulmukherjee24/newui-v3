import { MessageConstant } from '@shared/constants/message.constant';
import { Directive, ElementRef, HostListener, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { NgControl, ControlContainer } from '@angular/forms';

import { LookUp } from '@shared/services/lookup';
import { Utils } from '@shared/services/utility';
import { GlobalNotificationsService } from '@shared/services/global.notifications.service';
import { ServiceConstants } from '@shared/constants/service.constants';

@Directive({
    selector: '[fetchDescription]'
})
export class FetchDetailsDirective implements OnInit, OnChanges {
    @Input() table: string = '';
    @Input() query: string = '';
    @Input() select: string | string[] = '';
    @Input() field: string | string[] = '';
    @Input() hideNotification: boolean = false;
    @Input() shouldTrigger: any = '';

    private descriptionControl: string | string[] = '';

    constructor(
        private el: ElementRef,
        private control: NgControl,
        private controlContainer: ControlContainer,
        private lookup: LookUp,
        private utils: Utils,
        private constants: ServiceConstants,
        private notification: GlobalNotificationsService) { }

    @HostListener('change', ['$event']) onChange(_event?: any): void {
        this.fetchDescription(this.el.nativeElement.value);
    }

    public ngOnInit(): void {
        this.descriptionControl = this.field || this.select;
        this.initialCall();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.shouldTrigger && changes.shouldTrigger.currentValue) {
            this.fetchDescription(this.controlContainer['form'].controls[this.control.name].value);
        }
    }

    private initialCall(): void {
        if (this.controlContainer && this.controlContainer['form'] && this.control &&
            this.controlContainer['form'].controls[this.control.name]) {
            this.fetchDescription(this.controlContainer['form'].controls[this.control.name].value);
        }
    }

    private fetchDescription(value: string): void {
        if (!value) {
            this.clearDescription();
            return;
        }

        let query: any = {};
        query[this.query] = this.controlContainer['form'].controls[this.control.name].value;
        query[this.constants.BusinessCode] = this.utils.getBusinessCode();
        let queryData = [
            {
                'table': this.table,
                'query': query,
                'fields': typeof this.select === 'string' ? [this.select] : this.select
            }
        ];
        this.lookup.lookUpPromise(queryData).then(data => {
            if (data && data[0] && data[0].length) {
                this.controlContainer['form'].controls[this.control.name].setErrors(null);
                if (typeof this.select === 'string' && typeof this.descriptionControl === 'string') {
                    this.setControlValue(this.descriptionControl, data[0][0][this.select]);
                } else if (Array.isArray(this.descriptionControl)) {
                    this.descriptionControl.forEach((control, index) => {
                        this.setControlValue(control, data[0][0][this.select[index]]);
                    });
                }
            } else {
                this.clearDescription();
                this.controlContainer['form'].controls[this.control.name].setErrors({ recordNotFound: true });
                this.el.nativeElement.focus();
                if (!this.hideNotification) {
                    this.notification.displayMessage(MessageConstant.Message.recordNotFound + ' - ' + this.query);
                }
            }
        }).catch(error => {
            this.clearDescription();
            this.notification.displayMessage(error);
        });
    }

    private clearDescription(): void {
        if (typeof this.select === 'string' && typeof this.descriptionControl === 'string') {
            this.setControlValue(this.descriptionControl, '');
        } else if (Array.isArray(this.descriptionControl)) {
            this.descriptionControl.forEach((control) => {
                this.setControlValue(control, '');
            });
        }
    }

    private setControlValue(control: string, value: any): void {
        this.controlContainer['form'].controls[control].setValue(value);
    }
}
