export class CustomAlertConstants {
    public static readonly c_s_MESSAGE_TYPE_SUCCESS = 'success';
    public static readonly c_s_MESSAGE_TYPE_WARNING = 'warning';
    public static readonly c_s_MESSAGE_TYPE_ERROR = 'error';
}
