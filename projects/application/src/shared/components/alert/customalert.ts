import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';

@Component({
    selector: 'icabs-custom-alert',
    templateUrl: 'customalert.html'
})

export class CustomAlertComponent implements OnChanges {
    @Input() public alertMessage: any;
    @Input() public alertClosedTime: number = 10000;
    @Input() public messageType: string;
    @Output() alertClose = new EventEmitter<any>();

    public arrayList: any = [];
    public numberIncre: number = 0;
    public jsonObj: any = {};
    public arrayOfKeys: any = [];
    public alertWarning: boolean;
    public alertSuccess: boolean;
    public alertError: boolean;
    public glyphiconSign: string;

    ngOnChanges(data: any): void {
        if (this.alertMessage) {
            let alerId: any = 'id' + this.numberIncre;
            switch (this.messageType) {
                case 'warning':
                    this.glyphiconSign = 'glyphicon glyphicon-exclamation-sign';
                    break;
                case 'success':
                    this.glyphiconSign = 'glyphicon glyphicon-ok-circle';
                    break;
                case 'error':
                default:
                    this.glyphiconSign = 'glyphicon glyphicon-remove-circle';
                    this.messageType = 'danger';
                    break;
            }
            this.jsonObj[alerId] = {
                id: alerId,
                alertMessage: this.alertMessage.msg,
                messageType: 'alert alert-' + this.messageType,
                glyphiconType: this.glyphiconSign
            };
            this.arrayOfKeys = Object.keys(this.jsonObj);
            setTimeout(() => {
                delete this.jsonObj[alerId];
                this.arrayOfKeys = Object.keys(this.jsonObj);
            }, this.alertClosedTime);
            this.numberIncre++;
        }
    }

    public onCloseClick(key: string): void {
        delete this.jsonObj[key];
        this.arrayOfKeys = Object.keys(this.jsonObj);
        this.alertClose.emit();
    }
}
