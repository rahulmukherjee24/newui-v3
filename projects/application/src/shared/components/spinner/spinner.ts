import { Component, Input, OnDestroy, ElementRef } from '@angular/core';

@Component({
    selector: 'icabs-spinner',
    template: `
        <div tabindex="0" class="spinner-parent-elem">
            <div class="capture-focus">&nbsp;</div>
            <div [hidden]="!isDelayedRunning" [attr.data-hidden]="!isDelayedRunning" class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
            <div [hidden]="!isDelayedRunning" [attr.data-hidden]="!isDelayedRunning" class="screen-overlay"></div>
        </div>
        <div tabindex="0" (focus)="gotofirst()" class="spinner-last-elem"></div>
    `,
    styles: [
        `.capture-focus {position: absolute; top: 0; left: 0}
    `]
})
export class SpinnerComponent implements OnDestroy {
    constructor(private elem: ElementRef) {
        // statement
    }
    private currentTimeout: any;
    private id: number = (Math.floor(Math.random() * 900000) + 100000);
    private activeElem;
    public isDelayedRunning: boolean = false;
    @Input() public delay: number;
    @Input() public set isRunning(value: boolean) {
        if (!value) {
            this.cancelTimeout();
            if (this.activeElem) {
                this.activeElem.focus();
            }
            this.isDelayedRunning = false;
            return;
        }

        if (this.currentTimeout) {
            return;
        }
        this.activeElem = document.activeElement;
        if (this.delay) {
            this.currentTimeout = setTimeout(() => {
                this.showSpinner(value);
            }, this.delay);
        } else {
            this.showSpinner(value);
        }
    }

    private showSpinner(value: any): void {
        this.isDelayedRunning = value;
        let focusElem = this.elem.nativeElement.querySelectorAll('.capture-focus')[0];
        if (focusElem) {
            focusElem.focus();
        }
        this.cancelTimeout();
    }

    private cancelTimeout(): void {
        clearTimeout(this.currentTimeout);
        this.currentTimeout = undefined;
    }

    public gotofirst(): void {
        let tabbables = this.elem.nativeElement.querySelectorAll('.capture-focus');
        tabbables[0].focus();
    }

    ngOnDestroy(): any {
        this.cancelTimeout();
        //this.riExchange.releaseReference(this);
    }
}


