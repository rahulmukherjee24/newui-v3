import { QueryParams } from './../../services/http-params-wrapper';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnChanges, Input, EventEmitter, ViewChild, Output, OnDestroy, NgZone } from '@angular/core';

import { Subscription } from 'rxjs';

import { Utils } from '../../../shared/services/utility';
import { DropdownComponent } from '../../../shared/components/dropdown/dropdown';
import { HttpService } from '../../../shared/services/http-service';
import { ServiceConstants } from '../../../shared/constants/service.constants';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { SearchDropdown } from '../../../app/base/search-dropdown';

@Component({
    selector: 'icabs-common-dropdown',
    templateUrl: 'common-dropdown.html'
})

export class CommonDropdownComponent extends SearchDropdown implements OnChanges, OnDestroy, AfterViewInit {
    @ViewChild('commonDropDown') commonDropDown: DropdownComponent;
    @Input() public inputParams: any;
    @Input() public columnType: Array<string> = [];
    @Input() public isDisabled: boolean;
    @Input() public isFirstItemSelected: boolean;
    @Input() public isActive: any;
    @Input() public isRequired: boolean;
    @Input() public displayFields: Array<string>;
    @Input() public isTriggerValidate: boolean;
    @Input() public doServiceCall: boolean;
    @Input() public inputData: Array<any>;
    @Input() public isMultiSelect: boolean = false;
    @Input() public multiSelectLimit: number;
    @Output() getDefaultInfo = new EventEmitter();
    @Output() onDataReceived: EventEmitter<any>;
    @Output() onError: EventEmitter<any>;
    @Output() onDataFetchSuccess: EventEmitter<any>;

    private httpSubscription: Subscription;

    public requestdata: Array<any>;

    constructor(
        private serviceConstants: ServiceConstants,
        private httpService: HttpService,
        private utils: Utils,
        private zone: NgZone) {
        super();
        this.onDataReceived = new EventEmitter();
        this.onError = new EventEmitter();
        this.onDataFetchSuccess = new EventEmitter();
    }

    ngAfterViewInit(index?: number): void {
        this.fetchData(index);
    }

    ngOnChanges(data: any): void {
        if (data.inputParams) {
            this.fetchData();
        }
    }

    ngOnDestroy(): void {
        if (this.httpSubscription) {
            this.httpSubscription.unsubscribe();
        }
        this.httpSubscription = null;
        this.onDataReceived = null;
        this.onError = null;
        this.requestdata = null;
    }

    public fetchData(index?: number): void {
        let arrIndex: number = 0;
        if (!this.inputParams) {
            return;
        }
        if (index && index > 0) {
            arrIndex = index;
        }
        if (this.doServiceCall === false) {
            this.requestdata = this.inputData ? this.inputData : [];
            this.postFetch(this.requestdata, arrIndex);
            this.commonDropDown.updateComponent(this.requestdata);
        } else {
            let search: QueryParams;
            search = this.inputParams.search || new QueryParams();
            search.set(this.serviceConstants.Action, this.inputParams.action || '0');
            search.set(this.serviceConstants.BusinessCode, this.inputParams.businessCode || this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.inputParams.countryCode || this.utils.getCountryCode());
            this.httpSubscription = this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search)
                .subscribe((data) => {
                    if (data) {
                        this.requestdata = data.records;
                        this.responseData = data.records;
                        this.postFetch(data, arrIndex);
                    }
                    this.zone.run(() => {
                        if (this.requestdata && this.requestdata.length && this.requestdata[0].hasOwnProperty(this.displayFields[0])) {
                            this.commonDropDown.updateComponent(this.requestdata);
                        }
                    });
                    this.onDataFetchSuccess.emit();
                }, (error) => {
                    this.onError.emit(error);
                });
        }
    }

    public postFetch(data: any, arrIndex: number): void {
        if (this.columnType && this.columnType.length && this.columnType.indexOf(MntConst.eTypeCode) > -1) {
            for (let i = 0; i < this.requestdata.length; i++) {
                this.requestdata[i][this.displayFields[0]] = this.requestdata[i][this.displayFields[0]].toString().toUpperCase();
            }
        }

        if (this.isFirstItemSelected) {
            this.getDefaultInfo.emit({
                totalRecords: this.requestdata ? this.requestdata.length : 0,
                firstRow: (this.requestdata && this.requestdata.length > 0) ? this.requestdata[arrIndex] : {}
            });
        }
        if (arrIndex > 0) {
            this.isActive = {
                id: this.requestdata[arrIndex][this.displayFields[0]],
                text: this.requestdata[arrIndex][this.displayFields[0]] + ' - ' + this.requestdata[arrIndex][this.displayFields[1]]
            };
        }
    }

    public onDropDownDataReceived(obj: any): void {
        this.onDataReceived.emit(obj.value);
    }
}
