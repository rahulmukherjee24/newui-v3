import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'icabs-mobile-header',
    templateUrl: 'mobile-header.html'
})
export class MobileHeaderComponent {
    @Input() public userName: string;
    @Output() public onSignOut = new EventEmitter<any>();

    constructor() {
    }
}
