import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'icabs-grid-to-slide',
    templateUrl: 'iCABSCMGridToSlide.html',
    encapsulation: ViewEncapsulation.None
})
export class GridToSlideComponent {
    @Input() public columnToHide: string[];
    @Input() public enableDrillDown: boolean = false;
    @Output() public onSlideDrillDown = new EventEmitter<any>();

    public slides: Array<any> = [];
    public selectedSlide: any = -1;

    private normalizeColumnToHide(): void {
        if (this.columnToHide && this.columnToHide.length) {
            this.columnToHide = this.columnToHide.map((col) => {
                return col.replace(/\s/g, '').toLowerCase();
            });
        }
    }

    private showOnScreen(cellHead: any, idx: number): any {
        if (!(this.columnToHide || this.columnToHide.length)) { console.log('no column to hide......'); return; }
        const txt: any = cellHead[idx]['text'].toLowerCase().replace(/\s/g, '');
        return this.columnToHide.indexOf(txt) >= 0 ? false : true;
    }

    public onDrillDown(e: any, slide: any): void {
        this.onSlideDrillDown.emit({ event: e, slide: slide });
    }

    public loadSlides(data: any, resetAll?: boolean): void {
        let slide: any[] = [], j: number = 0, cellHead: any = data.header['cells'], cellBody: any = data.body['cells'];
        if (resetAll) { this.slides = []; }

        this.normalizeColumnToHide();

        if (!(cellBody && cellBody.length)) { this.slides = []; return; }
        setTimeout(() => {
            for (let i = 0; i < cellBody.length; i++) {
                let obj = {};
                obj['key'] = cellHead[j]['text'];
                obj['value'] = cellBody[i]['text'];
                obj['show'] = this.showOnScreen(cellHead, j);
                if (cellHead[j]['text'].toLowerCase() === 'ticket') {
                    slide['rowId'] = cellBody[i]['rowID'];
                }
                slide.push(obj);

                if (j >= cellHead.length - 1) {
                    slide['cellColor'] = cellBody[i]['cellColor'];
                    this.slides.push(slide);
                    j = -1;
                    obj = {};
                    slide = [];
                }
                j++;
            }
        }, 100);
    }
}
