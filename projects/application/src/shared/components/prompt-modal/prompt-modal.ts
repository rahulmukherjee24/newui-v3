import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { RiExchange } from '../../../shared/services/riExchange';

@Component({
    selector: 'icabs-prompt-modal',
    templateUrl: 'prompt-modal.html',
    exportAs: 'child',
    styles: [`
    .shiftTop {
        top: 35%;
        width: 100%;
        position: fixed;
        z-index: 1050;
    }
    .shiftTop .modal {
        position: relative;
    }
    `]
})
export class PromptModalComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('childModal') childModal;
    @Input() config: any;
    @Input() title: string;
    @Input() optValueYesorNo: string;
    @Input() content: any;
    @Input() shiftTop: boolean;
    @Input() showHeader: boolean;
    @Input() showCloseButton: boolean;
    @Input() isImportant: boolean;
    @Input() showCancelBtn: boolean;
    @Input() disableEscape: boolean;
    @Output() saveEmit = new EventEmitter();
    @Output() cancelEmit = new EventEmitter();

    private _data: any;

    public static readonly SAVE: string = 'save';
    public static readonly CANCEL: string = 'cancel';
    public isContentAnArray: boolean = false;

    constructor(private riExchange: RiExchange) {
        if (this.showCancelBtn === undefined) {
            this.showCancelBtn = true;
        }
    }

    ngOnInit(): void {
        if (typeof this.config !== 'object' || !this.config || ((Object.keys(this.config).length === 0 && this.config.constructor === Object))) {
            this.config = {
                backdrop: 'static',
                keyboard: true
            };
        }
        if (this.showCloseButton == null) {
            this.showCloseButton = true;
        }
        if (this.disableEscape === undefined) { this.disableEscape = false; }
        if (this.shiftTop === null || this.shiftTop === undefined) {
            this.shiftTop = false;
        }
    }

    ngOnChanges(...args: any[]): void {
        if (typeof this.config !== 'object' || !this.config || ((Object.keys(this.config).length === 0 && this.config.constructor === Object))) {
            this.config = {
                backdrop: 'static',
                keyboard: !this.disableEscape  //pass 'true' in input variable if disable keyboard
            };
        }
        if (this.showCloseButton == null) {
            this.showCloseButton = true;
        }
    }
    ngOnDestroy(): void {
        this.riExchange.releaseReference(this);
    }

    show(data: any, error: any): void {
        this._data = data;
        this.isContentAnArray = (this.content instanceof Array);
        this.childModal.show();
    }

    hide(): void {
        this.childModal.hide();
    }

    onHidden(event: any): void {
        if (document.querySelectorAll('[bsmodal].in').length <= 0) {
            let elem = document.getElementsByClassName('modal-backdrop');
            let bodyElem: any = document.querySelector('body');
            let styleClass: string = bodyElem.getAttribute('class');
            bodyElem.setAttribute('class', styleClass.replace(' modal-open', ''));
            while (elem[0]) {
                if (elem[0].parentNode)
                    elem[0].parentNode.removeChild(elem[0]);
            }
        }
    }

    cancel(): void {
        this.cancelEmit.emit({
            value: PromptModalComponent.CANCEL,
            data: this._data
        });
        if (this.childModal !== null)
            this.childModal.hide();
    }

    save(): void {
        this.saveEmit.emit({
            value: PromptModalComponent.SAVE,
            data: this._data
        });
        if (this.childModal !== null)
            this.childModal.hide();
    }
}
