import { Injectable } from '@angular/core';

@Injectable()
export class GlobalConstant {

    public static Configuration =
        {
            DefaultLocale: 'en-GB',
            DefaultTranslation: null,
            SupportedTranslation: [],
            DefaultLocaleUrl: 'assets/i18n/',
            LocaleUrl: 'i18n/translations/',
            CldrSupplementalUrl: 'i18n/cldr-data/supplemental/',
            CldrMainUrl: 'i18n/cldr-data/main/',
            ParserUrl: 'i18n/cultures/',
            ParserPrefix: 'globalize.culture.',
            DateLocaleUrl: 'i18n/datejs/',
            DateLocalePrefix: 'date-',
            Yes: 'YES',
            True: 'TRUE',
            False: 'FALSE',
            No: 'NO',
            Dev: 'DEV',
            Prod: 'PROD',
            Failure: 'failure',
            Format: {
                Time: 'Time'
            },
            BackText: '< Back',
            All: 'All',
            ENG: 'ENG',
            AppName: 'iCabs',
            Home: 'postlogin',
            Valid: 'VALID',
            ServiceReceipt: 'https://rentokil-initial-service-receipt-asia-staging.s3.amazonaws.com/f78c8590-f752-11e7-824e-005056860f39.HTML'
        };

    public AppConstants(): any {
        return {
            'tableConfig': {
                'itemsPerPage': 10
            },
            'paginationConfig': {
                'maxSize': 5,
                'defaultStartPage': 1,
                'previousText': '<',
                'nextText': '>',
                'firstText': '<<',
                'lastText': '>>',
                'boundaryLinks': true,
                'directionLinks': true

            },
            'inputTypeConfig': {
                'radio': 'radio',
                'checkbox': 'checkbox',
                'text': 'text',
                'number': 'number',
                'url': 'url',
                'tel': 'tel'
            },
            'urlConstant': {
                'zipSearchUrl': 'content/json/zipSearch.json',
                'countrySearchUrl': 'content/json/countrySearch.json',
                'paymentTypeSearchUrl': 'content/json/paymentTypeSearch.json',
                'contractServiceSummaryUrl': 'content/json/contractServiceSummary.json'
            },
            'tabsConfig': {
                'accountMaintenance': {
                    'tabsList': [{ 'title': 'Address', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'Account Management', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'General', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'Bank Details', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'EDI Invoicing', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'Invoice Text', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'Telesales', 'templateUrl': 'template-1.html', 'disabled': false },
                    { 'title': 'Address', 'templateUrl': 'template-1.html', 'disabled': false }],
                    'defaultActiveTab': 0
                }
            },
            'defaultFormatSize': 9
        };
    }
}
