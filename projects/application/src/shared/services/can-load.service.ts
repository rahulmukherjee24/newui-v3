import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';

@Injectable()
export class CanLoadService implements CanLoad {
    constructor(private router: Router) {
    }

    canLoad(route: any): boolean {
        if (navigator && typeof navigator['onLine'] !== 'undefined' && navigator.onLine === false && environment['NODE_ENV'] !== 'DEV') {
            this.setDisplay('none');
            setTimeout(() => {
                this.router.navigate([], { skipLocationChange: true, preserveQueryParams: true });
            }, 0);
            return false;
        }
        return true;
    }

    private setDisplay(val: string): void {
        document.querySelector('icabs-app .lazy-spinner .spinner')['style'].display = val;
        document.querySelector('icabs-app .ajax-overlay')['style'].display = val;
    }
}
