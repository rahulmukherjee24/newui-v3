import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ComponentInteractionService {
    private _interactionSource = new BehaviorSubject<any>(0);
    private _interactionSource$ = this._interactionSource.asObservable();

    emitMessage(msg: any): void {
        this._interactionSource.next(msg);
    }

    getInteractionSource(): any {
        return this._interactionSource;
    }

    getObservableSource(): any {
        return this._interactionSource$;
    }
}
