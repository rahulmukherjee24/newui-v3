import { environment } from './../../environments/environment';
import * as moment from 'moment';
import { ErrorConstant } from './../constants/error.constant';
import { Observable } from 'rxjs/Rx';
import { GlobalConstant } from './../constants/global.constant';
import * as sha256 from 'fast-sha256';

export class StaticUtils {
    // Static Properties
    public static readonly c_i_DEFAULT_FORMAT_SIZE: number = 9;
    public static readonly c_s_DATE_FORMAT_1: string = 'YYYY-MM-DD';
    public static readonly c_s_DATE_FORMAT_2: string = 'YYYY-DD-MM';
    public static readonly c_s_DATE_FORMAT_3: string = 'YYYYDDMM';
    public static readonly c_s_DATE_FORMAT_4: string = 'DD/MM/YYYY';
    public static readonly c_s_DATE_FORMAT_5: string = 'MM/DD/YYYY';
    public static readonly c_s_DATE_FORMAT_FIXED: string = 'YYYY/MM/DD';
    public static readonly c_s_IMAGE_REPO_URL: string = environment['IMAGE_REPO_URL'];

    // iCABS/RI Model Delimiter properites
    public static readonly RI_SEPARATOR_STATEMENT: string = '\x06';
    public static readonly RI_SEPARATOR_VALUE_LIST: string = '\x05';
    public static readonly RI_SEPARATOR_EXECUTE_SUCCESS: string = '\x04';
    public static readonly RI_SEPARATOR_EXECUTE_FAIL: string = '\x03';
    public static readonly RI_SEPARATOR_EMPTY: string = '\x02';
    public static readonly RI_SEPARATOR_GRID_LINE: string = '\x01';
    public static readonly RI_SEPARATOR_VALUE_SUB_LIST: string = '\x07';
    public static readonly RI_SEPARATOR_VALUE_NEWLINE: string = '\n';

    public static getSHA256Hash(options: any): string {
        if (!options || !options.value) {
            return;
        }
        let decodedString: Uint8Array = window['nacl'].util.decodeUTF8(options.value);
        let decodedSalt: Uint8Array = window['nacl'].util.decodeUTF8(options.skipSalt ? '' : environment['USER_ID_SALT']);
        let hashResult: string = window['nacl'].util.encodeBase64(sha256.pbkdf2(decodedString, decodedSalt, 1, options.len || environment['USER_ID_HASH_LEN']));
        return hashResult;
    }

    /**
     * Method - deepCopy
     * Clones the object and returns the value of the object
     * Helpful to bypass pass by reference of objects
     * @param obj - JS Object
     * @return any
     */
    public static deepCopy(obj: any): any {
        let newObj = obj;
        if (obj && typeof obj === 'object') {
            newObj = Object.prototype.toString.call(obj) === '[object Array]' ? [] : {};
            for (let i in obj) {
                if (i) {
                    newObj[i] = this.deepCopy(obj[i]);
                }
            }
        }
        return newObj;
    }

    /**
     * Adds leading 0 to make the string of desired length
     * If desired length is not passed in the method default size is used
     * @method fillLeadingZeros
     * @param input - string to pad
     * @param size - desired size of the string
     * @return string - string padded with leading 0
     */
    public static fillLeadingZeros(input: string, size?: number): string {
        let result = input,
            formatSize = size;

        if (!size) {
            formatSize = StaticUtils.c_i_DEFAULT_FORMAT_SIZE;
        }

        for (let i = 0; i < (formatSize - input.length); i++) {
            result = '0' + result;
        }

        return result;
    }

    /**
     * Method - convertResponseValueToCheckboxInput
     * Accepts response values as yes/no
     * Returns value as true/false so that can be set in checkboxes
     */
    public static convertResponseValueToCheckboxInput(responseValue: string): boolean {
        return (GlobalConstant.Configuration.Yes === responseValue.toUpperCase());
    }

    /**
     * Method - convertCheckboxValueToRequestValue
     * Accepts any as parameter
     * Return string as yes/no
     */
    public static convertCheckboxValueToRequestValue(checkboxValue: any): string {
        if (typeof checkboxValue === 'boolean') {
            return (!checkboxValue ? GlobalConstant.Configuration.No.toLowerCase() : GlobalConstant.Configuration.Yes.toLowerCase());
        } else {
            if (typeof checkboxValue === 'string') {
                return (checkboxValue.toLowerCase() === 'true') ? GlobalConstant.Configuration.Yes.toLowerCase() : GlobalConstant.Configuration.No.toLowerCase();
            }
        }
    }

    /**
     * Formats a date string to the specified format
     * If invaid date returns string 'Invalid Date'
     * @method - convertDateToFormat
     * @param value - Value to be formatted
     * @param fromFormat - Current format
     * @param toFormat - New format
     * @return - string - Formatted string
     */
    public static convertDateToFormat(value: string, fromFormat: string, toFormat?: string): string {
        let date: any;

        if (!toFormat) {
            toFormat = StaticUtils.c_s_DATE_FORMAT_4;
        }

        date = moment(value, fromFormat, true);

        if (!date.isValid()) {
            return 'Invalid Date';
        }

        return date.format(toFormat);
    }

    public static extractDataFromResponse(res: any): Observable<any> {
        let body: any = res.json ? res.json() : res;
        if (typeof body['error_description'] !== 'undefined' && body['error_description'].toString().indexOf(ErrorConstant.Message.Invalid) !== -1) {
            return Observable.throw(res);
        }
        //Check for business error
        if (body && body.hasOwnProperty('oResponse')) {
            if (body.info && body.info.error) {
                body.oResponse = {};
                body.oResponse.errorMessage = body.info.error;
                body.oResponse.hasError = true;
            } else if (body.oResponse && body.oResponse.errorMessage) {
                body.oResponse.hasError = true;
            } else if (body.oResponse && body.oResponse.ErrorMessageDesc) {
                body.oResponse.hasError = true;
                body.oResponse.errorMessage = body.oResponse.ErrorMessageDesc;
            }
            return body.oResponse;
        }
        return body || {};
    }

    public static secondsToHMS(time: any): string {
        time = Number(time);
        let h = Math.floor(time / 3600);
        let m = Math.floor(time % 3600 / 60);

        let hDisplay: string;
        if (h > 0) {
            hDisplay = h > 9 ? h + ':' : '0' + h + ':';
        } else {
            hDisplay = '00' + ':';
        }
        let mDisplay: string;
        if (m > 0) {
            mDisplay = m > 9 ? m + '' : '0' + m;
        } else {
            mDisplay = '00';
        }
        return hDisplay + mDisplay;
    }

    /**
     * @property - To handle conversion of data to different types
     */
    public static ConversionHelper = {

        /**
         * Converts value to integer
         * @method toInteger
         * @param val - Value to be converted
         * @return Formatted value
         */
        toInteger: (val: any): string => {
            try {
                if (isNaN(val)) {
                    throw 'Not a number';
                }

                return parseInt(val, 10).toString();
            } catch (excp) {
                console.log('ERROR', excp);
            }
            return val;
        },

        /**
         * Converts value to float
         * @method toFloat
         * @param val - Value to be converted
         * @param decimals - Decimal places to be required
         * @return Formatted value
         */
        toFloat: (val: any, decimals: number): string => {
            try {
                if (isNaN(val)) {
                    throw 'Not a number';
                }

                return parseFloat(val).toFixed(decimals);
            } catch (excp) {
                console.log('ERROR', excp);
            }
            return val;
        },

        /**
         * Converts value to float and upto 1 decimal places
         * @method toDecimal1
         * @param val - Value to be converted
         * @return Formatted value
         */
        toDecimal1: (val: any): string => {
            return StaticUtils.ConversionHelper.toFloat(val, 1);
        },

        /**
         * Converts value to float and upto 2 decimal places
         * @method toDecimal2
         * @param val - Value to be converted
         * @return Formatted value
         */
        toDecimal2: (val: any): string => {
            return StaticUtils.ConversionHelper.toFloat(val, 2);
        },

        /**
         * Converts value to float and upto 3 decimal places
         * @method toDecimal3
         * @param val - Value to be converted
         * @return Formatted value
         */
        toDecimal3: (val: any): string => {
            return StaticUtils.ConversionHelper.toFloat(val, 3);
        },

        /**
         * Converts value to float and upto 4 decimal places
         * @method toDecimal4
         * @param val - Value to be converted
         * @return Formatted value
         */
        toDecimal4: (val: any): string => {
            return StaticUtils.ConversionHelper.toFloat(val, 4);
        },

        /**
         * Converts value to float and upto 5 decimal places
         * @method toDecimal5
         * @param val - Value to be converted
         * @return Formatted value
         */
        toDecimal5: (val: any): string => {
            return StaticUtils.ConversionHelper.toFloat(val, 5);
        },

        /**
         * Converts value to float and upto 6 decimal places
         * @method toDecimal6
         * @param val - Value to be converted
         * @return Formatted value
         */
        toDecimal6: (val: any): string => {
            return StaticUtils.ConversionHelper.toFloat(val, 6);
        },

        /**
         * Converts value to time in HH:MM format
         * @method toTime
         * @param val - Value to be converted
         * @return Formatted value
         */
        toTime: (val: any): string => {
            return StaticUtils.secondsToHMS(val);
        },

        /**
         * Converts value to code i.e. capitializes the value
         * @method toCode
         * @param val - Value to be converted
         * @return Formatted value
         */
        toCode: (val: any): string => {
            return val ? val.toUpperCase() : val;
        }
    };

    /**
     * @description Set grid input box to readonly using cell index
     * @method - setGridInputReadonly
     * @param - cellIndex - Cell index to set readonly; Index starts from 1
     * @return - void
     */
    public static setGridInputReadonly(cellIndex: number): void {
        setTimeout(function (): void {
            let elems: any = document.querySelectorAll('.gridtable tbody tr td:nth-child(' + cellIndex + ') input');

            elems.forEach(element => {
                element.setAttribute('readonly', '');
            });
        }, 300);
    }

    /**
     * @description Converts all alphabetic charaters to upper case
     * @method - toUpperCase
     * @param - value - String to be converted to upper case
     * @return - void
     */
    public static toUpperCase(value: any): string {
        return value ? value.toString().toUpperCase() : '';
    }

    /**
     * @description Below fn removes white space from texts
     * @return - string
     */
    public static removeWhiteSpace(someTextWithWhiteSpace: string): string {
        return someTextWithWhiteSpace.replace(/ /g, '');
    }

    /**
     * @description Get Query Paramaters From A URL
     * @method getQueryParameterByName
     * @param {string} url - URL String
     * @param {string} name - Parameter Name
     * @returns {string}
     */
    public static getParameterByName(url: string, name: string): string {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    public static capitalizeFirstLetterAlphaNumberic(value: string): string {
        if (value) {
            return value.toString().split(' ').map(strPart => {
                return strPart ? strPart[0].toUpperCase() + strPart.substr(1) : '';
            }).join(' ');
        } else {
            return '';
        }
    }

    public static setTimeToMinutes(): string {
        let today: Date = new Date(),
            hours: number = today.getHours(),
            minutes: number = today.getMinutes();
        return ((Number(hours) * 60) + Number(minutes)).toString();
    }

    public static getFirstDayOfMonth(): Date {
        let firstDate: Date = new Date();

        return new Date(firstDate.setDate(1));
    }

    public static getFirstAndLastDayOfYear(): Array<Date> {
        let firstDay: Date = new Date();
        let lastDay: Date = new Date();

        firstDay.setMonth(0);
        firstDay.setDate(1);
        lastDay.setMonth(11);
        lastDay.setDate(31);

        return [firstDay, lastDay];
    }

    public static getReportManDest(value: string, printer?: string): string {
        let dest: string = '';

        switch (value.toLowerCase()) {
            case 'direct':
                dest = 'batch|ReportID';
                break;
            case 'email':
                dest = 'email|User';
                break;
            case 'printer':
                dest = 'print|' + printer;
                break;
        }

        return dest;
    }

    /**
     * If invalid date or no date passed it will use current date
     */
    public static getWeek(value?: string): number {
        let date: Date = value ? new Date(value) : new Date();

        if (date.toString() === 'Invalid Date') {
            date = new Date();
        }

        date.setHours(0, 0, 0, 0);
        // Thursday in current week decides the year.
        date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
        // January 4 is always in week 1.
        let week1 = new Date(date.getFullYear(), 0, 4);
        // Adjust to Thursday in week 1 and count number of weeks from date to week1.
        return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
            - 3 + (week1.getDay() + 6) % 7) / 7);
    }

    /**
     * Get Page Title
     */
    public static getTitle(): string {
        let pageTitleElem: any = document.querySelector('.screen-header h1')
            || document.querySelector('.page-title h1')
            || document.querySelector('.page-header small');

        return pageTitleElem.innerHTML;
    }
}
