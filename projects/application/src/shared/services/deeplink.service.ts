import { CBBConstants } from './../constants/cbb.constants';
import { AppModuleRoutes } from '@base/PageRoutes';
import { StaticUtils } from './static.utility';
import { NGXLogger } from 'ngx-logger';
import { SessionStorageService } from 'ngx-webstorage';
import { Utils } from './utility';
import { Injectable } from '@angular/core';
import { Router, NavigationStart, NavigationCancel } from '@angular/router';

@Injectable()
export class DeepLinkService {
    private isDeepLinked: boolean = false;
    private key: string;
    private url: string;
    private path: string;
    private params: any;
    private requiredParams: Array<string> = [];
    private branchCodePattern: any = /^[0-9]{1,4}$/;
    private utils: Utils;

    // Constants
    public static readonly c_s_URL_PARAM_STORAGE_KEY: string = 'DeepLinkURL';
    public static readonly c_s_URL_PARAM_DEEPLINK: string = 'deeplink';
    public static readonly c_s_URL_PARAM_PARENTMODE: string = 'parentMode';
    public static readonly c_n_KEY_LENGTH: number = 10;

    constructor(private sessionStorage: SessionStorageService,
        private router: Router,
        private logger: NGXLogger) {

        this.params = {};
        this.router.events.filter(event => event instanceof NavigationStart).subscribe((event) => {
            let key: string = StaticUtils.getSHA256Hash({
                value: DeepLinkService.c_s_URL_PARAM_STORAGE_KEY,
                salt: DeepLinkService.c_s_URL_PARAM_DEEPLINK,
                len: DeepLinkService.c_n_KEY_LENGTH
            });
            let url: string = this.sessionStorage.retrieve(key);
            if (!url) {
                url = event['url'];
                this.registerDeepLink();
            }

            if (url.indexOf(DeepLinkService.c_s_URL_PARAM_DEEPLINK) >= 0) {
                this.url = url;
                this.parseURL();
                if (!this.validateURLParams()) {
                    this.router.navigate([AppModuleRoutes.POSTLOGIN]);
                }
            }
        });
        this.router.events.filter(event => event instanceof NavigationCancel).subscribe((event) => {
            let key: string = StaticUtils.getSHA256Hash({
                value: DeepLinkService.c_s_URL_PARAM_STORAGE_KEY,
                salt: DeepLinkService.c_s_URL_PARAM_DEEPLINK,
                len: DeepLinkService.c_n_KEY_LENGTH
            });
            let url: string = this.sessionStorage.retrieve(key);
            if (!url) {
                url = event['url'];
                this.registerDeepLink();
            }

            if (url.indexOf(DeepLinkService.c_s_URL_PARAM_DEEPLINK) >= 0) {
                this.url = url;
                this.parseURL();
                if (!this.validateURLParams()) {
                    this.router.navigate([AppModuleRoutes.POSTLOGIN]);
                }
            }
        });

        // Set up required params
        this.requiredParams = [
            DeepLinkService.c_s_URL_PARAM_PARENTMODE,
            CBBConstants.c_s_URL_PARAM_BUSINESSCODE,
            CBBConstants.c_s_URL_PARAM_COUNTRYCODE,
            CBBConstants.c_s_URL_PARAM_BRANCHCODE
        ];
    }

    // Is URL Deep Linked
    public get isURLDeepLinked(): boolean {
        return this.isDeepLinked;
    }

    public set isURLDeepLinked(value: boolean) {
        this.isDeepLinked = value;
    }

    // Parse URL To Get The Parameters
    private parseURL(): void {
        let urlParts: Array<string> = this.url.split('?');

        if (!urlParts[1] || urlParts[1].indexOf(DeepLinkService.c_s_URL_PARAM_DEEPLINK) < 0) {
            return;
        }

        this.path = urlParts[0];

        urlParts[1].split('&').forEach(val => {
            let param: Array<any> = val.split('=');
            if (param[0] !== DeepLinkService.c_s_URL_PARAM_DEEPLINK) {
                this.params[param[0]] = param[1];
            }
        });
    }

    // Validate If All The Required Parameters Are Present In The URL
    private validateURLParams(): boolean {
        /* let isInvalid: boolean = false;
        if (!this.cbbService.getCBBList()) {
            return true;
        }
        // Few checks are kept raw, to minimize the number of iterations
        this.requiredParams.forEach(name => {
            let branchList: Array<any> = [];
            if (!this.params || !this.params[name]) {
                isInvalid = true;
                return;
            }
            if (name === CBBConstants.c_s_URL_PARAM_COUNTRYCODE && !this.cbbService.getBusinessListByCountry(this.params[name]).length) {
                isInvalid = true;
                return;
            }

            if (name === CBBConstants.c_s_URL_PARAM_BUSINESSCODE && !this.cbbService.getBranchListByCountryAndBusiness(this.params[CBBConstants.c_s_URL_PARAM_COUNTRYCODE], this.params[name]).length) {
                isInvalid = true;
                return;
            }

            if (name === CBBConstants.c_s_URL_PARAM_BRANCHCODE) {
                let branchCode: any = this.params[name];

                if (!this.branchCodePattern.test(branchCode)) {
                    isInvalid = true;
                    return;
                }

                if (!this.cbbService.isValidBranch(this.params[CBBConstants.c_s_URL_PARAM_COUNTRYCODE], this.params[CBBConstants.c_s_URL_PARAM_BUSINESSCODE], this.params[CBBConstants.c_s_URL_PARAM_BRANCHCODE])) {
                    isInvalid = true;
                    return;
                }
            }
        });

        if (isInvalid) {
            this.discardDeepLinking();
        } */

        return true;
    }

    // Create Storage Object
    public registerDeepLink(): void {
        if (!this.url || this.url === AppModuleRoutes.LOGIN) {
            return;
        }

        try {
            this.isDeepLinked = true;

            if (!this.key) {
                this.key = StaticUtils.getSHA256Hash({
                    value: DeepLinkService.c_s_URL_PARAM_STORAGE_KEY,
                    salt: DeepLinkService.c_s_URL_PARAM_DEEPLINK,
                    len: DeepLinkService.c_n_KEY_LENGTH
                });
            }

            this.sessionStorage.store(this.key, this.url);
        } catch (excp) {
            this.logger.log('DeepLinkService', excp);
            this.discardDeepLinking();
        }
    }

    public initDeepLink(): void {
        let url: string = window.location.href.split('#')[1];

        if (url && url.indexOf(DeepLinkService.c_s_URL_PARAM_DEEPLINK) >= 0) {
            this.url = url;
            this.parseURL();

            this.isDeepLinked = true;

            if (!this.key) {
                this.key = StaticUtils.getSHA256Hash({
                    value: DeepLinkService.c_s_URL_PARAM_STORAGE_KEY,
                    salt: DeepLinkService.c_s_URL_PARAM_DEEPLINK,
                    len: DeepLinkService.c_n_KEY_LENGTH
                });
            }

            this.sessionStorage.store(this.key, this.url);
        }
    }

    // Navigate to deep link
    public navigateToDeepLinkPage(): void {
        let path: string = this.path;
        let params: any = StaticUtils.deepCopy(this.params);

        this.discardDeepLinking();

        if (!this.validateURLParams()) {
            this.router.navigate([AppModuleRoutes.POSTLOGIN]);
        } else {
            this.router.navigate([path], { queryParams: params });
        }
    }

    // Discard Deep Linking
    public discardDeepLinking(): void {
        this.sessionStorage.clear(this.key);
        this.isDeepLinked = false;
        this.url = null;
        this.key = null;
        this.path = null;
        this.params = {};
        this.requiredParams = [];
    }
}
