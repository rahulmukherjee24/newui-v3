export class RenegsConstants {
    public static readonly AccountName: string = 'Account Name';
    public static readonly AccountNumber: string = 'Account Number';
    public static readonly AddressLine1: string = 'Address Line 1';
    public static readonly AddressLine2: string = 'Address Line 2';
    public static readonly AddressLine3: string = 'Address Line 3';
    public static readonly AmendmentRef: string = 'Amendement Ref';
    public static readonly btnCancelTitle: string = 'Cancel';
    public static readonly btnDeleteTitle: string = 'Delete';
    public static readonly btnGeocode: string = 'Geocode';
    public static readonly btnSaveTitle: string = 'Save';
    public static readonly btnUpdateTitle: string = 'Update';
    public static readonly ClientReference: string = 'Client Reference';
    public static readonly CommenceDate: string = 'Commence Date';
    public static readonly ContactName: string = 'Contact Name';
    public static readonly ContractDetailsFormFields: any = {
        lblAgreementNo: 'Agreement Number',
        lblCompanyRegNo: 'Company Registration Number',
        lblContractName: 'Contact Name',
        lblContinousContract: 'Continous Contract',
        lblDepartment: 'Department',
        lblEmail: 'Email',
        lblFax: 'Fax',
        lblInvoiceFreq: 'Invoice Frequency',
        lblLandLine: 'Landline Telephone',
        lblMinDuration: 'Minimum Duration',
        lblMobileNo: 'Mobile Number',
        lblName: 'Name',
        lblPaymentMethod: 'Payment Method',
        lblPosition: 'Position',
        lblRenewalAgreementNo: 'Renewal Agreement Number',
        lblTaxRegNo: 'Tax Registration Number',
        lblTaxRegNo2: 'Tax Registration Number 2'
    };
    public static readonly ContractNumber: string = 'Contract Number';
    public static readonly County: string = 'County';
    public static readonly CurrentPremises: string = 'Current Premises';
    public static readonly Department: string = 'Department';
    public static readonly EffectiveDate: string = 'Effective Date';
    public static readonly Email: string = 'Email';
    public static readonly errorMsgOnInvalidInput: string = 'Invalid Input, Please enter valid integer...';
    public static readonly ExistAccountNumber: string = 'Existing Account Number';
    public static readonly ExistCommDate: string = 'Existing Commence Date';
    public static readonly ExistContractNumber: string = 'Existing Contract Number';
    public static readonly Fax: string = 'Fax';
    public static readonly General: string = 'General';
    public static readonly GPSCoordinatex: string = ' Coordinate X';
    public static readonly GPSCoordinatey: string = ' Coordinate Y';
    public static readonly GroupAccountName: string = 'Group Name';
    public static readonly GroupAccountNumber: string = 'Group Account Number';
    public static readonly InvoiceAnnivDate: string = 'Invoice Anniv Date';
    public static readonly InvoiceFreq: string = 'Invoice Frequency';
    public static readonly labelFilters: string = 'Filters';
    public static readonly LandLineTelephone: string = 'Landline Telephone';
    public static readonly MobileNumber: string = 'Mobile Number';
    public static readonly msgTypeError: string = 'error';
    public static readonly msgTypeSuccess: string = 'success';
    public static readonly msgTypeWarn: string = 'warning';
    public static readonly Name: string = 'Name';
    public static readonly NegBranch: string = 'Neg Branch';
    public static readonly NewAccount: string = 'New Account';
    public static readonly NewContract: string = 'New Contract';
    public static readonly NoRecord: string = 'No Record Found.';
    public static readonly pageTitleAccountDetails: string = 'Renegotiations - Account Details';
    public static readonly pageTitleAddReneg: string = 'Renegotiations - Add';
    public static readonly pageTitleContactDetailsNew: string = 'New Renegotiations - Contact Details';
    public static readonly pageTitleContractDetails: string = 'Renegotiations - Contract Details';
    public static readonly pageTitleMain: string = 'Renegotiations';
    public static readonly pageTitlePremisesGrid: string = 'Renegotiations - Premises Grid';
    public static readonly pageTitlePremisesMaintainance: string = 'Renegotiations - Premises Maintainance';
    public static readonly pageTitleServiceCoverGrid: string = 'Renegotiations - Service Cover Grid';
    public static readonly pageTitleUpdateReneg: string = 'Renegotiations - Update';
    public static readonly Position: string = 'Position';
    public static readonly PostCode: string = 'Postcode';
    public static readonly PremisesName: string = 'Premises Name';
    public static readonly PremisesNumber: string = 'Premises Number';
    public static readonly PurchaseOrderNo: string = 'Purchase Order No';
    public static readonly RenegsAddUpdateDefaultMode: string = 'new';
    public static readonly RenegType: string = 'Reneg Type';
    public static readonly RiskAssessmentText: string = 'Risk Assessment Text';
    public static readonly Score: string = 'Score';
    public static readonly SelRoutingSource: string = 'Source';
    public static readonly Telephone: string = 'Telephone';
    public static readonly Town: string = 'Town';
    public static readonly UseExistAccount: string = 'Use Existing Account';
    public static readonly UseExistContract: string = 'Use Existing Contract';
    public static readonly Value: string = 'Value';
}
