import { Component, Injector, OnInit, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';

import { RenegotiationRoutes, ContractManagementModuleRoutes } from '@base/PageRoutes';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsGridInterface } from '../RenegsInterfaces/iCABSARenegsGrid.interface';
import { RenegsXHRParamsInterface } from './../RenegsInterfaces/iCABSARenegsXHRParams.interface';


@Component({
    selector: 'icabs-renegs-grid',
    templateUrl: 'iCABSARenegsGrid.component.html'
})
export class RenegsGridComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') riPagination: PaginationComponent;

    private fetchDataforRiGridSubscription: Subscription;
    private getRenegsFilterOptionsSubscription: Subscription;
    private renegsFilterVal: string;
    private selectedFilter: string;
    private xhrParams: RenegsXHRParamsInterface = {
        module: 'contract-admin',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSARenegGrid'
    };

    public actualPageSize: number = 10;
    public controls: RenegsFormControlsInterface[] = [
        { name: 'RenegsFilter' },
        { name: 'renegsFilterOptionsList' }
    ];
    public customAlertMsg: any;
    public labelFilter: string = RenegsConstants.labelFilters;
    public messageType: string;
    public messageConstant = MessageConstant.Message;
    public pageId: string = '';
    public pageTitle: string = '';
    public renegsFilterOptions: any[];

    constructor(injector: Injector) {
        super(injector);
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitleMain;
        this.pageId = PageIdentifier.ICABSARENEGGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.totalRecords = 1;
        this.pageParams.pageSize = 10;
        this.pageParams.isPaginationEnabled = false;
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
    }

    ngAfterContentInit(): void {
        this.handleBackStack(this.pageId);
        this.renegsFilterVal = this.getControlValue('RenegsFilter');

        if (this.isReturning()) {
            this.renegsFilterOptions = this.formData.renegsFilterOptionsList;
            this.populateUIFromFormData();

            this.pageParams.gridCacheRefresh = !this.isReturning();
            this.fetchDataforRiGrid(this.renegsFilterVal);

        } else {
            this.pageParams.gridCurPage = 1;
            this.pageParams.totalRecords = 1;
            this.getRenegsFilterOptions();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.fetchDataforRiGridSubscription) {
            this.fetchDataforRiGridSubscription.unsubscribe();
        }
        if (this.getRenegsFilterOptionsSubscription) {
            this.getRenegsFilterOptionsSubscription.unsubscribe();
        }
    }

    /* building grid columns after fetching data from API */
    private buildRiGrid(riGridData?: RenegsGridInterface[]): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('riGridRef', 'RenegRef', 'riGridRef', MntConst.eTypeTextFree, 18);
        this.riGrid.AddColumn('riGridType', 'RenegType', 'riGridType', MntConst.eTypeTextFree, 8);
        this.riGrid.AddColumn('riGridServiceContractNumber', 'ServiceContactNumber', 'riGridServiceContractNumber', MntConst.eTypeCode, 9);
        this.riGrid.AddColumn('riGridServiceContractName', 'ServiceContactName', 'riGridServiceContractName', MntConst.eTypeTextFree, 14);
        this.riGrid.AddColumn('riGridEffectiveDate', 'EffectiveDate', 'riGridEffectiveDate', MntConst.eTypeDate, 4);
        this.riGrid.AddColumn('riGridCommenceDate', 'CommenceDate', 'riGridCommenceDate', MntConst.eTypeDate, 2);
        this.riGrid.AddColumn('riGridPremises', 'Premises', 'riGridPremises', MntConst.eTypeTextFree, 4);
        this.riGrid.AddColumn('riGridServiceCovers', 'ServiceCovers', 'riGridServiceCovers', MntConst.eTypeTextFree, 6);
        this.riGrid.AddColumn('riGridValueChange', 'ValueChange', 'riGridValueChange', MntConst.eTypeCurrency, 3);
        this.riGrid.AddColumn('riGridStatus', 'RenegStatus', 'riGridStatus', MntConst.eTypeTextFree, 3);
        this.riGrid.AddColumn('riGridView', 'RenegView', 'riGridView', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('riGridAction', 'RenegAction', 'riGridAction', MntConst.eTypeButton, 2);

        this.riGrid.AddColumnAlign('riGridCommenceDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridValueChange', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridStatus', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridEffectiveDate', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    /* fetch grid data on the basis of filter option drop down */
    private fetchDataforRiGrid(selectedFilter: string): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams = this.getURLSearchParamObject();
        searchParams.set('FilterBy', selectedFilter);
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
        searchParams.set(this.serviceConstants.BranchNumber, this.cbbService.getBranchCode());
        searchParams.set(this.serviceConstants.BusinessCode, this.cbbService.getBusinessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.cbbService.getCountryCode());
        searchParams.set(this.serviceConstants.Function, 'RenegGrid');
        searchParams.set(this.serviceConstants.PageSize, (this.pageParams.pageSize === 0) ? 1 : this.pageParams.pageSize);
        searchParams.set(this.serviceConstants.GridPageCurrent, (this.pageParams.gridCurPage === 0) ? 1 : this.pageParams.gridCurPage);
        this.riGrid.Clear();

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response['hasError'] || response['errorMessage']) {
                    this.pageParams.totalRecords = 0;
                    this.pageParams.gridCurPage = 0;
                    this.pageParams.isPaginationEnabled = false;
                    this.notifyOnAPIError(response);

                } else {

                    if (response && response['pageData']) {
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        this.pageParams.gridCurPage = response['pageData'] ? response['pageData'].pageNumber : 0;
                        this.pageParams.totalRecords = response['pageData'] ? response['pageData'].lastPageNumber * this.actualPageSize : 0;
                        this.pageParams.isPaginationEnabled = true;

                        this.riGrid.RefreshRequired();
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riPagination.setPage(this.pageParams.gridCurPage);
                            }, 250);
                        }
                        this.buildRiGrid(response);
                        this.riGrid.Execute(response);
                    } else {
                        this.notifyOnAPIError(response);
                    }

                }

            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.notifyOnAPIError(error);

            });
    }

    /* fetching filteroptions drop-down data, required for fetching grid API */
    private getRenegsFilterOptions(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set(this.serviceConstants.Function, 'RenegStatusSelect');
        searchParams.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response['hasError'] || response['errorMessage']) {
                    this.notifyOnAPIError(response);

                } else {
                    this.renegsFilterOptions = Object.keys(response).map((key) => {
                        return { 'title': response[key], 'value': key };
                    });
                    this.setControlValue('renegsFilterOptionsList', this.renegsFilterOptions);
                    this.selectedFilter = this.renegsFilterOptions[this.renegsFilterOptions.length - 1]['title'];
                    this.setControlValue('RenegsFilter', this.selectedFilter);
                    this.fetchDataforRiGrid(this.selectedFilter);
                }
            },
                (error) => {
                    this.notifyOnAPIError(error);
                });
    }

    private notifyOnAPIError(error: any): void {
        this.messageType = ((error['hasError'] || error['errorMessage'])) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    private onPrintReport(): void { // ITA-670
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set('riBusinessCode', this.utils.getBusinessCode());
        searchParams.set('advQuoteRef', this.riGrid.Details.GetValue('riGridRef'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response['hasError'] || response['errorMessage']) {
                    this.notifyOnAPIError(response);

                } else {
                    this.openRenegPDFReport(response['url']);
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.notifyOnAPIError(error);
                });
    }

    private openRenegPDFReport(url: string): void {
        const routingSearchWindow: any = window.open(url,
            '_blank',
            'fullscreen=yes,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,width=' + screen.width + ',height=' + screen.height);
    }

    public onAddNewReneg(): void {
        this.navigate('addNewReneg', RenegotiationRoutes.ICABSARENEGSADDUPDATEFULLPATH);
    }

    public onCellClick(data: any): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        const textContent: string = data.target.textContent.toLowerCase();
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set('advQuoteRef', this.riGrid.Details.GetAttribute('riGridAction', 'rowid'));

        if (textContent === 'submit') {
            searchParams.set(this.serviceConstants.Function, 'Submit');
            this.onGridActionButtonClick(searchParams);

        } else if (textContent === 'process') {
            searchParams.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
            searchParams.set(this.serviceConstants.Function, 'ProcessAmendment');
            searchParams.set('Level', 'Branch');
            this.onGridActionButtonClick(searchParams);

        } else if (textContent === 'review') {
            searchParams.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
            searchParams.set(this.serviceConstants.Function, 'SetToReviewed');
            this.onGridActionButtonClick(searchParams);

        } else if (data.target.offsetParent && data.target.offsetParent.attributes['additionalproperty'] && data.target.offsetParent.attributes['additionalproperty'].textContent.toLowerCase() === 'show report') {
            this.onPrintReport();  // ITA-670
        }
    }

    public onClickRiGridBody(event: any): void {
        return;
    }

    public OnDblClickRiGridBody(event: any): void {
        const renegType: string = this.riGrid.Details.GetValue('riGridType').toLowerCase();
        switch (this.riGrid.CurrentColumnName.toLowerCase()) {
            case 'rigridref':
                this.navigate('updateReneg', RenegotiationRoutes.ICABSARENEGSADDUPDATEFULLPATH, {
                    advQuoteRef: this.riGrid.Details.GetValue('riGridRef')
                });
                break;

            case 'rigridtype':
                if (this.riGrid.Details.GetValue('riGridType')) {  // to accountmaintenance screen

                    let searchupdateParams: QueryParams = this.getURLSearchParamObject();
                    searchupdateParams.set('action', '0');
                    searchupdateParams.set(this.serviceConstants.BusinessCode, this.businessCode());
                    searchupdateParams.set(this.serviceConstants.CountryCode, this.countryCode());
                    searchupdateParams.set('advQuoteRef', this.riGrid.Details.GetValue('riGridRef'));

                    this.ajaxSource.next(this.ajaxconstant.START);

                    this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, 'Application/iCABSRenegMaintenance', searchupdateParams).subscribe((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data['hasError'] || data['errorMessage']) {
                            this.notifyOnAPIError(data);

                        } else {
                            this.navigate('CallCentreSearch', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, {
                                'AccountNumber': data.RenegAccountNumber,
                                'AccountName': data.RenegAccountName,
                                'parentMode': 'CallCentreSearch'
                            });
                        }

                    }, (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.notifyOnAPIError(error);
                    });
                }
                break;

            case 'rigridservicecontractnumber':
                if (this.riGrid.Details.GetValue('riGridServiceContractNumber')) {  // to contractmaintenance screen
                    this.navigate('ServiceCover', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                        'ContractNumber': this.riGrid.Details.GetValue('riGridServiceContractNumber'),
                        'parentMode': 'ServiceCover'
                    });
                }
                break;

            case 'rigridservicecovers':
                this.navigate('serviceCoverGrid', RenegotiationRoutes.ICABSARENEGSSERVICECOVERGRIDFULLPATH, {
                    amendmentRef: this.riGrid.Details.GetValue('riGridRef'),
                    contractNumber: this.riGrid.Details.GetValue('riGridServiceContractNumber'),
                    contractName: this.riGrid.Details.GetValue('riGridServiceContractName'),
                    premisesNumber: this.riGrid.Details.GetValue('riGridPremises'),
                    renegType: renegType
                });
                break;

            case 'rigridpremises':
                this.navigate('premisesgrid', RenegotiationRoutes.ICABSARENEGSPREMISESGRIDPATH, {
                    advQuoteRef: this.riGrid.Details.GetValue('riGridRef'),
                    PremiseNumber: this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName),
                    contractNumber: this.riGrid.Details.GetValue('riGridServiceContractNumber'),
                    contractName: this.riGrid.Details.GetValue('riGridServiceContractName'),
                    renegType: renegType
                });
                break;

            default:
                break;
        }

        if (event.target.offsetParent && event.target.offsetParent.attributes['additionalproperty'] && event.target.offsetParent.attributes['additionalproperty'].textContent.toLowerCase() === 'show report') {
            this.onPrintReport();  // ITA-670
        }
    }

    public onGridActionButtonClick(searchParams: QueryParams): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams)
            .subscribe((response) => {

                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);
                } else {
                    this.onRefreshRiGrid();
                }

            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.notifyOnAPIError(error);

                });
    }

    public onGridCellClick(e: any): void { }

    public onKeyDownRiGridBody(event: any): void { return; }

    public onGridSort(): void { return; }

    //  refresh the grid content
    public onRefreshRiGrid(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.gridReset();
        this.fetchDataforRiGrid(this.selectedFilter);
    }

    //  Get page specific grid data from service end
    public getCurrentPage(data: any): void {
        if (this.pageParams.gridCurPage === data.value) return;
        this.pageParams.gridCurPage = data.value;
        this.fetchDataforRiGrid(this.renegsFilterVal);
    }

    public onTRClick(ev: any): void {
        return;
    }

    /* on filter option change, need to make API call for fetching grid data */
    public onUpdateSelectedFilter(): void {
        const renegsFilterVal: string = this.renegsFilterVal = this.getControlValue('RenegsFilter');

        this.renegsFilterOptions.forEach((item) => {
            if (item.value === renegsFilterVal) {
                this.selectedFilter = item.value;
            }
        });
        this.gridReset();
        this.pageParams.totalRecords = 0;
    }

    public gridReset(): void {
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
        this.buildRiGrid();
        this.pageParams.gridCurPage = 0;
        this.pageParams.totalRecords = 1;
    }
}
