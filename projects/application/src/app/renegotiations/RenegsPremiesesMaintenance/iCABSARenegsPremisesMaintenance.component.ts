import { AfterContentInit, Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsPremisesMaintainanceInterface } from './../RenegsInterfaces/iCABSARenegsPremisesMaintainance.interface';
import { RenegsXHRParamsInterface } from './../RenegsInterfaces/iCABSARenegsXHRParams.interface';

@Component({
    selector: 'icabs-renegs-premises-maintenance',
    templateUrl: 'iCABSARenegsPremisesMaintenance.component.html'
})

export class RenegsPremisesMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('customAlert') public customAlert;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private xhrParams: RenegsXHRParamsInterface = {
        module: 'premises',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSARenegPremiseMaintenance'
    };

    public controls: RenegsFormControlsInterface[] = [
        { name: 'AmendmentRef', type: MntConst.eTypeText, disabled: true },
        { name: 'PremisesNumber', type: MntConst.eTypeText },
        { name: 'AddressLine1', type: MntConst.eTypeText, disabled: false },
        { name: 'AddressLine2', type: MntConst.eTypeText, disabled: false },
        { name: 'AddressLine3', type: MntConst.eTypeText, disabled: false },
        { name: 'Town', type: MntConst.eTypeText, disabled: false },
        { name: 'State', type: MntConst.eTypeText, disabled: false },
        { name: 'PostCode', type: MntConst.eTypeText, disabled: false },
        { name: 'Name', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactName', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactPosition', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactDepartment', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactTelephone', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactMobile', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactFax', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseContactEmail', type: MntConst.eTypeText, disabled: false },
        { name: 'PremiseSpecialInstructions', type: MntConst.eTypeTextFree, disabled: false },
        { name: 'ClientReference', type: MntConst.eTypeText, disabled: false },
        { name: 'PurchaseOrderNo', type: MntConst.eTypeText, disabled: false }

    ];
    public customAlertMsg: any;
    public messageType: string;
    public pageId: string;
    public pageTitle: string;
    public parentMode: string;
    public renegsConstants: RenegsConstants = RenegsConstants;
    public tabNameMap: any = {
        general: true,
        risk_assessment_text: false
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENEGPREMISESMAIN;
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitlePremisesMaintainance;
    }

    ngAfterContentInit(): void {
        this.changeFormFieldsStateInUpdateMode();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode() || 'renegsPremisesMaintenance';
        this.setupPageParams();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private changeFormFieldsStateInUpdateMode(): void {
        let searchupdateParams: QueryParams = this.getURLSearchParamObject();
        searchupdateParams.set('ACTION', '0');
        searchupdateParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchupdateParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchupdateParams.set('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        searchupdateParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        searchupdateParams.set('riHTMLPage', 'Application/iCABSARenegPremiseMaintenance.htm');
        this.ajaxSource.next(this.ajaxconstant.START);

        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchupdateParams).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data['hasError'] || data['errorMessage']) {
                this.notifyOnAPIError(data);
            } else {
                this.setControlValue('AddressLine1', data.PremiseAddressLine1);
                this.setControlValue('AddressLine2', data.PremiseAddressLine2);
                this.setControlValue('AddressLine3', data.PremiseAddressLine3);
                this.setControlValue('AmendmentRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
                this.setControlValue('ClientReference', data.ClientReference);
                this.setControlValue('Name', data.PremiseName);
                this.setControlValue('PostCode', data.PremisePostcode);
                this.setControlValue('PremiseContactDepartment', data.PremiseContactDepartment);
                this.setControlValue('PremiseContactEmail', data.PremiseContactEmail);
                this.setControlValue('PremiseContactFax', data.PremiseContactFax);
                this.setControlValue('PremiseContactMobile', data.PremiseContactMobile);
                this.setControlValue('PremiseContactName', data.PremiseContactName);
                this.setControlValue('PremiseContactPosition', data.PremiseContactPosition);
                this.setControlValue('PremiseContactTelephone', data.PremiseContactTelephone);
                this.setControlValue('PremisesNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                this.setControlValue('PurchaseOrderNo', data.PurchaseOrderNo);
                this.setControlValue('State', data.PremiseAddressLine5);
                this.setControlValue('Town', data.PremiseAddressLine4);
            }
        }, (error) => {
            this.notifyOnAPIError(error);
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    private generateAPIPayloadForUpdate(): RenegsPremisesMaintainanceInterface {
        let payload: RenegsPremisesMaintainanceInterface = {
            advQuoteRef: this.getControlValue('AmendmentRef'),
            ClientReference: this.getControlValue('ClientReference'),
            GPSCoordinateX: '',
            GPSCoordinateY: '',
            PremiseAddressLine1: this.getControlValue('AddressLine1'),
            PremiseAddressLine2: this.getControlValue('AddressLine2'),
            PremiseAddressLine3: this.getControlValue('AddressLine3'),
            PremiseAddressLine4: this.getControlValue('Town'),
            PremiseAddressLine5: this.getControlValue('State'),
            PremiseContactDepartment: this.getControlValue('PremiseContactDepartment'),
            PremiseContactEmail: this.getControlValue('PremiseContactEmail'),
            PremiseContactFax: this.getControlValue('PremiseContactFax'),
            PremiseContactMobile: this.getControlValue('PremiseContactMobile'),
            PremiseContactName: this.getControlValue('PremiseContactName'),
            PremiseContactPosition: this.getControlValue('PremiseContactPosition'),
            PremiseContactTelephone: this.getControlValue('PremiseContactTelephone'),
            PremiseName: this.getControlValue('Name'),
            PremisePostCode: this.getControlValue('PostCode'),
            PremiseSpecialInstructions: '',
            PremiseSRADate: '',
            PremiseSRAEmployee: '',
            PurchaseOrderNo: this.getControlValue('PurchaseOrderNo'),
            RoutingScore: '',
            RoutingSource: ''
        };
        return payload;
    }

    private notifyOnAPIError(error: any): void {
        this.messageType = (error['hasError'] || error['errorMessage']) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    public onCancelMaintainaceUpdate(): void {
        this.location.back();
    }

    public onSavePremiseMaintainance(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        searchParams.set('advQuoteRef', this.getControlValue('AmendmentRef'));
        searchParams.set('riHTMLPage', 'Application/iCABSARenegPremiseMaintenance.htm');
        let payload: RenegsPremisesMaintainanceInterface = this.generateAPIPayloadForUpdate();

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams['method'], this.xhrParams['module'], this.xhrParams['operation'], searchParams, payload)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);

                } else {
                    //success
                    setTimeout(() => {
                        this.location.back();
                    }, 1000);
                }
            },
            (error) => {
                this.notifyOnAPIError(error);
            });
    }

    private setupPageParams(): void {
        this.pageParams.actionOnAddNew = '1';
        this.pageParams.actionOnUpdate = '2';
        this.pageParams.BusinessCode = this.businessCode();
        this.pageParams.CountryCode = this.countryCode();
    }

    public changeTab(tabname: string): void {
        for (let key in this.tabNameMap) {
            if (this.tabNameMap.hasOwnProperty(key)) {
                this.tabNameMap[key] = false;
            }
        }
        this.tabNameMap[tabname] = true;
    }
}
