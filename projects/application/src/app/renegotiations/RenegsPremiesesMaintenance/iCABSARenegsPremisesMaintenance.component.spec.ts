import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenegsAddUpdateComponent } from './iCABSARenegsAddUpdate.component';

describe('RenegsAddUpdateComponent', () => {
    let component: RenegsAddUpdateComponent;
    let fixture: ComponentFixture<RenegsAddUpdateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RenegsAddUpdateComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RenegsAddUpdateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
