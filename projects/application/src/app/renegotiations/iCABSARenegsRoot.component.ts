import { Component, ViewContainerRef } from '@angular/core';

@Component({
    template: `<router-outlet></router-outlet>`
})
export class RenegsRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {}
}
