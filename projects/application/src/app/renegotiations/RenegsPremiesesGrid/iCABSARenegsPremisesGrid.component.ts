import { Component, Injector, OnInit, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { RenegotiationRoutes } from './../../base/PageRoutes';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsGridInterface } from '../RenegsInterfaces/iCABSARenegsGrid.interface';
import { RenegsXHRParamsInterface } from './../RenegsInterfaces/iCABSARenegsXHRParams.interface';

@Component({
    selector: 'icabs-renegs-premises-grid',
    templateUrl: 'iCABSARenegsPremisesGrid.component.html'
})

export class RenegsPremisesGridComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('customAlert') public customAlert;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') riPagination: PaginationComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private xhrParams: RenegsXHRParamsInterface = {
        module: 'premises',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSARenegPremiseGrid'
    };

    public actualPageSize: number = 10;
    public contractSearchComponent = ContractSearchComponent;
    public controls: RenegsFormControlsInterface[] = [
        { name: 'AmendmentRef', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'contracttypecode', type: MntConst.eTypeText, disabled: true }
    ];
    public customAlertMsg: any;
    public disableContractDetails: boolean;
    public enableAutoOpen: boolean = false;
    public inputParamsContractSearch: any = {
        'parentMode': 'premisesGrid',
        'accountNumber': '',
        'currentContractType': 'C',
        'enableAccountSearch': false
    };
    public messageType: string;
    public pageId: string;
    public pageTitle: string;
    public parentMode: string;
    public renegsConstants: RenegsConstants = RenegsConstants;
    public renegType: string;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENEGPREMISESGRID;
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitlePremisesGrid;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode() || 'addNewReneg';
        this.renegType = this.riExchange.getParentHTMLValue('renegType');

        this.disableContractDetails = (this.renegType === 'copy') ? true : false;
        if (this.disableContractDetails) {
            this.disableControls([]);
        } else {
            this.disableControls(['ContractNumber', 'ContractName', 'contracttypecode']);
        }
    }

    ngAfterContentInit(): void {
        this.handleBackStack(this.pageId);

        if (this.isReturning()) {
            this.populateUIFromFormData();

            this.pageParams.gridCacheRefresh = !this.isReturning();
            this.fetchPremisesGridData(false, true);
        } else {
            this.pageParams.gridCurPage = 1;
            this.pageParams.totalRecords = 1;
            this.fetchPremisesGridData(true);
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /* building grid columns after fetching data from API */
    private buildPremisesGrid(riGridData: RenegsGridInterface[]): void {
        this.riGrid.Clear();
        const isRenegBoth: boolean = (riGridData['header']['cells'].length === 8) ? true : false;
        this.riGrid.AddColumn('riGridPremisesNumber', 'PremisesNumber', 'riGridPremisesNumber', MntConst.eTypeTextFree, 5);
        this.riGrid.AddColumn('riGridPremisesName', 'PremisesName', 'riGridPremisesName', MntConst.eTypeTextFree, 1);
        this.riGrid.AddColumn('riGridPremisesAddress', 'Address', 'riGridPremisesAddress', MntConst.eTypeTextFree, 1);
        this.riGrid.AddColumn('riGridPremisesPostCode', 'PostCode', 'riGridPremisesPostCode', MntConst.eTypeTextFree, 1);
        this.riGrid.AddColumn('riGridPremisesCovers', 'ServiceCovers', 'riGridPremisesCovers', MntConst.eTypeTextFree, 2);
        if (isRenegBoth) {
            this.riGrid.AddColumn('riGridPremisesKeep', 'Keep', 'riGridPremisesKeep', MntConst.eTypeImage, 2);
        }
        this.riGrid.AddColumn('riGridPremisesTransfer', 'Transfer', 'riGridPremisesCovers', MntConst.eTypeImage, 2);
        if (isRenegBoth) {
            this.riGrid.AddColumn('riGridPremisesDelete', 'Delete', 'riGridPremisesDelete', MntConst.eTypeImage, 2);
        }

        this.riGrid.AddColumnAlign('riGridPremisesName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridPremisesAddress', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridPremisesPostCode', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
        this.riGrid.Execute(riGridData);
    }

    private fetchContractdetails(): void {
        let data = [{
            'table': 'Contract',
            'query': { 'ContractNumber': this.riExchange.getParentHTMLValue('contractNumber') },
            'fields': ['ContractNumber', 'ContractName']
        }];

        this.setControlValue('AmendmentRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.lookUpRecord(data, 1)
            .subscribe((response) => {
                if (response && response['results'] && response['results'].length) {
                    this.setControlValue('ContractNumber', response['results'][0][0]['ContractNumber']);
                    this.setControlValue('ContractName', response['results'][0][0]['ContractName']);
                } else {
                    this.notifyOnAPIError(response);
                }
            }, (error) => {
                this.notifyOnAPIError(error);
            });
    }

    private fetchPremisesGridData(onGridRefresh?: boolean, onGridPaging?: boolean): void {
        let queryParams: QueryParams = new QueryParams();
        if (onGridRefresh) {
            this.fetchContractdetails();
        }
        queryParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
        queryParams.set('ContractCommenceDate', this.riExchange.getParentHTMLValue('ContractCommenceDate'));
        queryParams.set('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        queryParams.set('riAppName', 'riControlGrid');
        queryParams.set('riHTMLPage', 'Application/iCABSARenegPremiseGrid.htm');
        queryParams.set('UserCode', this.utils.getUserCode());
        queryParams.set(this.serviceConstants.Action, (onGridPaging) ? '2' : '0');
        queryParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.countryCode());
        queryParams.set(this.serviceConstants.PageSize, '10');
        queryParams.set(this.serviceConstants.GridPageCurrent, this.pageParams.gridCurPage.toString());
        this.riGrid.Clear();

        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, queryParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response && response['pageData']) {
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.UpdateHeader = true;
                    this.pageParams.gridCurPage = response['pageData'] ? response['pageData'].pageNumber : 1;
                    this.pageParams.totalRecords = response['pageData'] ? response['pageData'].lastPageNumber * this.actualPageSize : 1;
                    this.pageParams.isPaginationEnabled = (this.pageParams.totalRecords > 0) ? true : false;
                    this.buildPremisesGrid(response);

                } else {
                    this.notifyOnAPIError(response);
                }


            }, (error) => {
                this.notifyOnAPIError(error);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private lookUpRecord(data: Object, maxresults: number): any {
        let queryLookUp: QueryParams = new QueryParams();
        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.BusinessCode, this.businessCode());
        queryLookUp.set(this.serviceConstants.CountryCode, this.countryCode());
        if (maxresults) {
            queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(queryLookUp, data);
    }

    private notifyOnAPIError(error: any): void {
        this.messageType = ((error['hasError'] || error['errorMessage'])) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    public OnDblClickRiGridBody(data: any): void {
        const renegType: string = this.riExchange.getParentHTMLValue('renegType');

        const premiseNumber = (this.riGrid.Details.GetValue('riGridPremisesNumber').toLowerCase() === 'new') ? this.riGrid.Details.GetValue('riGridPremisesNumber', this.riGrid.CurrentRow - 1) : this.riGrid.Details.GetValue('riGridPremisesNumber', this.riGrid.CurrentRow);
        switch (this.riGrid.CurrentColumnName.toLowerCase()) {
            case 'rigridpremisescovers':
                this.navigate('serviceCoverGrid', RenegotiationRoutes.ICABSARENEGSSERVICECOVERGRIDFULLPATH, {
                    amendmentRef: this.getControlValue('AmendmentRef'),
                    contractNumber: this.getControlValue('ContractNumber'),
                    contractName: this.getControlValue('ContractName') || '',
                    premisesNumber: premiseNumber,
                    renegType: renegType
                });
                break;
            case 'rigridpremisesnumber':
                if (this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName).toLowerCase() === 'new') {//reneg/premisesmaintanance
                    this.navigate('renegspremisesmaintainance', RenegotiationRoutes.ICABSARENEGSPREMISESMAINTENANCENEWFULLPATH, {
                        PremiseNumber: this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName, this.riGrid.CurrentRow - 1),
                        advQuoteRef: this.getControlValue('AmendmentRef'),
                        renegType: renegType
                    });
                } else {  // to contractmanagement/premisesmaintenance
                    this.navigate('ServiceCover', RenegotiationRoutes.ICABSAPREMISESMAINTAINANCEPATH, {
                        advQuoteRef: this.getControlValue('AmendmentRef'),
                        PremiseNumber: premiseNumber,
                        renegType: renegType
                    });
                    break;
                }
        }
    }

    public onCellClick(data: any): void {

        let getPremiseNumber = this.riGrid.Details.GetAttribute('riGridPremisesNumber', 'additionalproperty');
        const currentColumn = this.riGrid.CurrentColumnName.toLowerCase();
        if (this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName) === 'bl') {
            return;
        } else {
            let queryParams: QueryParams = new QueryParams();
            queryParams.set('advQuoteRef', this.riExchange.getParentHTMLValue('advQuoteRef'));
            queryParams.set(this.serviceConstants.Action, '6');
            queryParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            queryParams.set(this.serviceConstants.CountryCode, this.countryCode());
            queryParams.set(this.serviceConstants.PremiseNumber, getPremiseNumber);

            if (currentColumn === 'rigridpremiseskeep') {
                queryParams.set(this.serviceConstants.Function, 'PremiseCheckBox');
                queryParams.set('FieldName', 'KeepInd');
                this.onGridActionButtonClick(queryParams);

            } else if (currentColumn === 'rigridpremisestransfer') {
                queryParams.set(this.serviceConstants.Function, 'PremiseCheckBox');
                queryParams.set('FieldName', 'TransferInd');
                this.onGridActionButtonClick(queryParams);

            } else if (currentColumn === 'rigridpremisesdelete') {
                queryParams.set(this.serviceConstants.Function, 'PremiseCheckBox');
                queryParams.set('FieldName', 'DeleteInd');
                this.onGridActionButtonClick(queryParams);
            }
        }
    }

    private onGridActionButtonClick(queryParams: QueryParams): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, queryParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.riGrid.Clear();
                this.fetchPremisesGridData();
            }, (error) => {
                this.notifyOnAPIError(error);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    // Get page specific grid data from service end
    public getCurrentPage(data: any): void {
        if (this.pageParams.gridCurPage === data.value) { return; }
        this.pageParams.gridCurPage = data.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.fetchPremisesGridData(false, true);
    }

}
