export interface RenegsAddNewRenegInterface {
    ContractCommenceDate: any;
    ContractName: string;
    ContractNumber: string;
    EffectiveDate: any;
    RenegType: string;
    advQuoteRef?: string;
    ContractAnnualValue?: string;
    CurrentPremises?: string;
    dlStatusCode?: string;
    ExistingAccountInd?: string;
    ExistingContractInd?: string;
    InvoiceAnnivDate?: any;
    InvoiceFrequencyCode?: string;
    NegBranchNumber?: string;
    OldContractCommenceDate?: any;
    RenegAccountName?: string;
    RenegAccountNumber?: string;
    ScannedContractName?: string;
    ScannedContractNumber?: string;
    SubScreenCodeList?: string;
    SubScreenDescList?: string;
    ToAccountName?: string;
    ToAccountNumber?: string;
}
