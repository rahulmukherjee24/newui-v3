export interface RenegsAddNewContractRenegInterface {
    advQuoteRef: string;
    AgreementNumber: string;
    BusinessCode: string;
    CompanyRegistrationNumber: string;
    CompanyVATNumber: string;
    CompanyVATNumber2: string;
    ContinuousInd: string;
    ContractContactDepartment: string;
    ContractContactEmail: string;
    ContractContactFax: string;
    ContractContactMobile: string;
    ContractContactName: string;
    ContractContactPosition: string;
    ContractContactTelephone: string;
    ContractDurationCode: string;
    ContractExpiryDate: string;
    ContractName: string;
    ContractNumber: string;
    InvoiceFrequencyCode: string;
    MinimumDurationCode: string;
    MinimumExpiryDate: string;
    PaymentTypeCode: string;
    RenewalAgreementNumber: string;
    UpdateableInd: string;
}
