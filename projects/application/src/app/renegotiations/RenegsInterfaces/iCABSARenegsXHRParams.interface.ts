export interface RenegsXHRParamsInterface {
    module: string;
    method: string;
    operation: string;
}
