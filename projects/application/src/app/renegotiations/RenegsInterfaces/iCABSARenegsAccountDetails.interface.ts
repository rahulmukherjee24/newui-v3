export interface RenegsAccountDetailsInterface {
    AccountAddressLine1: string;
    AccountAddressLine2: string;
    AccountAddressLine3?: string;
    AccountAddressLine4?: string;
    AccountAddressLine5: string;
    AccountContactDepartment?: string;
    AccountContactEmail?: string;
    AccountContactFax?: string;
    AccountContactMobile?: string;
    AccountContactName?: string;
    AccountContactPosition?: string;
    AccountContactTelephone?: string;
    AccountName: string;
    AccountNumber?: string;
    AccountPostcode?: string;
    advQuoteRef?: string;
    BusinessCode: string;
    groupaccountnumber?: string;
    UpdateableInd: string;
}
