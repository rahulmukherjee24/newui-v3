export interface RenegsPremisesMaintainanceInterface {
    advQuoteRef?: string;
    ClientReference: string;
    GPSCoordinateX: any;
    GPSCoordinateY: any;
    PremiseAddressLine1: string;
    PremiseAddressLine2: string;
    PremiseAddressLine3: string;
    PremiseAddressLine4: string;
    PremiseAddressLine5: string;
    PremiseContactDepartment: string;
    PremiseContactEmail?: string;
    PremiseContactFax: string;
    PremiseContactMobile: string;
    PremiseContactName: string;
    PremiseContactPosition: string;
    PremiseContactTelephone: string;
    PremiseName: string;
    PremisePostCode: string;
    PremiseSpecialInstructions: string;
    PremiseSRADate: string;
    PremiseSRAEmployee: string;
    PurchaseOrderNo: string;
    RoutingScore: string;
    RoutingSource: any;
}
