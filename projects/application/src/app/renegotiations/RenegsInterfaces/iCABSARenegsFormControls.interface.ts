export interface RenegsFormControlsInterface {
    disabled?: boolean;
    name: string;
    ignoreSubmit?: boolean;
    readonly?: boolean;
    required?: boolean;
    type?: string;
    value?: string;
}
