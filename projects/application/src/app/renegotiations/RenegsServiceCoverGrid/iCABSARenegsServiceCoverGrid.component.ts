import { AfterContentInit, Component, Injector, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';

import { MessageConstant } from './../../../shared/constants/message.constant';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { RenegsConstants } from '../iCABSARenegs.constant';
import { RenegsFormControlsInterface } from '../RenegsInterfaces/iCABSARenegsFormControls.interface';
import { RenegsGridInterface } from './../RenegsInterfaces/iCABSARenegsGrid.interface';
import { RenegsXHRParamsInterface } from './../RenegsInterfaces/iCABSARenegsXHRParams.interface';

@Component({
    selector: 'icabs-renegs-service-cover-grid',
    templateUrl: 'iCABSARenegsServiceCoverGrid.component.html'
})

export class RenegsServiceCoverGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('customAlert') public customAlert;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('Image') public image: ElementRef;

    private queryParams: QueryParams = this.getURLSearchParamObject();
    private xhrParams: RenegsXHRParamsInterface = {
        module: 'service-cover',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSARenegServiceCoverGrid'
    };

    public actualPageSize: number = 10;
    public controls: RenegsFormControlsInterface[] = [
        { name: 'AmendmentRef', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'PremisesNumber', type: MntConst.eTypeText, disabled: true }
    ];
    public customAlertMsg: any;
    public messageType: string;
    public messageConstant = MessageConstant.Message;
    public pageId: string;
    public pageTitle: string;
    public parentMode: string;
    public renegsConstants: RenegsConstants = RenegsConstants;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARENEGSERVICECOVERGRID;
        this.browserTitle = this.pageTitle = RenegsConstants.pageTitleServiceCoverGrid;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode() || 'serviceCoverGrid';
    }

    ngAfterContentInit(): void {
        this.setControlValue('AmendmentRef', this.riExchange.getParentHTMLValue('amendmentRef'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('contractName'));
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('contractNumber'));
        this.setControlValue('PremisesNumber', this.riExchange.getParentHTMLValue('premisesNumber') || '');
        this.pageParams.gridCurPage = 1;
        this.pageParams.totalRecords = 1;
        this.pageParams.isPaginationEnabled = false;
        this.fetchServiceCoverGridData();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /* building grid columns after fetching data from API */
    private buildRiGrid(riGridData: RenegsGridInterface[]): void {
        this.riGrid.Clear();

        const isRenegBoth: boolean = riGridData['header']['cells'].length === 8;

        this.riGrid.AddColumn('riGridPremisesNo', 'PremisesNumber', 'riGridPremisesNo', MntConst.eTypeTextFree, 3);
        this.riGrid.AddColumn('riGridProdCode', 'ProductCode', 'riGridProdCode', MntConst.eTypeCode, 9);
        this.riGrid.AddColumn('riGridProdName', 'ProuductDesc', 'riGridProdName', MntConst.eTypeTextFree, 14);
        this.riGrid.AddColumn('riGridAnnualValue', 'AnnualValue', 'riGridAnnualValue', MntConst.eTypeCurrency, 3);
        this.riGrid.AddColumn('riGridVisitFreq', 'VisitFrequency', 'riGridVisitFreq', MntConst.eTypeTextFree, 1);
        this.riGrid.AddColumn('riGridEntitlement', 'Entitlement', 'riGridEntitlement', MntConst.eTypeImage, 2);
        this.riGrid.AddColumn('riGridInstallRemove', 'InstallRemove', 'riGridInstallRemove', MntConst.eTypeImage, 2);
        if (!isRenegBoth) { this.riGrid.AddColumn('riGridKeep', 'Keep', 'riGridKeep', MntConst.eTypeImage, 2); }
        this.riGrid.AddColumn('riGridTransfer', 'Transfer', 'riGridTransfer', MntConst.eTypeImage, 2);
        if (!isRenegBoth) { this.riGrid.AddColumn('riGridDelete', 'Delete', 'riGridDelete', MntConst.eTypeImage, 2); }

        this.riGrid.AddColumnAlign('riGridPremisesNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridVisitFreq', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridAnnualValue', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('riGridProdCode', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    private fetchServiceCoverGridData(onGridPaging?: boolean): void {
        this.queryParams.set(this.serviceConstants.Action, '2');
        this.queryParams.set('advQuoteRef', this.getControlValue('AmendmentRef'));
        this.queryParams.set('riHTMLPage', this.xhrParams.operation + '.htm');
        this.queryParams.set('riAppName', 'riControlGrid');
        this.queryParams.set(this.serviceConstants.PageSize, '10');
        this.queryParams.set(this.serviceConstants.GridPageCurrent, this.pageParams.gridCurPage.toString());
        this.riGrid.Clear();

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, this.queryParams).subscribe((response) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (response.hasError || response.errorMessage || response.fullError) {
                this.pageParams.totalRecords = 0;
                this.pageParams.gridCurPage = 0;
                this.pageParams.isPaginationEnabled = false;
                this.notifyOnAPIError(response);

            } else {

                if (response && response['pageData'] || response['body']['cells'].length) {
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.UpdateHeader = true;
                    this.pageParams.gridCurPage = response['pageData'] ? response['pageData'].pageNumber : 1;
                    this.pageParams.totalRecords = response['pageData'] ? response['pageData'].lastPageNumber * this.actualPageSize : 1;
                    this.pageParams.isPaginationEnabled = (this.pageParams.totalRecords > 0) ? true : false;
                    this.buildRiGrid(response);
                    this.riGrid.Execute(response);
                } else {
                    this.notifyOnAPIError(response);
                }
            }

        }, (error) => {
            this.notifyOnAPIError(error);
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    public onGridActionButtonClick(searchParams: QueryParams): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams)
            .subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError || response.errorMessage || response.fullError) {
                    this.notifyOnAPIError(response);
                } else {
                    this.onRefreshRiGrid();
                }

            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.notifyOnAPIError(error);

                });
    }

    public onGridSort(): void { }

    private notifyOnAPIError(error: any): void {
        this.pageParams.totalRecords = 0;
        this.pageParams.isPaginationEnabled = false;
        this.pageParams.gridCurPage = 1;
        this.messageType = ((error['hasError'] || error['errorMessage'])) ? RenegsConstants.msgTypeError : RenegsConstants.msgTypeWarn;
        this.customAlertMsg = {
            msg: error.errorMessage + ' ' + error.fullError,
            timestamp: (new Date()).getMilliseconds()
        };
    }

    public OnDblClickRiGridBody(event: any): void {
        return;
    }

    public onCellClick(data: any): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set('advQuoteRef', this.getControlValue('AmendmentRef'));
        searchParams.set('ServiceCoverRowid', this.riGrid.Details.GetRowId(this.riGrid.CurrentColumnName));
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set(this.serviceConstants.Function, 'ServiceCoverCheckBox');
        searchParams.set(this.serviceConstants.PremiseNumber, this.riGrid.Details.GetAttribute('riGridPremisesNo', 'rowid'));
        const nonActionColumns: string[] = ['rigridentitlement', 'rigridprodcode', 'rigridprodname', 'rigridannualvalue', 'rigridvisitfreq', 'rigridpremisesno'];
        const currentColumnName: string = this.riGrid.CurrentColumnName.toLowerCase();

        if (this.riExchange.getParentHTMLValue('renegType') !== 'rngboth' && currentColumnName === 'rigridtransfer') {// transfer should be allow only for renegboth
            const msg: any = { 'errorMessage': '  Transfer is allowed ONLY in RenegBoth....', 'fullError': '' };
            this.notifyOnAPIError(msg);
            return;
        }

        if (nonActionColumns.indexOf(currentColumnName) >= 0 || this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName).toLowerCase() === 'bl') {
            return;

        } else {
            switch (currentColumnName) {
                case 'rigriddelete':
                    searchParams.set('FieldName', 'DeleteInd');
                    break;

                case 'rigridinstallremove':
                    searchParams.set('FieldName', 'InstallationRequired');
                    break;

                case 'rigridkeep':
                    searchParams.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
                    searchParams.set('FieldName', 'KeepInd');
                    break;

                case 'rigridtransfer':
                    searchParams.set('FieldName', 'TransferInd');
                    break;

                default:
                    break;
            }

            this.onGridActionButtonClick(searchParams);
        }
    }

    // Get page specific grid data from service end
    public getCurrentPage(data: any): void {
        this.pageParams.gridCurPage = data.value;
        this.riGrid.UpdateHeader = true;
        this.riGrid.UpdateRow = true;
        this.riGrid.UpdateFooter = true;
        this.fetchServiceCoverGridData(true);
    }

    // refresh the grid content
    public onRefreshRiGrid(): void {
        this.fetchServiceCoverGridData();
    }

}
