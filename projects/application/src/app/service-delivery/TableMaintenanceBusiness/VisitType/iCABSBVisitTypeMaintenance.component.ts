import { Component, OnInit, Injector, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';


import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { RouteAwayComponent } from '../../../../shared/components/route-away/route-away';
import { VisitTypeSearchComponent } from './../../../internal/search/iCABSBVisitTypeSearch.component';
import { InternalSearchModuleRoutes } from './../../../base/PageRoutes';


@Component({
    templateUrl: 'iCABSBVisitTypeMaintenance.html'
})

export class VisitTypeMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('visitTypeSearch') visitTypeSearch: VisitTypeSearchComponent;

    private queryPost: QueryParams = this.getURLSearchParamObject();
    private muleConfig = {
        method: 'service-delivery/admin',
        module: 'service',
        operation: 'Business/iCABSBVisitTypeMaintenance'
    };

    public pageId: string = '';
    public controls = [
        { name: 'VisitTypeCode', required: true, disabled: false, type: MntConst.eTypeCode },
        { name: 'VisitTypeDesc', required: true, disabled: false, type: MntConst.eTypeText },
        { name: 'WeightFactor', required: false, disabled: false, type: MntConst.eTypeDecimal2 },
        { name: 'PassToPDAInd', required: false, disabled: false },
        { name: 'OtherInd', required: false, disabled: false },
        { name: 'VisitTypeNarrative', required: false, disabled: false },
        { name: 'InspectionInd', required: false, disabled: false },
        { name: 'PestNetInd', required: false, disabled: false },
        { name: 'PestControlInd', required: false, disabled: false },
        { name: 'ProductSaleInd', required: false, disabled: false },
        { name: 'ActiveInd', required: false, disabled: false }
    ];
    public searchConfigs: any = {
        visitTypeSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                parentMode: 'Search'
            },
            active: {
                id: '',
                text: ''
            }
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVISITTYPEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Visit Type Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.searchConfigs.visitTypeSearch.active = {
                id: this.getControlValue('VisitTypeCode'),
                text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
            };
            this.setFormMode(this.c_s_MODE_UPDATE);
        } else {
            this.setFormMode(this.c_s_MODE_SELECT);
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Fetching Visit Type Data
    private fetchVisitTypeData(): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.formPristine();
            let queryGet: QueryParams = this.getURLSearchParamObject();
            queryGet.set(this.serviceConstants.Action, '0');
            queryGet.set(this.serviceConstants.BusinessCode, this.businessCode());
            queryGet.set(this.serviceConstants.VisitTypeCode, this.getControlValue('VisitTypeCode'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryGet)
                .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setFormMode(this.c_s_MODE_SELECT);
                        this.pageParams.mode = this.formMode;
                    } else {
                        this.setFormMode(this.c_s_MODE_UPDATE);
                        this.pageParams = data;
                        this.setControlValue('VisitTypeCode', data.VisitTypeCode);
                        this.setControlValue('VisitTypeDesc', data.VisitTypeDesc);
                        this.setControlValue('WeightFactor', data.WeightFactor);
                        this.setControlValue('PassToPDAInd', data.PassToPDAInd);
                        this.setControlValue('OtherInd', data.OtherInd);
                        this.setControlValue('VisitTypeNarrative', data.VisitTypeNarrative);
                        this.setControlValue('InspectionInd', data.InspectionInd);
                        this.setControlValue('PestNetInd', data.PestNetInd);
                        this.setControlValue('PestControlInd', data.PestControlInd);
                        this.setControlValue('ProductSaleInd', data.ProductSaleInd);
                        this.setControlValue('ActiveInd', data.ActiveInd);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    // Saving Visit Type Data
    private saveVisitType(): void {
        let formdata: any = {};
        if (this.formMode === this.c_s_MODE_ADD) {
            this.queryPost.set(this.serviceConstants.Action, '1');
            formdata = {
                VisitTypeCode: this.getControlValue('VisitTypeCode'),
                VisitTypeDesc: this.getControlValue('VisitTypeDesc'),
                WeightFactor: this.getControlValue('WeightFactor'),
                PassToPDAInd: this.getControlValue('PassToPDAInd'),
                VisitTypeNarrative: this.getControlValue('VisitTypeNarrative'),
                ActiveInd: this.getControlValue('ActiveInd'),
                OtherInd: this.getControlValue('OtherInd'),
                InspectionInd: this.getControlValue('InspectionInd'),
                PestNetInd: this.getControlValue('PestNetInd'),
                PestControlInd: this.getControlValue('PestControlInd'),
                ProductSaleInd: this.getControlValue('ProductSaleInd')
            };
        } else {
            this.queryPost.set(this.serviceConstants.Action, '2');
            formdata = {
                ROWID: this.pageParams.ttVisitType,
                VisitTypeDesc: this.getControlValue('VisitTypeDesc'),
                WeightFactor: this.getControlValue('WeightFactor'),
                PassToPDAInd: this.getControlValue('PassToPDAInd'),
                VisitTypeNarrative: this.getControlValue('VisitTypeNarrative'),
                ActiveInd: this.getControlValue('ActiveInd'),
                OtherInd: this.getControlValue('OtherInd'),
                InspectionInd: this.getControlValue('InspectionInd'),
                PestNetInd: this.getControlValue('PestNetInd'),
                PestControlInd: this.getControlValue('PestControlInd'),
                ProductSaleInd: this.getControlValue('ProductSaleInd')
            };
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    if (this.formMode === this.c_s_MODE_UPDATE) {
                        this.pageParams = data;
                        this.visitTypeSearch.triggerDataFetch(this.searchConfigs.visitTypeSearch.params);
                        this.searchConfigs.visitTypeSearch.active = {
                            id: this.getControlValue('VisitTypeCode'),
                            text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                        };
                    } else {
                        this.addNewVisitType();
                    }
                    this.formPristine();
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // Deleting Visit Type Data
    private deleteVisitType(): void {
        this.queryPost.set(this.serviceConstants.Action, '3');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            ROWID: this.pageParams.ttVisitType
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                    this.pageParams = {};
                    this.uiForm.reset();
                    this.searchConfigs.visitTypeSearch.active = {
                        id: '',
                        text: ''
                    };
                    this.visitTypeSearch.triggerDataFetch(this.searchConfigs.visitTypeSearch.params);
                    this.setFormMode(this.c_s_MODE_SELECT);
                    this.formPristine();
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // On init of dropdown, checking if Dropwdown have any data
    public initVisitTypeSearch(data: any): void {
        if (!this.isReturning() || this.formMode === this.c_s_MODE_SELECT) {
            if (data.totalRecords === 0) {
                this.addNewVisitType();
            } else {
                this.onVisitTypeReceived(data.firstRow);
            }
        }
    }

    // On receiving data from Dropdown
    public onVisitTypeReceived(data: any): void {
        if (data.VisitTypeCode) {
            this.setControlValue('VisitTypeCode', data.VisitTypeCode);
            this.setControlValue('VisitTypeDesc', data.VisitTypeDesc);
            this.searchConfigs.visitTypeSearch.active = {
                id: this.getControlValue('VisitTypeCode'),
                text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
            };
            this.fetchVisitTypeData();
        } else {
            this.searchConfigs.visitTypeSearch.active = {
                id: this.getControlValue('VisitTypeCode'),
                text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
            };
        }
    }

    // Add New Visit Type
    public addNewVisitType(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.uiForm.reset();
        this.riExchange.riInputElement.Enable(this.uiForm, 'VisitTypeDesc');
        this.pageParams = {};
    }

    public promptModalForSave(lookUp: boolean): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveVisitType.bind(this));
            this.modalAdvService.emitPrompt(modalVO);
        }
    }

    public promptModalForDelete(): void {
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteVisitType.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
    }

    // Cancel Button operation
    public cancelVisitType(): void {
        if (this.formMode !== this.c_s_MODE_SELECT) {
            if (this.formMode === this.c_s_MODE_ADD) {
                this.setFormMode(this.c_s_MODE_SELECT);
                this.uiForm.reset();
                this.searchConfigs.visitTypeSearch.active = {
                    id: '',
                    text: ''
                };
            } else {
                this.setFormMode(this.c_s_MODE_UPDATE);
                this.setControlValue('VisitTypeCode', this.pageParams.VisitTypeCode);
                this.setControlValue('VisitTypeDesc', this.pageParams.VisitTypeDesc);
                this.setControlValue('WeightFactor', this.pageParams.WeightFactor);
                this.setControlValue('PassToPDAInd', this.pageParams.PassToPDAInd);
                this.setControlValue('OtherInd', this.pageParams.OtherInd);
                this.setControlValue('VisitTypeNarrative', this.pageParams.VisitTypeNarrative);
                this.setControlValue('InspectionInd', this.pageParams.InspectionInd);
                this.setControlValue('PestNetInd', this.pageParams.PestNetInd);
                this.setControlValue('PestControlInd', this.pageParams.PestControlInd);
                this.setControlValue('ProductSaleInd', this.pageParams.ProductSaleInd);
                this.setControlValue('ActiveInd', this.pageParams.ActiveInd);
            }
        }
        this.formPristine();
    }

    // Option Dropdown selection
    public selectedOption(optionValue: string): void {
        switch (optionValue) {
            case 'VisitAction':
                this.navigate('VisitType', InternalSearchModuleRoutes.ICABSBVISITACTIONSEARCH, {
                    VisitTypeCode: this.getControlValue('VisitTypeCode'),
                    VisitTypeDesc: this.getControlValue('VisitTypeDesc'),
                    fromPageNav: true
                });
                break;
        }
    }
}
