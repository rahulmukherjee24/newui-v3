
export class LocaleCulture {
  public globalizeLocaleCode: string;
  public globalizeParserLocaleCode: string;
  public dateLocaleCode: string;
  constructor(
    public name: string,
    public localeKey: string,
    public language: string,
    public country: string,
    public languageCode: string,
    public countryCode: string,
    public localeCode: string,
    public iCABSLanguageCode: string,
    public currencyCode?: string) {
    this.globalizeLocaleCode = this.localeCode;
    this.globalizeParserLocaleCode = this.localeCode;
    this.dateLocaleCode = this.localeCode;
  }
}
