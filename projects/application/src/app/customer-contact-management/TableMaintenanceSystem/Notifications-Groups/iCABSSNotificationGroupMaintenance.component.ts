import { Component, OnInit, Injector, ViewChild, AfterContentInit, OnDestroy } from '@angular/core';

import { BaseComponent } from '@base/BaseComponent';
import { CommonDropdownComponent } from '@shared/components/common-dropdown/common-dropdown.component';
import { ErrorConstant } from '@shared/constants/error.constant';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalGridSearchApplicationModuleRoutes } from '@base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSNotificationGroupMaintenance.html'
})

export class NotificationGroupMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('notificationGroupSearch') notificationGroupSearch: CommonDropdownComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: Object = {
        operation: 'System/iCABSSNotificationGroupMaintenance',
        module: 'rules',
        method: 'ccm/admin'
    };

    public controls: Array<Object> = [
        { name: 'NotifyGroupCode', required: true, type: MntConst.eTypeCode },
        { name: 'NotifyGroupSystemDesc', required: true, type: MntConst.eTypeTextFree },
        { name: 'RowID' }
    ];
    public notificationGroupCode: any = {
        isRequired: true,
        isDisabled: false,
        isTriggerValidate: false,
        isFirstItemSelected: true,
        params: {
            operation: 'System/iCABSSNotificationGroupSearch',
            module: 'rules',
            method: 'ccm/search'
        },
        active: { id: '', text: '' },
        displayFields: ['NotifyGroupCode', 'NotifyGroupSystemDesc']
    };
    public pageId: string = '';

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNOTIFICATIONGROUPMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Notification Group Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.mode = 'update';
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.hideShowFields();
            this.notificationGroupCode.isFirstItemSelected = false;
            this.notificationGroupCode.active = {
                id: this.pageParams.notifyGroupCode,
                text: this.pageParams.notifyGroupCode + ' - ' + this.pageParams.notifyGroupSystemDesc
            };
            this.pageParams.mode = 'update';
            this.setControlValue('NotifyGroupCode', this.pageParams.notifyGroupCode);
            this.setControlValue('NotifyGroupSystemDesc', this.pageParams.notifyGroupSystemDesc);
            this.notificationGroupSearch.fetchData();
        } else {
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.pageParams.parentMode = this.riExchange.getParentMode();
        this.hideShowFields();
        if (this.pageParams.parentMode === 'BusinessTriggerDetail') {
            this.setControlValue('NotifyGroupCode', this.riExchange.getParentHTMLValue('IntNotifyGroupCode'));
            this.fetchNotifyGroupSystemDesc();
        }
    }

    //Fetch NotifyGroupSystemDesc
    private fetchNotifyGroupSystemDesc(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();

        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('NotifyGroupCode', this.pageParams.notifyGroupCode);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data && data.NotifyGroupCode) {
                        this.pageParams.mode = 'update';
                        this.setControlValue('NotifyGroupCode', data.NotifyGroupCode);
                        this.setControlValue('NotifyGroupSystemDesc', data.NotifyGroupSystemDesc);
                        this.setControlValue('RowID', data.ttNotificationGroup);
                        this.notificationGroupCode.active = {
                            id: this.getControlValue('NotifyGroupCode'),
                            text: this.getControlValue('NotifyGroupCode') + ' - ' + this.getControlValue('NotifyGroupSystemDesc')
                        };
                        this.pageParams.notifyGroupCode = this.getControlValue('NotifyGroupCode');
                        this.pageParams.notifyGroupSystemDesc = this.getControlValue('NotifyGroupSystemDesc');
                        this.pageParams.ttNotificationGroup = this.getControlValue('RowID');
                        if (this.getControlValue('NotifyGroupSystemDesc')) {
                            this.pageParams.isGroupDetailsButton = false;
                        } else {
                            this.pageParams.isGroupDetailsButton = true;
                        }
                        this.formPristine();
                    } else {
                        this.pageParams.mode = 'update';
                        this.setControlValue('NotifyGroupCode', '');
                        this.setControlValue('NotifyGroupSystemDesc', '');
                    }
                    this.hideShowFields();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            });
    }

    //Add Record
    private addRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '1');
        postParams['NotifyGroupCode'] = this.getControlValue('NotifyGroupCode');
        postParams['NotifyGroupSystemDesc'] = this.getControlValue('NotifyGroupSystemDesc');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                    this.notificationGroupCode.isFirstItemSelected = true;
                    this.pageParams.mode = 'add';
                    this.hideShowFields();
                    this.resetForm();
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            }
        );
    }

    //Update Record
    private updateRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '2');
        postParams['NotifyGroupSystemDesc'] = this.getControlValue('NotifyGroupSystemDesc');
        postParams['RowID'] = this.getControlValue('RowID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                    this.notificationGroupSearch.fetchData();
                    this.notificationGroupCode.active = {
                        id: this.getControlValue('NotifyGroupCode'),
                        text: this.getControlValue('NotifyGroupCode') + ' - ' + this.getControlValue('NotifyGroupSystemDesc')
                    };
                    this.hideShowFields();
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            }
        );
    }

    //Delete Record
    private deleteRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        searchParams.set(this.serviceConstants.Action, '3');
        postParams['RowID'] = this.getControlValue('RowID');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                    this.notificationGroupCode.isFirstItemSelected = true;
                    this.pageParams.mode = 'delete';
                    this.notificationGroupSearch.fetchData();
                    this.hideShowFields();
                    this.resetForm();
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError, error['fullError']));
            }
        );
    }

    private hideShowFields(): void {
        if (this.pageParams.mode === 'update' || this.pageParams.mode === 'delete') {
            this.pageParams.isGroupCodeDropdownHide = true;
            this.pageParams.isAddDeleteButton = true;
            this.pageParams.isGroupCodeAddHide = false;
            this.pageParams.isGroupDetailsButton = false;
        } else {
            this.pageParams.isGroupCodeDropdownHide = false;
            this.pageParams.isAddDeleteButton = false;
            this.pageParams.isGroupCodeAddHide = true;
            this.pageParams.isGroupDetailsButton = true;
        }
    }

    private resetForm(): void {
        this.clearControls([]);
        this.setControlValue('NotifyGroupCode', '');
        this.setControlValue('NotifyGroupSystemDesc', '');
    }

    //Button click action
    public actionButtonClicked(action: string): void {
        switch (action) {
            case 'add':
                this.pageParams.mode = 'add';
                this.hideShowFields();
                this.resetForm();
                break;
            case 'save':
                this.hideShowFields();
                if (this.riExchange.validateForm(this.uiForm)) {
                    if (this.pageParams.mode === 'add') {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, '', this.addRecord.bind(this)));
                    } else {
                        this.pageParams.mode = 'update';
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, '', this.updateRecord.bind(this)));
                    }
                }
                break;
            case 'cancel':
                this.hideShowFields();
                if (this.pageParams.mode === 'add') {
                    this.pageParams.mode = 'update';
                    this.notificationGroupCode.isFirstItemSelected = true;
                    this.notificationGroupSearch.fetchData();
                } else {
                    this.pageParams.mode = 'update';
                    this.fetchNotifyGroupSystemDesc();
                }
                this.formPristine();
                break;
            case 'delete':
                this.pageParams.mode = 'delete';
                this.hideShowFields();
                if (this.riExchange.validateForm(this.uiForm)) {
                    this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, '', this.deleteRecord.bind(this)));
                }
                break;
            case 'groupDetail':
                this.navigate('AddGroup', InternalGridSearchApplicationModuleRoutes.ICABSSNOTIFICATIONGROUPGRID);
                break;
        }
    }

    //GroupCode Dropdown
    public onNotificationGroupSearchDataDefaulted(obj: any): void {
        let notifyGroupCode: string, notifyGroupSystemDesc: string;

        if (obj && obj.hasOwnProperty('firstRow') && obj.firstRow.NotifyGroupCode) {
            notifyGroupCode = (obj.firstRow.NotifyGroupCode) ? obj.firstRow.NotifyGroupCode.toString() : '';
            notifyGroupSystemDesc = (obj.firstRow.NotifyGroupSystemDesc) ? obj.firstRow.NotifyGroupSystemDesc : '';
            if (this.getControlValue('NotifyGroupCode') === notifyGroupCode) return;
            this.setControlValue('NotifyGroupCode', notifyGroupCode);
            this.setControlValue('NotifyGroupSystemDesc', notifyGroupSystemDesc);
            this.notificationGroupCode.active = {
                id: this.getControlValue('NotifyGroupCode'),
                text: this.getControlValue('NotifyGroupCode') + ' - ' + this.getControlValue('NotifyGroupSystemDesc')
            };
            this.pageParams.notifyGroupCode = this.getControlValue('NotifyGroupCode');
            this.pageParams.notifyGroupSystemDesc = this.getControlValue('NotifyGroupSystemDesc');
            this.pageParams.mode = 'update';
            this.notificationGroupCode.isFirstItemSelected = true;
            this.fetchNotifyGroupSystemDesc();
        } else {
            this.pageParams.mode = 'add';
        }
    }

    public onSelectedValue(obj: any): void {
        let notifyGroupSystemDesc: string;
        if (obj && obj.NotifyGroupCode !== null && obj.NotifyGroupCode !== undefined) {
            notifyGroupSystemDesc = (obj.hasOwnProperty('NotifyGroupSystemDesc') ? obj.NotifyGroupSystemDesc : obj.Object.NotifyGroupSystemDesc);
            this.setControlValue('NotifyGroupCode', obj.NotifyGroupCode);
            this.setControlValue('NotifyGroupSystemDesc', notifyGroupSystemDesc);
            this.setControlValue('RowID', obj.ttNotificationGroup);
            this.pageParams.notifyGroupCode = this.getControlValue('NotifyGroupCode');
            this.pageParams.notifyGroupSystemDesc = this.getControlValue('NotifyGroupSystemDesc');
            this.pageParams.ttNotificationGroup = this.getControlValue('RowID');
            this.notificationGroupCode.isFirstItemSelected = false;
            this.fetchNotifyGroupSystemDesc();
        }
        this.uiForm.controls['NotifyGroupSystemDesc'].markAsPristine();
    }

    public onNotifyGroupSystemDescChange(event: any): void {
        if (event) {
            this.pageParams.isGroupDetailsButton = true;
        }
    }

}
