
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { CCMModuleRoutes, AppModuleRoutes } from './../../../base/PageRoutes';
import { CommonDropdownComponent } from './../../../../shared/components/common-dropdown/common-dropdown.component';
import { Component, OnInit, AfterContentInit, OnDestroy, ViewChild, Injector } from '@angular/core';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { QueryParametersCallback } from './../../../base/Callback';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSNotificationTemplateMaintenance.html'
})

export class SNotificationTemplateMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy, QueryParametersCallback {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('templateSearch') templateSearch: CommonDropdownComponent;

    public cachedData: any = {};
    public controls: Array<any> = [
        { name: 'NotifyTemplateCode', disabled: false, type: MntConst.eTypeCode, required: true, value: '' },
        { name: 'NotifyTemplateSystemDesc', disabled: false, type: MntConst.eTypeTextFree, required: true, value: '' },
        { name: 'NotifyMaxSMSLength', disabled: false, type: MntConst.eTypeInteger, required: false, value: '' },
        { name: 'NotifyTemplateType', disabled: false, type: MntConst.eTypeTextFree, required: false, value: '' }
    ];
    public dropDown: any = {
        templateSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            isFirstItemSelected: true,
            params: {
                operation: 'System/iCABSSNotificationTemplateSearch',
                module: 'notification',
                method: 'ccm/search'
            },
            active: { id: '', text: '' },
            displayFields: ['NotifyTemplateCode', 'NotifyTemplateSystemDesc']
        }
    };
    public isInit: boolean = false;
    public isUpdateEnabled: boolean = true;
    public pageId: string = '';
    public pageMode: string = '';
    public queryParams: any;
    public xhrParams: any = {
        module: 'notification',
        method: 'ccm/admin',
        operation: 'System/iCABSSNotificationTemplateMaintenance'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNOTIFICATIONTEMPLATEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Notification Template Maintenance';
        this.setURLQueryParameters(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.isInit = true;
        this.init();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public getURLQueryParameters(param: any): void {
        this.queryParams = param;
        if (this.isInit) { this.init(); }
    }

    public init(): void {
        this.uiForm.reset();
        this.pageParams.vBusinessCode = this.utils.getBusinessCode();
        this.pageParams.vCountryCode = this.utils.getCountryCode();
        this.parentMode = this.riExchange.getParentMode();
    }

    //Notification Template Dropdown - Start
    public onDataDefaulted(obj: any): void {
        if (obj && obj.hasOwnProperty('firstRow')) {
            this.pageMode = MntConst.eModeUpdate;
            if (this.queryParams.NotifyTemplateCode) {
                let arr: Array<any> = this.templateSearch.commonDropDown.inputArray;
                if (arr && arr.length > 0) {
                    let len: number = arr.length;
                    for (let i = 0; i < len; i++) {
                        if (arr[i].NotifyTemplateCode.indexOf(this.queryParams.NotifyTemplateCode) === 0) {
                            this.cachedData = arr[i];
                            break;
                        }
                    }
                    this.dropDown.templateSearch.active = { id: this.cachedData.NotifyTemplateCode, text: this.cachedData.NotifyTemplateCode + ' - ' + this.cachedData.NotifyTemplateSystemDesc };
                    this.onDataReceived(this.cachedData);
                }
            } else {
                let tempNotifyTemplateCode: any = (obj.firstRow.NotifyTemplateCode) ? obj.firstRow.NotifyTemplateCode.toString() : '';
                let tempNotifyTemplateSystemDesc: any = (obj.firstRow.NotifyTemplateSystemDesc) ? obj.firstRow.NotifyTemplateSystemDesc : '';
                let tempNotifyTemplateType: any = (obj.firstRow.NotifyTemplateType) ? obj.firstRow.NotifyTemplateType : '';
                let tempNotifyMaxSMSLength: any = (obj.firstRow.NotifyMaxSMSLength) ? obj.firstRow.NotifyMaxSMSLength : '';
                if (tempNotifyMaxSMSLength === '') { tempNotifyMaxSMSLength = 0; }
                if (this.getControlValue('NotifyTemplateCode') === tempNotifyTemplateCode) { return; }
                this.setControlValue('NotifyTemplateCode', tempNotifyTemplateCode);
                this.setControlValue('NotifyTemplateSystemDesc', tempNotifyTemplateSystemDesc);
                this.setControlValue('NotifyTemplateType', tempNotifyTemplateType);
                this.setControlValue('NotifyMaxSMSLength', tempNotifyMaxSMSLength);
                this.cachedData = {
                    NotifyTemplateCode: tempNotifyTemplateCode,
                    NotifyTemplateSystemDesc: tempNotifyTemplateSystemDesc,
                    NotifyTemplateType: tempNotifyTemplateType,
                    NotifyMaxSMSLength: tempNotifyMaxSMSLength
                };
            }
        }
    }

    public onDataReceived(obj: any): void {
        if (obj && obj.NotifyTemplateCode !== null && obj.NotifyTemplateCode !== undefined) {
            let tempNotifyTemplateCode: any = (obj.NotifyTemplateCode) ? obj.NotifyTemplateCode.toString() : '';
            let tempNotifyTemplateSystemDesc: any = (obj.NotifyTemplateSystemDesc) ? obj.NotifyTemplateSystemDesc : '';
            let tempNotifyTemplateType: any = (obj.NotifyTemplateType) ? obj.NotifyTemplateType : '';
            let tempNotifyMaxSMSLength: any = (obj.NotifyMaxSMSLength) ? obj.NotifyMaxSMSLength : '';
            if (tempNotifyMaxSMSLength === '') { tempNotifyMaxSMSLength = 0; }
            if (this.getControlValue('NotifyTemplateCode') === tempNotifyTemplateCode) { return; }
            this.setControlValue('NotifyTemplateCode', tempNotifyTemplateCode);
            this.setControlValue('NotifyTemplateSystemDesc', tempNotifyTemplateSystemDesc);
            this.setControlValue('NotifyTemplateType', tempNotifyTemplateType);
            this.setControlValue('NotifyMaxSMSLength', tempNotifyMaxSMSLength);
            this.pageMode = MntConst.eModeUpdate;
            this.cachedData = obj;
            this.formPristine();
        }
    }

    //Dropdown - End

    //Template Details button click
    public onClickDetail(): void {
        if (this.pageMode === MntConst.eModeUpdate) {

            let urlStrArr = this.location.path().split('?');
            let urlStr = urlStrArr[0];
            let qParams = 'NotifyTemplateCode=' + this.cachedData.NotifyTemplateCode;
            this.location.replaceState(urlStr, qParams);

            this.navigate('AddTemplate', AppModuleRoutes.CCM + CCMModuleRoutes.ICABSSNOTIFICATIONTEMPLATEGRID, {
                NotifyTemplateCode: this.getControlValue('NotifyTemplateCode'),
                NotifyTemplateSystemDesc: this.getControlValue('NotifyTemplateSystemDesc'),
                NotifyTemplateType: this.getControlValue('NotifyTemplateType')
            });
        }
    }

    //Save button click
    public save(): void {
        this.riExchange.validateForm(this.uiForm);

        for (let i in this.dropDown) {
            if (this.dropDown.hasOwnProperty(i)) {
                if (this.dropDown[i].hasOwnProperty('isRequired') && this.dropDown[i].isRequired) {
                    this.dropDown[i].isTriggerValidate = true;
                }
            }
        }

        if (this.uiForm.status === 'VALID') {
            if (this.pageMode === MntConst.eModeAdd) {
                this.add();
            } else {
                this.update();
            }
        } else {
            for (let control in this.uiForm.controls) {
                if (this.uiForm.controls[control].invalid) {
                    this.logger.log('   >> Invalid:', control, this.uiForm.controls[control].errors, this.uiForm.controls[control]);
                }
            }
        }
    }

    //Cancel button click
    public cancel(): void {
        if (this.pageMode === MntConst.eModeUpdate) {
            this.populateForm(this.cachedData);
        } else {
            this.setControlValue('NotifyTemplateCode', this.cachedData.NotifyTemplateCode);
            this.defaultToUpdateMode();
            this.populateForm(this.cachedData);
        }
        this.formPristine();
    }

    //Delete button click
    public delete(): void {
        function deleteCanceled(): void { this.pageMode = MntConst.eModeUpdate; }
        function deleteSuccess(): void {
            this.switchToAddMode();
            setTimeout(() => { this.defaultToUpdateMode(); }, 0);
        }
        function deleteXHR(): void {
            if (this.pageMode === MntConst.eModeDelete) {
                this.ajaxSource.next(this.ajaxconstant.START);
                let search: QueryParams = new QueryParams();
                search.set(this.serviceConstants.Action, '3');
                search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
                search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

                let formData: any = {};
                formData['NotifyTemplateCode'] = this.getControlValue('NotifyTemplateCode');

                this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.formPristine();
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        let tempModal: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully);
                        tempModal.closeCallback = deleteSuccess.bind(this);
                        this.modalAdvService.emitMessage(tempModal);
                    }
                });
            }
        }
        this.pageMode = MntConst.eModeDelete;
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, deleteXHR.bind(this), deleteCanceled.bind(this)));
    }

    //Function to check form dirty state on Add button click
    public checkFormDirty(): void {
        if (!this.uiForm.dirty) {
            this.switchToAddMode();
        } else {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.RouteAway, null, this.switchToAddMode.bind(this), null));
        }
    }

    //Function to create XHR object
    public createXHRObj(action: number): any {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, action.toString());
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        let len = this.controls.length;
        for (let i = 0; i < len; i++) {
            let ctrlName: string = this.controls[i].name;
            let ctrlValue: any = this.getControlValue(this.controls[i].name, true);
            if (this.controls[i].type === MntConst.eTypeCheckBox) {
                ctrlValue = this.utils.convertCheckboxValueToRequestValue(ctrlValue);
            }
            formData[ctrlName] = ctrlValue;
        }
        this.cachedData = formData;
        return { search: search, formData: formData };
    }

    //Generic function to handle the XHR request for Add & Update mode
    public doXHR(): void {
        let action: number = 2;
        if (this.pageMode === MntConst.eModeAdd) { action = 1; }
        this.ajaxSource.next(this.ajaxconstant.START);
        let obj: any = this.createXHRObj(action);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, obj.search, obj.formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.formPristine();
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                let index: number = -1;
                if (this.pageMode === MntConst.eModeUpdate) {
                    let deletedTemplate: any = this.getControlValue('NotifyTemplateCode');
                    let len: number = this.templateSearch.requestdata.length;
                    for (let i = 0; i < len; i++) {
                        if (this.templateSearch.requestdata[i].NotifyTemplateCode === deletedTemplate) {
                            index = i;
                            break;
                        }
                    }
                }
                for (let i in data) {
                    if (data.hasOwnProperty(i)) {
                        this.setControlValue(i, data[i]);
                        if (this.pageMode === MntConst.eModeUpdate) {
                            this.templateSearch.requestdata[index][i] = data[i];
                            this.templateSearch.ngAfterViewInit(index);
                        }
                    }
                }
                let tempModal: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.SavedSuccessfully);
                tempModal.closeCallback = this.defaultToUpdateMode.bind(this);
                if (this.pageMode === MntConst.eModeAdd) { tempModal.closeCallback = this.switchToAddMode.bind(this); }
                this.modalAdvService.emitMessage(tempModal);
            }
        });
    }

    //Handle Add mode click - called from save()
    public add(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.doXHR.bind(this), null));
    }

    //Handle Update button click - called from save()
    public update(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.doXHR.bind(this), null));
    }

    //Function to default to Add mode
    public switchToAddMode(): void {
        this.uiForm.reset();
        this.parentMode = 'ADD';
        this.pageMode = MntConst.eModeAdd;
        this.setControlValue('NotifyTemplateCode', '');
        this.setControlValue('NotifyTemplateType', '1');
    }

    //Function to default to Update mode
    public defaultToUpdateMode(): void {
        this.pageMode = MntConst.eModeUpdate;
        this.parentMode = 'UPDATE';
    }

    //Generic function to populate the form from XHR data or cached data
    public populateForm(data: any): void {
        for (let i in data) {
            if (data.hasOwnProperty(i)) {
                this.setControlValue(i, data[i]);
            }
        }
    }
}
