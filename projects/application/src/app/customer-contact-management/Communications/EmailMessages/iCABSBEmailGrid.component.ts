import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { BaseComponent } from '@base/BaseComponent';
import { EllipsisComponent } from '@shared/components/ellipsis/ellipsis';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { GridComponent } from '@shared/components/grid/grid';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBEmailGrid.html'
})

export class EmailGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('emailEllipsis') public emailEllipsis: EllipsisComponent;
    @ViewChild('emailGrid') emailGrid: GridComponent;
    @ViewChild('emailPagination') emailPagination: PaginationComponent;
    @ViewChild('messageModal') public messageModal;

    public controls = [
        { name: 'DateFrom', readonly: false, disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', readonly: false, disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', readonly: true, disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'selectDestination', readonly: false, disabled: false, required: true, value: 'I' },
        { name: 'selectStatus', readonly: false, disabled: false, required: true, value: 'Created' }
    ];
    public currentPage: number = 1;
    public DateFrom: Date;
    public dateObjects: any = {};
    public DateTo: Date = new Date();
    public employeeSearchComponent = EmployeeSearchComponent;
    public ellipsisQueryParams: any = {
        inputParamsemployee: {
            parentMode: 'LookUp'
        }
    };
    public errorMessage: string;
    public fromDateDisplay;
    public gridSearch: QueryParams = new QueryParams();
    public gridTotalItems: number;
    public inputParams: any = {};
    public isFormValid: boolean = false;
    public itemsPerPage: number = 11;
    public maxColumn: number = 5;
    public modalConfig: Object;
    public pageId: string = '';
    public pageTitle: string = '';
    public queryParams: any = {
        operation: 'Business/iCABSBEmailGrid',
        module: 'notification',
        method: 'it-functions/maintenance'
    };
    public search: QueryParams = new QueryParams();
    public serviceCoverRowID: string = '';
    public showErrorHeader: boolean = true;
    public showHeader = true;
    public toDateDisplay;
    public totalItems: number = 10;
    public uiForm: FormGroup;
    public validateProperties: Array<any> = [];
    public visiblePagination: Boolean;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBEMAILGRID;
        this.browserTitle = 'Email Notification Message Grid';
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.visiblePagination = true;
        this.pageTitle = 'Email Notification Message Grid';
        let getToDate = this.globalize.parseDateToFixedFormat(new Date()).toString();
        this.toDateDisplay = this.globalize.parseDateStringToDate(getToDate);
        this.DateTo = this.toDateDisplay;
        let getFromDate = this.globalize.parseDateToFixedFormat(this.utils.removeDays(new Date(), 28)).toString();
        this.fromDateDisplay = this.globalize.parseDateStringToDate(getFromDate);
        this.DateFrom = this.fromDateDisplay;
        this.buildGridInit();
    }

    public buildGridInit(): void {
        this.validateProperties = [
            {
                'type': MntConst.eTypeText,
                'index': 0,
                'align': 'center'
            },
            {
                'type': MntConst.eTypeText,
                'index': 1,
                'align': 'center'
            },
            {
                'type': MntConst.eTypeText,
                'index': 2,
                'align': 'center'
            },
            {
                'type': MntConst.eTypeText,
                'index': 3
            },
            {
                'type': MntConst.eTypeText,
                'index': 4,
                'align': 'center'
            },
            {
                'type': MntConst.eTypeText,
                'index': 6,
                'align': 'center'
            }
        ];
        this.buildGrid();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onEmployeeDataReceived(data: any): void {
        this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeCode', data['EmployeeCode']);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeSurname', data['EmployeeSurname']);
        this.setFormMode(this.c_s_MODE_UPDATE);
    }

    public fetchAPIDetails(event: any): void {
        if (!this.getControlValue('EmployeeCode')) {
            this.setControlValue('EmployeeSurname', '');
            this.setFormMode(this.c_s_MODE_SELECT);
        }
        else {
            this.search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            this.search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            this.search.set(this.serviceConstants.Action, '6');
            this.search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
            this.search.set('Function', 'GetEmployeeName');
            this.queryParams.search = this.search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, this.search)
                .subscribe(
                    (e) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                            this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeSurname', '');
                        } else {
                            this.messageService.emitMessage(e);
                            this.riExchange.riInputElement.SetValue(this.uiForm, 'EmployeeSurname', e.EmployeeSurname);
                            if (this.getControlValue('EmployeeCode'))
                                this.setFormMode(this.c_s_MODE_UPDATE);
                        }
                    },
                    (error) => {
                        this.errorMessage = error as any;
                        this.errorService.emitError(error);
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    }
                );
        }
    }

    public dateFromSelectedValue(value: any): void {
        if (value && value.value) {
            this.fromDateDisplay = value.value;
        }
        else {
            this.fromDateDisplay = '';
        }
    }

    public dateToSelectedValue(value: any): void {
        if (value && value.value) {
            this.toDateDisplay = value.value;
        }
        else {
            this.toDateDisplay = '';
        }
    }

    public buildGrid(): void {
        this.inputParams.module = this.queryParams.module;
        this.inputParams.method = this.queryParams.method;
        this.inputParams.operation = this.queryParams.operation;
        this.gridSearch.set(this.serviceConstants.Action, '2');
        this.gridSearch.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.gridSearch.set(this.serviceConstants.CountryCode, this.countryCode());
        this.gridSearch.set('BranchNumber', '3');
        this.gridSearch.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        this.gridSearch.set('Status', this.getControlValue('selectStatus'));
        this.gridSearch.set('DateFrom', this.globalize.parseDateToFixedFormat(this.fromDateDisplay).toString());
        this.gridSearch.set('DateTo', this.globalize.parseDateToFixedFormat(this.toDateDisplay).toString());
        this.gridSearch.set('EmailDestinationType', this.getControlValue('selectDestination'));
        this.gridSearch.set('riGridMode', '0');
        this.gridSearch.set('riGridHandle', '30606786');
        this.gridSearch.set('riCacheRefresh', 'True');
        this.gridSearch.set('PageSize', this.itemsPerPage.toString());
        this.gridSearch.set('PageCurrent', this.currentPage.toString());
        this.gridSearch.set('riSortOrder', 'Descending');
        this.gridSearch.set('HeaderClickedColumn', '');
        this.inputParams.search = this.gridSearch;
        this.emailGrid.loadGridData(this.inputParams);
    }

    public getGridInfo(value: any): void {
        if (value.totalRows !== undefined) {
            if (value.totalRows === 0)
                this.visiblePagination = false;
            else
                this.visiblePagination = true;
            this.emailPagination.totalItems = value.totalRows;
        }
    }

    public getCurrentPage(data: any): void {
        this.currentPage = data.value;
        this.buildGrid();
    }

    public refresh(): void {
        this.currentPage = 1;
        if (this.utils.convertDate(this.fromDateDisplay).getTime() > this.utils.convertDate(this.toDateDisplay).getTime()) {
            this.messageModal.show({ msg: this.fromDateDisplay + '>' + this.toDateDisplay }, false);
        }
        else {
            this.buildGrid();
        }
    }
}
