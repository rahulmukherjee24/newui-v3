import * as moment from 'moment';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { Component, OnInit, AfterContentInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { Location } from '@angular/common';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSCMContactRedirectionMaintenance.html',
    providers: [CommonLookUpUtilsService]
})

export class ContactRedirectionMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('promptModal') public promptModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    protected controls: IControls[] = [
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', required: true, type: MntConst.eTypeCode },
        { name: 'EmployeeCodeTo', required: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'EmployeeSurnameTo', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'RedirectEmployeeInd', value: true, type: MntConst.eTypeCheckBox },
        { name: 'RedirectMessagingInd', value: true, type: MntConst.eTypeCheckBox }
    ];

    public pageId: string;
    public pageMode: string = MntConst.eModeUpdate;
    public pageTitle: string;
    public promptConfirmContent: string;

    private redirtectionData: Object;
    private rowID: string;

    public headerParams: any = {
        method: 'ccm/maintenance',
        operation: 'ContactManagement/iCABSCMContactRedirectionMaintenance',
        module: 'ContactRedirection'
    };

    //Ellipsis Config
    public ellipsis = {
        employeeEllipsis: {
            childparams: {
                'parentMode': 'LookUp'
            },
            childparamsEmpTo: {
                'parentMode': 'LookUpTo'
            },
            component: EmployeeSearchComponent
        }
    };

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService, private location: Location, private modalAdvService: ModalAdvService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMCONTACTREDIRECTIONMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Contact Redirection Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        this.rowID = this.riExchange.getParentHTMLValue('EmployeeCodeContactRowID');

        if (this.rowID) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 0);
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            let postObj: Object = {};

            postObj['ROWID'] = this.rowID;
            postObj['table'] = 'ContactRedirection';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, postObj).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.errorMessage) {
                        this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    } else {
                        this.redirtectionData = data;
                        this.setData();
                        this.controlChange(true);
                    }
                });
        } else {
            this.pageMode = MntConst.eModeAdd;
        }
    }


    private setData(): void {
        this.controls.forEach(control => {
            this.setControlValue(control.name, this.redirtectionData[control.name]);
        });
        this.onEmployeeChange('FromEmp');
        this.onEmployeeChange('DelegateEmp');
    }

    public onEmployeeDataReceived(data: any, control: string): void {
        if (data) {
            if (control === 'FromEmp') {
                this.setControlValue('EmployeeCode', data.EmployeeCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
            } else {
                this.setControlValue('EmployeeCodeTo', data.EmployeeCodeTo);
                this.setControlValue('EmployeeSurnameTo', data.EmployeeSurnameTo);
            }
        }
    }

    public onEmployeeChange(control: string): void {
        let codeSuff = control === 'DelegateEmp' ? 'To' : '';
        this.setControlValue('EmployeeSurname' + codeSuff, '');
        this.commonLookup.getEmployeeSurname(this.getControlValue('EmployeeCode' + codeSuff)).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('EmployeeSurname' + codeSuff, data[0][0].EmployeeSurname);
            } else {
                this.setControlValue('EmployeeSurname' + codeSuff, '');
                this.setControlValue('EmployeeCode' + codeSuff, '');
            }
        });
    }

    public onBtnClicked(action: string): void {
        switch (action) {
            case 'Save':
                if (this.riExchange.validateForm(this.uiForm)) {
                    this.promptModal.show();
                    this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                }
                break;
            case 'Delete':
                this.pageMode = MntConst.eModeDelete;
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteRecord.bind(this), this.deleteCanceled.bind(this)));
                this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
                break;
            case 'Add':
                this.resetData();
                this.pageMode = MntConst.eModeAdd;
                this.controlChange(false);
                break;
            case 'Cancel':
                if (this.pageMode === MntConst.eModeUpdate) {
                    this.setData();
                } else if (this.pageMode === MntConst.eModeAdd) {
                    if (this.parentMode === 'AddNew') {
                        event.preventDefault();
                        this.location.back();
                    } else {
                        this.setData();
                        this.controlChange(true);
                        this.pageMode = MntConst.eModeUpdate;
                    }
                }
                break;
        }
    }

    public resetData(): void {
        this.uiForm.reset();
        this.setControlValue('RedirectMessagingInd', true);
        this.setControlValue('RedirectEmployeeInd', true);
    }

    public confirmed(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.deleteRecord();
        }
        else {
            this.validateRedirection();
        }
    }

    private deleteCanceled(): void {
        this.pageMode = MntConst.eModeUpdate;
    }

    private controlChange(status: boolean): void {
        this.disableControl('EmployeeCode', status);
        this.disableControl('DateFrom', status);
    }

    private validateRedirection(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 6);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        let postObj: Object = {};

        postObj[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
        postObj['EmployeeCodeTo'] = this.getControlValue('EmployeeCodeTo');
        postObj[this.serviceConstants.DateFrom] = this.utils.formatDate(this.getControlValue('DateFrom'));
        postObj[this.serviceConstants.DateTo] = this.utils.formatDate(this.getControlValue('DateTo'));
        postObj['table'] = 'ContactRedirection';
        postObj[this.serviceConstants.Function] = 'ValidateRedirection';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, postObj).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.ErrorMessage) {
                    this.displayMessage(data.ErrorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    this.saveRecord();
                }
            });
    }

    private saveRecord(): void {
        let dt = new Date();
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : 2);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        let postObj: Object = {};

        if (this.pageMode === MntConst.eModeUpdate) {
            postObj['ROWID'] = this.rowID;
        }
        postObj['EmployeeCode'] = this.getControlValue('EmployeeCode');
        postObj['EmployeeCodeTo'] = this.getControlValue('EmployeeCodeTo');
        postObj['DateFrom'] = this.utils.formatDate(this.getControlValue('DateFrom'));
        postObj['DateTo'] = this.utils.formatDate(this.getControlValue('DateTo'));
        postObj['RedirectMessagingInd'] = this.getControlValue('RedirectMessagingInd') ? true : false;
        postObj['RedirectEmployeeInd'] = this.getControlValue('RedirectEmployeeInd') ? true : false;
        postObj['CreatedByUserCode'] = this.utils.getUserCode();
        postObj['DateCreated'] = this.utils.formatDate(dt);
        postObj['TimeCreated'] = this.globalize.parseTimeToFixedFormat(moment(dt).format('HH:mm'));
        postObj['table'] = 'ContactRedirection';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, postObj).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    this.redirtectionData = data;
                    this.rowID = data.ttContactRedirection;
                    this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.pageMode = MntConst.eModeUpdate;
                    this.parentMode = '';
                    this.controlChange(true);
                    this.formPristine();
                }
            });
    }

    private deleteRecord(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 3);
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        let postObj: Object = {};

        postObj['table'] = 'ContactRedirection';
        postObj['EmployeeCode'] = this.getControlValue('EmployeeCode');
        postObj['DateFrom'] = this.getControlValue('DateFrom');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, postObj).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage(data.errorMessage, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                } else {
                    event.preventDefault();
                    this.location.back();
                }
            });
    }
}
