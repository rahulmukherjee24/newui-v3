import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { BulkSMSMessageMaintenanceComponent } from './Communications/iCABSBulkSMSMessageMaintenance';
import { CallCenterGridAccountsComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridAccounts.component';
import { CallCenterGridCallLogsComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridCallLogs.component';
import { CallCenterGridContractsComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridContracts.component';
import { CallCenterGridDashboardComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridDashboard.component';
import { CallCenterGriddlContractComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGriddlContract.component';
import { CallCenterGridEventHistoryComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridEventHistory.component';
import { CallCenterGridHistoryComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridHistory.component';
import { CallCenterGridInvoicesComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridInvoices.component';
import { CallCenterGridPremisesComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridPremises.component';
import { CallCenterGridWorkOrdersComponent } from './CustomerContact/CallCenterGridComponents/iCABSCMCallCentreGridWorkOrders.component';
import { CallCentreGridComponent } from './CustomerContact/iCABSCMCallCentreGrid.component';
import { CallCentreGridNewContactComponent } from './CallCentreGridNewContact/iCABSCMCallCentreGridNewContact';
import { CCMRootComponent } from './ccm.component';
import { CCMRouteDefinitions } from './ccm.route';
import { CentreReviewGridComponent } from './CustomerContact/iCABSCMCallCentreReviewGrid';
import { CMCallAnalysisGridComponent } from './ReportsContact/CallAnalysis/iCABSCMCallAnalysisGrid.component';
import { CMCallCentreAssignGridComponent } from './CustomerContact/ContactCentreAssign/iCABSCMCallCentreAssignGrid.component';
import { ContactActionMaintenanceComponent } from './CustomerContact/iCABSCMContactActionMaintenance.component';
import { ContactMediumGridComponent } from './CustomerContact/iCABSContactMediumGrid.Component';
import { ContactRedirectionComponent } from './Communications/iCABSCMContactRedirection.component';
import { ContactRedirectionMaintenanceComponent } from './Communications/iCABSCMContactRedirectionMaintenance.component';
import { ContactSearchGridComponent } from './CustomerContact/iCABSCMCustomerContactSearchGrid';
import { ContactTypeMaintenanceComponent } from './TableMaintenanceSystem/ContactType/iCABSSContactTypeMaintenance.component';
import { CustomerContactCalloutGridComponent } from './CustomerContact/CalloutSearch/iCABSCMCustomerContactCalloutGrid';
import { EmailGridComponent } from './Communications/EmailMessages/iCABSBEmailGrid.component';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { NotificationGroupMaintenanceComponent } from './TableMaintenanceSystem/Notifications-Groups/iCABSSNotificationGroupMaintenance.component';
import { NotificationTemplateGridComponent } from '@internal/grid-search/iCABSSNotificationTemplateGrid.component';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';
import { SMSMessagesGridComponent } from './Communications/SMSMessages/iCABSCMSMSMessagesGrid.component';
import { SNotificationTemplateMaintenanceComponent } from './TableMaintenanceBusiness/LostBusinessDetail/iCABSSNotificationTemplateMaintenance.component';
import { TeleSalesOrderGridComponent } from './Telesales/iCABSATeleSalesOrderGrid';
import { WorkorderReviewGridComponent } from './CustomerContact/WorkOrderReview/iCABSCMWorkorderReviewGrid.component';

@NgModule({
    imports: [
        CCMRouteDefinitions,
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule
    ],
    declarations: [
        BulkSMSMessageMaintenanceComponent,
        CallCenterGridAccountsComponent,
        CallCenterGridCallLogsComponent,
        CallCenterGridContractsComponent,
        CallCenterGridDashboardComponent,
        CallCenterGriddlContractComponent,
        CallCenterGridEventHistoryComponent,
        CallCenterGridHistoryComponent,
        CallCenterGridInvoicesComponent,
        CallCenterGridPremisesComponent,
        CallCenterGridWorkOrdersComponent,
        CallCentreGridComponent,
        CallCentreGridNewContactComponent,
        CCMRootComponent,
        CentreReviewGridComponent,
        CMCallAnalysisGridComponent,
        CMCallCentreAssignGridComponent,
        ContactActionMaintenanceComponent,
        ContactMediumGridComponent,
        ContactRedirectionComponent,
        ContactRedirectionMaintenanceComponent,
        ContactSearchGridComponent,
        ContactTypeMaintenanceComponent,
        CustomerContactCalloutGridComponent,
        EmailGridComponent,
        NotificationGroupMaintenanceComponent,
        NotificationTemplateGridComponent,
        SMSMessagesGridComponent,
        SNotificationTemplateMaintenanceComponent,
        TeleSalesOrderGridComponent,
        WorkorderReviewGridComponent
    ],
    entryComponents: [
        CallCenterGridAccountsComponent,
        CallCenterGridCallLogsComponent,
        CallCenterGridContractsComponent,
        CallCenterGridDashboardComponent,
        CallCenterGriddlContractComponent,
        CallCenterGridEventHistoryComponent,
        CallCenterGridHistoryComponent,
        CallCenterGridInvoicesComponent,
        CallCenterGridPremisesComponent,
        CallCenterGridWorkOrdersComponent,
        CallCentreGridNewContactComponent,
        CMCallAnalysisGridComponent,
        CMCallCentreAssignGridComponent,
        ContactMediumGridComponent,
        ContactRedirectionComponent,
        ContactRedirectionMaintenanceComponent,
        CustomerContactCalloutGridComponent,
        EmailGridComponent,
        NotificationGroupMaintenanceComponent,
        NotificationTemplateGridComponent,
        SMSMessagesGridComponent,
        SNotificationTemplateMaintenanceComponent,
        TeleSalesOrderGridComponent
    ]
})

export class CCMModule { }
