
export class SmallDeviceUtils {
    public static readonly defaultTxtLimit: number = 50;

    public static ellipsesText(txt: string, txtLimit?: number): any {
        return txt.length > (txtLimit || this.defaultTxtLimit) ? (txt.substring(0, txtLimit || this.defaultTxtLimit)) + '...' : txt;
    }
}
