import { OnInit, Injector, Component, ViewChild } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { IControls } from '@app/base/ControlsType';
import {
    IGenericEllipsisControl,
    EllipsisGenericComponent,
    GenericEllipsisEvent
} from '@shared/components/ellipsis-generic/ellipsis-generic';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';

@Component({
    templateUrl: 'iCABSBVehicleManufacturerModelMaintenance.html'
})

export class VehicleManufacturerModelMaintenanceComponent extends LightBaseComponent implements OnInit {

    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;
    @ViewChild('vehicleManufacturerModelEllipsis') vehicleManufacturerModelEllipsis: EllipsisGenericComponent;

    public controls: IControls[] = [
        { name: 'Manufacturer', required: true, type: MntConst.eTypeText },
        { name: 'Model', required: true, type: MntConst.eTypeText }
    ];

    public pageId: string;
    public isDisabled: boolean = true;
    public pageMode: string = MntConst.eModeSelect;

    private headerParams: IXHRParams = {
        operation: 'Business/iCABSBVehicleManufacturerModelMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };

    public MntConst = MntConst;
    public vehicleManufacturerModelEllipsisConfig: IGenericEllipsisControl = {
        autoOpen: true,
        ellipsisTitle: 'Vehicle Manufacture Model Search',
        configParams: {
            table: 'VehicleManufacturerModel',
            shouldShowAdd: true
        },
        tableColumn: [
            { title: 'Manufacturer', name: 'Manufacturer', type: MntConst.eTypeText },
            { title: 'Model', name: 'Model', type: MntConst.eTypeText }
        ],
        disable: false
    };
    private tempVehicleManufacturerModelData: Record<string, string>;

    constructor(injector: Injector, private modalAdvService: ModalAdvService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVEHICLEMANUFACTURERMODELMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Vehicle Manufacturer Model Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    private getCommonFormData(): Record<string, string> {
        return {
            table: 'VehicleManufacturerModel',
            [this.serviceConstants.MethodType]: 'maintenance',
            UserCode: this.utils.getUserCode(),
            Manufacturer: this.getControlValue('Manufacturer'),
            Model: this.getControlValue('Model')
        };
    }

    public onInputChange(): void {
        if (
            this.pageMode !== MntConst.eModeAdd &&
            this.getControlValue('Manufacturer') &&
            this.getControlValue('Model')
        ) {
            this.commonServiceCall(0, this.getCommonFormData(), this.fetchVehicleManufacturerModelRecord.bind(this));
        }
    }

    public onBtnClick(action: string): void {
        switch (action) {
            case 'Save':
                if (this.riExchange.validateForm(this.uiForm)) {
                    this.modalAdvService.emitPrompt(
                        new ICabsModalVO(
                            MessageConstant.Message.ConfirmRecord, null,
                            () => { this.onBtnClick('PromptConfirm'); },
                            () => { this.onBtnClick('PromptCancel'); }
                        )
                    );
                }
                break;
            case 'Cancel':
                if (this.pageMode === MntConst.eModeAdd) {
                    this.enablePageSelectMode();
                } else {
                    this.setFormControlValues(this.tempVehicleManufacturerModelData);
                }
                break;
            case 'Delete':
                this.pageMode = MntConst.eModeDelete;
                this.modalAdvService.emitPrompt(
                    new ICabsModalVO(
                        MessageConstant.Message.DeleteRecord, null,
                        () => { this.onBtnClick('PromptConfirm'); },
                        () => { this.onBtnClick('PromptCancel'); }
                    )
                );
                break;
            case 'PromptCancel':
                if (this.pageMode === MntConst.eModeDelete) {
                    this.pageMode = MntConst.eModeUpdate;
                }
                break;
            case 'PromptConfirm':
                if (this.pageMode === MntConst.eModeDelete) {
                    this.commonServiceCall(3, this.getCommonFormData(), this.deleteVehicleManufacturerModelRecord.bind(this));
                } else {
                    this.commonServiceCall(1, this.getCommonFormData(), this.saveVehicleManufacturerModelRecord.bind(this));
                }
                break;
        }
    }

    private fetchVehicleManufacturerModelRecord(error: any, successData: any): void {
        if (successData) {
            this.enablePageEditMode(successData);
        }
    }

    private deleteVehicleManufacturerModelRecord(error: any, successData: any): void {
        if (error) {
            this.pageMode = MntConst.eModeUpdate;
        } else if (successData) {
            this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.enablePageSelectMode();
        }
    }

    private saveVehicleManufacturerModelRecord(error: any, successData: any): void {
        if (successData) {
            this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.enablePageEditMode(successData);
        }
    }

    public onEllipsisDataRecived(data: any): void {
        if (data[GenericEllipsisEvent.add]) {
            this.uiForm.reset();
            this.isDisabled = false;
            this.vehicleManufacturerModelEllipsisConfig.disable = true;
            this.vehicleManufacturerModelEllipsisConfig.autoOpen = false;
            this.enableControls([]);
            this.pageMode = MntConst.eModeAdd;
        } else {
            this.enablePageEditMode(data);
        }
    }

    private enablePageEditMode(data: any): void {
        if (data) {
            this.isDisabled = false;
            this.disableControls([]);
            this.vehicleManufacturerModelEllipsisConfig.disable = false;
            this.pageMode = MntConst.eModeUpdate;
            this.tempVehicleManufacturerModelData = data;
            this.setFormControlValues(data);
        }
    }

    private enablePageSelectMode(openModal: boolean = true): void {
        this.enableControls([]);
        this.uiForm.reset();
        this.isDisabled = true;
        this.pageMode = MntConst.eModeSelect;
        this.vehicleManufacturerModelEllipsisConfig.disable = false;
        if (openModal) {
            this.vehicleManufacturerModelEllipsis.openModal();
        }
    }

    private setFormControlValues(data: any): void {
        if (data) {
            this.uiForm.patchValue(data);
            this.formPristine();
        }
    }

    private commonServiceCall(action: number | string, formData: any, callback: (error: any, success: any) => void): void {
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, action);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then((data) => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                callback('Success with error', null);
                this.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                return;
            }
            callback(null, data);
        }).catch((error) => {
            callback(error, null);
            this.isRequesting = false;
            this.displayMessage(error);
        });
    }
}
