import { OnInit, Injector, Component, ViewChild, AfterContentInit } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GenericSearch, IGenericSearchQuery } from '@app/base/GenericSearch';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { ProductSearchGridComponent } from '@app/internal/search/iCABSBProductSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';

@Component({
    templateUrl: 'iCABSBVehicleComponentsMaintenance.html'
})

export class VehicleComponentsMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit {

    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;
    @ViewChild('promptModal') public promptModal;

    public controls: IControls[] = [
        { name: 'CompartmentID' },
        { name: 'LoadSpaceCapacity', required: true },
        { name: 'Height', required: true },
        { name: 'Width', required: true },
        { name: 'Length', required: true },
        { name: 'LoadWeightCapacity', required: true },
        { name: 'ProductCodes' }
    ];

    public pageId: string;
    public isDisabled: boolean = true;
    public pageMode = MntConst.eModeUpdate;
    public promptConfirmContent: string;

    public productCodeEllipsis: Record<string, any> = {
        childparams: {
            parentMode: 'ProductCodesSearch',
            ProductCodes: ''
        },
        component: ProductSearchGridComponent
    };

    public headerParams: Record<string, string> = {
        operation: 'Business/iCABSBVehicleComponentsMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };

    public genericSearch: GenericSearch;
    public vehicleList: Array<any>;

    public genericSearchQuery: IGenericSearchQuery = {
        table: 'VehicleComponents'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVEHICLECOMPONENTSMAINTENANCE;
        this.genericSearch = new GenericSearch(injector);
        this.browserTitle = this.pageTitle = 'Vehicle Components Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.genericSearch.init(this.genericSearchQuery);
        this.getVehicleList();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.disableControls(['CompartmentID']);
    }

    private getVehicleList(): void {
        this.genericSearch.fetch().then((data) => {
            this.vehicleList = data.records;
        });
    }

    public onAddClick(): void {
        this.setRequiredStatus('CompartmentID', true);
        this.isDisabled = false;
        this.uiForm.reset();
        this.uiForm.enable();
        this.pageMode = MntConst.eModeAdd;
        this.onProductCodeChange();
    }

    public onCompartmentIDSelection(): void {
        this.isRequesting = true;
        this.isDisabled = false;
        this.uiForm.enable();
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 0);

        let formData: Object = {
            CompartmentID: this.getControlValue('CompartmentID')
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then((data) => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                return;
            }
            this.setFormControlValues(data);
            this.formPristine();
        }).catch((error) => {
            this.handleError(error);
        });
    }

    public onSaveClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.promptModal.show();
            this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
        }
    }

    public onCancelClick(): void {
        if (this.pageMode === MntConst.eModeAdd) {
            this.enablePageEditMode();
            this.setRequiredStatus('CompartmentID', false);
        } else {
            const selectedCompartmentID = this.getControlValue('CompartmentID');
            const selectedVehicle = this.vehicleList.find(({ CompartmentID }) => CompartmentID === selectedCompartmentID);
            if (selectedVehicle) {
                this.setFormControlValues(selectedVehicle);
            }
        }
    }

    public onDeleteClick(): void {
        this.pageMode = MntConst.eModeDelete;
        this.promptModal.show();
        this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
    }

    public onConfirmation(obj: any): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.deleteVehicleRecord();
        } else {
            this.saveVehicleRecord();
        }
    }

    public onCancellation(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.pageMode = MntConst.eModeUpdate;
        }
    }

    private saveVehicleRecord(): void {
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : 2);

        let postObj: Object = {
            table: 'VehicleComponents',
            methodtype: 'maintenance',
            CompartmentID: this.getControlValue('CompartmentID'),
            LoadSpaceCapacity: this.getControlValue('LoadSpaceCapacity'),
            Height: this.getControlValue('Height'),
            Width: this.getControlValue('Width'),
            Length: this.getControlValue('Length'),
            LoadWeightCapacity: this.getControlValue('LoadWeightCapacity'),
            ProductCodes: this.getControlValue('ProductCodes')
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, postObj).then((data) => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                return;
            }
            this.pageMode = MntConst.eModeUpdate;
            this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.getVehicleList();
        }).catch((error) => {
            this.handleError(error);
        });
    }

    private deleteVehicleRecord(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 3);

            let formData: Object = {
                CompartmentID: this.getControlValue('CompartmentID')
            };

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then((data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (this.hasError(data)) {
                    this.pageMode = MntConst.eModeUpdate;
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                this.getVehicleList();
                this.enablePageEditMode();
            }).catch((error) => {
                this.pageMode = MntConst.eModeUpdate;
                this.handleError(error);
            });
        }
    }

    public onProductSearchDataReceived(data: any): void {
        if (data) {
            this.setControlValue('ProductCodes', data['ProductCodes']);
            this.onProductCodeChange();
        }
    }

    public onProductCodeChange(): void {
        this.productCodeEllipsis.childparams.ProductCodes = this.getControlValue('ProductCodes');
    }

    private setFormControlValues(data: any): void {
        if (data) {
            this.uiForm.patchValue(data);
            this.onProductCodeChange();
        }
    }

    private enablePageEditMode(): void {
        this.uiForm.reset();
        this.disableControls(['CompartmentID']);
        this.isDisabled = true;
        this.pageMode = MntConst.eModeUpdate;
        this.onProductCodeChange();
    }

    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.isRequesting = false;
        this.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
    }
}
