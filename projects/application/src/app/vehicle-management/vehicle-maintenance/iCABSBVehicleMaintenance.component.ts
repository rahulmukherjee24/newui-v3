import { OnInit, Injector, Component, ViewChild, AfterContentInit } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { IControls } from '@app/base/ControlsType';
import { IGenericEllipsisControl, EllipsisGenericComponent } from '@shared/components/ellipsis-generic/ellipsis-generic';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';

@Component({
    templateUrl: 'iCABSBVehicleMaintenance.html'
})

export class VehicleMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit {

    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;
    @ViewChild('promptModal') public promptModal: any;
    @ViewChild('vehicleSearchEllipsis') vehicleSearchEllipsis: EllipsisGenericComponent;

    public controls: IControls[] = [
        { name: 'VehicleRegistration', required: true, type: MntConst.eTypeText },
        { name: 'VehicleModel', type: MntConst.eTypeText },
        { name: 'VehicleTypeNumber', required: true, type: MntConst.eTypeCode },
        { name: 'VehicleTypeDesc', required: true, disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeCode },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'PreviousEndMileage', required: true, type: MntConst.eTypeInteger },
        { name: 'VehiclePurchaseDate', type: MntConst.eTypeDate },
        { name: 'VehicleFirstUsedDate', type: MntConst.eTypeDate },
        { name: 'InactiveInd', type: MntConst.eTypeCheckBox }
    ];

    public pageId: string;
    public isDisabled: boolean = true;
    public pageMode: string = MntConst.eModeSelect;
    public promptConfirmContent: string;

    public ellipsisConfigs: Record<string, IGenericEllipsisControl> = {
        vehicleType: {
            autoOpen: false,
            ellipsisTitle: 'Vehicle Type Search',
            configParams: {
                table: 'VehicleType'
            },
            tableColumn: [
                { title: 'Type Number', name: 'VehicleTypeNumber' },
                { title: 'Description', name: 'VehicleTypeDesc' },
                { title: 'Weight', name: 'Weight' },
                { title: 'Height', name: 'Height' },
                { title: 'Depth', name: 'Depth' },
                { title: 'Width', name: 'Width' },
                { title: 'Cost Per KM', name: 'CostPerKM' },
                { title: 'Average Load Time', name: 'AverageLoadTime', type: MntConst.eTypeTime }
            ],
            disable: true
        },
        vehicleSearch: {
            autoOpen: true,
            ellipsisTitle: 'Vehicle Search',
            configParams: {
                table: 'Vehicle',
                shouldShowAdd: true
            },
            tableColumn: [
                { title: 'Vehicle Registration', name: 'VehicleRegistration', type: MntConst.eTypeText },
                { title: 'Previous End Mileage', name: 'PreviousEndMileage', type: MntConst.eTypeInteger }
            ],
            disable: false
        }
    };

    public branchDropdownConfig: Record<string, Record<string, string>> = {
        inputParams: {
            parentMode: 'LookUp'
        },
        active: {
            id: '',
            text: ''
        }
    };

    public headerParams: Record<string, string> = {
        operation: 'Business/iCABSBVehicleMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };

    private tempVehicleRegistrationData: object = null;
    public MntConst = MntConst;
    public shouldTrigger: number;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBVEHICLEMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Vehicle Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.disableControls(['VehicleRegistration']);
    }

    public onBtnClick(action: string): void {
        switch (action) {
            case 'Save':
                if (this.riExchange.validateForm(this.uiForm)) {
                    this.promptModal.show();
                    this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                }
                break;
            case 'Cancel':
                if (this.pageMode === MntConst.eModeAdd) {
                    this.enableSelectMode();
                } else {
                    this.setFormControlValues(this.tempVehicleRegistrationData);
                }
                break;
            case 'Delete':
                this.pageMode = MntConst.eModeDelete;
                this.promptConfirmContent = MessageConstant.Message.DeleteRecord;
                this.promptModal.show();
                break;
            case 'PromptCancel':
                if (this.pageMode === MntConst.eModeDelete) {
                    this.pageMode = MntConst.eModeUpdate;
                }
                break;
            case 'PromptConfirm':
                if (this.pageMode === MntConst.eModeDelete) {
                    this.deleteVehicleRegistrationRecord();
                } else {
                    this.saveVehicleRegistrationRecord();
                }
                break;
        }
    }

    public onVehicleRegistrationInputChange(): void {
        if (this.getControlValue('VehicleRegistration')) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 0);

            const formData = this.getCommonFormData();

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then((data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    this.enableSelectMode(false);
                    return;
                }
                this.enableEditMode(data);
            }).catch((error) => {
                this.handleError(error);
            });
        } else {
            this.enableSelectMode(false);
        }
    }

    private saveVehicleRegistrationRecord(): void {
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, this.pageMode === MntConst.eModeAdd ? 1 : 2);

        const formData = {
            ...this.getCommonFormData(),
            PreviousEndMileage: this.getControlValue('PreviousEndMileage'),
            VehicleModel: this.getControlValue('VehicleModel'),
            VehiclePurchaseDate: this.globalize.parseDateToFixedFormat(this.getControlValue('VehiclePurchaseDate')),
            VehicleFirstUsedDate: this.globalize.parseDateToFixedFormat(this.getControlValue('VehicleFirstUsedDate')),
            InactiveInd: this.getControlValue('InactiveInd'),
            VehicleTypeNumber: this.getControlValue('VehicleTypeNumber'),
            BranchNumber: this.getControlValue('BranchNumber')
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then((data) => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                return;
            }
            this.enableEditMode(data);
            this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
        }).catch((error) => {
            this.handleError(error);
        });
    }

    private deleteVehicleRegistrationRecord(): void {
        if (this.pageMode === MntConst.eModeDelete) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 3);

            const formData = this.getCommonFormData();

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then((data) => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (this.hasError(data)) {
                    this.pageMode = MntConst.eModeUpdate;
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.tempVehicleRegistrationData = null;
                this.displayMessage(MessageConstant.Message.RecordDeletedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                this.enableSelectMode();
            }).catch((error) => {
                this.pageMode = MntConst.eModeUpdate;
                this.handleError(error);
            });
        }
    }

    private getCommonFormData(): Record<string, string | boolean> {
        return {
            table: 'Vehicle',
            [this.serviceConstants.MethodType]: 'maintenance',
            UserCode: this.utils.getUserCode(),
            VehicleRegistration: this.getControlValue('VehicleRegistration')
        };
    }

    private setFormControlValues(data: any): void {
        if (data) {
            if (Number(this.getControlValue('VehicleTypeNumber')) !== (this.tempVehicleRegistrationData && this.tempVehicleRegistrationData['VehicleTypeNumber'])) {
                this.shouldTrigger = Math.random();
            }
            this.uiForm.patchValue(data);
            this.formPristine();
            this.branchDropdownConfig.active = {
                id: this.getControlValue('BranchNumber'),
                text: this.getControlValue('BranchNumber') ? this.utils.getBranchText(this.getControlValue('BranchNumber')) : ''
            };
        }
    }

    private enableSelectMode(openModal: boolean = true): void {
        this.uiForm.reset();
        this.disableControls(['VehicleRegistration']);
        this.disableControl('VehicleRegistration', false);
        this.ellipsisConfigs.vehicleSearch.disable = false;
        this.ellipsisConfigs.vehicleType.disable = true;
        this.isDisabled = true;
        this.pageMode = MntConst.eModeSelect;
        this.branchDropdownConfig.active = {
            id: '',
            text: ''
        };
        if (openModal) {
            this.vehicleSearchEllipsis.openModal();
        }
    }

    private enableEditMode(data: any): void {
        this.isDisabled = false;
        this.tempVehicleRegistrationData = data;
        this.setFormControlValues(data);
        this.ellipsisConfigs.vehicleType.disable = false;
        this.ellipsisConfigs.vehicleSearch.disable = false;
        this.enableControls(['VehicleTypeDesc']);
        this.disableControl('VehicleRegistration', true);
        this.pageMode = MntConst.eModeUpdate;
    }

    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.isRequesting = false;
        this.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
    }

    public onDataRecived(data: any, ellisisFor: string): void {
        switch (ellisisFor) {
            case 'vehicleRegistration':
                if (data['isAddEvent']) {
                    this.uiForm.reset();
                    this.enableControls(['VehicleTypeDesc']);
                    this.isDisabled = false;
                    this.ellipsisConfigs.vehicleType.disable = false;
                    this.ellipsisConfigs.vehicleSearch.autoOpen = false;
                    this.ellipsisConfigs.vehicleSearch.disable = true;
                    this.pageMode = MntConst.eModeAdd;
                    this.branchDropdownConfig.active = {
                        id: '',
                        text: ''
                    };
                } else {
                    this.enableEditMode(data);
                }
                break;
            case 'vehicleType':
                this.setControlValue('VehicleTypeNumber', data.VehicleTypeNumber);
                this.setControlValue('VehicleTypeDesc', data.VehicleTypeDesc);
                break;
        }
    }

    public onDefaultBranchChange(data: any): void {
        this.setControlValue('BranchNumber', data.BranchNumber);
        this.setControlValue('BranchName', data.BranchName);
        this.uiForm.markAsDirty();
    }
}
