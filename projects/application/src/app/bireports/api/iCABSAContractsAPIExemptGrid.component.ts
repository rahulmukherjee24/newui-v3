import { OnInit, Component, Injector, OnDestroy, ViewChild, AfterContentInit } from '@angular/core';

import { BranchSearchComponent } from '@internal/search/iCABSBBranchSearch';
import { CommonGridFunction } from '@base/CommonGridFunction';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAContractsAPIExemptGrid.html'
})

export class ContractsApiExemptGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    protected pageId: string;
    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;

    public controls: Array<Object> = [
        { name: 'BranchName', required: true, type: MntConst.eTypeCode },
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ShowContracts', type: MntConst.eTypeText }
    ];

    public dropdown: Record<string, Record<string, Object>> = {
        branchParams: {
            'parentMode': 'LookUp',
            'active': { id: this.utils.getBranchCode(), text: this.utils.getBranchCode() + ' - ' + this.utils.getBranchTextOnly() }
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageId = PageIdentifier.iCABSACONTRACTSAPIEXEMPTGRID;
        this.pageTitle = this.browserTitle = 'Contracts API Exempt - ' + this.utils.getBusinessText();
        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this.setControlValue('ShowContracts', 'All');
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1
            };
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
        } else {
            let branchNumber = this.getControlValue('BranchNumber');
            this.dropdown.branchParams['active'] = {
                id: branchNumber,
                text: branchNumber + ' - ' + this.utils.getBranchTextOnly(branchNumber)
            };
            this.onRiGridRefresh();
        }
        this.buildGrid();
    }

    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        search.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = 'Branch'; //For drilldown from parent pages we will need to change this
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData['RegionCode'] = ''; //For drilldown from parent pages we will need to change this
        formData['ShowContracts'] = this.getControlValue('ShowContracts');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData['riSortOrder'] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Application/iCABSAContractsAPIExemptGrid', search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data);
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ContractNumber', 'ContractAPI', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('AccountNumber', 'ContractAPI', 'AccountNumber', MntConst.eTypeText, 9);
        this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractName', 'ContractAPI', 'ContractName', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractAddress', 'ContractAPI', 'ContractAddress', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractAddress', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractCommenceDate', 'ContractAPI', 'ContractCommenceDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('ContractCommenceDate', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractExpiryDate', 'ContractAPI', 'ContractExpiryDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('ContractExpiryDate', MntConst.eAlignmentLeft);


        this.riGrid.AddColumn('ContractAnnualValue', 'ContractAPI', 'ContractAnnualValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('ContractAnnualValue', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('APIExemptText', 'ContractAPI', 'APIExemptText', MntConst.eTypeText, 50);
        this.riGrid.AddColumnAlign('APIExemptText', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('LastAPIDate', 'ContractAPI', 'LastAPIDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('LastAPIDate', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('LastPriceChangeDate', 'ContractAPI', 'LastPriceChangeDate', MntConst.eTypeDate, 5);
        this.riGrid.AddColumnAlign('LastPriceChangeDate', MntConst.eAlignmentLeft);


        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('AccountNumber', true);
        this.riGrid.AddColumnOrderable('ContractExpiryDate', true);
        this.riGrid.AddColumnOrderable('ContractCommenceDate', true);
        this.riGrid.AddColumnOrderable('ContractAnnualValue', true);
        this.riGrid.AddColumnOrderable('LastAPIDate', true);

        this.riGrid.Complete();

    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public onDataRcv(event: any): void {
        this.setControlValue('BranchNumber', event.BranchNumber);
        this.setControlValue('BranchName', event.BranchName);
        this.commonGridFunction.resetGrid();
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'ContractNumber' && this.riGrid.Details.GetRowId('ContractNumber') !== 'TOTALS') {
            this.navigate('LoadByKeyFields', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                parentMode: 'LoadByKeyFields',
                ContractNumber: this.riGrid.Details.GetValue('ContractNumber'),
                ContractName: this.riGrid.Details.GetValue('ContractName')
            });
        }
    }

}
