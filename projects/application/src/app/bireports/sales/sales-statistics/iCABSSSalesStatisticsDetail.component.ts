import { AfterContentInit, OnInit, Component, Injector, OnDestroy, ViewChild } from '@angular/core';

import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSSalesStatisticsDetail.html'
})

export class SalesStatisticsDetailComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public pageId: string;
    public controls = [
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'BusinessCode', disabled: true, type: MntConst.eTypeText },
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'CompanyDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'DateFrom', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateOnBefore', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateTo', disabled: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', disabled: true, type: MntConst.eTypeText },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ExpenseCode', disabled: true, type: MntConst.eTypeText },
        { name: 'ExpenseDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'FilterCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'FilterColumn', disabled: true, type: MntConst.eTypeText },
        { name: 'FilterDesc', disabled: true, type: MntConst.eTypeCode },
        { name: 'OriginCode', disabled: true, type: MntConst.eTypeText },
        { name: 'OriginDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'Postcode', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'RegionCode', disabled: true, type: MntConst.eTypeText },
        { name: 'RegionDesc', disabled: true, type: MntConst.eTypeText }
    ];

    private strGridData: any = {};
    private strMode: string;

    public filterByTxt: string;
    public hasGridData: boolean = false;
    public strLevel: string;
    public strReportMode: string;
    public pageParent: string;

    constructor(injector: Injector, public sysCharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSSALESSTATISTICSDETAIL;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.strReportMode = this.riExchange.getParentHTMLValue('ReportMode');
        this.pageTitle = 'Sales Statistics - ' + this.strReportMode;
        this.utils.setTitle(this.pageTitle);
        super.ngAfterContentInit();
        this.loadSysChars();

        this.pageParams.gridConfig = {
            pageSize: 10,
            totalRecords: 1,
            gridHandle: this.utils.randomSixDigitString(),
            gridCacheRefresh: true
        };
        this.pageParams.gridCurrentPage = 1;
        this.doEventForParent();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private doEventForParent(): void {
        this.strLevel = this.riExchange.getParentHTMLValue('ViewType');
        if (this.strLevel === 'Negotiating Employee' || this.strLevel === 'Sales Employee') {
            this.strLevel = 'Employee';
        }

        this.strMode = this.riExchange.getParentHTMLValue('ViewMode');

        this.strGridData[this.serviceConstants.Level] = 'detail';
        this.strGridData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        this.strGridData['DateFrom'] = this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('DateFrom')) as string;
        this.strGridData['DateTo'] = this.globalize.parseDateToFixedFormat(this.riExchange.getParentHTMLValue('DateTo')) as string;
        this.strGridData['DateOnBefore'] = this.riExchange.getParentHTMLValue('DateOnBefore');
        this.strGridData['LanguageCode'] = this.riExchange.LanguageCode();
        this.strGridData['RequestMode'] = 'Grid';
        this.strGridData['ViewMode'] = this.strMode;
        this.strGridData['StatMode'] = this.riExchange.getParentHTMLValue('pageMode') === 'national' ? 'national' : 'normal';
        this.strGridData['ViewType'] = this.riExchange.getParentHTMLValue('ViewType');
        this.strGridData['CompanyCode'] = this.riExchange.getParentHTMLValue('CompanyCode');
        this.strGridData['EmployeeLevelParent'] = this.riExchange.getParentHTMLValue('EmployeeLevelParent');
        this.pageParent = this.parentMode;

        switch (this.parentMode) {
            case 'business':
                this.loadBuisness();
                break;
            case 'region':
                this.loadRegion();
                break;
            case 'branch':
                this.loadBranch();
                break;
            case 'employee':
                this.loadEmployee();
                break;
            case 'Postcode':
                this.loadPostcode();
                break;
            default:
                this.displayMessage('Error invalid parent mode! ' + this.parentMode);
                break;
        }

        if (this.riExchange.getParentHTMLValue('FilterCode')) {
            this.strGridData['FilterBy'] = this.riExchange.getParentHTMLValue('FilterBy');
            this.strGridData['FilterCode'] = this.riExchange.getParentHTMLValue('FilterCode');
            this.strGridData['FilterDesc'] = this.riExchange.getParentHTMLValue('FilterDesc');
            this.filterByTxt = this.riExchange.getParentHTMLValue('FilterBy');
        }

        if (this.riExchange.getParentHTMLValue('CompanyCode')) {
            this.setControlValue('CompanyDesc', this.riExchange.getParentHTMLValue('CompanyCode') + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc'));
        }

        this.setControlValue('FilterColumn', this.riExchange.getParentAttributeValue('ColumnName'));

        this.strGridData['ReportMode'] = this.strReportMode;
        this.strGridData['RowID'] = this.riExchange.getParentAttributeValue('RowID');
        this.strGridData['Column'] = this.riExchange.getParentAttributeValue('Column');

        this.buildGrid();
        this.populateGrid();
    }

    private loadBuisness(): void {
        const groupDesc: string = this.riExchange.getParentAttributeValue('GroupDesc');
        const groupCode: string = this.riExchange.getParentAttributeValue('GroupCode');
        const funcDetail: string = 'BusinessDetailFor';

        this.setControlValue('BusinessCode', this.riExchange.getParentHTMLValue('BusinessCode'));
        this.setControlValue('BusinessDesc', this.riExchange.getParentHTMLValue('BusinessDesc'));

        switch (this.strLevel) {
            case 'Region':
                this.setControlValue('RegionDesc', groupDesc);
                this.setControlValue('RegionCode', groupCode);
                this.strGridData[this.serviceConstants.Function] = funcDetail + 'Region';
                break;
            case 'Branch':
                this.setControlValue('BranchName', groupDesc);
                this.setControlValue('BranchNumber', groupCode);
                this.strGridData[this.serviceConstants.Function] = funcDetail + 'Branch';
                break;
            case 'Product':
                this.setControlValue('ProductDesc', groupDesc);
                this.setControlValue('ProductCode', groupCode);
                this.strGridData[this.serviceConstants.Function] = funcDetail + 'Product';
                break;
            case 'Employee':
                this.setControlValue('EmployeeSurname', groupDesc);
                this.setControlValue('EmployeeCode', groupCode);
                this.strGridData['BranchNumber'] = this.riExchange.getParentAttributeValue('Branch') || '';
                this.strGridData[this.serviceConstants.Function] = funcDetail + 'Employee';
                break;
            case 'Origin':
                this.setControlValue('OriginDesc', groupDesc);
                this.setControlValue('OriginCode', groupCode);
                this.strGridData[this.serviceConstants.Function] = 'BusinessDetailForBusinessOrigin';
                break;
            case 'ExpenseCode':
                this.setControlValue('ExpenseDesc', groupDesc);
                this.setControlValue('ExpenseCode', groupCode);
                this.strGridData[this.serviceConstants.Function] = 'BusinessDetailForExpenseCode';
                break;
            default:
                this.displayMessage('Invalid strMode ' + this.strLevel);
                break;
        }
    }

    private loadRegion(): void {
        const groupDesc: string = this.riExchange.getParentAttributeValue('GroupDesc');
        const groupCode: string = this.riExchange.getParentAttributeValue('GroupCode');
        const regionCode: string = this.riExchange.getParentAttributeValue('RegionCode');
        const branchNumber: string = this.riExchange.getParentAttributeValue('BranchNumber') || '';

        this.setControlValue('BusinessCode', this.riExchange.getParentHTMLValue('BusinessCode'));
        this.setControlValue('BusinessDesc', this.riExchange.getParentHTMLValue('BusinessDesc'));
        this.setControlValue('RegionCode', this.riExchange.getParentHTMLValue('RegionCode'));
        this.setControlValue('Regionname', this.riExchange.getParentHTMLValue('RegionDesc'));
        this.setControlValue('FilterColumn', this.riExchange.getParentAttributeValue('ColumnName'));

        switch (this.strLevel) {
            case 'Branch':
                this.setControlValue('BranchName', groupDesc);
                this.setControlValue('BranchNumber', groupCode);
                this.strGridData['RegionCode'] = regionCode;
                this.strGridData[this.serviceConstants.Function] = 'RegionDetailForBranch';
                break;
            case 'Employee':
                this.setControlValue('EmployeeSurname', groupDesc);
                this.setControlValue('EmployeeCode', groupCode);
                this.strGridData['BranchNumber'] = branchNumber;
                this.strGridData['RegionCode'] = regionCode;
                this.strGridData[this.serviceConstants.Function] = 'RegionDetailForEmployee';
                break;
            case 'Product':
                this.setControlValue('ProductDesc', groupDesc);
                this.setControlValue('ProductCode', groupCode);
                this.strGridData['RegionCode'] = regionCode;
                this.strGridData[this.serviceConstants.Function] = 'RegionDetailForProduct';
                break;
            default:
                this.displayMessage('Invalid strMode ' + this.strLevel);
                break;
        }
    }

    private loadPostcode(): void {
        let postCode = this.riExchange.getParentHTMLValue('PostcodeCode');
        this.strGridData['BranchNumber'] = this.riExchange.getParentHTMLValue('BranchNumber');

        switch (this.strLevel) {
            case 'Product':
                this.setControlValue('ProductDesc', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('ProductCode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'PostcodeDetailForProduct';
                this.strGridData['Postcode'] = postCode;
                break;
            default:
                this.displayMessage('Invalid strMode ' + this.strLevel);
                break;
        }
    }

    private loadBranch(): void {
        this.riExchange.getParentHTMLValue('BranchName');
        this.strGridData['BranchNumber'] = this.riExchange.getParentHTMLValue('BranchNumber');

        switch (this.strLevel) {
            case 'Employee':
                this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'BranchDetailForEmployee';
                break;
            case 'Product':
                this.setControlValue('ProductDesc', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('ProductCode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'BranchDetailForProduct';
                break;
            case 'ProductQuantity':
                this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'ProductDetailForProductQuantity';
                break;
            case 'Origin':
                this.setControlValue('OriginDesc', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('OriginCode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'BranchDetailForBusinessOrigin';
                break;
            case 'Postcode':
                this.setControlValue('Postcode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'BranchDetailForPostcode';
                break;
            default:
                this.displayMessage('Invalid strMode ' + this.strLevel);
                break;
        }
    }

    private loadEmployee(): void {
        let EmpCode = this.riExchange.getParentHTMLValue('EmployeeCode');
        this.riExchange.getParentHTMLValue('EmployeeSurname');
        this.strGridData['BranchNumber'] = this.riExchange.getParentHTMLValue('BranchNumber');
        this.strGridData['ReportMode'] = this.strReportMode;
        switch (this.strLevel) {
            case 'Product':
                this.setControlValue('ProductDesc', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('ProductCode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'EmployeeDetailForProduct';
                this.strGridData['EmployeeCode'] = EmpCode;
                break;
            case 'Origin':
                this.setControlValue('OriginDesc', this.riExchange.getParentAttributeValue('GroupDesc'));
                this.setControlValue('OriginCode', this.riExchange.getParentAttributeValue('GroupCode'));
                this.strGridData[this.serviceConstants.Function] = 'EmployeeDetailForBusinessOrigin';
                this.strGridData['EmployeeCode'] = EmpCode;
                break;
            default:
                this.displayMessage('Invalid strMode ' + this.strLevel);
                break;
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ChangeProcessed', 'SalesStatistics', 'ChangeProcessed', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Contract', 'SalesStatistics', 'Contract', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('TierCode', 'SalesStatistics', 'TierCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnScreen('TierCode', false);
        this.riGrid.AddColumn('AccountOwner', 'SalesStatistics', 'AccountOwner', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnScreen('AccountOwner', false);
        this.riGrid.AddColumn('PremiseNumber', 'SalesStatistics', 'PremiseNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('BranchNumber', 'SalesStatistics', 'BranchNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ServiceCommissionEmployee', 'SalesStatistics', 'ServiceCommissionEmployee', this.riGrid.AddColumn, 10);
        this.riGrid.AddColumn('NegCommissionEmployee', 'SalesStatistics', 'NegCommissionEmployee', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Product', 'SalesStatistics', 'Product', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ProductDetail', 'SalesStatistics', 'ProductDetail', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('ProductDetail', false);
        this.riGrid.AddColumn('ProductSaleGroupDesc', 'SalesStatistics', 'ProductSaleGroupDesc', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Quantity', 'SalesStatistics', 'Quantity', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ProductGroupsPremise', 'SalesStatistics', 'ProductGroupsPremise', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ProductGroupsContract', 'SalesStatistics', 'ProductGroupsContract', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ProductGroupsAccount', 'SalesStatistics', 'ProductGroupsAccount', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ValueChangeCode', 'SalesStatistics', 'ValueChangeCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ServiceEffectDate', 'SalesStatistics', 'ServiceEffectDate', MntConst.eTypeDate, 10);

        if (this.pageParams.vSCEnableMinimumDuration) {
            this.riGrid.AddColumn('ContractMinimumDuration', 'SalesStatistics', 'ContractMinimumDuration', MntConst.eTypeText, 10);
            this.riGrid.AddColumnScreen('ContractMinimumDuration', false);
        }

        this.riGrid.AddColumn('SalesStatsValue', 'SalesStatistics', 'SalesStatsValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('SalesStatsValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('VisitFrequency', 'SalesStatistics', 'VisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnScreen('VisitFrequency', false);
        this.riGrid.AddColumn('InvoiceNarrativeText', 'SalesStatistics', 'InvoiceNarrativeText', MntConst.eTypeText, 30);
        this.riGrid.AddColumnScreen('InvoiceNarrativeText', false);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');

        this.strGridData[this.serviceConstants.GridMode] = '0';
        this.strGridData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        this.strGridData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        this.strGridData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
        this.strGridData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        this.strGridData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        this.strGridData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Sales/iCABSSSalesStatisticsDetail', search, this.strGridData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                if (this.isReturning()) {
                    setTimeout(() => {
                        this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                    }, 500);
                }
                this.riGrid.Execute(data);
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.displayMessage(error);
            });
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.gridCacheRefresh = false;
        super.getCurrentPage(currentPage);
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let sysCharQuery: QueryParams = new QueryParams();
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableCompanyCode,
            this.sysCharConstants.SystemCharEnableMinimumDuration
        ];

        sysCharQuery.set(this.serviceConstants.Action, '0');
        sysCharQuery.set(this.serviceConstants.BusinessCode, this.businessCode());
        sysCharQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        sysCharQuery.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));
        this.httpService.sysCharRequest(sysCharQuery).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            try {
                if (data.records) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                    this.pageParams.vSCEnableMinimumDuration = data.records[1].Required;
                }
            } catch (error) {
                this.displayMessage(error);
            }
        });
    }
}
