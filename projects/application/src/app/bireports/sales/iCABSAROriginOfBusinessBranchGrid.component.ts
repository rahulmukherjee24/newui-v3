import { Component, OnInit, Injector, AfterContentInit, ViewChild } from '@angular/core';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { BusinessOriginLangSearchComponent } from '@app/internal/search/iCABSBBusinessOriginLanguageSearch.component';
import { DropdownComponent } from '@shared/components/dropdown/dropdown';
import { BIReportsRoutes } from '@app/base/PageRoutes';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { StaticUtils } from '@shared/services/static.utility';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';

@Component({
  selector: 'icabs-origin-of-business-branch-grid',
  templateUrl: './iCABSAROriginOfBusinessBranchGrid.html',
  providers: [CommonLookUpUtilsService]
})

export class OriginOfBusinessBranchGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
  @ViewChild('riGrid') riGrid: GridAdvancedComponent;
  @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
  @ViewChild('businessOriginDropdown') businessOriginDropdown: BusinessOriginLangSearchComponent;
  @ViewChild('businessOriginSourceDropdown') businessOriginSourceDropdown: DropdownComponent;
  @ViewChild('businessOriginDetailDropdown') businessOriginDetailDropdown: DropdownComponent;

  public controls = [
    { name: 'BusinessCode', readonly: true, required: true, type: MntConst.eTypeCode },
    { name: 'BusinessDesc', disabled: true, required: true, type: MntConst.eTypeText },
    { name: 'BranchNumber', readonly: true, disabled: true, type: MntConst.eTypeInteger },
    { name: 'BranchName', readonly: true, disabled: true },
    { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
    { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
    { name: 'EmployeeCode', type: MntConst.eTypeCode },
    { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
    { name: 'BusinessOriginCode', type: MntConst.eTypeCode },
    { name: 'BusinessOriginDesc', type: MntConst.eTypeText, disabled: true },
    { name: 'BusinessSourceCode', type: MntConst.eTypeCode },
    { name: 'BusinessSourceDesc', type: MntConst.eTypeText, disabled: true },
    { name: 'BusinessOriginDetailCode', type: MntConst.eTypeCode },
    { name: 'BusinessOriginDetailDesc', type: MntConst.eTypeText, disabled: true },
    { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
    { name: 'DateTo', required: true, type: MntConst.eTypeDate },
    { name: 'LevelType', required: true, value: 'BusinessOrigin' },
    { name: 'FilterType', required: true, type: MntConst.eTypeDate, value: 'All' },
    { name: 'SalesType', required: true, type: MntConst.eTypeDate, value: 'All' },
    { name: 'ViewBy', required: true, value: 'Branch' },
    { name: 'ContractTypeCode', required: true, disabled: true, type: MntConst.eTypeCode },
    { name: 'ContractTypeDesc', required: true, disabled: true, type: MntConst.eTypeText }
  ];

  public levelListValues: Array<any> = [
    { value: 'BusinessSource', text: 'Business Source' },
    { value: 'BusinessOrigin', text: 'Business Origin' },
    { value: 'BusinessOriginDetail', text: 'Business Origin Detail' }
  ];

  public filterListValues: Array<any> = [
    { value: 'All', text: 'All' },
    { value: 'ExcludeReneg', text: 'Exclude Renegs' }
  ];

  public salesTypeValues: Array<any> = [
    { value: 'All', text: 'All' },
    { value: 'SalesEmp', text: 'Sales Employee' },
    { value: 'ServiceSales', text: 'Service/Sales Emp' }
  ];

  public viewByValues: Array<any> = [
    { value: 'Branch', text: 'Branch' },
    { value: 'Region', text: 'Region' }
  ];

  public commonGridFunction: CommonGridFunction;
  public pageId: string;
  public curPageTitle: string;
  public hasGridData: boolean = false;
  public pageType: string;
  public vBusinessCode: string;
  levelType: string;
  trigger: boolean;
  viewByValue: any;
  functionName: string;
  displayEmpField: boolean = false;
  displayOriginField: boolean;
  displayDetailField: boolean;
  displaySourceField: boolean;

  public ellipsis = {
    employeeConfig: {
      inputParams: {
        parentMode: 'LookUp'
      },
      disabled: false,
      component: EmployeeSearchComponent
    }
  };

  public dropdown: any = {
    businessOriginLang: {
      inputParams: {
        businessCode: '',
        countryCode: '',
        BusinessOriginCode: ''
      },
      displayFields: ['BusinessOriginCode', 'BusinessOriginDesc'],
      isRequired: false,
      isDisabled: false,
      triggerValidate: false,
      active: {
        'id': '',
        'text': ''
      }
    },
    businessOriginSouceLang: {
      inputParams: {
        businessCode: '',
        countryCode: ''
      },
      displayFields: ['BusinessSourceCode', 'BusinessSourceDesc'],
      isRequired: false,
      isDisabled: false,
      triggerValidate: false,
      active: {
        'id': '',
        'text': ''
      }
    },
    businessOriginDetailLang: {
      inputParams: {
        businessCode: '',
        countryCode: ''
      },
      displayFields: ['BusinessOriginDetailCode', 'BusinessOriginDetailSystemDesc', 'BusinessOriginCode'],
      isRequired: false,
      isDisabled: false,
      triggerValidate: false,
      active: {
        'id': '',
        'text': ''
      }
    }
  };

  constructor(injector: Injector, private commonLookupUtil: CommonLookUpUtilsService) {
    super(injector);
    this.commonGridFunction = new CommonGridFunction(this);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    this.levelType = this.getControlValue('LevelType');
  }

  public ngAfterContentInit(): void {
    this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('pageType'));
    this.pageId = PageIdentifier['ICABSARORIGINOFBUSINESSGRID' + this.pageType.toUpperCase()];
    this.pageTitle = 'Origin Of Business - ' + this.pageType;
    this.utils.setTitle(this.pageTitle);
    super.ngAfterContentInit();
    this.windowOnload();
  }

  private windowOnload(): void {
    if (this.isReturning()) {
      const fields = ['BusinessCode', 'BranchNumber', 'BranchName', 'BusinessOriginCode', 'BusinessOriginDesc', 'BusinessSourceCode', 'BusinessSourceDesc', 'BusinessOriginDetailCode', 'DateFrom', 'DateTo', 'LevelType', 'FilterType', 'SalesType'];
      fields.forEach(control => {
        this.setControlValue(control, this.getControlValue(control));
      });
      this.levelType = this.getControlValue('LevelType');
      this.trigger = true;
      this.setDropdownActive();
      this.setControlValue('ViewBy', this.getControlValue('ViewBy'));
      this.setControlValue('BusinessDesc', this.utils.getBusinessText());
      this.buildGrid();
      this.commonGridFunction.onRiGridRefresh();
    }
    else {
      this.pageParams.gridConfig = {
        pageSize: 10,
        totalRecords: 1,
        gridHandle: this.utils.randomSixDigitString(),
        gridCacheRefresh: true
      };
      this.pageParams.gridCurrentPage = 1;
      this.pageParams.valueColumnsCount = [];
      this.pageParams.viewModeValues = [];
      this.doEventsForParentMode();
    }
  }

  private doEventsForParentMode(): void {
    switch (this.parentMode) {
      case 'Business':
        this.viewByValue = this.riExchange.getParentHTMLValue('ViewBy');
        this.functionName = this.viewByValue === 'Region' ? 'BusinessDetailForRegion' : 'BusinessDetailForBranch';
        this.setCommonFieldsFromParentMode();
        break;
      case 'Region':
        this.functionName = 'RegionDetailForBranch';
        this.setCommonFieldsFromParentMode();
        break;
      case 'Branch':
        this.functionName = 'BranchDetailForEmployee';
        this.setCommonFieldsFromParentMode();
        break;
      case 'Employee':
        this.functionName = 'EmployeeDetail';
        this.setCommonFieldsFromParentMode();
        break;
      default:
        if (this.pageType !== 'Business') {
          this.setControlValue('BranchNumber', this.utils.getBranchCode());
          this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        }
        if (this.pageType === 'Region') {
          this.commonLookupUtil.getRegionDesc().subscribe((data) => {
            this.setControlValue('RegionCode', data[0][0]['RegionCode']);
            this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
        });
        }
        this.setControlValue('BusinessCode', this.businessCode());
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(StaticUtils.getFirstDayOfMonth()));
        this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(new Date()));
        this.buildGrid();
    }
  }

  private setCommonFieldsFromParentMode(): void {
    const branchNumber: string = this.riExchange.getParentAttributeValue('BranchNumber') || this.riExchange.getParentHTMLValue('BranchNumber');
    const parentHTMLValues = ['BusinessCode','RegionCode','RegionDesc','BusinessOriginCode', 'BusinessOriginDesc', 'BusinessOriginDetailCode', 'BusinessOriginDetailDesc', 'BusinessSourceCode', 'BusinessSourceDesc', 'LevelType', 'FilterType', 'DateFrom', 'DateTo','SalesType'];
    parentHTMLValues.forEach(htmlKey => {
      this.setControlValue(htmlKey, this.riExchange.getParentHTMLValue(htmlKey) || this.riExchange.getParentAttributeValue(htmlKey));
    });
    this.setControlValue('BranchNumber', branchNumber);
    this.setControlValue('BranchName', this.utils.getBranchTextOnly(branchNumber));
    this.levelType = this.getControlValue('LevelType');
    this.trigger = true;
    switch (this.pageType) {
      case 'Detail':
        let contractType = this.riExchange.getParentAttributeValue('ContractTypeCode') || 'All';
        let contractValues = contractType === 'C' ? 'Contracts' : contractType === 'J' ? 'Jobs' : 'All';
        this.setControlValue('ContractTypeCode', contractType);
        this.setControlValue('ContractTypeDesc', contractValues);
        this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.getControlValue('BusinessCode')));
        this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode') || this.riExchange.getParentAttributeValue('EmployeeCode'));
        this.disableControls(['FilterType']);
        this.displayEmpField = this.getControlValue('EmployeeCode') !== '';
        this.displayOriginField = this.getControlValue('BusinessOriginCode') !== '';
        this.displayDetailField = this.getControlValue('BusinessOriginDetailCode') !== '';
        this.displaySourceField = this.getControlValue('BusinessSourceCode') !== '';
        this.dropdown.businessOriginLang.isDisabled = true;
        this.dropdown.businessOriginSouceLang.isDisabled = true;
        this.dropdown.businessOriginDetailLang.isDisabled = true;
        this.ellipsis.employeeConfig.disabled = true;
        break;
      case 'Employee':
        this.setRequiredStatus('EmployeeCode', true);
        const EmpCode: string = this.riExchange.getParentAttributeValue('EmployeeCode');
        this.setControlValue('EmployeeCode', EmpCode);
        break;
      default:
    }
    this.setDropdownActive();
    this.buildGrid();
    this.populateGrid();
  }

  public populateGrid(): void {
    if (this.uiForm.invalid) {
      return;
    }
    else {
      let commonFormArray = ['LevelType', 'BusinessCode', 'BusinessOriginCode', 'BusinessOriginDetailCode', 'DateFrom', 'DateTo', 'FilterType'];
      if (!this.riExchange.validateForm(this.uiForm))
        return;
      let formData: any = {
        'LanguageCode': this.riExchange.LanguageCode()
      };
      commonFormArray.forEach(ele => {
        formData = {
          ...formData,
          [ele]: this.getControlValue(ele)
        };
      });
      let search: QueryParams = this.getURLSearchParamObject();
      search.set(this.serviceConstants.Action, '2');
      switch (this.pageType) {
        case 'Business':
          formData['ViewBy'] = this.getControlValue('ViewBy');
          break;
        case 'Region':
          formData['RegionCode'] = this.getControlValue('RegionCode');
          formData['SalesType'] = this.getControlValue('SalesType');
          formData['ValuePass'] = this.parentMode === 'Business';
          break;
        case 'Branch':
          formData['SalesType'] = this.getControlValue('SalesType');
          formData['BranchNumber'] = this.getControlValue('BranchNumber');
          break;
        case 'Employee':
          formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
          formData['BranchNumber'] = this.getControlValue('BranchNumber');
          break;
        case 'Detail':
          formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
          formData['ContractTypeCode'] = this.getControlValue('ContractTypeCode');
          formData['BranchNumber'] = this.getControlValue('BranchNumber');
          formData['RegionCode'] = this.getControlValue('RegionCode');
          formData['function'] = this.functionName;
          formData['level'] = 'detail';
          break;
        default:
      }
      if (this.pageType !== 'Detail') {
        formData[this.serviceConstants.Level] = this.pageType;
      }
      formData[this.serviceConstants.GridMode] = '0';
      formData[this.serviceConstants.PageSize] = '14';
      formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
      formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
      formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
      formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
      formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
      this.ajaxSource.next(this.ajaxconstant.START);
      this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSAROriginOfBusiness', search, formData).then(data => {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.commonGridFunction.setPageData(data);
      },
        (error) => {
          this.ajaxSource.next(this.ajaxconstant.COMPLETE);
          this.hasGridData = false;
          this.displayMessage(error);
        });
    }
  }

  public buildGrid(): void {
    this.riGrid.Clear();
    switch (this.pageType) {
      case 'Business':
        this.viewByValue = this.getControlValue('ViewBy');
        if (this.viewByValue === 'Region') {
          this.riGrid.AddColumn('RegionCode', 'OriginOfBusiness', 'RegionCode', MntConst.eTypeCode, 6);
          this.riGrid.AddColumnAlign('RegionCode', MntConst.eAlignmentCenter);
          this.riGrid.AddColumn('RegionDesc', 'OriginOfBusiness', 'RegionDesc', MntConst.eTypeText, 14);
          this.riGrid.AddColumnAlign('RegionDesc', MntConst.eAlignmentLeft);
          this.riGrid.AddColumnOrderable('RegionCode', true);
        }
        else {
          this.riGrid.AddColumn('BranchNumber', 'OriginOfBusiness', 'BranchNumber', MntConst.eTypeInteger, 6);
          this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
          this.riGrid.AddColumn('BranchName', 'OriginOfBusiness', 'BranchName', MntConst.eTypeText, 14);
          this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
          this.riGrid.AddColumnOrderable('BranchNumber', true);
        }
        this.riGrid.AddColumn('BusinessOriginCode', 'OriginOfBusiness', 'BusinessOriginCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('BusinessOriginCode', MntConst.eAlignmentCenter);
        break;
      case 'Region':
        this.riGrid.AddColumn('BranchNumber', 'OriginOfBusiness', 'BranchNumber', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchName', 'OriginOfBusiness', 'BranchName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('BusinessOriginCode', 'OriginOfBusiness', 'BusinessOriginCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('BusinessOriginCode', MntConst.eAlignmentCenter);
        break;
      case 'Branch':
        this.riGrid.AddColumn('EmployeeCode', 'OriginOfBusiness', 'EmployeeCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeSurname', 'OriginOfBusiness', 'EmployeeSurname', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('BusinessOriginCode', 'OriginOfBusiness', 'BusinessOriginCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('BusinessOriginCode', MntConst.eAlignmentCenter);
        break;
      case 'Employee':
        this.riGrid.AddColumn('BusinessOriginCode', 'OriginOfBusiness', 'BusinessOriginCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('BusinessOriginCode', MntConst.eAlignmentCenter);
        break;
      case 'Detail':
        this.riGrid.AddColumn('BusinessOrigin', 'OriginOfBusiness', 'BusinessOrigin', MntConst.eTypeText, 2);
        this.riGrid.AddColumnAlign('BusinessOrigin', MntConst.eAlignmentCenter);
        this.businessOriginDetailColumn();
        this.riGrid.AddColumn('Contract', 'OriginOfBusiness', 'Contract', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('Employee', 'OriginOfBusiness', 'Employee', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('Employee', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceValueEffectDate', 'OriginOfBusiness', 'ServiceValueEffectDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceValueEffectDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ValueChangeReason', 'OriginOfBusiness', 'ValueChangeReason', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('SalesStatsProcessedDate', 'OriginOfBusiness', 'SalesStatsProcessedDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('SalesStatsProcessedDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'OriginOfBusiness', 'PremiseNumber', MntConst.eTypeInteger, 8);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductCode', 'OriginOfBusiness', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchNumber', 'OriginOfBusiness', 'BranchNumber', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('SalesStatsValue', 'OriginOfBusiness', 'SalesStatsValue', MntConst.eTypeCurrency, 10);
        break;
      default:
    }
    if (this.pageType !== 'Detail') {
      this.riGrid.AddColumn('BusinessOriginDesc', 'OriginOfBusiness', 'BusinessOriginDesc', MntConst.eTypeText, 30);
      this.riGrid.AddColumnAlign('BusinessOriginDesc', MntConst.eAlignmentLeft);
      this.businessOriginDetailColumn();
      this.riGrid.AddColumn('ContractNewBusiness', 'OriginOfBusiness', 'ContractNewBusiness', MntConst.eTypeInteger, 4);
      this.riGrid.AddColumnAlign('ContractNewBusiness', MntConst.eAlignmentRight);
      this.riGrid.AddColumn('ContractNewValue', 'OriginOfBusiness', 'ContractNewValue', MntConst.eTypeCurrency, 10);
      this.riGrid.AddColumnAlign('ContractNewValue', MntConst.eAlignmentRight);
      this.riGrid.AddColumn('ContractNewPercentage', 'OriginOfBusiness', 'ContractNewPercentage', MntConst.eTypeDecimal2, 6);
      this.riGrid.AddColumnAlign('ContractNewPercentage', MntConst.eAlignmentRight);
      this.riGrid.AddColumn('JobNewBusiness', 'OriginOfBusiness', 'JobNewBusiness', MntConst.eTypeInteger, 4);
      this.riGrid.AddColumnAlign('JobNewBusiness', MntConst.eAlignmentRight);
      this.riGrid.AddColumn('JobNewValue', 'OriginOfBusiness', 'JobNewValue', MntConst.eTypeCurrency, 10);
      this.riGrid.AddColumnAlign('JobNewValue', MntConst.eAlignmentRight);
      this.riGrid.AddColumn('JobNewPercentage', 'OriginOfBusiness', 'JobNewPercentage', MntConst.eTypeDecimal2, 6);
      this.riGrid.AddColumnAlign('JobNewPercentage', MntConst.eAlignmentRight);
      this.riGrid.AddColumnOrderable('EmployeeCode', true);
      this.riGrid.AddColumnOrderable('BusinessOriginCode', true);
    }
    this.riGrid.Complete();
    this.dateCollist = this.riGrid.getColumnIndexListFromFull([
      'ServiceValueEffectDate',
      'SalesStatsProcessedDate'
    ]);
  }

  private businessOriginDetailColumn(): void {
    if (this.levelType === 'BusinessOriginDetail') {
      this.riGrid.AddColumn('BusinessOriginDetailCode', 'OriginOfBusiness', 'BusinessOriginDetailCode', MntConst.eTypeCode, 5);
      this.riGrid.AddColumnAlign('BusinessOriginDetailCode', MntConst.eAlignmentLeft);
      this.riGrid.AddColumn('BusinessOriginDetailDesc', 'OriginOfBusiness', 'BusinessOriginDetailDesc', MntConst.eTypeText, 30);
      this.riGrid.AddColumnAlign('BusinessOriginDetailDesc', MntConst.eAlignmentLeft);
    }
  }

  public onLevelTypeChange(): void {
    this.levelType = this.getControlValue('LevelType');
    this.setControlValue('BusinessOriginCode', '');
    this.setControlValue('BusinessOriginDesc', '');
    this.setControlValue('BusinessSourceCode', '');
    this.setControlValue('BusinessSourceDesc', '');
    this.setControlValue('BusinessOriginDetailCode', '');
    this.setControlValue('BusinessOriginDetailDesc', '');
    if (this.levelType === 'BusinessSource') {
      this.setBusninessSource();
    }
    if (this.levelType === 'BusinessOriginDetail') {
      if (this.getControlValue('BusinessOriginCode')) {
        this.businessOriginDetail_LookUp(this.getControlValue('BusinessOriginCode'));
      }
      else {
        this.businessOriginDetail_LookUp();
      }
    }
    this.dropdown.businessOriginLang.active = {
      id: '', text: ''
    };
    this.dropdown.businessOriginSouceLang.active = {
      id: '', text: ''
    };
    this.dropdown.businessOriginDetailLang.active = {
      id: '', text: ''
    };
  }

  public onRiGridRefresh(): void {
    this.buildGrid();
    this.commonGridFunction.onRefreshClick();
  }

  public filterOptionSelection(filterName: any, data: any): void {
    this.commonGridFunction.resetGrid();
    switch (filterName) {
      case 'BusinessOrigin':
        this.setControlValue('BusinessOriginCode', data['BusinessOriginLang.BusinessOriginCode']);
        this.setControlValue('BusinessOriginDesc', data['BusinessOriginLang.BusinessOriginDesc']);
        this.setControlValue('BusinessOriginDetailCode', '');
        this.setControlValue('BusinessOriginDetailDesc', '');
        this.dropdown.businessOriginDetailLang.active = {
          id: '',
          text: ''
        };
        if (this.levelType === 'BusinessOriginDetail') {
          this.businessOriginDetail_LookUp(this.getControlValue('BusinessOriginCode'));
        }
        break;
      case 'BusinessSource':
        this.setControlValue('BusinessSourceCode', data.value['BusinessSourceCode']);
        this.setControlValue('BusinessSourceDesc', data.value['BusinessSourceDesc']);
        this.setControlValue('BusinessOriginCode', data.value['BusinessSourceCode']);
        break;
      case 'BusinessOriginDetail':
        this.setControlValue('BusinessOriginDetailCode', data['value']['BusinessOriginDetailCode']);
        this.setControlValue('BusinessOriginDetailDesc', data['value']['BusinessOriginDetailSystemDesc']);
        if (data['value']['BusinessOriginDetailCode'] !== '' && this.getControlValue('BusinessOriginCode') !== data['value']['BusinessOriginCode']) {
          this.setControlValue('BusinessOriginCode', data['value']['BusinessOriginCode']);
          this.businessOrigin_LookUp();
        }
        break;
      case 'EmployeeDetails':
        this.setControlValue('EmployeeCode', data.EmployeeCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        break;
      default:
    }
  }

  private businessOriginDetail_LookUp(businessOriginCode?: any): void {
    let query =
      [{
        'table': 'BusinessOriginDetail',
        'query': {
          'BusinessCode': this.businessCode()
        },
        'fields': ['BusinessOriginDetailCode', 'BusinessOriginDetailSystemDesc', 'BusinessOriginCode']
      }];
    if (businessOriginCode) {
      query[0]['query']['BusinessOriginCode'] = businessOriginCode;
    }
    this.LookUp.lookUpPromise(query, 1000)
      .then(data => {
        if (data[0].length !== 0) {
          this.businessOriginDetailDropdown.updateComponent(data[0]);
        }
        else {
          this.businessOriginDetailDropdown.updateComponent([]);
        }
      }, (error) => {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.displayMessage(error);
      });
  }

  private businessOrigin_LookUp(): any {
    let Query = {
      'table': 'BusinessOrigin',
      'query': {
        'BusinessOriginCode': this.getControlValue('BusinessOriginCode'),
        'BusinessCode': this.businessCode()
      },
      'fields': ['BusinessOriginSystemDesc', 'BusinessOriginCode']
    };
    this.LookUp.lookUpPromise([Query])
      .then(data => {
        if (data[0].length !== 0) {
          this.setControlValue('BusinessOriginDesc', data[0][0]['BusinessOriginSystemDesc']);
          this.dropdown.businessOriginLang.active = {
            id: this.getControlValue('BusinessOriginCode'), text: this.getControlValue('BusinessOriginCode') + ' - ' + this.getControlValue('BusinessOriginDesc')
          };
          if (this.levelType === 'BusinessOriginDetail') {
            this.businessOriginDetail_LookUp(this.getControlValue('BusinessOriginCode'));
          }
        }
      });
  }

  private setBusninessSource(): void {
    this.commonLookupUtil.getBusinessSource().then(data => {
      if (data[0]) {
        this.businessOriginSourceDropdown.updateComponent(data[0]);
      }
    }, (error) => {
      this.ajaxSource.next(this.ajaxconstant.COMPLETE);
      this.displayMessage(error);
    });
  }

  private setDropdownActive(): void {
    if (this.levelType === 'BusinessSource') { this.setBusninessSource(); }
    if (this.getControlValue('BusinessSourceCode')) {
      this.dropdown.businessOriginSouceLang.active = {
        id: this.getControlValue('BusinessSourceCode'), text: this.getControlValue('BusinessSourceCode') + ' - ' + this.getControlValue('BusinessSourceDesc')
      };
    }
    if (this.getControlValue('BusinessOriginCode') && this.levelType !== 'BusinessSource') {
      this.businessOrigin_LookUp();
    }
    if (this.getControlValue('BusinessOriginDetailCode')) {
      this.dropdown.businessOriginDetailLang.active = {
        id: this.getControlValue('BusinessOriginDetailCode'), text: this.getControlValue('BusinessOriginDetailCode') + ' - ' + this.getControlValue('BusinessOriginDetailDesc') + ' - ' + this.getControlValue('BusinessOriginCode')
      };
    }
    else if (!this.getControlValue('BusinessOriginDetailCode') && this.levelType === 'BusinessOriginDetail') {
      this.businessOriginDetail_LookUp();
    }
  }

  public riGridBodyOnDblClick(): void {
    let params = {};
    let cType;
    let columnName = this.riGrid.CurrentColumnName;
    const contractTypeList = ['ContractNewBusiness', 'ContractNewValue', 'ContractNewPercentage'];
    const jobTypeList = ['JobNewBusiness', 'JobNewValue', 'JobNewPercentage'];
    const allTypeList = ['BusinessOriginDetailCode', 'BusinessOriginDetailDesc'];
    cType = contractTypeList.includes(columnName) ? 'C' : jobTypeList.includes(columnName) ? 'J' : allTypeList.includes(columnName) ? 'All' : '';
    if (this.pageType !== 'Detail') {
      switch (this.pageType) {
        case 'Business':
          if (this.viewByValue === 'Branch') {
            this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
            this.setAttribute('BranchName', this.riGrid.Details.GetValue('BranchName'));
          }
          else {
            this.setAttribute('RegionCode', this.riGrid.Details.GetValue('RegionCode'));
            this.setAttribute('RegionDesc', this.riGrid.Details.GetValue('RegionDesc'));
          }
          let check = this.riGrid.Details.GetRowId('BranchNumber') !== 'TOTAL' && this.riGrid.Details.GetRowId('BranchNumber') !== undefined || this.riGrid.Details.GetRowId('RegionCode') !== 'TOTAL' && this.riGrid.Details.GetRowId('RegionCode') !== undefined;
          this.setAttribute('ContractTypeCode', cType);
          params['pageType'] = cType ? 'Detail' : this.viewByValue;
          if (check && this.riGrid.Details.GetAttribute(columnName,'drilldown')) {
            this.setDetailAttributes(params['pageType']);
          }
          break;
        case 'Branch':
          if (this.riGrid.Details.GetRowId('EmployeeCode') !== 'TOTAL' && this.riGrid.Details.GetAttribute(columnName,'drilldown')) {
            this.setAttribute('EmployeeCode', this.riGrid.Details.GetValue('EmployeeCode'));
            this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
            params['pageType'] = cType ? 'Detail' : 'Employee';
            this.setAttribute('ContractTypeCode', cType);
            this.setDetailAttributes(params['pageType']);
          }
          break;
        case 'Region':
          if (this.riGrid.Details.GetRowId('BranchNumber') !== 'TOTAL' && this.riGrid.Details.GetAttribute(columnName,'drilldown')) {
            this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('BranchNumber'));
            params['pageType'] = cType ? 'Detail' : 'Branch';
            this.setAttribute('ContractTypeCode', cType);
            this.setDetailAttributes(params['pageType']);
          }
          break;
        case 'Employee':
          if (columnName !== 'BusinessOriginDesc') {
            this.setAttribute('ContractTypeCode', cType);
            params['pageType'] = 'Detail';
            this.setDetailAttributes(params['pageType']);
          }
          break;
        default:
      }
    }
  }

  private setDetailAttributes(pagetype: any): void {
    let attr = ['BusinessOriginCode', 'BusinessOriginDesc', 'BusinessOriginDetailCode', 'BusinessOriginDetailDesc', 'BusinessSourceCode', 'BusinessSourceDesc'];
    if (pagetype === 'Detail') {
      attr.forEach(ele => {
        let eleVal = this.riGrid.Details.GetRowId(ele) !== 'TOTAL' ? this.riGrid.Details.GetValue(ele) : '';
        this.setAttribute(ele, eleVal);
      });
      if (this.levelType === 'BusinessSource' && this.riGrid.Details.GetRowId('BusinessOriginCode') !== 'TOTAL') {
        this.setAttribute('BusinessOriginCode', this.riGrid.Details.GetValue('BusinessOriginCode'));
        this.setAttribute('BusinessSourceCode', this.riGrid.Details.GetValue('BusinessOriginCode'));
        this.setAttribute('BusinessSourceDesc', this.riGrid.Details.GetValue('BusinessOriginDesc'));
      }
    }
    else if (this.levelType === 'BusinessOriginDetail' && pagetype !== 'Detail') {
      attr.forEach(ele => {
        let eleVal = this.riGrid.Details.GetRowId(ele) !== 'TOTAL' ? this.riGrid.Details.GetValue(ele) : '';
        this.setAttribute(ele, eleVal);
      });
    }
    else {
      attr.forEach(ele => {
        this.setAttribute(ele, '');
      });
    }
    this.navigate(this.pageType, BIReportsRoutes['iCABSARORIGINOFBUSINESS' + pagetype.toUpperCase()], { pageType: pagetype });
  }
}
