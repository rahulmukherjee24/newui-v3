import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { IControls } from '@app/base/ControlsType';

@Component({
    templateUrl: 'iCABSADeferredTurnoverDetailGrid.html'
})

export class DeferredTurnoverDetailGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public commonGrifFunction: CommonGridFunction;
    public pageId: string;
    public pageTitle: string;
    public controls: IControls[] = [
        { name: 'BranchName', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'BusinessCode', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'CompanyCode' },
        { name: 'CompanyDesc' },
        { name: 'MonthNumber' },
        { name: 'RegionCode', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'RegionDesc', disabled: true, required: true, type: MntConst.eTypeText }
    ];
    public type: string;
    public hasGridData: boolean = false;

    public dropDown: Record<string, Object> = {
        company: {
            inputParams: {
                'parentMode': 'LookUp',
                businessCode: this.utils.getBusinessCode(),
                countryCode: this.utils.getCountryCode()
            },
            active: {
                id: '',
                text: ''
            }
        }
    };

    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.commonGrifFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSADEFERREDTURNOVERDETAILGRID;
        this.pageTitle = 'Deferred Turnover';

    }

    ngOnInit(): void {
        super.ngOnInit();
        this.utils.setTitle('Detailed Deferred Turnover');
        this.riExchange.getParentHTMLValue('MonthNumber');
        this.pageParams.gridConfig = {
            totalRecords: 1,
            gridHandle: this.utils.randomSixDigitString(),
            pageSize: 10
        };
        this.pageParams.gridCacheRefresh = true;
        this.pageParams.gridCurrentPage = 1;
        this.getDetailsFromParentMode();
        this.loadSysChars();
        if (this.getControlValue('CompanyCode')) {
            this.dropDown.company['active'] = {
                id: this.getControlValue('CompanyCode'),
                text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
            };
        }

    }

    private getDetailsFromParentMode(): void {
        switch (this.parentMode) {
            case 'DeferredTurnoverBusiness':
                this.riExchange.getParentHTMLValue('BusinessCode');
                this.riExchange.getParentHTMLValue('BusinessDesc');
                if (this.riExchange.getParentHTMLValue('RegionCode')) {
                    this.riExchange.getParentHTMLValue('RegionCode');
                } else {
                    this.riExchange.getParentHTMLValue('BranchNumber');
                }
                this.type = 'business';
                break;

            case 'DeferredTurnoverRegion':
                this.riExchange.getParentHTMLValue('RegionCode');
                this.riExchange.getParentHTMLValue('RegionDesc');
                this.type = 'region';
                break;
            case 'DeferredTurnoverBranch':
                this.riExchange.getParentHTMLValue('BranchNumber');
                this.riExchange.getParentHTMLValue('BranchName');
                this.type = 'branch';
                break;
            case 'DeferredTurnoverBranchTotals':
                this.setControlValue('BranchNumber', '0');
                this.setControlValue('BranchName', 'Totals');
                this.type = 'branch';
                break;
            case 'DeferredTurnoverRegionTotals':
                this.setControlValue('RegionCode', '0');
                this.setControlValue('RegionDesc', 'Totals');
                this.type = 'region';
                break;
        }

    }

    private buildGrid(): void {
        if (this.pageParams.SCEnableCompanyCode && !this.getControlValue('CompanyCode')) {
            this.riGrid.AddColumn('CompanyCode', 'DeferredTurnover', 'CompanyCode', MntConst.eTypeText, 10, false);
            this.riGrid.AddColumnAlign('CompanyCode', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('InvoiceNumber', 'DeferredTurnover', 'InvoiceNumber', MntConst.eTypeText, 10, false);
        this.riGrid.AddColumnAlign('InvoiceNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ContractNumber', 'DeferredTurnover', 'ContractNumber', MntConst.eTypeText, 8, false);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'DeferredTurnover', 'PremiseNumber', MntConst.eTypeInteger, 5, false);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'DeferredTurnover', 'PremiseName', MntConst.eTypeText, 20, false);
        this.riGrid.AddColumn('ProductCode', 'DeferredTurnover', 'ProductCode', MntConst.eTypeCode, 6, false);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ExpenseCode', 'DeferredTurnover', 'ExpenseCode', MntConst.eTypeCode, 7, false);
        this.riGrid.AddColumnAlign('ExpenseCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('TurnoverValue', 'DeferredTurnover', 'TurnoverValue', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumnAlign('TurnoverValue', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('BranchNumber', 'DeferredTurnover', 'BranchNumber', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumn('BranchName', 'DeferredTurnover', 'BranchName', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumnScreen('BranchNumber', false);
        this.riGrid.AddColumnScreen('BranchName', false);

        this.riGrid.Complete();
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableCompanyCode].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.SCEnableCompanyCode = data.records[0].Required;
            if (this.pageParams.SCEnableCompanyCode) {
                this.riExchange.getParentHTMLValue('CompanyCode');
                this.riExchange.getParentHTMLValue('CompanyDesc');
            }
            this.buildGrid();
            this.populateGrid();
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
                this.buildGrid();
                this.populateGrid();
            });
    }

    private populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData: Object = {
                'Level': 'DetailGrid',
                'BusinessCode': this.utils.getBusinessCode(),
                'Filter': this.getControlValue('MonthNumber'),
                'Branch': this.getControlValue('BranchNumber'),
                'Region': this.getControlValue('RegionCode'),
                'CompanyCode': this.getControlValue('CompanyCode'),
                'CountryCode': ''
            };
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Application/iCABSADeferredTurnoverDetailGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = true;
                        this.displayMessage(data);
                    } else {
                        this.riGrid.RefreshRequired();
                        this.hasGridData = true;
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = true;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }
}

