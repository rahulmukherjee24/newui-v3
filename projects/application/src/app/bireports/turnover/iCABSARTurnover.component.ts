import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { BCompanySearchComponent } from '@internal/search/iCABSBCompanySearch';
import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';
import { StaticUtils } from '@shared/services/static.utility';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSARTurnover.html',
    providers: [CommonLookUpUtilsService]
})
export class TurnoverComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('companyDropdown') companyDropdown: BCompanySearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('serviceTypeSearch') serviceTypeSearch: ServiceTypeSearchComponent;

    protected pageId: string;

    public bCode = this.businessCode();
    public dropdownDisable: boolean = false;
    public companyDropdownDisable: boolean = false;
    public hasGridData: boolean = false;
    public pageType: string;
    public showInnovationMetric: boolean = false;

    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARTurnoverBusiness'
    };

    public dropdown: any = {
        company: {
            params: {
                businessCode: this.bCode,
                countryCode: this.countryCode(),
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        },
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        },
        marketSegment: {
            inputParams: {
                operation: 'System/iCABSSMarketSegmentSearch',
                module: 'segmentation',
                method: 'ccm/search'
            },
            displayColumns: ['MarketSegment.MarketSegmentCode', 'MarketSegment.MarketSegmentDesc'],
            active: { id: '', text: '' }
        }
    };

    protected controls: Array<Object> = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'CompanyCode', type: MntConst.eTypeCode },
        { name: 'CompanyDesc', type: MntConst.eTypeText },
        { name: 'InnovationMetricInd', type: MntConst.eTypeCheckBox },
        { name: 'MarketSegmentCode', type: MntConst.eTypeCode },
        { name: 'MarketSegmentDesc', type: MntConst.eTypeText },
        { name: 'MonthFrom', required: true, type: MntConst.eTypeInteger },
        { name: 'MonthTo', required: true, type: MntConst.eTypeInteger },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText },
        { name: 'ViewType', type: MntConst.eTypeText },
        { name: 'YearFrom', required: true, type: MntConst.eTypeInteger },
        { name: 'YearTo', required: true, type: MntConst.eTypeInteger }
    ];

    public viewTypeBusiness: any = [
        { value: 'Branch', text: 'Branch' },
        { value: 'Region', text: 'Region' },
        { value: 'Product', text: 'Product' },
        { value: 'Expense', text: 'Expense Code' },
        { value: 'Service', text: 'Service Type' },
        { value: 'MarketSegment', text: 'Market Segment' }
    ];

    public viewTypeBranch: any = [
        { value: 'Product', text: 'Product' },
        { value: 'Expense', text: 'Expense Code' },
        { value: 'Service', text: 'Service Type' },
        { value: 'MarketSegment', text: 'Market Segment' }
    ];

    public gridConfig: any = {
        totalItem: 1,
        itemsPerPage: 10
    };

    constructor(injector: Injector, public sysCharConstants: SysCharConstants, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.browserTitle = this.pageTitle = 'Turnover';
        this.utils.setTitle(this.pageTitle);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('type'));
        let filterby = this.riExchange.getParentHTMLValue('filterby');
        this.pageId = PageIdentifier['ICABSARTURNOVER' + this.pageType.toUpperCase() + (filterby ? filterby : '').toUpperCase()];

        super.ngAfterContentInit();
        this.loadSysChars();

        this.pageParams.viewTypeOptions = this['viewType' + this.pageType] || [];
        this.setControlValue('ViewType', this.getControlValue('ViewType') ? this.getControlValue('ViewType') : this.pageType === 'Business' ? 'Branch' : 'Expense');

        if (this.pageType === 'Branch') {
            // tslint:disable-next-line: no-unused-expression
            this.riExchange.getParentHTMLValue('BranchNumber') || this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.commonLookup.getBranchName(this.getControlValue('BranchNumber')).then(data => {
                if (data && data[0] && data[0][0]) {
                    this.setControlValue('BranchName', data[0][0].BranchName);
                }
            }).catch(error => {
                this.displayMessage(error);
            });
        }

        if (this.isReturning()) {
            if (this.getControlValue('CompanyDesc')) {
                this.dropdown.company.active = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            if (this.getControlValue('ServiceTypeCode')) {
                this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
            }
            if (this.getControlValue('MarketSegmentDesc')) {
                this.dropdown.marketSegment.active = {
                    id: this.getControlValue('MarketSegmentCode'),
                    text: this.getControlValue('MarketSegmentCode') + ' - ' + this.getControlValue('MarketSegmentDesc')
                };
            }
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.populateDate();
            this.setControlValue('BusinessCode', this.bCode);
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        }

        if (this.parentMode) {
            this.riExchange.getParentHTMLValue('MonthFrom');
            this.riExchange.getParentHTMLValue('MonthTo');
            this.riExchange.getParentHTMLValue('YearFrom');
            this.riExchange.getParentHTMLValue('YearTo');
            if (this.riExchange.getParentHTMLValue('CompanyCode')) {
                this.dropdown.company.active = {
                    id: this.riExchange.getParentHTMLValue('CompanyCode'),
                    text: this.riExchange.getParentHTMLValue('CompanyCode') + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc')
                };
            }
            if (this.riExchange.getParentHTMLValue('ServiceTypeCode')) {
                this.setControlValue('ServiceTypeDesc', this.riExchange.getParentAttributeValue('ServiceDesc') || this.riExchange.getParentHTMLValue('ServiceTypeDesc'));
                this.serviceTypeSearch.setValue(this.riExchange.getParentHTMLValue('ServiceTypeCode'));
            }
            if (this.riExchange.getParentHTMLValue('MarketSegmentCode')) {
                this.riExchange.getParentHTMLValue('MarketSegmentCode');
                this.dropdown.marketSegment.active = {
                    id: this.riExchange.getParentHTMLValue('MarketSegmentCode'),
                    text: this.riExchange.getParentHTMLValue('MarketSegmentCode') + ' - ' + this.riExchange.getParentHTMLValue('MarketSegmentDesc')
                };
            }
            this.dropdownDisable = !(filterby === 'service' || filterby === 'market');
            this.companyDropdownDisable = true;
            this.disableControl('MonthFrom', true);
            this.disableControl('MonthTo', true);
            this.disableControl('YearFrom', true);
            this.disableControl('YearTo', true);
            if (!this.isReturning) {
                this.setControlValue('ViewType', 'Expense');
            }
        }

        this.buildGrid();
        if (this.isReturning() || this.parentMode) {
            this.onRiGridRefresh();
        }

    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        switch (this.getControlValue('ViewType')) {
            case 'Region':
                this.riGrid.AddColumn('GroupCode', 'Turnover', 'GroupCode', MntConst.eTypeText, 2, true);
                break;
            case 'Branch':
                this.riGrid.AddColumn('GroupCode', 'Turnover', 'GroupCode', MntConst.eTypeText, 3, true);
                break;
            case 'Product':
                this.riGrid.AddColumn('GroupCode', 'Turnover', 'GroupCode', MntConst.eTypeText, 8);
                this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
                break;
            case 'Expense':
                this.riGrid.AddColumn('GroupCode', 'Turnover', 'GroupCode', MntConst.eTypeText, 6);
                this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
                break;
            case 'Service':
                this.riGrid.AddColumn('GroupCode', 'Turnover', 'GroupCode', MntConst.eTypeText, 6, true);
                break;
            case 'MarketSegment':
                this.riGrid.AddColumn('GroupCode', 'Turnover', 'GroupCode', MntConst.eTypeText, 5, true);
                this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
                break;
        }

        this.riGrid.AddColumn('GroupDesc', 'Turnover', 'GroupDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('BFWD', 'Turnover', 'BFWD', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('BFWD', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('INVOICED', 'Turnover', 'INVOICED', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('INVOICED', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('RELEASED', 'Turnover', 'RELEASED', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('RELEASED', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('CFWD', 'Turnover', 'CFWD', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('CFWD', MntConst.eAlignmentRight);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = this.pageType;
        formData[this.serviceConstants.BusinessCode] = this.bCode;
        if (this.pageType === 'Branch') {
            formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        }
        formData['ViewType'] = this.getControlValue('ViewType');
        formData['YearFrom'] = this.getControlValue('YearFrom');
        formData['MonthFrom'] = this.getControlValue('MonthFrom');
        formData['YearTo'] = this.getControlValue('YearTo');
        formData['MonthTo'] = this.getControlValue('MonthTo');
        formData['CompanyCode'] = this.getControlValue('CompanyCode');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['MarketSegmentCode'] = this.getControlValue('MarketSegmentCode');
        formData['InnovationMetricInd'] = this.getControlValue('InnovationMetricInd');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                this.setPagination();
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    private setPagination(): void {
        setTimeout(() => {
            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
        }, 100);
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.riGrid.RefreshRequired();
            this.populateGrid();
        }
    }

    public getCurrentPage(currentPage: any): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            super.getCurrentPage(currentPage);
        } else {
            this.setPagination();
        }
    }

    public onGridBodyDoubleClick(_event?: any): void {
        let rowid: string = this.riGrid.Details.GetAttribute('GroupCode', 'rowid');

        let columnName = this.riGrid.CurrentColumnName.toLowerCase();
        let viewTypeVal = this.getControlValue('ViewType');
        let columnCode: any;
        let params: any = {};

        if ((columnName === 'invoiced' || columnName === 'released') && this.pageType === 'Branch') {
            let lTotalsDrill = this.riGrid.Details.GetValue('GroupDesc') === 'Totals';
            if (viewTypeVal === 'Expense' || viewTypeVal === 'Product' || !lTotalsDrill) {
                columnCode = this.riGrid.Details.GetValue('GroupCode');
            } else if (viewTypeVal === 'MarketSegment' || viewTypeVal === 'Service' || !lTotalsDrill) {
                columnCode = this.riGrid.Details.GetAttribute('GroupCode', 'additionalproperty');
            }
            params['ColumnCode'] = columnCode;
            params['ColumnDesc'] = this.riGrid.Details.GetValue('GroupDesc');
            params['ColumnClicked'] = columnName;

            this.navigate((lTotalsDrill ? 'BranchTotal' : 'Branch'), BIReportsRoutes.ICABSARBRANCHTURNOVERDETAILGRID, params);
        } else if (this.riGrid.CurrentColumnName === 'GroupCode' && rowid !== 'Total') {
            switch (viewTypeVal) {
                case 'Branch':
                    this.navigate('business', BIReportsRoutes.ICABSARTURNOVERBRANCH, {
                        type: 'Branch',
                        BranchName: this.riGrid.Details.GetValue('GroupDesc'),
                        BranchNumber: this.riGrid.Details.GetValue('GroupCode')
                    });
                    break;
                case 'Region':
                    break;
                case 'MarketSegment':
                    this.navigate(this.pageType.toLowerCase(), BIReportsRoutes.ICABSARTURNOVERBUSINESSMARKET, {
                        type: this.pageType,
                        filterby: 'market',
                        MarketSegmentCode: this.riGrid.Details.GetValue('GroupCode'),
                        MarketSegmentDesc: this.riGrid.Details.GetValue('GroupDesc')
                    });
                    break;
                case 'Service':
                    this.navigate(this.pageType.toLowerCase(), BIReportsRoutes.ICABSARTURNOVERBUSINESSSERVICE, {
                        type: this.pageType,
                        filterby: 'service',
                        ServiceTypeCode: this.riGrid.Details.GetValue('GroupCode'),
                        ServiceDesc: this.riGrid.Details.GetValue('GroupDesc')
                    });
                    break;
            }
        }
    }

    private populateDate(): void {
        if (this.parentMode) {
            return;
        }
        this.commonLookup.getTurnoverTradingDates(this.bCode).then(data => {
            let salesTradingMonth = data && data[0] && data[0].length ? data[0][0].TurnoverTradingMonth : new Date().getMonth() + 1;
            let salesTradingYear = data && data[0] && data[0].length ? data[0][0].TurnoverTradingYear : new Date().getMonth() + 1;
            this.setControlValue('MonthFrom', salesTradingMonth);
            this.setControlValue('YearFrom', salesTradingYear);
            this.setControlValue('MonthTo', salesTradingMonth);
            this.setControlValue('YearTo', salesTradingYear);
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onMarketSegmentSelect(data: any): void {
        this.setControlValue('MarketSegmentCode', data['MarketSegment.MarketSegmentCode']);
        this.setControlValue('MarketSegmentDesc', data['MarketSegment.MarketSegmentDesc']);
    }

    public onServiceTypeChange(data: any): void {
        if (data) {
            this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
            this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        }
    }

    public onViewTypeChange(): void {
        this.showInnovationMetric = (this.getControlValue('ViewType') === 'Product');
    }

    public onCompanyChange(data: any): void {
        if (data) {
            this.setControlValue('CompanyCode', data.CompanyCode);
            this.setControlValue('CompanyDesc', data.CompanyDesc);
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let sysCharQuery: QueryParams = new QueryParams();
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableCompanyCode
        ];
        sysCharQuery.set(this.serviceConstants.Action, '0');
        sysCharQuery.set(this.serviceConstants.BusinessCode, this.bCode);
        sysCharQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        sysCharQuery.set(this.serviceConstants.SystemCharNumber, syscharList);
        this.httpService.sysCharRequest(sysCharQuery).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            try {
                if (data && data.records && data.records.length) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                }
            } catch (error) {
                this.displayMessage(error);
            }
        });
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
