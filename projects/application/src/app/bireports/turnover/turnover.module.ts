import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@app/base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

import { TurnoverComponent } from './iCABSARTurnover.component';
import { BranchTurnoverDetailGridComponent } from './iCABSARBranchTurnoverDetailGrid.component';
import { DeferredTurnoverBusinessGridComponent } from './iCABSADeferredTurnoverBusinessGrid.component';
import { DeferredTurnoverDetailGridComponent } from './iCABSADeferredTurnoverDetailGrid.component';

@Component({
    template: `<router-outlet></router-outlet>
    `
})
export class TurnoverRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        HttpClientModule,
        RouterModule.forChild([
            {
                path: '', component: TurnoverRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARTURNOVERBUSINESS, component: TurnoverComponent },
                    { path: BIReportsRoutesConstant.ICABSARTURNOVERBUSINESSMARKET, component: TurnoverComponent },
                    { path: BIReportsRoutesConstant.ICABSARTURNOVERBUSINESSSERVICE, component: TurnoverComponent },
                    { path: BIReportsRoutesConstant.ICABSARTURNOVERBRANCH, component: TurnoverComponent },
                    { path: BIReportsRoutesConstant.ICABSARTURNOVERBRANCHMARKET, component: TurnoverComponent },
                    { path: BIReportsRoutesConstant.ICABSARTURNOVERBRANCHSERVICE, component: TurnoverComponent },
                    { path: BIReportsRoutesConstant.ICABSARBRANCHTURNOVERDETAILGRID, component: BranchTurnoverDetailGridComponent },
                    { path: BIReportsRoutesConstant.ICABSADEFERREDTURNOVERBUSINESSGRID, component: DeferredTurnoverBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSADEFERREDTURNOVERBRANCHGRID, component: DeferredTurnoverBusinessGridComponent },
                    { path: BIReportsRoutesConstant.ICABSADEFERREDTURNOVERDETAILGRID, component: DeferredTurnoverDetailGridComponent }
                ], data: { domain: 'TURNOVER' }
            }

        ])
    ],
    declarations: [
        TurnoverRootComponent,
        TurnoverComponent,
        BranchTurnoverDetailGridComponent,
        DeferredTurnoverBusinessGridComponent,
        DeferredTurnoverDetailGridComponent
    ]

})

export class TurnoverModule {
}
