import { AfterContentInit, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { DropdownConstants } from '@app/base/ComponentConstants';
import { IExportOptions } from '@app/base/ExportConfig';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@app/base/PageRoutes';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { AccountSearchComponent } from '@app/internal/search/iCABSASAccountSearch';
import { BranchSearchComponent } from '@app/internal/search/iCABSBBranchSearch';
import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';
import { VisitTypeSearchComponent } from '@app/internal/search/iCABSBVisitTypeSearch.component';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { MessageConstant } from '@shared/constants/message.constant';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { SpeedScript } from '@shared/services/speedscript';

@Component({
    templateUrl: 'iCABSSeVisitTypeGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class VisitTypeGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('serviceTypeSearch') serviceTypeSearch: ServiceTypeSearchComponent;
    @ViewChild('visitTypeSearch') visitTypeSearch: VisitTypeSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;
    protected controls: any = [
        { name: 'BranchServiceAreaCode',required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname',required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode',required: false, type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc',required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'DateFrom',required: true, type: MntConst.eTypeDate },
        { name: 'DateTo',required: true, type: MntConst.eTypeDate },
        { name: 'VisitTypeCodeString',required: false, type: MntConst.eTypeCode },
        { name: 'NumOfVisits',required: true, type: MntConst.eTypeInteger },
        { name: 'BranchNumber',required: false, type: MntConst.eTypeInteger },
        { name: 'BranchName',required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'AccountNumber',required: false, type: MntConst.eTypeText },
        { name: 'AccountName',required: true, disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber',required: false, type: MntConst.eTypeText },
        { name: 'ContractName',required: true, disabled: true, type: MntConst.eTypeText }
    ];

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public exportConfig: IExportOptions = {};

    private xhrParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSeVisitTypeGrid'
    };

    public dropdown: any = {
        branchParams: {
            parentMode: 'LookUp'
        },
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        },
        visitTypeSearch: {
            params: {
                parentMode: 'LookUp-String'
            },
            active: { id: '', text: '' }
        }
    };

    public ellipsis: any = {
        accountNumber: {
            childParams: {
                parentMode: 'LookUp',
                showAddNew: false,
                showAddNewDisplay: false,
                showBusinessCode: false,
                showCountryCode: false
            },
            component: AccountSearchComponent
        },
        serviceArea: {
            isDisabled: false,
            childParams: {
                parentMode: 'LookUp',
                ServiceBranchNumber: '',
                BranchNumberServiceBranchNumber: '',
                BranchName: ''
            },
            component: BranchServiceAreaSearchComponent
        },
        contract: {
            childParams: {
                parentMode: 'LookUp',
                accountNumber: '',
                enableAccountSearch: false,
                showCountry: false,
                showBusiness: false,
                showBranch: false
            },
            component: ContractSearchComponent
        }
    };

    constructor(injector: Injector,public sysCharConstants: SysCharConstants, public speedScript: SpeedScript, private commonLookUp: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        this.browserTitle = this.pageTitle = 'Service Visits By Type';
        this.pageId = PageIdentifier.ICABSSEVISITTYPEGRID;
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (this.isReturning()) {
            if (this.getControlValue('BranchNumber')) {
                this.branchSearchDropDown.active = {
                    id: this.getControlValue('BranchNumber'), text: this.getControlValue('BranchNumber') + ' - ' + this.getControlValue('BranchName')
                };
                this.ellipsis.serviceArea.childParams.ServiceBranchNumber = this.getControlValue('BranchNumber');
                this.ellipsis.serviceArea.childParams.BranchNumberServiceBranchNumber = this.getControlValue('BranchNumber');
                this.ellipsis.serviceArea.childParams.BranchName = this.getControlValue('BranchName');
            } else {
                this.ellipsis.serviceArea.isDisabled = true;
                this.disableControl('BranchServiceAreaCode', true);
            }
            if (this.getControlValue('BranchName') === 'ALL BRANCHES') {
                this.branchSearchDropDown.active = {
                    id: this.getControlValue('BranchNumber'), text: this.getControlValue('BranchName')
                };
            }
            if (this.getControlValue('VisitTypeCodeString')) {
                this.dropdown.visitTypeSearch.active = {
                    id: '', text: '', multiSelectDisplay: this.getControlValue('VisitTypeCodeString')
                };
            }

            if (this.getControlValue('ServiceTypeCode')) {
                this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
            }
            if (this.getControlValue('AccountNumber')) {
                this.ellipsis.contract.childParams.accountNumber = this.getControlValue('AccountNumber');
                this.ellipsis.contract.childParams.enableAccountSearch = true;
            }
            this.pageParams.gridCacheRefresh = false;
            this.onRiGridRefresh();
        } else {
            let date = new Date();
            this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth(), 1)));
            this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date));
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.utils.getBranchCode()));
            this.setControlValue('NumOfVisits', 1);
            this.branchSearchDropDown.active = {
                id: this.utils.getBranchCode(), text: this.utils.getBranchCode() + ' - ' + this.utils.getBranchTextOnly(this.utils.getBranchCode())
            };
            this.ellipsis.serviceArea.childParams.ServiceBranchNumber = this.getControlValue('BranchNumber');
            this.ellipsis.serviceArea.childParams.BranchNumberServiceBranchNumber = this.getControlValue('BranchNumber');
            this.ellipsis.serviceArea.childParams.BranchName = this.getControlValue('BranchName');
            this.pageParams.gridConfig = {
                pageSize: 10,
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true
            };
            this.pageParams.gridCurrentPage = 1;
            this.getSysCharDetail();
        }
    }

    private getSysCharDetail(): void {
        let sysCharSearch = {
            module: this.xhrParams.module,
            operation: this.xhrParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: this.sysCharConstants.SystemCharEnableServiceCoverDisplayLevel.toString()
        };
        this.speedScript.sysCharPromise(sysCharSearch).then((data) => {
            this.pageParams.vSCEnableServiceCoverDispLev = data.records[0]['Required']; // 3360
        });
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ServiceVisitText','ServiceVisit','ServiceVisitText',MntConst.eTypeText,10);
        this.riGrid.AddColumnAlign('ServiceVisitText',MntConst.eAlignmentLeft);
        this.riGrid.AddColumnScreen('ServiceVisitText',false);
        this.riGrid.AddColumn('VisitNarrativeCode','ServiceVisit','VisitNarrativeCode',MntConst.eTypeCode,8);
        this.riGrid.AddColumnAlign('VisitNarrativeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('VisitNarrativeCode',false);
        this.riGrid.AddColumn('VisitNarrativeDesc','ServiceVisit','VisitNarrativeDesc',MntConst.eTypeText,10);
        this.riGrid.AddColumnAlign('VisitNarrativeDesc',MntConst.eAlignmentLeft);
        this.riGrid.AddColumnScreen('VisitNarrativeDesc',false);
        this.riGrid.AddColumn('ServiceBranchNumber','ServiceVisit','ServiceBranchNumber',MntConst.eTypeCode,3);
        this.riGrid.AddColumnAlign('ServiceBranchNumber',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaCode','ServiceVisit','BranchServiceAreaCode',MntConst.eTypeCode,4);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaSeqNo','ServiceVisit','BranchServiceAreaSeqNo',MntConst.eTypeText,6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ContractPremise','ServiceVisit','ContractPremise',MntConst.eTypeText,14);
        this.riGrid.AddColumnAlign('ContractPremise',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName','ServiceVisit','PremiseName',MntConst.eTypeText,30);
        this.riGrid.AddColumnAlign('PremiseName',MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ContactDetails','ServiceVisit','ContactDetails',MntConst.eTypeText,20);
        this.riGrid.AddColumnScreen('ContactDetails',false);
        this.riGrid.AddColumn('ProductCode','ServiceVisit','ProductCode',MntConst.eTypeCode,8,true);
        this.riGrid.AddColumnAlign('ProductCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceCoverDetail','ServiceVisit','ServiceCoverDetail',MntConst.eTypeText,5);
        this.riGrid.AddColumnAlign('ServiceCoverDetail',MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ServiceVisitFrequency','ServiceVisit','ServiceVisitFrequency',MntConst.eTypeInteger,4);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency',MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('ServiceVisitFrequency',false);
        this.riGrid.AddColumn('ServiceQuantity','ServiceVisit','ServiceQuantity',MntConst.eTypeInteger,4);
        this.riGrid.AddColumnAlign('ServiceQuantity',MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('ServiceQuantity',false);
        this.riGrid.AddColumn('ServiceTypeCode','ServiceVisit','ServiceTypeCode',MntConst.eTypeCode,2);
        this.riGrid.AddColumnAlign('ServiceTypeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitTypeCode','ServiceVisit','VisitTypeCode',MntConst.eTypeCode,4,true);
        this.riGrid.AddColumnAlign('VisitTypeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServicedQuantity','ServiceVisit','ServicedQuantity',MntConst.eTypeCode,4);
        this.riGrid.AddColumnAlign('ServicedQuantity',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceDateStart','ServiceVisit','ServiceDateStart',MntConst.eTypeDate,10);
        this.riGrid.AddColumnAlign('ServiceDateStart',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceDateEnd','ServiceVisit','ServiceDateEnd',MntConst.eTypeDate,10);
        this.riGrid.AddColumnAlign('ServiceDateEnd',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('ServiceDateEnd',false);
        this.riGrid.AddColumn('ServiceCommenceDate','ServiceVisit','ServiceCommenceDate',MntConst.eTypeDate,10);
        this.riGrid.AddColumnAlign('ServiceCommenceDate',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Duration','ServiceVisit','Duration',MntConst.eTypeDuration,6);
        this.riGrid.AddColumnAlign('Duration',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeCode','ServiceVisit','EmployeeCode',MntConst.eTypeCode,6);
        this.riGrid.AddColumnAlign('EmployeeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('AdditionalChargeValue','ServiceVisit','AdditionalChargeValue',MntConst.eTypeDecimal2,6);
        this.riGrid.AddColumnAlign('AdditionalChargeValue',MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('AdditionalChargeValue',false);
        this.riGrid.AddColumn('InvoiceSuspendInd','ServiceVisit','InvoiceSuspendInd',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('InvoiceSuspendInd',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('InvoiceSuspendInd',false);
        this.riGrid.AddColumn('InvoiceUnitValue','ServiceVisit','InvoiceUnitValue',MntConst.eTypeDecimal2,6);
        this.riGrid.AddColumnAlign('InvoiceUnitValue',MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('InvoiceUnitValue',false);
        this.riGrid.AddColumn('NoServiceReasonCode','ServiceVisit','NoServiceReasonCode',MntConst.eTypeCode,2);
        this.riGrid.AddColumnAlign('NoServiceReasonCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('NoServiceReasonCode',false);
        this.riGrid.AddColumn('NoServiceReasonDesc','ServiceVisit','NoServiceReasonDesc',MntConst.eTypeText,10);
        this.riGrid.AddColumnAlign('NoServiceReasonDesc',MntConst.eAlignmentLeft);
        this.riGrid.AddColumnScreen('NoServiceReasonDesc',false);
        if (this.pageParams.vSCEnableServiceCoverDispLev) {
            this.riGrid.AddColumn('PremiseLocationNumber','ServiceVisit','PremiseLocationNumber',MntConst.eTypeInteger,4);
            this.riGrid.AddColumnAlign('PremiseLocationNumber',MntConst.eAlignmentCenter);
            this.riGrid.AddColumnScreen('PremiseLocationNumber',false);
            this.riGrid.AddColumn('PremiseLocationDesc','ServiceVisit','PremiseLocationDesc',MntConst.eTypeText,10);
            this.riGrid.AddColumnAlign('PremiseLocationDesc',MntConst.eAlignmentCenter);
            this.riGrid.AddColumnScreen('PremiseLocationDesc',false);
            this.riGrid.AddColumn('ProductComponentCode','ServiceVisit','ProductComponentCode',MntConst.eTypeCode,8);
            this.riGrid.AddColumnAlign('ProductComponentCode',MntConst.eAlignmentCenter);
            this.riGrid.AddColumnScreen('ProductComponentCode',false);
            this.riGrid.AddColumn('ProductComponentDesc','ServiceVisit','ProductComponentDesc',MntConst.eTypeCode,10);
            this.riGrid.AddColumnAlign('ProductComponentDesc',MntConst.eAlignmentLeft);
            this.riGrid.AddColumnScreen('ProductComponentDesc',false);
            this.riGrid.AddColumn('ProductComponentRemoved','ServiceVisit','ProductComponentRemoved',MntConst.eTypeCode,8);
            this.riGrid.AddColumnAlign('ProductComponentRemoved',MntConst.eAlignmentCenter);
            this.riGrid.AddColumnScreen('ProductComponentRemoved',false);
            this.riGrid.AddColumn('ProductComponentRemDesc','ServiceVisit','ProductComponentDesc',MntConst.eTypeCode,10);
            this.riGrid.AddColumnAlign('ProductComponentRemDesc',MntConst.eAlignmentLeft);
            this.riGrid.AddColumnScreen('ProductComponentRemDesc',false);
        }
        this.riGrid.AddColumn('SharedInd','ServiceVisit','SharedInd',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('SharedInd',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('SharedInd',false);
        this.riGrid.AddColumn('SignatureRecordedInd','ServiceVisit','SignatureRecordedInd',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('SignatureRecordedInd',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('SignatureRecordedInd',false);
        this.riGrid.AddColumn('StandardDuration','ServiceVisit','StandardDuration',MntConst.eTypeDuration,5);
        this.riGrid.AddColumnAlign('StandardDuration',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('StandardDuration',false);
        this.riGrid.AddColumn('VisitReference','ServiceVisit','VisitReference',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('VisitReference',MntConst.eAlignmentLeft);
        this.riGrid.AddColumnScreen('VisitReference',false);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo',true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaCode',true);
        this.riGrid.AddColumnOrderable('VisitTypeCode',true);
        this.riGrid.AddColumnOrderable('ServiceDateStart',true);
        this.riGrid.AddColumnOrderable('ServiceCommenceDate',true);
        this.riGrid.AddColumnOrderable('EmployeeCode',true);
        this.riGrid.Complete();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexListFromFull([
            'ServiceDateStart',
            'ServiceDateEnd',
            'ServiceCommenceDate'
        ]);
        this.durationCollist = this.riGrid.getColumnIndexListFromFull([
            'Duration',
            'StandardDuration'
        ]);

        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
        if (this.durationCollist.length) {
            this.exportConfig.durationColumns = this.durationCollist;
        }
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
      }

    public dropdownDataReceived(data: any, type: string): void {
        switch (type) {
            case 'ServiceBranch':
                if (data) {
                    this.setControlValue('BranchNumber', data.BranchNumber);
                    this.setControlValue('BranchName', data.BranchName);
                    this.setControlValue('BranchServiceAreaCode', '');
                    this.setControlValue('EmployeeSurname', '');
                    this.ellipsis.serviceArea.childParams.BranchNumberServiceBranchNumber = data.BranchNumber;
                    this.ellipsis.serviceArea.childParams.ServiceBranchNumber = data.BranchNumber;
                    this.ellipsis.serviceArea.childParams.BranchName = data.BranchName;
                    this.ellipsis.serviceArea.isDisabled = data.BranchNumber ? false : true;
                    this.disableControl('BranchServiceAreaCode', data.BranchNumber ? false : true);
                }
            break;
            case 'ServiceType':
                if (data && data.ServiceTypeCode) {
                    this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
                    this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
                }
            break;
            case 'ServiceVisit':
                if (data) {
                    this.setControlValue('VisitTypeCodeString', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
                }
            break;
            default:
            break;
        }
        this['commonGridFunction'].resetGrid();
    }

    public serviceAreaChange(): void {
        this['commonGridFunction'].resetGrid();
        this.setControlValue('EmployeeSurname', '');
            if (!this.getControlValue('BranchServiceAreaCode')) {
                return;
            }
            this.commonLookUp.getEmpSurNameFromBranchArea(this.getControlValue('BranchServiceAreaCode'), this.getControlValue('BranchNumber'))
            .subscribe(data => {
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
            },
            err => {
                this.setControlValue('BranchServiceAreaCode', '');
                this.displayMessage(MessageConstant.Message.RecordNotFound);
            });
    }

    public accounNumChange(): void {
        this['commonGridFunction'].resetGrid();
        if (!this.getControlValue('AccountNumber')) {
            this.setControlValue('AccountNumber', '');
            this.setControlValue('AccountName', '');
            this.setControlValue('ContractNumber', '');
            this.setControlValue('ContractName', '');
            this.ellipsis.contract.childParams.accountNumber = '';
            this.ellipsis.contract.childParams.enableAccountSearch = false;
            return;
        }
        this.commonLookUp.getAccountName(this.getControlValue('AccountNumber'))
            .then(data => {
                if (data && data[0] && data[0].length) {
                    this.setControlValue('AccountName', data[0][0]['AccountName']);
                    this.ellipsis.contract.childParams.accountNumber = this.getControlValue('AccountNumber');
                    this.ellipsis.contract.childParams.enableAccountSearch = true;
                } else {
                    this.displayMessage(MessageConstant.Message.RecordNotFound);
                    this.setControlValue('AccountNumber', '');
                    this.setControlValue('AccountName', '');
                    this.ellipsis.contract.childParams.accountNumber = '';
                    this.ellipsis.contract.childParams.enableAccountSearch = false;
                    this.setControlValue('ContractNumber', '');
                    this.setControlValue('ContractName', '');
                }
            })
            .catch(error => {
                this.displayMessage(error);
            });
    }

    public contractNumChange(): void {
        this['commonGridFunction'].resetGrid();
        if (!this.getControlValue('ContractNumber')) {
            this.setControlValue('ContractName', '');
            return;
        }
        this.commonLookUp.getContractName(this.getControlValue('ContractNumber'))
            .then(data => {
                if (data && data[0] && data[0].length) {
                    this.setControlValue('ContractName', data[0][0]['ContractName']);
                } else {
                    this.displayMessage(MessageConstant.Message.RecordNotFound);
                    this.setControlValue('ContractNumber', '');
                    this.setControlValue('ContractName', '');
                }
            })
            .catch(error => {
                this.displayMessage(error);
            });
    }

    public onEllipsisDataReceived(data: any, type: string): void {
        switch (type) {
            case 'AccountNumber':
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.setControlValue('AccountName', data.AccountName);
                this.ellipsis.contract.childParams.accountNumber = data.AccountNumber;
                this.ellipsis.contract.childParams.enableAccountSearch = true;
                this.setControlValue('ContractNumber', '');
                this.setControlValue('ContractName', '');
            break;
            case 'ServiceAreaCode':
                this.setControlValue('BranchServiceAreaCode',data.BranchServiceAreaCode);
                this.setControlValue('EmployeeSurname',data.EmployeeSurname);
            break;
            case 'Contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.setControlValue('AccountName', data.AccountName);
                this.ellipsis.contract.childParams.accountNumber = data.AccountNumber;
                this.ellipsis.contract.childParams.enableAccountSearch = true;
            break;
            default:
            break;
        }
        this['commonGridFunction'].resetGrid();
    }

    public populateGrid(): void {
        let formArray = ['BranchServiceAreaCode','ServiceTypeCode','DateFrom','DateTo','VisitTypeCodeString','NumOfVisits','BranchNumber','AccountNumber','ContractNumber'];
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let formData: any = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '2');
        formArray.forEach(control => {
            formData = {
                ...formData,
                [control]: this.getControlValue(control)
            };
        });
        if (this.getControlValue('AccountNumber') === '' && this.getControlValue('ContractNumber') === '' && this.getControlValue('BranchNumber') === '') {
            this.setControlValue('BranchName', 'ALL BRANCHES');
            this.branchSearchDropDown.active = {
                id: this.getControlValue('BranchNumber'), text: this.getControlValue('BranchName')
            };
        }
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.PageSize] = '10';
        formData['BusinessCode'] = this.businessCode();
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.hasGridData = true;
            this.commonGridFunction.setPageData(data);
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.displayMessage(error);
            });
    }

    public riGridBodyOnDblClick(): void {
        let columnName = this.riGrid.CurrentColumnName;
        let RowID = this.riGrid.Details.GetAttribute(columnName, 'rowid');
        this.setAttribute('RowID',RowID);
        if (RowID) {
            switch (columnName) {
                case 'ProductCode':
                    this.navigate('ServiceVisit', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT,{
                        ServiceCoverRowID: RowID
                    });
                break;
                case 'VisitTypeCode':
                    this.navigate('ServiceVisit', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                        ServiceVisitRowID: RowID
                    });
                break;
                default:
                break;
            }
        }
    }
}
