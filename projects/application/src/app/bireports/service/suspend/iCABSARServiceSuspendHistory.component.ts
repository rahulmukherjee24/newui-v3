import { BranchSearchComponent } from '@app/internal/search/iCABSBBranchSearch';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { Component, OnInit, Injector, ViewChild, AfterContentInit } from '@angular/core';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { DatepickerComponent } from '@shared/components/datepicker/datepicker';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { PremiseSearchComponent } from '@app/internal/search/iCABSAPremiseSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceCoverSearchComponent } from '@app/internal/search/iCABSAServiceCoverSearch';

@Component({
    templateUrl: 'iCABSARServiceSuspendHistory.html',
    providers: [CommonLookUpUtilsService]
})

export class ServiceSuspendHistoryComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('suspendStartDate') public suspendStartDate: DatepickerComponent;
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;

    public commonGridFunction: CommonGridFunction;
    public currentBranchCode: string = this.utils.getBranchCode();
    public pageId: string = '';
    public hasGridData: boolean = false;
    public dropdownDisable: boolean = false;
    public ellipsisDisabled: boolean = false;

    public controls: IControls[] = [
        { name: 'BranchNumber', required: true, type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', required: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', type: MntConst.eTypeText },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeText },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'SuspendStartDate', type: MntConst.eTypeDate },
        { name: 'SuspendEndDate', type: MntConst.eTypeDate },
        { name: 'SuspendReasonCode', type: MntConst.eTypeCode },
        { name: 'SuspendReasonDesc', type: MntConst.eTypeTextFree }
    ];

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    public dropdown: Record<string, Record<string, Object>> = {
        suspendReasonSearch: {
            params: {
                operation: 'Business/iCABSBSuspendReasonSearch',
                module: 'suspension',
                method: 'contract-management/search'
            },
            displayFields: ['SuspendReasonCode', 'SuspendReasonDesc'],
            active: { id: '', text: '' }
        }
    };

    public branch_dropdown: any = {
        branchParams: {
            'parentMode': 'LookUp'
        },
        component: BranchSearchComponent,
        active: { id: '', text: '' }
    };

    public ellipsis = {
        contractSearch: {
            childConfigParams: {
                'parentMode': 'LookUp-All'
            },
            component: ContractSearchComponent
        },
        premiseSearch: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': ''
            },
            component: PremiseSearchComponent
        },
        productSearch: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': '',
                'PremiseName': '',
                'PremiseNumber': ''
            },
            component: ServiceCoverSearchComponent
        }
    };

    public xhrParams: any = {
        operation: 'ApplicationReport/iCABSARServiceSuspendHistory',
        module: 'reports',
        method: 'bi/reports'
    };

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARSERVICESUSPENDHISTORY;
        this.pageTitle = 'Service Suspend History';
        this.browserTitle = 'Service and Invoice Suspend';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.buildGrid();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.gridCacheRefresh = true;
        if (this.parentMode === 'SuspendServiceandInvoice') {
            this.dropdownDisable = true;
            let branchNum = this.riExchange.getParentHTMLValue('BranchNumber');
            if (branchNum) {
                this.branch_dropdown['active'] = {
                    id: branchNum, text: branchNum + ' - ' + this.riExchange.getParentHTMLValue('BranchName')
                };
            }

            let suspendReasonCode = this.riExchange.getParentHTMLValue('SuspendReasonCode');
            if (suspendReasonCode) {
                this.dropdown.suspendReasonSearch['active'] = {
                    id: suspendReasonCode,
                    text: suspendReasonCode + ' - ' + this.riExchange.getParentHTMLValue('SuspendReasonDesc')
                };
            }
            this.setControlValue('SuspendStartDate', this.riExchange.getParentAttributeValue('SuspendStartDate'));
            this.setControlValue('SuspendEndDate', this.riExchange.getParentAttributeValue('SuspendEndDate'));
            this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
            this.setControlValue('ProductCode', this.riExchange.getParentAttributeValue('ProductCode'));
            this.onContractNumberChange();
            this.onPremiseNumberChange();
            this.onProductCodeChange();
            this.controls.forEach(control => {
                this.disableControl(control.name, true);
            });
            this.ellipsisDisabled = true;
            this.populateGrid();
        } else {
            let date = new Date();
            let edate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            let sdate = this.utils.rollBackTwelveMonths(edate);

            this.setControlValue('SuspendStartDate', this.globalize.parseDateToFixedFormat(sdate) as string);
            this.setControlValue('SuspendEndDate', this.globalize.parseDateToFixedFormat(edate) as string);
            this.setControlValue('BranchNumber', this.currentBranchCode);
            this.setControlValue('BranchName', this.utils.getBranchTextOnly());
            this.branch_dropdown['active'] = {
                id: this.currentBranchCode, text: this.currentBranchCode + ' - ' + this.utils.getBranchTextOnly()
            };
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'ServiceSuspendHistory', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'ServiceSuspendHistory', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductCode', 'ServiceSuspendHistory', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceSuspendQuantity', 'ServiceSuspendHistory', 'ServiceSuspendQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceSuspendQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('SuspendReason', 'ServiceSuspendHistory', 'SuspendReason', MntConst.eTypeTextFree, 20);
        this.riGrid.AddColumnAlign('SuspendReason', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('StartDate', 'ServiceSuspendHistory', 'StartDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('StartDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EndDate', 'ServiceSuspendHistory', 'EndDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('EndDate', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        gridQuery.set(this.serviceConstants.Action, '2');

        let formData: Object = {};
        formData[this.serviceConstants.Level] = 'Branch';
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        formData['ProductCode'] = this.getControlValue('ProductCode');
        formData['SuspendStartDate'] = this.getControlValue('SuspendStartDate');
        formData['SuspendEndDate'] = this.getControlValue('SuspendEndDate');
        formData['SuspendReasonFilter'] = this.getControlValue('SuspendReasonCode') === '' ? 'all' : this.getControlValue('SuspendReasonCode');
        formData['ServiceCoverRowID'] = this.parentMode === 'SuspendServiceandInvoice' ? this.riExchange.getParentAttributeValue('ServiceCoverRowID') : '';
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });

    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }


    public onSuspendReasonDataRecieved(data: any): void {
        if (data) {
            this.setControlValue('SuspendReasonCode', data.SuspendReasonCode);
            this.setControlValue('SuspendReasonDesc', data.SuspendReasonDesc);
        }
    }

    public onBranchDataRecived(data: any): void {
        if (data && data.BranchNumber) {
            this.setControlValue('BranchNumber', data.BranchNumber);
            this.setControlValue('BranchName', data.BranchName);
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
    }

    public onContractSearchDataReturn(data: any): void {
        if (data) {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            this.ellipsis.premiseSearch.childConfigParams.ContractNumber = data.ContractNumber;
            this.ellipsis.premiseSearch.childConfigParams.ContractName = data.ContractName;
            this.ellipsis.productSearch.childConfigParams.ContractName = data.ContractName;
            this.ellipsis.productSearch.childConfigParams.ContractNumber = data.ContractNumber;
        }
    }

    public onContractNumberChange(): void {
        this.setControlValue('ContractName', '');
        if (!this.getControlValue('ContractNumber')) {
            this.ellipsis.premiseSearch.childConfigParams.ContractName = '';
            this.ellipsis.premiseSearch.childConfigParams.ContractNumber = '';
            this.ellipsis.productSearch.childConfigParams.ContractName = '';
            this.ellipsis.productSearch.childConfigParams.ContractNumber = '';
            this.setControlValue('PremiseNumber', '');
            this.onPremiseNumberChange();
            this.setControlValue('ProductCode', '');
            this.onProductCodeChange();
            return;
        }
        this.lookupUtils.getContractName(this.getControlValue('ContractNumber')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('ContractName', data[0][0].ContractName);
                this.ellipsis.premiseSearch.childConfigParams.ContractName = data[0][0].ContractName;
                this.ellipsis.premiseSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
                this.ellipsis.productSearch.childConfigParams.ContractName = data[0][0].ContractName;
                this.ellipsis.productSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound);
            }
        });
    }

    public onPremiseSearchDataReturn(data: any): void {
        if (data) {
            this.setControlValue('PremiseNumber', data.PremiseNumber);
            this.setControlValue('PremiseName', data.PremiseName);
            this.ellipsis.productSearch.childConfigParams.PremiseName = data.PremiseName;
            this.ellipsis.productSearch.childConfigParams.PremiseNumber = data.PremiseNumber;
        }
    }

    public onPremiseNumberChange(): void {
        this.setControlValue('PremiseName', '');
        if (!this.getControlValue('PremiseNumber')) {
            this.ellipsis.productSearch.childConfigParams.PremiseName = '';
            this.ellipsis.productSearch.childConfigParams.PremiseNumber = '';
            return;
        }
        this.lookupUtils.getPremiseName(this.getControlValue('PremiseNumber'), null, this.getControlValue('ContractNumber')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('PremiseName', data[0][0].PremiseName);
                this.ellipsis.productSearch.childConfigParams.PremiseName = data[0][0].PremiseName;
                this.ellipsis.productSearch.childConfigParams.PremiseNumber = this.getControlValue('PremiseNumber');
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound);
            }
        });
    }

    public onProductSearchReceive(data: Object): void {
        if (data) {
            this.setControlValue('ProductCode', data['ProductCode']);
            this.setControlValue('ProductDesc', data['ProductDesc']);
        }
    }

    public onProductCodeChange(): void {
        this.setControlValue('ProductDesc', '');
        if (!this.getControlValue('PremiseNumber') || !this.getControlValue('ContractNumber') || !this.getControlValue('ProductCode')) {
            return;
        }
        this.lookupUtils.getProductCode(this.getControlValue('ProductCode')).then((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('ProductDesc', data[0][0].ProductDesc);
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound);
            }
        });
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
