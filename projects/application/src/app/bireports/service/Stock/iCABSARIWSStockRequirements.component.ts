import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARIWSStockRequirements.html'
})

export class StockRequirementsComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;


    public commonGridFunction: CommonGridFunction;
    public currentContractType: string;
    public hasGridData: boolean = false;
    public pageId: string;


    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true, value: this.utils.getBranchTextOnly() },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true, value: this.utils.getBranchCode() },
        { name: 'FromDate', type: MntConst.eTypeDate, required: true },
        { name: 'ToDate', type: MntConst.eTypeDate, required: true }

    ];

    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARIWSStockRequirements'
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARIWSSTOCKREQUIREMENTS;
        this.browserTitle = this.pageTitle = 'Stock Requirements';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.buildGrid();
        this.pageParams.gridCurrentPage = 1;
        this.windowOnload();
    }

    private windowOnload(): void {
        let date: Date = new Date();
        let dateTo: Date = new Date(date.getFullYear(), date.getMonth() + 1, date.getDate());

        this.riGrid.FunctionPaging = true;
        this.setControlValue('FromDate', this.globalize.parseDateToFixedFormat(date).toString());
        this.setControlValue('ToDate', this.globalize.parseDateToFixedFormat(dateTo).toString());
        this.pageParams.gridConfig = {
            itemsPerPage: 10,
            totalItem: 1
        };
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('BranchServiceAreaCode', 'StockRequirements', 'BranchServiceAreaCode', MntConst.eTypeCode, 8, true);
        this.riGrid.AddColumn('BranchServiceAreaName', 'StockRequirements', 'BranchServiceAreaName', MntConst.eTypeText, 30, true);
        this.riGrid.AddColumn('ServiceGroup', 'StockRequirements', 'ServiceGroup', MntConst.eTypeText, 8, true);
        this.riGrid.AddColumn('ServiceDesc', 'StockRequirements', 'ServiceDesc', MntConst.eTypeText, 30, true);
        this.riGrid.AddColumn('SaleGroup', 'StockRequirements', 'SaleGroup', MntConst.eTypeText, 8, true);
        this.riGrid.AddColumn('SaleDesc', 'StockRequirements', 'SaleDesc', MntConst.eTypeText, 30, true);
        this.riGrid.AddColumn('ProductCode', 'StockRequirements', 'ProductCode', MntConst.eTypeText, 4, true);
        this.riGrid.AddColumn('ProductDesc', 'StockRequirements', 'ProductDesc', MntConst.eTypeText, 20, true);
        this.riGrid.AddColumn('StockDate', 'StockRequirements', 'StockDate', MntConst.eTypeDate, 20, true);
        this.riGrid.AddColumn('StockQty', 'StockRequirements', 'StockQty', MntConst.eTypeText, 8, true);

        this.riGrid.AddColumnScreen('BranchServiceAreaName', false);
        this.riGrid.AddColumnScreen('ServiceDesc', false);
        this.riGrid.AddColumnScreen('SaleDesc', false);

        this.riGrid.Complete();
        this.dateCollist = this.riGrid.getColumnIndexListFromFull(['StockDate']);
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData['FromDate'] = this.utils.formatDate(this.getControlValue('FromDate'));
        formData['ToDate'] = this.utils.formatDate(this.getControlValue('ToDate'));

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.GridCacheRefresh] = true;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Descending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data);
            this.hasGridData = true;
      },
        (error) => {
          this.ajaxSource.next(this.ajaxconstant.COMPLETE);
          this.hasGridData = false;
          this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        this.commonGridFunction.onRefreshClick();
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
