import { Component, OnInit, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ProductivityDropDownService } from './productivityDropDown.service';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSeProductivityMultiPESGrid.html',
    providers: [CommonLookUpUtilsService, ProductivityDropDownService]
})

export class ProductivityMultiPESGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    public controls: Array<Object> = [
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText },
        { name: 'GroupBy', type: MntConst.eTypeText },
        { name: 'ShowType', type: MntConst.eTypeText },
        { name: 'RegionCode', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true }
    ];
    public pageId: string;
    public pageType: string;
    public URLReturn: string;
    public contractSearchComponent: any;
    public employeeSearchComponent = EmployeeSearchComponent;
    public contractParams: any = {
        'parentMode': 'LookUp-All',
        'showAddNew': false,
        'showHeader': true,
        'showCloseButton': true,
        'shouldClear': true
    };
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp',
            active: {
                id: this.utils.getBranchCode(),
                text: this.utils.getBranchText()
            }
        },
        employee: {
            parentMode: 'LookUp-Supervisor'
        }
    };
    public gridConfig: any = {
        pageSize: 10,
        totalRecords: 1,
        actionGrid: '2',
        actionBatch: '6',
        riSortOrder: ''
    };

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService, public prodService: ProductivityDropDownService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYMULTIPESGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType');
        this.browserTitle = this.pageTitle = this.pageType + ' Service Productivity';
        this.contractSearchComponent = ContractSearchComponent;
        if (this.pageType === 'Region') {
            this.lookupUtils.getRegionDesc().subscribe(data => {
                this.setControlValue('RegionCode', data[0][0].RegionCode);
                this.setControlValue('RegionDesc', data[0][0].RegionDesc);
            });
        }
        if (!this.isReturning()) {
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            this.setControlValue('GroupBy', this.pageType === 'Region' ? 'Branch' : 'Employee');
            this.setControlValue('ShowType', 'All');
            const date = new Date();
            this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth(), 1)));
            this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date));
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        } else {
            this.onRiGridRefresh();
        }
        this.pageParams.gridHandle = this.pageParams.gridHandle || this.utils.randomSixDigitString();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('ReportNumber', 'Productivity', 'ReportNumber', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumn('GeneratedDate', 'Productivity', 'GeneratedDate', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumn('GeneratedTime', 'Productivity', 'GeneratedTime', MntConst.eTypeTime, 15, false);
        this.riGrid.AddColumn('DateFrom', 'Productivity', 'DateFrom', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumn('DateTo', 'Productivity', 'DateTo', MntConst.eTypeDate, 10, false);

        this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('GeneratedDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('GeneratedTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('DateFrom', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('DateTo', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionGrid.toString());
            let form: any = {};
            form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            form['Level'] = this.pageType;
            form['RegionCode'] = this.getControlValue('RegionCode');
            form['BranchNumber'] = this.getControlValue('BranchNumber');
            form['SupervisorEmployeeCode'] = this.getControlValue('GroupBy') === 'Supervisor' ? '' : this.getControlValue('SupervisorEmployeeCode');
            form['ContractNumber'] = this.getControlValue('ContractNumber');
            form['GroupBy'] = this.getControlValue('GroupBy');
            form['DateFrom'] = this.getControlValue('DateFrom');
            form['DateTo'] = this.getControlValue('DateTo');
            form['ShowType'] = this.getControlValue('ShowType');
            form['EmployeeCode'] = this.getControlValue('EmployeeCode');
            form[this.serviceConstants.GridMode] = '0';
            form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
            form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
            form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            form['riSortOrder'] = 'Ascending';
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeProductivityMultiPESGrid', gridSearch, form).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (this.hasError(data)) {
                    this.displayMessage(data);
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                    setTimeout(() => {
                        this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                    }, 100);
                }
            }).catch((error) => {
                this.displayMessage(error);
            });
        }
    }

    public onDataRecieved(data: any, type: any): void {
        switch (type) {
            case 'Contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                break;
            case 'Employee':
                this.setControlValue('SupervisorEmployeeCode', data.SupervisorEmployeeCode);
                this.setControlValue('SupervisorSurname', data.SupervisorSurname);
                break;
        }
    }

    public onControlChange(type: string): void {
        switch (type) {
            case 'ContractNumber':
                this.setControlValue('ContractName', '');
                this.lookupUtils.getContractName(this.getControlValue('ContractNumber')).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('ContractName', data[0][0].ContractName);
                    }
                });
                this.resetValues();
                break;
            case 'SupervisorEmployeeCode':
                this.setControlValue('SupervisorSurname', '');
                this.lookupUtils.getEmployeeSurname(this.getControlValue('SupervisorEmployeeCode')).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('SupervisorSurname', data[0][0].EmployeeSurname);
                    }
                });
                this.resetValues();
                break;
            case 'ShowType':
                switch (this.getControlValue('ShowType')) {
                    case 'J':
                        this.contractParams['currentContractType'] = 'J';
                        this.contractParams['parentMode'] = 'LookUp';
                        break;
                    case 'C':
                        this.contractParams['currentContractType'] = 'C';
                        this.contractParams['parentMode'] = 'LookUp';
                        break;
                    default:
                        this.contractParams['currentContractType'] = '';
                        this.contractParams['parentMode'] = 'LookUp-All';
                        break;
                }
                this.resetValues();
                break;

        }
    }

    public onRiGridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public onGenerateReport(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionBatch.toString());

            gridSearch.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            gridSearch.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            gridSearch.set('Function', 'SubmitBatchProcess');
            gridSearch.set('SubmitBatchProcess', 'yes');
            gridSearch.set('DateFrom', this.getControlValue('DateFrom'));
            gridSearch.set('DateTo', this.getControlValue('DateTo'));
            gridSearch.set('ShowType', this.getControlValue('ShowType'));
            gridSearch.set('Level', this.pageType);
            switch (this.pageType) {
                case 'Region':
                    gridSearch.set('RegionCode', this.getControlValue('RegionCode'));
                    break;
                case 'Branch':
                    gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
                    gridSearch.set('ContractNumber', this.getControlValue('ContractNumber'));
                    gridSearch.set('SupervisorEmployeeCode', this.getControlValue('GroupBy') === 'Supervisor' ? '' : this.getControlValue('SupervisorEmployeeCode'));
                    gridSearch.set('GroupBy', this.getControlValue('GroupBy'));
                    break;
                case 'Employee':
                    gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
                    gridSearch.set('ContractNumber', this.getControlValue('ContractNumber'));
                    gridSearch.set('EmployeeCode', this.getControlValue('EmployeeCode'));
                    gridSearch.set('GroupBy', this.getControlValue('GroupBy'));
                    break;
            }
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest('bi/reports', 'reports', 'Service/iCABSSeProductivityMultiPESGrid', gridSearch)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (this.hasError(data)) {
                            this.displayMessage(data);
                        } else {
                            this.URLReturn = data.BatchProcessInformation;
                        }
                    },
                    (error) => {
                        this.displayMessage(error);
                    });
        }
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'ReportNumber') {
            let params: any = {
                'ShowType': this.getControlValue('ShowType'),
                'GroupBy': this.getControlValue('GroupBy'),
                'DateFrom': this.getControlValue('DateFrom'),
                'DateTo': this.getControlValue('DateTo'),
                'ReportNumber': this.riGrid.Details.GetValue('ReportNumber'),
                'RowID': this.riGrid.Details.GetAttribute('ReportNumber', 'rowid')
            };
            switch (this.pageType) {
                case 'Business':
                    this.navigate(this.pageType, BIReportsRoutes.ICABSSEPRODUCTIVITYBUSINESSPESGRID, params);
                    break;
                case 'Branch':
                    params['ContractNumber'] = this.getControlValue('ContractNumber');
                    params['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
                    this.navigate(this.pageType, BIReportsRoutes.ICABSSEPRODUCTIVITYBRANCHPESGRID, params);
                    break;
                case 'Region':
                    params['RegionCode'] = this.getControlValue('RegionCode');
                    params['RegionDesc'] = this.getControlValue('RegionDesc');
                    this.navigate(this.pageType, BIReportsRoutes.ICABSSEPRODUCTIVITYREGIONPESGRID, params);
                    break;
            }
        }
    }

    public resetValues(): void {
        this.pageParams.gridCurrentPage = 1;
        this.gridConfig.totalRecords = 1;
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
    }
}
