import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from './../../../../../shared/components/pagination/pagination';
import { ProductivityDropDownService } from './productivityDropDown.service';
import { QueryParams } from './../../../../../shared/services/http-params-wrapper';
import { SysCharConstants } from './../../../../../shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeProductivityServiceVisitsPESGrid.html',
    providers: [ProductivityDropDownService]
})

export class ProductivityServiceVisitsPESGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    constructor(injector: Injector, public prodService: ProductivityDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYSERVICEVISITSPESGRID;
        this.browserTitle = this.pageTitle = 'Service Productivity Detail';
    }

    public pageId: string;
    public pageType: string;
    public gridConfig: any = {
        pageSize: 10,
        totalRecords: 1,
        actionGrid: '2',
        riSortOrder: ''
    };
    public controls: Array<Object> = [
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'ShowType', type: MntConst.eTypeText, disabled: true },
        { name: 'GroupBy', type: MntConst.eTypeText, disabled: true },
        { name: 'EmployeeCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'Date', type: MntConst.eTypeDate, disabled: true },
        { name: 'Options', type: MntConst.eTypeText }
    ];

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType');
        super.ngAfterContentInit();
        this.pageParams.gridCacheRefresh = true;
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber'));
        this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName'));
        this.setControlValue('ShowType', this.riExchange.getParentHTMLValue('ShowType'));
        this.setControlValue('GroupBy', this.riExchange.getParentHTMLValue('GroupBy'));
        this.setControlValue('Date', this.riExchange.getParentHTMLValue('Date'));
        this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
        this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
        this.setControlValue('Options', 'options');
        this.onRiGridRefresh();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'Productivity', 'BranchServiceAreaSeqNo', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractNumber', 'Productivity', 'ContractNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'Productivity', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'Productivity', 'PremiseName', MntConst.eTypeInteger, 20);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductCode', 'Productivity', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'Productivity', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('StandardDuration', 'Productivity', 'StandardDuration', MntConst.eTypeTime, 6);
        this.riGrid.AddColumnAlign('StandardDuration', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('OvertimeDuration', 'Productivity', 'OvertimeDuration', MntConst.eTypeTime, 6);
        this.riGrid.AddColumnAlign('OvertimeDuration', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProductivityValue', 'Productivity', 'ProductivityValue', MntConst.eTypeDecimal2, 10);

        this.riGrid.Complete();

    }

    private populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionGrid.toString());
        let form: any = {};
        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['Function'] = 'Productivity';
        form['Level'] = 'VisitDetail';
        form['LanguageCode'] = this.riExchange.LanguageCode();
        form['BranchNumber'] = this.riExchange.getParentHTMLValue('BranchNumber');
        form['DateFrom'] = this.getControlValue('Date');
        form['DateTo'] = this.getControlValue('Date');
        form['EmployeeCode'] = this.getControlValue('EmployeeCode');
        form['ShowType'] = this.getControlValue('ShowType');
        form['GroupBy'] = this.getControlValue('GroupBy');
        if (this.riExchange.getParentHTMLValue('Employee')) {
            form['BusEmployee'] = this.getControlValue('Employee');
        }
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = 'Descending';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeProductivityPESGrid', gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public onOptionsChange(): void {
        if (this.getControlValue('Options') === 'ServiceSummary') {
            let params: any = {
                'EmployeeCode': this.getControlValue('EmployeeCode'),
                'EmployeeSurname': this.getControlValue('EmployeeSurname'),
                'Date': this.getControlValue('Date'),
                'BranchNumber': this.getControlValue('BranchNumber'),
                'BranchName': this.getControlValue('BranchName')
            };
            this.navigate('Productivity', '/servicedelivery/techWorkGridService/TechnicianWorkSummaryGrid', params);
        }
    }
}
