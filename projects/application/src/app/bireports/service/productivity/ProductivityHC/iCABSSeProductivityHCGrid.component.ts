import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent, GridConstants } from '@shared/components/grid-advanced/grid-advanced';
import { IGridHandlers } from '@base/BaseComponentLight';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ProductivityDropDownService } from './../ProductivityPES/productivityDropDown.service';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { MessageConstant } from '@shared/constants/message.constant';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

@Component({
    templateUrl: 'iCABSSeProductivityHCGrid.html',
    providers: [ProductivityDropDownService, CommonLookUpUtilsService]
})
export class ProductivityHCGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            'Branch': 'ApplicationReport/iCABSSeProductivityBranchGrid',
            'Business': 'ApplicationReport/iCABSSeProductivityBusinessGrid',
            'BranchDetail': 'Service/iCABSSeProductivityGrid',
            'Region': 'Service/iCABSSeProductivityRegionGrid'
        }
    };

    protected controls: Array<Object> = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'GroupBy', type: MntConst.eTypeText, value: 'Branch' },
        { name: 'RegionCode', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ViewBy', type: MntConst.eTypeText, value: 'Product' },
        { name: 'ShowType', type: MntConst.eTypeText, value: 'All' }
    ];
    protected pageId: string;

    public gridConfig: any = {
        totalItem: 1,
        itemsPerPage: 10
    };
    public pageType: string;

    constructor(injector: Injector, public prodService: ProductivityDropDownService, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYBUSINESSGRID;
        this.browserTitle = this.pageTitle = 'Business Service Productivity';
    }

    // #region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {

        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('type'));

        this.pageId = PageIdentifier['ICABSSEPRODUCTIVITY' + this.pageType.toUpperCase() + 'GRID'];
        this.pageTitle = this.pageType === 'BranchDetail' ? 'Service Productivity Detail' : this.pageType + ' Service Productivity';
        this.utils.setTitle(this.pageTitle);

        super.ngAfterContentInit();

        this.buildGrid();

        if (this.pageType === 'BranchDetail') {
            this.disableControl('DateFrom', true);
            this.disableControl('DateTo', true);
            this.disableControl('ShowType', true);
        }

        if (this.isReturning()) {
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.groupbyOptions = this.prodService['groupby' + this.pageType] || [];
            this.pageParams.viewbyOptions = this.prodService['viewby' + this.pageType] || [];

            if (this.pageType === 'Branch') {
                this.setControlValue('GroupBy', this.pageParams.groupbyOptions[0].value);
            } else if (this.pageType === 'BranchDetail') {
                this.setControlValue('GroupBy', 'Product');
            }

            this.setControlValue('BusinessCode', this.businessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            if (!this.parentMode) {
                this.setControlValue('DateFrom', StaticUtils.getFirstDayOfMonth());
                this.setControlValue('DateTo', new Date());
            }

            // Left In If Else Block; Please Modify For Region Handling
            if (this.pageType === 'Branch' || this.pageType === 'BranchDetail') {
                /**
                 * Disabled This Rule Since If Value Not Recieved From Parent Then Take The Current Branch
                 * Cases:
                 *   # Recieve From Parent - While Navigating From Business Level
                 *   # Take From Current   - While Navigated From Menu
                 */
                // tslint:disable-next-line: no-unused-expression
                this.riExchange.getParentHTMLValue('BranchNumber') || this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.commonLookup.getBranchName(this.getControlValue('BranchNumber')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('BranchName', data[0][0].BranchName);
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
                if (this.pageType === 'BranchDetail') {
                    this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('ServiceCode'));
                    this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentAttributeValue('ServiceDesc'));
                }
                if (this.parentMode === 'ProductivityBusiness' || this.parentMode === 'ProductivityBranch') {
                    this.riExchange.getParentHTMLValue('DateFrom');
                    this.riExchange.getParentHTMLValue('DateTo');
                    this.riExchange.getParentHTMLValue('ShowType');

                    this.onRiGridRefresh();
                }
            } else if (this.pageType === 'Region') {
                if (this.parentMode === 'ProductivityBusiness') {
                    let regionCode: string = this.riExchange.getParentHTMLValue('RegionCode');
                    this.commonLookup.getRegionDescFromCode(regionCode).then(data => {
                        if (data && data[0] && data[0][0]) {
                            this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                        } else {
                            this.displayMessage(MessageConstant.Message.RecordNotFound, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                    this.riExchange.getParentHTMLValue('DateFrom');
                    this.riExchange.getParentHTMLValue('DateTo');
                    this.populateGrid();
                } else {
                    this.commonLookup.getRegionDesc().subscribe(data => {
                        if (data && data[0] && data[0][0]) {
                            this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                            this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                        } else {
                            this.displayMessage(MessageConstant.Message.RecordNotFound, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                        }
                    }, error => {
                        this.displayMessage(error);
                    });
                }
            }
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    // #endregion

    // #region Private Methods
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('GroupCode', 'Productivity', 'GroupCode', MntConst.eTypeCode, 6, true);
        this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);
        if (this.pageType !== 'BranchDetail') {
            this.riGrid.AddColumn('DaysAvail', 'Productivity', 'DaysAvail', MntConst.eTypeInteger, 10);
        }
        this.riGrid.AddColumn('TotalExchanges', 'Productivity', 'TotalExchanges', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ExVisits', 'Productivity', 'ExVisits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ExQty', 'Productivity', 'ExQty', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('ExUnits', 'Productivity', 'ExUnits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('InsVisits', 'Productivity', 'InsVisits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('InsQty', 'Productivity', 'InsQty', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('InsUnits', 'Productivity', 'InsUnits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('RemVisits', 'Productivity', 'RemVisits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('RemQty', 'Productivity', 'RemQty', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('RemUnits', 'Productivity', 'RemUnits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('RepVisits', 'Productivity', 'RepVisits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('RepQty', 'Productivity', 'RepQty', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('RepUnits', 'Productivity', 'RepUnits', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('DelVisits', 'Productivity', 'DelVisits', MntConst.eTypeInteger, 10);

        if (this.pageType === 'Branch' || this.pageType === 'BranchDetail') {
            this.riGrid.AddColumn('TotalMiles', 'Productivity', 'TotalMiles', MntConst.eTypeInteger, 10);
            this.riGrid.AddColumn('TotalProductTime', 'Productivity', 'TotalProductTime', MntConst.eTypeTime, 10);
            this.riGrid.AddColumn('TotalRevenue', 'Productivity', 'TotalRevenue', MntConst.eTypeInteger, 10);

            this.riGrid.AddColumnAlign('TotalMiles', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('TotalProductTime', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('TotalRevenue', MntConst.eAlignmentCenter);

            this.riGrid.AddColumnOrderable('TotalMiles', true);
            this.riGrid.AddColumnOrderable('TotalProductTime', true);
            this.riGrid.AddColumnOrderable('TotalRevenue', true);
            this.riGrid.AddColumnOrderable('ExVisits', true);
            this.riGrid.AddColumnOrderable('ExQty', true);
            this.riGrid.AddColumnOrderable('InsVisits', true);
            this.riGrid.AddColumnOrderable('InsQty', true);
            this.riGrid.AddColumnOrderable('RemVisits', true);
            this.riGrid.AddColumnOrderable('RemQty', true);
            this.riGrid.AddColumnOrderable('RepVisits', true);
            this.riGrid.AddColumnOrderable('RepQty', true);
            this.riGrid.AddColumnOrderable('DelVisits', true);
        }

        if (this.pageType !== 'BranchDetail') {
            this.riGrid.AddColumnAlign('DaysAvail', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumnAlign('TotalExchanges', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ExVisits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ExQty', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ExUnits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('InsVisits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('InsQty', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('RemVisits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('RemUnits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('RepVisits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('RepQty', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('RepUnits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('DelVisits', MntConst.eAlignmentCenter);

        this.riGrid.Complete();

        this.durationCollist = this.riGrid.getColumnIndexList(['TotalProductTime']);
    }

    private populateGrid(): void {
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = this.pageType;
        formData[this.serviceConstants.Function] = 'Productivity';
        formData[this.serviceConstants.LanguageCode] = this.riExchange.LanguageCode();
        formData['ShowType'] = this.getControlValue('ShowType');
        formData['GroupBy'] = this.getControlValue('GroupBy');
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        switch (this.pageType) {
            case 'Branch':
                formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
                break;
            case 'BranchDetail':
                formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
                formData[this.serviceConstants.BusinessCode] = this.getControlValue('BusinessCode');
                formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
                formData['ViewBy'] = this.getControlValue('ViewBy');
                formData['GroupBy'] = 'Product';
                break;
            case 'Region':
                formData[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
                break;
        }

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.pageType === 'Branch' ? this.riGrid.HeaderClickedColumn : '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation[this.pageType], gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                this.setPagination();
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private setPagination(): void {
        // setPage Method In Pagination Executes Earlier Than Change; Hence Added The setTimeout
        setTimeout(() => {
            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
        }, 100);
    }
    // #endregion

    // #region Public Methods
    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.riGrid.RefreshRequired();
            this.populateGrid();
        }
    }

    public getCurrentPage(currentPage: any): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            super.getCurrentPage(currentPage);
        } else {
            this.setPagination();
        }
    }

    public onGridBodyDoubleClick(_event?: any): void {
        let groupBy: string = this.getControlValue('GroupBy');
        let rowid: string = this.riGrid.Details.GetAttribute('GroupCode', 'rowid');

        if (this.riGrid.CurrentColumnName !== 'GroupCode' || rowid === 'TOTAL') {
            return;
        }

        switch (groupBy) {
            case 'Branch':
                this.navigate('ProductivityBusiness', BIReportsRoutes.ICABSSEPRODUCTIVITYBRANCHGRID, {
                    type: 'Branch',
                    BranchNumber: this.riGrid.Details.GetAttribute('GroupCode', 'rowid')
                });
                break;
            case 'Region':
                this.navigate('ProductivityBusiness', BIReportsRoutes.ICABSSEPRODUCTIVITYREGIONGRID, {
                    type: 'Region',
                    RegionCode: this.riGrid.Details.GetAttribute('GroupCode', 'rowid')
                });
                break;
            case 'BranchServiceArea':
                this.navigate('ProductivityBranch', BIReportsRoutes.ICABSSEPRODUCTIVITYGRID, {
                    type: 'BranchDetail',
                    ServiceCode: this.riGrid.Details.GetAttribute('GroupCode', 'rowid'),
                    ServiceDesc: this.riGrid.Details.GetAttribute('GroupCode', 'additionalproperty')
                });
                break;
        }
    }

    public onViewByChange(): void {
        let isDate: boolean = this.getControlValue('ViewBy') === 'Date';
        let params: any = {};
        params[GridConstants.c_s_KEY_INDEX] = 0;
        params[GridConstants.c_s_PKEY_TYPE] = isDate ? MntConst.eTypeDate : MntConst.eTypeCode;
        params[GridConstants.c_s_PKEY_SIZE] = isDate ? 10 : 6;
        this.riGrid.updateCoulmnProperties([params]);
        if (this.pageType === 'BranchDetail') {
            this.onRiGridRefresh();
        }
    }

    public onHeaderClick(): void {
        if (this.pageType === 'Business') {
            return;
        }

        this.onRiGridRefresh();
    }
    // #endregion
}
