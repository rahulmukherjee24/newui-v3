import { AfterContentInit, Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSSeDailyProductivity.html',
    providers: [CommonLookUpUtilsService]
})

export class DailyProductivityComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public isBranch: boolean;
    public isSummary: boolean;
    public pageType: string;
    public isEmployeeDisbaled: boolean = false;

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 25
    };

    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            'Business': 'Service/iCABSSeDailyProductivityBusinessGrid',
            'Branch': 'Service/iCABSSeDailyProductivityBranchGrid',
            'Summary': 'Service/iCABSSeDailyProductivitySummaryGrid'
        }
    };

    public ellipsis: any = {
        employee: {
            childParams: {
                'parentMode': 'LookUp-Service'
            },
            component: EmployeeSearchComponent
        }
    };

    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeInteger, disabled: true, required: true },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessName', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'GridPageSize', required: true, type: MntConst.eTypeInteger, value: 25 },
        { name: 'ViewBy', required: true, value: 'Region' }
    ];
    public expCfg: IExportOptions = {};

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BusinessName', this.utils.getBusinessText());
    }

    public ngAfterContentInit(): void {
        this.pageType = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('pageType'));
        this.pageId = PageIdentifier['ICABSSEDAILYPRODUCTIVITY' + this.pageType.toUpperCase()];
        this['is' + this.pageType] = this.pageType;

        this.browserTitle = this.pageTitle
            = this.isSummary ? 'Employee Daily Productivity Summary' : (this.pageType + ' Daily Productivity');

        super.ngAfterContentInit();

        if (this.isReturning()) {
            this.buildGrid();
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;

            if (this.parentMode === 'Region' || this.parentMode === 'Business') {
                this.riExchange.getParentHTMLValue('BusinessCode');
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');

                this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));

            } else if (this.parentMode === 'Branch') {
                this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));

                this.riExchange.getParentHTMLValue('BusinessCode');
                this.riExchange.getParentHTMLValue('BranchNumber');
                this.riExchange.getParentHTMLValue('DateFrom');
                this.riExchange.getParentHTMLValue('DateTo');
                this.isEmployeeDisbaled = true;
            } else {
                let date: Date = new Date();
                let formDate: Date = new Date(date.getFullYear(), date.getMonth(), 1);
                this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(formDate) as string);
                this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date) as string);

                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            }

            this.buildGrid();

            if (this.parentMode) {
                this.onRiGridRefresh();
            }
        }
        if (this.parentMode === 'Region' || this.parentMode === 'Business') {
            this.disableControl('DateFrom', true);
            this.disableControl('DateTo', true);
        } else if (this.parentMode === 'Branch') {
            this.disableControl('EmployeeCode', true);
            this.disableControl('EmployeeSurname', true);
            this.disableControl('DateFrom', true);
            this.disableControl('DateTo', true);
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public buildGrid(): void {
        this.riGrid.Clear();

        if (this.isSummary) {
            if (this.getControlValue('EmployeeCode') === '') {
                this.riGrid.AddColumn('EmpCode', 'Activity', 'EmpCode', MntConst.eTypeCode, 6);
                this.riGrid.AddColumnAlign('EmpCode', MntConst.eAlignmentCenter);
            }
            this.riGrid.AddColumn('ServiceDate', 'Activity', 'ServiceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServiceDate', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('StartTime', 'Activity', 'StartTime', MntConst.eTypeTime, 5);
            this.riGrid.AddColumnAlign('StartTime', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('EndTime', 'Activity', 'EndTime', MntConst.eTypeTime, 5);
            this.riGrid.AddColumnAlign('EndTime', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('Mileage', 'Activity', 'Mileage', MntConst.eTypeInteger, 5);

            this.riGrid.AddColumn('AverageMilesPerHour', 'Activity', 'AverageMilesPerHour', MntConst.eTypeInteger, 3);

            this.riGrid.AddColumn('PremisesVisited', 'Activity', 'PremisesVisited', MntConst.eTypeInteger, 3);

            this.riGrid.AddColumn('UnitsServiced', 'Activity', 'UnitsServiced', MntConst.eTypeInteger, 3);

            this.riGrid.AddColumn('HoursWorked', 'Activity', 'HoursWorked', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('HoursWorked', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('DrivingTime', 'Activity', 'DrivingTime', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('DrivingTime', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ProductiveTime', 'Activity', 'ProductiveTime', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('ProductiveTime', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ServicingValue', 'Activity', 'ServicingValue', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('ProductValue', 'Activity', 'ProductValue', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('DrivingCost', 'Activity', 'DrivingCost', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('Revenue', 'Activity', 'Revenue', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('RevenuePerHourOnSite', 'Activity', 'RevenuePerHourOnSite', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('RevenuePerHourWorked', 'Activity', 'RevenuePerHourWorked', MntConst.eTypeDecimal2, 10);
        } else {
            this.riGrid.AddColumn('GroupCode', 'Activity', 'GroupCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('GroupDesc', 'Activity', 'GroupDesc', MntConst.eTypeText, 20);

            if (this.pageType === 'Business') {
                this.riGrid.AddColumn('NumberOfStaff', 'Activity', 'NumberOfStaff', MntConst.eTypeInteger, 2);
                this.riGrid.AddColumnAlign('NumberOfStaff', MntConst.eAlignmentCenter);
            }

            this.riGrid.AddColumn('DaysWorked', 'Activity', 'DaysWorked', MntConst.eTypeInteger, 2);
            this.riGrid.AddColumnAlign('DaysWorked', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('AverageMilesPerDay', 'Activity', 'AverageMilesPerDay', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('AverageMilesPerDay', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('AverageSpeed', 'Activity', 'AverageSpeed', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('AverageSpeed', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('HoursWorked', 'Activity', 'HoursWorked', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('HoursWorked', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('PremisesVisited', 'Activity', 'PremisesVisited', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('PremisesVisited', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('UnitsServiced', 'Activity', 'UnitsServiced', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('UnitsServiced', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('AveragePremisesPerDay', 'Activity', 'AveragePremisesPerDay', MntConst.eTypeDecimal2, 4);
            this.riGrid.AddColumnAlign('AveragePremisesPerDay', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('AverageHoursPerDay', 'Activity', 'AverageHoursPerDay', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('AverageHoursPerDay', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('AverageDrivingTimePerDay', 'Activity', 'AverageDrivingTimePerDay', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('AverageDrivingTimePerDay', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('AverageProductiveTimePerDay', 'Activity', 'AverageProductiveTimePerDay', MntConst.eTypeText, 5);
            this.riGrid.AddColumnAlign('AverageProductiveTimePerDay', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('Revenue', 'Activity', 'Revenue', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('AverageRevenuePerHour', 'Activity', 'AverageRevenuePerHour', MntConst.eTypeDecimal2, 10);

            this.riGrid.AddColumn('AverageRevenuePerDay', 'Activity', 'AverageRevenuePerDay', MntConst.eTypeDecimal2, 10);
        }

        this.riGrid.Complete();
        if (this.isSummary) {
            this.expCfg.dateColumns = this.riGrid.getColumnIndexListFromFull(['ServiceDate']);
            this.expCfg.durationColumns = this.riGrid.getColumnIndexListFromFull(['StartTime','EndTime']);
        }
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');
        formData[this.serviceConstants.Level] = this.pageType;
        formData[this.serviceConstants.BusinessCode] = this.getControlValue('BusinessCode');
        if (this.pageType === 'Business') {
            formData[this.serviceConstants.ViewBy] = this.getControlValue('ViewBy');
        } else {
            formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
            if (this.isSummary) {
                formData[this.serviceConstants.EmployeeCode] = this.getControlValue('EmployeeCode');
            }
        }
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = this.getControlValue('GridPageSize');
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation[this.pageType], gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.getControlValue('GridPageSize') : 1;
                this.gridConfig.itemsPerPage = this.getControlValue('GridPageSize');
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onDataRecieved(data: object): void {
        if (data) {
            this.setControlValue('EmployeeCode', data['EmployeeCode']);
            this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
            this.buildGrid();
        }
    }

    public onSupervisorChange(): void {
        this.setControlValue('EmployeeSurname', '');
        if (!this.getControlValue('EmployeeCode')) {
            return;
        }
        this.lookupUtils.getEmployeeSurname(this.getControlValue('EmployeeCode')).then(data => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
            } else {
                this.setControlValue('EmployeeCode', '');
                this.setControlValue('EmployeeSurname', '');
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Employee');
            }
            this.buildGrid();
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onDtChange(): void {
        if (this.isSummary) {
            this.buildGrid();
            this.hasGridData = false;
            this.riGrid.RefreshRequired();
        }
    }

    public onGridBodyDoubleClick?(): void {
        let currentColName = this.riGrid.CurrentColumnName;
        if (this.riGrid.Details.GetValue(currentColName) === 'TOTAL') {
            return;
        }
        if (this.isBranch) {
            if (currentColName === 'GroupCode') {
                this.setAttribute('EmployeeCode', this.riGrid.Details.GetValue(currentColName));
                this.setAttribute('EmployeeSurname', this.riGrid.Details.GetValue('GroupDesc'));
                this.navigate('Branch', BIReportsRoutes.ICABSSEDAILYPRODUCTIVITYSUMMARYGRID, {
                    pageType: 'summary'
                });
            }
        } else if (this.isSummary) {
            if (currentColName === 'EmpCode' || currentColName === 'ServiceDate') {
                this.setAttribute('GroupCode', this.getControlValue('EmployeeCode') || this.riGrid.Details.GetValue('EmpCode'));
                this.setAttribute('GroupDesc', this.getControlValue('EmployeeSurname') || this.riGrid.Details.GetAttribute('EmpCode', 'title'));
                this.setAttribute('DateFrom', this.riGrid.Details.GetValue('ServiceDate'));
                this.setAttribute('DateTo', this.riGrid.Details.GetValue('ServiceDate'));
                this.navigate('Summary', BIReportsRoutes.ICABSSESERVICEDAILYPRODUCTIVITYGRID);
            }
        } else if (this.pageType === 'Business') {
            const viewBy: string = this.getControlValue('ViewBy');
            /**
             * @todo - Remove The Message And Return Statement Once Region Page Is Completed
             */
            if (viewBy === 'Region') {
                this.setAttribute('RegionCode', this.riGrid.Details.GetValue('GroupCode'));
                this.setAttribute('RegionDesc', this.riGrid.Details.GetValue('GroupDesc'));
                this.displayMessage(MessageConstant.Message.PageNotDeveloped, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                return;
            } else {
                this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('GroupCode'));
                this.setAttribute('BranchName', this.riGrid.Details.GetValue('GroupDesc'));
            }

            this.navigate(
                'Business',
                BIReportsRoutes['ICABSSEDAILYPRODUCTIVITY' + viewBy.toUpperCase() + 'GRID'],
                {
                    pageType: viewBy.toLocaleLowerCase()
                }
            );
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
