import { Component, OnInit, OnDestroy, Injector, AfterContentInit, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SOSDropDownService } from './sosDropDown.service';
import { StaticUtils } from '@shared/services/static.utility';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';

@Component({
    templateUrl: 'iCABSSeBatchSOSAgedProfileGrid.html',
    providers: [CommonLookUpUtilsService, SOSDropDownService]
})

export class BatchSOSAgedProfileGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;

    public commonGridFunction: CommonGridFunction;
    public pageType: string = '';

    private xhrParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: {
            Business: 'Service/iCABSSeBatchSOSAgedProfileBusinessGrid',
            Branch: 'Service/iCABSSeBatchSOSAgedProfileBranchGrid'
        }
    };

    protected controls: IControls[] = [
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'CompanyCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'CompanyDesc', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'DateFrom', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateSelect', disabled: true, type: MntConst.eTypeText },
        { name: 'DateTo', disabled: true, type: MntConst.eTypeDate },
        { name: 'FilterType', disabled: true, type: MntConst.eTypeText },
        { name: 'GroupBy', type: MntConst.eTypeText },
        { name: 'NumberOfVisits', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ProductServiceGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ReportGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ReportGroupDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ReportNumber', type: MntConst.eTypeText },
        { name: 'SeasonalSelect', value: 'All', type: MntConst.eTypeText },
        { name: 'ServiceType', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ShowPortfolioType', disabled: true, type: MntConst.eTypeText },
        { name: 'ShowType', disabled: true, type: MntConst.eTypeText },
        { name: 'SupervisorEmployeeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'VisitFrequency', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ViewBy', value: 'Branch', type: MntConst.eTypeText }
    ];

    constructor(injector: Injector, public sosService: SOSDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('type'));

        this.pageId = PageIdentifier['ICABSSEBATCHSOSAGEDPROFILE' + this.pageType.toUpperCase() + 'GRID'];
        this.pageTitle = this.pageType + ' State Of Service Aged Portfolio';
        this.utils.setTitle(this.pageTitle);

        super.ngAfterContentInit();

        if (this.isReturning()) {
            this.buildGrid();
            this.pageParams.gridCacheRefresh = false;
            this.commonGridFunction.onRiGridRefresh();
        } else {
            this.pageParams.gridConfig = {
                itemsPerPage: 21,
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true,
                totalItem: 1
            };
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.hasGridData = false;
            this.pageParams.riGridSortOrder = 'Descending';
            this.pageParams.riGridHeaderClickedColumn = this.riGrid.HeaderClickedColumn;

            if (this.riExchange.getParentHTMLValue('ShowType') === 'Contracts') {
                this.setControlValue('NumberOfVisits', 1);
            } else {
                this.setControlValue('NumberOfVisits', 0);
            }

            switch (this.pageType) {
                case 'Business':
                    this.riExchange.getParentHTMLValue('BusinessCode');
                    this.riExchange.getParentHTMLValue('BusinessDesc');
                    break;
                case 'Branch':
                    if (this.parentMode === 'BatchStateOfServiceBusiness' || this.parentMode === 'BatchStateOfServiceRegion') {
                        this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                        this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                        this.riExchange.getParentHTMLValue('SeasonalSelect');
                    } else {
                        this.riExchange.getParentHTMLValue('BranchName');
                        this.riExchange.getParentHTMLValue('BranchNumber');
                        this.riExchange.getParentHTMLValue('SupervisorEmployeeCode');
                        this.riExchange.getParentHTMLValue('SupervisorSurname');
                    }
                    break;
            }
            // Fetch values from parent
            const controlsListToFetchParentValue = [
                'CompanyCode', 'CompanyDesc', 'ContractName', 'ContractNumber',
                'DateFrom', 'DateSelect', 'DateTo', 'FilterType', 'GroupBy',
                'ProductServiceGroupCode', 'ReportGroupCode', 'ReportGroupDesc',
                'ServiceTypeCode', 'ShowPortfolioType', 'VisitFrequency'
            ];
            controlsListToFetchParentValue.forEach(control => {
                this.riExchange.getParentHTMLValue(control);
            });

            this.pageParams.fromDateShow = true;
            this.pageParams.toDateShow = true;

            this.dateSelectSetValue();
            this.getSysChars();
            this.onRiGridRefresh();
        }
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.buildGrid();
            this.commonGridFunction.onRefreshClick();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private dateSelectSetValue(): void {
        switch (this.riExchange.getParentHTMLValue('DateSelect')) {
            case 'AnniversaryDate':
            case 'ExcludeFromDate':
                this.pageParams.fromDateShow = false;
                break;
        }
    }

    private buildGrid(): void {
        let dg = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten'];

        this.riGrid.Clear();
        switch (this.pageType) {
            case 'Branch':
                this.riGrid.AddColumn('BranchServiceAreaCode', 'StateOfService', 'BranchServiceAreaCode', MntConst.eTypeCode, 6, true);
                this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);

                this.riGrid.AddColumn('BranchServiceAreaDesc', 'StateOfService', 'BranchServiceAreaDesc', MntConst.eTypeText, 14);
                this.riGrid.AddColumnAlign('BranchServiceAreaDesc', MntConst.eAlignmentLeft);

                this.riGrid.AddColumn('EmployeeSurname', 'StateOfService', 'EmployeeSurname', MntConst.eTypeText, 15);
                this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
                break;
            case 'Business':
                if (this.getControlValue('ViewBy') === 'Region') {
                    this.riGrid.AddColumn('RegionCode', 'StateOfService', 'RegionCode', MntConst.eTypeCode, 6, true);
                    this.riGrid.AddColumnAlign('RegionCode', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumnOrderable('RegionCode', true);

                    this.riGrid.AddColumn('RegionDesc', 'StateOfService', 'RegionDesc', MntConst.eTypeText, 14);
                    this.riGrid.AddColumnAlign('RegionDesc', MntConst.eAlignmentLeft);
                } else {
                    this.riGrid.AddColumn('BranchNumber', 'StateOfService', 'BranchNumber', MntConst.eTypeText, 6, true);
                    this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
                    this.riGrid.AddColumnOrderable('BranchNumber', true);

                    this.riGrid.AddColumn('BranchName', 'StateOfService', 'BranchName', MntConst.eTypeText, 14);
                    this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentLeft);
                }
                break;
        }
        this.riGrid.AddColumn('VisitsDue', 'StateOfService', 'VisitsDue', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('VisitsDue', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('VisitsDone', 'StateOfService', 'VisitsDone', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('VisitsDone', MntConst.eAlignmentRight);

        for (let idx = 0; idx < 10; idx++) {
            this.riGrid.AddColumn(dg[idx] + 'Behind', 'StateOfService', dg[idx] + 'Behind', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign(dg[idx] + 'Behind', MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);

        this.riGrid.Complete();
        // Retain state of grid header sort order
        if (this.isReturning()) {
            this.riGrid.HeaderClickedColumn = this.pageParams.riGridHeaderClickedColumn;
            this.riGrid.DescendingSort = this.pageParams.riGridSortOrder === 'Descending';
        }
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let formData: any = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');

        if (this.pageType === 'Business') {
            formData['ViewBy'] = this.getControlValue('ViewBy');
        } else if (this.pageType === 'Branch') {
            formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        }
        formData[this.serviceConstants.BusinessCode] = this.getControlValue('BusinessCode') || this.businessCode();
        formData['CompanyCode'] = this.getControlValue('CompanyCode');
        formData[this.serviceConstants.Level] = this.pageType;
        formData['ReportGroupCode'] = this.getControlValue('ReportGroupCode');
        formData['ReportNumber'] = this.riExchange.getParentAttributeValue('ReportNumber');
        formData['RowID'] = this.riExchange.getParentAttributeValue('ROWID');
        formData['SeasonalSelect'] = this.getControlValue('SeasonalSelect');
        formData['ShowType'] = this.getControlValue('ShowType');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.pageParams.riGridHeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.pageParams.riGridSortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation[this.pageType], search, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
            }).catch(error => {
                this.pageParams.hasGridData = false;
                this.displayMessage(error);
            });
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharEnableCompanyCode
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.enableCompanyCode = list[0].Required;
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    public onGridBodyDoubleClick(): void {
        let currentCol: string = this.riGrid.CurrentColumnName;
        let excludeCol: string[];
        switch (this.pageType) {
            case 'Branch':
                excludeCol = ['BranchServiceAreaDesc', 'EmployeeSurname', 'VisitsDue', 'VisitsDone'];
                break;
            case 'Business':
                excludeCol = [
                    'RegionDesc', 'BranchName', 'VisitsDue', 'VisitsDone',
                    'OneBehind', 'TwoBehind', 'ThreeBehind', 'FourBehind',
                    'FiveBehind', 'SixBehind', 'SevenBehind', 'EightBehind',
                    'NineBehind', 'TenBehind'
                ];
                break;
        }
        if (excludeCol.includes(currentCol)) {
            return;
        }
        const currentColValue = this.riGrid.Details.GetValue(currentCol);
        if (!currentColValue || currentColValue !== 'TOTAL') {
            let params: Object = {};
            switch (this.pageType) {
                case 'Branch':
                    let serviceArea: string = this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'rowid');
                    params['ViewTotal'] = serviceArea === 'TOTAL' ? 'Yes' : '';
                    params['BranchServiceAreaCode'] = serviceArea === 'TOTAL' ? '' : serviceArea;
                    params['EmployeeSurname'] = serviceArea === 'TOTAL' ? '' : this.riGrid.Details.GetValue('EmployeeSurname');
                    params['ValueBehind'] = this.riGrid.Details.GetAttribute(currentCol, 'AdditionalProperty') || '';
                    this.navigate('BatchStateOfServiceBranchAged', BIReportsRoutes.ICABSSESTATEOFSERVICEGRID, params);
                    break;
                case 'Business':
                    if (this.getControlValue('ViewBy') === 'Region') {
                        this.displayMessage(MessageConstant.Message.PageNotCovered, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                    } else {
                        this.setAttribute('ROWID', this.riExchange.getParentAttributeValue('ROWID'));
                        this.setAttribute('ReportNumber', this.riExchange.getParentAttributeValue('ReportNumber'));
                        this.setAttribute('BranchNumber', this.riGrid.Details.GetAttribute('BranchNumber', 'rowid'));
                        this.setAttribute('BranchName', this.riGrid.Details.GetValue('BranchName'));
                        this.navigate('BatchStateOfServiceBusiness', BIReportsRoutes.ICABSSEBATCHSOSAGEDPROFILEBRANCHGRID, {
                            type: 'Branch'
                        });
                    }
                    break;
            }
        }
    }

    public onHeaderClick(): void {
        this.pageParams.riGridSortOrder = this.riGrid.SortOrder;
        this.pageParams.riGridHeaderClickedColumn = this.riGrid.HeaderClickedColumn;
        this.onRiGridRefresh();
    }
}
