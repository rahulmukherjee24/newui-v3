import { CommonLookUpUtilsService } from './../../../../shared/services/commonLookupUtils.service';
import { Injectable } from '@angular/core';

@Injectable()
export class SOSDropDownService {
    constructor(private lookupUtils: CommonLookUpUtilsService) { }

    public reportGroup: Array<string> = [];
    public allDateSelectValues: any = [
        { text: 'Exclude From Date', value: 'ExcludeFromDate' },
        { text: 'Specify From Date', value: 'SpecifyDate' },
        { text: 'Use Anniversary Date', value: 'AnniversaryDate' },
        { text: 'Rolling 12 Months', value: 'Rolling12Months' }
    ];

    public dateSelectValues: any = [
        { text: 'Specify From Date', value: 'SpecifyDate' },
        { text: 'Use Anniversary Date', value: 'AnniversaryDate' },
        { text: 'Rolling 12 Months', value: 'Rolling12Months' }
    ];

    public showTypeValues: any = [
        { text: 'Contracts', value: 'Contract' },
        { text: 'Jobs', value: 'Job' }
    ];

    public showPortfolioValues: any = [
        { text: 'All', value: 'AllContracts' },
        { text: 'Nationals', value: 'NationalAccounts' },
        { text: 'Inter Company', value: 'InterCompany' }
    ];

    public groupByValues: any = [
        { text: 'Area', value: 'Area' },
        { text: 'Town', value: 'Town' }
    ];

    public filterValues: any = [
        { text: 'All', value: 'All' },
        { text: 'Ahead', value: 'Ahead' },
        { text: 'Behind', value: 'Behind' },
        { text: 'No Visits', value: 'NoVisits' }
    ];

    public viewBy: any = [
        { text: 'Branch', value: 'Branch' },
        { text: 'Region', value: 'Region' }
    ];

    public seasonalOptions: any = [
        { value: 'All', text: 'All' },
        { value: 'Seasonal', text: 'Seasonal' },
        { value: 'NotSeasonal', text: 'Not Seasonal' }
    ];
    public contractSeasonalOptions: any = [
        { value: 'All', text: 'All' },
        { value: 'Seasonal', text: 'Seasonal' },
        { value: 'NotSeasonal', text: 'Not Seasonal' }
    ];
    public jobSeasonalOptions: any = [
        { value: 'All', text: 'All' }
    ];
    public showTypeBy: Array<Record<string, string>> = [
        { value: 'Branch', text: 'Branch' },
        { value: 'Region', text: 'Region' },
        { value: 'ProductServiceGroup', text: 'Product Group' }
    ];

    public showTypeByBranch: Array<Record<string, string>> = [
        { value: 'ProductServiceGroup', text: 'Product Group' },
        { value: 'BranchServiceAreaCode', text: 'Service Area' }
    ];

    public selectLevel: Array<Record<string, string>> = [
        { value: 'Branch', text: 'Branch' },
        { value: 'ServiceArea', text: 'Service Area' }
    ];

    public setDateSelect(showTypeSelected: any, enableJobsToInvoiceAfterVisit: any): void {
        if (showTypeSelected === 'Job') {
            this.dateSelectValues = [
                { text: 'Exclude From Date', value: 'ExcludeFromDate' }
            ];
            if (enableJobsToInvoiceAfterVisit) {
                this.filterValues = [
                    { text: 'All', value: 'All' },
                    { text: 'Not Completed', value: 'NotCompleted' },
                    { text: 'No Visits', value: 'NoVisits' },
                    { text: 'Not Released', value: 'NotReleased' }
                ];
            } else {
                this.filterValues = [
                    { text: 'All', value: 'All' },
                    { text: 'Not Completed', value: 'NotCompleted' },
                    { text: 'No Visits', value: 'NoVisits' }
                ];
            }
        } else {
            this.dateSelectValues = [
                { text: 'Specify From Date', value: 'SpecifyDate' },
                { text: 'Use Anniversary Date', value: 'AnniversaryDate' },
                { text: 'Rolling 12 Months', value: 'Rolling12Months' }
            ];
            this.filterValues = [
                { text: 'All', value: 'All' },
                { text: 'Ahead', value: 'Ahead' },
                { text: 'Behind', value: 'Behind' },
                { text: 'No Visits', value: 'NoVisits' }
            ];
        }
    }

    public getReportGroupData(): void {
        this.lookupUtils.getReportGroup().then((data) => {
            this.reportGroup = data[0];
        });
    }

    public setSeasonalOptions(showType: string): void {
        if (showType === 'Job') {
            this.seasonalOptions = this.jobSeasonalOptions;
        } else {
            this.seasonalOptions = this.contractSeasonalOptions;
        }
    }
}
