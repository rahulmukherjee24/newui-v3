import { AppWebWorker } from '@shared/web-worker/app-worker';
import { CommonLookUpUtilsService } from './../../../../shared/services/commonLookupUtils.service';
import { Component, OnInit, Injector, AfterContentInit, ViewChild } from '@angular/core';
import { DropdownConstants } from './../../../base/ComponentConstants';
import { EmployeeSearchComponent } from '@internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { IGridHandlers } from './../../../base/BaseComponentLight';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { RefreshWorker } from '@shared/components/grid-advanced/refresh-worker';
import { SOSDropDownService } from './sosDropDown.service';
import { SysCharConstants } from './../../../../shared/constants/syscharservice.constant';
import { WebWorkerService } from '@shared/web-worker/web-worker';
import { BIReportsRoutes } from '@app/base/PageRoutes';


@Component({
    templateUrl: 'iCABSSeAgedArrearsReportsGrid.html',
    providers: [SOSDropDownService, CommonLookUpUtilsService]
})

export class AgedArrearsReportsGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private businessDesc: string;
    protected controls: IControls[] = [
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeInteger, disabled: true },
        { name: 'ShowType', type: MntConst.eTypeText, value: 'Contract' },
        { name: 'ServiceTypes', type: MntConst.eTypeText },
        { name: 'ProductServiceGroups', type: MntConst.eTypeText },
        { name: 'AgedMonth', type: MntConst.eTypeInteger, value: new Date().getMonth() + 1 },
        { name: 'AgedYear', type: MntConst.eTypeInteger, value: new Date().getFullYear() },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'CompanyCode', type: MntConst.eTypeCode },
        { name: 'CompanyDesc', type: MntConst.eTypeText }
    ];
    protected pageId: string;
    public pageType: string = '';
    public companySearch: Record<string, object> = {
        inputParams: {
            parentMode: 'LookUp',
            countryCode: this.countryCode(),
            businessCode: this.businessCode()
        }
    };
    public companyInputParams: any = {
    };
    public companyDefault: Object = {
        id: '',
        text: ''
    };
    public serviceTypeDefault: Object;
    public productGroupDefault: Object;
    public dropdown: Record<string, object> = {
        serviceType: {
            params: {
                'parentMode': 'LookUpMultiple'
            },
            type: 'InServicMntConst.eTypeCode'
        },
        employee: {
            type: 'SupervisorEmployeeCode',
            parentMode: 'LookUp-Supervisor'
        },
        productServiceGroup: {
            params: {
                parentMode: 'LookUp',
                ProductServiceGroupString: '',
                SearchValue3: '',
                ProductServiceGroupCode: ''
            },
            active: { id: '', text: '' },
            isDisabled: false,
            isRequired: false
        }
    };
    public gridConfig: Record<string, any> = {
        pageSize: 10,
        totalRecords: 1,
        actionGrid: '2',
        actionBatch: '6',
        riSortOrder: ''
    };
    public isBlnAgedReport: boolean;
    public employeeSearchComponent = EmployeeSearchComponent;
    public URLReturn: string;

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService, public sosService: SOSDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEAGEDARREARSREPORTGRIDBUSINESS;
        this.companyInputParams[this.serviceConstants.CountryCode] = this.countryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.businessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.businessDesc = this.utils.getBusinessText();
        this.pageType = this.riExchange.getParentHTMLValue('type');
        this.browserTitle = this.pageTitle = this.pageType + ' Aged Arrears Reports - ' + this.businessDesc;
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.pageParams.gridCacheRefresh = !this.isReturning();
        this.onWindowLoad();
        this.checkTimeOut();
    }

    public checkTimeOut(): void {
        let worker: AppWebWorker = new AppWebWorker();
        let workerService: WebWorkerService;
        workerService = new WebWorkerService(RefreshWorker);
        workerService.run(worker.delegatePromise, {}, true);
        workerService.worker.onmessage = response => {
            this.onRiGridRefresh();
        };
    }

    private onWindowLoad(): void {
        this.setControlValue('BusinessDesc', this.businessDesc);
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        this.getSysChars();
        this.isBlnAgedReport = this.riExchange.getParentHTMLValue('Aged') || this.riExchange.getParentHTMLValue('AgedSOS') ? true : false;
        if (this.isReturning()) {
            this.serviceTypeDefault = {
                id: '',
                text: '',
                multiSelectDisplay: this.getControlValue('ServiceTypes')
            };
            this.productGroupDefault = {
                id: '',
                text: '',
                multiSelectDisplay: this.getControlValue('ProductServiceGroups')
            };
            let companyCode: string = this.getControlValue('CompanyCode');
            if (companyCode) {
                this.companyDefault = {
                    id: companyCode,
                    text: companyCode + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            this.onRiGridRefresh();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('ReportNumber', 'StateOfService', 'ReportNumber', MntConst.eTypeInteger, 15, true, 'Report Number');
        this.riGrid.AddColumnAlign('ReportNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GeneratedDate', 'StateofService', 'GeneratedDate', MntConst.eTypeDate, 10, false, 'Report Generated On');
        this.riGrid.AddColumnAlign('GeneratedDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('GeneratedTime', 'StateOfService', 'GeneratedTime', MntConst.eTypeTime, 10, false, 'Report Generated At');
        this.riGrid.AddColumnAlign('GeneratedTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('CompanyCode', 'StateOfService', 'CompanyCode', MntConst.eTypeCode, 2, false, 'Company Code');
        this.riGrid.AddColumnAlign('CompanyCode', MntConst.eAlignmentCenter);

        if (this.pageType === 'Branch') {
            this.riGrid.AddColumn('SupervisorEmployeeCode', 'StateOfService', 'SupervisorEmployeeCode', MntConst.eTypeCode, 6, false, 'Supervisor Employee');
            this.riGrid.AddColumnAlign('SupervisorEmployeeCode', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('ServicMntConst.eTypeCode', 'StateOfService', 'ServicMntConst.eTypeCode', MntConst.eTypeCode, 6, false, 'Service Type Code');
        this.riGrid.AddColumnAlign('ServicMntConst.eTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitFreq', 'StateOfService', 'VisitFreq', MntConst.eTypeCode, 6, false, 'Visit Frequency');
        this.riGrid.AddColumnAlign('VisitFreq', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProductServiceGroupCode', 'StateOfService', 'ProductServiceGroupCode', MntConst.eTypeCode, 6, false, 'Product Group Code');
        this.riGrid.AddColumnAlign('ProductServiceGroupCode', MntConst.eAlignmentCenter);

        if (this.getControlValue('ShowType') === 'Contract') {
            this.riGrid.AddColumn('DateFrom', 'StateOfService', 'DateFrom', MntConst.eTypeDate, 10, false, 'From Date');
            this.riGrid.AddColumnAlign('DateFrom', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('DateTo', 'StateOfService', 'DateTo', MntConst.eTypeDate, 10, false, 'To Date');
            this.riGrid.AddColumnAlign('DateTo', MntConst.eAlignmentCenter);
        } else {
            this.riGrid.AddColumn('ExcludeFrom', 'StateOfService', 'ExcludeFrom', MntConst.eTypeDate, 10, false, 'Exclude From Date');
            this.riGrid.AddColumnAlign('ExcludeFrom', MntConst.eAlignmentCenter);
        }
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let form: Record<string, any> = {};
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionGrid.toString());
        form['Level'] = this.pageType;
        form['BusinessCode'] = this.utils.getBusinessCode();
        form['CompanyCode'] = this.getControlValue('CompanyCode');
        form['ServiceTypeCode'] = this.getControlValue('ServiceTypes');
        form['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroups');
        form['AgedMonth'] = this.getControlValue('AgedMonth');
        form['AgedYear'] = this.getControlValue('AgedYear');
        form['DateSelect'] = 'SpecifyDate';
        form['ShowType'] = this.getControlValue('ShowType');
        form['NumberOfVisits'] = '1';
        form['FilterType'] = 'Behind'; // As per present codebase, when the URL will contain 'Aged' or 'AgesSOS' then this needs to be changed to incorporate checking
        form['BatchBuildType'] = 'AgedArrears';
        form['SubmitBatchProcess'] = 'No';
        form['AgedPortfolio'] = 'Yes'; // As per present codebase, when the URL will contain 'Aged' or 'AgesSOS' then this needs to be changed to incorporate checking
        if (this.pageType === 'Branch') {
            form['BranchNumber'] = this.getControlValue('BranchNumber');
            form['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
        }
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeAgedArrearsReportsGrid', gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharEnableCompanyCode
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.enableCompanyCode = list[0].Required;
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    public onDataRecieved(data: Record<string, any>, type: string): void {
        if (data) {
            switch (type) {
                case 'serviceType':
                    this.setControlValue('ServiceTypes', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
                    this.resetValues();
                    break;
                case 'productServiceGroup':
                    this.setControlValue('ProductServiceGroups', data[DropdownConstants.c_s_KEY_MULTISELECTVALUE]);
                    this.resetValues();
                    break;
                case 'company':
                    this.setControlValue('CompanyCode', data.CompanyCode);
                    this.setControlValue('CompanyDesc', data.CompanyDesc);
                    this.resetValues();
                    break;
                case 'superVisorEmployeeCode':
                    this.setControlValue('SupervisorSurname', '');
                    this.setControlValue('SupervisorEmployeeCode', data.SupervisorEmployeeCode || this.getControlValue('SupervisorEmployeeCode'));
                    this.lookupUtils.getEmployeeSurname(this.getControlValue('SupervisorEmployeeCode')).then((records) => {
                        if (records || (records && records[0][0])) {
                            this.setControlValue('SupervisorSurname', records.SupervisorSurname || records[0][0].EmployeeSurname);
                        }
                    });
                    this.resetValues();
                    break;
            }
        }

    }

    public onRefreshClick(): void {
        this.pageParams.gridCacheRefresh = true;
        this.onRiGridRefresh();
    }

    public onRiGridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: Record<string, number>): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }

    public onGenerateReport(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionBatch.toString());
        gridSearch.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        gridSearch.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        gridSearch.set('SubmitBatchProcess', 'yes');
        gridSearch.set('AgedPortfolio', 'no');
        gridSearch.set('CompanyCode', this.getControlValue('CompanyCode'));
        gridSearch.set('AgedMonth', this.getControlValue('AgedMonth'));
        gridSearch.set('AgedYear', this.getControlValue('AgedYear'));
        gridSearch.set('ShowType', this.getControlValue('ShowType'));
        gridSearch.set('FilterType', 'Behind');
        gridSearch.set('DateSelect', 'SpecifyDate');
        gridSearch.set('BatchBuildType', 'AgedArrears');
        gridSearch.set('NumberOfVisits', '1');
        if (this.getControlValue('ServiceTypes')) {
            gridSearch.set('ServiceTypeCode', this.getControlValue('ServiceTypes'));
        }
        if (this.getControlValue('ProductServiceGroups')) {
            gridSearch.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroups'));
        }
        switch (this.pageType) {
            case 'Region':
                gridSearch.set('RegionCode', this.getControlValue('RegionCode')); // To be implemented in future
                break;
            case 'Branch':
                gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
                gridSearch.set('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode'));
                break;
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest('bi/reports', 'reports', 'Service/iCABSSeAgedArrearsReportsGrid', gridSearch)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                    } else {
                        this.URLReturn = data.BatchProcessInformation;
                    }
                },
                (error) => {
                    this.displayMessage(error);
                });
    }

    public onGridBodyDoubleClick(e: any): void {
        let params: Record<string, any> = {
            CompanyCode: this.getControlValue('CompanyCode'),
            CompanyDesc: this.getControlValue('CompanyDesc'),
            ServiceTypes: this.getControlValue('ServiceTypes'),
            ProductServiceGroups: this.getControlValue('ProductServiceGroups'),
            ReportNumber: this.riGrid.Details.GetValue('ReportNumber'),
            ROWID: this.riGrid.Details.GetAttribute('ReportNumber', 'rowid'),
            ShowType: this.getControlValue('ShowType')
        };
        if (this.pageType === 'Business' && this.riGrid.CurrentColumnName === 'ReportNumber') {
            this.navigate('Business', BIReportsRoutes.ICABSSEBATCHSOSAGEDARREARSBUSINESSGRID, params);
        }
        if (this.pageType === 'Branch' && this.riGrid.CurrentColumnName === 'ReportNumber') {
            params.Branch = this.getControlValue('BranchNumber');
            this.navigate('Branch', BIReportsRoutes.ICABSSEBATCHSOSAGEDARREARSBRANCHGRID, params);
        }
    }

    public resetValues(): void {
        this.pageParams.gridCurrentPage = 1;
        this.gridConfig.totalRecords = 1;
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
    }
}
