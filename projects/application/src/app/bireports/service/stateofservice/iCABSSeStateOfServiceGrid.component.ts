import { Component, Injector, OnInit, AfterContentInit, ViewChild, OnDestroy } from '@angular/core';

import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IGridHandlers } from '@base/BaseComponentLight';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SOSDropDownService } from './sosDropDown.service';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeStateOfServiceGrid.html',
    providers: [CommonLookUpUtilsService, SOSDropDownService]
})

export class StateOfServiceGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    /**
     * Kept Here Since Handled By SysChars
     */
    private reportViewOptionsDetails: any = {
        '1': { value: 'Values', text: 'Values' },
        '2': { value: 'Exchanges', text: 'Exchanges' },
        '3': { value: 'Times', text: 'Times' }
    };

    protected controls: any[] = [
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaCode', disabled: true, type: MntConst.eTypeText },
        { name: 'chkDoneBy', disabled: true, type: MntConst.eTypeCheckBox },
        { name: 'CompanyCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'CompanyDesc', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'DateDone', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateFrom', disabled: true, type: MntConst.eTypeDate },
        { name: 'DateSelect', disabled: true, type: MntConst.eTypeText },
        { name: 'DateTo', disabled: true, type: MntConst.eTypeDate },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'FilterType', disabled: true, type: MntConst.eTypeText },
        { name: 'NumberOfVisits', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ProductServiceGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ReportGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ReportNumber', type: MntConst.eTypeText },
        { name: 'ReportView', type: MntConst.eTypeText },
        { name: 'ROWID', type: MntConst.eTypeText },
        { name: 'SeasonalSelect', type: MntConst.eTypeText },
        { name: 'ServiceTypeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ShowPortfolioType', disabled: true, type: MntConst.eTypeText },
        { name: 'ShowType', disabled: true, type: MntConst.eTypeText },
        { name: 'SupervisorEmployeeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'SupervisorSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ViewTotal', type: MntConst.eTypeText },
        { name: 'VisitFrequency', disabled: true, type: MntConst.eTypeInteger }
    ];
    protected pageId: string;

    public blnAgedReport: boolean = false;
    public filterType: string = '';
    public gridConfig: any = {
        totalItem: 0,
        itemsPerPage: 10
    };
    public isDoneByChecked: boolean = false;

    constructor(injector: Injector, private syscharConstants: SysCharConstants, public dropdowns: SOSDropDownService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESTATEOFSERVICEGRID;
        this.browserTitle = this.pageTitle = 'State Of Service';
    }

    // #region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();

        /**
         * @todo - Need To Find A Way To Stop LookUp Call While Returning
         */
        if (!this.dropdowns.reportGroup.length) {
            this.dropdowns.getReportGroupData();
        }
        this.blnAgedReport = this.parentMode === 'BatchStateOfServiceBranchAged';
        this.disableControl('ShowType', !this.blnAgedReport);
        if (this.isReturning()) {
            this.dropdowns.setSeasonalOptions(this.getControlValue('ShowType'));
            this.buildGrid();
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.reportViewOptions = [];
            this.pageParams.fromDateShow = true;
            this.getSyschars();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    // #endregion

    // #region Private Methods
    private getSyschars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharEnableInstallsRemovals,
            this.syscharConstants.SystemCharEnableJobsToInvoiceAfterVisit,
            this.syscharConstants.SystemCharEnableDoneByDate,
            this.syscharConstants.SystemCharEnableCompanyCode,
            this.syscharConstants.SystemCharEnableTimePlanning,
            this.syscharConstants.SystemCharStateOfServiceReportView
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.enableInstallsRemovals = list[0].Required;
                this.pageParams.enableJobsToInvoiceAfterVisit = list[1].Required;
                this.pageParams.enableDoneByDate = list[2].Required;
                this.pageParams.enableCompanyCode = list[3].Required;
                this.pageParams.enableTimePlanning = list[4].Required;
                this.pageParams.stateOfServiceReportView = list[5].Required ? list[5].Text : '1,2,3';

                if (!list[5].Required) {
                    if (!this.pageParams.enableInstallsRemovals) {
                        this.pageParams.stateOfServiceReportView = '1';
                    } else {
                        this.pageParams.stateOfServiceReportView = '1,2';
                    }
                }

                this.buildReportViewOptions();
                /** Other Parent Modes Are Not Hanndled Since Those Are Not Being Developed */
                if (this.parentMode === 'BatchStateOfServiceBranch' || this.parentMode === 'BatchStateOfServiceBranchAged') {
                    this.setControlValues();
                    this.buildGrid();
                    this.onRiGridRefresh();
                }
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private buildReportViewOptions(): void {
        let list: Array<string> = this.pageParams.stateOfServiceReportView.split(',');

        list.forEach(item => {
            this.pageParams.reportViewOptions.push(this.reportViewOptionsDetails[item]);
        });
    }

    private setControlValues(): void {
        this.controls.forEach(control => {
            /**
             * Need To Update Control Only If It Does Not Have Values
             * Disabld ts-rule Purposefully
             */
            // tslint:disable-next-line: no-unused-expression
            this.getControlValue(control.name) || this.riExchange.getParentHTMLValue(control.name);
        });

        if (this.parentMode === 'BatchStateOfServiceBranch') {
            this.filterType = this.getControlValue('FilterType');
            this.isDoneByChecked = this.getControlValue('chkDoneBy');
            if (!this.isDoneByChecked) {
                this.setControlValue('DateDone', '');
            }
        }


        if (this.parentMode === 'BatchStateOfServiceBranchAged') {
            this.setControlValue('NumberOfVisits', 1);
            this.setControlValue('FilterType', 'Behind');
            this.setControlValue('ReportView', 'Values');
        }

        this.dropdowns.setSeasonalOptions(this.getControlValue('ShowType'));
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        let fieldLength: number = !this.getControlValue('BranchNumber') || !this.getControlValue('BranchServiceAreaCode') ? 15 : 6;

        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'StateOfService', 'BranchServiceAreaSeqNo', MntConst.eTypeText, fieldLength, true);
        this.riGrid.AddColumn('ContractPremise', 'StateOfService', 'ContractPremise', MntConst.eTypeText, 14);
        this.riGrid.AddColumn('PremiseName', 'StateOfService', 'PremiseName', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('ProductCode', 'StateOfService', 'ProductCode', MntConst.eTypeCode, 6);

        /**
         * This Will Only Be Called From Here If Page Is Not Returning
         * Used Three Different Methods For Better Maintenance Purpose
         */
        this['buildGrid' + this.getControlValue('ReportView')]();

        this.riGrid.AddColumn('PremiseServiceNote', 'StateOfService', 'PremiseServiceNote', MntConst.eTypeImage, 1);

        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ContractPremise', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseServiceNote', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);

        this.riGrid.Complete();

        this.dateCollist = this.riGrid.getColumnIndexList([
            'ServiceVisitAnnivDate',
            'ServiceCommenceDate',
            'LastRoutine'
        ], [3, 4, 5, 6, 7]);
    }

    /**
     * Different Build Grid Methods For Different Report View
     * Might Show As Ununsed Because Of How It Is Called
     */
    private buildGridValues(): void {
        this.riGrid.AddColumn('ServiceCoverDetail', 'StateOfService', 'ServiceCoverDetail', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'StateOfService', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('ServiceTypeCode', 'StateOfService', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumn('ProductServiceGroupCode', 'StateOfService', 'ProductServiceGroupCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumn('ServiceCommenceDate', 'StateOfService', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('LastRoutine', 'StateOfService', 'LastRoutine', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'StateOfService', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDueNumber', 'StateOfService', 'VisitsDueNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDueValue', 'StateOfService', 'VisitsDueValue', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('RoutineVisitsNumber', 'StateOfService', 'RoutineVisitsNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('OtherVisitsNumber', 'StateOfService', 'OtherVisitsNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('CreditVisitsNumber', 'StateOfService', 'CreditVisitsNumber', MntConst.eTypeInteger, 5);

        this.riGrid.AddColumnAlign('ServiceCoverDetail', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductServiceGroupCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('LastRoutine', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitsDueNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDueValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('RoutineVisitsNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('OtherVisitsNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('CreditVisitsNumber', MntConst.eAlignmentRight);
    }

    private buildGridExchanges(): void {
        this.riGrid.AddColumn('ServiceQuantity', 'StateOfService', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'StateOfService', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDue', 'StateOfService', 'VisitsDue', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDone', 'StateOfService', 'VisitsDone', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDifference', 'StateOfService', 'VisitsDifference', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('ExchangesDue', 'StateOfService', 'ExchangesDue', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesAheadNumber', 'StateOfService', 'ExchangesAheadNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesAheadValue', 'StateOfService', 'ExchangesAheadValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ExchangesBehindNumber', 'StateOfService', 'ExchangesAheadNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ExchangesBehindValue', 'StateOfService', 'ExchangesAheadValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('SuspendedExchanges', 'StateOfService', 'SuspendedExchanges', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SuspendedExchangesValue', 'StateOfService', 'SuspendedExchangesValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ExchangesDone', 'StateOfService', 'ExchangesDue', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('MissedExchanges', 'StateOfService', 'MissedExchanges', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SuspendedExchangesCreditNumber', 'StateOfService', 'SuspendedExchangesCreditNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('SuspendedExchangesCreditValue', 'StateOfService', 'SuspendedExchangesCreditValue', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'StateOfService', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);

        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitsDue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDone', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDifference', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('MissedExchanges', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);
    }

    private buildGridTimes(): void {
        this.riGrid.AddColumn('ServiceQuantity', 'StateOfService', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'StateOfService', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDue', 'StateOfService', 'VisitsDue', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDone', 'StateOfService', 'VisitsDone', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('VisitsDifference', 'StateOfService', 'VisitsDifference', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumn('VisitSecondsDue', 'StateOfService', 'VisitSecondsDue', MntConst.eTypeTextFree, 5);
        this.riGrid.AddColumn('VisitSecondsDone', 'StateOfService', 'VisitSecondsDone', MntConst.eTypeTextFree, 5);
        this.riGrid.AddColumn('VisitSecondsDiff', 'StateOfService', 'VisitSecondsDiff', MntConst.eTypeTextFree, 5);
        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'StateOfService', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);

        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitsDue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDone', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('VisitsDifference', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);
    }
    // #endregion

    // #region Public Methods
    public onRiGridRefresh(): void {
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData['Level'] = 'Detail';
        formData['CompanyCode'] = this.getControlValue('CompanyCode');
        formData['BranchNumber'] = this.getControlValue('BranchNumber');
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['VisitFrequency'] = this.getControlValue('VisitFrequency');
        formData['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
        formData['DateSelect'] = this.getControlValue('DateSelect');
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        formData['DateDone'] = this.getControlValue('DateDone');
        formData['ShowType'] = this.getControlValue('ShowType');
        formData['ShowPortfolioType'] = this.getControlValue('ShowPortfolioType');
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['FilterType'] = this.getControlValue('FilterType');
        formData['NumberOfVisits'] = this.getControlValue('NumberOfVisits');
        formData['ValueBehind'] = this.blnAgedReport ? this.riExchange.getParentAttributeValue('ValueBehind') || '' : 0;
        formData['SeasonalSelect'] = this.getControlValue('SeasonalSelect');
        formData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
        formData['ReportView'] = this.getControlValue('ReportView');
        formData['ReportGroupCode'] = this.getControlValue('ReportGroupCode');
        formData['ViewTotal'] = this.getControlValue('ViewTotal');

        formData[this.serviceConstants.GridMode] = 0;
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridPageSize] = 10;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = 'BranchServiceAreaSeqNo';
        formData[this.serviceConstants.GridSortOrder] = 'Descending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeStateOfServiceGrid', gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onGridBodyDoubleClick(_event: any): void {
        if (this.riGrid.CurrentColumnName === 'BranchServiceAreaSeqNo') {
            let rowId: string = this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'rowid');
            this.setAttribute('ServiceCoverRowID', rowId);
            if (rowId !== 'TOTAL') {
                this.navigate('StateOfService', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    ServiceCoverRowID: rowId,
                    currentContractType: this.riGrid.Details.GetAttribute('BranchServiceAreaSeqNo', 'additionalproperty')
                });
            }
        }
    }

    public onshowTypeChanges(): void {
        this.resetGrid();
        let showType: any = this.getControlValue('ShowType');
        this.dropdowns.setDateSelect(showType, this.pageParams.enableJobsToInvoiceAfterVisit);
        this.setControlValue('DateSelect', showType === 'Job' ? this.dropdowns.dateSelectValues[0].value : this.riExchange.getParentHTMLValue('DateSelect'));
        this.dropdowns.setSeasonalOptions(showType);
        this.pageParams.fromDateShow = showType !== 'Job';
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

    public resetGrid(): void {
        this.gridConfig.totalItem = 0;
        this.pageParams.gridCurrentPage = 1;
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }
    // #endregion
}
