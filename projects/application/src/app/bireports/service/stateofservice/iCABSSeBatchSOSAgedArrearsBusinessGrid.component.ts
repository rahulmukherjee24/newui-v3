import { AfterContentInit, OnInit, Component, Injector, ViewChild } from '@angular/core';
import { BIReportsRoutes, ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SOSDropDownService } from './sosDropDown.service';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';


@Component({
    templateUrl: 'iCABSSeBatchSOSAgedArrearsBusinessGrid.html',
    providers: [SOSDropDownService, CommonLookUpUtilsService]
})

export class BatchSOSAgedArrearsBusinessGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    private businessDesc: string;
    protected pageId: string;
    protected controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'Company', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductServiceGroups', type: MntConst.eTypeText, disabled: true, value: this.riExchange.getParentHTMLValue('ProductServiceGroups') },
        { name: 'SeasonalSelect', type: MntConst.eTypeText, value: 'All' },
        { name: 'ServiceTypes', type: MntConst.eTypeText, disabled: true, value: this.riExchange.getParentHTMLValue('ServiceTypes') },
        { name: 'ViewBy', type: MntConst.eTypeText, value: 'Branch' }
    ];
    public parentMode: string = '';
    public showTypeBy: string;
    public companyInputParams: any = {
    };
    public companyDefault: Object = {
        id: '',
        text: ''
    };
    public companySearch: Record<string, object> = {
        inputParams: {
            parentMode: 'LookUp',
            countryCode: this.countryCode(),
            businessCode: this.businessCode()
        }
    };
    public dropdown: Record<string, object> = {
        serviceType: {
            params: {
                'parentMode': 'LookUpMultiple'
            },
            type: 'InServicMntConst.eTypeCode'
        },
        company: {
            type: 'CompanyCode'
        },
        employee: {
            type: 'SupervisorEmployeeCode',
            parentMode: 'LookUp-Supervisor'
        },
        productServiceGroup: {
            params: {
                parentMode: 'LookUp',
                ProductServiceGroupString: '',
                SearchValue3: '',
                ProductServiceGroupCode: ''
            },
            active: { id: '', text: '' },
            isDisabled: false,
            isRequired: false
        }
    };
    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService, public sosService: SOSDropDownService, private syscharConstants: SysCharConstants) {
        super(injector);
        this.companyInputParams[this.serviceConstants.CountryCode] = this.countryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.businessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    private onWindowLoad(): void {
        this.setControlValue('BusinessDesc', this.businessDesc);
        this.getSysChars();

        if (this.isReturning()) {
            if (this.riExchange.getParentHTMLValue('CompanyCode')) {
                this.setControlValue('Company', this.riExchange.getParentHTMLValue('CompanyCode') + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc'));
            }
        } else {
            if (this.riExchange.getParentHTMLValue('CompanyCode')) {
                this.setControlValue('Company', this.riExchange.getParentHTMLValue('CompanyCode') + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc'));
            } else {
                this.riExchange.getParentHTMLValue('Company');
            }
            this.setControlValue('ServiceTypes', this.riExchange.getParentHTMLValue('ServiceTypes'));
            this.setControlValue('ProductServiceGroups', this.riExchange.getParentHTMLValue('ProductServiceGroups'));

            if (this.parentMode === 'Branch') {
                this.setControlValue('ViewBy', 'ProductServiceGroup');
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.riExchange.getParentAttributeValue('Branch')));
            }

            if (this.parentMode === 'Detail') {
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.riExchange.getParentAttributeValue('Branch')));
                if (this.riExchange.getParentHTMLValue('ViewBy') === 'ProductServiceGroup') {
                    this.setControlValue('ProductServiceGroups', this.riExchange.getParentAttributeValue('ProductServiceGroup') || '');
                } else {
                    this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentAttributeValue('BranchServiceAreaDesc') || '');
                }
            }
        }

        this.onRiGridRefresh();
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharEnableCompanyCode
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.enableCompanyCode = list[0].Required;
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private buildGrid(): void {
        let viewBy = this.getControlValue('ViewBy');
        let colName = viewBy + (viewBy === 'Branch' ? 'Number' : 'Code');
        let strColName = viewBy + (viewBy === 'Branch' || viewBy === 'ProductServiceGroup' ? 'Name' : 'Desc');

        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;

        if (this.parentMode === 'Detail') {
            this.riGrid.AddColumn('ServiceCoverRef', 'StateOfService', 'ServiceCoverRef', MntConst.eTypeText, 20, true);
            this.riGrid.AddColumnOrderable('ServiceCoverRef', true);
            this.riGrid.AddColumn('PremiseName', 'StateOfService', 'PremiseName', MntConst.eTypeText, 30, true);
        } else {
            this.riGrid.AddColumn(colName, 'StateOfService', colName, viewBy === 'Region' ? MntConst.eTypeCode : MntConst.eTypeText, 6, true);
            this.riGrid.AddColumnAlign(colName, MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable(colName, true);
            this.riGrid.AddColumn(strColName, 'StateOfService', strColName, MntConst.eTypeText, 14);
            this.riGrid.AddColumnAlign(strColName, MntConst.eAlignmentLeft);
        }

        this.riGrid.AddColumn('TotalOver', 'StateOfService', 'TotalOver', MntConst.eTypeText, 8);
        this.riGrid.AddColumnAlign('TotalOver', MntConst.eAlignmentRight);

        for (let i = 1; i < 7; i++) {
            this.riGrid.AddColumn('Month' + i, 'StateOfService', 'Month' + i, MntConst.eTypeText, 8);
            this.riGrid.AddColumnAlign('Month' + i, MntConst.eAlignmentRight);
        }

        this.riGrid.AddColumn('OverMonth6', 'StateOfService', 'OverMonth6', MntConst.eTypeText, 8);
        this.riGrid.AddColumnAlign('OverMonth6', MntConst.eAlignmentRight);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let form: Record<string, any> = {};
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.actionGrid.toString());
        form[this.serviceConstants.Level] = this.parentMode;
        form['ReportNumber'] = this.riExchange.getParentHTMLValue('ReportNumber');
        form['RowID'] = this.riExchange.getParentHTMLValue('ROWID');
        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['CompanyCode'] = this.getControlValue('CompanyCode') || this.getControlValue('Company').split('-')[0].trim();
        if (this.parentMode !== 'Business') {
            form[this.serviceConstants.BranchNumber] = this.riExchange.getParentAttributeValue('Branch');
        }
        if (this.parentMode === 'Detail') {
            form['ServiceTypeCode'] = this.getControlValue('ServiceTypes');
            form['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroups');
            form['BranchServiceAreaCode'] = this.riExchange.getParentAttributeValue('BranchServiceAreaCode');
        }
        form['ViewBy'] = this.getControlValue('ViewBy');
        form['ShowType'] = this.riExchange.getParentHTMLValue('ShowType');
        form['SeasonalSelect'] = this.getControlValue('SeasonalSelect');
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeBatchSOSAgedArrears' + this.parentMode + 'Grid', gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }

    public gridConfig: Record<string, any> = {
        pageSize: 10,
        totalRecords: 1,
        actionGrid: '2',
        actionBatch: '6',
        riSortOrder: ''
    };

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageId = (this.parentMode === 'Business') ? PageIdentifier.ICABSSEBATCHSOSAGEDARREARSBUSINESSGRID : (this.parentMode === 'Branch') ? PageIdentifier.ICABSSEBATCHSOSAGEDARREARSBRANCHGRID : PageIdentifier.ICABSSEBATCHSOSAGEDARREARSDETAILGRID;
        super.ngAfterContentInit();
        this.businessDesc = this.utils.getBusinessText();
        this.parentMode = this.riExchange.getParentHTMLValue('parentMode');
        this.browserTitle = this.pageTitle = this.parentMode + ' Aged Arrears ' + this.businessDesc;
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.pageParams.gridCacheRefresh = !this.isReturning();
        this.showTypeBy = this.sosService['showTypeBy' + (this.parentMode === 'Branch' ? 'Branch' : '')];
        this.onWindowLoad();
    }

    public onRefreshClick(): void {
        this.pageParams.gridCacheRefresh = true;
        this.onRiGridRefresh();
    }

    public onRiGridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: Record<string, number>): void {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        }
    }

    public onGridBodyDoubleClick(e: any): void {
        let viewBy: string = this.getControlValue('ViewBy');
        let params: Record<string, string> = {
            ReportNumber: this.riExchange.getParentHTMLValue('ReportNumber'),
            ROWID: this.riExchange.getParentHTMLValue('ROWID'),
            ShowType: this.riExchange.getParentHTMLValue('ShowType')
        };

        if (this.parentMode === 'Business') {
            let branchNumber = this.riGrid.Details.GetAttribute('BranchNumber', 'rowid');
            if (this.riGrid.CurrentColumnName === 'BranchNumber' && branchNumber && branchNumber !== 'TOTAL') {
                params.Branch = this.riGrid.Details.GetValue('BranchNumber');

                if (viewBy === 'Branch') {
                    this.navigate('Branch', BIReportsRoutes.ICABSSEBATCHSOSAGEDARREARSBRANCHGRID, params);
                }
            }
        } else if (this.parentMode === 'Branch') {
            params.Branch = this.riExchange.getParentHTMLValue('Branch');
            let currentRow = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid');
            if (!currentRow || currentRow === 'TOTAL') {
                return;
            } else {
                if (viewBy === 'ProductServiceGroup') {
                    params.ProductServiceGroup = this.riGrid.Details.GetAttribute('ProductServiceGroupCode', 'rowid');
                } else {
                    params.BranchServiceAreaCode = this.riGrid.Details.GetAttribute('BranchServiceAreaCodeCode', 'rowid');
                    params.BranchServiceAreaDesc = this.riGrid.Details.GetValue('BranchServiceAreaCodeDesc');
                }

                this.navigate('Detail', BIReportsRoutes.ICABSSEBATCHSOSAGEDARREARSDETAILGRID, params);
            }
        } else if (this.parentMode === 'Detail') {
            let currentRow = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid');
            let selectedContractTypeCode = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty');
            if (!currentRow || currentRow === 'TOTAL') {
                return;
            } else {
                this.navigate('PortfolioNoTurnover', selectedContractTypeCode === 'C' ? ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT : ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB, {
                    'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ServiceCoverRef', 'rowid'),
                    currentContractType: selectedContractTypeCode
                });
            }
        }
    }

}
