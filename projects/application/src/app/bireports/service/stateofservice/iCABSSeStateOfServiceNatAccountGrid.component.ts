import { Component, Injector, OnInit, ViewChild, AfterContentInit } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeStateOfServiceNatAccountGrid.html',
    providers: [CommonLookUpUtilsService]
})
export class StateOfServiceNetAccountGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public pageId: string;
    public pageTitle: string;
    public controls: IControls[] = [
        { name: 'ContractName', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'DateDone', required: false, type: MntConst.eTypeDate },
        { name: 'DateFrom', required: false, type: MntConst.eTypeDate },
        { name: 'DateSelect', disabled: false, required: true, value: 'SpecifyDate' },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'FilterType', disabled: false, required: true, value: 'All' },
        { name: 'NumberOfVisits', disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'ProductServiceGroupCode' },
        { name: 'ProductServiceGroupDesc' },
        { name: 'ReportView' },
        { name: 'ServiceTypeCode' },
        { name: 'ServiceTypeDesc' },
        { name: 'chkDoneBy', required: false, value: false, type: MntConst.eTypeCheckBox }
    ];
    public isCheckboxDoneByChecked: boolean = false;
    public commonGridFunction: CommonGridFunction;
    public ellipsis: Record<string, Record<string, Object>> = {
        contract: {
            imputParams: {
                parentMode: 'LookUp'
            },
            component: ContractSearchComponent
        }
    };

    public dropdown: Record<string, Record<string, Object>> = {
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUpMultiple'
            },
            active: {
                id: '',
                text: ''
            }
        },
        productServiceGroupSearch: {
            params: {
                parentMode: 'LookUpMultiple',
                ProductServiceGroupCode: ''
            },
            active: {
                id: '',
                text: ''
            }
        }
    };

    constructor(injector: Injector, private sysCharConstants: SysCharConstants, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESTATEOFSERVICENATACCOUNTGRID;
        this.browserTitle = this.pageTitle = 'Contract State Of Service';
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                action: '2',
                gridHandle: this.utils.randomSixDigitString(),
                pageSize: 10,
                totalRecords: 1
            };
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
            if (this.parentMode === 'SOS') {
                this.riExchange.getParentHTMLValue('ContractNumber');
                this.riExchange.getParentHTMLValue('ContractName');
            }
            this.setDateToAndFromValue();
            this.loadSysChars();
        } else {
            this.pageParams.gridCacheRefresh = false;
            if (this.getControlValue('ServiceTypeCode')) {
                this.dropdown.serviceTypeSearch.active = {
                    id: this.getControlValue('ServiceTypeCode'),
                    text: this.getControlValue('ServiceTypeCode') + ' - ' + this.getControlValue('ServiceTypeDesc')
                };
            }
            if (this.getControlValue('ProductServiceGroupCode')) {
                this.dropdown.productServiceGroupSearch.active = {
                    id: this.getControlValue('ProductServiceGroupCode'),
                    text: this.getControlValue('ProductServiceGroupCode') + ' - ' + this.getControlValue('ProductServiceGroupDesc')
                };
            }
            this.populateGrid();
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableDoneByDate, this.sysCharConstants.SystemCharEnableInstallsRemovals].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data) {
                if (data.records[0]) {
                    this.pageParams.vShowDateDoneBy = data.records[0].Required;
                }
                if (data.records[1]) {
                    this.pageParams.vShowComboBox = data.records[1].Required;
                    this.setControlValue('ReportView', this.pageParams.vShowComboBox ? 'Exchanges' : 'Values');
                }
                this.populateGrid();
            }

        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    private setDateToAndFromValue(): void {
        let date: Date = new Date();
        switch (this.getControlValue('DateSelect')) {
            case 'SpecifyDate':
            case 'AnniversaryDate':
                this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), 0, 1)));
                this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth() + 1, 0)));
                break;
            case 'ExcludeFromDate':
                this.setControlValue('DateFrom', this.getControlValue('DateTo'));
                break;
        }

    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchNumber', 'StateOfService', 'BranchNumber', MntConst.eTypeText, 6, true);
        this.riGrid.AddColumn('BranchServiceAreaCode', 'StateOfService', 'BranchServiceAreaCode', MntConst.eTypeCode, 4);
        this.riGrid.AddColumn('EmployeeSurname', 'StateOfService', 'EmployeeSurname', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('PremiseNumber', 'StateOfService', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ProductCode', 'StateOfService', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        if (this.getControlValue('ReportView') === 'Values') {
            this.riGrid.AddColumn('ServiceCoverDetail', 'StateOfService', 'ServiceCoverDetail', MntConst.eTypeText, 5);
            this.riGrid.AddColumn('ServiceTypeCode', 'StateOfService', 'ServiceTypeCode', MntConst.eTypeCode, 2);
            this.riGrid.AddColumn('ProductServiceGroupCode', 'StateOfService', 'ProductServiceGroupCode', MntConst.eTypeCode, 2);
            this.riGrid.AddColumn('ServiceCommenceDate', 'StateOfService', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('ServiceVisitFrequency', 'StateOfService', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumn('VisitsDueNumber', 'StateOfService', 'VisitsDueNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('VisitsDueValue', 'StateOfService', 'VisitsDueValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumn('RoutineVisitsNumber', 'StateOfService', 'RoutineVisitsNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('OtherVisitsNumber', 'StateOfService', 'OtherVisitsNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('CreditVisitsNumber', 'StateOfService', 'CreditVisitsNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumnAlign('ServiceCoverDetail', MntConst.eAlignmentLeft);
            this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ProductServiceGroupCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('VisitsDueNumber', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('VisitsDueValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('RoutineVisitsNumber', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('OtherVisitsNumber', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('CreditVisitsNumber', MntConst.eAlignmentRight);

        } else {
            this.riGrid.AddColumn('ServiceQuantity', 'StateOfService', 'ServiceQuantity', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('ServiceVisitFrequency', 'StateOfService', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('VisitsDue', 'StateOfService', 'VisitsDue', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('VisitsDone', 'StateOfService', 'VisitsDone', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('VisitsDifference', 'StateOfService', 'VisitsDifference', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumn('ExchangesDue', 'StateOfService', 'ExchangesDue', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('ExchangesAheadNumber', 'StateOfService', 'ExchangesAheadNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('ExchangesAheadValue', 'StateOfService', 'ExchangesAheadValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumn('ExchangesBehindNumber', 'StateOfService', 'ExchangesAheadNumber', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('ExchangesBehindValue', 'StateOfService', 'ExchangesAheadValue', MntConst.eTypeCurrency, 10);
            this.riGrid.AddColumn('ExchangesDone', 'StateOfService', 'ExchangesDone', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('MissedExchanges', 'StateOfService', 'MissedExchanges', MntConst.eTypeInteger, 5);
            this.riGrid.AddColumn('ServiceVisitAnnivDate', 'StateOfService', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnAlign('VisitsDue', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('VisitsDone', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('VisitsDifference', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('ExchangesDue', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('ExchangesDone', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('MissedExchanges', MntConst.eAlignmentRight);
            this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumnOrderable('BranchNumber', true);
        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        if (this.uiForm.valid) {
            this.buildGrid();
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {
                'Level': 'NatAccount'
            };
            Object.keys(this.uiForm.controls).forEach(control => {
                formData = {
                    ...formData,
                    [control]: this.getControlValue(control)
                };
            });
            formData['BusinessCode'] = this.utils.getBusinessCode();
            formData['BranchNumber'] = this.utils.getBranchCode();
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = (this.pageParams.gridCurrentPage) ? this.pageParams.gridCurrentPage : '1';
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest('bi/reports', 'reports', 'Service/iCABSSeStateOfServiceNatAccountGrid', search, formData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (data.hasError) {
                        this.displayMessage(data);
                    } else {

                        this.riGrid.RefreshRequired();
                        this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });
        }
    }

    private getPromiseCall(funcName: string, displayfeild: string, queryField: string, event: Record<string, Object>): void {
        this.commonLookup[funcName](this.getControlValue(event['target']['id'])).then(data => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue(displayfeild, data[0][0][queryField]);
            } else {
                this.setControlValue(displayfeild, '');
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    public onCheckDoneByChnage(): void {
        this.isCheckboxDoneByChecked = this.getControlValue('chkDoneBy');
        let date: Date = new Date();
        this.setControlValue('DateDone', (this.pageParams.vShowDateDoneBy && this.isCheckboxDoneByChecked) ? this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth() + 1, 0)) : '');
    }

    public onContractDataReceived(event: Record<string, Object>): void {
        this.setControlValue('ContractNumber', event.ContractNumber);
        this.setControlValue('ContractName', event.ContractName);
    }

    public onDropDownSelect(event: Record<string, Object>): void {
        Object.keys(event).forEach(e => {
            this.setControlValue(e, event[e]);
        });
    }

    public onContractBlur(event: Record<string, Object>): void {
        this.getPromiseCall('getContractName', 'ContractName', 'ContractName', event);
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public onFilterTypeChange(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'NumberOfVisits', false);
        if (this.getControlValue('FilterType') === 'Ahead' || this.getControlValue('FilterType') === 'Behind') {
            this.setControlValue('NumberOfVisits', '1');
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'NumberOfVisits', true);
        }
    }

    public onGridBodyDoubleClick(): void {
        let rowId = this.riGrid.Details.GetAttribute('BranchNumber', 'rowid');
        if (this.riGrid.CurrentColumnName === 'BranchNumber' && rowId !== 'TOTAL') {
            this.navigate('ContractSoS', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                'currentContractType': this.riGrid.Details.GetAttribute('BranchNumber', 'additionalData'),
                'ServiceCoverRowID': this.riGrid.Details.GetAttribute('BranchNumber', 'rowid')
            });
        }
    }
}
