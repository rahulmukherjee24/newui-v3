import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';

import { BranchSearchComponent } from '@internal/search/iCABSBBranchSearch';
import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { IExportOptions } from '@app/base/ExportConfig';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSSeOutstandingGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class OutstandingGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;

    public commonGridFunction: CommonGridFunction;
    public exportConfig: IExportOptions = {};
    public hasGridData: boolean = false;
    public isBranch: boolean;
    public isBusiness: boolean;
    public pageMode: string;
    public pageType: string;

    public ellipsis: Record<string, Record<string, Object>> = {
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: BranchServiceAreaSearchComponent
        }
    };

    public dropdown: Record<string, Record<string, Object>> = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'iCABSOutstandingVisitReportGrids'
    };

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };


    public controls: IControls[] = [
        { name: 'BranchName', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'BusinessCode', required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractTypeCode', value: 'C' },
        { name: 'FilterBy', type: MntConst.eTypeText, value: 'All' },
        { name: 'FromDate', required: true, type: MntConst.eTypeDate },
        { name: 'ToDate', required: true, type: MntConst.eTypeDate }
    ];

    constructor(injector: Injector, private sysCharConstants: SysCharConstants, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getSysChars();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('pageType'));
        this.pageMode = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('mode'));
        this.pageId = PageIdentifier['ICABSSEOUTSTANDING' + this.pageMode.toUpperCase() + this.pageType.toUpperCase() + 'GRID'];

        this['is' + this.pageType] = this.pageType;

        this.browserTitle = this.pageTitle = 'Oustanding ' + this.pageMode;

        if (this.isBranch) {
            this.setControlValue('BranchNumber', this.utils.getBranchCode());
            this.setControlValue('BranchName', this.utils.getBranchTextOnly());
        } else if (this.isBusiness) {
            this.setControlValue('BusinessCode', this.utils.getBusinessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
        }

        let date: Date = new Date();
        let fromDate: Date = new Date(date.getFullYear(), date.getMonth(), 0);
        let toDate: Date = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        this.setControlValue('FromDate', this.globalize.parseDateToFixedFormat(fromDate));
        this.setControlValue('ToDate', this.globalize.parseDateToFixedFormat(toDate));

        this.buildGrid();

        this.pageParams.gridHandle = this.utils.randomSixDigitString();
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.gridCacheRefresh = true;
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        if (this.isBranch) {
            this.riGrid.AddColumn('ServiceArea', 'OutstandingVisit', 'ServiceArea', MntConst.eTypeCode, 6);
            this.riGrid.AddColumn('Premise', 'OutstandingVisit', 'Premise', MntConst.eTypeText, 20);
            this.riGrid.AddColumnAlign('Premise', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Address', 'OutstandingVisit', 'Address', MntConst.eTypeText, 40);
            this.riGrid.AddColumn('VisitType', 'OutstandingVisit', 'VisitType', MntConst.eTypeText, 40);
            this.riGrid.AddColumnAlign('VisitType', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Product', 'OutstandingVisit', 'Product', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('Product', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Qty', 'OutstandingVisit', 'Qty', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('Qty', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CommenceDate', 'OutstandingVisit', 'CommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('CommenceDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('VisitDate', 'OutstandingVisit', 'VisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentCenter);
        } else if (this.isBusiness) {
            this.riGrid.AddColumn('BranchNumber', 'OutstandingVisit', 'BranchNumber', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumn('BranchName', 'OutstandingVisit', 'BranchName', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('ServiceArea', 'OutstandingVisit', 'ServiceArea', MntConst.eTypeCode, 6);
            this.riGrid.AddColumn('Premise', 'OutstandingVisit', 'Premise', MntConst.eTypeText, 20);
            this.riGrid.AddColumnAlign('Premise', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Address', 'OutstandingVisit', 'Address', MntConst.eTypeText, 40);
            this.riGrid.AddColumn('VisitType', 'OutstandingVisit', 'VisitType', MntConst.eTypeText, 40);
            this.riGrid.AddColumnAlign('VisitType', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Product', 'OutstandingVisit', 'Product', MntConst.eTypeCode, 6);
            this.riGrid.AddColumnAlign('Product', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('Qty', 'OutstandingVisit', 'Qty', MntConst.eTypeInteger, 3);
            this.riGrid.AddColumnAlign('Qty', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CommenceDate', 'OutstandingVisit', 'CommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('CommenceDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('VisitDate', 'OutstandingVisit', 'VisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentCenter);
        }

        if (this.pageParams.vEnableDeliveryRelease) {
            this.riGrid.AddColumn('OutstandingValue', 'OutstandingVisit', 'OutstandingValue', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumnAlign('OutstandingValue', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumnOrderable('ServiceArea', true);
        this.riGrid.AddColumnOrderable('Premise', true);
        this.riGrid.AddColumnOrderable('CommenceDate', true);
        this.riGrid.AddColumnOrderable('VisitDate', true);

        this.riGrid.Complete();

        this.exportConfig = {};
        this.dateCollist = this.riGrid.getColumnIndexList([
            'CommenceDate',
            'VisitDate'
        ]);
        if (this.dateCollist.length) {
            this.exportConfig.dateColumns = this.dateCollist;
        }
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Function] = 'Outstanding' + this.pageMode;
        formData[this.serviceConstants.Level] = this.pageType;
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        if (this.isBranch) {
            formData[this.serviceConstants.BranchServiceAreaCode] = this.getControlValue('BranchServiceAreaCode');
        }
        formData['ToDate'] = this.getControlValue('ToDate');
        formData['FromDate'] = this.getControlValue('FromDate');
        if (this.pageMode === 'Installations') {
            formData['ContractType'] = this.getControlValue('ContractTypeCode');
        }
        formData['FilterBy'] = this.getControlValue('FilterBy');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onDataRcv(event: any, control: string): void {
        if (control === 'Service Area') {
            this.setControlValue('BranchServiceAreaCode', event.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', event.BranchServiceAreaDesc);
        } else if (control === 'Branch Search') {
            this.setControlValue('BranchNumber', event.BranchNumber);
            this.setControlValue('BranchName', event.BranchName);
        }
    }

    public onChnage(control: string): void {
        if (control === 'Service Area') {
            let serviceArea = this.getControlValue('BranchServiceAreaCode');
            if (!serviceArea) {
                this.setControlValue('BranchServiceAreaDesc', '');
            } else {
                this.lookupUtils.getBranchServiceAreaDesc(serviceArea, this.getControlValue('BranchNumber')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('BranchServiceAreaDesc', data[0][0].BranchServiceAreaDesc);
                    } else {
                        this.setControlValue('BranchServiceAreaCode', '');
                        this.setControlValue('BranchServiceAreaDesc', '');
                        this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area');
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
            }
        }
    }

    private getSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let charDeliverySysChar: QueryParams = this.getURLSearchParamObject();
        charDeliverySysChar.set(this.serviceConstants.Action, '0');
        charDeliverySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharDeliveryRelease].toString());
        this.httpService.sysCharRequest(charDeliverySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.records[0])
                this.pageParams.vEnableDeliveryRelease = data.records[0].Required;
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }
}
