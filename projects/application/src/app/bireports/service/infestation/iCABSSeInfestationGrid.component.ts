import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';

import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@base/PageRoutes';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { ProductCoverSearchComponent } from '@app/internal/search/iCABSBProductCoverSearch.component';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SalesAreaSearchComponent } from '@app/internal/search/iCABSBSalesAreaSearch.component';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';

@Component({
    templateUrl: 'iCABSSeInfestationGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class InfestationGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('serviceTypeSearch') serviceTypeSearch: ServiceTypeSearchComponent;

    private clearGrid: boolean = false;

    public blnNatAccount: boolean = false;
    public commonGridFunction: CommonGridFunction;
    public currentContractType: string;
    public hasGridData: boolean = false;
    public pageId: string;

    public controls: IControls[] = [
        { name: 'BranchNumber', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', required: false, type: MntConst.eTypeCode, value: '' },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'DeletedInd', type: MntConst.eTypeCheckBox },
        { name: 'DetailsInd', type: MntConst.eTypeCheckBox },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'ExcludeInfestInTreatPlan', type: MntConst.eTypeCheckBox },
        { name: 'ExcludeNoInfestInd', type: MntConst.eTypeCheckBox },
        { name: 'ExcludeProductInd', type: MntConst.eTypeCheckBox },
        { name: 'InfestationGroupCode', type: MntConst.eTypeCode, value: '' },
        { name: 'InfestationGroupDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'InfestationLocationCode', type: MntConst.eTypeCode, value: '' },
        { name: 'InfestationLocationDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'NumOfVisits', type: MntConst.eTypeInteger, value: 1 },
        { name: 'ProductDetailCodeString', type: MntConst.eTypeText },
        { name: 'SalesAreaCode', type: MntConst.eTypeCode },
        { name: 'SalesAreaDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'SelLevel', type: MntConst.eTypeCode, value: '' },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText },
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeText },
        { name: 'SupervisorSurname', disabled: true, type: MntConst.eTypeText }
    ];

    public dropdown: Record<string, Record<string, Object>> = {
        infestationGroupSearch: {
            params: {
                operation: 'Business/iCABSBInfestationGroupSearch',
                module: 'service',
                method: 'service-delivery/search'
            },
            displayFields: ['InfestationGroupCode', 'InfestationGroupDesc'],
            active: { id: '', text: '' }
        },
        infestationLocationSearch: {
            params: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBInfestationLocationSearch'
            },
            displayFields: ['InfestationLocationCode', 'InfestationLocationDesc'],
            active: { id: '', text: '' }
        },
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' }
        }
    };

    public ellipsis: Record<string, Record<string, Object>> = {
        contract: {
            childConfigParams: {
                parentMode: 'LookUp'
            },
            contentComponent: ContractSearchComponent
        },
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUp-Emp'
            },
            component: BranchServiceAreaSearchComponent
        },
        salesArea: {
            childparams: {
                'parentMode': 'LookUp'
            },
            component: SalesAreaSearchComponent
        },
        productCoverSearch: {
            childConfigParams: {
                parentMode: 'LookUp-String',
                productDetailCodeString: ''
            },
            contentComponent: ProductCoverSearchComponent
        },
        supervisorEmployee: {
            childparams: {
                'parentMode': 'LookUp-Supervisor-All'
            },
            component: EmployeeSearchComponent
        }
    };

    public selLevelValues: any = [
        { key: 'All', value: '' },
        { key: 'High', value: '1' },
        { key: 'Medium', value: '2' },
        { key: 'Low', value: '3' }
    ];

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSeInfestationGrid'
    };

    constructor(injector: Injector, private lookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSSEINFESTATIONGRID;
        this.browserTitle = this.pageTitle = 'Infestation';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.currentContractType = this.riExchange.getCurrentContractType();

        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.blnNatAccount = this.riExchange.URLParameterContains('NatAccount');
        this.setRequiredStatus('ContractNumber', this.blnNatAccount);

        this.buildGrid();

        if (this.isReturning()) {

            if (this.getControlValue('InfestationGroupCode')) {
                this.dropdown.infestationGroupSearch.active = {
                    id: this.getControlValue('InfestationGroupCode'),
                    text: this.getControlValue('InfestationGroupCode') + ' - ' + this.getControlValue('InfestationGroupDesc')
                };
            }
            if (this.getControlValue('InfestationLocationCode')) {
                this.dropdown.infestationLocationSearch.active = {
                    id: this.getControlValue('InfestationLocationCode'),
                    text: this.getControlValue('InfestationLocationCode') + ' - ' + this.getControlValue('InfestationLocationDesc')
                };
            }

            if (this.getControlValue('ServiceTypeCode')) {
                this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
            }
            this.populateGrid();
        } else {
            let date = new Date();
            let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            let currDay = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(firstDay) as string);
            this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(currDay) as string);
            this.setControlValue('ExcludeNoInfestInd', true);

            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        if (!this.blnNatAccount) {

            this.riGrid.AddColumn('BranchServiceAreaCode', 'Infestation', 'BranchServiceAreaCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'Infestation', 'BranchServiceAreaSeqNo', MntConst.eTypeText, 6);
            this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('ContractPremise', 'Infestation', 'ContractPremise', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('ContractPremise', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'Infestation', 'PremiseName', MntConst.eTypeText, 30);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('PremiseAddress', 'Infestation', 'PremiseAddress', MntConst.eTypeText, 30);
        this.riGrid.AddColumnScreen('PremiseAddress', false);

        this.riGrid.AddColumn('AddressLine1', 'Infestation', 'AddressLine1', MntConst.eTypeText, 30);
        this.riGrid.AddColumnScreen('AddressLine1', false);

        this.riGrid.AddColumn('ClientReference', 'Infestation', 'ClientReference', MntConst.eTypeText, 30);
        this.riGrid.AddColumnScreen('ClientReference', false);

        this.riGrid.AddColumn('ContactDetails', 'Infestation', 'ContactDetails', MntConst.eTypeText, 20);
        this.riGrid.AddColumnScreen('ContactDetails', false);

        this.riGrid.AddColumn('ProductCode', 'Infestation', 'ProductCode', MntConst.eTypeCode, 8, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceCoverDetail', 'Infestation', 'ServiceCoverDetail', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('ServiceCoverDetail', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ServiceTypeCode', 'Infestation', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'Infestation', 'VisitTypeCode', MntConst.eTypeCode, 4, true);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceDateStart', 'Infestation', 'ServiceDateStart', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeCode', 'Infestation', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('InfestationDetail', 'Infestation', 'InfestationDetail', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('InfestationDetail', MntConst.eAlignmentLeft);

        if (this.getControlValue('DetailsInd')) {
            this.riGrid.AddColumn('Level', 'Infestation', 'InfestationLevelDesc', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('Level', MntConst.eAlignmentLeft);

            this.riGrid.AddColumn('InfestationLocationDesc', 'Infestation', 'InfestationLocationDesc', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('InfestationLocationDesc', MntConst.eAlignmentLeft);

            if (!this.getControlValue('ExcludeInfestInTreatPlan')) {
                this.riGrid.AddColumn('TreatmentPlanID', 'Infestation', 'TreatmentPlanID', MntConst.eTypeInteger, 8);
                this.riGrid.AddColumnAlign('TreatmentPlanID', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('TreatmentPlanStatus', 'Infestation', 'TreatmentPlanStatus', MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign('TreatmentPlanStatus', MntConst.eAlignmentCenter);
                this.riGrid.AddColumn('TreatmentPlanCreateDate', 'Infestation', 'TreatmentPlanCreateDate', MntConst.eTypeDate, 10);
                this.riGrid.AddColumnAlign('TreatmentPlanCreateDate', MntConst.eAlignmentCenter);
            }
        }

        if (!this.blnNatAccount) {
            this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        }

        this.riGrid.Complete();
        this.dateCollist = this.riGrid.getColumnIndexListFromFull([
            'ServiceDateStart',
            'TreatmentPlanCreateDate'
        ]);
    }

    private populateGrid(): void {
        if (this.clearGrid) {
            this.buildGrid();
            this.clearGrid = false;
        }
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData[this.serviceConstants.BranchServiceAreaCode] = this.getControlValue('BranchServiceAreaCode');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['InfestationGroupCode'] = this.getControlValue('InfestationGroupCode');
        formData['InfestationLevel'] = this.getControlValue('SelLevel');
        formData['InfestationLocationCode'] = this.getControlValue('InfestationLocationCode');
        formData['DetailsInd'] = this.getControlValue('DetailsInd');
        formData['DeletedInd'] = this.getControlValue('DeletedInd');
        formData['ExcludeProductInd'] = this.getControlValue('ExcludeProductInd');
        formData['ExcludeNoInfestInd'] = this.getControlValue('ExcludeNoInfestInd');
        formData['ExcludeInfestInTreatPlan'] = this.getControlValue('ExcludeInfestInTreatPlan');
        formData['NumOfVisits'] = this.getControlValue('NumOfVisits');
        formData[this.serviceConstants.DateFrom] = this.getControlValue('DateFrom');
        formData[this.serviceConstants.DateTo] = this.getControlValue('DateTo');
        formData['ProductDetailCodeString'] = this.getControlValue('ProductDetailCodeString');
        formData['SalesAreaDesc'] = this.getControlValue('SalesAreaDesc');
        formData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onDateChange(): void {
        this.riGrid.RefreshRequired();
    }

    public onCheckboxChange(): void {
        this.clearGrid = true;
    }

    public onGridBodyDoubleClick?(): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'ProductCode':
                if (this.riGrid.Details.GetValue('ContractNumber') !== '') {
                    this.navigate('Infestation', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT, {
                        ServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCode', 'rowid'),
                        currentContractType: this.currentContractType
                    });
                }
                break;
            case 'VisitTypeCode':
                this.navigate('ServiceStatsAdjust', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                    'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProductCode', 'rowid'),
                    'ServiceVisitRowID': this.riGrid.Details.GetAttribute('VisitTypeCode', 'rowid')
                });
                break;
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

    public onDataRcv(event: any, value: string): void {
        switch (value) {
            case 'Service Area':
                this.setControlValue('BranchServiceAreaCode', event.BranchServiceAreaCode);
                this.setControlValue('EmployeeSurname', event.EmployeeSurname);
                break;
            case 'Sales Area':
                this.setControlValue('SalesAreaCode', event.SalesAreaCode);
                this.setControlValue('SalesAreaDesc', event.SalesAreaDesc);
                break;
            case 'Infestation Group Code':
                this.setControlValue('InfestationGroupCode', event.InfestationGroupCode);
                this.setControlValue('InfestationGroupDesc', event.InfestationGroupDesc);
                break;
            case 'Service Type Search':
                this.setControlValue('ServiceTypeCode', event.ServiceTypeCode);
                this.setControlValue('ServiceTypeDesc', event.ServiceTypeDesc);
                break;
            case 'Infestation Location':
                this.setControlValue('InfestationLocationCode', event.InfestationLocationCode);
                this.setControlValue('InfestationLocationDesc', event.InfestationLocationDesc);
                break;
            case 'Supervisor Employee':
                this.setControlValue('SupervisorEmployeeCode', event.SupervisorEmployeeCode);
                this.setControlValue('SupervisorSurname', event.SupervisorSurname);
                break;
            case 'Product Detail Code':
                this.setControlValue('ProductDetailCodeString', event.ProductDetailCodeString);
                this.ellipsis.productCoverSearch.childConfigParams['productDetailCodeString'] = this.getControlValue('ProductDetailCodeString');
                break;
            case 'Contract':
                this.setControlValue('ContractNumber', event.ContractNumber);
                this.setControlValue('ContractName', event.ContractName);
                break;
        }
    }

    public onValueChange(control: string): void {
        switch (control) {
            case 'Branch Service Area':
                this.setControlValue('EmployeeSurname', '');
                if (!this.getControlValue('BranchServiceAreaCode')) {
                    return;
                }
                this.lookupUtils.getBranchServiceAreaDescWithEmp(this.getControlValue('BranchServiceAreaCode'), this.getControlValue('BranchNumber')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.lookupUtils.getEmployeeSurname(data[0][0].EmployeeCode).then(res => {
                            if (res && res[0] && res[0][0]) {
                                this.setControlValue('EmployeeSurname', res[0][0].EmployeeSurname);
                            } else {
                                this.setControlValue('BranchServiceAreaCode', '');
                                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area Code');
                            }
                        }).catch(error => {
                            this.displayMessage(error);
                        });

                    } else {
                        this.setControlValue('BranchServiceAreaCode', '');
                        this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area Code');
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
                break;
            case 'Sales Area':
                this.setControlValue('SalesAreaDesc', '');
                if (!this.getControlValue('SalesAreaCode')) {
                    return;
                }
                this.lookupUtils.getSalesAreaDesc(this.getControlValue('SalesAreaCode'), this.getControlValue('BranchNumber')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('SalesAreaDesc', data[0][0].SalesAreaDesc);
                    } else {
                        this.setControlValue('SalesAreaDesc', '');
                        this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Sales Area');
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
                break;
            case 'Supervisor':
                this.setControlValue('SupervisorSurname', '');
                if (!this.getControlValue('SupervisorEmployeeCode')) {
                    return;
                }
                this.lookupUtils.getEmployeeSurname(this.getControlValue('SupervisorEmployeeCode')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('SupervisorSurname', data[0][0].EmployeeSurname);
                    } else {
                        this.setControlValue('SupervisorSurname', '');
                        this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Employee');
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
                break;
            case 'Contract':
                this.setControlValue('ContractName', '');
                if (!this.getControlValue('ContractNumber')) {
                    return;
                }
                this.lookupUtils.getContractName(this.getControlValue('ContractNumber')).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('ContractName', data[0][0].ContractName);
                    } else {
                        this.setControlValue('ContractNumber', '');
                    }
                });
                break;
        }
    }
}
