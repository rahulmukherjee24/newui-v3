import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';
import { BranchServiceAreaSearchComponent } from '@app/internal/search/iCABSBBranchServiceAreaSearch';
import { ProductCoverSearchComponent } from '@app/internal/search/iCABSBProductCoverSearch.component';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@app/base/PageRoutes';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { IExportOptions } from '@app/base/ExportConfig';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { MntConst } from '@shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSSeInfestationServiceCoverGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class InfestationServiceCoverGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;
    protected controls: IControls[] = [
        { name: 'BusinessCode' , type: MntConst.eTypeCode , required: true , disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeInteger , required: true , disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText , required: true , disabled: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText , disabled: true },
        { name: 'InfestationLocationCode', type: MntConst.eTypeInteger },
        { name: 'InfestationLocationDesc', type: MntConst.eTypeText , disabled: true },
        { name: 'DateFrom', type: MntConst.eTypeDate , required: true },
        { name: 'DateTo', type: MntConst.eTypeDate , required: true },
        { name: 'ProductDetailCodeString', type: MntConst.eTypeCode },
        { name: 'ShowType', type: MntConst.eTypeText, required: true, value: 'All'}
    ];

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public trigger: boolean = false;
    public expConfig: IExportOptions;

    public ellipseCofig: any = {
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: BranchServiceAreaSearchComponent
        },
        productCoverSearch: {
            childConfigParams: {
                parentMode: 'LookUp-String',
                productDetailCodeString: ''
            },
            contentComponent: ProductCoverSearchComponent
        }
    };

    public dropdownConfig: any = {
        infestationLocation: {
            params: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBInfestationLocationSearch'
            },
            displayFields: ['InfestationLocationCode', 'InfestationLocationDesc'],
            active: { id: '', text: '' }
        }
    };

    public showTypeList: Record<string,string>[] = [
        { value: 'All', text: 'All Premises' },
        { value: 'Infest', text: 'Infestations Only' }
    ];

    private reqParam: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSeInfestationServiceCoverGrid'
    };

    constructor(public injector: Injector, private commonLookUpUtil: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.browserTitle = 'Infestations (All)';
        this.pageTitle = 'All Infestations Summary';
        this.pageId = PageIdentifier.ICABSSEINFESTATIONSERVICECOVERGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (this.isReturning()) {
            if (this.getControlValue('InfestationLocationCode')) {
                this.dropdownConfig.infestationLocation.active = {
                    id: this.getControlValue('InfestationLocationCode'),
                    text: this.getControlValue('InfestationLocationCode') + ' - ' + this.getControlValue('InfestationLocationDesc')
                };
            }
            this.buildGrid();
            this.pageParams.gridCacheRefresh = false;
            this.riGrid.HeaderClickedColumn = this.pageParams.headerGirdColumn || '';
            this.riGrid.DescendingSort = this.pageParams.GridSortOrder === 'Descending';
            this.commonGridFunction.onRiGridRefresh();
        } else {
            if (this.parentMode === 'Business' || this.parentMode === 'Region') {
                this.setControlValue('BusinessCode', this.riExchange.getParentHTMLValue('BusinessCode'));
                this.setControlValue('BranchNumber', this.getAttribute('GroupCode'));
                this.setControlValue('BranchName', this.getAttribute('GroupDesc'));
                this.setControlValue('InfestationLocationCode', this.riExchange.getParentHTMLValue('InfestationLocationCode'));
                this.setControlValue('InfestationLocationDesc', this.riExchange.getParentHTMLValue('InfestationLocationDesc'));
                this.setControlValue('ProductDetailCodeString', this.riExchange.getParentHTMLValue('ProductDetailCodeString'));
                this.setControlValue('DateFrom', this.riExchange.getParentHTMLValue('DateFrom'));
                this.setControlValue('DateTo', this.riExchange.getParentHTMLValue('DateTo'));
            } else {
                let date = new Date();
                let firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
                this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(firstDate));
                this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(date));
                this.setControlValue('BusinessCode', this.utils.getBusinessCode());
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.setControlValue('BranchName', this.utils.getBranchTextOnly());
                }
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.GridSortOrder = 'Descending';
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1
            };
        }
        this.ellipseCofig.serviceArea.childConfigParams.ServiceBranchNumber = this.getControlValue('BranchNumber');
        this.ellipseCofig.serviceArea.childConfigParams.BranchName = this.getControlValue('BranchName');
        this.ellipseCofig.productCoverSearch.childConfigParams.productDetailCodeString = this.getControlValue('ProductDetailCodeString');
    }

    public onDataReceived(data: any, type: string): void {
        switch (type) {
            case 'ServiceAreaCode':
                    this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                    this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                break;
            case 'ProductDetail':
                    this.setControlValue('ProductDetailCodeString', data.ProductDetailCodeString ? data.ProductDetailCodeString : data);
                    this.ellipseCofig.productCoverSearch.childConfigParams.productDetailCodeString = this.getControlValue('ProductDetailCodeString');
                break;
            case 'InfestationLocation':
                    this.setControlValue('InfestationLocationCode', data.InfestationLocationCode);
                    this.setControlValue('InfestationLocationDesc', data.InfestationLocationDesc);
                break;
        }
        this.pageParams.gridCacheRefresh = true;
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        if (this.parentMode !== 'Business' && this.parentMode !== 'Region') {
            this.riGrid.AddColumn('BranchServiceAreaSeqNo','Infestation','BranchServiceAreaSeqNo',MntConst.eTypeText,6);
            this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo',MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('ContractPremise','Infestation','ContractPremise',MntConst.eTypeText,14);
        this.riGrid.AddColumnAlign('ContractPremise',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName','Infestation','PremiseName',MntConst.eTypeText,30);
        this.riGrid.AddColumnAlign('PremiseName',MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ContactDetails','Infestation','ContactDetails',MntConst.eTypeText,20);
        this.riGrid.AddColumnScreen('ContactDetails',false);
        this.riGrid.AddColumn('ProductCode','Infestation','ProductCode',MntConst.eTypeCode,8,true);
        this.riGrid.AddColumnAlign('ProductCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceAreaCode','Infestation','ServiceAreaCode',MntConst.eTypeText,5);
        this.riGrid.AddColumnAlign('ServiceAreaCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceAreaDesc','Infestation','ServiceAreaDesc',MntConst.eTypeText,20);
        this.riGrid.AddColumnScreen('ServiceAreaDesc',false);
        this.riGrid.AddColumnAlign('ServiceAreaDesc',MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ServiceCoverDetail','Infestation','ServiceCoverDetail',MntConst.eTypeText,5);
        this.riGrid.AddColumnAlign('ServiceCoverDetail',MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ServiceTypeCode','Infestation','ServiceTypeCode',MntConst.eTypeCode,2);
        this.riGrid.AddColumnAlign('ServiceTypeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitTypeCode','Infestation','VisitTypeCode',MntConst.eTypeCode,4,true);
        this.riGrid.AddColumnAlign('VisitTypeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceDateStart','Infestation','ServiceDateStart',MntConst.eTypeDate,10);
        this.riGrid.AddColumnAlign('ServiceDateStart',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeCode','Infestation','EmployeeCode',MntConst.eTypeCode,6);
        this.riGrid.AddColumnAlign('EmployeeCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('InfestationDetail','Infestation','InfestationDetail',MntConst.eTypeText,5);
        this.riGrid.AddColumnAlign('InfestationDetail',MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('LowInfestInd','Infestation','LowInfestInd',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('LowInfestInd',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('MediumInfestInd','Infestation','MediumInfestInd',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('MediumInfestInd',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('HighInfestInd','Infestation','HighInfestInd',MntConst.eTypeImage,1);
        this.riGrid.AddColumnAlign('HighInfestInd',MntConst.eAlignmentCenter);
        if (this.parentMode !== 'Business' && this.parentMode !== 'Region') {
            this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo',true);
        }
        this.riGrid.AddColumnOrderable('ContractPremise',true);
        this.riGrid.AddColumnOrderable('ServiceTypeCode',true);
        this.riGrid.AddColumnOrderable('VisitTypeCode',true);
        this.riGrid.AddColumnOrderable('ServiceDateStart',true);
        this.riGrid.AddColumnOrderable('LowInfestInd',true);
        this.riGrid.AddColumnOrderable('MediumInfestInd',true);
        this.riGrid.AddColumnOrderable('HighInfestInd',true);
        this.riGrid.Complete();
        this.expConfig = {
            dateColumns: this.riGrid.getColumnIndexListFromFull(['ServiceDateStart'])
        };
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.pageParams.gridCacheRefresh = true;
        this.commonGridFunction.onRiGridRefresh();
    }

    public populateGrid(): void {
        if (this.uiForm.valid) {
            let formArray = ['BusinessCode','BranchNumber','BranchServiceAreaCode','InfestationLocationCode','DateFrom','DateTo','ProductDetailCodeString','ShowType'];
            let queryParams: QueryParams = this.getURLSearchParamObject();
            queryParams.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
            queryParams.set(this.serviceConstants.Action,'2');
            let formData: Object = {
                Level: 'Detail'
            };
            formArray.forEach(control => {
                formData = {
                    ...formData,
                    [control]: this.getControlValue(control)
                };
            });
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.PageSize] = '10';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder ;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.reqParam.method, this.reqParam.module, this.reqParam.operation, queryParams, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = true;
                this.commonGridFunction.setPageData(data);
            }).catch(error => {
                this.hasGridData = false;
                this.displayMessage(error);
            });
        }
    }

    public onGridDblClk(): void {
        const columnName = this.riGrid.CurrentColumnName;
        const rowId = this.riGrid.Details.GetAttribute(columnName,'rowid');
        if (rowId) {
            switch (columnName) {
                case 'ProductCode':
                    this.setAttribute('ServiceCoverRowID', rowId);
                    this.navigate('Infestation', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT , { ServiceCoverRowID : rowId });
                    break;
                case 'VisitTypeCode':
                    this.setAttribute('ServiceVisitRowID', rowId);
                    this.navigate('Infestation', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE , { ServiceVisitRowID : rowId });
                    break;
            }
        }
    }

    public onHeaderClick(): void {
        this.pageParams.headerGirdColumn = this.riGrid.HeaderClickedColumn;
        this.pageParams.GridSortOrder = this.riGrid.SortOrder;
        this.onRiGridRefresh();
    }
}
