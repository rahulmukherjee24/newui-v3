import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '../../base/PageRoutes';
import { SharedModule } from '../../../shared/shared.module';

import { TechnicianWorkSummaryReportComponent } from './iCABSARTechnicianWorkSummaryReport.component';

@Component({
    template: `<router-outlet></router-outlet>
    `
})
export class PDABIReportsRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        SharedModule,
        HttpClientModule,
        RouterModule.forChild([
            {
                path: '', component: PDABIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARTECHNICIANWORKSUMMARYREPORT, component: TechnicianWorkSummaryReportComponent }
                ], data: { domain: 'BI REPORTS PDA' }
            }

        ])
    ],
    declarations: [
        PDABIReportsRootComponent,
        TechnicianWorkSummaryReportComponent
    ]

})

export class PDABIReportsModule {
}
