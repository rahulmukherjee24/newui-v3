import { AfterContentInit, Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';

import { QueryParams } from '@shared/services/http-params-wrapper';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CommonGridFunction } from '@base/CommonGridFunction';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { IExportOptions } from './../../base/ExportConfig';

@Component({
    templateUrl: 'riBatchGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class RiBatchComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.RIBATCHCOMPONENT;
    }

    public controls = [];
    public pageId: string;
    public commonGridFunction: CommonGridFunction;
    private submittedTime: any;
    public expCfg: IExportOptions = {};

    private ReportDateTime: string = '';

    public ngOnInit(): void {
        super.ngOnInit();
        this.submittedTime = this.riExchange.getParentHTMLValue('BatchProcessSubmittedTime');
        this.ReportDateTime = new Date(this.riExchange.getParentHTMLValue('BatchProcessSubmittedDate')).getMonth() + 1 + '/'
            + new Date(this.riExchange.getParentHTMLValue('BatchProcessSubmittedDate')).getDate() + '/'
            + new Date(this.riExchange.getParentHTMLValue('BatchProcessSubmittedDate')).getFullYear()
        + ' '
        + new Date(this.submittedTime * 1000).toISOString().substr(11, 8);

        this.pageTitle = this.browserTitle = 'Tax code report ' + this.ReportDateTime;
        this.pageParams.gridHandle = this.utils.randomSixDigitString();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.pageParams.gridConfig = {
            itemsPerPage: 10,
            totalItem: 1
        };

        this.onRiGridRefresh();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Column1BatchGrid','GridBatchGrid','Column1BatchGrid', MntConst.eTypeCode,10, false,'');
        this.riGrid.AddColumn('Column2BatchGrid','GridBatchGrid','Column2BatchGrid', MntConst.eTypeInteger,10, false,'');
        this.riGrid.AddColumn('Column3BatchGrid','GridBatchGrid','Column3BatchGrid', MntConst.eTypeTextFree,40, false,'');
        this.riGrid.AddColumn('Column4BatchGrid','GridBatchGrid','Column4BatchGrid', MntConst.eTypeTextFree,40, false,'');
        this.riGrid.AddColumn('Column5BatchGrid','GridBatchGrid','Column5BatchGrid', MntConst.eTypeTextFree,40, false,'');
        this.riGrid.AddColumn('Column6BatchGrid','GridBatchGrid','Column6BatchGrid', MntConst.eTypeCode,2, false,'');
        this.riGrid.AddColumn('Column7BatchGrid','GridBatchGrid','Column7BatchGrid', MntConst.eTypeDate,10, false,'');
        this.riGrid.AddColumn('Column8BatchGrid','GridBatchGrid','Column8BatchGrid', MntConst.eTypeDecimal2,10, false,'');
        this.riGrid.Complete();
        this.expCfg.dateColumns = this.riGrid.getColumnIndexListFromFull(['Column7BatchGrid']);
    }

    public populateGrid(): void {
        this.isRequesting = true;

        const search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            const formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            formData['ReportID'] = this.riExchange.getParentHTMLValue('ReportNumber');
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
            formData[this.serviceConstants.GridHeaderClickedColumn] = '';
            formData[this.serviceConstants.GridSortOrder] = 'Descending';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('it-functions/ri-model', 'batch-process', 'Model/riBatchGrid', search, formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.commonGridFunction.setPageData(data);
            }).catch(error => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.pageParams.hasGridData = false;
                    this.displayMessage(error);
            });
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }
}
