
import { AfterContentInit, OnInit, Component, Injector, ViewChild } from '@angular/core';

import { AccountSearchComponent } from '@internal/search/iCABSASAccountSearch';
import { ContractSearchComponent } from '@internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARNextInvoiceRunForecastDetailGrid.html'
})

export class NextInvoiceRunForecaseDetailGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public accountSearchComponent = AccountSearchComponent;
    public alertMessage: any;
    public pageId: string;
    public queryLookUp: QueryParams = new QueryParams();
    public controls: Array<Object> = [
        { name: 'BranchTypeFilter', value: 'N', disabled: true },
        { name: 'ContractNumber' },
        { name: 'ContractName', disabled: true },
        { name: 'BranchNumberFilter' },
        { name: 'BranchName' },
        { name: 'BusinessCode', disabled: true },
        { name: 'BusinessDesc', disabled: true },
        { name: 'InvoiceLevel', value: 'Con' },
        { name: 'AccountNumber' },
        { name: 'AccountName', disabled: true },
        { name: 'GeneratedDate', disabled: true },
        { name: 'GeneratedTime', disabled: true },
        { name: 'ForecastDate', disabled: true },
        { name: 'PageSize', value: 20 }
    ];

    public ellipsisConfig = {
        contract: {
            childConfigParams: {
                'parentMode': 'LookUp-ProspectConversion'
            },
            contentComponent: ContractSearchComponent
        },
        account: {
            businessCode: this.businessCode(),
            accountNumber: '',
            accountName: '',
            showAddNewDisplay: false
        }
    };

    public gridConfig: any = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2',
        riSortOrder: ''
    };

    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        },
        parentMode: 'LookUp'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARNEXTINVOICERUNFORECASTDETAILGRID;
        this.browserTitle = this.pageTitle = 'Next Invoice Run Forecast';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.riExchange.getParentHTMLValue('GeneratedDate');
        this.riExchange.getParentHTMLValue('GeneratedTime');
        this.riExchange.getParentHTMLValue('ForecastDate');
        this.riExchange.getParentHTMLValue('GroupRowID');
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.utils.getBusinessDesc(this.utils.getBusinessCode(), this.countryCode()).subscribe((data) => {
            this.setControlValue('BusinessDesc', data.BusinessDesc);
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        let invoiceLevel: string = this.getControlValue('InvoiceLevel');
        /* The following AddColumn records are only for those columns that will be shown on screen. Export-Only columns are not included*/
        this.riGrid.AddColumn('BranchNumber', 'NextInvoiceRunForecast', 'BranchNumber', MntConst.eTypeInteger, 10, false);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('BranchNumber', true);
        if (invoiceLevel !== 'IG') {
            this.riGrid.AddColumn('InvContractNumber', 'NextInvoiceRunForecast', 'InvContractNumber', MntConst.eTypeText, 10, false);
            this.riGrid.AddColumnAlign('InvContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('InvContractNumber', true);
        }
        this.riGrid.AddColumn('InvAccountNumber', 'NextInvoiceRunForecast', 'InvAccountNumber', MntConst.eTypeText, 9, false);
        this.riGrid.AddColumnAlign('InvAccountNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('InvAccountNumber', true);
        this.riGrid.AddColumn('InvoiceCreditCode', 'NextInvoiceRunForecast', 'InvoiceCreditCode', MntConst.eTypeText, 1, false);
        this.riGrid.AddColumnAlign('InvoiceCreditCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('InvoiceCreditCode', true);
        this.riGrid.AddColumn('ContractName', 'NextInvoiceRunForecast', 'ContractName', MntConst.eTypeText, 40, false);
        this.riGrid.AddColumnAlign('ContractName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnNoWrap('ContractName', true);
        this.riGrid.AddColumn('InvoiceGroupName', 'NextInvoiceRunForecast', 'InvoiceGroupName', MntConst.eTypeText, 40, false);
        this.riGrid.AddColumnAlign('InvoiceGroupName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnNoWrap('InvoiceGroupName', true);
        this.riGrid.AddColumnScreen('InvoiceGroupName', false);
        if (invoiceLevel !== 'Con') {
            this.riGrid.AddColumn('NegBranchNumber', 'NextInvoiceRunForecast', 'NegBranchNumber', MntConst.eTypeInteger, 5, false);
            this.riGrid.AddColumnAlign('NegBranchNumber', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('PremiseNumber', 'NextInvoiceRunForecast', 'PremiseNumber', MntConst.eTypeText, 5, false);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentRight);
        }
        if (invoiceLevel.toLowerCase().toLowerCase() === 'prod') {
            this.riGrid.AddColumn('ProductCode', 'NextInvoiceRunForecast', 'ProductCode', MntConst.eTypeText, 40, false);
            this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ServiceVisitFrequency', 'NextInvoiceRunForecast', 'ServiceVisitFrequency', MntConst.eTypeInteger, 10, false);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentRight);
        }
        this.riGrid.AddColumn('ValueExclTax', 'NextInvoiceRunForecast', 'ValueExclTax', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumnAlign('ValueExclTax', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('TaxValue', 'NextInvoiceRunForecast', 'TaxValue', MntConst.eTypeText, 12, false);
        this.riGrid.AddColumnAlign('TaxValue', MntConst.eAlignmentRight);
        if (invoiceLevel.toUpperCase() !== 'IG') {
            this.riGrid.AddColumn('InvoiceFrequencyCode', 'NextInvoiceRunForecast', 'InvoiceFrequencyCode', MntConst.eTypeText, 2, false);
            this.riGrid.AddColumnAlign('InvoiceFrequencyCode', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('InvoiceAnnivDate', 'NextInvoiceRunForecast', 'InvoiceAnnivDate', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('InvoiceAnnivDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PeriodStart', 'NextInvoiceRunForecast', 'PeriodStart', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('PeriodStart', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PeriodEnd', 'NextInvoiceRunForecast', 'PeriodEnd', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('PeriodEnd', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('InvAdvanced', 'NextInvoiceRunForecasst', 'InvAdvanced', MntConst.eTypeImage, 1, false);

        this.riGrid.Complete();

        this.dateCollist = this.riGrid.getColumnIndexList([
            'InvoiceAnnivDate',
            'PeriodStart',
            'PeriodEnd'
        ], [6]);
    }

    private populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
        let form: any = {};
        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['GroupRowID'] = this.riExchange.getParentHTMLValue('GroupRowID');
        form['BranchType'] = this.getControlValue('BranchTypeFilter');
        form['BranchNumberFilter'] = this.getControlValue('BranchNumberFilter');
        form['Level'] = 'Business';
        form['InvoiceLevel'] = this.getControlValue('InvoiceLevel');
        form['ContractNumberFilter'] = this.getControlValue('ContractNumber');
        form['AccountNumber'] = this.getControlValue('AccountNumber');
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        form[this.serviceConstants.GridCacheRefresh] = true;
        form[this.serviceConstants.PageSize] = this.getControlValue('PageSize');
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : this.gridConfig.currentPage;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = this.gridConfig.riSortOrder === 'Descending' ? 'Descending' : this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest('bi/reports', 'reports', 'ApplicationReport/iCABSARNextInvoiceRunForecastDetailGrid', gridSearch, form)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        let msgTxt: string = data['errorMessage'];
                        msgTxt = data['fullError'] ? ' - ' + data['fullError'] : '';
                        this.alertMessage = {
                            msg: msgTxt,
                            timestamp: (new Date()).getMilliseconds()
                        };
                        return;
                    }
                    this.riGrid.RefreshRequired();
                    this.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.riGrid.Execute(data);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    let msgTxt: string = error.errorMessage;
                    msgTxt = error.fullError ? ' - ' + error.fullError : '';
                    this.alertMessage = {
                        msg: msgTxt,
                        timestamp: (new Date()).getMilliseconds()
                    };
                });
    }

    public onContractNumberSearchDataReceived(data: any): void {
        this.setControlValue('AccountNumber', data.AccountNumber);
        this.setControlValue('AccountName', data.AccountName);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractNumber', data.ContractNumber);
        if (data.ContractName) {
            this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractName', data.ContractName);
        }
    }

    public onAccountDataReceived(data: any): void {
        this.setControlValue('AccountNumber', data.AccountNumber);
        this.setControlValue('AccountName', data.AccountName);
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.populateGrid();
    }

    public onChangeInvoiceLevel(): void {
        this.disableControl('BranchTypeFilter', false);
        if (this.getControlValue('InvoiceLevel').toLowerCase() === 'con') {
            this.setControlValue('BranchTypeFilter', 'N');
            this.disableControl('BranchTypeFilter', true);
        }
        this.riGrid.HeaderClickedColumn = '';
        this.gridConfig.riSortOrder = 'Descending';
    }

    public onChangeContractOrAccount(parameter: any): void {
        parameter === 'Contract' ? this.setControlValue('ContractName', '') : this.setControlValue('AccountName', '');
        let query = {};
        query[parameter + 'Number'] = this.getControlValue(parameter + 'Number');
        query[this.serviceConstants.BusinessCode] = this.getControlValue('BusinessCode');
        let fields = [parameter + 'Name'];
        let data = [
            {
                'table': parameter,
                'query': query,
                'fields': fields
            }
        ];
        this.LookUp.lookUpRecord(data, 1).subscribe(
            (e) => {
                if (e[0][0]) {
                    this.queryLookUp.set(this.serviceConstants.Action, '0');
                    this.queryLookUp.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
                    this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
                    e[0][0].ContractName ? this.setControlValue('ContractName', e[0][0].ContractName) : this.setControlValue('AccountName', e[0][0].AccountName);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                let msgTxt: string = error.errorMessage;
                msgTxt = error.fullError ? ' - ' + error.fullError : '';
                this.alertMessage = {
                    msg: msgTxt,
                    timestamp: (new Date()).getMilliseconds()
                };
            }
        );
    }

    public onBranchDataReceived(obj: any): void {
        if (obj) {
            this.setControlValue('BranchNumberFilter', obj.BranchNumber);
        }
    }

}
