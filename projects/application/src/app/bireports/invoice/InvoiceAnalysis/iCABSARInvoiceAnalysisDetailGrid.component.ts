import { Component, OnInit, Injector, ViewChild, AfterContentInit, OnDestroy } from '@angular/core';

import { InternalMaintenanceApplicationModuleRoutes, ContractManagementModuleRoutes } from '@base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSARInvoiceAnalysisDetailGrid.html'
})

export class InvoiceAnalysisDetailGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected pageId: string;

    public controls: Array<Object> = [
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'FilterBy', type: MntConst.eTypeText, value: 'RunNumber' },
        { name: 'ProcessDateFrom', type: MntConst.eTypeDate },
        { name: 'ProcessDateTo', type: MntConst.eTypeDate },
        { name: 'RunNumberFrom', type: MntConst.eTypeInteger, required: true },
        { name: 'RunNumberTo', type: MntConst.eTypeInteger, required: true },
        { name: 'TaxCode', type: MntConst.eTypeText },
        { name: 'ViewBy', type: MntConst.eTypeText, value: 'NegBranch' }
    ];
    public filterBy: string = '';
    public gridConfig: any = {
        pageSize: 10,
        totalRecords: 1
    };

    constructor(injector: Injector, private syscharConstants: SysCharConstants) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARINVOICEANALYSISDETAILGRID;
        this.browserTitle = this.pageTitle = 'Invoice Analysis Details';
    }

    // #region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning() && this.parentMode === 'InvoiceAnalysis') {
            // getParentHTML Value Will Set BranchNumber Control; We Need BranchNumber To Set BranchName
            let branch: string = this.riExchange.getParentHTMLValue('BranchNumber');
            if (branch.toLowerCase() !== 'total') {
                this.setControlValue('BranchName', this.utils.getBranchTextOnly(branch));
            }
            this.riExchange.getParentHTMLValue('ViewBy');
            this.riExchange.getParentHTMLValue('FilterBy');
            this.riExchange.getParentHTMLValue('ProcessDateFrom');
            this.riExchange.getParentHTMLValue('ProcessDateTo');
            this.riExchange.getParentHTMLValue('RunNumberFrom');
            this.riExchange.getParentHTMLValue('RunNumberTo');
            this.riExchange.getParentHTMLValue('TaxCode');
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridHandle = this.pageParams.gridHandle || this.utils.randomSixDigitString();
        } else {
            this.pageParams.gridCacheRefresh = false;
        }
        this.filterBy = this.getControlValue('FilterBy');
        /** @Todo - Optimize This Service By Fetching It From Parent pageParams Object */
        this.getTaxCodes();
        this.getSysChars();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    // #endregion

    // #region Private Methods
    private getTaxCodes(): void {
        /*
         * Query Executed On TaxCode Table Instead Of TaxCodeLang,
         * Since Not All Countries Have Populated TaxCodeLang. E.g. - UK
         */
        let queryData: any = [
            {
                'table': 'TaxCode',
                'query': {},
                'fields': ['TaxCode', 'TaxCodeDesc']
            }
        ];
        this.LookUp.lookUpPromise(queryData).then(data => {
            if (this.hasError(data)) {
                this.displayMessage(data);
                return;
            }
            if (data && data[0] && data[0].length) {
                this.pageParams.taxCodes = data[0];
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + 'TaxCode');
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private getSysChars(): void {
        let syscharList: Array<number> = [
            this.syscharConstants.SystemCharEnableMultipleInvoiceLayouts,
            this.syscharConstants.SystemCharEnableSeparateTaxAndNonTaxInvoiceLayouts
        ];
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set(this.serviceConstants.SystemCharNumber, syscharList.join(','));

        this.httpService.sysCharRequest(query).subscribe(data => {
            if (data && data.records && data.records.length) {
                let list: Array<any> = data.records;
                this.pageParams.enableSeparateTaxAndNonTaxInvoiceLayouts = list[1].required;
                this.pageParams.enableMultipleInvoiceLayouts = 0;
                if (list[0].Required && list[0].Integer > 0 && !this.pageParams.enableSeparateTaxAndNonTaxInvoiceLayouts) {
                    this.pageParams.enableMultipleInvoiceLayouts = list[0].Integer;
                }

                this.buildGrid();
                this.onRiGridRefresh();
            } else {
                this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Syschars');
            }
        }, error => {
            this.displayMessage(error);
        });
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('BranchNumber', 'InvoiceAnalysis', 'BranchNumber', MntConst.eTypeText, 2, false);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('BranchNumber', true);
        this.riGrid.AddColumn('BranchName', 'InvoiceAnalysis', 'BranchName', MntConst.eTypeText, 20, false);
        this.riGrid.AddColumn('CompanyCode', 'InvoiceAnalysis', 'CompanyCode', MntConst.eTypeText, 2, false);
        this.riGrid.AddColumnAlign('CompanyCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('InvoiceNumber', 'InvoiceAnalysis', 'InvoiceNumber', MntConst.eTypeInteger, 9, true);
        this.riGrid.AddColumnAlign('InvoiceNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('InvoiceNumber', true);
        this.riGrid.AddColumn('AccountNumber', 'InvoiceAnalysis', 'AccountNumber', MntConst.eTypeText, 9);
        this.riGrid.AddColumn('InvoiceGroupNumber', 'InvoiceAnalysis', 'InvoiceGroupNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('InvoiceGroupNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('InvoiceExtractNumber', 'InvoiceAnalysis', 'InvoiceExtractNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('InvoiceExtractNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('InvoiceExtractNumber', true);
        this.riGrid.AddColumn('ExtractDate', 'InvoiceAnalysis', 'ExtractDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('PeriodStart', 'InvoiceAnalysis', 'PeriosStart', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('TaxPointDate', 'InvoiceAnalysis', 'TaxPointDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('AdvanceInvoice', 'InvoiceAnalysis', 'AdvanceInvoice', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('AdvanceInvoice', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ValueExclTax', 'InvoiceAnalysis', 'ValueExclTax', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('ValueExclTax', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('TaxValue', 'InvoiceHeader', 'TaxValue', MntConst.eTypeCurrency, 15);
        this.riGrid.AddColumnAlign('TaxValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('InvoiceName', 'InvoiceAnalysis', 'InvoiceName', MntConst.eTypeText, 15, true);
        this.riGrid.AddColumnAlign('InvoiceName', MntConst.eAlignmentCenter);

        let layout: number = 0;
        do {
            this.riGrid.AddColumn('PrintImage' + layout, 'InvoiceAnalysis', 'PrintImage' + layout, MntConst.eTypeImage, 1, true);
            this.riGrid.AddColumnAlign('PrintImage' + layout, MntConst.eAlignmentCenter);
            ++layout;
        } while (layout < this.pageParams.enableMultipleInvoiceLayouts);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        let form: any = {};
        let branch: string = this.getControlValue('BranchNumber');

        gridSearch.set(this.serviceConstants.Action, 2);

        form[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        form['BranchNumber'] = branch === 'TOTAL' ? '' : branch;
        form['ViewBy'] = this.getControlValue('ViewBy');
        form['FilterBy'] = this.filterBy;
        form[this.filterBy + 'From'] = this.getControlValue(this.filterBy + 'From');
        form[this.filterBy + 'To'] = this.getControlValue(this.filterBy + 'To');
        form['TaxCode'] = this.getControlValue('TaxCode');
        form['Detail'] = 'Yes';
        form['SCNumberOfInvoiceLayouts'] = this.pageParams.enableMultipleInvoiceLayouts;
        form[this.serviceConstants.GridMode] = '0';
        form[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        form[this.serviceConstants.PageSize] = this.gridConfig.pageSize;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage ? this.pageParams.gridCurrentPage : 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        form['riSortOrder'] = this.riGrid.SortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSARInvoiceAnalysisDetailGrid', gridSearch, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(data.pageData ? data.pageData.pageNumber : 1);
                }, 100);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }

    private invoicePrint(layoutNumber: string): void {
        let printQuery: QueryParams = this.getURLSearchParamObject();

        printQuery.set(this.serviceConstants.Action, 6);
        printQuery.set(this.serviceConstants.Function, 'Single');
        printQuery.set('InvoiceNumber', this.riGrid.Details.GetAttribute('InvoiceNumber', 'rowid'));
        printQuery.set('LayoutNumber', layoutNumber);
        printQuery.set('EmailInvoice', false);
        printQuery.set('Copy', false);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet('bi/reports', 'reports', 'ApplicationReport/iCABSARInvoiceAnalysisDetailGrid', printQuery).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                window.open(data.url);
            }
        }).catch((error) => {
            this.displayMessage(error);
        });
    }
    // #endregion

    // #region Public Methods
    public onChangeFilterBy(): void {
        let orControlMapping: any = {
            ProcessDate: 'RunNumber',
            RunNumber: 'ProcessDate'
        };
        // Set Into A Variable To Avoid Method Execution In Two Places(TS and HTML)
        this.filterBy = this.getControlValue('FilterBy');
        this.setRequiredStatus(this.filterBy + 'From', true);
        this.setRequiredStatus(this.filterBy + 'To', true);
        this.setRequiredStatus(orControlMapping[this.filterBy] + 'From', false);
        this.setRequiredStatus(orControlMapping[this.filterBy] + 'To', false);
    }

    public onRefreshClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.pageParams.gridCacheRefresh = true;
            this.onRiGridRefresh();
        }
    }

    // Kept A One Liner Method Since It Is Implementation Of IGridHandler
    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.pageParams.gridCacheRefresh = false;
            super.getCurrentPage(currentPage);
        } else {
            // setPage Method In Pagination Executes Earlier Than Change; Hence Added The setTimeout
            setTimeout(() => {
                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
            }, 100);
        }
    }

    public onGridBodyDoubleClick(event?: any): void {
        if (event.target.tagName === 'IMG' && this.riGrid.CurrentColumnName.indexOf('PrintImage') >= 0) {
            this.invoicePrint(!this.pageParams.enableMultipleInvoiceLayouts ? '' : this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid'));
            return;
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'InvoiceName':
                this.setAttribute('InvoiceHeaderRowID', this.riGrid.Details.GetAttribute('InvoiceNumber', 'rowid'));
                this.navigate('InvoiceAddressDetails', InternalMaintenanceApplicationModuleRoutes.ICABSAINVOICEHEADERADDRESSDETAILS, {
                    'InvoiceHeaderRowID': this.riGrid.Details.GetAttribute('InvoiceNumber', 'rowid')
                });
                break;
            case 'InvoiceNumber':
                this.setAttribute('InvoiceNumber', this.riGrid.Details.GetValue('InvoiceNumber'));
                this.setAttribute('SystemInvoiceNumber', this.riGrid.Details.GetAttribute('InvoiceNumber', 'additionalProperty'));
                this.setAttribute('CompanyCode', this.riGrid.Details.GetValue('CompanyCode'));
                this.setAttribute('CompanyDesc', this.riGrid.Details.GetAttribute('CompanyCode', 'additionalProperty'));
                this.setAttribute('InvoiceName', this.riGrid.Details.GetValue('InvoiceName'));
                this.setAttribute('AccountNumber', this.riGrid.Details.GetValue('AccountNumber'));
                this.setAttribute('AccountName', this.riGrid.Details.GetAttribute('AccountNumber', 'additionalProperty'));
                this.setAttribute('InvoiceGroupNumber', this.riGrid.Details.GetValue('InvoiceGroupNumber'));
                this.setAttribute('InvoiceGroupDesc', this.riGrid.Details.GetAttribute('InvoiceGroupNumber', 'additionalProperty'));
                this.navigate('InvoiceGroup', ContractManagementModuleRoutes.ICABSACONTRACTINVOICEDETAILGRID);
                break;
        }
    }

    public onHeaderClick(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.pageParams.gridCacheRefresh = false;
            this.onRiGridRefresh();
        }
    }
    // #endregion
}
