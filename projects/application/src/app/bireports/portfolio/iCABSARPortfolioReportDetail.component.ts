import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { ContractManagementModuleRoutes, BIReportsRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';

@Component({
    templateUrl: 'iCABSARPortfolioReportDetail.html'
})

export class PortfolioReportDetailsComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public companyDefault: Object = {
        id: '',
        text: ''
    };
    public companyInputParams: any = {
    };
    public controls = [
        { name: 'AreaBranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'AtDate', required: true, disabled: true, type: MntConst.eTypeDate },
        { name: 'BusinessCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'CompanyCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'CompanyDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ContractTypeCode', disabled: true, value: 'C' },
        { name: 'CountLevel', value: 'Premise' },
        { name: 'GroupBy', disabled: true, value: 'Branch' },
        { name: 'GroupCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'SubGroupCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'IncludeZeroValue' },
        { name: 'NumberOfLines', type: MntConst.eTypeText },
        { name: 'RegionCode', readonly: true, disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'RegionDesc', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'ViewBy', value: 'Premise' }
    ];
    public hasGridData: boolean = false;
    public isBranch: boolean = false;
    public isBusiness: boolean = false;
    public isCompanyLabel: boolean = true;
    public isGroupCode: boolean = true;
    public isRegion: boolean = false;
    public pageId: string;
    public pageTitle: string;
    public showAreaBranchNumber: boolean = false;
    public spanAreaBranch: string;
    public spanGroupCode: string = 'Group';
    public spanSubGroupCode: string = 'Group';
    public isContractDetails: any = false;
    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.companyInputParams[this.serviceConstants.CountryCode] = this.countryCode();
        this.companyInputParams[this.serviceConstants.BusinessCode] = this.businessCode();
        this.companyInputParams['parentMode'] = 'LookUp';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.isContractDetails = this.riExchange.getParentHTMLValue('contractDetails');
        this.pageId = this.isContractDetails ? PageIdentifier.ICABSARPORTFOLIOREPORTCONTRACTDETAIL : PageIdentifier.ICABSARPORTFOLIOREPORTDETAIL;
        super.ngAfterContentInit();
        if (this.isContractDetails) {
            this.riExchange.riInputElement.Disable(this.uiForm, 'CountLevel');
            this.riExchange.riInputElement.Disable(this.uiForm, 'ViewBy');
            this.riExchange.riInputElement.Disable(this.uiForm, 'GroupBy');
            this.riExchange.riInputElement.Disable(this.uiForm, 'ContractTypeCode');
        }
        if (!this.isReturning()) {
            this.pageParams.viewByOption = [];
            this.pageParams.gridConfig = {
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true
            };
            this.pageParams.gridCurrentPage = 1;
            Object.keys(this.uiForm.controls).forEach(control => {
                if (control !== 'ViewBy') {
                    this.riExchange.getParentHTMLValue(control);
                }
            });
            if (this.isContractDetails) {
                this.riExchange.getParentHTMLValue('ViewBy');
            }
            this.setControlValue('SubGroupCode', this.riExchange.getParentHTMLValue('BusinessCodeSubGroupCode'));
            if (!this.getControlValue('NumberOfLines')) {
                this.setControlValue('NumberOfLines', 20);
            }
            if (this.getControlValue('CompanyCode'))
                this.companyDefault = {
                    id: this.riExchange.getParentHTMLValue('CompanyCode'),
                    text: this.riExchange.getParentHTMLValue('CompanyCode') + ' - ' + this.riExchange.getParentHTMLValue('CompanyDesc')
                };
            this.loadSysChars();
        } else {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            this.isCompanyLabel = this.pageParams.SCEnableCompanyCode;
            this.buildGrid();
            this.populateGrid();
            if (this.getControlValue('CompanyCode'))
                this.companyDefault = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            this.setBasedOnParentMode();
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnableInstallsRemovals, this.sysCharConstants.SystemCharEnableCompanyCode].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.lInstallationReq = data.records[0].Required;
            this.pageParams.SCEnableCompanyCode = data.records[1].Required;
            this.isCompanyLabel = this.pageParams.SCEnableCompanyCode;
            this.setBasedOnParentMode();
            if (this.isContractDetails) {
                this.buildGrid();
                this.populateGrid();
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
                this.setBasedOnParentMode();
            });
    }

    private setBasedOnParentMode(): void {
        const groupBy: string = this.getControlValue('GroupBy');
        let contractTitle = this.isContractDetails ? ' Contract' : '';
        this.pageTitle = this.parentMode + ' Portfolio' + contractTitle + ' Detail';
        this.utils.setTitle(this.pageTitle);
        switch (this.parentMode) {
            case 'Business':
                this.isBusiness = true;
                if (!this.isContractDetails)
                    this.setControlValue('GroupCode', this.riExchange.getParentAttributeValue('BusinessCodeGroupCode'));
                if (groupBy === 'Company') {
                    this.isCompanyLabel = false;
                    this.setControlValue('CompanyCode', this.getControlValue('GroupCode'));
                    this.doLookuPforCompany();
                }
                if (this.getControlValue('GroupCode') === 'Totals') {
                    this.setControlValue('GroupCode', '');
                }
                if (groupBy === 'SalesArea' || groupBy === 'ServiceArea') {
                    this.spanAreaBranch = 'Branch';
                    this.showAreaBranchNumber = true;
                    if (!this.isContractDetails)
                        this.setControlValue('AreaBranchNumber', this.riExchange.getParentAttributeValue('BusinessCodeBranchNumber'));
                }
                break;
            case 'Region':
                this.isRegion = true;
                if (!this.isContractDetails)
                    this.setControlValue('GroupCode', this.riExchange.getParentAttributeValue('RegionCodeGroupCode'));
                if (groupBy === 'Company') {
                    this.isCompanyLabel = false;
                    this.setControlValue('CompanyCode', this.getControlValue('GroupCode'));
                    this.doLookuPforCompany();
                }
                if (groupBy === 'SalesArea' || groupBy === 'ServiceArea') {
                    this.spanAreaBranch = 'Branch';
                    this.showAreaBranchNumber = true;
                    if (!this.isContractDetails)
                        this.setControlValue('AreaBranchNumber', this.riExchange.getParentAttributeValue('RegionCodeBranchNumber'));
                }
                break;
            case 'Branch':
                this.isBranch = true;
                if (!this.isContractDetails)
                    this.setControlValue('GroupCode', this.riExchange.getParentAttributeValue('BranchNumberGroupCode'));
                if (groupBy === 'Company') {
                    this.isCompanyLabel = false;
                    this.riExchange.riInputElement.Disable(this.uiForm, 'CompanyCode');
                    if (!this.isContractDetails)
                        this.setControlValue('CompanyCode', this.riExchange.getParentAttributeValue('BranchNumberGroupCode'));
                    this.doLookuPforCompany();
                    this.isBranch = true;
                    this.isGroupCode = false;
                }
                break;
            case 'BranchTotal':
                this.pageTitle = 'Branch Total Portfolio Detail';
                this.utils.setTitle(this.pageTitle);
                this.isBranch = true;
                this.isGroupCode = false;
                if (groupBy === 'Company') {
                    this.riExchange.riInputElement.Disable(this.uiForm, 'CompanyCode');
                    this.doLookuPforCompany();
                }
                break;
        }

        if (groupBy === 'ExpenseCode') {
            this.setControlValue('ViewBy', 'ServiceCover');
        }
        switch (groupBy) {
            case 'Region': this.spanGroupCode = 'Region'; break;
            case 'Branch': this.spanGroupCode = 'Servicing Branch'; break;
            case 'NegBranch': this.spanGroupCode = 'Negotiating Branch'; break;
            case 'NegEmployee': this.spanGroupCode = 'Negotiating Employee'; break;
            case 'Contract': this.spanGroupCode = 'Contract/Job'; break;
            case 'Anniversary': this.spanGroupCode = 'Anniversary Month'; break;
            case 'InvoiceFrequency': this.spanGroupCode = 'Invoice Frequency'; break;
            case 'Payment': this.spanGroupCode = 'Payment Method'; break;
            case 'Town': this.spanGroupCode = 'Town'; break;
            case 'Postcode': this.spanGroupCode = 'Postcode'; break;
            case 'CustomerType': this.spanGroupCode = 'Customer Type'; break;
            case 'MarketSegment': this.spanGroupCode = 'Market Segment'; break;
            case 'SalesArea': this.spanGroupCode = 'Sales Area'; break;
            case 'ServiceArea': this.spanGroupCode = 'Service Area'; break;
            case 'VisitFrequency': this.spanGroupCode = 'Visit Frequency'; break;
            case 'Product': this.spanGroupCode = 'Product'; break;
            case 'ProductGroup': this.spanGroupCode = 'Product Service Group'; break;
            case 'ProductSalesGroup': this.spanGroupCode = 'Product Sales Group'; break;
            case 'InvoiceType': this.spanGroupCode = 'Invoice Type'; break;
            case 'ServiceType': this.spanGroupCode = 'Service Type'; break;
            case 'RMMCategory': this.spanGroupCode = 'RMM Category'; break;
            case 'Account': this.spanGroupCode = 'Account Number'; break;
            case 'PestNetLevel': this.spanGroupCode = 'PestNet Level'; break;
            default: this.spanGroupCode = groupBy; break;
        }

        switch (this.getControlValue('ViewBy')) {
            case 'Product': this.spanSubGroupCode = 'Product'; break;
            case 'Postcode': this.spanSubGroupCode = 'Postcode'; break;
            case 'Contract': this.spanSubGroupCode = 'Contract/Job'; break;
            case 'Premise': this.spanSubGroupCode = 'Premise'; break;
            case 'ServiceCover': this.spanSubGroupCode = 'Service Cover'; break;
            case 'NegEmployee': this.spanSubGroupCode = 'Negotiating Employee'; break;
            case 'InvoiceFrequency': this.spanSubGroupCode = 'Invoice Frequency'; break;
        }
        if (!this.isReturning()) {
            if (groupBy !== 'InvoiceFrequency') {
                this.pageParams.viewByOption.push({ name: 'By Invoice Frequency', value: 'InvoiceFrequency' });
            }
            if (groupBy !== 'Product') {
                this.pageParams.viewByOption.push({ name: 'By Product', value: 'Product' });
            }
            if (groupBy !== 'Postcode') {
                this.pageParams.viewByOption.push({ name: 'By Postcode', value: 'Postcode' });
            }
            if (groupBy !== 'Contract') {
                this.pageParams.viewByOption.push({ name: 'By Contract/Job', value: 'Contract' });
            }
            if (groupBy !== 'NegEmployee') {
                this.pageParams.viewByOption.push({ name: 'By Negotiating Employee', value: 'NegEmployee' });
            }
        }
        this.buildGrid();
    }

    private buildGrid(): void {
        const viewBy: string = this.getControlValue('ViewBy');
        const groupBy: string = this.getControlValue('GroupBy');
        this.riGrid.Clear();
        this.riGrid.AddColumn('GroupCode', 'Portfolio', 'GroupCode ', MntConst.eTypeText, 6);
        if (viewBy !== 'Postcode') {
            this.riGrid.AddColumn('GroupDesc', 'Portfolio', 'GroupDesc ', MntConst.eTypeText, 20);
        }
        if (this.parentMode === 'BranchTotal' && viewBy === 'ServiceCover' && groupBy === 'ExpenseCode') {
            this.riGrid.AddColumn('ExpenseCode', 'Portfolio', 'ExpenseCode ', MntConst.eTypeText, 10);
        }
        if (groupBy === 'Contract' && viewBy === ('Premise' || 'ServiceCover')) {
            this.riGrid.AddColumn('AccountNumber', 'Portfolio', 'AccountNumber ', MntConst.eTypeText, 20);
        }
        if (viewBy === 'ServiceCover' && groupBy === 'Product') {
            this.riGrid.AddColumn('ServiceCommenceDate', 'Portfolio', 'ServiceCommenceDate ', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CustomerTypeCode', 'Portfolio', 'CustomerTypeCode ', MntConst.eTypeText, 12);
            this.riGrid.AddColumnAlign('CustomerTypeCode', MntConst.eAlignmentLeft);
        }
        this.riGrid.AddColumn('GroupValue', 'Portfolio', 'GroupValue ', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('GroupValue', MntConst.eAlignmentRight);
        if (this.getControlValue('CountLevel') === 'All') {
            this.riGrid.AddColumn('NumOfContracts', 'Portfolio', 'NumOfContracts ', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumn('AverageContractValue', 'Portfolio', 'AverageContractValue ', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('AverageContractValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('NumOfPremises', 'Portfolio', 'NumOfPremises ', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfPremises', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('AveragePremiseValue', 'Portfolio', 'AveragePremiseValue ', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('AveragePremiseValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('NumOfServiceCovers', 'Portfolio', 'NumOfServiceCovers ', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfServiceCovers', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('AverageServiceCoverValue', 'Portfolio', 'AverageServiceCoverValue ', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('AverageServiceCoverValue', MntConst.eAlignmentRight);
        } else {
            this.riGrid.AddColumn('NumOfPremises', 'Portfolio', 'NumOfPremises ', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumnAlign('NumOfPremises', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('AverageValue', 'Portfolio', 'AverageValue ', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('AverageValue', MntConst.eAlignmentRight);
        }
        this.riGrid.AddColumn('GroupQty', 'Portfolio', 'GroupQty ', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('GroupQty', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('GroupAvgValue', 'Portfolio', 'GroupAvgValue ', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('GroupAvgValue', MntConst.eAlignmentRight);
        if (this.pageParams.lInstallationReq) {
            this.riGrid.AddColumn('GroupExchanges', 'Portfolio', 'GroupExchanges ', MntConst.eTypeInteger, 10);
            this.riGrid.AddColumnAlign('GroupExchanges', MntConst.eAlignmentRight);
            this.riGrid.AddColumn('GroupAvgExValue', 'Portfolio', 'GroupAvgExValue ', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('GroupAvgExValue', MntConst.eAlignmentRight);
        }
        this.riGrid.AddColumn('PercentageOfTotal', 'Portfolio', 'PercentageOfTotal ', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumnAlign('PercentageOfTotal', MntConst.eAlignmentRight);
        this.riGrid.Complete();
    }

    private doLookuPforCompany(): void {
        if (this.getControlValue('CompanyCode') !== '') {
            let lookupIP = [
                {
                    'table': 'Company',
                    'query': {
                        'BusinessCode': this.businessCode(),
                        'CompanyCode': this.getControlValue('CompanyCode')
                    },
                    'fields': ['CompanyDesc']
                }
            ];
            this.LookUp.lookUpPromise(lookupIP).then((data) => {
                if (data && data[0] && data[0].length) {
                    this.setControlValue('CompanyDesc', data[0][0].CompanyDesc);
                } else {
                    this.setControlValue('CompanyDesc', '');
                }
                this.companyDefault = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            });
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    private populateGrid(): void {
        this.pageParams.gridConfig['pageSize'] = this.getControlValue('NumberOfLines');
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        let formData: Object = {
            'Function': 'PortfolioDetail',
            'Level': this.parentMode,
            'LanguageCode': this.utils.getDefaultLang(),
            'BusinessCode': this.utils.getBusinessCode(),
            'CompanyCode': this.getControlValue('CompanyCode'),
            'BranchNumber': this.getControlValue('BranchNumber'),
            'RegionCode': this.getControlValue('RegionCode'),
            'AtDate': this.getControlValue('AtDate'),
            'ContractTypeCode': this.getControlValue('ContractTypeCode'),
            'GroupBy': this.getControlValue('GroupBy'),
            'GroupCode': this.getControlValue('GroupCode'),
            'SubGroupCode': this.getControlValue('SubGroupCode'),
            'AreaBranchNumber': this.getControlValue('AreaBranchNumber'),
            'ViewBy': this.getControlValue('ViewBy'),
            'CountLevel': this.getControlValue('CountLevel'),
            'InstallationReq': this.pageParams.lInstallationReq,
            'IncludeZeroValue': this.getControlValue('IncludeZeroValue')
        };
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.PageSize] = this.getControlValue('NumberOfLines');
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('bi/reports', 'reports', this.isContractDetails ? 'ApplicationReport/iCABSARPortfolioReportContractDetail' : 'ApplicationReport/iCABSARPortfolioReportDetail', search, formData).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.isRequesting = false;
                if (this.hasError(data)) {
                    this.hasGridData = false;
                    this.displayMessage(data);
                } else {
                    this.hasGridData = true;
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    if (this.isReturning()) {
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 500);
                    }
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.isRequesting = false;
                this.displayMessage(error);
            });
    }

    public onDataRecieved(data: any): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.setControlValue('CompanyCode', data.CompanyCode);
        this.setControlValue('CompanyDesc', data.CompanyDesc);
    }

    public getCurrentPage(data: any): any {
        if (!this.riGrid.RefreshRequiredStatus()) {
            this.pageParams.gridConfig.gridCacheRefresh = false;
            super.getCurrentPage(data);
        }
    }

    public onRiGridRefresh(): void {
        this.populateGrid();
    }

    public onDropDownChnage(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.hasGridData = false;
        this.buildGrid();
    }

    public onGridBodyDoubleClick(): void {
        this.pageParams.contractTypeCode = this.getControlValue('ContractTypeCode');
        switch (this.riGrid.CurrentColumnName) {
            case 'GroupCode':
                let rowid: string = this.riGrid.Details.GetAttribute('GroupCode', 'AdditionalProperty');
                if (this.riGrid.Details.GetValue('GroupCode') !== '' && rowid !== 'TOTAL') {
                    if (!this.isContractDetails) {
                        switch (this.getControlValue('ViewBy')) {
                            case 'Premise':
                                this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                                    PremiseRowID: rowid,
                                    contracttypecode: this.pageParams.contractTypeCode
                                });
                                break;
                            case 'Contract':
                                if (this.pageParams.contractTypeCode === 'C') {
                                    this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                                        ContractRowID: rowid,
                                        contractTypeCode: this.pageParams.contractTypeCode
                                    });
                                } else {
                                    this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, {
                                        ContractRowID: rowid,
                                        contractTypeCode: this.pageParams.contractTypeCode
                                    });
                                }
                                break;
                            case 'ServiceCover':
                                if (this.pageParams.contractTypeCode === 'C') {
                                    this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT, {
                                        currentContractType: this.pageParams.contractTypeCode,
                                        ServiceCoverRowID: rowid
                                    });
                                } else {
                                    this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB, {
                                        currentContractType: this.pageParams.contractTypeCode,
                                        ServiceCoverRowID: rowid
                                    });
                                }
                                break;
                            case 'InvoiceFrequency':
                            case 'Postcode':
                            case 'Product':
                                this.navigate(this.parentMode, BIReportsRoutes.ICABSARPORTFOLIOREPORTCONTRACTDETAIL, {
                                    BusinessCodeSubGroupCode: this.riGrid.Details.GetValue('GroupCode'),
                                    contractDetails: true
                                });
                                break;
                        }
                    } else {
                        if (this.pageParams.contractTypeCode === 'C') {
                            this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                                ContractRowID: rowid,
                                contractTypeCode: this.pageParams.contractTypeCode
                            });
                        } else {
                            this.navigate('PortfolioReports', ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, {
                                ContractRowID: rowid,
                                contractTypeCode: this.pageParams.contractTypeCode
                            });
                        }
                    }
                }
        }
    }
}
