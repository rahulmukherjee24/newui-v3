import { Component, OnInit, Injector, ViewChild } from '@angular/core';

import { AccountSearchComponent } from '@app/internal/search/iCABSASAccountSearch';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { EmployeeSearchComponent } from '@app/internal/search/iCABSBEmployeeSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARAccountsByCategory.html'
})

export class AccountsByCategoryComponent extends LightBaseComponent implements OnInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    public pageId: string = '';
    public controls: IControls[] = [
        { name: 'CategoryCode', type: MntConst.eTypeCode },
        { name: 'CategoryDesc', type: MntConst.eTypeText },
        { name: 'AccountOwnerCode', type: MntConst.eTypeCode },
        { name: 'AccountOwnerSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'AccountNumber', type: MntConst.eTypeCode },
        { name: 'AccountName', type: MntConst.eTypeText, disabled: true },
        { name: 'ShowIndContract', type: MntConst.eTypeCheckBox }
    ];

    public commonGridFunction: CommonGridFunction;

    public dropdown: any = {
        custCategory: {
            isDisabled: false,
            params: {
                parentMode: 'LookUp',
                module: 'contract-admin',
                method: 'contract-management/search',
                operation: 'Business/iCABSBCustomerCategorySearch'
            },
            active: {
                id: '',
                text: ''
            }
        }
    };

    public xhrParams: IXHRParams = {
        operation: 'ApplicationReport/iCABSARAccountsByCategory',
        module: 'reports',
        method: 'bi/reports'
    };

    public ellipsis = {
        AccountOwnerCode: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: EmployeeSearchComponent
        },
        AccountNumber: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: AccountSearchComponent
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARACCOUNTSBYCATEGORY;
        this.browserTitle = this.pageTitle = 'Accounts By Category';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.hasGridData = false;
        this.pageParams.gridConfig = {
            totalItem: 1,
            itemsPerPage: 10
        };
        this.pageParams.isMandatory = true;
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.getControlValue('ShowIndContract')) {

            this.riGrid.AddColumn('AccountNumber', 'AccountByCategory', 'AccountNumber', MntConst.eTypeCode, 9);
            this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountName', 'AccountByCategory', 'AccountName', MntConst.eTypeText, 30);
            this.riGrid.AddColumnAlign('AccountName', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CategoryCode', 'AccountByCategory', 'CategoryCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumnAlign('CategoryCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CategoryDesc', 'AccountByCategory', 'CategoryDesc', MntConst.eTypeText, 30);
            this.riGrid.AddColumnAlign('CategoryDesc', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountOwnerCode', 'AccountByCategory', 'AccountOwnerCode', MntConst.eTypeCode, 20);
            this.riGrid.AddColumnAlign('AccountOwnerCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountOwnerSurname', 'AccountByCategory', 'AccountOwnerSurame', MntConst.eTypeText, 8);
            this.riGrid.AddColumnAlign('AccountOwnerSurname', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountAnnualValue', 'AccountByCategory', 'AccountAnnualValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AccountAnnualValue', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountBalance', 'AccountByCategory', 'AccountBalance', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AccountBalance', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractCount', 'AccountByCategory', 'ContractCount', MntConst.eTypeInteger, 4);
            this.riGrid.AddColumnAlign('ContractCount', MntConst.eAlignmentCenter);

            this.riGrid.AddColumn('ContractNumber', 'AccountByCategory', 'ContractNumber', MntConst.eTypeCode, 10);
            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractName', 'AccountByCategory', 'ContractName', MntConst.eTypeText, 6);
            this.riGrid.AddColumnAlign('ContractName', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractAnnualValue', 'AccountByCategory', 'ContractAnnualValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('ContractAnnualValue', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractNegBranch', 'AccountByCategory', 'ContractNegBranch', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumnAlign('ContractNegBranch', MntConst.eAlignmentCenter);
        } else {
            this.riGrid.AddColumn('AccountNumber', 'AccountByCategory', 'AccountNumber', MntConst.eTypeCode, 9);
            this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountName', 'AccountByCategory', 'AccountName', MntConst.eTypeText, 30);
            this.riGrid.AddColumnAlign('AccountName', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CategoryCode', 'AccountByCategory', 'CategoryCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumnAlign('CategoryCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('CategoryDesc', 'AccountByCategory', 'CategoryDesc', MntConst.eTypeText, 30);
            this.riGrid.AddColumnAlign('CategoryDesc', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountOwnerCode', 'AccountByCategory', 'AccountOwnerCode', MntConst.eTypeCode, 20);
            this.riGrid.AddColumnAlign('AccountOwnerCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountOwnerSurname', 'AccountByCategory', 'AccountOwnerSurame', MntConst.eTypeText, 8);
            this.riGrid.AddColumnAlign('AccountOwnerSurname', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountAnnualValue', 'AccountByCategory', 'AccountAnnualValue', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AccountAnnualValue', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('AccountBalance', 'AccountByCategory', 'AccountBalance', MntConst.eTypeDecimal2, 10);
            this.riGrid.AddColumnAlign('AccountBalance', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ContractCount', 'AccountByCategory', 'ContractCount', MntConst.eTypeInteger, 4);
            this.riGrid.AddColumnAlign('ContractCount', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumnOrderable('AccountNumber', true);
        this.riGrid.AddColumnOrderable('AccountName', true);
        this.riGrid.AddColumnOrderable('CategoryCode', true);
        this.riGrid.AddColumnOrderable('CategoryDesc', true);
        this.riGrid.AddColumnOrderable('AccountOwnerCode', true);
        this.riGrid.AddColumnOrderable('AccountOwnerSurname', true);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm) || this.pageParams.isMandatory) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        gridQuery.set(this.serviceConstants.Action, '2');

        const postObj: Object = {
            BusinessCode: this.utils.getBusinessCode(),
            CategoryCode: this.getControlValue('CategoryCode'),
            AccountOwnerCode: this.getControlValue('AccountOwnerCode'),
            AccountNumber: this.getControlValue('AccountNumber'),
            ShowIndContract: this.getControlValue('ShowIndContract', true),
            riGridMode: '0',
            riGridHandle: this.utils.randomSixDigitString(),
            riCacheRefresh: true,
            PageSize: '10',
            PageCurrent: this.pageParams.gridCurrentPage,
            riSortOrder: this.riGrid.SortOrder,
            HeaderClickedColumn: this.riGrid.HeaderClickedColumn
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, postObj).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data);
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onChange(): void {
        if (this.getControlValue('CategoryCode') || this.getControlValue('AccountOwnerCode') || this.getControlValue('AccountNumber')) {
            this.pageParams.isMandatory = false;
        } else {
            this.pageParams.isMandatory = true;
            this.pageParams.hasGridData = false;
            this.commonGridFunction.resetGrid();
        }
    }

    public onRiGridRefresh(): void {
        if (this.getControlValue('CategoryCode') || this.getControlValue('AccountOwnerCode') || this.getControlValue('AccountNumber')) {
            this.buildGrid();
            this.populateGrid();
        } else {
            this.displayMessage('Please select a Category Code, Account Owner or Account Number before clicking Refresh', CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

    public onDataRecieved(data: any, type: string): void {
        switch (type) {
            case 'CategoryCode':
                this.setControlValue('CategoryCode', data.CategoryCode);
                this.setControlValue('CategoryDesc', data.CategoryDesc);
                break;
            case 'AccountOwnerCode':
                this.setControlValue('AccountOwnerCode', data.EmployeeCode);
                this.setControlValue('AccountOwnerSurname', data.EmployeeSurname);
                break;
            case 'AccountNumber':
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.setControlValue('AccountName', data.AccountName);
                break;
        }
        this.pageParams.isMandatory = false;
    }
}
