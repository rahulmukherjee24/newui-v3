import { AfterContentInit, OnInit, Component, Injector, ViewChild } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { BIReportsRoutes } from '@app/base/PageRoutes';


@Component({
    templateUrl: 'iCABSAContractsWithExpiringPOBusinessGrid.html'
})

export class ContractsWithExpiringPOBBusinessGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public controls: IControls[] = [
        { name: 'BusinessCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'SelectDateTo', required: true, type: MntConst.eTypeDate },
        { name: 'ViewBy', value: 'Branch' }
    ];
    public pageId: string = '';
    public pageTitle: string;
    public commonGridFunction: CommonGridFunction;

    constructor(injector: Injector, private sysCharConstants: SysCharConstants) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSACONTRACTSWITHEXPIRINGPOBUSINESSGRID;
        this.browserTitle = this.pageTitle = `Business - Continuous Contracts with PO's`;
    }
    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                pageSize: 10
            };
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridCurrentPage = 1;
            this.setControlValue('BusinessCode', this.businessCode());
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());
            let date = new Date();
            let selectToDate = new Date(date.getFullYear(), date.getMonth() + 2, 0);
            this.setControlValue('SelectDateTo', this.globalize.parseDateToFixedFormat(selectToDate) as string);
            this.loadSysChars();
        } else {
            this.pageParams.gridCacheRefresh = false;
            this.buildGrid();
            this.populateGrid();
        }
    }

    public cacheRefreshOnHeaderClick(): void {
        this.pageParams.gridCacheRefresh = false;
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let companySysChar: QueryParams = this.getURLSearchParamObject();
        companySysChar.set(this.serviceConstants.Action, '0');
        companySysChar.set(this.serviceConstants.SystemCharNumber, [this.sysCharConstants.SystemCharEnablePORefsAtServiceCoverLevel].toString());
        this.httpService.sysCharRequest(companySysChar).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.glSCPORefsAtServiceCover = data.records[0].Required;
            this.buildGrid();
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
                this.pageParams.glSCPORefsAtServiceCover = false;
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('RowCode', 'Expire', 'RowCode', MntConst.eTypeCode, 6, true);
        this.riGrid.AddColumnAlign('RowCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('RowCode', true);
        this.riGrid.AddColumn('RowDesc', 'Expire', 'RowDesc', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('RowDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnOrderable('RowDesc', true);

        this.riGrid.AddColumn('NumContracts', 'Expire', 'NumContracts', MntConst.eTypeInteger, 4, false);
        this.riGrid.AddColumnAlign('NumContracts', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('NumContracts', true);

        this.riGrid.AddColumn('ContractAnnualValue', 'Expire', 'ContractAnnualValue', MntConst.eTypeCurrency, 10, false);
        this.riGrid.AddColumnAlign('ContractAnnualValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('ContractAnnualValue', true);

        if (this.pageParams.glSCPORefsAtServiceCover) {
            this.riGrid.AddColumn('NumPremises', 'Expire', 'NumPremises', MntConst.eTypeInteger, 4, false);
            this.riGrid.AddColumnAlign('NumPremises', MntConst.eAlignmentRight);
            this.riGrid.AddColumnOrderable('NumPremises', true);

            this.riGrid.AddColumn('PremiseAnnualValue', 'Expire', 'PremiseAnnualValue', MntConst.eTypeCurrency, 10, false);
            this.riGrid.AddColumnAlign('PremiseAnnualValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumnOrderable('PremiseAnnualValue', true);

            this.riGrid.AddColumn('NumCovers', 'Expire', 'NumCovers', MntConst.eTypeInteger, 4, false);
            this.riGrid.AddColumnAlign('NumCovers', MntConst.eAlignmentRight);
            this.riGrid.AddColumnOrderable('NumCovers', true);

            this.riGrid.AddColumn('ServiceAnnualValue', 'Expire', 'ServiceAnnualValue', MntConst.eTypeCurrency, 10, false);
            this.riGrid.AddColumnAlign('ServiceAnnualValue', MntConst.eAlignmentRight);
            this.riGrid.AddColumnOrderable('ServiceAnnualValue', true);
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');

            let formData: Object = {
                'Level': 'Business',
                'SummaryOnly': 'true'
            };

            Object.keys(this.uiForm.controls).forEach(control => {
                formData = {
                    ...formData,
                    [control]: this.getControlValue(control)
                };
            });


            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Application/iCABSAContractsWithExpiringPOBusinessGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                    } else {
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onGridBodyDoubleClick(): void {
        let additinalData: string = this.riGrid.Details.GetAttribute('RowCode', 'AdditionalProperty');
        let rowID: string = this.riGrid.Details.GetAttribute('RowCode', 'rowID');
        if (this.riGrid.CurrentColumnName === 'RowCode') {
            if (this.getControlValue('ViewBy') === 'Branch') {
                if (rowID === 'Total') {
                    this.setAttribute('BranchNumber', 0);
                    this.setAttribute('BranchName', 'Total');
                    this.setAttribute('RegionCode', 'Total');
                    this.setAttribute('RegionDesc', 'Total');
                } else {
                    this.setAttribute('BranchNumber', rowID);
                    this.setAttribute('BranchName', this.riGrid.Details.GetValue('RowDesc'));
                }
                this.navigate('Business', BIReportsRoutes.ICABSACONTRACTSWITHEXPIRINGPOGRID);
            } else {
                if (rowID === 'Total') {
                    this.setAttribute('BranchNumber', 0);
                    this.setAttribute('BranchName', 'Total');
                    this.setAttribute('RegionCode', 'Total');
                    this.setAttribute('RegionDesc', 'Total');
                } else {
                    this.setAttribute('RegionCode', rowID);
                    this.setAttribute('RegionDesc', this.riGrid.Details.GetValue('RowDesc'));
                }
                this.displayMessage(MessageConstant.Message.PageNotDeveloped);
            }
        }
    }
}
