import { Component, OnInit, Injector, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';


import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAUnallocatedUnitsGrid.html'
})

export class UnAllocatedUnitsComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public pageId: string = '';
    public commonGridFunction: CommonGridFunction;
    public controls: IControls[] = [
        { name: 'SortBy', type: MntConst.eTypeText, value: 'Contract' }
    ];

    public sortbyvalues: any = [
        { key: 'Contract', value: 'Premises Within Contract' },
        { key: 'Empty', value: 'Highest Number of Empty Locations' },
        { key: 'UnallocatedUnits', value: 'Highest Number of Unallocated Units' },
        { key: 'InHold', value: 'Highest Number of Units In Holding Location' }
    ];

    private xhrParams: IXHRParams = {
        operation: 'Application/iCABSAUnallocatedUnitsGrid',
        module: 'Contract',
        method: 'contract-management/grid'
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSAUNALLOCATEDUNITSGRID;
        this.browserTitle = this.pageTitle = 'Unallocated Units';
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.buildGrid();
        this.pageParams.gridConfig = {
            itemsPerPage: 10,
            gridHandle: this.utils.randomSixDigitString(),
            totalItem: 1
        };
        if (this.isReturning()) {
            this.riGrid.HeaderClickedColumn = this.pageParams.headerGirdColumn || '';
            this.riGrid.DescendingSort = this.pageParams.GridSortOrder === 'Descending';
            this.onRiGridRefresh();
        } else {
            this.pageParams.currentPage = 1;
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        /* The following AddColumn records are only for those columns that will be shown on screen. Export-Only columns are not included*/
        this.riGrid.AddColumn('ContractNumber', 'PremiseLocationSummary', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumn('ContractName', 'PremiseLocationSummary', 'ContractName', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PremiseNumber', 'PremiseLocationSummary', 'PremiseNumber', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('PremiseName', 'PremiseLocationSummary', 'PremiseName', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ProductCode', 'PremiseLocationSummary', 'ProductCode', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('BranchServiceAreaCode', 'PremiseLocationSummary', 'BranchServiceAreaCode', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ServiceQuantity', 'PremiseLocationSummary', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('UnallocatedQuantity', 'PremiseLocationSummary', 'UnallocatedQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('QuantityInHold', 'PremiseLocationSummary', 'QuantityInHold', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('TotalEmptyLocations', 'PremiseLocationSummary', 'TotalEmptyLocations', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('ContractName', true);
        this.riGrid.AddColumnOrderable('PremiseNumber', true);
        this.riGrid.AddColumnOrderable('PremiseName', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        const gridQuery: QueryParams = this.getURLSearchParamObject();
        gridQuery.set(this.serviceConstants.Action, '2');

        const form: object = {};
        form['BusinessCode'] = this.utils.getBusinessCode();
        form['BranchNumber'] = this.utils.getBranchCode();
        form['ContractTypeCode'] = this.riExchange.getCurrentContractType();
        form['SortType'] = this.getControlValue('SortBy');
        form[this.serviceConstants.GridMode] = 0;
        form[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        form[this.serviceConstants.GridCacheRefresh] = true;
        form[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        form[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
        form[this.serviceConstants.GridHeaderClickedColumn] = this.pageParams.headerGirdColumn;
        form[this.serviceConstants.GridSortOrder] = this.pageParams.GridSortOrder;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, form).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data, this.pageParams.gridConfig);
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public onHeaderClick(): void {
        this.pageParams.headerGirdColumn = this.riGrid.HeaderClickedColumn;
        this.pageParams.GridSortOrder = this.riGrid.SortOrder;
        this.onRiGridRefresh();
    }
    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'PremiseNumber') {
            this.navigate('Premise-Allocate', 'application/premiseLocationAllocation', {
                'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                'PremiseNumber' : this.riGrid.Details.GetValue('PremiseNumber'),
                'ContractName' : this.riGrid.Details.GetValue('ContractName'),
                'PremiseName' : this.riGrid.Details.GetValue('PremiseName')
            });
        }
    }
}
