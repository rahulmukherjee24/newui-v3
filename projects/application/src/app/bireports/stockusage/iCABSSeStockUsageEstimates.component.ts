import { Component, OnInit, AfterContentInit, Injector, ViewChild } from '@angular/core';

import { BIReportsRoutes } from '@base/PageRoutes';
import { CommonGridFunction } from '@base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { IXHRParams } from '@app/base/XhrParams';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSSeStockUsageEstimatesBranch.html',
    providers: [CommonLookUpUtilsService]
})

export class ICABSSeStockUsageEstimatesComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private xhrParams: IXHRParams = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSeStockUsageEstimates'
    };

    protected controls: IControls[] = [
        { name: 'BusinessCode', disabled: true, required: true, value: this.utils.getBusinessCode(), type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, value: this.utils.getBusinessText(), type: MntConst.eTypeText },
        { name: 'DateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EnvironmentMetricInd', type: MntConst.eTypeCheckBox },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'SelEnvironmentUsage', type: MntConst.eTypeText }
    ];

    public commonGridFunction: CommonGridFunction;
    public environmentalUsages: Array<any>;
    public isRegion: boolean = false;
    public pageId: string;
    public pageType: string;

    constructor(injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.riExchange.getParentHTMLValue('pageType');

        this.isRegion = this.pageType === 'Region';

        this.pageId = PageIdentifier['ICABSSESTOCKUSAGEESTIMATES' + this.pageType.toUpperCase()];
        this.browserTitle = this.pageTitle = 'Stock Usage Estimates ' + this.pageType;
        this.utils.setTitle(this.pageTitle);

        super.ngAfterContentInit();

        this.getEnvironmentalUsage();
        this.buildGrid();

        if (this.isRegion) {
            this.commonLookup.getRegionDesc().subscribe((data) => {
                this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
            });
        }

        if (this.isReturning()) {
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridConfig = {
                itemsPerPage: 10,
                totalItem: 1,
                gridHandle: this.utils.randomSixDigitString()
            };
            this.getTradingDates();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('PrepCode', 'Usage', 'PrepCode', MntConst.eTypeCode, 3, true);
        this.riGrid.AddColumnAlign('PrepCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PrepDesc', 'Usage', 'PrepDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('NumberOfPremises', 'Usage', 'NumberOfPremises', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Quantity', 'Usage', 'Quantity', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumn('MeasuredIn', 'Usage', 'MeasuredIn', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('Value', 'Usage', 'Value', MntConst.eTypeDecimal2, 5);

        if (this.getControlValue('EnvironmentMetricInd')) {
            this.riGrid.AddColumn('WeightUsage', 'Usage', 'WeightUsage', MntConst.eTypeDecimal2, 5);
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        this.isRequesting = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        let formData: Object = {};

        formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
        formData['Level'] = this.pageType;
        if (this.isRegion) {
            formData[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
            formData['ViewBy'] = '';
        }
        formData['DateFrom'] = this.getControlValue('DateFrom');
        formData['DateTo'] = this.getControlValue('DateTo');
        formData['SelEnvironmentUsage'] = this.getControlValue('SelEnvironmentUsage');
        formData['EnvironmentMetricInd'] = this.getControlValue('EnvironmentMetricInd') ? 'True' : 'False';

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.itemsPerPage;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage || 1;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData[this.serviceConstants.GridSortOrder] = 'Descending';

        this.ajaxSource.next(this.ajaxconstant.START);

        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonGridFunction.setPageData(data);
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.pageParams.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'PrepCode') {
            if (this.isRegion) {
                this.setAttribute('RegionCode', this.getControlValue('RegionCode'));
                this.setAttribute('RegionDesc', this.getControlValue('RegionDesc'));
            }
            this.navigate(this.isRegion ? 'Region' : 'Branch', BIReportsRoutes.ICABSSESTOCKUSAGEESTIMATESDETAIL,
                {
                    'PrepCode': this.riGrid.Details.GetValue('PrepCode'),
                    'PrepDesc': this.riGrid.Details.GetValue('PrepDesc'),
                    'MeasuredIn': this.riGrid.Details.GetValue('MeasuredIn'),
                    'DateFrom': this.getControlValue('DateFrom'),
                    'DateTo': this.getControlValue('DateTo'),
                    'ViewBy': this.pageType
                });
        }
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    private getTradingDates(): void {
        this.commonLookup.getSalesTradingFromBusiness().then((data) => {
            if (data && data[0] && data[0].length) {
                let salesTradingMonth: number = data[0][0].SalesTradingMonth;
                let salesTradingYear: number = data[0][0].SalesTradingYear;
                let fromDate: Date = new Date(salesTradingYear, salesTradingMonth - 1, 1);
                let toDate: Date = new Date(salesTradingYear, salesTradingMonth, 0);
                this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(fromDate) as string);
                this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(toDate) as string);
            } else {
                this.setControlValue('DateFrom', '');
                this.setControlValue('DateTo', '');
            }
        });
    }

    private getEnvironmentalUsage(): void {
        this.commonLookup.getEnvironmentalUsageDetails().then(data => {
            if (data[0].length > 0) {
                this.environmentalUsages = data[0];
                this.setControlValue('SelEnvironmentUsage', this.environmentalUsages[0].EnvironmentCode);
            }
        });
    }

    public onChange(event: any, type: string): void {
        this.commonGridFunction.resetGrid();
        this.setControlValue('SelEnvironmentUsage', event);

    }
}
