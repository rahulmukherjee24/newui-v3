import { Component, Injector, ViewChild, AfterContentInit, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';

import { NGXLogger } from 'ngx-logger';

import { BranchSearchComponent } from '@internal/search/iCABSBBranchSearch';
import { FilterCleanUpHandlerService } from './FilterCleanUpHandlerService';
import { GenericSearch } from '@base/GenericSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';

class SelectedList {
    public static readonly c_s_LIST_SEPARATOR: string = ';';
    public static readonly c_s_RAISEDBYEMPLOYEE = 'CREATED BY EMPLOYEE:';
    public static readonly c_s_CURRENTOWNEREMPLOYEE = 'CURRENT OWNER EMPLOYEE:';
    public static readonly c_s_CURRENTACTIONEREMPLOYEE = 'CURRENT ACTIONER EMPLOYEE:';
    public static readonly c_s_RAISEDBYTEAM = 'CREATED BY TEAM:';
    public static readonly c_s_CURRENTTEAM = 'CREATED BY TEAM:';
    public static readonly c_s_CURRENTACTIONERBRANCH = 'CURRENT ACTIONER BRANCH:';
    public static readonly c_s_SERVICEBRANCH = 'SERVICE BRANCH:';
    public static readonly c_s_CURRENTACTIONERREGION = 'CURRENT ACTIONER REGION:';
    public static readonly c_s_CURRENTOWNERREGION = 'CURRENT OWNER REGION:';
    public static readonly c_s_CONTACTSTATUS = 'CONTACT STATUS:';
    public static readonly c_s_ACCOUNT = 'ACCOUNT:';
    public static readonly c_s_GROUPACCOUNT = 'GROUP ACCOUNT:';

    public RaisedByEmployee: string = '';
    public CurrentOwnerEmployee: string = '';
    public CurrentActionerEmployee: string = '';
    public RaisedByTeam: string = '';
    public CurrentTeam: string = '';
    public CurrentActionerBranch: string = '';
    public ServiceBranch: string = '';
    public CurrentOwnerRegion: string = '';
    public CurrentActionerRegion: string = '';
    public Account: string = '';
    public ContactStatus: string = '';
    public GroupAccount: string = '';

    public getReturnItems(key: string): string {
        return this[key] || '';
    }

    public clear(): void {
        this.RaisedByEmployee = '';
        this.CurrentOwnerEmployee = '';
        this.CurrentActionerEmployee = '';
        this.RaisedByTeam = '';
        this.CurrentTeam = '';
        this.CurrentActionerBranch = '';
        this.ServiceBranch = '';
        this.CurrentOwnerRegion = '';
        this.CurrentActionerRegion = '';
        this.Account = '';
        this.ContactStatus = '';
        this.GroupAccount = '';
    }
}

export class SelectedValueHandler {
    private filters: Record<string, string> = {};

    constructor() {
    }

    public set value(data: ISelectedProperty) {
        this.filters[data.key] = data.value;
    }

    public get list(): string {
        let filterString: string = '';

        for (let key in this.filters) {
            if (this.filters[key]) {
                filterString += this.filters[key] + '\n';
            }
        }
        return filterString;
    }

    public get hasSelected(): boolean {
        let selected: boolean = false;

        for (let key in this.filters) {
            if (this.filters[key]) {
                selected = true;
                break;
            }
        }
        return selected;
    }

    public get hasSelectedEmployee(): boolean {
        let valueString: string = JSON.stringify(this.filters);
        return (valueString.indexOf(SelectedList.c_s_CURRENTACTIONEREMPLOYEE) >= 0
            || valueString.indexOf(SelectedList.c_s_RAISEDBYEMPLOYEE) >= 0
            || valueString.indexOf(SelectedList.c_s_CURRENTOWNEREMPLOYEE) >= 0);
    }

    public get hasSelectedTeam(): boolean {
        let valueString: string = JSON.stringify(this.filters);
        return (valueString.indexOf(SelectedList.c_s_CURRENTTEAM) >= 0
            || valueString.indexOf(SelectedList.c_s_RAISEDBYTEAM) >= 0);
    }

    public get hasSelectedBranch(): boolean {
        let valueString: string = JSON.stringify(this.filters);
        return (valueString.indexOf(SelectedList.c_s_CURRENTACTIONERBRANCH) >= 0
            || valueString.indexOf(SelectedList.c_s_SERVICEBRANCH) >= 0);
    }

    public get hasSelectedRegion(): boolean {
        let valueString: string = JSON.stringify(this.filters);
        return (valueString.indexOf(SelectedList.c_s_CURRENTOWNERREGION) >= 0
            || valueString.indexOf(SelectedList.c_s_CURRENTACTIONERREGION) >= 0);
    }

    public get hasSelectedAccount(): boolean {
        return this.filters['Account'] !== undefined
            && this.filters['Account'] !== null
            && this.filters['Account'] !== '';
    }

    public get hasSelectedGroupAccount(): boolean {
        return this.filters['GroupAccount'] !== undefined
            && this.filters['GroupAccount'] !== null
            && this.filters['GroupAccount'] !== '';
    }

    public get hasSelectedStatus(): boolean {
        let valueString: string = JSON.stringify(this.filters);
        return valueString.indexOf(SelectedList.c_s_CONTACTSTATUS) >= 0;
    }

    public clear(): void {
        this.filters = {};
    }
}

export interface ISelectedProperty {
    key: string;
    value: string;
}

@Component({
    selector: 'icabs-ticket-filter-select',
    templateUrl: 'iCABSCMTicketAnalysisSelect.html'
})
export class TicketAnalysisSelectComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;

    @Input() type: string = 'Employee';
    @Output() onSelect: EventEmitter<ISelectedProperty>;

    private genericSearch: GenericSearch;
    private list: SelectedList;
    private property: string;

    protected pageId: string;
    protected controls: IControls[] = [
        // Employee Select Controls
        { name: 'EmployeeType', value: 'RaisedByEmployee' },
        { name: 'SelStatus', value: 'AllEmp' },
        { name: 'SelEmployeeSurname' },
        { name: 'BranchNumber' },
        { name: 'BranchName' },

        // Team Select Controls
        { name: 'TeamType', value: 'RaisedByTeam' },

        // Branch Select Controls
        { name: 'BranchType', value: 'CurrentActionerBranch' },
        { name: 'RegionCode' },

        // Region Select Controls
        { name: 'RegionType', value: 'CurrentOwnerRegion' },

        // Account Select Controls
        { name: 'AccountSearchType', value: 'namebegins' },
        { name: 'AccountSearchValue' },

        // Group Account Select Controls
        { name: 'GroupAccountSearchType', value: 'namebegins' },
        { name: 'GroupAccountSearchValue' }
    ];

    public gridConfig: Record<string, number> = {
        totalRecords: 1,
        pageSize: 10
    };
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };
    public regionList: Array<any> = [{
        RegionCode: '', RegionDesc: ''
    }];

    constructor(injector: Injector,
        private cleanup: FilterCleanUpHandlerService,
        private logger: NGXLogger) {
        super(injector);

        this.pageId = PageIdentifier.ICABSCMTICKETANALYSISSELECT;
        this.pageTitle = 'Employee Select';

        this.onSelect = new EventEmitter();
        this.list = new SelectedList();
        this.genericSearch = new GenericSearch(injector);

        this.cleanup.getObservableSource().subscribe(_data => {
            if (this.uiForm) {
                this.clear(false);
            }
        }, error => {
            this.logger.log(error);
        });
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();

        this.type = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.type);
        this.pageParams.currentPage = 1;

        this.buildGrid();
        this.setListPropertyForUpdate();

        if (this.type === 'Branch') {
            this.genericSearch.init({
                table: 'Region',
                conditions: [{
                    field: 'BusinessCode',
                    value: this.businessCode()
                }]
            });
            this.genericSearch.fetch().then(data => {
                this.regionList = data.records;
            }).catch(error => {
                this.displayMessage(error);
            });
        }
        if (this.type === 'Account') {
            this.setRequiredStatus('AccountSearchValue', true);
        }
        if (this.type === 'GroupAccount') {
            this.setRequiredStatus('GroupAccountSearchValue', true);
        }

        if (this.type !== 'Account' && this.type !== 'GroupAccount') {
            this.populateGrid();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this.clear(true);
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('ItemCode', 'SelectGrid', 'ItemCode', MntConst.eTypeText, 8, true);
        this.riGrid.AddColumn('ItemDesc', 'SekectGrid', 'ItemDesc', MntConst.eTypeText, 20, true);

        if (this.type === 'Account' || this.type === 'Branch') {
            this.riGrid.AddColumn('ItemExtraField', 'SekectGrid', 'ItemExtraField', MntConst.eTypeText, 20, true);
        }

        this.riGrid.AddColumn('ItemSelect', 'SelectGrid', 'ItemSelect', MntConst.eTypeCheckBox, 1, true);

        this.riGrid.AddColumnOrderable('ItemCode', true);
        this.riGrid.AddColumnOrderable('ItemDesc', true);
        if (this.type === 'Account' || this.type === 'Branch') {
            this.riGrid.AddColumnOrderable('ItemExtraField', true);
        }
        this.riGrid.AddColumnOrderable('ItemSelect', true);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        let isCurrentTeam: boolean = this.getControlValue('TeamType') === 'CurrentTeam';
        let accountSearchType: string = '';
        let accountSearchValue: string = '';

        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }

        let search: QueryParams = new QueryParams();
        let formData: Record<string, string | boolean | number> = {};

        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.Action, '2');

        switch (this.type) {
            case 'Account':
                accountSearchType = this.getControlValue('AccountSearchType');
                accountSearchValue = this.getControlValue('AccountSearchValue');
                break;
            case 'GroupAccount':
                accountSearchType = this.getControlValue('GroupAccountSearchType');
                accountSearchValue = this.getControlValue('GroupAccountSearchValue');
                break;
        }

        formData = {
            'TableName': this.type,
            'SearchType': this.type,
            'EmployeeSurname': this.getControlValue('SelEmployeeSurname'),
            'Status': this.getControlValue('SelStatus'),
            'RetItemCodes': this.list.getReturnItems(this.property),
            'CurrentTeam': isCurrentTeam,
            'RaisedByTeam': !isCurrentTeam,
            'RegionCode': this.getControlValue('RegionCode'),
            'AccountSearchType': accountSearchType,
            'AccountSearchValue': accountSearchValue,
            'BranchNumber': this.getControlValue('BranchNumber')
        };

        formData[this.serviceConstants.GridMode] = 0;
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridPageSize] = this.gridConfig.pageSize;
        formData[this.serviceConstants.GridPageCurrent] = this.pageParams.currentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData[this.serviceConstants.GridSortOrder] = 'Descending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost('ccm/maintenance', 'tickets', 'ContactManagement/iCABSCMTicketAnalysisSelect', search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            } else {
                this.riGrid.RefreshRequired();
                this.pageParams.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                this.riGrid.Execute(data);
            }
        }).catch(error => {
            this.displayMessage(error);
        });
    }

    private setListPropertyForUpdate(): void {
        switch (this.type) {
            case 'Account':
            case 'ContactStatus':
            case 'GroupAccount':
                this.property = this.type;
                break;
            default:
                /**
                 * For 'Region' Control Name Will Be Evaluated To 'RegionType'
                 * Hence This Will Pick The Selected Radio
                 */
                this.property = this.getControlValue(this.type + 'Type');
                break;
        }
    }

    private onValueAmend(value: string, isAdded: boolean): void {
        let currentValue: string = this.list[this.property];
        let emitValue: string = '';

        if (!currentValue && isAdded) {
            currentValue = value;
        } else if (isAdded) {
            currentValue += SelectedList.c_s_LIST_SEPARATOR + value;
        } else {
            let list: Array<string> = currentValue.split(SelectedList.c_s_LIST_SEPARATOR);
            let index: number = list.indexOf(value);
            list.splice(index, 1);
            currentValue = list.join(SelectedList.c_s_LIST_SEPARATOR);
        }

        this.list[this.property] = currentValue;
        emitValue = currentValue ? SelectedList['c_s_' + this.property.toUpperCase()] + this.list[this.property] : '';

        this.onSelect.emit({
            key: this.property,
            value: emitValue
        });
    }

    private clear(isDestroying: boolean): void {
        this.uiForm.reset();
        this.list.clear();

        // Reset Default Values
        this.setControlValue('EmployeeType', 'RaisedByEmployee');
        this.setControlValue('SelStatus', 'AllEmp');
        this.setControlValue('TeamType', 'RaisedByTeam');
        this.setControlValue('BranchType', 'CurrentActionerBranch');
        this.setControlValue('RegionType', 'CurrentOwnerRegion');
        this.setControlValue('AccountSearchType', 'namebegins');
        this.setControlValue('GroupAccountSearchType', 'namebegins');

        if (this.branchSearchDropDown) {
            this.branchSearchDropDown.active = {
                id: '',
                text: ''
            };
        }

        if (!isDestroying) {
            this.populateGrid();
        }
    }

    public onRefreshClick(): void {
        this.populateGrid();
    }

    public getCurrentPage(data: Record<string, number>): void {
        this.pageParams.currentPage = data.value;
        this.populateGrid();
    }

    public onOwnerTypeChange(): void {
        this.pageParams.currentPage = 1;
        this.setListPropertyForUpdate();
        this.populateGrid();
    }

    public onGridBodyClick(): void {
        if (this.riGrid.CurrentColumnName === 'ItemSelect') {
            this.onValueAmend(this.riGrid.Details.GetValue('ItemCode'), this.riGrid.Details.GetValue('ItemSelect'));
        }
    }

    public onBranchDataRecived(data: Record<string, string>): void {
        this.setControlValue('BranchNumber', data.BranchNumber);
    }
}
