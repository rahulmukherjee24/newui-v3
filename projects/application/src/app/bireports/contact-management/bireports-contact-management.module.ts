
import { HttpClientModule } from '@angular/common/http';
import { Component, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@base/PageRoutes';
import { SharedModule } from '@shared/shared.module';
import { SearchEllipsisDropdownModule } from './../../internal/search-ellipsis-dropdown.module';

import { TicketAnalysisSelectComponent } from './selection-screens/iCABSCMTicketAnalysisSelect.component';
import { TicketAnalysisGenerationComponent } from './TicketAnalysis/iCABSCMTicketAnalysisGeneration.component';
import { FilterCleanUpHandlerService } from './selection-screens/FilterCleanUpHandlerService';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class ContactManagementBIReportsRootComponent {
    constructor() {
    }
}

@NgModule({
    exports: [
    ],
    imports: [
        HttpClientModule,
        SharedModule,
        SearchEllipsisDropdownModule,
        RouterModule.forChild([
            {
                path: '', component: ContactManagementBIReportsRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSCMTICKETANALYSISGENERATION, component: TicketAnalysisGenerationComponent }
                ], data: { domain: 'BI REPORTS CONTACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        ContactManagementBIReportsRootComponent,
        TicketAnalysisSelectComponent,
        TicketAnalysisGenerationComponent
    ],
    providers: [
        FilterCleanUpHandlerService
    ]

})

export class ContactManagementBIReportsModule { }
