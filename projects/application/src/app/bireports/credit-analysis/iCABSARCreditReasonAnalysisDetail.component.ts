import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { BIReportsRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSARCreditReasonAnalysisDetail.html',
    providers: [CommonLookUpUtilsService]
})

export class CreditReasonAnalysisDetailComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public bCode: string = this.businessCode();
    public commonGrifFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public isRegion: boolean = false;
    public pageId: string;

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true, required: true },
        { name: 'ContractType', type: MntConst.eTypeText, required: true },
        { name: 'ContractTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'InvoiceCreditReasonCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'InvoiceCreditReasonDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'TradingMonth', type: MntConst.eTypeText, required: true, disabled: true },
        { name: 'TradingYear', type: MntConst.eTypeText, required: true, disabled: true }
    ];

    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARCreditReasonAnalysisDetail'
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGrifFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARCREDITREASONANALYSISDETAIL;
        this.browserTitle = this.pageTitle = 'Credit Analysis By Reason';
        this.utils.setTitle(this.pageTitle);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.buildGrid();

        if (!this.isReturning()) {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;

            this.setControlValue('BusinessCode', this.bCode);
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());

            this.riExchange.getParentHTMLValue('TradingMonth');
            this.riExchange.getParentHTMLValue('TradingYear');
            this.riExchange.getParentHTMLValue('InvoiceCreditReasonCode');
            this.riExchange.getParentHTMLValue('InvoiceCreditReasonDesc');
            this.riExchange.getParentHTMLValue('ServiceTypeCode');
            this.riExchange.getParentHTMLValue('ServiceTypeDesc');

            this.isRegion = this.riExchange.getParentHTMLValue('ViewType') === 'Region';

            if (this.parentMode === 'CreditAnalysisBusiness' && this.isRegion) {
                this.riExchange.getParentAttributeValue('RegionCode');
                this.riExchange.getParentAttributeValue('RegionDesc');
            } else {
                if (this.riExchange.getParentAttributeValue('BranchNumber') || this.riExchange.getParentAttributeValue('BranchName')) {
                    this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                    this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                } else {
                    this.riExchange.getParentHTMLValue('BranchNumber');
                    this.riExchange.getParentHTMLValue('BranchName');
                }
            }

            this.setControlValue('ContractType', this.riExchange.getParentAttributeValue('ColumnValue'));
            this.setControlValue('ContractTypeDesc', this.riExchange.getParentAttributeValue('ColumnDesc'));
        }
        this.onRiGridRefresh();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('Code', 'CreditAnalysis', 'Code', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('Code', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Description', 'CreditAnalysis', 'Description', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('Value', 'CreditAnalysis', 'Value', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('Value', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('Shortfall', 'CreditAnalysis', 'Shortfall', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('Shortfall', MntConst.eAlignmentRight);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = 'Detail';
        formData[this.serviceConstants.Function] = 'CreditAnalysisByReason';
        formData[this.serviceConstants.BusinessCode] = this.bCode;
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
        formData['ContractType'] = this.getControlValue('ContractType');
        formData['TradingMonth'] = this.getControlValue('TradingMonth');
        formData['TradingYear'] = this.getControlValue('TradingYear');
        formData['LanguageCode'] = this.riExchange.LanguageCode();
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['InvoiceCreditReasonCode'] = this.getControlValue('InvoiceCreditReasonCode');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGrifFunction.onRefreshClick();
        }
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.Details.GetAttribute('Code', 'rowid') === 'Total') {
            return;
        } else {
            this.navigate('', BIReportsRoutes.ICABSARCREDITREASONANALYSISDETAILDETAIL, {
                ReasonCode: this.riGrid.Details.GetValue('Code'),
                ReasonDesc: this.riGrid.Details.GetValue('Description')
            });
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

}
