import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';
import { BCompanySearchComponent } from '@app/internal/search/iCABSBCompanySearch';
import { BIReportsRoutes } from '@base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ServiceTypeSearchComponent } from '@app/internal/search/iCABSBServiceTypeSearch.component';
import { StaticUtils } from '@shared/services/static.utility';
import { SysCharConstants } from '@shared/constants/syscharservice.constant';
import { SystemInvoiceCreditReasonSearchComponent } from '@app/internal/search/iCABSSSystemInvoiceCreditReasonSearch.component';

@Component({
    templateUrl: 'iCABSARCreditReasonAnalysis.html',
    providers: [CommonLookUpUtilsService]
})

export class CreditReasonAnalysisComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('companyDropdown') companyDropdown: BCompanySearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('serviceTypeSearch') serviceTypeSearch: ServiceTypeSearchComponent;


    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public pageId: string;
    public pageType: string;
    public isBranch: boolean = false;
    public isBusiness: boolean = false;
    public isRegion: boolean = false;

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessCode', required: true, type: MntConst.eTypeCode, disabled: true },
        { name: 'BusinessDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'CompanyCode', type: MntConst.eTypeCode },
        { name: 'CompanyCode', type: MntConst.eTypeCode },
        { name: 'CompanyDesc', type: MntConst.eTypeText },
        { name: 'InvoiceCreditReasonCode', type: MntConst.eTypeCode },
        { name: 'InvoiceCreditReasonDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText },
        { name: 'TradingMonth', type: MntConst.eTypeText, required: true },
        { name: 'TradingYear', type: MntConst.eTypeText, required: true },
        { name: 'ViewType', type: MntConst.eTypeText, value: 'branch' },
        { name: 'RegionCode', disabled: true, type: MntConst.eTypeText },
        { name: 'RegionDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ReportBy', type: MntConst.eTypeText, value: 'Branch' }
    ];

    public dropdown: Record<string, Record<string, Object>> = {
        company: {
            params: {
                businessCode: this.businessCode(),
                countryCode: this.countryCode(),
                parentMode: 'LookUp'
            },
            active: { id: '', text: '' },
            disabled: false
        },
        serviceTypeSearch: {
            params: {
                parentMode: 'LookUp'
            },
            disabled: false
        }
    };

    public ellipsis: Record<string, Record<string, Object>> = {
        invoiceCreditReasonCode: {
            childConfigParams: {
                'parentMode': 'LookUp'
            },
            component: SystemInvoiceCreditReasonSearchComponent,
            disabled: false
        }
    };

    public readonly viewType: Array<Object> = [
        { value: 'branch', text: 'Branch' },
        { value: 'region', text: 'Region' }
    ];

    public readonly reportBy: Array<Object> = [
        { value: 'Branch', text: 'Branch' },
        { value: 'Reason', text: 'Reason' }
    ];

    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARCreditReasonAnalysis'
    };

    constructor(injector: Injector, public sysCharConstants: SysCharConstants, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.browserTitle = this.pageTitle = 'Credit Analysis By Reason';
        this.utils.setTitle(this.pageTitle);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        this.pageType = StaticUtils.capitalizeFirstLetterAlphaNumberic(this.riExchange.getParentHTMLValue('type'));
        this.pageId = PageIdentifier['ICABSARCREDITREASONANALYSIS' + this.pageType.toUpperCase()];
        this.isBranch = this.pageType === 'Branch';
        this.isBusiness = this.pageType === 'Business';
        this.isRegion = this.pageType === 'Region';
        this.loadSysChars();
        this.buildGrid();
        super.ngAfterContentInit();

        if (this.isReturning()) {
            if (this.getControlValue('ServiceTypeCode')) {
                this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
            }
            if (this.getControlValue('CompanyDesc')) {
                this.dropdown.company['active'] = {
                    id: this.getControlValue('CompanyCode'),
                    text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                };
            }
            this.pageParams.gridCacheRefresh = false;
            this.onRiGridRefresh();
        } else {

            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;

            if (this.isRegion) {
                this.commonLookup.getRegionDesc().subscribe((data) => {
                    this.setControlValue('RegionCode', data[0][0]['RegionCode']);
                    this.setControlValue('RegionDesc', data[0][0]['RegionDesc']);
                });
            }

            if (this.parentMode === 'CreditAnalysisBusiness' || this.parentMode === 'CreditAnalysisRegion') {

                this.riExchange.getParentHTMLValue('TradingMonth');
                this.riExchange.getParentHTMLValue('TradingYear');
                this.riExchange.getParentHTMLValue('BusinessCode');
                this.riExchange.getParentHTMLValue('ServiceTypeCode');
                this.riExchange.getParentHTMLValue('ServiceTypeDesc');
                this.riExchange.getParentHTMLValue('CompanyCode');
                this.riExchange.getParentHTMLValue('CompanyDesc');
                this.riExchange.getParentHTMLValue('InvoiceCreditReasonCode');
                this.riExchange.getParentHTMLValue('InvoiceCreditReasonDesc');

                if (this.parentMode === 'CreditAnalysisBusiness') {
                    this.setControlValue('BranchNumber', this.riExchange.getParentAttributeValue('BranchNumber'));
                    this.setControlValue('BranchName', this.riExchange.getParentAttributeValue('BranchName'));
                }

                if (this.getControlValue('ServiceTypeCode')) {
                    this.serviceTypeSearch.setValue(this.getControlValue('ServiceTypeCode'));
                }
                if (this.getControlValue('CompanyDesc')) {
                    this.dropdown.company['active'] = {
                        id: this.getControlValue('CompanyCode'),
                        text: this.getControlValue('CompanyCode') + ' - ' + this.getControlValue('CompanyDesc')
                    };
                }

                this.disableControl('TradingMonth', true);
                this.disableControl('TradingYear', true);
                this.ellipsis.invoiceCreditReasonCode.disabled = true;
                this.dropdown.company.disabled = true;
                this.dropdown.serviceTypeSearch.disabled = true;

                this.onRiGridRefresh();
            }
            else {
                let date: Date = new Date();
                this.setControlValue('TradingMonth', date.getMonth() + 1);
                this.setControlValue('TradingYear', date.getFullYear());
                this.setControlValue('BusinessCode', this.businessCode());

                if (this.isBusiness) {
                    this.setControlValue('BusinessCode', this.utils.getBusinessCode());
                    this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.utils.getBusinessCode(), this.countryCode()));
                }

                // tslint:disable-next-line: no-unused-expression
                this.riExchange.getParentHTMLValue('BranchNumber') || this.setControlValue('BranchNumber', this.utils.getBranchCode());
                this.commonLookup.getBranchName(this.getControlValue('BranchNumber')).then(data => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('BranchName', data[0][0].BranchName);
                    }
                }).catch(error => {
                    this.displayMessage(error);
                });
            }
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('GroupCode', 'CreditAnalysis', 'GroupCode', MntConst.eTypeCode, 8, false);
        this.riGrid.AddColumnAlign('GroupCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GroupDesc', 'CreditAnalysis', 'GroupDesc', MntConst.eTypeText, 20);

        this.riGrid.AddColumn('Contract', 'CreditAnalysis', 'Contract', MntConst.eTypeText, 15, false);
        this.riGrid.AddColumnAlign('Contract', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('Shortfall', 'CreditAnalysis', 'Shortfall', MntConst.eTypeText, 15, false);
        this.riGrid.AddColumnAlign('Shortfall', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('Job', 'CreditAnalysis', 'Job', MntConst.eTypeText, 15, false);
        this.riGrid.AddColumnAlign('Job', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('Product', 'CreditAnalysis', 'Product', MntConst.eTypeText, 15, false);
        this.riGrid.AddColumnAlign('Product', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('Total', 'CreditAnalysis', 'Total', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('Total', MntConst.eAlignmentRight);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = this.pageType;
        formData[this.serviceConstants.Function] = 'CreditAnalysisByReason';
        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        if (this.isBusiness) {
            formData['ViewType'] = this.getControlValue('ViewType');
        }

        if (this.isBranch) {
            formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        }

        if (this.isRegion) {
            formData[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
            formData['ReportBy'] = this.getControlValue('ReportBy');
        }
        formData['CompanyCode'] = this.getControlValue('CompanyCode');
        formData['TradingMonth'] = this.getControlValue('TradingMonth');
        formData['TradingYear'] = this.getControlValue('TradingYear');
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['InvoiceCreditReasonCode'] = this.getControlValue('InvoiceCreditReasonCode');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation + this.pageType, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onGridBodyDoubleClick(): void {
        let rowID = this.riGrid.Details.GetAttribute('GroupCode', 'rowid');
        let isBranchViewType: boolean = this.getControlValue('ViewType') === 'branch';
        if (this.pageType !== 'Business' && rowID === 'TOTAL') {
            return;
        }

        if (this.isBusiness || this.isRegion) {
            if (isBranchViewType) {
                this.setAttribute('BranchNumber', this.riGrid.Details.GetValue('GroupCode'));
                this.setAttribute('BranchName', this.riGrid.Details.GetValue('GroupDesc'));
            }
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'Product':
            case 'Job':
            case 'Contract': {
                if (this.isRegion && this.getControlValue('ReportBy') === 'Reason') {
                    this.displayMessage(MessageConstant.Message.PageNotDeveloped);
                } else {
                    this.navigate('', BIReportsRoutes.ICABSARCREDITREASONANALYSISDETAIL, {
                        GroupRowID: rowID,
                        ColumnDesc: this.riGrid.CurrentColumnName,
                        ColumnValue: this.riGrid.CurrentColumnName.charAt(0)
                    });
                }
                break;
            }
            case 'GroupCode':
                if (this.isBusiness) {
                    if (rowID !== 'TOTAL') {
                        if (isBranchViewType) {
                            this.navigate('CreditAnalysisBusiness', BIReportsRoutes.ICABSARCREDITREASONANALYSIS, {
                                type: 'branch'
                            });
                        } else {
                            this.displayMessage(MessageConstant.Message.PageNotDeveloped);
                        }
                    } else {
                        this.setAttribute('InvoiceCreditReasonCode', this.getControlValue('InvoiceCreditReasonCode'));
                        this.setAttribute('InvoiceCreditReasonDesc', this.getControlValue('InvoiceCreditReasonDesc'));
                        if (isBranchViewType) {
                            this.setAttribute('BranchNumber', 0);
                            this.setAttribute('BranchName', 'Totals');
                            this.setAttribute('RegionCode', 0);
                            this.setAttribute('RegionDesc', 'Totals');

                            this.navigate('CreditAnalysisBusinessTotal', BIReportsRoutes.ICABSARCREDITREASONANALYSISDETAILDETAIL);
                        } else {
                            this.displayMessage(MessageConstant.Message.PageNotDeveloped);
                        }
                    }
                }
                break;
            default:
                return;
                break;
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

    public onViewTypeChange(): void {
        this.onRiGridRefresh();
    }

    public onInvoiceCreditReasonChange(): void {
        let reasonCode = this.getControlValue('InvoiceCreditReasonCode');
        if (reasonCode) {
            let lookupIP = [
                {
                    'table': 'SystemInvoiceCreditReason',
                    'query': {
                        'InvoiceCreditReasonCode   ': reasonCode
                    },
                    'fields': ['InvoiceCreditReasonSystemDesc']
                }
            ];

            this.ajaxSource.next(this.ajaxconstant.START);
            this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data && data[0].length > 0) {
                    this.setControlValue('InvoiceCreditReasonDesc', data[0][0].InvoiceCreditReasonSystemDesc);
                } else {
                    this.setControlValue('InvoiceCreditReasonCode', '');
                    this.setControlValue('InvoiceCreditReasonDesc', '');
                }
            });
        } else {
            this.setControlValue('InvoiceCreditReasonDesc', '');
        }
    }

    public onInvoiceCreditReasonReceived(data: Record<string, Object>): void {
        if (data) {
            this.setControlValue('InvoiceCreditReasonCode', data.InvoiceCreditReasonCode);
            this.setControlValue('InvoiceCreditReasonDesc', data.InvoiceCreditReasonDesc);
        }
    }

    public onServiceTypeChange(data: Record<string, Object>): void {
        if (data) {
            this.setControlValue('ServiceTypeCode', data.ServiceTypeCode);
            this.setControlValue('ServiceTypeDesc', data.ServiceTypeDesc);
        }
    }

    public onCompanyChange(data: Record<string, Object>): void {
        if (data) {
            this.setControlValue('CompanyCode', data.CompanyCode);
            this.setControlValue('CompanyDesc', data.CompanyDesc);
        }
    }

    private loadSysChars(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let sysCharQuery: QueryParams = new QueryParams();
        let syscharList: Array<number> = [
            this.sysCharConstants.SystemCharEnableCompanyCode
        ];
        sysCharQuery.set(this.serviceConstants.Action, '0');
        sysCharQuery.set(this.serviceConstants.BusinessCode, this.businessCode());
        sysCharQuery.set(this.serviceConstants.CountryCode, this.countryCode());
        sysCharQuery.set(this.serviceConstants.SystemCharNumber, syscharList);
        this.httpService.sysCharRequest(sysCharQuery).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            try {
                if (data && data.records && data.records.length) {
                    this.pageParams.vSCEnableCompanyCode = data.records[0].Required;
                }
            } catch (error) {
                this.displayMessage(error);
            }
        });
    }

}
