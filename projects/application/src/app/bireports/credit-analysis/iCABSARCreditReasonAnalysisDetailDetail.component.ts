import { AfterContentInit, Component, OnInit, Injector, ViewChild } from '@angular/core';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { ContractManagementModuleRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSARCreditReasonAnalysisDetail.html',
    providers: [CommonLookUpUtilsService]
})

export class CreditReasonAnalysisDetailDetailComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public bCode: string = this.businessCode();
    public commonGrifFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public isRegion: boolean = false;
    public pageId: string;
    public isBranch: boolean;

    public gridConfig: Record<string, number> = {
        totalItem: 1,
        itemsPerPage: 10
    };

    public controls: IControls[] = [
        { name: 'BranchName', type: MntConst.eTypeInteger, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeText, disabled: true },
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true, required: true },
        { name: 'ContractType', type: MntConst.eTypeText, required: true },
        { name: 'ContractTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'InvoiceCreditReasonCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'InvoiceCreditReasonDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'RegionCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'RegionDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceTypeCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ServiceTypeDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'TradingMonth', type: MntConst.eTypeText, required: true, disabled: true },
        { name: 'TradingYear', type: MntConst.eTypeText, required: true, disabled: true }
    ];

    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'ApplicationReport/iCABSARCreditReasonAnalysisDetailDetail'
    };

    constructor(injector: Injector) {
        super(injector);
        this.commonGrifFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSARCREDITREASONANALYSISDETAILDETAIL;
        this.browserTitle = this.pageTitle = 'Credit Analysis By Reason';
        this.utils.setTitle(this.pageTitle);
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.buildGrid();

        if (!this.isReturning()) {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;

            this.setControlValue('BusinessCode', this.bCode);
            this.setControlValue('BusinessDesc', this.utils.getBusinessText());

            this.riExchange.getParentHTMLValue('TradingMonth');
            this.riExchange.getParentHTMLValue('TradingYear');
            this.setControlValue('InvoiceCreditReasonCode', this.riExchange.getParentAttributeValue('ReasonCode'));
            this.setControlValue('InvoiceCreditReasonDesc', this.riExchange.getParentAttributeValue('ReasonDesc'));

            this.riExchange.getParentHTMLValue('ServiceTypeCode');
            this.riExchange.getParentHTMLValue('ServiceTypeDesc');
            this.riExchange.getParentHTMLValue('RegionCode');
            this.riExchange.getParentHTMLValue('RegionDesc');
            this.riExchange.getParentHTMLValue('ContractTypeDesc');
            this.setControlValue('ContractType', this.getControlValue('ContractTypeDesc').charAt(0));

            if (this.parentMode === 'CreditAnalysisBusinessTotal' || this.parentMode === 'CreditAnalysisReasonTotal') {
                this.setControlValue('RegionCode', 0);
                this.setControlValue('RegionDesc', '');
                this.setControlValue('BranchNumber', 0);
                this.setControlValue('BranchName', '');

                // tslint:disable-next-line: no-unused-expression
                this.riExchange.getParentHTMLValue('InvoiceCreditReasonCode') || this.setControlValue('InvoiceCreditReasonCode', 'Total');
                // tslint:disable-next-line: no-unused-expression
                this.riExchange.getParentHTMLValue('InvoiceCreditReasonDesc') || this.setControlValue('InvoiceCreditReasonDesc', '');
                this.setControlValue('ContractType', 'T');
                this.isRegion = this.parentMode === 'CreditAnalysisReasonTotal';
                this.isBranch = this.parentMode === 'CreditAnalysisBusinessTotal';

            } else {
                if (this.getControlValue('RegionCode') !== '') {

                    if (this.riExchange.getParentHTMLValue('ReportBy') === 'Reason') {
                        this.setControlValue('InvoiceCreditReasonCode', this.riExchange.getParentHTMLValue('BranchNumber'));
                        this.setControlValue('InvoiceCreditReasonDesc', this.riExchange.getParentHTMLValue('BranchName'));
                        this.setControlValue('ContractTypeDesc', this.riExchange.getParentAttributeValue('ColumnDesc'));
                        this.setControlValue('ContractType', this.riExchange.getParentAttributeValue('ColumnValue'));
                    } else {
                        this.isRegion = true;
                        this.isBranch = false;
                    }
                } else {
                    this.riExchange.getParentHTMLValue('BranchNumber');
                    this.riExchange.getParentHTMLValue('BranchName');

                    this.isRegion = false;
                    this.isBranch = true;
                }

            }
        }
        this.onRiGridRefresh();
    }

    private buildGrid(): void {

        this.riGrid.Clear();

        this.riGrid.AddColumn('AccountNumber', 'CreditAnalysis', 'AccountNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('AccountNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractNumber', 'CreditAnalysis', 'ContractNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractName', 'CreditAnalysis', 'ContractName', MntConst.eTypeText, 20);

        this.riGrid.AddColumn('BranchNumber', 'CreditAnalysis', 'BranchNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('BranchNumber', false);

        this.riGrid.AddColumn('BranchName', 'CreditAnalysis', 'BranchName', MntConst.eTypeCode, 20);
        this.riGrid.AddColumnAlign('BranchName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('BranchName', false);

        this.riGrid.AddColumn('ServiceBranchNumber', 'CreditAnalysis', 'ServiceBranchNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('ServiceBranchNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('ServiceBranchNumber', false);

        this.riGrid.AddColumn('ServiceBranchName', 'CreditAnalysis', 'ServiceBranchName', MntConst.eTypeCode, 20);
        this.riGrid.AddColumnAlign('ServiceBranchName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('ServiceBranchName', false);

        this.riGrid.AddColumn('ReasonCode', 'CreditAnalysis', 'ReasonCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ReasonCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ReasonDesc', 'CreditAnalysis', 'ReasonDesc', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ReasonDesc', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('LostBusinessDesc', 'CreditAnalysis', 'LostBusinessDesc', MntConst.eTypeCode, 20);
        this.riGrid.AddColumnAlign('LostBusinessDesc', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('LostBusinessDesc', false);

        this.riGrid.AddColumn('PremiseNumber', 'CreditAnalysis', 'PremiseNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProductCode', 'CreditAnalysis', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Quantity', 'CreditAnalysis', 'Quantity', MntConst.eTypeInteger, 6);

        this.riGrid.AddColumn('ItemValue', 'CreditAnalysis', 'ItemValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('ItemValue', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('CompanyCode', 'CreditAnalysis', 'CompanyCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('CompanyCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Invoice', 'CreditAnalysis', 'Invoice', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('Invoice', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ExpenseCode', 'CreditAnalysis', 'ExpenseCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('ExpenseCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('TurnoverShortfall', 'CreditAnalysis', 'TurnoverShortfall', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('TurnoverShortfall', MntConst.eAlignmentRight);

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm))
            return;
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.Level] = 'DetailDetail';
        formData[this.serviceConstants.Function] = 'CreditAnalysisByReason';
        formData[this.serviceConstants.BusinessCode] = this.bCode;
        formData[this.serviceConstants.BranchNumber] = this.getControlValue('BranchNumber');
        formData[this.serviceConstants.RegionCode] = this.getControlValue('RegionCode');
        formData['ContractType'] = this.getControlValue('ContractType');
        formData['TradingMonth'] = this.getControlValue('TradingMonth');
        formData['TradingYear'] = this.getControlValue('TradingYear');
        formData['LanguageCode'] = this.riExchange.LanguageCode();
        formData['ServiceTypeCode'] = this.getControlValue('ServiceTypeCode');
        formData['InvoiceCreditReasonCode'] = this.getControlValue('InvoiceCreditReasonCode');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGrifFunction.onRefreshClick();
        }
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.Details.GetAttribute('AccountNumber', 'rowid') === 'Total') {
            return;
        } else {
            let windowPath: string;
            let exchangeMode: string;
            let params: Record<string, string> = {};
            let contractType: string = this.getControlValue('ContractType');
            let contarctNum: string = this.riGrid.Details.GetAttribute('ContractNumber', 'rowid');

            switch (this.riGrid.CurrentColumnName) {

                case 'AccountNumber':
                    windowPath = ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE;
                    exchangeMode = 'LoadByKeyFields';
                    params = {
                        'AccountNumber': this.riGrid.Details.GetAttribute('AccountNumber', 'rowid')
                    };
                    break;

                case 'ContractNumber':
                    windowPath = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                    exchangeMode = 'LoadByKeyFields';
                    params = {

                        'ContractNumber': contarctNum,
                        'currentContractType': contractType
                    };
                    break;

                case 'PremiseNumber':
                    windowPath = ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE;
                    exchangeMode = 'LoadByKeyFields';
                    params = {
                        'contracttypecode': contractType,
                        'ContractNumber': contarctNum,
                        'ContractName': this.riGrid.Details.GetValue('ContractName'),
                        'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber')
                    };
                    break;

                case 'ProductCode':

                    switch (contractType) {
                        case 'C':
                            windowPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT;
                            break;

                        case 'J':
                            windowPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB;
                            break;

                        case 'P':
                            windowPath = ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE;
                            params = {
                                currentContractType: contractType
                            };
                            break;
                    }

                    params.ServiceCoverROWID = this.riGrid.Details.GetAttribute('ProductCode', 'rowid');
                    exchangeMode = 'ServiceCoverGrid';

                    break;
            }
            this.navigate(exchangeMode, windowPath, params);
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

}
