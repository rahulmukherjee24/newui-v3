import { Component, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BIReportsRoutesConstant } from '@app/base/PageRoutes';
import { LostBusinessRequestOutstadingBranchComponent } from './iCABSARLostBusinessRequestsOutstandingBranch.component';
import { LostBusinessRequestOutcomeComponent } from './iCABSARLostBusinessRequestsOutcome.component';
import { SharedModule } from '@shared/shared.module';



@Component({
    template: `<router-outlet></router-outlet>
    `
})
export class ClientRetentionRootComponent {
    constructor() {

    }
}

@NgModule({
    imports: [
        SharedModule,
        HttpClientModule,
        RouterModule.forChild([
            {
                path: '', component: ClientRetentionRootComponent, children: [
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGBRANCH, component: LostBusinessRequestOutstadingBranchComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEBUSINESS, component: LostBusinessRequestOutcomeComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEBRANCH, component: LostBusinessRequestOutcomeComponent },
                    { path: BIReportsRoutesConstant.ICABSARLOSTBUSINESSREQUESTSOUTCOMEDETAIL, component: LostBusinessRequestOutcomeComponent }
                ], data: { domain: 'CLENT RETENTION' }
            }

        ])
    ],
    declarations: [
        ClientRetentionRootComponent,
        LostBusinessRequestOutstadingBranchComponent,
        LostBusinessRequestOutcomeComponent
    ]

})

export class ClientRetentionModule {
}
