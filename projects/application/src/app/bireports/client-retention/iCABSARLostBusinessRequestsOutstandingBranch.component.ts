import { Component, Injector, OnInit, ViewChild, AfterContentInit } from '@angular/core';

import { ContractManagementModuleRoutes } from '@app/base/PageRoutes';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARLostBusinessRequestsOutstandingBranch.html'
})

export class LostBusinessRequestOutstadingBranchComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public alertMessage: any;
    public controls = [
        { name: 'BranchName', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', readonly: true, disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'FromDate', required: true, type: MntConst.eTypeDate },
        { name: 'ToDate', required: true, type: MntConst.eTypeDate },
        { name: 'WhatDate', value: 'All' }
    ];
    public hasGridData: boolean = false;
    public pageId: string;
    public pageTitle: string;

    constructor(private injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARLOSTBUSINESSREQUESTSOUTSTANDINGBRANCH;
        this.browserTitle = this.pageTitle = 'Client Retention Requests Outstanding';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setControlValue('BranchNumber', this.parentMode === 'PotentialLosses' ? this.riExchange.getParentHTMLValue('BranchNumber') : this.utils.getBranchCode());
        this.setControlValue('BranchName', this.parentMode === 'PotentialLosses' ? this.riExchange.getParentHTMLValue('BranchName') : this.utils.getBranchTextOnly());
        this.buildGrid();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (this.isReturning()) {
            this.populateGrid();
        } else {
            this.pageParams.gridConfig = {
                pageSize: 10,
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                gridCacheRefresh: true
            };
            this.pageParams.gridCurrentPage = 1;
            this.lookUPforBusiness();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('xEmployeeCode', 'xEmployeeCode', 'xEmployeeCode ', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('ContractNumber', 'ContractNumber', 'ContractNumber ', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('PremiseNumber', 'PremiseNumber', 'PremiseNumber ', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('ProductCode', 'ProductCode', 'ProductCode ', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('Address', 'Address', 'Address ', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('DateRequested', 'DateRequested', 'DateRequested', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('EarliestEffectiveDate', 'DateRequested', 'EarliestEffectiveDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('LastContactDate', 'LastContactDate', 'LastContactDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('LBValue', 'LBValue', 'LBValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('CommenceDate', 'CommenceDate', 'CommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('EffectiveDate', 'EffectiveDate', 'EffectiveDate', MntConst.eTypeDate, 20);
        this.riGrid.AddColumn('Narrative', 'Narrative', 'Narrative', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('DaysOpen', 'DaysOpen', 'DaysOpen', MntConst.eTypeInteger, 10);

        this.riGrid.AddColumnOrderable('xEmployeeCode', true);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('DateRequested', true);
        this.riGrid.AddColumnOrderable('EarliestEffectiveDate', true);
        this.riGrid.AddColumnOrderable('LBValue', true);
        this.riGrid.AddColumnOrderable('EffectiveDate', true);
        this.riGrid.AddColumnOrderable('DaysOpen', true);

        this.riGrid.AddColumnAlign('xEmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('Address', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('DateRequested', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EarliestEffectiveDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('LastContactDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('LBValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('CommenceDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('EffectiveDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('DaysOpen', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnScreen('Narrative', false);
        this.riGrid.Complete();
    }

    private lookUPforBusiness(): void {
        let lookupIP = [
            {
                'table': 'Business',
                'query': {
                    'BusinessCode': this.businessCode()
                },
                'fields': ['SalesTradingYear', 'SalesTradingMonth']
            }
        ];
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0] && data[0].length) {
                let salesTradingMonth: number = data[0][0].SalesTradingMonth;
                let salesTradingYear: number = data[0][0].SalesTradingYear;
                let fromDate: Date = new Date(salesTradingYear, salesTradingMonth - 1, 1);
                let toDate: Date = new Date(salesTradingYear, salesTradingMonth, 0);
                this.setControlValue('FromDate', this.globalize.parseDateToFixedFormat(fromDate) as string);
                this.setControlValue('ToDate', this.globalize.parseDateToFixedFormat(toDate) as string);
            } else {
                this.setControlValue('FromDate', '');
                this.setControlValue('ToDate', '');
            }
        });
    }

    private populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {
                'Level': 'Branch',
                'Function': 'Outstanding',
                'BranchNumber': this.getControlValue('BranchNumber'),
                'FromDate': this.getControlValue('FromDate'),
                'ToDate': this.getControlValue('ToDate'),
                'WhatDate': this.getControlValue('WhatDate')
            };
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridConfig.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSARLostBusinessRequestsOutstandingBranch', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = false;
                        this.displayMessage(data);
                    } else {
                        this.hasGridData = true;
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = false;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onGridBodyDoubleClick(): void {
        let currentContractType: string = this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty');
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                if (this.riGrid.Details.GetValue('ContractNumber') !== '') {
                    this.navigate('ClientRetention', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                        currentContractType: currentContractType,
                        ContractNumber: this.riGrid.Details.GetValue('ContractNumber')

                    });
                }
                break;
            case 'PremiseNumber':
                if (this.riGrid.Details.GetValue('PremiseNumber') !== '') {
                    this.navigate('ClientRetention', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                        CurrentContractType: currentContractType,
                        PremiseRowID: this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty') === 'x' ? '' : this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty')
                    });
                }
                break;
            case 'ProductCode':
                if (this.riGrid.Details.GetValue('ProductCode') !== '') {
                    this.navigate('ClientRetention', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT, {
                        CurrentContractType: currentContractType,
                        ServiceCoverRowID: this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty') === 'x' ? '' : this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty')
                    });
                }
                break;

        }
    }

    public onRiGridRefresh(): void {
        this.pageParams.gridConfig.gridCacheRefresh = true;
        this.populateGrid();
    }


}
