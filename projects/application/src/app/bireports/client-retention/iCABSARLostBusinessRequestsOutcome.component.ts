import { ContractManagementModuleRoutes } from './../../base/PageRoutes';
import { Component, Injector, OnInit, ViewChild, AfterContentInit, OnDestroy } from '@angular/core';


import { BIReportsRoutes } from '@app/base/PageRoutes';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent, IGridHandlers } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARLostBusinessRequestsOutcome.html',
    providers: [CommonLookUpUtilsService]
})

export class LostBusinessRequestOutcomeComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    protected controls: IControls[] = [
        { name: 'BusinessCode', disabled: true, required: true, value: this.utils.getBusinessCode(), type: MntConst.eTypeCode },
        { name: 'BusinessDesc', disabled: true, required: true, value: this.utils.getBusinessText(), type: MntConst.eTypeText },
        { name: 'FromDate', required: true, type: MntConst.eTypeDate },
        { name: 'ToDate', required: true, type: MntConst.eTypeDate },
        { name: 'GroupBy', value: 'Branch' },
        { name: 'BranchNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'Column', disabled: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText }
    ];

    public commonGridFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public isBusiness: boolean = false;
    public isBranch: boolean = false;
    public isDetail: boolean = false;
    public pageId: string;
    public pageTitle: string;
    public pageType: string;
    public level: string;
    public detailLevel: string;

    public gridConfig: Record<string, number> = {
        itemsPerPage: 10,
        totalItem: 1
    };

    constructor(private injector: Injector, private commonLookup: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.level = this.riExchange.getParentHTMLValue('DetailLevel');
        this.buildGrid();
    }

    public ngAfterContentInit(): void {
        this.pageType = this.utils.capitalizeFirstLetter(this.riExchange.getParentHTMLValue('pageType'));
        this.pageId = this.level.includes('Detail') ? PageIdentifier.ICABSARLOSTBUSINESSREQUESTSOUTCOMEDETAIL : PageIdentifier['ICABSARLOSTBUSINESSREQUESTSOUTCOME' + this.pageType.toUpperCase()];
        this.browserTitle = this.pageTitle = 'Client Retention Contract Terminations Requests Outcome';
        this['is' + this.pageType] = this.pageType;

        super.ngAfterContentInit();

        if (this.isReturning()) {
            if (this.riExchange.getParentHTMLValue('parentMode') !== 'Business') {
                this.setControlValue('BranchNumber', this.utils.getBranchCode());
            }
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridCurrentPage = 1;
            this.setValues();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        if (this.level) {
            this.riGrid.AddColumn('ContractNumber', 'LostBusiness', 'ContractNumber', MntConst.eTypeCode, 10);
            this.riGrid.AddColumn('ContractName', 'LostBusiness', 'ContractName', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('RequestNo', 'LostBusiness', 'RequestNo', MntConst.eTypeInteger, 3);
            if (this.riExchange.getParentHTMLValue('Column') === 'TermDel') {
                this.riGrid.AddColumn('RequestOriginDesc', 'LostBusiness', 'RequestOriginDesc', MntConst.eTypeText, 40);
            }
            this.riGrid.AddColumn('PremiseNumber', 'LostBusiness', 'PremiseNumber', MntConst.eTypeInteger, 4);
            this.riGrid.AddColumn('ProductCode', 'LostBusiness', 'ProductCode', MntConst.eTypeCode, 6);
            this.riGrid.AddColumn('RequestDate', 'LostBusiness', 'RequestDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('BranchNumber', 'LostBusiness', 'BranchNumber', MntConst.eTypeInteger, 6);
            this.riGrid.AddColumn('EmployeeCode', 'LostBusiness', 'EmployeeCode', MntConst.eTypeCode, 10);
            this.riGrid.AddColumn('Status', 'LostBusiness', 'Status', MntConst.eTypeText, 20);
            if (this.riExchange.getParentHTMLValue('Column') === 'Balance'
                || this.riExchange.getParentHTMLValue('Column') === '7Days'
                || this.riExchange.getParentHTMLValue('Column') === '30Days'
                || this.riExchange.getParentHTMLValue('Column') === 'GT30Days') {
                this.riGrid.AddColumn('PendingValue', 'LostBusiness', 'PendingValue', MntConst.eTypeText, 20);
            } else {
                this.riGrid.AddColumn('Outcome', 'LostBusiness', 'Outcome', MntConst.eTypeText, 20);
                if (this.riExchange.getParentHTMLValue('Column') === 'TermDel') {
                    this.riGrid.AddColumn('LostBusinessDesc', 'LostBusiness', 'LostBusinessDesc', MntConst.eTypeText, 40);
                }
                this.riGrid.AddColumn('LostValue', 'LostBusiness', 'LostValue', MntConst.eTypeText, 20);
                this.riGrid.AddColumn('SavedValue', 'LostBusiness', 'SavedValue', MntConst.eTypeText, 20);
            }
        } else {
            this.riGrid.AddColumn('GroupCode', 'ClientRetention', 'GroupCode', MntConst.eTypeText, 6);

            let colName: Object = ['BroughtForward', 'ThisPeriod', 'TotalPending', 'FullPrice', 'SavedRedDel', 'SavedOther', 'TermDel', 'PercentageSaved', 'PercentageSavedYTOD', 'Balance', '7Days', '30Days', 'GT30Days'];
            for (let idx = 0; idx < 13; idx++) {
                this.riGrid.AddColumn(colName[idx], 'ClientRetention', colName[idx], MntConst.eTypeText, 10);
                this.riGrid.AddColumnAlign(colName[idx], MntConst.eAlignmentRight);
            }
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            let operationSuffix: string = this.level ? 'Detail' : this.pageType;
            search.set(this.serviceConstants.Action, '2');

            let formData: Object = this.level ? {
                'Function': 'RequestsOutcomeDetail',
                'Level': this.riExchange.getParentHTMLValue('Level'),
                'FromDate': this.getControlValue('FromDate'),
                'ToDate': this.getControlValue('ToDate'),
                'GroupRowID': this.riExchange.getParentHTMLValue('GroupRowID'),
                'Column': this.riExchange.getParentHTMLValue('Column'),
                'LanguageCode': this.riExchange.LanguageCode(),
                'BranchNumber': this.riExchange.getParentHTMLValue('BranchNumber')
            } : {
                    'Function': 'RequestsOutcome',
                    'Level': this.pageType,
                    'FromDate': this.getControlValue('FromDate'),
                    'ToDate': this.getControlValue('ToDate'),
                    'GroupBy': this.pageType === 'Business' ? this.getControlValue('GroupBy') : 'Employee'
                };
            if (this.pageType === 'Branch') {
                formData['BranchNumber'] = this.getControlValue('BranchNumber');
            }
            formData[this.serviceConstants.BusinessCode] = this.getControlValue('BusinessCode');

            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.PageSize] = 10;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            formData[this.serviceConstants.GridHeaderClickedColumn] = '';
            formData[this.serviceConstants.GridSortOrder] = 'Ascending';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'ApplicationReport/iCABSARLostBusinessRequestsOutcome' + operationSuffix, search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = false;
                        this.gridConfig.totalItem = 1;
                        this.displayMessage(data);
                    } else {
                        this.hasGridData = true;
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                        this.riGrid.Execute(data);
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 100);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = false;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onChangeCommon(): void {
        this.gridConfig.totalItem = 0;
        this.pageParams.gridCurrentPage = 1;
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }
    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGridFunction.onRefreshClick();
        }
    }

    public onGridBodyDoubleClick(): void {
        let currentColumn = this.riGrid.CurrentColumnName;
        let rowId = this.riGrid.Details.GetAttribute(currentColumn, 'rowid');
        let isDrillable: boolean = this.riGrid.Details.GetAttribute(currentColumn, 'drilldown');
        let params: Record<string, string> = {};
        this.detailLevel = this.isBusiness ? 'Business' : 'Branch';
        if ((this.isBusiness && this.getControlValue('GroupBy') === 'Branch') || this.isBranch) {
            if ((currentColumn === 'ContractNumber') && rowId !== 'TOTAL') {
                this.navigate('LostBusinessRequestsOutcomeBusiness', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    ContractNumber: this.riGrid.Details.GetValue(currentColumn)
                });
            }
            if (currentColumn === 'GroupCode' && rowId !== 'TOTAL') {
                this.setAttribute('rowId', rowId);
                this.navigate('Business', BIReportsRoutes.ICABSARLOSTBUSINESSREQUESTSOUTCOMEBRANCH, {
                    'FromDate': this.getControlValue('FromDate'),
                    'ToDate': this.getControlValue('ToDate'),
                    'Level': this.getControlValue('GroupBy'),
                    'pageType': this.getControlValue('GroupBy'),
                    'BranchNumber': this.riGrid.Details.GetValue('GroupCode')
                });
            }

            if (currentColumn !== 'GroupCode' && isDrillable) {
                this.setAttribute('GroupRowID', this.riGrid.Details.GetAttribute('GroupCode', 'rowid'));
                this.setAttribute('Column', this.riGrid.headerArray[0][this.riGrid.CurrentCell]['text']);
                this.setAttribute('GroupCode', this.riGrid.Details.GetValue('GroupCode'));
                this.setAttribute('GroupDesc', this.riGrid.Details.GetAttribute('GroupCode', 'title'));
                params['pageType'] = 'Branch';
                params['DetailLevel'] = 'Detail' + this.detailLevel;
                params['Level'] = 'Employee';
                params['GroupRowID'] = this.riGrid.Details.GetAttribute('GroupCode', 'rowid');
                params['Column'] = this.riGrid.CurrentColumnName;
                switch (this.level) {
                    case '':
                        if (this.riExchange.getParentHTMLValue('BranchNumber')) {
                            params['BranchNumber'] = (this.riExchange.getParentHTMLValue('BranchNumber'));
                        } else {
                            params['BranchNumber'] = this.pageType === 'Business'
                                ? this.riGrid.Details.GetValue('GroupCode') : this.utils.getBranchCode();
                            params['Level'] = this.pageType === 'Business'
                                ? 'Branch' : 'Employee';
                        }
                        break;
                    case 'Branch':
                        params['BranchNumber'] = this.riExchange.getParentHTMLValue('BranchNumber');
                        break;
                }
                params['EmployeeCode'] = this.riGrid.Details.GetValue('GroupCode');
                params['FromDate'] = this.getControlValue('FromDate');
                params['ToDate'] = this.getControlValue('ToDate');
                this.navigate(this.getControlValue('GroupBy'), BIReportsRoutes.ICABSARLOSTBUSINESSREQUESTSOUTCOMEDETAIL, params);
            }
        } else if (this.isBusiness && this.getControlValue('GroupBy') === 'Region') {
            this.displayMessage(MessageConstant.Message.PageNotCovered, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
        } else {
            if (isDrillable && this.riGrid.Details.GetAttribute('GroupCode', 'rowid') !== 'TOTAL') {
                this.displayMessage(MessageConstant.Message.PageNotCovered, CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
            }
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

    public setValues(): void {
        let date: Date = new Date();
        let fromDate: Date = new Date(date.getFullYear(), date.getMonth(), 1);
        let setColumnDescription: string;
        let disableDateFields: boolean = this.level === 'DetailBranch' || this.level === 'DetailBusiness';
        this.disableControl('FromDate', disableDateFields);
        this.disableControl('ToDate', disableDateFields);

        switch (this.pageType) {
            case 'Branch':
                if (this.level === 'DetailBranch') {
                    this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
                    this.commonLookup.getEmployeeSurname(this.riExchange.getParentHTMLValue('EmployeeCode')).then((data) => {
                        if (data[0][0])
                            this.setControlValue('EmployeeSurname', data[0][0].EmployeeSurname);
                    });
                } else {
                    this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('BranchNumber') || this.utils.getBranchCode());
                    this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName') || this.utils.getBranchTextOnly(this.getControlValue('BranchNumber')));
                }
                this.setControlValue('FromDate', this.riExchange.getParentHTMLValue('FromDate') || this.globalize.parseDateToFixedFormat(new Date(new Date().getFullYear(), new
                    Date().getMonth(), 1)));
                this.setControlValue('ToDate', this.riExchange.getParentHTMLValue('ToDate') || this.utils.TodayAsDDMMYYYY());
                if (this.riExchange.getParentHTMLValue('parentMode')) {
                    this.onRiGridRefresh();
                }
                if (this.level) {
                    switch (this.riExchange.getParentHTMLValue('Column')) {
                        case 'FullPrice':
                            setColumnDescription = 'Saved Full Price';
                            break;
                        case 'SavedRedDel':
                            setColumnDescription = 'Saved Reduction / Deletion';
                            break;
                        case 'SavedOther':
                            setColumnDescription = 'Saved Other';
                            break;
                        case 'LostRedDel':
                            setColumnDescription = 'Lost Other';
                            break;
                        case 'TermDel':
                            setColumnDescription = 'Lost Terminated / Deleted';
                            break;
                        case 'Reinst':
                            setColumnDescription = 'Reinstated';
                            break;
                        case 'Balance':
                            setColumnDescription = 'Balance';
                            break;
                        case '7Days':
                            setColumnDescription = '0 - 7 Days';
                            break;
                        case '30Days':
                            setColumnDescription = '8 - 30 Days';
                            break;
                        case 'GT30Days':
                            setColumnDescription = 'Greater Than 30 Days';
                            break;
                    }
                    this.setControlValue('Column', setColumnDescription);
                }
                break;
            case 'Business':
                this.setControlValue('FromDate', this.globalize.parseDateToFixedFormat(fromDate) as string);
                this.setControlValue('ToDate', this.globalize.parseDateToFixedFormat(date) as string);
                break;
        }
    }
}
