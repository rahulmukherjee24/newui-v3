import { Action } from '@ngrx/store';

export class CustomAction implements Action {
    type: string;

    constructor(public payload: any) { }
}
