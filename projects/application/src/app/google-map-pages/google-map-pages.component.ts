import { Component, ViewContainerRef, OnInit, AfterViewInit, OnDestroy, ViewChild, NgZone } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '@shared/services/auth.service';
import { ComponentInteractionService } from '@shared/services/component-interaction.service';
import { ErrorService } from '@shared/services/error.service';
import { RiExchange } from '@shared/services/riExchange';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class GoogleMapPagesRootComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('errorModal') public errorModal;
    public authJson: any;
    public componentInteractionSubscription: Subscription;
    public displayNavBar: boolean;
    public errorSubscription: Subscription;
    public muleJson: any;
    public routerSubscription: Subscription;
    public showErrorHeader: boolean = true;

    constructor(viewContainerRef: ViewContainerRef, private riExchange: RiExchange, private _authService: AuthService, private _router: Router, private _ls: LocalStorageService, private _componentInteractionService: ComponentInteractionService, private _zone: NgZone, private _errorService: ErrorService) {}

    ngOnInit(): void {
        this.displayNavBar = true;
        if (!this._authService.oauthResponse) {
            this.authJson = this._ls.retrieve('OAUTH');
        } else {
            this.authJson = this._authService.oauthResponse;
        }
    }

    ngAfterViewInit(): void {
        // statement
    }

    ngOnDestroy(): void {
        if (this.componentInteractionSubscription) {
            this.componentInteractionSubscription.unsubscribe();
        }
        if (this.errorSubscription) {
            this.errorSubscription.unsubscribe();
        }
        if (this.routerSubscription) {
            this.routerSubscription.unsubscribe();
        }
        this.riExchange.releaseReference(this);
    }
}
