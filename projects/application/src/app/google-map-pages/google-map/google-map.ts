import { Component, AfterViewInit, ElementRef, Input, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';
import { NGXLogger } from 'ngx-logger';

import { GoogleMapConstants, GoogleMapLocationData } from './google-map-utils';
import { MessageConstant } from '@shared/constants/message.constant';


@Component({
    selector: 'icabs-google-map',
    template: `<div id="map" style="height: 800px" [ngClass]="{'hidden': isHidden }"></div>`
})

export class GoogleMapComponent implements AfterViewInit, OnDestroy {
    // Decorated Properties
    @Input() isHidden: boolean;

    // Private Properties
    private map: any;
    private mapObject: any;
    private directionsService: any;
    private directionsDisplay: any;
    private travelTime: Subject<any>;

    constructor(private elem: ElementRef,
        private logger: NGXLogger) {
        this.isHidden = false;
        this.travelTime = new Subject<any>();
    }

    // Lifecycle Hooks
    public ngAfterViewInit(): void {
        this.mapObject = window['google'].maps;
        if (!this.mapObject) {
            return;
        }

        this.map = new this.mapObject.Map(this.elem.nativeElement.querySelector('#map'), GoogleMapConstants.c_obj_INIT_DEFAULT);
    }

    public ngOnDestroy(): void {
        if (this.mapObject) {
            this.mapObject = null;
        } else {
            return;
        }

        this.map = null;
        this.directionsService = null;
        this.directionsDisplay = null;
    }

    // Public Methods
    // Get Observable Source - Travel Time
    public getTravelTimeObservableSource(): Subject<number> {
        return this.travelTime;
    }

    /**
     * Calculate Travel Time
     * carryForward - Data to be sent as it with the observable response; Response key name will be same
     */
    public calculateTravelTime(origin: GoogleMapLocationData, destination: GoogleMapLocationData, carryForward?: any): any {
        let travelTimeRequestParam: any = GoogleMapConstants.c_obj_TRAVELTIME_DEFAULT;
        let emitResponse: any = {};

        this.directionsService = new this.mapObject.DirectionsService();
        this.directionsDisplay = new this.mapObject.DirectionsRenderer();
        this.directionsDisplay.setMap(this.map);

        try {
            travelTimeRequestParam[GoogleMapConstants.c_s_REQPARAM_ORIGIN] = origin.locationLatLng;
            travelTimeRequestParam[GoogleMapConstants.c_s_REQPARAM_DESTINATION] = destination.locationLatLng;

            this.directionsService.route(travelTimeRequestParam, (response, status) => {
                if (status === this.mapObject.DirectionsStatus.OK) {
                    this.directionsDisplay.setDirections(response);
                    emitResponse[GoogleMapConstants.c_s_TRAVEL_TIME] = response.routes[0].legs[0].duration.value;
                    if (carryForward !== null) {
                        emitResponse[GoogleMapConstants.c_s_CARRY_FORWARD_DATA] = carryForward;
                    }
                } else {
                    emitResponse[GoogleMapConstants.c_s_ERROR] = status;
                }

                this.travelTime.next(emitResponse);
            });
        } catch (excp) {
            this.logger.log(excp);
            emitResponse[GoogleMapConstants.c_s_ERROR] = MessageConstant.Message.GeneralError;
            this.travelTime.next(emitResponse);
        }
    }
}
