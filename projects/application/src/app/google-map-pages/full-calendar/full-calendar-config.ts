// import { Options } from 'fullcalendar';

export class FullCalendarConstants {
    // Calendar Events
    public static readonly c_s_EVENT_CLICK: string = 'eventClick';
    public static readonly c_s_EVENT_GOTODATE: string = 'gotoDate';
    public static readonly c_s_EVENT_CHANGEVIEW: string = 'changeView';
    public static readonly c_s_EVENT_VIEWRENDER: string = 'viewRender';
    public static readonly c_s_EVENT_REMOVEEVENTS: string = 'removeEvents';
    public static readonly c_s_EXTERNAL_EVENT_DROP: string = 'drop';
    public static readonly c_s_CALENDAR_EVENT_DROP: string = 'eventDrop';
    public static readonly c_s_CALENDAR_EVENT_RESIZE: string = 'eventResize';
    public static readonly c_s_CALENDAR_RENDER_EVENT: string = 'renderEvent';
    public static readonly c_s_CALENDAR_DAY_CLICK: string = 'dayClick';
    public static readonly c_s_CALENDAR_EVENTRENDER: string = 'eventAfterAllRender';
    public static readonly c_s_CALENDAR_EVENT_DRAGSTART: string = 'eventDragStart';

    // View Type
    public static readonly c_s_VIEW_MONTH: string = 'month';
    public static readonly c_s_VIEW_WEEK: string = 'agendaWeek';
    public static readonly c_s_VIEW_DAY: string = 'agendaDay';

    // Calendar Configuration
    public static readonly c_obj_CALENDAR_CONFIG_DEFAULT: any = {
        header: {
            left: 'prev, next today',
            center: 'title',
            right: 'month, agendaWeek, agendaDay'
        },
        views: {
            agendaWeek: {
                columnFormat: 'ddd D/M'
            }
        },
        eventOrder: 'start',
        aspectRatio: 2.1,
        editable: true,
        droppable: true
    };

    // Key Names
    public static readonly c_s_CALENDAR_ID: string = 'id';
    public static readonly c_s_CALENDAR_TITLE: string = 'title';
    public static readonly c_s_CALENDAR_START: string = 'start';
    public static readonly c_s_CALENDAR_END: string = 'end';
    public static readonly c_s_CALENDAR_COLOR: string = 'color';
    public static readonly c_s_CALENDAR_TEXTCOLOR: string = 'textColor';
    public static readonly c_s_CALENDAR_ALLDAY: string = 'allDay';
    public static readonly c_s_FIRST_DAY: string = 'firstDay';
    public static readonly c_s_HARD_SLOT: string = 'isVisitHardSlot';
    public static readonly c_s_OPTION: string = 'option';

    // Default Date And Time Format
    public static readonly c_s_DEFAULT_DATE_FORMAT: string = 'MM/dd/yyyy HH:mm';
}
