export class TechnicianVisitDiaryConstants {
    public static readonly c_arr_DIARY_COLOUR: Array<any> = [{
        header: {
            main: 'Diary Background Colours',
            sub: {
                text: 'Colour',
                desc: 'Description'
            }
        },
        body: [{
            color: '#FFF79A',
            text: ['Technician normal working hours']
        }, {
            color: '#808080',
            text: ['Outside the technician\'s normal working hours']
        }]
    }, {
        header: {
            main: 'Events & Visits/Appointments',
            sub: {
                text: 'Colour',
                desc: 'Description'
            }
        },
        body: [{
            color: '#FF3333',
            text: ['Visits that do not fit the Technicians day in the grid or overlap with other visits', '[NB: Overlap including travel time if google distance api turned on]']
        }, {
            color: '#FF69B4',
            text: ['Visits are too close to other visits']
        }, {
            color: '#89E894',
            text: ['Unconfirmed Appointment Visits, manual slotted and unplanned']
        }, {
            color: '#339933',
            text: ['Events that exist in iCABS Diary and/or Technicians Diary that take specific hours,', 'or are All Day Events']
        }, {
            color: '#6698FF',
            text: ['Unconfirmed appointment Visits, auto slotted and unplanned']
        }, {
            color: '#FFFF00',
            text: ['Confirmed appointment Visits, unplanned']
        }, {
            color: '#F4A433',
            text: ['Confirmed appointments without Visit start times']
        }, {
            color: '#FFFFFF',
            text: ['Confirmed Plan Visit (Visit not yet completed)']
        }, {
            color: '#D3D3D3',
            text: ['Confirmed Plan Visit (visit completed)']
        }]
    }];

    public static readonly c_arr_SORT_OPTIONS: Array<any> = [
        { value: 'CustomerNameAZ', text: 'Customer name: A - Z' },
        { value: 'CustomerNameXA', text: 'Customer name: Z - A' },
        { value: 'ContractJobNumberLH', text: 'Contract/job Number: low to high' },
        { value: 'ContractJobNumberHL', text: 'Contract/job Number: high to low' },
        { value: 'DueDateLH', text: 'Due date: earliest to most recent (default)' },
        { value: 'DueDateHL', text: 'Due date: most recent to earliest' }
    ];

    // Separators
    public static readonly c_s_LIST_RESPONSE_MAIN_SEPARATOR: string = '|';
    public static readonly c_s_LIST_RESPONSE_SUB_SEPARATOR: string = '\u0007';
    public static readonly c_s_DETAILS_RESPONSE_MAIN_SEPARATOR: string = '|';
    public static readonly c_s_DETAILS_DISPLAY_TITLE_SEPARATOR: string = '/';
    public static readonly c_s_ID_SEPARATOR: string = '^';

    // Planning Type
    public static readonly c_s_TYPE_PLANNED: string = 'Planned';
    public static readonly c_s_TYPE_UNPLANNED: string = 'Unplanned';

    // Date Formats
    public static readonly c_s_CONNECTOR_DATE_FORMAT: string = 'MM/DD/YYYY';
    public static readonly c_s_CONNECTOR_TIME_FORMAT: string = 'HH:mm';
    public static readonly c_s_DETAIL_MODAL_DATE_FORMAT: string = 'ddd DD MMMM';
    public static readonly c_s_DETAIL_MODAL_TIME_FORMAT: string = 'HH:mm';

    // Travel Time Buffer
    public static readonly c_s_TRAVELTIME_BUFFER: number = 900;

    // Visit Too Close Color
    public static readonly c_s_VISIT_TOOCLOSE_COLORCODE: string = '#FF69B4';
}
