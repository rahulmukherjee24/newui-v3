import { BaseComponent } from '@base/BaseComponent';
import { Component, OnInit, OnDestroy, Injector, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';

import { FullCalendarComponent } from '../full-calendar/full-calendar';
import { FullCalendarConstants } from '../full-calendar/full-calendar-config';
import { GoogleMapComponent } from '../google-map/google-map';
import { GoogleMapConstants, GoogleMapLocationData } from '../google-map/google-map-utils';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceApplicationModuleRoutes, ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes } from '@base/PageRoutes';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { ModalComponent } from '@shared/components/modal/modal';
import { PageIdentifier } from '@base/PageIdentifier';
import { PlanningObjectCollection, PlanningObject } from './PlanningObject';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';
import { TechnicianVisitDiaryConstants } from './TechnicianVisitDiaryConstants';
import * as moment from 'moment';

@Component({
    templateUrl: 'iCABSATechnicianVisitDiaryGrid.html'
})

export class TechnicianVisitDiaryGridComponent extends BaseComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {
    // View Children
    @ViewChild('fullCalendar') public fullCalendar: FullCalendarComponent;
    @ViewChild('gmap') public gmap: GoogleMapComponent;
    @ViewChild('legendModal') public legendModal: ModalComponent;
    @ViewChild('sortOptionsModal') public sortOptionsModal: ModalComponent;
    @ViewChild('visitDetailsModal') public visitDetailsModal: ModalComponent;

    // Private Properties
    private headerParams: any = {
        method: 'service-planning/maintenance',
        module: 'diary',
        operation: 'Application/iCABSATechnicianVisitDiaryGrid'
    };
    private messageProperties: ICabsModalVO = new ICabsModalVO();
    private isPageLoadCompleteOnReturn: boolean = false;

    // Public Properties
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'AvailableVisitFilters' },
        { name: 'BranchName' },
        { name: 'BranchNumber' },
        { name: 'BranchServiceAreaCode' },
        { name: 'CalendarEndDate' },
        { name: 'CalendarMode' },
        { name: 'CalendarStartDate' },
        { name: 'EmployeeCode' },
        { name: 'EmployeeName', disabled: true },
        { name: 'EmployeeSurname' },
        { name: 'EnableConfirmedPlanning' },
        { name: 'EndDate' },
        { name: 'FullDiaryUpdate' },
        { name: 'PassDiaryEmployeeCode' },
        { name: 'PassDiaryEmployeeName' },
        { name: 'PlanVisitRowId' },
        { name: 'StartDate' },
        { name: 'TotalVisits', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'UseGeocoding' },
        { name: 'VisitDataString' },
        { name: 'VisitFullId' },
        { name: 'VisitTime', disabled: true },
        { name: 'VisitValue', disabled: true },
        { name: 'WeekDayStart' }
    ];
    public lastUpdated: string = '';
    // Modal Properties
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public availableVisitFilters: string = '';

    // Planning Objects
    public availableVisits: PlanningObjectCollection;
    public diaryVisits: PlanningObjectCollection;
    public visitDetails: PlanningObject;
    public isRequesting: boolean = true;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSATECHNICIANVISITDIARYGRID;
        this.pageTitle = this.browserTitle = 'Diary';
        this.availableVisits = new PlanningObjectCollection();
        this.diaryVisits = new PlanningObjectCollection();
        this.visitDetails = new PlanningObject();
    }

    // Lifecycle Hooks
    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.pageParams.isFullDiaryUpdateAllowed = this.pageParams.isFullDiaryUpdateAllowed || false; //IUI-22947
        this.availableVisitFilters = 'DueDateLH'; //IUI-22947
    }

    public ngAfterViewInit(): void {
        let date: Date = new Date();
        let startDate: string;
        let endDate: string;
        this.setRequiredStatus('EmployeeCode', true); //IUI-22947

        this.pageParams.parentMode = this.parentMode = this.riExchange.getParentMode();
        // Same Property Assignment To Handle Returning Case
        this.pageParams.allDayText = this.pageParams.allDayText || MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.allDayEvent;


        if (this.isReturning()) {
            this.fullCalendar.weekFirstDay = this.getControlValue('WeekDayStart');
            this.fullCalendar.gotoView(this.getControlValue('CalendarMode'), moment(this.getControlValue('StartDate'), TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT));
            this.fullCalendar.gotoDate(moment(this.getControlValue('StartDate'), TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT));
            this.setControlValue('AvailableVisitFilters', this.availableVisitFilters);
            this.getDiaryDetails();
            this.isPageLoadCompleteOnReturn = true;
            if (this.getControlValue('CalendarMode') === FullCalendarConstants.c_s_VIEW_MONTH) {
                date = new Date(this.getControlValue('StartDate'));
                this.fullCalendar.gotoMonth(date.getFullYear(), date.getMonth());
            }
            if (this.pageParams['AvailVisits']) {
                this.availableVisits = new PlanningObjectCollection();
                this.availableVisits.data = this.pageParams['AvailVisits'];
                this.availableVisits.parse(TechnicianVisitDiaryConstants.c_s_TYPE_UNPLANNED);
            }

            return;
        }

        switch (this.parentMode) {
            case 'ServicePlanning':
                this.riExchange.getParentHTMLValue('EmployeeCode');
                this.setControlValue('EmployeeName', this.riExchange.getParentHTMLValue('EmployeeSurname0'));
                startDate = moment(this.riExchange.getParentHTMLValue('StartDate'), 'yyyy/mm/dd').format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
                this.setControlValue('StartDate', startDate);
                endDate = moment(this.riExchange.getParentHTMLValue('EndDate'), 'yyyy/mm/dd').format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
                this.setControlValue('EndDate', endDate);
                this.riExchange.getParentHTMLValue('BranchNumber');
                this.riExchange.getParentHTMLValue('BranchName');
                this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
                this.riExchange.getParentHTMLValue('EmployeeSurname');
                date = new Date(this.getControlValue('StartDate'));
                if (!this.isReturning()) {
                    this.fullCalendar.gotoMonth(date.getFullYear(), date.getMonth());
                }
                break;
            default:
                this.setControlValue('StartDate', moment().format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT));
                this.setControlValue('EndDate', moment().add('M', 1).add('d', -1).format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT));
                this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('ServiceEmployeeCode'));
                this.setControlValue('EmployeeName', this.riExchange.getParentHTMLValue('ServiceEmployeeSurname'));
                this.setControlValue('BranchNumber', this.riExchange.getParentHTMLValue('ServiceBranchNumber'));
                this.setControlValue('BranchName', this.riExchange.getParentHTMLValue('BranchName'));
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
                break;
        }

        this.getTranslatedValue(this.pageParams.allDayText, null).subscribe(data => {
            if (data) {
                this.pageParams.allDayText = data;
            }
        });

        this.setControlValue('PassEmployeeCode', this.getControlValue('EmployeeCode'));
        this.setControlValue('PassEmployeeName', this.getControlValue('EmployeeName'));

        this.clearCalendarEvents();
        this.getAvailableVisits();

        this.setControlValue('AvailableVisitFilters', this.availableVisitFilters);
        this.setControlValue('CalendarMode', FullCalendarConstants.c_s_VIEW_MONTH);
        this.setPlanningDiaryOptions();

        this.gmap.getTravelTimeObservableSource().subscribe(data => {
            if (!data[GoogleMapConstants.c_s_ERROR]) {
                this.markVisitsTooCloseToPrevious(data);
            }
        });
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Private Methods
    // Get planning diary options from service call
    private setPlanningDiaryOptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};
        let response: Promise<any>;

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetPlanningDiaryOptions';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe((data: any) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }

                for (let key in data) {
                    if (data.hasOwnProperty(key)) {
                        this.setControlValue(key, data[key]);
                    }
                }

                this.fullCalendar.weekFirstDay = this.getControlValue('WeekDayStart');
                this.pageParams.isFullDiaryUpdateAllowed = StaticUtils.convertResponseValueToCheckboxInput(this.getControlValue('FullDiaryUpdate'));
                this.getDiaryDetails();
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Get available visits for the diary
    private getAvailableVisits(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};
        let endDate: any = moment(this.fullCalendar.currentViewEnd.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT), TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetAvailVisits';
        formData['StartDate'] = this.fullCalendar.getFormattedViewStart(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['EndDate'] = endDate.add(1, 'd').format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['AvailableVisitFilters'] = this.getControlValue('AvailableVisitFilters');

        // Close sort option modal
        this.onModalCancelClick('sortOptionsModal');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }

                this.availableVisits = new PlanningObjectCollection();
                if (data.AvailVisits) {
                    this.pageParams['AvailVisits'] = this.availableVisits.data = data.AvailVisits;
                    this.availableVisits.parse(TechnicianVisitDiaryConstants.c_s_TYPE_UNPLANNED);
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Get data
    private getDiaryDetails(): void {
        if (!this.getControlValue('StartDate')) {
            return;
        }
        /**
         * @todo - setLoading()
         */
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'DiaryDetails';
        formData['StartDate'] = this.fullCalendar.getFormattedViewStart(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['EndDate'] = this.fullCalendar.getFormattedViewEnd(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['CalendarMode'] = this.getControlValue('CalendarMode');
        formData['UseGeocoding'] = this.getControlValue('UseGeocoding');
        formData['EnableConfirmedPlanning'] = StaticUtils.convertResponseValueToCheckboxInput(this.getControlValue('EnableConfirmedPlanning'));

        try {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
                this.headerParams.module,
                this.headerParams.operation,
                search,
                formData).subscribe(data => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.errorMessage) {
                        this.displayMessage('Error', data.errorMessage, data.fullError);
                        return;
                    }

                    this.pageParams['DiaryDetailsRtn'] = data.DiaryDetailsRtn;

                    this.setControlValue('TotalVisits', data.VisitCount);
                    this.setControlValue('VisitValue', data.VisitTotal);
                    this.setControlValue('VisitTime', data.VisitTime);

                    /**
                     * @todo - Check for translation
                     */
                    this.lastUpdated = data.LastUpdate;

                    // Clear Before Adding Events
                    this.clearCalendarEvents(true);
                    // Call Method To Load Calendar Events
                    if (data.DiaryDetailsRtn) {
                        this.loadEvents(data.DiaryDetailsRtn);
                    }
                }, error => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage('Error', error.errorMessage);
                });
        } catch (excp) {
            this.logger.log(excp);
        }
    }

    // Get Visit Click Data
    private getVisitClickData(category?: string): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Object = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetVisitClickData';
        formData['VisitRowID'] = this.getControlValue('VisitFullId');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }

                this.setControlValue('VisitDataString', data.VisitData);
                this.visitDetails.visitData = data.VisitData;
                if (category && category === 'P') {
                    let eventDateStart: Date = this.fullCalendar.event.start.toDate(),
                        checkDate: Date = new Date();

                    checkDate.setDate(checkDate.getDate() - 1);

                    this.visitDetails.isVisitPlanningEligible = (this.getControlValue('EnableConfirmedPlanning') === 'yes' && eventDateStart >= checkDate);
                }
                this.visitDetailsModal.show({});
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Load Calendar Events
    private loadEvents(data: string): void {
        let calendarData: Array<any> = [];
        this.diaryVisits.data = data;
        this.diaryVisits.parse(TechnicianVisitDiaryConstants.c_s_TYPE_PLANNED);

        calendarData = this.diaryVisits.getDataInCalendarFormat(this.fullCalendar.currentViewName);

        /**
         * @todo - Implement sort functionality here
         */
        if (this.fullCalendar.currentViewName !== FullCalendarConstants.c_s_VIEW_MONTH && this.getControlValue('UseGeocoding')) {
            calendarData.forEach((data, index) => {
                let currentEvent: PlanningObject;
                let nextEvent: PlanningObject;
                let origin: GoogleMapLocationData;
                let destination: GoogleMapLocationData;

                if ((index + 1) === calendarData.length) {
                    return;
                }

                currentEvent = this.diaryVisits.getVisitDetailsAtIndex(index);
                nextEvent = this.diaryVisits.getVisitDetailsAtIndex(index + 1);
                origin = new GoogleMapLocationData(currentEvent.visitGPSX, currentEvent.visitGPSY);
                destination = new GoogleMapLocationData(nextEvent.visitGPSX, nextEvent.visitGPSY);

                this.gmap.calculateTravelTime(origin, destination, index);
            });
        }

        this.fullCalendar.renderData(this.diaryVisits.getDataInCalendarFormat(this.fullCalendar.currentViewName));
    }

    // Validate And Update Visit Plan
    private validateEventRecord(id: string, startDate: string, startTime: string, endDate: string, endTime: string, text: string, undoUpdate?: any): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'ValidateEventRecord';
        formData['Id'] = id;
        formData['StartDate'] = startDate;
        formData['StartTime'] = startTime;
        formData['EndDate'] = endDate;
        formData['EndTime'] = endTime;
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['EnableConfirmedPlanning'] = this.getControlValue('EnableConfirmedPlanning');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (!data.ErrorMessage) {
                    this.refreshCalendar();
                    return;
                }
                switch (data.ErrorType) {
                    case 'error':
                        this.displayMessage('Error', data.ErrorMessage);
                        if (undoUpdate) {
                            undoUpdate();
                        }
                        break;
                    case 'warning':
                        this.displayMessage('Message', data.ErrorMessage);
                        break;
                    case 'alert':
                        this.modalAdvService.emitPrompt(new ICabsModalVO(data.ErrorMessage, null, () => {
                            this.updateEventRecord(id, startDate, startTime, endDate, endTime, text);
                        }, () => {
                            if (undoUpdate) {
                                undoUpdate();
                            }
                        }));
                        break;
                    default:
                        this.refreshCalendar();
                        break;
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Update Event Record
    private updateEventRecord(id: string, startDate: string, startTime: string, endDate: string, endTime: string, text: string): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'updateEventRecord';
        formData['Id'] = id;
        formData['StartDate'] = startDate;
        formData['StartTime'] = startTime;
        formData['EndDate'] = endDate;
        formData['EndTime'] = endTime;
        formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
        formData['EnableConfirmedPlanning'] = this.getControlValue('EnableConfirmedPlanning');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage('Error', data.errorMessage, data.fullError);
                    return;
                }
                this.refreshCalendar();
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Clear Calendar Events
    private clearCalendarEvents(keepUnplanned?: boolean): void {
        this.fullCalendar.clear();
        this.diaryVisits = new PlanningObjectCollection();
        if (!keepUnplanned) {
            this.availableVisits = new PlanningObjectCollection();
        }
    }

    // Display Planned Event Details
    private displayDayEventDetails(category: string): void {
        this.visitDetails.modalSubTitleText = this.getSubTitleText();

        switch (category) {
            case 'BH':
            case 'NH':
            case 'PD':
            case 'TL':
                this.visitDetails.populateModalData({
                    title: this.fullCalendar.eventTitle,
                    subTitle: this.getSubTitleText()
                });
                this.visitDetailsModal.show({});
                break;
            case 'DE':
                this.visitDetails.isVisitPlanningEligible = this.visitDetails.hasVisitPlan = this.visitDetails.hasVisitServiceCover = this.visitDetails.canRefreshVisitCalendar = false;
                this.visitDetails.canUndoVisitPlanning = true;
                this.getVisitClickData();
                break;
            case 'P':
                this.visitDetails.isVisitPlanningEligible = false;
                this.visitDetails.hasVisitPlan = this.visitDetails.hasVisitServiceCover = this.visitDetails.canUndoVisitPlanning = this.visitDetails.canRefreshVisitCalendar = true;
                this.getVisitClickData('P');
                break;
            case 'AV':
                this.visitDetails.isVisitPlanningEligible = false;
                this.visitDetails.hasVisitPlan = this.visitDetails.hasVisitServiceCover = this.visitDetails.canUndoVisitPlanning = this.visitDetails.canRefreshVisitCalendar = true;
                this.getVisitClickData();
                break;
        }
    }

    // Populate SubTitle
    private getSubTitleText(): string {
        let eventStartDate: string = '',
            eventEndDate: string = '',
            eventStartTime: string = '',
            eventEndTime: string = '',
            eventText: string;

        eventStartDate = this.fullCalendar.getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_DATE_FORMAT);
        eventEndDate = this.fullCalendar.getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_DATE_FORMAT);
        eventStartTime = this.fullCalendar.getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_TIME_FORMAT);
        eventEndTime = this.fullCalendar.getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_TIME_FORMAT);

        eventText = eventStartDate + ' ';
        eventText += this.fullCalendar.isAllDay ? '(' + this.pageParams.allDayText + ')' : eventStartTime;

        if (!this.fullCalendar.isAllDay) {
            eventText += ' - ';
            eventText += eventStartDate !== eventEndDate ? eventEndDate : '';
            eventText += eventEndTime !== '23:59' ? eventEndTime : '';
        }

        return eventText;
    }

    // Process Undo Plan
    private processUndoPlan(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        this.closeVisitModal();
        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'ProcessUndo';
        formData['VisitRowID'] = this.getControlValue('PlanVisitRowId');
        formData['EnableConfirmedPlanning'] = this.getControlValue('EnableConfirmedPlanning');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.UndoSuccess !== 'yes') {
                    this.displayMessage('Error', data.ErrorMessage);
                    return;
                }
                this.refreshCalendar();
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Plan Visit
    private planVisit(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        this.closeVisitModal();
        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'Plan';
        formData['VisitRowID'] = this.getControlValue('PlanVisitRowId');
        formData['StartDate'] = this.fullCalendar.getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['EndDate'] = this.fullCalendar.getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['RoutingVisitStartTime'] = this.utils.hmsToSeconds(this.fullCalendar.getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT));
        formData['RoutingVisitEndTime'] = this.utils.hmsToSeconds(this.fullCalendar.getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.UndoSuccess !== 'yes') {
                    this.displayMessage('Error', data.ErrorMessage);
                    return;
                }
                this.refreshCalendar();
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Display Message
    private displayMessage(type: string, message: string, fullError?: string): void {
        this.messageProperties.msg = message;
        this.messageProperties.fullError = fullError;

        this.modalAdvService['emit' + type](this.messageProperties);
    }

    // Set Visit Id In Controls
    private setVisitIdControlValues(fullId: string): void {
        this.setControlValue('VisitFullId', fullId);
        this.setControlValue('PlanVisitRowId', fullId.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR)[1]);
    }

    // Close Visit Modal
    private closeVisitModal(): void {
        this.onModalCancelClick('visitDetailsModal');
    }

    // Mark Visits With Too Close Timing
    private markVisitsTooCloseToPrevious(data: any): void {
        let currentIndex: number;
        let previousVisit: PlanningObject;
        let currentVisit: PlanningObject;
        let currentVisitStart: any;
        let previousVisitEnd: any;
        let visitTimeDifference: number;
        let calendarData: Array<any> = [];

        try {
            currentIndex = data[GoogleMapConstants.c_s_CARRY_FORWARD_DATA];
            previousVisit = this.diaryVisits.getVisitDetailsAtIndex(currentIndex);
            currentVisit = this.diaryVisits.getVisitDetailsAtIndex(currentIndex + 1);
            currentVisitStart = moment.utc(new Date(currentVisit.visitStart));
            previousVisitEnd = moment.utc(new Date(previousVisit.visitEnd));
            calendarData = this.diaryVisits.getDataInCalendarFormat();

            visitTimeDifference = currentVisitStart.diff(previousVisitEnd, 'seconds');

            if (visitTimeDifference < (data[GoogleMapConstants.c_s_TRAVEL_TIME] + TechnicianVisitDiaryConstants.c_s_TRAVELTIME_BUFFER)) {
                calendarData[currentIndex + 1][FullCalendarConstants.c_s_CALENDAR_COLOR] = TechnicianVisitDiaryConstants.c_s_VISIT_TOOCLOSE_COLORCODE;
            }

            if (currentIndex + 1 >= calendarData.length - 1) {
                this.fullCalendar.renderData(calendarData);
            }
        } catch (excp) {
            this.displayMessage('Error', excp.message, null);
        }
    }

    // Static Getters
    // Readonly Properties
    // Getter for legend details
    public get legendDetails(): any {
        return TechnicianVisitDiaryConstants.c_arr_DIARY_COLOUR;
    }

    // Getter for sort options
    public get sortOptions(): any {
        return TechnicianVisitDiaryConstants.c_arr_SORT_OPTIONS;
    }

    // Public Methods
    /**
     * Events - Used in HTML only. Not Outside the class
     */
    // Display Visit Details
    public onClickVisitDetails(event: any, index: number): void {
        event.preventDefault();

        this.visitDetails = this.availableVisits.getVisitDetailsAtIndex(index);
        this.setVisitIdControlValues(this.visitDetails.visitId);
        this.getVisitClickData();
    }

    // Navigate to PlanVisitMaintenance
    public onVisitsClick(): void {
        this.navigate('TechVisitDiary', InternalMaintenanceApplicationModuleRoutes.ICABSAPLANVISITMAINTENANCE);
    }

    // Functionality To Handle Calendar Event Click
    public onCalendarEventClick(): void {
        let fullId: string = this.fullCalendar.eventFullId,
            idParts: Array<string> = fullId.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR);

        this.setVisitIdControlValues(fullId);

        if (!fullId.length) {
            return;
        }

        // If calendar is in month mode navigate to day mode and stop execution
        if (this.fullCalendar.currentViewName === FullCalendarConstants.c_s_VIEW_MONTH) {
            this.fullCalendar.gotoDate(this.fullCalendar.eventStart);
            return;
        }

        this.displayDayEventDetails(idParts[0]);
    }

    public onCalendarDayClick(data: any): void {
        let dateSelected: Date = new Date(data.clickDate.format('YYYY/MM/DD'));
        let eventDate: string = this.globalize.formatDateToLocaleFormat(dateSelected).toString();
        let eventStartTime: string = moment(data.clickDate._d).utc().format('HH:mm');
        let eventEndTime: string = moment(data.clickDate._d).utc().add(30, 'm').format('HH:mm');
        this.navigate('TechVisitDiary', InternalMaintenanceServiceModuleRoutes.ICABSADIARYENTRY, {
            PassDiaryEmployeeCode: this.getControlValue('EmployeeCode'),
            PassDiaryEmployeeName: this.getControlValue('EmployeeName'),
            DiaryEventNumber: '',
            DiaryDate: eventDate,
            PassDiaryStartTime: eventStartTime,
            PassDiaryEndTime: eventEndTime
        });
    }

    // Functionality To Handle Calendar View Render
    public onCalendarViewRender(view: any): void {
        let viewName: string = this.fullCalendar.currentViewName,
            endDate: Date,
            startDate: Date;

        if (this.isReturning() && !this.isPageLoadCompleteOnReturn) {
            return;
        }

        this.setControlValue('CalendarMode', viewName);
        this.clearCalendarEvents(true);

        // Dates are returned as Moment object
        startDate = this.fullCalendar.eventStart.toDate();
        endDate = this.fullCalendar.eventEnd.add(-1, 'd').toDate();

        this.setControlValue('StartDate', this.fullCalendar.eventStart.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT));
        this.setControlValue('EndDate', this.fullCalendar.eventEnd.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT));

        if (this.getControlValue('WeekDayStart')) {
            this.getDiaryDetails();
        }
    }

    // On External Event Drop
    public onExternalEventDrop(data: any): void {
        let visitDetails: PlanningObject,
            date: any = data.dropDate,
            sequence: number = data.index;

        if (this.fullCalendar.currentViewName !== FullCalendarConstants.c_s_VIEW_MONTH) {
            // Get event object
            visitDetails = this.availableVisits.getVisitDetailsAtIndex(sequence);

            visitDetails.visitStart = date.utc().format();
            visitDetails.visitEnd = date.utc().format();

            this.validateEventRecord(visitDetails.visitId,
                date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                visitDetails.visitTitle);
        }
    }

    public onCalendarEventDrop(data: any): void {
        let event: any = data.calendarEvent,
            eventFullId: string = event.id,
            idParts: Array<string> = eventFullId.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR),
            eventType: string = idParts[0],
            eventId: string = idParts[1],
            startDate: any = this.fullCalendar.currentViewStart.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
            endDate: any = this.fullCalendar.currentViewEnd.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);

        if (!eventType) {
            return;
        }

        switch (eventType) {
            case 'BH':
            case 'NH':
            case 'TL':
            case 'DE':
            case 'PD':
                data.undo();
                break;
            case 'AV':
                if (this.pageParams.isFullDiaryUpdateAllowed) {
                    this.validateEventRecord(
                        eventFullId,
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        event.title,
                        data.undo
                    );
                }
                break;
            case 'P':
                if (!event.hardslot && this.pageParams.isFullDiaryUpdateAllowed) {
                    /**
                     * @todo - Google Map Functionality; validateEventRecord Will Be Called From Within Google Map Functionality
                     */
                    this.validateEventRecord(
                        eventFullId,
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        event.title,
                        data.undo
                    );
                } else {
                    data.undo();
                }
                break;
        }
    }

    // Refresh Calendar And Available Visits
    public refreshCalendar(): void {
        this.closeVisitModal();
        this.clearCalendarEvents();
        this.getAvailableVisits();
        this.getDiaryDetails();
    }

    // Open Sort Options Modal
    public onClickSort(): void {
        this.sortOptionsModal.show({}, false);
    }

    // Sort Option Change
    public onSortOptionChange(): void {
        this.setControlValue('AvailableVisitFilters', this.availableVisitFilters);
    }

    // Ok Button Click On Sort Modal
    public onSortOkClick(): void {
        this.getAvailableVisits();
    }

    // Close Modal With Name Passed
    public onModalCancelClick(modal: string): void {
        this[modal].childModal.hide();
    }

    // Open Legends Modal
    public onClickLegend(): void {
        this.legendModal.show({}, false);
    }

    // Navigate To Visit Conflicts Page
    public onClickVisitConflict(): void {
        /**
         * @todo - Implement the following navigation when the page is ready
         * WindowPath="/wsscripts/riHTMLWrapper.p?riFileName=Service/iCABSSeVisitConflictsGrid.htm"
         * riExchange.Mode = "ServicePlanning": window.location = WindowPath
         */
        this.displayMessage('Message', MessageConstant.Message.PageNotCovered);
    }

    // Plan Visit
    public onPlanClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.plan, null, () => {
            this.planVisit();
        }, () => {
            this.closeVisitModal();
        }));
    }

    // Navigate To SCM
    public onServiceCoverClick(): void {
        this.setAttribute('ServiceCoverRowID', this.visitDetails.serviceCoverRowID);
        this.navigate('TechVisitDiary', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE);
    }

    // Undo Plan Visit
    public onUndoClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.undoPlan, null, () => {
            this.processUndoPlan();
        }, () => {
            this.closeVisitModal();
        }));
    }

    // Print Calendar
    public printCalendar(): void {
        window.print();
    }
}
