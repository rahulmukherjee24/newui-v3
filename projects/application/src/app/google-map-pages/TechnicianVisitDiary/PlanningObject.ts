import { TechnicianVisitDiaryConstants } from './TechnicianVisitDiaryConstants';
import { FullCalendarConstants } from '../full-calendar/full-calendar-config';

// Planning Object Class
export class PlanningObject {
    // Private Properties
    private id: string;
    private title: string;
    private start: string;
    private end: string;
    private color: string;
    private textColor: string;
    private isAllDay: boolean;
    private gpsX: number;
    private gpsY: number;
    private isHardSlot: boolean;
    private travelTime: string;
    private sequenceNumber: number;
    // Readonly Propeties
    private rowId: string;
    private scRowId: string;
    private visitDataString: string;
    private contractType: string;
    private planStatus: string;
    private modalTitle: string;
    private modalSubTitle: string;
    private modalBody: Array<Array<string>>;
    // Button Show/Hide Flags; ReadOnly Properties
    private isDiaryUpdatatable: boolean;
    private isPlanningEligible: boolean;
    private hasVisits: boolean;
    private hasServiceCover: boolean;
    private canUndoPlanning: boolean;
    private canRefreshCalendar: boolean;

    // Constructor
    constructor() {
        this.id = '';
        this.title = '';
        this.start = '';
        this.end = '';
        this.color = '';
        this.textColor = '';
        this.isAllDay = false;
        this.gpsX = -1;
        this.gpsY = -1;
        this.isHardSlot = false;
        this.travelTime = '';
        this.sequenceNumber = -1;
        this.rowId = '';
        this.scRowId = '';
        this.visitDataString = '';
        this.contractType = '';
        this.planStatus = '';
        this.modalTitle = '';
        this.modalSubTitle = '';
        this.modalBody = [];

        // Button Show/Hide Flags
        this.isDiaryUpdateAllowed = false;
        this.isPlanningEligible = false;
        this.hasVisits = false;
        this.hasServiceCover = false;
        this.canUndoPlanning = false;
        this.canRefreshCalendar = false;
    }

    // Getters And Setters
    // Plan ID
    public get visitId(): string {
        return this.id;
    }

    public set visitId(value: string) {
        this.rowId = value.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR)[1];
        this.id = value;
    }

    // Plan Title
    public get visitTitle(): string {
        return this.title;
    }

    public set visitTitle(value: string) {
        this.title = value;
    }

    // Plan Start
    public get visitStart(): string {
        return this.start;
    }

    public set visitStart(value: string) {
        this.start = value;
    }

    // Plan End
    public get visitEnd(): string {
        return this.end;
    }

    public set visitEnd(value: string) {
        this.end = value;
    }

    // Color
    public get visitColor(): string {
        return this.color;
    }

    public set visitColor(value: string) {
        this.color = value;
    }

    // Text Color
    public get visitTextColor(): string {
        return this.textColor;
    }

    public set visitTextColor(value: string) {
        this.textColor = value;
    }

    // All Day Visit
    public get isAllDayVisit(): boolean {
        return this.isAllDay;
    }

    public set isAllDayVisit(value: boolean) {
        this.isAllDay = value;
    }

    // Visit GPS X
    public get visitGPSX(): number {
        return this.gpsX;
    }

    public set visitGPSX(value: number) {
        this.gpsX = value;
    }

    // Visit GPS Y
    public get visitGPSY(): number {
        return this.gpsY;
    }

    public set visitGPSY(value: number) {
        this.gpsY = value;
    }

    // Visit Hard Slot
    public get isVisitHardSlot(): boolean {
        return this.isHardSlot;
    }

    public set isVisitHardSlot(value: boolean) {
        this.isHardSlot = value;
    }

    // Visit Travel Time
    public get visitTravelTime(): string {
        return this.travelTime;
    }

    public set visitTravelTime(value: string) {
        this.travelTime = value;
    }

    // Visit Data
    public get visitData(): string {
        return this.visitDataString;
    }

    public set visitData(value: string) {
        this.visitDataString = value;
        this.parseVisitDataString();
    }

    // Visit Data
    public get sequence(): number {
        return this.sequenceNumber;
    }

    public set sequence(value: number) {
        this.sequenceNumber = value;
    }

    // Readonly Properties
    // Row ID
    public get visitRowId(): string {
        return this.rowId;
    }

    // Modal Header
    public get modalTitleText(): string {
        return this.modalTitle;
    }

    // Modal Sub Header
    public get modalSubTitleText(): string {
        return this.modalSubTitle;
    }

    public set modalSubTitleText(text: string) {
        this.modalSubTitle = text;
    }

    // Modal Body
    public get modalBodyTexts(): string[][] {
        return this.modalBody;
    }

    // Diary Update Allowed
    public get isDiaryUpdateAllowed(): boolean {
        return this.isDiaryUpdatatable;
    }

    public set isDiaryUpdateAllowed(value: boolean) {
        this.isDiaryUpdatatable = value;
    }

    // Planning Eligible
    public get isVisitPlanningEligible(): boolean {
        return this.isPlanningEligible;
    }

    public set isVisitPlanningEligible(value: boolean) {
        this.isPlanningEligible = value;
    }

    // Visit Plan Available
    public get hasVisitPlan(): boolean {
        return this.hasVisits;
    }

    public set hasVisitPlan(value: boolean) {
        this.hasVisits = value;
    }

    // Visit Service Cover Available
    public get hasVisitServiceCover(): boolean {
        return this.hasServiceCover;
    }

    public set hasVisitServiceCover(value: boolean) {
        this.hasServiceCover = value;
    }

    // Undo Visit Plan
    public get canUndoVisitPlanning(): boolean {
        return this.canUndoPlanning;
    }

    public set canUndoVisitPlanning(value: boolean) {
        this.canUndoPlanning = value;
    }

    // Undo Visit Plan
    public get canRefreshVisitCalendar(): boolean {
        return this.canRefreshCalendar;
    }

    public set canRefreshVisitCalendar(value: boolean) {
        this.canRefreshCalendar = value;
    }

    // Service Cover Row ID
    public get serviceCoverRowID(): string {
        return this.scRowId;
    }

    // Contract Type Code
    public get planStatusCode(): string {
        return this.planStatus;
    }

    // Contract Type Code
    public get contractTypeCode(): string {
        return this.contractType;
    }

    // Private Methods
    // Parse Visit Data String Receieved From Service And Populate Modal Data
    private parseVisitDataString(): void {
        let visitDataParts: Array<string> = this.visitDataString.split(TechnicianVisitDiaryConstants.c_s_DETAILS_RESPONSE_MAIN_SEPARATOR);
        this.modalTitle = '';
        this.modalBody = [];

        this.contractType = visitDataParts[0];
        this.planStatus = visitDataParts[7];

        this.modalTitle = visitDataParts[0]
            + TechnicianVisitDiaryConstants.c_s_DETAILS_DISPLAY_TITLE_SEPARATOR
            + visitDataParts[1]
            + TechnicianVisitDiaryConstants.c_s_DETAILS_DISPLAY_TITLE_SEPARATOR
            + visitDataParts[2];

        this.modalBody.push([visitDataParts[6]]);
        this.modalBody.push(['Visit Frequency', ': ', visitDataParts[3]]);
        this.modalBody.push(['Address', ': ', visitDataParts[4]]);
        this.scRowId = visitDataParts[5];

        this.hasVisits = true;
    }

    // Public Methods
    /**
     * Converter for the boolean properties in the class;
     * Use this for setting value in the boolean properties
     */
    public convertToBoolean(value: any): boolean {
        return (1 === value || '1' === value || 'true' === value) as boolean;
    }

    // Populate Modal Data; Used For Setting Modal Data Without Service Response
    public populateModalData(options: any): void {
        this.modalTitle = options.title || '';
        this.modalSubTitle = options.subTitle || '';
        this.modalBody = options.body || [];
    }
}

// Planning Object Collection Class
export class PlanningObjectCollection {
    // ReadOnly Properties
    // Private Properties
    private rawData: string;
    private list: Array<PlanningObject>;
    private dataInCalendarFormat: Array<any>;

    // Constructor
    // Default
    constructor() {
        this.rawData = '';
        this.list = [];
        this.dataInCalendarFormat = [];
    }

    // Getters And Setters
    // Raw Data
    public set data(value: string) {
        this.rawData = value;
    }

    // Visit List
    public get visitList(): Array<PlanningObject> {
        return this.list;
    }

    public set visitList(list: Array<PlanningObject>) {
        this.list = list;
    }

    // Visit Count
    public get count(): number {
        return this.list.length;
    }

    // Data In Calendar Format
    public get formattedData(): Array<any> {
        return this.dataInCalendarFormat;
    }

    // Parse And Create List Of Unplanned
    private initUnplanned(visitDetails: Array<string>): PlanningObject {
        let visit: PlanningObject = new PlanningObject();

        // Set into visit list
        visit.visitId = visitDetails[0];
        visit.visitTitle = visitDetails[1];
        visit.visitStart = '0';
        visit.visitEnd = '0';
        visit.isAllDayVisit = false;
        visit.visitGPSX = parseFloat(visitDetails[2]);
        visit.visitGPSY = parseFloat(visitDetails[3]);

        return visit;
    }

    private initPlanned(visitDetails: Array<string>, index: number): PlanningObject {
        let visit: PlanningObject = new PlanningObject();

        // Set into visit list
        visit.sequence = index;
        visit.visitId = visitDetails[0];
        visit.visitTitle = visitDetails[1];
        visit.isAllDayVisit = visit.convertToBoolean(visitDetails[2]);
        visit.visitStart = visitDetails[3];
        visit.visitEnd = visitDetails[4];
        visit.visitColor = visitDetails[5];
        visit.visitTextColor = visitDetails[6];
        visit.visitGPSX = parseFloat(visitDetails[7]);
        visit.visitGPSY = parseFloat(visitDetails[8]);
        visit.isVisitHardSlot = visit.convertToBoolean(visitDetails[9]);

        return visit;
    }

    // Public Methods
    // Parse Visits
    public parse(type?: string): Array<PlanningObject> {
        if (!this.rawData) {
            return;
        }
        let visits: Array<any> = this.rawData.split(TechnicianVisitDiaryConstants.c_s_LIST_RESPONSE_MAIN_SEPARATOR);

        visits.forEach((value, index) => {
            if (!value) {
                return;
            }

            let visitDetails: Array<string> = value.split(TechnicianVisitDiaryConstants.c_s_LIST_RESPONSE_SUB_SEPARATOR);

            this.add((type === TechnicianVisitDiaryConstants.c_s_TYPE_UNPLANNED) ? this.initUnplanned(visitDetails) : this.initPlanned(visitDetails, index));
        });

        return this.list;
    }

    // Sort Visits
    public sort(): Array<PlanningObject> {
        return this.list;
    }

    // Add Visit
    public add(visit: PlanningObject): void {
        if (!this.list) {
            this.list = [];
        }
        this.list.push(visit);
    }

    // Get Details - For Index
    public getVisitDetailsAtIndex(index: number): PlanningObject {
        return this.list[index];
    }

    // Get Data In Calendar Format
    public getDataInCalendarFormat(view?: string): Array<any> {
        let dataInCalendarFormat: Array<any> = [];

        if (!this.list || !this.list.length) {
            return;
        }

        this.list.forEach(data => {
            let eventSource: any = {};

            eventSource[FullCalendarConstants.c_s_CALENDAR_ID] = data[FullCalendarConstants.c_s_CALENDAR_ID];
            eventSource[FullCalendarConstants.c_s_CALENDAR_TITLE] = data[FullCalendarConstants.c_s_CALENDAR_TITLE];
            eventSource[FullCalendarConstants.c_s_CALENDAR_COLOR] = data[FullCalendarConstants.c_s_CALENDAR_COLOR];
            eventSource[FullCalendarConstants.c_s_CALENDAR_TEXTCOLOR] = data[FullCalendarConstants.c_s_CALENDAR_TEXTCOLOR];
            eventSource[FullCalendarConstants.c_s_CALENDAR_ALLDAY] = data['isAllDay'];
            eventSource[FullCalendarConstants.c_s_CALENDAR_START] = data[FullCalendarConstants.c_s_CALENDAR_START];
            eventSource[FullCalendarConstants.c_s_CALENDAR_END] = data['isAllDay'] ? data[FullCalendarConstants.c_s_CALENDAR_START] : data[FullCalendarConstants.c_s_CALENDAR_END];
            eventSource[FullCalendarConstants.c_s_CALENDAR_END] = data['isAllDay'] ? data[FullCalendarConstants.c_s_CALENDAR_START] : data[FullCalendarConstants.c_s_CALENDAR_END];
            eventSource[FullCalendarConstants.c_s_HARD_SLOT] = data[FullCalendarConstants.c_s_HARD_SLOT];

            dataInCalendarFormat.push(eventSource);
        });

        return dataInCalendarFormat;
    }
}
