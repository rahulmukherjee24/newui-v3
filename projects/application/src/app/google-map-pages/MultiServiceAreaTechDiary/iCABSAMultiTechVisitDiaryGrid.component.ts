import { Component, Injector, OnInit, AfterContentInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';

import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs/Rx';

import { ErrorConstant } from '@shared/constants/error.constant';
import { IControls } from '@base/ControlsType';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PeopleModuleRoutes } from '@base/PageRoutes';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';
import { InternalMaintenanceApplicationModuleRoutes, ContractManagementModuleRoutes } from '@base/PageRoutes';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { IDraggedSet } from '@shared/directives/draggable.directive';
import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';

import { PlanningObjectCollection, PlanningObject } from './../TechnicianVisitDiary/PlanningObject';
import { MultiAreaTechDiaryConstants } from './MultiAreaTechDiaryConstants';
import { TechnicianVisitDiaryConstants } from './../TechnicianVisitDiary/TechnicianVisitDiaryConstants';
import { LegendModalComponent } from './../common/legend-modal/legend-modal.component';
import { SortOptionsModalComponent } from './../common/sort-options/sort-options-modal.component';
import { VisitDetailsModalComponent, ICommandEmitter } from './../common/visit-details/visit-details-modal.component';
import { FullCalendarComponent } from './../full-calendar/full-calendar';
import { VisitDetailsModalConstants } from './../common/visit-details/VisitDetailsModalConstants';

import * as moment from 'moment';

@Component({
    templateUrl: 'iCABSAMultiTechVisitDiaryGrid.html'
})

export class MultiTechnicianVisitDiaryGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, AfterViewInit, OnDestroy {
    @ViewChild('legendModal') public legendModal: LegendModalComponent;
    @ViewChild('sortModal') public sortModal: SortOptionsModalComponent;
    @ViewChild('visitDetailsModal') public visitDetailsModal: VisitDetailsModalComponent;
    @ViewChild('fullCalendar0') public fullCalendar0: FullCalendarComponent;
    @ViewChild('fullCalendar1') public fullCalendar1: FullCalendarComponent;
    @ViewChild('fullCalendar2') public fullCalendar2: FullCalendarComponent;

    private index: number = -1;
    private headerParams: any = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Application/iCABSAMultiTechVisitDiaryGrid'
    };

    protected pageId: string;
    protected controls: IControls[] = [
        { name: 'BranchServiceAreaCode' },
        { name: 'PlanningDate', type: MntConst.eTypeDate },
        { name: 'PlanFrom' },
        { name: 'PlanTo' },
        { name: 'SearchDate', type: MntConst.eTypeDate },
        { name: 'PlanVisitRowId' },
        { name: 'VisitFullId' }
    ];

    public employeeVisible: number = -1;
    public visitDetails: PlanningObject;
    public visitDetailsModalData: Object;

    constructor(injector: Injector,
        private location: Location,
        private logger: NGXLogger,
        private modalAdvService: ModalAdvService) {
        super(injector);

        this.pageId = PageIdentifier.ICABSAMULTITECHVISITDIARYGRID;
        this.browserTitle = this.pageTitle = 'Diary';

        this.visitDetails = new PlanningObject();
        this.visitDetailsModalData = {};
    }

    //#region Getters And Setters
    public get config(): Record<string, any> {
        return MultiAreaTechDiaryConstants.c_obj_CALENDAR_CONFIG;
    }
    //#endregion

    //#region LifeCycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();

        this.pageParams.employeeDetailsList = [];
        this.pageParams.employeeAvailablevisitList = [];
        this.pageParams.employeeDiaryDetails = [];
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();

        if (this.isReturning()) {
            return;
        }

        this.pageParams.serviceAreaList = this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
        this.pageParams.employeeList = this.riExchange.getParentHTMLValue('EmployeeCodes');

        if (typeof this.pageParams.serviceAreaList === 'string') {
            this.pageParams.serviceAreaList = this.pageParams.serviceAreaList.split(',');
            this.pageParams.employeeList = this.pageParams.employeeList.split(',');
        }

        if (!this.pageParams.serviceAreaList.length) {
            this.displayMessage(ErrorConstant.Message.UnexpectedError);
            this.location.back();
            return;
        }

        if (this.parentMode === 'ServicePlanning') {
            let planDate: string = this.riExchange.getParentHTMLValue('StartDate');
            this.setControlValue('PlanningDate', planDate);
            this.setControlValue('PlanFrom', planDate);
            this.setControlValue('PlanTo', this.riExchange.getParentHTMLValue('EndDate'));
            this.setControlValue('SearchDate', planDate);
        }

        this.getPlanningDiaryOptions();
        this.clearCalendars();
    }

    public ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateCalendars();
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    //#endregion

    //#region Private Methods
    private getPlanningDiaryOptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Record<string, string> = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetPlanningDiaryOptions';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).then(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.pageParams.PlanningDiaryOptions = data;

                // Convert Yes/No Values In Response To Truthy/Falsy
                this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning = StaticUtils.convertResponseValueToCheckboxInput(this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning);
                this.pageParams.PlanningDiaryOptions.FullDiaryUpdate = StaticUtils.convertResponseValueToCheckboxInput(this.pageParams.PlanningDiaryOptions.FullDiaryUpdate);

                this.getEmployeeData();
            }).catch(error => {
                this.displayMessage(error);
            });
    }

    /**
     * @todo - clear the class properties
     */
    private clearCalendars(): void {
        this.fullCalendar0.clear();
        this.fullCalendar1.clear();

        if (this.fullCalendar2) {
            this.fullCalendar2.clear();
        }
    }

    private getEmployeeData(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let requests: Array<Observable<any>> = [];

        search.set(this.serviceConstants.Action, '6');

        try {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.pageParams.employeeList.forEach(item => {
                let formDataHeader: Record<string, string> = {};
                let formDataAvailableVisits: Record<string, string> = {};
                let formDataDiaryDetails: Record<string, string> = {};

                // For Header Data
                formDataHeader[this.serviceConstants.Function] = 'GetEmployeeDetails';
                formDataHeader[this.serviceConstants.EmployeeCode] = item;

                requests.push(this.httpService.makePostRequest(this.headerParams.method,
                    this.headerParams.module,
                    this.headerParams.operation,
                    search, formDataHeader));

                // For Available Visits Data
                formDataAvailableVisits[this.serviceConstants.Function] = 'GetAvailVisits';
                formDataAvailableVisits[this.serviceConstants.EmployeeCode] = item;
                formDataAvailableVisits['StartDate'] = this.getDateControlValue('SearchDate');
                formDataAvailableVisits['EndDate'] = this.getDateControlValue('SearchDate');
                formDataAvailableVisits['AvailableVisitFilters'] = this.pageParams.sortOptions || '';

                requests.push(this.httpService.makePostRequest(this.headerParams.method,
                    this.headerParams.module,
                    this.headerParams.operation,
                    search,
                    formDataAvailableVisits));

                // For Diary Details
                formDataDiaryDetails[this.serviceConstants.Function] = 'DiaryDetails';
                formDataDiaryDetails[this.serviceConstants.EmployeeCode] = item;
                formDataDiaryDetails['StartDate'] = this.getDateControlValue('SearchDate');
                formDataDiaryDetails['EndDate'] = this.getDateControlValue('SearchDate');
                formDataDiaryDetails['CalendarMode'] = 'agendaDay';
                formDataDiaryDetails['UseGeocoding'] = this.pageParams.PlanningDiaryOptions.UseGeocoding;
                formDataDiaryDetails['EnableConfirmedPlanning'] = this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning;

                requests.push(this.httpService.makePostRequest(this.headerParams.method,
                    this.headerParams.module,
                    this.headerParams.operation,
                    search,
                    formDataDiaryDetails));
            });

            this.fetchEmployeeData(requests);

        } catch (excp) {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.logger.log(excp);
            this.displayMessage(ErrorConstant.Message.UnexpectedError);
        }
    }

    private fetchEmployeeData(requests: Array<Observable<any>>): void {
        let employeeIdx: number = 0;

        // Clear Data
        this.pageParams.employeeDetailsList = [];

        Observable.forkJoin(requests).subscribe(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            for (let idx = 0; idx < data.length;) {
                let availableVisits: PlanningObjectCollection = new PlanningObjectCollection();
                let diaryDetails: PlanningObjectCollection = new PlanningObjectCollection();
                let employeeDetails: Record<string, any> = {};

                // Name And Code
                employeeDetails['EmployeeCode'] = this.pageParams.employeeList[employeeIdx];
                employeeDetails['EmployeeName'] = data[idx].CalHeader;
                employeeDetails['EmployeeDesc'] = data[idx].CalHeader + ' (' + this.pageParams.employeeList[employeeIdx++] + ')';

                // Available Visits
                if (data[idx + 1]['AvailVisits']) {
                    availableVisits.data = data[idx + 1]['AvailVisits'];
                    employeeDetails['AvailableVisits'] = availableVisits.parse(TechnicianVisitDiaryConstants.c_s_TYPE_UNPLANNED);
                }

                // Visit Details And Diary Events
                employeeDetails['LastUpdate'] = data[idx + 2].LastUpdate;
                employeeDetails['VisitCount'] = data[idx + 2].VisitCount || 0;
                employeeDetails['VisitTime'] = data[idx + 2].VisitTime;
                employeeDetails['VisitTotal'] = this.globalize.formatCurrencyToLocaleFormat(data[idx + 2].VisitTotal || 0);
                if (data[idx + 2].DiaryDetailsRtn) {
                    diaryDetails.data = data[idx + 2]['DiaryDetailsRtn'];
                    diaryDetails.parse(TechnicianVisitDiaryConstants.c_s_TYPE_PLANNED);
                    employeeDetails['DiaryDetails'] = diaryDetails.getDataInCalendarFormat('agendaDay');
                }

                this.pageParams.employeeDetailsList.push(employeeDetails);

                idx += 3;
            }
            this.populateCalendars();
        }, error => {
            this.displayMessage(error);
        });
    }

    private sortAvailableVisits(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let requests: Array<Observable<any>> = [];

        search.set(this.serviceConstants.Action, '6');


        try {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.pageParams.employeeList.forEach(item => {
                let formData: Record<string, string> = {};

                // For Available Visits Data
                formData[this.serviceConstants.Function] = 'GetAvailVisits';
                formData[this.serviceConstants.EmployeeCode] = item;
                formData['StartDate'] = this.getDateControlValue('SearchDate');
                formData['EndDate'] = this.getDateControlValue('SearchDate');
                formData['AvailableVisitFilters'] = this.pageParams.sortOptions || '';

                requests.push(this.httpService.makePostRequest(this.headerParams.method,
                    this.headerParams.module,
                    this.headerParams.operation,
                    search,
                    formData));
            });

            Observable.forkJoin(requests).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                for (let idx = 0; idx < data.length; idx++) {
                    let availableVisits = new PlanningObjectCollection();
                    if (data[idx]['AvailVisits']) {
                        availableVisits.data = data[idx]['AvailVisits'];
                        this.pageParams.employeeDetailsList[idx]['AvailableVisits'] = availableVisits.parse(TechnicianVisitDiaryConstants.c_s_TYPE_UNPLANNED);
                    }
                }
            }, error => {
                this.displayMessage(error);
            });

        } catch (excp) {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.logger.log(excp);
            this.displayMessage(ErrorConstant.Message.UnexpectedError);
        }
    }

    private getDateControlValue(control: string): string {
        let value: string = this.getControlValue(control);

        value = moment(new Date(value)).format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);

        return value;
    }

    private populateCalendars(): void {
        this.pageParams.employeeDetailsList.forEach((_item, index) => {
            if (moment.duration(moment(this.getControlValue('PlanningDate')).diff(this['fullCalendar' + index].currentViewStart)).asDays() !== 0) {
                this['fullCalendar' + index].gotoDate(this.getDateControlValue('SearchDate'));
            } else {
                let diaryDetails: PlanningObjectCollection = new PlanningObjectCollection();
                diaryDetails.visitList = this.pageParams.employeeDetailsList[index].DiaryDetails;
                let calendarId: string = 'fullCalendar' + index;
                if (diaryDetails && this[calendarId]) {
                    this[calendarId].renderData(diaryDetails.getDataInCalendarFormat());
                }
            }

        });
    }

    // @todo - Move To Common
    private getVisitClickData(id: string, category?: string): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Record<string, string> = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetVisitClickData';
        formData['VisitRowID'] = id;

        this.visitDetails.visitId = id.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR)[1];

        try {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method,
                this.headerParams.module,
                this.headerParams.operation,
                search,
                formData).subscribe(data => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.errorMessage) {
                        this.displayMessage(data);
                        return;
                    }

                    this.visitDetails.visitData = data.VisitData;

                    if (category && category === 'P') {
                        let eventDateStart: Date = this['fullCalendar' + this.index].event.start.toDate(),
                            checkDate: Date = new Date();

                        checkDate.setDate(checkDate.getDate() - 1);

                        this.visitDetails.isVisitPlanningEligible = (this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning && eventDateStart >= checkDate);
                    }

                    this.visitDetails.isDiaryUpdateAllowed = this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning;
                    this.visitDetailsModalData['visitDetails'] = this.visitDetails;
                    this.visitDetailsModal.show(this.visitDetailsModalData);
                }, error => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });

        } catch (excp) {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.logger.log(excp);
            this.displayMessage(ErrorConstant.Message.UnexpectedError);
        }
    }

    // @todo - Move To Common
    private displayCalendarEventData(id: string): void {
        this.visitDetails = new PlanningObject();

        this.visitDetails.modalSubTitleText = this.getSubTitleText();

        switch (id[0]) {
            case 'BH':
            case 'NH':
            case 'PD':
            case 'TL':
                this.visitDetails.populateModalData({
                    title: this['fullCalendar' + this.index].eventTitle,
                    subTitle: this.visitDetails.modalSubTitleText
                });
                this.visitDetailsModal.show({});
                break;
            case 'DE':
                this.visitDetails.isVisitPlanningEligible = this.visitDetails.hasVisitPlan = this.visitDetails.hasVisitServiceCover = this.visitDetails.canRefreshVisitCalendar = false;
                this.visitDetails.canUndoVisitPlanning = true;
                this.getVisitClickData(id);
                break;
            case 'P':
                this.visitDetails.isVisitPlanningEligible = false;
                this.visitDetails.hasVisitPlan = this.visitDetails.hasVisitServiceCover = this.visitDetails.canUndoVisitPlanning = this.visitDetails.canRefreshVisitCalendar = true;
                this.getVisitClickData(id, 'P');
                break;
            case 'AV':
                this.visitDetails.isVisitPlanningEligible = false;
                this.visitDetails.hasVisitPlan = this.visitDetails.hasVisitServiceCover = this.visitDetails.canUndoVisitPlanning = this.visitDetails.canRefreshVisitCalendar = true;
                this.getVisitClickData(id);
                break;
        }
    }

    // @todo - Move To Common
    private getSubTitleText(): string {
        let eventStartDate: string = '',
            eventEndDate: string = '',
            eventStartTime: string = '',
            eventEndTime: string = '',
            eventText: string;

        eventStartDate = this['fullCalendar' + this.index].getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_DATE_FORMAT);
        eventEndDate = this['fullCalendar' + this.index].getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_DATE_FORMAT);
        eventStartTime = this['fullCalendar' + this.index].getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_TIME_FORMAT);
        eventEndTime = this['fullCalendar' + this.index].getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_DETAIL_MODAL_TIME_FORMAT);

        eventText = eventStartDate + ' ';
        eventText += this['fullCalendar' + this.index].isAllDay ? '(' + MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.allDayEvent + ')' : eventStartTime;

        if (!this['fullCalendar' + this.index].isAllDay) {
            eventText += ' - ';
            eventText += eventStartDate !== eventEndDate ? eventEndDate : '';
            eventText += eventEndTime !== '23:59' ? eventEndTime : '';
        }

        return eventText;
    }

    private planVisit(visit: PlanningObject): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'Plan';
        formData['VisitRowID'] = visit.visitId;
        formData['StartDate'] = this.fullCalendar1.getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['EndDate'] = this.fullCalendar1.getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT);
        formData['RoutingVisitStartTime'] = this.utils.hmsToSeconds(this.fullCalendar1.getFormattedEventStart(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT));
        formData['RoutingVisitEndTime'] = this.utils.hmsToSeconds(this.fullCalendar1.getFormattedEventEnd(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.UndoSuccess !== 'yes') {
                    this.displayMessage(data.ErrorMessage);
                    return;
                } else {
                    this.displayMessage(MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.planSuccess, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.onRefresh();
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error.errorMessage);
            });
    }

    private undoPlanVisit(visit: PlanningObject): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'ProcessUndo';
        formData['VisitRowID'] = visit.visitId;
        formData['EnableConfirmedPlanning'] = StaticUtils.convertCheckboxValueToRequestValue(this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.UndoSuccess !== 'yes') {
                    this.displayMessage(data.ErrorMessage);
                    return;
                } else {
                    this.displayMessage(MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.unplanSuccess, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    this.onRefresh();
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error.errorMessage);
            });
    }

    // @todo - Move To Common
    private validateEventRecord(visit: PlanningObject, startDate: string, startTime: string, endDate: string, endTime: string, employeeIdx: number, undo?: any): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: Record<string, string> = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'ValidateEventRecord';
        formData['Id'] = visit.visitId;
        formData['StartDate'] = startDate;
        formData['StartTime'] = startTime;
        formData['EndDate'] = endDate;
        formData['EndTime'] = endTime;
        formData['EmployeeCode'] = this.pageParams.employeeDetailsList[employeeIdx].EmployeeCode;
        formData['EnableConfirmedPlanning'] = StaticUtils.convertCheckboxValueToRequestValue(this.pageParams.PlanningDiaryOptions.EnableConfirmedPlanning);
        formData['Text'] = visit.visitTitle;

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            search,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (!data.ErrorMessage) {
                    this.onRefresh();
                    return;
                }
                switch (data.ErrorType) {
                    case 'error':
                        this.displayMessage(data);
                        if (undo) {
                            undo();
                        }
                        break;
                    case 'warning':
                        this.displayMessage(data.ErrorMessage);
                        break;
                    case 'alert':
                        this.modalAdvService.emitPrompt(new ICabsModalVO(data.ErrorMessage, null, () => {
                            formData[this.serviceConstants.Function] = 'updateEventRecord';
                            this.updateEventRecord(formData, search);
                        }, () => {
                            if (undo) {
                                undo();
                            }
                        }));
                        break;
                    default:
                        this.onRefresh();
                        break;
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage('Error', error.errorMessage);
            });
    }

    // Update Event Record
    private updateEventRecord(formData: Record<string, string>, query: QueryParams): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method,
            this.headerParams.module,
            this.headerParams.operation,
            query,
            formData).subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage) {
                    this.displayMessage(data);
                    return;
                }
                this.onRefresh();
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }
    //#endregion

    //#region Public Methods
    public onToggleCollapse($event: Event, index: number): void {
        $event.preventDefault();
        this.employeeVisible = this.employeeVisible === index ? -1 : index;
    }

    public onEmployeeClick(index: number): void {
        this.navigate('TechDiary', PeopleModuleRoutes.ICABSBEMPLOYEEMAINTENANCE, {
            MultiEmployeeCode: this.pageParams.employeeDetailsList[index].EmployeeCode
        });
    }

    public onSortSelect(data: string): void {
        this.pageParams.sortOptions = data;
        this.sortAvailableVisits();
    }

    public onDateChange(_data: any): void {
        let newDate: Date = new Date(this.getDateControlValue('PlanningDate'));
        let fromDate: Date = new Date(this.getDateControlValue('PlanFrom'));
        let toDate: Date = new Date(this.getDateControlValue('PlanTo'));

        if (moment.duration(moment(newDate).diff(toDate)).asDays() > 0 || moment.duration(moment(newDate).diff(fromDate)).asDays() < 0) {
            this.setControlValue('PlanningDate', this.getControlValue('SearchDate'));
            return;
        } else {
            this.setControlValue('SearchDate', this.globalize.formatDateToLocaleFormat(newDate));
            this.clearCalendars();
            this.getEmployeeData();
        }
    }

    // @todo - Move To Common
    public onCalendarViewRender(_data: any, index: number): void {
        this.index = index;
        if (!this.pageParams.employeeDetailsList || !this.pageParams.employeeDetailsList.length) {
            return;
        }
        let diaryDetails: PlanningObjectCollection = this.pageParams.employeeDetailsList[this.index].DiaryDetails;
        let calendarId: string = 'fullCalendar' + this.index;
        if (diaryDetails && this[calendarId]) {
            this[calendarId].renderData(diaryDetails);
        }
    }

    // @todo - Move To Common
    public onClickVisitDetails(event: any, calendarIdx: number, visitIdx: number): void {
        event.preventDefault();

        this.index = calendarIdx;

        this.visitDetails = new PlanningObject();

        this.visitDetails = this.pageParams.employeeDetailsList[this.index].AvailableVisits[visitIdx];

        this.getVisitClickData(this.visitDetails.visitId);
    }

    // @todo - Move To Common
    public onExternalEventDrop(event: any, index: number): void {
        let visitDetails: PlanningObject,
            date: moment.Moment = event.dropDate,
            sequence: number = event.index,
            visitType: string = '';

        // Get event object
        visitDetails = this.pageParams.employeeDetailsList[this.index].AvailableVisits[sequence];
        visitType = visitDetails.visitId.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR)[0];

        visitDetails.visitStart = date.utc().format();
        visitDetails.visitEnd = date.utc().format();

        switch (visitType) {
            case 'AV':
                if (this.pageParams.PlanningDiaryOptions.FullDiaryUpdate) {
                    this.validateEventRecord(visitDetails,
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        index);
                }
                break;
            case 'P':
                if (this.pageParams.PlanningDiaryOptions.FullDiaryUpdate && !visitDetails.isVisitHardSlot) {
                    this.validateEventRecord(visitDetails,
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        index);
                }
                break;
            default:
                break;
        }
    }

    // @todo - Move To Common
    public onCalendarEventDrop(data: any, index: number): void {
        let event: any = data.calendarEvent,
            eventFullId: string = event.id,
            idParts: Array<string> = eventFullId.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR),
            eventType: string = idParts[0],
            visitDetails: PlanningObject = new PlanningObject();

        if (!eventType) {
            return;
        }

        visitDetails.visitId = eventFullId;
        visitDetails.visitTitle = event.title;
        visitDetails.visitStart = event.start.utc().format();
        visitDetails.visitEnd = event.end.utc().format();

        switch (eventType) {
            case 'BH':
            case 'NH':
            case 'TL':
            case 'DE':
            case 'PD':
                data.undo();
                break;
            case 'AV':
                if (this.pageParams.PlanningDiaryOptions.FullDiaryUpdate) {
                    this.validateEventRecord(
                        visitDetails,
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        index,
                        data.undo
                    );
                }
                break;
            case 'P':
                if (!event.hardslot && this.pageParams.PlanningDiaryOptions.FullDiaryUpdate) {
                    /**
                     * @todo - Google Map Functionality; validateEventRecord Will Be Called From Within Google Map Functionality
                     */
                    this.validateEventRecord(
                        visitDetails,
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.start.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        event.end.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        index,
                        data.undo
                    );
                } else {
                    data.undo();
                }
                break;
        }
    }

    // @todo - Move To Common
    public onCalendarEventClick(index: number): void {
        this.index = index;

        let fullId: string = this['fullCalendar' + this.index].eventFullId;

        this.displayCalendarEventData(fullId);
    }

    // @todo - Move To Common
    public onVisitDetailsCommandClick(data: ICommandEmitter): void {
        switch (data.command) {
            case VisitDetailsModalConstants.c_s_COMMAND_PLAN:
                this.planVisit(data.visit);
                break;
            case VisitDetailsModalConstants.c_s_COMMAND_VISITS:
                this.navigate('TechVisitDiary', InternalMaintenanceApplicationModuleRoutes.ICABSAPLANVISITMAINTENANCE, {
                    PlanVisitRowId: data.visit.visitId
                });
                break;
            case VisitDetailsModalConstants.c_s_COMMAND_SERVICECOVER:
                this.setAttribute('ServiceCoverRowID', data.visit.serviceCoverRowID);
                this.navigate('TechVisitDiary', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE);
                break;
            case VisitDetailsModalConstants.c_s_COMMAND_UNDO:
                this.undoPlanVisit(data.visit);
                break;
            case VisitDetailsModalConstants.c_s_COMMAND_REFRESH:
                this.onRefresh();
                break;
        }
    }

    public onRefresh(): void {
        this.clearCalendars();
        this.getEmployeeData();
    }

    // Set Dragged Set Index In Global Index For Further Processing
    public onVisitDrag(data: IDraggedSet): void {
        this.index = data.draggedSetIndex;
    }

    // On Calendar Event Drag
    public onEventDrag(data: any): void {
        this.index = data.index;
    }

    public onOtherCalendarEventDrop(data: any, index: number): void {
        let visitDetails: PlanningObject,
            date: moment.Moment = data.date || data,
            visitType: string = '';

        visitDetails = new PlanningObject();
        if (!data.event) {
            return;
        }
        visitDetails.visitId = data.event.id;
        visitDetails.visitTitle = data.event.title;
        visitType = visitDetails.visitId.split(TechnicianVisitDiaryConstants.c_s_ID_SEPARATOR)[0];

        switch (visitType) {
            case 'AV':
                if (this.pageParams.PlanningDiaryOptions.FullDiaryUpdate) {
                    this.validateEventRecord(visitDetails,
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        index);
                }
                break;
            case 'P':
                if (this.pageParams.PlanningDiaryOptions.FullDiaryUpdate && !visitDetails.isVisitHardSlot) {
                    this.validateEventRecord(visitDetails,
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_DATE_FORMAT),
                        date.format(TechnicianVisitDiaryConstants.c_s_CONNECTOR_TIME_FORMAT),
                        index);
                }
                break;
            default:
                break;
        }
    }

    public onPrintClick(): void {
        window.print();
    }
    //#endregion
}
