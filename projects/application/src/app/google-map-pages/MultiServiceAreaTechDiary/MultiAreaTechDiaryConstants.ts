export class MultiAreaTechDiaryConstants {
    public static readonly c_obj_CALENDAR_CONFIG: Record<string, any> = {
        header: false,
        views: {
            agenda: {
                columnFormat: 'dddd, MMM DD'
            }
        },
        eventOrder: 'start',
        aspectRatio: 2.1,
        editable: true,
        droppable: true,
        defaultView: 'agendaDay',
        height: 600
    };
    public static readonly c_s_DELIMITER_SERVICEAREA: string = ',';
}
