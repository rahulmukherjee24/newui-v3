import 'rxjs/add/operator/takeWhile';
import { Component, OnInit, Injector, ViewChild, AfterViewInit, OnDestroy, ElementRef } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARoutingSearch.html',
    styles: [`
    #map{
        height:800px;
    }
    `]
})

export class RoutingSearchComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private latlng: any;
    private isRecordUpdated: boolean = false;
    private headerParams: any = {
        method: 'service-planning/search',
        operation: 'Application/iCABSARoutingSearch',
        module: 'routing'
    };
    private markerTitle: string = '';
    private myLoadedOptions: string = '';
    private googleMapObject: any;
    private houseNumber: string = '';
    private lookUpSubscription: Subscription;
    private isAlive: boolean = true;

    public controls: any = [
        { name: 'Apply', type: MntConst.eTypeButton },
        { name: 'cbAddressLine1', type: MntConst.eTypeCheckBox },
        { name: 'cbAddressLine2', type: MntConst.eTypeCheckBox },
        { name: 'cbAddressLine3', type: MntConst.eTypeCheckBox },
        { name: 'cbAddressLine4', type: MntConst.eTypeCheckBox },
        { name: 'cbAddressLine5', type: MntConst.eTypeCheckBox },
        { name: 'cbCountry', type: MntConst.eTypeCheckBox },
        { name: 'cbGeonode', type: MntConst.eTypeCheckBox },
        { name: 'cbGPSX', type: MntConst.eTypeCheckBox },
        { name: 'cbPostcode', type: MntConst.eTypeCheckBox },
        { name: 'cmdAddress', type: MntConst.eTypeButton },
        { name: 'cmdFind', type: MntConst.eTypeButton },
        { name: 'strAddressLine1', type: MntConst.eTypeText },
        { name: 'strAddressLine2', type: MntConst.eTypeText },
        { name: 'strAddressLine3', type: MntConst.eTypeText },
        { name: 'strAddressLine4', type: MntConst.eTypeText },
        { name: 'strAddressLine5', type: MntConst.eTypeText },
        { name: 'strCountry', type: MntConst.eTypeText },
        { name: 'strGeonode', type: MntConst.eTypeText },
        { name: 'strGPSX', type: MntConst.eTypeText },
        { name: 'strGPSY', type: MntConst.eTypeText },
        { name: 'strPostcode', type: MntConst.eTypeCode },
        { name: 'strScore', type: MntConst.eTypeText },
        { name: 'strSelRoutingSource', type: MntConst.eTypeDropdown }
    ];
    public accountType: string = '';
    public adr1Content: string = '';
    public adr2Content: string = '';
    public adr3Content: string = '';
    public adr4Content: string = '';
    public adr5Content: string = '';
    public branchGPSX: string = '';
    public branchGPSY: string = '';
    public customerID: string = '';
    public gCountryCode: string = '';
    public geoAccountType: string = '';
    public geoCoder: any;
    public geocodingSource: string = '';
    public gMap: any;
    public googleAddresses: string = '';
    public googleError: string = '';
    public googleMapsUrl: string = '';
    public googleSignature: string = '';
    public infowindow: any = '';
    public initialLocation: string = '';
    public isChecked: any = {
        cbAddressLine1: false,
        cbAddressLine2: false,
        cbAddressLine3: false,
        cbAddressLine4: false,
        cbAddressLine5: false,
        cbCountry: false,
        cbPostcode: false,
        cbGPSX: true,
        cbGeonode: false
    };
    public isGeonode: boolean = false;
    public isGPSFind: boolean = false;
    public isLoadingError: boolean = false;
    public isPanoLoadMap: boolean = false;
    public isPosMap: boolean = true;
    public isRecordFound: boolean = false;
    public isUpdateGrid: boolean = false;
    public isUseGoogle: boolean = false;
    public isValidScore: boolean = false;
    public licenceKey: string = '';
    public map: any = '';
    public marker: any;
    public myLatlng: any = '';
    public myOptions: any;
    public newMarker: boolean = false;
    public numberLoc: string = '';
    public pageCurrent: number = 1;
    public pageSize: number = 5;
    public pageTitle: string = '';
    public pageId: string = '';
    public pano: any = null;
    public panoOpen: boolean = false;
    public panoX: any;
    public panoY: any;
    public parentModeFromURL: Object;
    public posX: string = '';
    public posY: string = '';
    public premAddress: string = '';
    public regValue: any;
    public routeSubscription: Subscription;
    public tmpX: string = '';
    public tmpY: string = '';
    public totalRecords: number = 0;
    public url: string = '';
    public useBranch: boolean = false;
    public vZoom: number = 8;
    public zoom: number = 8;

    constructor(injector: Injector, private _ls: LocalStorageService,
        private elem: ElementRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAROUTINGSEARCH;
        this.pageTitle = 'Address Search';
        this.browserTitle = 'Geocode Search';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode();
    }

    ngAfterViewInit(): void {
        /** Settimeout used to delay the lookup so as to sync with the updated CBB which takes some time to update */
        setTimeout(() => {
            this.doLookupBusinessRegistry();
        }, 1000);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.doCleanUp();
    }

    private doCleanUp(): void {
        this.serviceConstants = null;
        this.httpService = null;
        this.errorService = null;
        this.utils = null;
        this.ajaxSource = null;
        this.isAlive = false;
        this.routeSubscription = null;
    }

    private initialize(): void {
        let strGPSY: string = this.getControlValue('strGPSY');
        let strGPSX: string = this.getControlValue('strGPSX');
        try {
            this.infowindow = new this.googleMapObject.InfoWindow();
            this.geoCoder = new this.googleMapObject.Geocoder();
        }
        catch (error) {
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            this.isLoadingError = true;
            window.opener.getRoutingSearchScreenData();
            window.close();
        }
        if (!this.isLoadingError) {
            if (this.branchGPSY === '') {
                this.branchGPSY = '58.420000';
                this.vZoom = 15;
            }
            if (this.branchGPSX === '') {
                this.branchGPSX = '1.890000';
                this.vZoom = 15;
            }
            if (isNaN(parseFloat(strGPSY))) {
                strGPSY = this.branchGPSY;
                this.setControlValue('strGPSY', this.branchGPSY);
                this.useBranch = true;
            }
            if (isNaN(parseFloat(strGPSX))) {
                strGPSX = this.branchGPSX;
                this.setControlValue('strGPSX', this.branchGPSX);
                this.useBranch = true;
            }
            if (strGPSX !== '' && strGPSY !== '') {
                this.latlng = new this.googleMapObject.LatLng(parseFloat(strGPSX), parseFloat(strGPSY));
                this.myOptions = {
                    zoom: this.vZoom,
                    center: this.latlng,
                    mapTypeId: this.googleMapObject.MapTypeId.ROADMAP
                };
                this.map = new this.googleMapObject.Map(this.elem.nativeElement.querySelector('#map'), this.myOptions);
                if (this.useBranch === true) {
                    this.setControlValue('strGPSY', '');
                    this.setControlValue('strGPSX', '');
                }
                this.riGridBeforeExecute();
            }
        }
    }

    private geocodeAddress(address: any): void {
        if (this.marker !== undefined) {
            this.marker.setMap(null);
        }
        this.geoCoder.geocode({ 'address': address }, (results, status) => {
            if (status === this.googleMapObject.GeocoderStatus.OK) {
                this.map.setCenter(results[0].geometry.location);
                this.marker = new this.googleMapObject.Marker({
                    map: this.map,
                    title: 'Premise',
                    draggable: true,
                    position: results[0].geometry.location,
                    animation: this.googleMapObject.Animation.DROP
                });
                this.googleMapObject.event.clearListeners(this.marker, 'dragend');
                this.googleMapObject.event.addListener(this.marker, 'dragend', () => {
                    this.isRecordFound = false;
                    this.geocodePosition(this.marker.getPosition(), false);
                    this.map.setCenter(this.marker.getPosition());
                });
                this.createPano();
                this.buildGrid();
                this.riGrid.RefreshRequired();
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.GeoCodeErrorLabel + status));
            }
        });
    }

    private geocodeGPS(gpsX: number, gpsY: number): void {
        if (this.marker !== undefined) {
            this.marker.setMap(null);
        }
        this.myLatlng = new this.googleMapObject.LatLng(gpsY, gpsX);
        this.geoCoder.geocode({ 'latLng': this.myLatlng }, (results, status) => {
            if (status === this.googleMapObject.GeocoderStatus.OK) {
                this.map.setCenter(results[0].geometry.location);
                this.marker = new this.googleMapObject.Marker({
                    map: this.map,
                    title: 'Premise',
                    draggable: true,
                    position: results[0].geometry.location,
                    animation: this.googleMapObject.Animation.DROP
                });
                this.googleMapObject.event.clearListeners(this.marker, 'dragend');
                this.googleMapObject.event.addListener(this.marker, 'dragend', (e) => {
                    this.isRecordFound = false;
                    this.geocodePosition(this.marker.getPosition(), false);
                    this.map.setCenter(this.marker.getPosition());
                });
                this.createPano();
                this.buildGrid();
                this.riGrid.RefreshRequired();
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.GeoCodeErrorLabel + status));
            }
        });
    }

    private geocodeCurrentAddress(address: any): void {
        this.geoCoder.geocode({ 'address': address }, (results: any, status: any) => {
            if (status === this.googleMapObject.GeocoderStatus.OK) {
                if (this.marker !== undefined) {
                    this.marker.setMap(null);
                }
                this.extractAddress(results);
                this.map.setCenter(results[0].geometry.location);
                this.marker = new this.googleMapObject.Marker({
                    map: this.map,
                    title: 'Premise',
                    draggable: true,
                    position: results[0].geometry.location,
                    animation: this.googleMapObject.Animation.DROP
                });
                this.googleMapObject.event.clearListeners(this.marker, 'dragend');
                this.googleMapObject.event.addListener(this.marker, 'dragend', () => {
                    this.isRecordFound = false;
                    this.geocodePosition(this.marker.getPosition(), false);
                    this.map.setCenter(this.marker.getPosition());
                });
                this.createPano();
                this.riGridBeforeExecute();
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.GeoCodeErrorLabel + status));
            }
        });
    }

    private geocodeGPSAddress(gpsX: number, gpsY: number): void {
        this.myLatlng = new this.googleMapObject.LatLng(gpsY, gpsX);
        this.geoCoder.geocode({ 'latLng': this.myLatlng }, (results, status) => {
            if (status === this.googleMapObject.GeocoderStatus.OK) {
                if (this.marker) {
                    this.marker.setMap(null);
                }
                this.extractAddress(results);
                this.map.setCenter(results[0].geometry.location);
                this.marker = new this.googleMapObject.Marker({
                    map: this.map,
                    title: 'Premise',
                    draggable: true,
                    position: results[0].geometry.location,
                    animation: this.googleMapObject.Animation.DROP
                });
                this.googleMapObject.event.clearListeners(this.marker, 'dragend');
                this.googleMapObject.event.addListener(this.marker, 'dragend', () => {
                    this.isRecordFound = false;
                    this.geocodePosition(this.marker.getPosition(), false);
                    this.map.setCenter(this.marker.getPosition());
                });
                this.createPano();
                this.riGridBeforeExecute();
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.GeoCodeErrorLabel + status));
            }
        });
    }
    /** gets the geocode position */
    private geocodePosition(pos: string, ipNewMarker: boolean): void {
        this.newMarker = ipNewMarker;
        try {
            this.geoCoder.geocode({ 'latLng': pos }, (responses, status) => {
                this.getMapAddress(responses, status);
            });
        }
        catch (err) {
            this.modalAdvService.emitMessage(new ICabsModalVO(err));
        }
    }
    /** gets the Maps Address */
    private getMapAddress(responses: any, status: any): void {

        if (this.isRecordFound) {
            return;
        }
        if (status === this.googleMapObject.GeocoderStatus.OK) {
            this.extractAddress(responses);
        }
        else {
            if (status === this.googleMapObject.GeocoderStatus.ZERO_RESULTS) {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.GeoCodeErrorLabel + status));
            }
            else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.GeoCodeErrorLabel + status));
            }
        }
    }

    private setDecimalLen(Input: number): string {
        let strInput: string = '';
        let strInputArr: any;
        let strOutput: string = '';
        let strDecimal: any;
        strInput = Input.toString();
        strInputArr = strInput.split('.');
        strDecimal = strInputArr[1];
        strOutput = strInputArr[0] + '.' + strDecimal.substring(0, 7);
        return strOutput;
    }
    /** Extracts the address string */
    private extractAddress(responses: any): void {
        let houseNumber: string, state: string = '', street: string = '', postcode: string = '', town: string = '';
        let Political: string = '', tmpAddressString: string = '', tmpAdr1: string = '', tmpAdr2: string = '';
        let tmpAdr3: string = '', tmpAdr4: string = '', tmpAdr5: string = '', tmpPCode: string = '';
        let tmpX: string = '', tmpY: string = '', tmpCy: string = '', tmpScore = '50';
        let gpsString: string = '', addressString: string = '', addressSplit: any, tmpPCod: string = '', tmpCY: string = '';
        for (let x: number = 0; x < responses.length; x++) {
            gpsString = responses[x].geometry.location;
            addressString = responses[x].formatted_address;
            addressSplit = addressString.split(',');
            this.isRecordFound = true;
            if (x === 0) {
                this.setControlValue('strAddressLine1', addressSplit[x].trim());
                this.setControlValue('strGPSX', this.setDecimalLen(responses[x].geometry.location.lng()));
                this.setControlValue('strGPSY', this.setDecimalLen(responses[x].geometry.location.lat()));
                this.setControlValue('strSelRoutingSource', 'T');
                this.setControlValue('strAddressLine2', '');
                this.setControlValue('strAddressLine3', '');
                this.setControlValue('strAddressLine4', '');
                this.setControlValue('strAddressLine5', '');
                this.setControlValue('strGeonode', '0');
                this.setControlValue('strScore', '0');
                this.isRecordUpdated = true;
            }
            tmpAdr1 = '';
            tmpAdr2 = '';
            tmpAdr3 = '';
            tmpAdr4 = '';
            tmpAdr5 = '';
            tmpPCod = '';
            tmpCY = '';
            houseNumber = '';
            street = '';
            state = '';
            town = '';
            postcode = '';
            tmpX = this.setDecimalLen(responses[x].geometry.location.lng());
            tmpY = this.setDecimalLen(responses[x].geometry.location.lat());
            for (let i: number = 0; i < responses[x].address_components.length; i++) {
                for (let j: number = 0; j < responses[x].address_components[i].types.length; j++) {
                    if (responses[x].address_components[i].types[j] === 'postal_code')
                        postcode = responses[x].address_components[i].long_name;
                    if (responses[x].address_components[i].types[j] === 'street_number')
                        houseNumber = responses[x].address_components[i].long_name;
                    if (this.adr1Content !== '' && this.adr1Content !== null) {
                        if (responses[x].address_components[i].types[j] === this.adr1Content) {
                            street = responses[x].address_components[i].long_name;
                        }
                    }
                    else {
                        if (responses[x].address_components[i].types[j] === 'route')
                            street = responses[x].address_components[i].long_name;
                    }
                    if (this.adr4Content !== '' && this.adr4Content !== null) {
                        if (responses[x].address_components[i].types[j] === this.adr4Content) {
                            town = responses[x].address_components[i].long_name;
                        }
                    }
                    else {
                        if (responses[x].address_components[i].types[j] === 'locality')
                            town = responses[x].address_components[i].long_name;
                        if (responses[x].address_components[i].types[j] === 'sublocality' && town !== '' && town !== null)
                            town = responses[x].address_components[i].long_name;
                        if (responses[x].address_components[i].types[j] === 'administrative_area_level_3' && town !== '' && town !== null)
                            town = responses[x].address_components[i].long_name;
                    }
                    if (this.adr5Content !== '' && this.adr5Content !== null) {
                        if (responses[x].address_components[i].types[j] === this.adr5Content) {
                            state = responses[x].address_components[i].long_name;
                        }
                    }
                    else {
                        if (responses[x].address_components[i].types[j] === 'administrative_area_level_1')
                            state = responses[x].address_components[i].long_name;
                        if (responses[x].address_components[i].types[j] === 'administrative_area_level_2' && (state === '' || state === null))
                            state = responses[x].address_components[i].long_name;
                    }

                }
            }
            if (street) {
                if (this.numberLoc.toLowerCase() === 'end') {
                    tmpAdr1 = street + ' ' + houseNumber;
                }
                else {
                    tmpAdr1 = houseNumber + ' ' + street;
                }
            }
            if (town) {
                tmpAdr4 = town;
            }
            if (state) {
                tmpAdr5 = state;
            }
            if (postcode) {
                tmpPCode = postcode;
            }
            if (houseNumber) {
                tmpScore = '100';
            }
            else
                if (street) {
                    tmpScore = '80';
                }
                else
                    if (town) {

                        tmpScore = '60';
                    }
                    else
                        if (state) {
                            tmpScore = '40';
                        }
                        else {
                            tmpScore = '0';
                        }

            if (!tmpAddressString) {
                tmpAddressString = tmpAdr1 + '#' +
                    tmpAdr2 + '#' +
                    tmpAdr3 + '#' +
                    tmpAdr4 + '#' +
                    tmpAdr5 + '#' +
                    tmpPCode + '#' +
                    tmpX + '#' +
                    tmpY + '#' +
                    tmpCY + '#' +
                    tmpScore;
            }
            else {
                tmpAddressString = tmpAddressString + '|' +
                    tmpAdr1 + '#' +
                    tmpAdr2 + '#' +
                    tmpAdr3 + '#' +
                    tmpAdr4 + '#' +
                    tmpAdr5 + '#' +
                    tmpPCode + '#' +
                    tmpX + '#' +
                    tmpY + '#' +
                    tmpCY + '#' +
                    tmpScore;
            }
            if (x === 0) {
                if (tmpAdr1 !== '' && tmpAdr1 !== null) {
                    this.setControlValue('strAddressLine1', tmpAdr1.trim());
                }
                if (town !== '' && town !== null) {
                    this.setControlValue('strAddressLine4', town);
                }
                if (state !== '' && state !== null) {
                    this.setControlValue('strAddressLine5', state);
                }
                if (postcode !== '' && postcode !== null) {
                    this.setControlValue('strPostcode', postcode);
                }
                this.setControlValue('strScore', tmpScore);
                this.setControlValue('strSelRoutingSource', 'T');
                this.houseNumber = houseNumber;
            }
        }
        this.googleAddresses = tmpAddressString;
        if (this.isUpdateGrid !== true) {
            this.buildGrid();
            this.riGrid.RefreshRequired();
        }
        this.isUpdateGrid = false;
    }

    private createPano(): void {
        if (this.pano === null) {
            this.pano = this.map.getStreetView();
        }
        else {
            return;
        }
        this.googleMapObject.event.clearListeners(this.pano, 'visible_changed');
        this.googleMapObject.event.addListener(this.pano, 'visible_changed',
            () => {
                try {
                    if (this.panoOpen && !this.pano.getVisible()) {
                        this.panoOpen = false;
                        if (this.pano.getPosition()) {
                            this.panoX = this.pano.getPosition().lat();
                            this.panoY = this.pano.getPosition().lng();
                            this.myLatlng = new this.googleMapObject.LatLng(this.panoX, this.panoY);
                            this.map.setCenter(this.myLatlng);
                            let myOptions: any = {
                                zoom: this.vZoom,
                                center: this.myLatlng,
                                mapTypeId: this.googleMapObject.MapTypeId.ROADMAP
                            };
                            this.map = new this.googleMapObject.Map(document.getElementById('map'), myOptions);
                            this.geocodeGPS(this.panoX, this.panoY);
                        }
                        else {
                            this.myLatlng = new this.googleMapObject.LatLng(this.panoX, this.panoY);
                            this.map.setCenter(this.myLatlng);
                            return;
                        }
                        this.myLatlng = new this.googleMapObject.LatLng(this.panoX, this.panoY);
                        this.myLoadedOptions = '';
                        this.isPosMap = false;
                        this.setControlValue('strGPSY', this.panoX);
                        this.setControlValue('strGPSX', this.panoY);
                        this.isPanoLoadMap = true;
                        this.isRecordFound = false;
                        this.myLoadedOptions = '';
                        this.isUpdateGrid = false;
                        try {
                            this.geocodePosition(this.pano.getPosition(), true);
                        }
                        catch (err) {
                            this.modalAdvService.emitMessage(new ICabsModalVO(err));
                        }
                        this.pano = null;
                    }
                    if (this.panoOpen) {
                        this.panoOpen = true;
                    }
                    else {
                        this.panoOpen = false;
                    }
                }
                catch (err) {
                    this.panoOpen = false;
                }
            });
    }

    private windowOnload(): void {
        if (this.geocodingSource === 'Google') {
            this.isUseGoogle = true;
        }
        this.routeSubscription = this.activatedRoute.queryParams.subscribe((data) => {
            this.parentModeFromURL = data;
            this.parentMode = data['parentMode'];
            switch (this.parentMode) {
                case 'Premise':
                    if (data.hasOwnProperty('PremiseAddressLine1') && data['PremiseAddressLine1'] !== 'null') {
                        this.setControlValue('strAddressLine1', data['PremiseAddressLine1']);
                    }
                    if (data.hasOwnProperty('PremiseAddressLine2') && data['PremiseAddressLine2'] !== 'null') {
                        this.setControlValue('strAddressLine2', data['PremiseAddressLine2']);
                    }
                    if (data.hasOwnProperty('PremiseAddressLine3') && data['PremiseAddressLine3'] !== 'null') {
                        this.setControlValue('strAddressLine3', data['PremiseAddressLine3']);
                    }
                    if (data.hasOwnProperty('PremiseAddressLine4') && data['PremiseAddressLine4'] !== 'null') {
                        this.setControlValue('strAddressLine4', data['PremiseAddressLine4']);
                    }
                    if (data.hasOwnProperty('PremiseAddressLine5') && data['PremiseAddressLine5'] !== 'null') {
                        this.setControlValue('strAddressLine5', data['PremiseAddressLine5']);
                    }
                    if (data.hasOwnProperty('PremisePostcode') && data['PremisePostcode'] !== 'null') {
                        this.setControlValue('strPostcode', data['PremisePostcode']);
                    }
                    if (data.hasOwnProperty('GPSCoordinateX') && data['GPSCoordinateX'] !== '') {
                        this.setControlValue('strGPSX', data['GPSCoordinateX']);
                    } else {
                        this.setControlValue('strGPSX', '0');
                    }
                    if (data.hasOwnProperty('GPSCoordinateY') && data['GPSCoordinateY'] !== '') {
                        this.setControlValue('strGPSY', data['GPSCoordinateY']);
                    } else {
                        this.setControlValue('strGPSY', '0');
                    }
                    if (data.hasOwnProperty('RoutingGeonode') && data['RoutingGeonode'] !== 'null') {
                        this.setControlValue('strGeonode', data['RoutingGeonode']);
                    }
                    if (data.hasOwnProperty('RoutingScore') && data['RoutingScore'] !== 'null') {
                        this.setControlValue('strScore', data['RoutingScore']);
                    }
                    if (data.hasOwnProperty('SelRoutingSource') && data['SelRoutingSource'] !== 'null') {
                        this.setControlValue('strSelRoutingSource', data['SelRoutingSource']);
                    }
                    break;
                case 'Employee':
                    if (data.hasOwnProperty('EmployeeAddressLine1') && data['EmployeeAddressLine1'] !== 'null') {
                        this.setControlValue('strAddressLine1', data['EmployeeAddressLine1']);
                    }
                    if (data.hasOwnProperty('EmployeeAddressLine2') && data['EmployeeAddressLine2'] !== 'null') {
                        this.setControlValue('strAddressLine2', data['EmployeeAddressLine2']);
                    }
                    if (data.hasOwnProperty('EmployeeAddressLine3') && data['EmployeeAddressLine3'] !== 'null') {
                        this.setControlValue('strAddressLine3', data['EmployeeAddressLine3']);
                    }
                    if (data.hasOwnProperty('EmployeeAddressLine4') && data['EmployeeAddressLine4'] !== 'null') {
                        this.setControlValue('strAddressLine4', data['EmployeeAddressLine4']);
                    }
                    if (data.hasOwnProperty('EmployeeAddressLine5') && data['EmployeeAddressLine5'] !== 'null') {
                        this.setControlValue('strAddressLine5', data['EmployeeAddressLine5']);
                    }
                    if (data.hasOwnProperty('EmployeePostcode') && data['EmployeePostcode'] !== 'null') {
                        this.setControlValue('strPostcode', data['EmployeePostcode']);
                    }
                    if (data.hasOwnProperty('HomeGPSCoordinateX') && data['HomeGPSCoordinateX'] !== 'null') {
                        this.setControlValue('strGPSX', data['HomeGPSCoordinateX']);
                    } else {
                        this.setControlValue('strGPSX', '0');
                    }
                    if (data.hasOwnProperty('HomeGPSCoordinateY') && data['HomeGPSCoordinateY'] !== 'null') {
                        this.setControlValue('strGPSY', data['HomeGPSCoordinateY']);
                    } else {
                        this.setControlValue('strGPSY', '0');
                    }
                    if (data.hasOwnProperty('RoutingGeonode') && data['RoutingGeonode'] !== 'null') {
                        this.setControlValue('strGeonode', data['RoutingGeonode']);
                    }
                    if (data.hasOwnProperty('RoutingScore') && data['RoutingScore'] !== 'null') {
                        this.setControlValue('strScore', data['RoutingScore']);
                    }
                    if (data.hasOwnProperty('RoutingSource') && data['RoutingSource'] !== 'null') {
                        this.setControlValue('strSelRoutingSource', data['RoutingSource']);
                    }
                    break;
                case 'Branch':
                    if (data.hasOwnProperty('BranchAddressLine1') && data['BranchAddressLine1'] !== 'null') {
                        this.setControlValue('strAddressLine1', data['BranchAddressLine1']);
                    }
                    if (data.hasOwnProperty('BranchAddressLine2') && data['BranchAddressLine2'] !== 'null') {
                        this.setControlValue('strAddressLine2', data['BranchAddressLine2']);
                    }
                    if (data.hasOwnProperty('BranchAddressLine3') && data['BranchAddressLine3'] !== 'null') {
                        this.setControlValue('strAddressLine3', data['BranchAddressLine3']);
                    }
                    if (data.hasOwnProperty('BranchAddressLine4') && data['BranchAddressLine4'] !== 'null') {
                        this.setControlValue('strAddressLine4', data['BranchAddressLine4']);
                    }
                    if (data.hasOwnProperty('BranchAddressLine5') && data['BranchAddressLine5'] !== 'null') {
                        this.setControlValue('strAddressLine5', data['BranchAddressLine5']);
                    }
                    if (data.hasOwnProperty('BranchPostcode') && data['BranchPostcode'] !== 'null') {
                        this.setControlValue('strPostcode', data['BranchPostcode']);
                    }
                    if (data.hasOwnProperty('GPSCoordinateX') && data['GPSCoordinateX'] !== 'null') {
                        this.setControlValue('strGPSX', data['GPSCoordinateX']);
                    } else {
                        this.setControlValue('strGPSX', '0');
                    }
                    if (data.hasOwnProperty('GPSCoordinateY') && data['GPSCoordinateY'] !== 'null') {
                        this.setControlValue('strGPSY', data['GPSCoordinateY']);
                    } else {
                        this.setControlValue('strGPSY', '0');
                    }
                    if (data.hasOwnProperty('RoutingGeonode') && data['RoutingGeonode'] !== 'null') {
                        this.setControlValue('strGeonode', data['RoutingGeonode']);
                    }
                    if (data.hasOwnProperty('RoutingScore') && data['RoutingScore'] !== 'null') {
                        this.setControlValue('strScore', data['RoutingScore']);
                    }
                    if (data.hasOwnProperty('RoutingSource') && data['RoutingSource'] !== 'null') {
                        this.setControlValue('strSelRoutingSource', data['RoutingSource']);
                    }
                    break;
            }
            this.setControlValue('strCountry', data['CountryCode']);
        });
        this.riExchange.riInputElement.Disable(this.uiForm, 'strSelRoutingSource');
        let strGPSX = this.getControlValue('strGPSX');
        let strGPSY = this.getControlValue('strGPSY');
        if (isNaN(parseInt(strGPSX, 10))) {
            this.setControlValue('strGPSX', '');
        }
        if (isNaN(parseInt(strGPSY, 10))) {
            this.setControlValue('strGPSY', '');
        }
        this.zoom = 8;
        this.setControlValue('cbGeonode', true);
        this.setControlValue('cbGPSX', true);
        //this.getBranchGPS();
        this.getHouseNumberPosition();
        this.buildGrid();
        this.riGrid.RefreshRequired();
        this.initialize();
        if (strGPSX !== '0' && strGPSY !== '0' && strGPSY !== '' && strGPSY !== null && !isNaN(parseInt(strGPSY, 10)) && (strGPSX !== '' && strGPSX !== null && !isNaN(parseInt(strGPSX, 10)))) {
            this.geocodeGPS(parseFloat(strGPSX), parseFloat(strGPSY));
        }
        else {
            this.premAddress = this.getControlValue('strAddressLine1') + ' ' +
                this.getControlValue('strAddressLine2') + ' ' +
                this.getControlValue('strAddressLine3') + ' ' +
                this.getControlValue('strAddressLine4') + ' ' +
                this.getControlValue('strAddressLine5') + ' ' +
                this.getControlValue('strPostcode');
            this.premAddress = this.premAddress.replace(/'/g, '');
            this.geocodeAddress(this.premAddress);
        }
        this.isRecordFound = true;
    }

    private buildGrid(): void {
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = this.pageSize;
        this.riGrid.Clear();
        this.riGrid.AddColumn('Score', 'Routing', 'Score', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumn('houseNumber', 'Routing', 'houseNumber', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('AddressLine1', 'Routing', 'AddressLine1', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('AddressLine2', 'Routing', 'AddressLine2', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('AddressLine3', 'Routing', 'AddressLine3', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('AddressLine4', 'Routing', 'AddressLine4', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('AddressLine5', 'Routing', 'AddressLine5', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('postcode', 'Routing', 'postcode', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('Country', 'Routing', 'Country', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('GPSX', 'Routing', 'GPSX', MntConst.eTypeText, 13);
        this.riGrid.AddColumn('GPSY', 'Routing', 'GPSY', MntConst.eTypeText, 13);
        if (!this.isUseGoogle) {
            this.riGrid.AddColumn('Node', 'Routing', 'Node', MntConst.eTypeTextFree, 10);
            this.riGrid.AddColumnAlign('Node', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumnAlign('Score', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('AddressLine1', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('AddressLine2', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('AddressLine3', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('AddressLine4', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('AddressLine5', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('postcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnAlign('GPSX', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('GPSY', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): void {
        if (this.isGPSFind) {
            this.getQueryGPS();
        }
        else {
            this.getAddress();
        }
    }

    private setRoutingSource(): void {
        let decGPSX: number = 0, decGPSY: number = 0, intRoutingScore: number = 0;
        let strGPSX: string = this.getControlValue('strGPSX');
        let strGPSY: string = this.getControlValue('strGPSY');
        let strScore: string = this.getControlValue('strScore');
        if (isNaN(parseInt(strGPSX, 10))) {
            decGPSX = 0;
        }
        else {
            decGPSX = parseInt(strGPSX, 10);
        }
        if (isNaN(parseInt(strGPSY, 10))) {
            decGPSY = 0;
        }
        else {
            decGPSY = parseInt(strGPSY, 10);
        }
        if (isNaN(parseInt(strScore, 10))) {
            intRoutingScore = 0;
        }
        else {
            intRoutingScore = parseInt(strScore, 10);
        }
        if (decGPSX === 0 || decGPSY === 0 || intRoutingScore === 0) {
            this.setControlValue('strSelRoutingSource', '');
        } else {
            this.setControlValue('strSelRoutingSource', 'M');
        }
    }

    private checkstrScore(): void {
        this.isValidScore = true;
        let strScore: string = this.getControlValue('strScore');
        if (strScore) {
            if (parseInt(strScore, 10) > 100) {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.ScoreLessThanHundred));
                this.isValidScore = false;
            }
        }
    }

    private getQueryGPS(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        let bodyParams: any = {};
        bodyParams['Function'] = 'QueryGPS';
        bodyParams['GPSX'] = this.getControlValue('strGPSX');
        bodyParams['GPSY'] = this.getControlValue('strGPSY');
        bodyParams['addressString'] = this.googleAddresses;
        bodyParams[this.serviceConstants.GridMode] = '0';
        bodyParams[this.serviceConstants.GridHandle] = '131556';
        bodyParams[this.serviceConstants.GridPageSize] = this.pageSize.toString();
        bodyParams[this.serviceConstants.GridPageCurrent] = this.pageCurrent.toString();
        bodyParams[this.serviceConstants.GridHeaderClickedColumn] = '';
        bodyParams[this.serviceConstants.GridSortOrder] = 'Descending';
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 5 : 1;
                this.riGrid.Execute(data);
                this.elem.nativeElement.querySelector('form').click();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private doLookupBusinessRegistry(): void {
        let lookupIP: any = [
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Geocoding',
                    'Regkey': 'AddressLine1'
                },
                'fields': ['RegValue']
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Geocoding',
                    'Regkey': 'AddressLine2'
                },
                'fields': ['RegValue']
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Geocoding',
                    'Regkey': 'AddressLine3'
                },
                'fields': ['RegValue']
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Geocoding',
                    'Regkey': 'AddressLine4'
                },
                'fields': ['RegValue']
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Geocoding',
                    'Regkey': 'AddressLine5'
                },
                'fields': ['RegValue']
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Geocoding',
                    'Regkey': 'GeocodingSource'
                },
                'fields': ['RegValue']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data[0] && data[0][0]) {
                this.adr1Content = data[0][0]['RegValue'];
            }
            if (data[1] && data[1][0]) {
                this.adr2Content = data[1][0]['RegValue'];
            }
            if (data[2] && data[2][0]) {
                this.adr3Content = data[2][0]['RegValue'];
            }
            if (data[3] && data[3][0]) {
                this.adr4Content = data[3][0]['RegValue'];
            }
            if (data[4] && data[4][0]) {
                this.adr5Content = data[4][0]['RegValue'];
            }
            if (data[5] && data[5][0]) {
                this.geocodingSource = data[5][0]['RegValue'];
            }
            this.googleMapObject = window['google'].maps;
            this.windowOnload();
        });
    }

    public riGridBodyColumnFocus(): void {
        this.setControlValue('strHouseNumber', this.riGrid.Details.GetValue('houseNumber'));
        this.setControlValue('strAddressLine1', this.riGrid.Details.GetValue('AddressLine1'));
        this.setControlValue('strAddressLine2', this.riGrid.Details.GetValue('AddressLine2'));
        this.setControlValue('strAddressLine3', this.riGrid.Details.GetValue('AddressLine3'));
        this.setControlValue('strAddressLine4', this.riGrid.Details.GetValue('AddressLine4'));
        this.setControlValue('strAddressLine5', this.riGrid.Details.GetValue('AddressLine5'));
        this.setControlValue('strPostcode', this.riGrid.Details.GetValue('postcode'));
        this.setControlValue('strCountry', this.riGrid.Details.GetValue('Country'));
        this.setControlValue('strGPSX', this.riGrid.Details.GetValue('GPSX'));
        this.setControlValue('strGPSY', this.riGrid.Details.GetValue('GPSY'));
        this.setControlValue('strGeonode', this.riGrid.Details.GetValue('Node'));
        this.isRecordFound = false;
        this.setControlValue('strScore', this.riGrid.Details.GetValue('Score'));
        this.setControlValue('strSelRoutingSource', 'T');
    }

    public cmdFindOnclick(): void {
        this.isGPSFind = true;
        this.isRecordFound = false;
        let strGPSY: string = this.getControlValue('strGPSY');
        let strGPSX: string = this.getControlValue('strGPSX');
        if (strGPSY !== '' && strGPSY !== null && strGPSX !== '' && strGPSX !== null) {
            this.geocodeGPSAddress(parseFloat(strGPSX), parseFloat(strGPSY));
        }
        this.isGPSFind = false;
    }

    public cmdAddressOnclick(): void {
        this.isGPSFind = true;
        this.isRecordFound = false;
        let premAddress: string = this.getControlValue('strAddressLine1') + ' ' +
            this.getControlValue('strAddressLine2') + ' ' +
            this.getControlValue('strAddressLine3') + ' ' +
            this.getControlValue('strAddressLine4') + ' ' +
            this.getControlValue('strAddressLine5') + ' ' +
            this.getControlValue('strPostcode');
        premAddress = premAddress.replace(/'/g, '');
        this.geocodeCurrentAddress(premAddress);
        this.isGPSFind = false;
    }

    public strGPSXOnchange(): void {
        this.setRoutingSource();
    }

    public strGPSYOnchange(): void {
        this.setRoutingSource();
    }

    public strScoreOnchange(): void {
        this.checkstrScore();
        if (this.isValidScore) {
            this.setRoutingSource();
        }
    }

    public onCheckboxClicked(event: any, controlName: any): void {
        if (event) {
            switch (controlName) {
                case 'addressline1':
                    this.isChecked.cbAddressLine1 = event.target.checked;
                    break;
                case 'addressline2':
                    this.isChecked.cbAddressLine2 = event.target.checked;
                    break;
                case 'addressline3':
                    this.isChecked.cbAddressLine3 = event.target.checked;
                    break;
                case 'addressline4':
                    this.isChecked.cbAddressLine3 = event.target.checked;
                    break;
                case 'addressline5':
                    this.isChecked.cbAddressLine5 = event.target.checked;
                    break;
                case 'postcode':
                    this.isChecked.cbPostcode = event.target.checked;
                    break;
                case 'country':
                    this.isChecked.cbCountry = event.target.checked;
                    break;
                case 'GPS':
                    this.isChecked.cbGPSX = event.target.checked;
                    break;
                case 'geonode':
                    this.isChecked.cbGeonode = event.target.checked;
                    break;
            }
        }
    }

    public cmdApplyOnClick(): void {
        this.checkstrScore();
        if (this.isValidScore) {
            let storedDataObj: any = { applyClicked: 'true' };
            if (this.parentMode === 'Premise') {
                if (this.isChecked.cbAddressLine1) {
                    storedDataObj.PremiseAddressLine1 = this.getControlValue('strAddressLine1');
                }
                if (this.isChecked.cbAddressLine2) {
                    storedDataObj.PremiseAddressLine2 = this.getControlValue('strAddressLine2');
                }
                if (this.isChecked.cbAddressLine3) {
                    storedDataObj.PremiseAddressLine3 = this.getControlValue('strAddressLine3');
                }
                if (this.isChecked.cbAddressLine4) {
                    storedDataObj.PremiseAddressLine4 = this.getControlValue('strAddressLine4');
                }
                if (this.isChecked.cbAddressLine5) {
                    storedDataObj.PremiseAddressLine5 = this.getControlValue('strAddressLine5');
                }
                if (this.isChecked.cbPostcode) {
                    storedDataObj.PremisePostcode = this.getControlValue('strPostcode');
                }
                if (this.isChecked.cbCountry) {
                    storedDataObj.CountryCode = this.getControlValue('strCountry');
                }
                if (this.isChecked.cbGPSX) {
                    storedDataObj.GPScoordinateX = this.getControlValue('strGPSX');
                    storedDataObj.GPScoordinateY = this.getControlValue('strGPSY');
                    storedDataObj.RoutingScore = this.getControlValue('strScore');
                    storedDataObj.RoutingSource = this.getControlValue('strSelRoutingSource');
                }
                if (this.isChecked.cbGeonode) {
                    storedDataObj.RoutingGeonode = this.getControlValue('strGeonode');
                }
                this._ls.store('PremiseRoutingSearch', JSON.stringify(storedDataObj));
            }
            else if (this.parentMode === 'Employee') {
                if (this.getControlValue('cbAddressLine1')) {
                    storedDataObj.EmployeeAddressLine1 = this.getControlValue('strAddressLine1');
                }
                if (this.getControlValue('cbAddressLine2')) {
                    storedDataObj.EmployeeAddressLine2 = this.getControlValue('strAddressLine2');
                }
                if (this.getControlValue('cbAddressLine3')) {
                    storedDataObj.EmployeeAddressLine3 = this.getControlValue('strAddressLine3');
                }
                if (this.getControlValue('cbAddressLine4')) {
                    storedDataObj.EmployeeAddressLine4 = this.getControlValue('strAddressLine4');
                }
                if (this.getControlValue('cbAddressLine5')) {
                    storedDataObj.EmployeeAddressLine5 = this.getControlValue('strAddressLine5');
                }
                if (this.getControlValue('cbPostcode')) {
                    storedDataObj.EmployeePostcode = this.getControlValue('strPostcode');
                }
                if (this.getControlValue('cbGPSX')) {
                    storedDataObj.HomeGPSCoordinateX = this.getControlValue('strGPSX');
                    storedDataObj.HomeGPSCoordinateY = this.getControlValue('strGPSY');
                    storedDataObj.RoutingScore = this.getControlValue('strScore');
                    storedDataObj.RoutingSource = this.getControlValue('strSelRoutingSource');
                }
                if (this.getControlValue('cbGeonode')) {
                    storedDataObj.RoutingGeonode = this.getControlValue('strGeonode');
                }
                this._ls.store('EmployeeRoutingSearch', JSON.stringify(storedDataObj));
            }
            else if (this.parentMode === 'Branch') {
                if (this.getControlValue('cbAddressLine1')) {
                    storedDataObj.BranchAddressLine1 = this.getControlValue('strAddressLine1');
                }
                if (this.getControlValue('cbAddressLine2')) {
                    storedDataObj.BranchAddressLine2 = this.getControlValue('strAddressLine2');
                }
                if (this.getControlValue('cbAddressLine3')) {
                    storedDataObj.BranchAddressLine3 = this.getControlValue('strAddressLine3');
                }
                if (this.getControlValue('cbAddressLine4')) {
                    storedDataObj.BranchAddressLine4 = this.getControlValue('strAddressLine4');
                }
                if (this.getControlValue('cbAddressLine5')) {
                    storedDataObj.BranchAddressLine5 = this.getControlValue('strAddressLine5');
                }
                if (this.getControlValue('cbPostcode')) {
                    storedDataObj.BranchPostcode = this.getControlValue('strPostcode');
                }
                if (this.getControlValue('cbGPSX')) {
                    storedDataObj.GPScoordinateX = this.getControlValue('strGPSX');
                    storedDataObj.GPScoordinateY = this.getControlValue('strGPSY');
                    storedDataObj.RoutingScore = this.getControlValue('strScore');
                    storedDataObj.RoutingSource = this.getControlValue('strSelRoutingSource');
                }
                if (this.getControlValue('cbGeonode')) {
                    storedDataObj.RoutingGeonode = this.getControlValue('strGeonode');
                }
                this._ls.store('BranchRoutingSearch', JSON.stringify(storedDataObj));
            }
            window.close();

        }

    }

    public getBranchGPS(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        searchParams.set(this.serviceConstants.Action, '6');
        let bodyParams: any = {};
        bodyParams['Function'] = 'GetBranchGPS';
        bodyParams['BranchNumber'] = this.utils.getBranchCode();
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.branchGPSX = data['BranchGPSX'];
                this.branchGPSY = data['BranchGPSY'];
                let strGPSX: string = this.getControlValue('strGPSX');
                let strGPSY: string = this.getControlValue('strGPSY');
                if (isNaN(parseFloat(strGPSX)) || isNaN(parseFloat(strGPSY)) || (strGPSY === '0' && strGPSY === '0')) {
                    this.setControlValue('strGPSX', this.branchGPSX);
                    this.setControlValue('strGPSY', this.branchGPSY);
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    public getHouseNumberPosition(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        searchParams.set(this.serviceConstants.Action, '6');
        let bodyParams: any = {};
        bodyParams['Function'] = 'GetHouseNumberPosition';
        bodyParams['RegSection'] = 'Ortec';
        bodyParams['RegKey'] = 'HouseNumberPosition';
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.numberLoc = data.RegValue;
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    public getAddress(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        searchParams.set(this.serviceConstants.Action, '2');
        let bodyParams: any = {};
        bodyParams['Function'] = 'QueryAddress';
        bodyParams['AddressLine1'] = this.getControlValue('strAddressLine1');
        bodyParams['AddressLine2'] = this.getControlValue('strAddressLine2');
        bodyParams['AddressLine3'] = this.getControlValue('strAddressLine3');
        bodyParams['AddressLine4'] = this.getControlValue('strAddressLine4');
        bodyParams['AddressLine5'] = this.getControlValue('strAddressLine5');
        bodyParams['postcode'] = this.getControlValue('strPostcode');
        bodyParams['CountryCode'] = '';
        bodyParams['HouseNumber'] = this.houseNumber;
        bodyParams['GPSX'] = this.getControlValue('strGPSX');
        bodyParams['GPSY'] = this.getControlValue('strGPSY');
        bodyParams['AddressString'] = this.googleAddresses;
        bodyParams['riGridMode'] = '0';
        bodyParams['riGridHandle'] = '131556';
        bodyParams['PageSize'] = this.pageSize.toString();
        bodyParams['PageCurrent'] = this.pageCurrent.toString();
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 5 : 1;
                this.riGrid.Execute(data);
                this.elem.nativeElement.querySelector('form').click();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    public getCurrentPage(event: any): void {
        this.pageCurrent = event.value;
        this.refresh();
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
}
