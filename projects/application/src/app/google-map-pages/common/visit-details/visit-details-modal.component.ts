import { Component, ViewChild, Output, EventEmitter } from '@angular/core';

import { NGXLogger } from 'ngx-logger';

import { ModalAdvService } from '@shared/components/modal-adv/modal-adv.service';
import { ModalComponent } from '@shared/components/modal/modal';
import { MessageConstant } from '@shared/constants/message.constant';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';

import { VisitDetailsModalConstants } from './VisitDetailsModalConstants';
import { PlanningObject } from './../../TechnicianVisitDiary/PlanningObject';

export interface ICommandEmitter {
    command: string;
    visit: PlanningObject;
}

@Component({
    selector: 'icabs-diary-visit-details',
    templateUrl: 'visit-details-modal.html'
})

export class VisitDetailsModalComponent {
    @ViewChild('modal') public modal: ModalComponent;

    @Output() onCommand: EventEmitter<ICommandEmitter>;

    public visitDetails: PlanningObject;

    // Getters; Mainly For HTML, However Used In The  Command Click
    public get commandPlan(): string {
        return VisitDetailsModalConstants.c_s_COMMAND_PLAN;
    }
    public get commandVisits(): string {
        return VisitDetailsModalConstants.c_s_COMMAND_VISITS;
    }
    public get commandServiceCovers(): string {
        return VisitDetailsModalConstants.c_s_COMMAND_SERVICECOVER;
    }
    public get commandUndo(): string {
        return VisitDetailsModalConstants.c_s_COMMAND_UNDO;
    }
    public get commandRefresh(): string {
        return VisitDetailsModalConstants.c_s_COMMAND_REFRESH;
    }

    constructor(private modalAdvService: ModalAdvService,
        private logger: NGXLogger) {
        this.visitDetails = new PlanningObject();

        this.onCommand = new EventEmitter();
    }

    private planConfirm(): void {
        try {
            this.onCommand.emit({
                command: this.commandPlan,
                visit: this.visitDetails
            });
            setTimeout(() => {
                this.modal.hide();
            }, 500);
        } catch (_excp) {
            this.logger.log(_excp);
        }
    }

    private planRevert(): void {
        try {
            this.onCommand.emit({
                command: this.commandUndo,
                visit: this.visitDetails
            });
            setTimeout(() => {
                this.modal.hide();
            }, 500);
        } catch (_excp) {
            this.logger.log(_excp);
        }
    }

    public show(data: Object): void {
        this.visitDetails = data['visitDetails'] || new PlanningObject();

        this.modal.show({}, false);
    }

    public onCommandClick(command: string): void {
        switch (command) {
            case this.commandPlan:
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.plan, null, this.planConfirm.bind(this)));
                break;
            case this.commandUndo:
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.technicianVisitDiaryGrid.undoPlan, null, this.planRevert.bind(this)));
                break;
            default:
                this.modal.hide();
                this.onCommand.emit({
                    command: command,
                    visit: this.visitDetails
                });
                break;
        }
    }
}
