import { Component, ViewChild, Output, EventEmitter } from '@angular/core';

import { ModalComponent } from '../../../../shared/components/modal/modal';
import { TechnicianVisitDiaryConstants } from './../../TechnicianVisitDiary/TechnicianVisitDiaryConstants';

@Component({
    selector: 'icabs-diary-sort-options',
    templateUrl: 'sort-options-modal.html'
})

export class SortOptionsModalComponent {
    @ViewChild('modal') public modal: ModalComponent;

    @Output() onSortSelect: EventEmitter<string>;

    public availableVisitFilters: string = '';

    constructor() {
        this.onSortSelect = new EventEmitter();
    }

    public get sortOptions(): any {
        return TechnicianVisitDiaryConstants.c_arr_SORT_OPTIONS;
    }

    public show(): void {
        this.modal.show({}, false);
    }

    public onSortOkClick(): void {
        this.modal.hide();
        this.onSortSelect.emit(this.availableVisitFilters);
    }
}
