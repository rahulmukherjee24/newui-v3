import { Component, ViewChild } from '@angular/core';

import { ModalComponent } from '../../../../shared/components/modal/modal';
import { TechnicianVisitDiaryConstants } from './../../TechnicianVisitDiary/TechnicianVisitDiaryConstants';

@Component({
    selector: 'icabs-diary-legend',
    templateUrl: 'legend-modal.html'
})

export class LegendModalComponent {
    @ViewChild('modal') public modal: ModalComponent;

    constructor() {
    }

    public get legendDetails(): any {
        return TechnicianVisitDiaryConstants.c_arr_DIARY_COLOUR;
    }

    public show(): void {
        this.modal.show({}, false);
    }
}
