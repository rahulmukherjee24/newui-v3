import { Component, OnInit, Injector, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { QueryParams } from './../../../../shared/services/http-params-wrapper';


import { PageIdentifier } from './../../../base/PageIdentifier';
import { BaseComponent } from '../../../base/BaseComponent';
import { SalesAreaSearchComponent } from './../../../internal/search/iCABSBSalesAreaSearch.component';
import { ContractSearchComponent } from './../../../internal/search/iCABSAContractSearch';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';

@Component({
    templateUrl: 'iCABSBSalesAreaPostcodeRezoneGrid.html'
})

export class SalesAreaPostcodeRezoneGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    // View Child
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('pagination') pagination: PaginationComponent;

    // Private Properties
    public contractTypeCodeOptions: any = [
        { text: 'All', value: 'All' }
    ];
    public portfolioStatusOptions: any = [
        { value: 'All', text: 'All' },
        { value: 'Current', text: 'Current Portfolio' },
        { value: 'NonCurrent', text: 'Non Current Portfolio' }
    ];
    private headerParams: any = {
        method: 'prospect-to-contract/maintenance',
        module: 'sales',
        operation: 'Business/iCABSBSalesAreaPostcodeRezoneGrid'
    };
    private messageProperties: ICabsModalVO = new ICabsModalVO();

    // Public Properties
    public pageId: string = '';
    public controls = [
        { name: 'SalesAreaCode', type: MntConst.eTypeCode, required: true },
        { name: 'SalesAreaDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'SalesAreaCodeTo', type: MntConst.eTypeCode, required: true },
        { name: 'SalesAreaDescTo', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'PortfolioStatusType' },
        { name: 'ContractTypeCode' },
        { name: 'PostcodeFilter' },
        { name: 'Town' },
        { name: 'County' }
    ];
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsisParams: any = {
        salesAreaSearch: {
            childConfig: {
                parentMode: 'LookUp'
            },
            component: SalesAreaSearchComponent
        },
        salesAreaToSearch: {
            childConfig: {
                parentMode: 'LookUp-To'
            },
            component: SalesAreaSearchComponent
        },
        contractSearch: {
            childConfig: {
                parentMode: 'LookUp'
            },
            component: ContractSearchComponent
        }
    };
    public gridParams: any = {
        currentPage: 1,
        totalRecords: 0,
        cacheRefresh: true
    };
    public batchInformation: string = '';

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBSALESAREAPOSTCODEREZONEGRID;
        this.pageTitle = this.browserTitle = 'Sales Area Portfolio Rezoning';
    }

    // Lifecycle Hooks
    public ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterViewInit(): void {
        this.setControlValue('PortfolioStatusType', 'All');

        // Build Contract Type Options
        this.buildContractTypeOptions();

        this.pageParams['isRezoneDisabled'] = true;

        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.buildGrid();
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Private Methods
    // Build Contract Type Options
    private buildContractTypeOptions(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        searchParams.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetBusinessContractTypes';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, formData).then(data => {
            let contractTypeList: Array<any> = [];
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);

            if (data.errorMessage) {
                this.displayMessage('Error', data.errorMessage, data.fullError);
                return;
            }

            if (data.BusinessContractTypes) {
                contractTypeList = data.BusinessContractTypes.split(',');

                contractTypeList.forEach((data) => {
                    let option: any = {
                        value: '',
                        text: ''
                    };

                    option['value'] = data;
                    switch (data) {
                        case 'C':
                            option['text'] = 'Contracts';
                            break;
                        case 'J':
                            option['text'] = 'Jobs';
                            break;
                        case 'P':
                            option['text'] = 'Product Sales';
                            break;
                    }

                    this.contractTypeCodeOptions.push(option);
                });
            }

            this.setControlValue('ContractTypeCode', 'C');
        }).catch(error => {
            this.displayMessage('Error', error.errorMessage, error.fullError);
        });
    }

    // Build Grid Options
    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('SalesAreaCode', 'Rezone', 'SalesAreaCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('SalesAreaCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseSalesEmployee', 'Rezone', 'PremiseSalesEmployee', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('PremiseSalesEmployee', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractNumber', 'Rezone', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'Rezone', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'Rezone', 'PremiseName', MntConst.eTypeText, 20);

        this.riGrid.AddColumn('AnnualValue', 'Rezone', 'AnnualValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumnAlign('AnnualValue', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremisePostcode', 'Rezone', 'PremisePostcode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('PremisePostcode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('PremiseState', 'Rezone', 'PremiseState', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('PremiseState', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('PremiseTown', 'Rezone', 'PremiseTown', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('PremiseTown', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Rezoned', 'Rezone', 'Rezoned', MntConst.eTypeImage, 1, false, 'Click here to rezone');
        this.riGrid.AddColumnAlign('Rezoned', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('SalesAreaCode', true);
        this.riGrid.AddColumnOrderable('PremisePostcode', true);
        this.riGrid.AddColumnOrderable('PremiseState', true);
        this.riGrid.AddColumnOrderable('PremiseTown', true);

        this.riGrid.Complete();
    }

    // Load Grid Data
    private loadGridData(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        // Remove Error Status
        this.uiForm.controls['SalesAreaCode'].markAsPristine();
        this.uiForm.controls['SalesAreaCode'].markAsUntouched();
        this.uiForm.controls['SalesAreaCodeTo'].markAsPristine();
        this.uiForm.controls['SalesAreaCodeTo'].markAsUntouched();
        this.uiForm.controls['ContractNumber'].markAsPristine();
        this.uiForm.controls['ContractNumber'].markAsUntouched();
        this.uiForm.controls['PostcodeFilter'].markAsPristine();
        this.uiForm.controls['PostcodeFilter'].markAsUntouched();

        searchParams.set(this.serviceConstants.Action, '2');
        searchParams.set('BranchNumber', this.utils.getBranchCode());
        searchParams.set('SalesAreaCode', this.getControlValue('SalesAreaCode'));
        searchParams.set('SalesAreaCodeTo', this.getControlValue('SalesAreaCodeTo'));
        searchParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        searchParams.set('PostcodeFilter', this.getControlValue('PostcodeFilter'));
        searchParams.set('ContractTypeCode', this.getControlValue('ContractTypeCode'));
        searchParams.set('PortfolioStatusType', this.getControlValue('PortfolioStatusType'));
        searchParams.set('Town', this.getControlValue('Town'));
        searchParams.set('County', this.getControlValue('County'));
        searchParams.set(this.serviceConstants.GridMode, '0');
        searchParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        if (this.gridParams.cacheRefresh) {
            searchParams.set(this.serviceConstants.GridCacheRefresh, 'true');
        }
        searchParams.set(this.serviceConstants.GridPageSize, this.riGrid.PageSize.toString());
        searchParams.set(this.serviceConstants.PageCurrent, this.gridParams.currentPage);
        searchParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        searchParams.set(this.serviceConstants.GridSortOrder, this.riGrid.DescendingSort ? 'Descending' : 'Ascending');
        if (this.riGrid.Update && this.getAttribute('PremiseRowID')) {
            searchParams.set('ROWID', this.getAttribute('PremiseRowID'));
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrGet(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage) {
                this.displayMessage('Error', data.errorMessage, data.fullError);
                return;
            }

            this.riGrid.RefreshRequired();
            if (this.riGrid.Update) {
                this.riGrid.StartRow = this.getAttribute('Row');
                this.riGrid.StartColumn = 0;
                this.riGrid.RowID = this.getAttribute('PremiseRowID');

                this.riGrid.UpdateHeader = false;
                this.riGrid.UpdateBody = true;
                this.riGrid.UpdateFooter = false;
            } else {
                this.gridParams.currentPage = data.pageData.pageNumber;
                this.gridParams.totalRecords = data.pageData.lastPageNumber * 10 || 10;
            }

            this.riGrid.Execute(data);

            this.pageParams.isRezoneDisabled = false;
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage('Error', error.errorMessage, error.fullError || '');
        });
    }

    // Display Message
    private displayMessage(type: string, message: string, fullError?: string): void {
        this.messageProperties = new ICabsModalVO();
        this.messageProperties.msg = message;
        this.messageProperties.fullError = fullError;

        this.modalAdvService['emit' + type](this.messageProperties);
    }

    // Get Rezone Data
    private fetchRezoneData(): void {
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('Rezoned', 'rowid'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    // Rezone Area
    private rezoneArea(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'Rezone';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['SalesAreaCodeTo'] = this.riGrid.Details.GetValue('Rezoned') === 'yes' ? this.getControlValue('SalesAreaCode') : this.getControlValue('SalesAreaCodeTo');
        formData['PremiseRowID'] = this.getAttribute('PremiseRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage) {
                this.displayMessage('Error', data.errorMessage, data.fullError);
                return;
            }

            this.riGrid.Update = true;
            this.loadGridData();
        }).catch(error => {
            this.displayMessage('Error', error.errorMessage, error.fullError || '');
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    // Populate Descriptions
    private populateDescriptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetDescriptions';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        formData['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');
        formData['ContractNumber'] = this.getControlValue('ContractNumber');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.errorMessage) {
                this.displayMessage('Error', data.errorMessage, data.fullError);
                return;
            }

            this.setControlValue('SalesAreaDesc', data['SalesAreaDesc']);
            this.setControlValue('SalesAreaDescTo', data['SalesAreaDescTo']);
            this.setControlValue('ContractName', data['ContractName']);

            this.riGrid.RefreshRequired();
        }).catch(error => {
            this.displayMessage('Error', error.errorMessage, error.fullError || '');
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    // Confirm Rezone
    private confirmRezone(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'ConfirmRezone';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        formData['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');
        formData['ContractTypeCode'] = this.getControlValue('ContractTypeCode');
        formData['PortfolioStatusType'] = this.getControlValue('PortfolioStatusType');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.ErrorMessage) {
                this.messageProperties.title = data.MessageTitle;
                this.messageProperties.msg = data.ErrorMessage;
                this.messageProperties.confirmCallback = () => {
                    this.rezoneAreaGroup();
                };
                this.modalAdvService.emitPrompt(this.messageProperties);
            } else {
                this.rezoneAreaGroup();
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage('Error', error.errorMessage, error.fullError || '');
        });
    }

    // Rezone Group Of Areas
    private rezoneAreaGroup(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let formData: any = {};

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'RezoneGroup';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['SalesAreaCode'] = this.getControlValue('SalesAreaCode');
        formData['SalesAreaCodeTo'] = this.getControlValue('SalesAreaCodeTo');
        formData['PostcodeFilter'] = this.getControlValue('PostcodeFilter');
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['ContractTypeCode'] = this.getControlValue('ContractTypeCode');
        formData['PortfolioStatusType'] = this.getControlValue('PortfolioStatusType');
        formData['Town'] = this.getControlValue('Town');
        formData['County'] = this.getControlValue('County');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);

            this.batchInformation = data.BatchInformation;

            this.displayMessage('Message', data.SubmitMessage, null);
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.displayMessage('Error', error.errorMessage, error.fullError || '');
        });
    }

    // Public Methods
    // On Contract Data Received From Ellipsis
    public onContractDataReceived(data: any): void {
        this.setControlValue('ContractNumber', data['ContractNumber']);
        this.setControlValue('ContractName', data['ContractName']);
    }

    // On Sales Area Data Received From Ellipsis
    public onSalesAreaDataReceived(data: any): void {
        this.setControlValue('SalesAreaCode', data['SalesAreaCode']);
        this.setControlValue('SalesAreaDesc', data['SalesAreaDesc']);
    }

    // On Sales Area To Data Received From Ellipsis
    public onSalesAreaToDataReceived(data: any): void {
        this.setControlValue('SalesAreaCodeTo', data['SalesAreaCode']);
        this.setControlValue('SalesAreaDescTo', data['SalesAreaDesc']);
    }

    // On Refresh Click
    public onGridRefresh(): void {
        this.gridParams.cacheRefresh = true;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    // On Current Page Recieved
    public onCurrentPageRecieved(data: any): void {
        if (this.gridParams.currentPage === data.value) {
            return;
        }
        this.gridParams.cacheRefresh = false;
        this.gridParams.currentPage = data.value || 1;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    // On Grid Header Sort Click
    public onGridSortClick(): void {
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    // On Grid Header Sort Click
    public onGridBodyClick(event: any): void {
        if (this.riGrid.CurrentColumnName !== 'Rezoned' || this.riGrid.RefreshRequiredStatus()) {
            return;
        }

        // Set Error Status
        if (!this.getControlValue('SalesAreaCodeTo')) {
            this.uiForm.controls['SalesAreaCodeTo'].markAsDirty();
            this.uiForm.controls['SalesAreaCodeTo'].markAsTouched();
            return;
        }

        this.fetchRezoneData();

        this.rezoneArea();
    }

    // On Change Of Fields
    public onFieldChange(control: string): void {
        let descRequiredList: Array<String> = ['SalesAreaCode', 'SalesAreaCodeTo', 'ContractNumber'];

        this.pageParams.isRezoneDisabled = true;

        if (descRequiredList.indexOf(control) >= 0) {
            this.populateDescriptions();
        }
    }

    // On Click Of Rezone
    public onRezoneClick(): void {
        let isError: boolean = false;
        let search: QueryParams;
        let formData: any = {};

        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }

        if (!this.getControlValue('ContractNumber') && !this.getControlValue('PostcodeFilter') && !this.getControlValue('Town') && !this.getControlValue('County')) {
            this.confirmRezone();
        } else {
            this.rezoneAreaGroup();
        }
    }
}
