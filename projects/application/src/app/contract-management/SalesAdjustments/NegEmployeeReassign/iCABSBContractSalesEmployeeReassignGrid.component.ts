import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';

import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from '../../../../shared/components/pagination/pagination';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { ContractSearchComponent } from './../../../internal/search/iCABSAContractSearch';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { BaseComponent } from '../../../base/BaseComponent';

@Component({
    templateUrl: 'iCABSBContractSalesEmployeeReassignGrid.html'
})

export class ContractSalesEmployeeReassignGridComponent extends BaseComponent implements OnInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('contractSalesEmployeeReassignPagination') contractSalesEmployeeReassignPagination: PaginationComponent;
    private muleConfig: any = {
        operation: 'Business/iCABSBContractSalesEmployeeReassignGrid',
        module: 'sales',
        method: 'contract-management/maintenance'
    };
    public pageId: string = '';
    public currentPage: number = 1;
    public itemsPerPage: number = 10;
    public totalRecords: number = 1;
    public isPaginationEnabled: boolean = false;
    public ellipsisConfig: any = {
        employeeSearch: {
            childConfigParams: {
                parentMode: 'LookUp-ContractSalesEmployee',
                showAddNew: false
            },
            modalConfig: '',
            contentComponent: EmployeeSearchComponent,
            showHeader: true,
            disabled: false
        },
        employeeSearchTo: {
            childConfigParams: {
                parentMode: 'LookUp-ContractSalesEmployeeTo',
                showAddNew: false
            },
            modalConfig: '',
            contentComponent: EmployeeSearchComponent,
            showHeader: true,
            disabled: false
        },
        contract: {
            isAutoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp',
                showAddNew: false,
                currentContractTypeURLParameter: this.pageParams.CurrentContractTypeURLParameter,
                currentContractType: this.riExchange.getCurrentContractType()
            },
            modalConfig: '',
            contentComponent: ContractSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        }
    };

    public controls: Array<any> = [
        { name: 'ContractSalesEmployee', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'SalesEmployeeSurname', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'ContractNumber', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', readonly: false, disabled: false, required: false },
        { name: 'ContractSalesEmployeeTo', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'SalesEmployeeSurnameTo', readonly: false, disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'PostcodeFilter', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBCONTRACTSALESEMPLOYEEREASSIGNGRID;
        this.browserTitle = this.pageTitle = 'Reassign Negotiating Employee';
    }
    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    private windowOnload(): void {
        this.riGrid.FunctionPaging = true;
        this.riMaintenance.FunctionAdd = false;
        this.riMaintenance.FunctionUpdate = false;
        this.riMaintenance.FunctionDelete = false;
        this.riMaintenance.FunctionSelect = false;
        this.riMaintenance.FunctionSnapShot = false;
        this.riExchange.riInputElement.Disable(this.uiForm, 'NegBranchNumber');
        this.riExchange.riInputElement.Disable(this.uiForm, 'SalesEmployeeSurname');
        this.riExchange.riInputElement.Disable(this.uiForm, 'SalesEmployeeSurnameTo');
        this.riExchange.riInputElement.Disable(this.uiForm, 'ContractName');
        this.setControlValue('NegBranchNumber', '');
        this.buildGrid();
    }


    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.HighlightBar = true;
        this.riGrid.AddColumn('ContractSalesEmployee', 'Reassign', 'ContractSalesEmployee', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ContractSalesEmployee', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('SalesEmployeeSurname', 'Reassign', 'SalesEmployeeSurname', MntConst.eTypeText, 20);

        this.riGrid.AddColumn('ContractNumber', 'Reassign', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractName', 'Reassign', 'ContractName', MntConst.eTypeText, 20);

        this.riGrid.AddColumn('AnnualValue', 'Reassign', 'AnnualValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumnAlign('AnnualValue', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('ContractPostcode', 'Reassign', 'ContractPostcode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractPostcode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractState', 'Reassign', 'ContractState', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractState', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContractTown', 'Reassign', 'ContractTown', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ContractTown', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Reassigned', 'Reassign', 'Reassigned', MntConst.eTypeImage, 1, false, 'Click here to Reassign');
        this.riGrid.AddColumnAlign('Reassigned', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('ContractSalesEmployee', true);
        this.riGrid.AddColumnOrderable('ContractPostcode', true);
        this.riGrid.AddColumnOrderable('ContractState', true);
        this.riGrid.AddColumnOrderable('ContractTown', true);

        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): any {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        let sortOrder: any = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set('ContractSalesEmployee', this.getControlValue('ContractSalesEmployee'));
        gridQueryParams.set('ContractSalesEmployeeTo', this.getControlValue('ContractSalesEmployeeTo'));
        gridQueryParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        gridQueryParams.set('PostcodeFilter', this.getControlValue('PostcodeFilter'));
        gridQueryParams.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.currentPage.toString());
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        //gridQueryParams.set(this.serviceConstants.GridSortOrder, sortOrder);
        gridQueryParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        gridQueryParams.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        if (this.riGrid.Update) {
            gridQueryParams.set('ROWID', this.getAttribute('ContractSalesEmployeeContractRowID'));
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    if (data.hasError || data['error_description']) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.totalRecords = 0;
                        this.isPaginationEnabled = false;
                        this.riGrid.ResetGrid();
                    } else {
                        this.riGrid.RefreshRequired();
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.getAttribute('ContractSalesEmployeeRow');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('ContractSalesEmployeeContractRowID');
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = false;
                        } else {
                            this.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                        }
                        this.riGrid.Execute(data);
                    }
                }
            },
            error => {
                this.totalRecords = 1;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    /**
     * Handle AfterExecute event of Grid
     */
    public riGridAfterExecute(): void {
        this.isPaginationEnabled = (this.riGrid.bodyArray && this.riGrid.bodyArray.length > 0) ? true : false;
        this.totalRecords = (this.riGrid.bodyArray.length > 0 && this.totalRecords === 0) ? 1 : this.totalRecords;
        setTimeout(() => {
            if ((this.riGrid.bodyArray.length > 1) && this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children[0]) {
                if (this.riGrid.HTMLGridBody.children[0].children[0]) {
                    this.riGrid.SetDefaultFocus();
                    this.reassignFocus(this.riGrid.HTMLGridBody.children[0].children[0]);
                }
            }
        }, 0);
    }

    public getGridInfo(info: any): void {
        this.totalRecords = info.totalRows;
        this.riGridAfterExecute();
    }

    // Get page specific grid data from service end
    public getCurrentPage(data: any): void {
        this.currentPage = data.value;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    // refresh the grid content
    public refresh(): void {
        this.reset();
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public reassignFocus(rsrcElement: any): void {
        let oTR: any = null;
        oTR = rsrcElement.parentElement;
        if (oTR) {
            this.setAttribute('ContractSalesEmployeeContractRowID', oTR.children[8].children[0].getAttribute('RowID'));
            this.setAttribute('ContractSalesEmployeeRow', this.riGrid.CurrentRow);
        }
    }

    public cmdReassignOnclick(): void {
        let binError: boolean = false;
        if (!this.getControlValue('ContractSalesEmployee')) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'ContractSalesEmployee');
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ContractSalesEmployee', true);
            binError = true;
        }
        if (!this.getControlValue('ContractSalesEmployeeTo')) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'ContractSalesEmployeeTo');
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ContractSalesEmployeeTo', true);
            binError = true;
        }
        if (!this.getControlValue('PostcodeFilter') && !this.getControlValue('ContractNumber')) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'PostcodeFilter');
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'PostcodeFilter', true);
            this.riExchange.riInputElement.markAsError(this.uiForm, 'ContractNumber');
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ContractNumber', true);
            binError = true;
        } else {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'PostcodeFilter');
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'PostcodeFilter', false);
            this.riExchange.riInputElement.markAsError(this.uiForm, 'ContractNumber');
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ContractNumber', false);
        }

        if (!binError) {
            let formData: Object = {};
            let confirmMessage = '';
            let query: QueryParams = this.getURLSearchParamObject();
            formData['Function'] = 'ReassignGroup';
            formData['ContractSalesEmployee'] = this.getControlValue('ContractSalesEmployee');
            formData['ContractSalesEmployeeTo'] = this.getControlValue('ContractSalesEmployeeTo');
            formData['PostcodeFilter'] = this.getControlValue('PostcodeFilter');
            formData['ContractNumber'] = this.getControlValue('ContractNumber');

            query.set(this.serviceConstants.Action, '6');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query, formData)
                .subscribe((data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.refresh();
                    }
                },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    public contractSalesEmployeeOnchange(): void {
        this.populateDescriptions();
    }

    public contractSalesEmployeeToOnchange(): void {
        this.populateDescriptions();
    }

    public contractNumberOnchange(): void {
        if (this.getControlValue('ContractNumber')) {
            let data: any = this.utils.numberPadding(this.getControlValue('ContractNumber'), 8); //Fill the contract number field with leading zeroes.
            this.setControlValue('ContractNumber', data);
        }
        this.populateDescriptions();
    }

    public populateDescriptions(): void {
        let formData: Object = {};
        let confirmMessage = '';
        let query: QueryParams = this.getURLSearchParamObject();
        formData['Function'] = 'GetDescriptions';
        formData['ContractSalesEmployee'] = this.getControlValue('ContractSalesEmployee');
        formData['ContractSalesEmployeeTo'] = this.getControlValue('ContractSalesEmployeeTo');
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        query.set(this.serviceConstants.Action, '6');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query, formData)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('SalesEmployeeSurname', data.SalesEmployeeSurname ? data.SalesEmployeeSurname : '');
                    this.setControlValue('SalesEmployeeSurnameTo', data.SalesEmployeeSurnameTo ? data.SalesEmployeeSurnameTo : '');
                    this.setControlValue('ContractName', data.ContractName ? data.ContractName : '');
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    // handles grid sort functionality
    public gridSort(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public riGridBodyOnClick(event: Event): void {
        if ((this.riGrid.CurrentColumnName === 'Reassigned') && (this.riGrid.bodyArray && (this.riGrid.CurrentRow + 1 < this.riGrid.bodyArray.length))) {
            let binError: boolean = false;
            if (!this.getControlValue('ContractSalesEmployeeTo')) {
                this.riExchange.riInputElement.markAsError(this.uiForm, 'ContractSalesEmployeeTo');
                this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ContractSalesEmployeeTo', true);
                binError = true;
            }
            if (!binError) {
                this.reassignFocus(event.srcElement.parentElement.parentElement);
                let formData: Object = {};
                let confirmMessage = '';
                let query: QueryParams = this.getURLSearchParamObject();
                formData['Function'] = 'Reassign';
                formData['ContractRowID'] = this.riGrid.Details.GetAttribute('Reassigned', 'rowid');
                formData['ContractSalesEmployeeTo'] = this.getControlValue('ContractSalesEmployeeTo');
                query.set(this.serviceConstants.Action, '6');

                this.ajaxSource.next(this.ajaxconstant.START);
                this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query, formData)
                    .subscribe((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.riGrid.Update = true;
                            this.riGridBeforeExecute();
                        }
                    },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        });
            }
        }
    }

    // Handle Grid Body On KeyDown event
    public riGridBodyOnKeyDown(event: any): void {
        switch (event.keyCode) {
            case 38:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.previousSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.reassignFocus(event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }
                }
                break;
            case 40:
            case 9:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.nextSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.reassignFocus(event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }
                }
                break;
        }
    }

    public onEmployeeSearchDataReceived(data: any, route: any): void {
        this.setControlValue('ContractSalesEmployee', data.ContractSalesEmployee);
        this.setControlValue('SalesEmployeeSurname', data.SalesEmployeeSurname);
    }

    public onToEmployeeSearchDataReceived(data: any, route: any): void {
        this.setControlValue('ContractSalesEmployeeTo', data.ContractSalesEmployeeTo);
        this.setControlValue('SalesEmployeeSurnameTo', data.SalesEmployeeSurnameTo);
    }

    public onContractSearchDataReceived(data: any, route: any): void {
        this.setControlValue('ContractNumber', data.ContractNumber);
        this.setControlValue('ContractName', data.AccountName);
    }

    public reset(): void {
        for (let name in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(name)) {
                if (!this.uiForm.controls[name].value)
                    this.uiForm.controls[name].setValue('');
                this.uiForm.controls[name].setErrors(null);
            }
        }
    }
}
