import { ProductExpenseMaintenanceComponent } from './TableMaintenanceBusiness/ProductExpense/iCABSBProductExpenseMaintenance.component';
import { ProductCoverMaintenanceComponent } from './TableMaintenanceBusiness/ProductCover/iCABSBProductCoverMaintenance.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class ProductAdminRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ProductAdminRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSBPRODUCTCOVERMAINTENANCE_SUB, component: ProductCoverMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSBPRODUCTEXPENSEMAINTENANCE, component: ProductExpenseMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ],
                data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        ProductAdminRootComponent,
        ProductCoverMaintenanceComponent,
        ProductExpenseMaintenanceComponent
    ],
    exports: [
        ProductCoverMaintenanceComponent,
        ProductExpenseMaintenanceComponent
    ],
    entryComponents: [
        ProductCoverMaintenanceComponent,
        ProductExpenseMaintenanceComponent
    ]
})

export class ProductAdminModule { }
