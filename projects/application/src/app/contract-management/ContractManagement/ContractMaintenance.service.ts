import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Utils } from './../../../shared/services/utility';
import { AuthService } from './../../../shared/services/auth.service';
import { HttpService } from './../../../shared/services/http-service';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ContractMaintenanceService {

    private queryParamsContract: any = {
        action: '0',
        operation: 'Application/iCABSAContractMaintenance',
        module: 'contract',
        method: 'contract-management/maintenance'
    };

    constructor(
        private serviceConstants: ServiceConstants,
        private httpService: HttpService,
        private utils: Utils,
        private authService: AuthService
    ) {

    }

    private getURLSearchParams(action?: string): QueryParams {
        let params: QueryParams = new QueryParams();
        if (!action) {
            action = this.queryParamsContract.action;
        }
        params.set(this.serviceConstants.Action, action);
        params.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        params.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        return params;
    }

    public fetchContractRecord(contractNumber: any, contractTypeCode: any): Observable<any> {
        let params = this.getURLSearchParams();
        params.set('ContractNumber', contractNumber);
        params.set('ContractTypeCode', contractTypeCode);
        return this.httpService.makeGetRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, params);
    }

    public fetchContractRecordByRowID(contractRowID: any, contractTypeCode: any): Observable<any> {
        let params = this.getURLSearchParams();
        params.set('ContractRowID', contractRowID);
        params.set('ContractTypeCode', contractTypeCode);
        return this.httpService.makeGetRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, params);
    }

    public updateContract(formData: any): Observable<any> {
        let params = this.getURLSearchParams('2');
        return this.httpService.makePostRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, params, formData);
    }

    public insertContract(formData: any): Observable<any> {
        let params = this.getURLSearchParams('1');
        return this.httpService.makePostRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, params, formData);
    }

    public lookUpRecord(data: Object, maxresults: number): Observable<any> {
        let params = this.getURLSearchParams();
        if (maxresults) {
            params.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(params, data);
    }

    public lookUpAccountDetails(accountNumber: any): Observable<any> {
        let data: any[] = [];
        data.push(this.buildLookupAccountDetails(accountNumber));
        return this.lookUpRecord(data, 5);
    }

    private callFunctions(functionNames: string, additionalParams?: Object): Observable<any> {
        let params = this.getURLSearchParams('6');
        params.set('Function', functionNames);

        // Set additional parameters
        if (additionalParams) {
            for (let key in additionalParams) {
                if (key) {
                    params.set(key, additionalParams[key]);
                }
            }
        }

        return this.httpService.makeGetRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, params);
    }

    public fetchAccountData(params: Object): Observable<any> {
        let standardParams = this.getURLSearchParams();
        for (let key in params) {
            if (key) {
                standardParams.set(key, params[key]);
            }
        }
        return this.httpService.makeGetRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, standardParams);
    }

    public fetchSysChar(sysCharNumbers: string): Observable<any> {
        let params = this.getURLSearchParams();
        params.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(params);
    }

    public fetchUserAuthority(): Observable<any> {
        let userCode = this.authService.getSavedUserCode();
        let data: any[] = [];
        data.push(this.buildLookupUserAuthorityPermissions(userCode.UserCode));
        return this.lookUpRecord(data, 1);
    }

    public fetchProspectDetails(prospectNumber: any): Observable<any> {
        return this.callFunctions('GetProspectDetails', { ProspectNumber: prospectNumber });
    }

    public fetchCIParams(): Observable<any> {
        let data: any[];
        data.push(this.buildLookupCIParams());
        return this.lookUpRecord(data, 1);
    }

    public checkCommenceDateIsInAllowedRange(commenceDate: string): Observable<any> {
        return this.callFunctions('WarnCommenceDate', { ContractCommenceDate: commenceDate });
    }

    public checkMandateReferenceValid(contractNumber: any, companyCode: any): Observable<any> {
        return this.callFunctions('WarnMandateRef', {
            ContractNumber: contractNumber,
            CompanyCode: companyCode
        });
    }

    public checkPostcodeNegBranch(negBranchNumber: any, addressLine4: any, addressLine5: any, postCode: any): Observable<any> {
        return this.callFunctions('CheckPostcodeNegBranch', {
            NegBranchNumber: negBranchNumber,
            ContractAddressLine4: addressLine4,
            ContractAddressLine5: addressLine5,
            ContractPostcode: postCode
        });
    }

    public checkPostCode(contractName: any, addressLine1: any, addressLine2: any, addressLine3: any, addressLine4: any, addressLine5: any, postCode: any): Observable<any> {
        return this.callFunctions('CheckPostcode', {
            ContractName: contractName,
            ContractAddressLine1: addressLine1,
            ContractAddressLine2: addressLine2,
            ContractAddressLine3: addressLine3,
            ContractAddressLine4: addressLine4,
            ContractAddressLine5: addressLine5,
            ContractPostcode: postCode
        });
    }

    private checkBranchFunction(functionName: string, negBranchNumber: any, accountNumber: any, contractTypeCode: any): Observable<any> {
        return this.callFunctions(functionName, {
            NegBranchNumber: negBranchNumber,
            AccountNumber: accountNumber,
            ContractTypeCode: contractTypeCode
        });
    }

    public checkPostCodeWarnings(negBranchNumber: any, accountNumber: any, contractTypeCode: any): Observable<any> {
        return this.checkBranchFunction('WarnPostcode', negBranchNumber, accountNumber, contractTypeCode);
    }

    public checkNegBranchIsNationalAccountsBranch(negBranchNumber: any, accountNumber: any, contractTypeCode: any): Observable<any> {
        return this.checkBranchFunction('CheckNatAcc', negBranchNumber, accountNumber, contractTypeCode);
    }

    public getDefaultFieldInvoiceAndPaymentFieldValues(): Observable<any> {
        return this.callFunctions('GetDefaultInvoiceFrequency,GetDefaultInvoiceFeeCode,GetDefaultPaymentType,GetPaymentTypeDetails');
    }

    public getDefaultSalesEmployeeFromPostcode(branchNumber: any, postCode: any, addressLine4: any, addressLine5: any): Observable<any> {
        return this.callFunctions('DefaultFromPostcode', {
            BranchNumber: branchNumber,
            ContractPostcode: postCode,
            ContractAddressLine4: addressLine4,
            ContractAddressLine5: addressLine5
        });
    }

    public getPostcodeTownAndState(postCode: any, town: any, state: any): Observable<any> {
        let params = this.getURLSearchParams('0');
        params.set('Function', 'GetPostCodeTownAndState');
        params.set('Postcode', postCode);
        params.set('Town', town);
        params.set('State', state);
        return this.httpService.makeGetRequest(this.queryParamsContract.method, this.queryParamsContract.module, this.queryParamsContract.operation, params);
    }

    public getDefaultDates(contractTypeCode: string, contractCommenceDate: any, minimumDurationCode: any, contractDurationCode: any, companyCode: any): Observable<any> {
        let functionList = '';
        switch (contractTypeCode) {
            case 'C':
                functionList = 'GetAnniversaryDate,GetMinimumExpiryDate,GetExpiryDate';
                break;
            case 'J':
                functionList = 'GetAnniversaryDate,GetJobExpiryDate';
                break;
            case 'P':
                functionList = 'GetAnniversaryDate,GetPaymentTypeDetails,GetNoticePeriod';
                break;
        }
        let dataObj = {
            ContractCommenceDate: contractCommenceDate,
            MinimumDurationCode: minimumDurationCode,
            ContractDurationCode: contractDurationCode,
            CompanyCode: companyCode
        };
        return this.callFunctions(functionList, dataObj);
    }

    public getDurationData(): Observable<any> {
        return this.httpService.makeGetRequest('contract-management/search', 'duration', 'Business/iCABSBContractDurationSearch', this.getURLSearchParams());
    }

    public getSEPAMandateWarning(): Observable<any> {
        return this.httpService.riGetErrorMessage(3201, this.utils.getCountryCode(), this.utils.getBusinessCode());
    }

    public getDiscountData(discountTypeCode: string): Observable<any> {
        let query = this.getURLSearchParams('6');
        query.set('function', 'GetDefaultDiscountDetails');
        return this.httpService.makePostRequest('contract-management/maintenance', 'account', 'Application/iCABSAAccountMaintenance', query, { discountTypeCode: discountTypeCode });
    }

    public getContactPersonUpdates(contractNumber: any): Observable<any> {
        return this.callFunctions('GetContactPersonChanges', { ContractNumber: contractNumber });
    }

    public getAccountMandateNumber(accountNumber: any): Observable<any> {
        return this.callFunctions('GetAccountMandateNumber', { AccountNumber: accountNumber });
    }

    public getMandatePaymentTypeDetails(paymentTypeCode: any): Observable<any> {
        return this.callFunctions('GetPaymentTypeDetails,GetNoticePeriod', { 'PaymentTypeCode': paymentTypeCode, 'BranchNumber': this.utils.getBranchCode() });
    }

    public getPaymentTypeWarning(paymentTypeCode: any): Observable<any> {
        return this.getMandatePaymentTypeDetails(paymentTypeCode);
    }

    public getInvoiceFrequencyCharge(invoiceFrequencyCode: any): Observable<any> {
        return this.callFunctions('GetInvoiceFrequencyCharge', { InvoiceFrequencyCharge: invoiceFrequencyCode });
    }

    public getMinimumExpiryDate(contractCommenceDate: any, invoiceAnnivDate: any, minimumDurationCode: any): Observable<any> {
        return this.callFunctions('GetMinimumExpiryDate', {
            ContractCommenceDate: contractCommenceDate,
            InvoiceAnnivDate: invoiceAnnivDate,
            MinimumDurationCode: minimumDurationCode
        });
    }

    public getExpiryDate(contractCommenceDate: any, invoiceAnnivDate: any, contractDurationCode: any): Observable<any> {
        return this.callFunctions('GetExpiryDate', {
            ContractCommenceDate: contractCommenceDate,
            InvoiceAnnivDate: invoiceAnnivDate,
            ContractDurationCode: contractDurationCode
        });
    }

    public getSubsequentExpiryDate(contractCommenceDate: any, invoiceAnnivDate: any, contractDurationCode: any): Observable<any> {
        return this.callFunctions('GetSubsequentExpiryDate', {
            ContractCommenceDate: contractCommenceDate,
            InvoiceAnnivDate: invoiceAnnivDate,
            ContractDurationCode: contractDurationCode
        });
    }

    public generateNewRef(accountNumber: any): Observable<any> {
        return this.callFunctions('GenerateNewRef', { AccountNumber: accountNumber });
    }

    public updateAccountAddress(accountNumber: any, addressLine1: any, addressLine2: any, addressLine3: any, addressLine4: any, addressLine5: any, contractPostcode: any, countryCode: any): Observable<any> {
        return this.callFunctions('UpdateAccountAddress', {
            AccountNumber: accountNumber,
            ContractAddressLine1: addressLine1,
            ContractAddressLine2: addressLine2,
            ContractAddressLine3: addressLine3,
            ContractAddressLine4: addressLine4,
            ContractAddressLine5: addressLine5,
            ContractPostcode: contractPostcode,
            CountryCode: countryCode
        });
    }

    public updateTrialPeriod(contractNumber: any): Observable<any> {
        //This function is deliberately disabled as it causes massive damage to service cover records.
        // When the remaining code is tidied up, it should be removed
        // For reference:
        // Function: 'UpdateContractTrialInd'
        // action: '7'
        // It should be an HTTP POST request
        return undefined;
    }

    public searchForAccountByContractName(contractName: any): Observable<any> {
        return this.fetchAccountData({ SearchName: contractName });
    }

    public validateCompanyCodeChange(accountNumber: any, companyCode: any, contractTypeCode: any, paymentTypeCode: any, contractNumber: any, negBranchNumber: any): Observable<any> {
        return this.callFunctions('CompanyCodeChange,GetPaymentTypeDetails,GetNoticePeriod', {
            AccountNumber: accountNumber,
            CompanyCode: companyCode,
            ContractTypeCode: contractTypeCode,
            PaymentTypeCode: paymentTypeCode,
            ContractNumber: contractNumber,
            BranchNumber: negBranchNumber
        });
    }

    private buildLookupSection(table: string, query: Object, fields: string[]): any {
        return {
            'table': table,
            'query': query,
            'fields': fields
        };
    }

    public buildLookupCIParams(): any {
        return this.buildLookupSection('CIParams', { 'BusinessCode': this.utils.getBusinessCode() }, ['BusinessCode', 'CIEnabled']);
    }

    public buildLookupContactPerson(): any {
        return this.buildLookupSection('riRegistry', { 'RegSection': 'Contact Person' }, ['RegSection']);
    }

    public buildLookupCCMReviewFromDrill(): any {
        return this.buildLookupSection('riRegistry',
            { 'RegSection': 'Contact Centre Review', 'RegKey': this.utils.getBusinessCode() + '_' + 'System Default Review From Drill Option' },
            ['RegSection', 'RegValue']);
    }

    public buildLookupAccountDetails(accountNumber: any): any {
        return this.buildLookupSection('Account',
            { 'BusinessCode': this.utils.getBusinessCode(), 'AccountNumber': accountNumber },
            ['AccountName', 'AccountNumber', 'NationalAccount', 'AccountBalance']
        );
    }

    public buildLookupAllBranchDetails(): any {
        return this.buildLookupSection('Branch',
            { 'BusinessCode': this.utils.getBusinessCode() },
            ['BranchNumber', 'BranchName', 'EnablePostCodeDefaulting']);
    }

    public buildLookupDefaultBranchAuthority(userCode: any): any {
        return this.buildLookupSection('UserAuthorityBranch',
            { 'UserCode': userCode, 'BusinessCode': this.utils.getBusinessCode(), 'DefaultBranchInd': 'true' },
            ['BranchNumber', 'DefaultBranchInd', 'CurrentBranchInd']);
    }

    public buildLookupDefaultCompany(): any {
        return this.buildLookupSection('Company', { 'BusinessCode': this.utils.getBusinessCode() }, ['DefaultCompanyInd', 'CompanyCode', 'CompanyDesc']);
    }

    public buildLookupUserAuthorityPermissions(userCode: any): any {
        return this.buildLookupSection('UserAuthority',
            { 'BusinessCode': this.utils.getBusinessCode(), 'UserCode': userCode },
            ['BusinessCode', 'AllowViewOfSensitiveInfoInd', 'UserCode', 'AllowUpdateOfContractInfoInd']);
    }

    public buildLookupGroupAccountDetails(groupAccountNumber: any): any {
        return this.buildLookupSection('GroupAccount', { 'GroupAccountNumber': groupAccountNumber }, ['GroupAccountNumber', 'GroupName']);
    }

    public buildLookupEmployeeSurname(employeeCode: any): any {
        return this.buildLookupSection('Employee',
            { 'BusinessCode': this.utils.getBusinessCode(), 'EmployeeCode': employeeCode },
            ['EmployeeCode', 'EmployeeSurname']);
    }

    public buildLookupSystemInvoiceFrequencyDesc(invoiceFrequencyCode: any): any {
        return this.buildLookupSection('SystemInvoiceFrequency',
            { 'InvoiceFrequencyCode': invoiceFrequencyCode },
            ['InvoiceFrequencyCode', 'InvoiceFrequencyDesc']);
    }

    public buildLookupInvoiceFeeDesc(invoiceFeeCode: any): any {
        return this.buildLookupSection('InvoiceFee',
            { 'InvoiceFeeCode': invoiceFeeCode },
            ['InvoiceFeeCode', 'InvoiceFeeDesc']);
    }

    public buildLookupPaymentTypeDesc(paymentTypeCode: any): any {
        return this.buildLookupSection('PaymentType',
            { 'PaymentTypeCode': paymentTypeCode },
            ['PaymentTypeCode', 'PaymentDesc']);
    }

    public buildLookupGroupAccountPriceGroupDesc(groupAccountPriceGroupID: any): any {
        return this.buildLookupSection('GroupAccountPriceGroup',
            { 'GroupAccountPriceGroupID': groupAccountPriceGroupID },
            ['GroupAccountPriceGroupID', 'GroupAccountPriceGroupDesc']);
    }

    public buildLookupGroupAccountPriceGroupDescForGroupAccount(groupAccountPriceGroupID: any, groupAccountNumber: any): any {
        return this.buildLookupSection('GroupAccountPriceGroup',
            { 'GroupAccountPriceGroupID': groupAccountPriceGroupID, 'GroupAccountNumber': groupAccountNumber },
            ['GroupAccountPriceGroupID', 'GroupAccountPriceGroupDesc']);
    }

    public buildLookupContractDuractionDesc(contractDurationCode: any): any {
        return this.buildLookupSection('ContractDuration',
            { 'ContractDurationCode': contractDurationCode },
            ['ContractDurationCode', 'ContractDurationDesc']);
    }
}
