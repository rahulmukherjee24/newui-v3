import { Component, OnInit, Injector, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { PremiseSearchComponent } from '@internal/search/iCABSAPremiseSearch';
import { ContractSearchComponent } from '@internal/search/iCABSAContractSearch';
import { PageIdentifier } from '@base/PageIdentifier';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { IControls } from '@base/ControlsType';
import { IXHRParams } from '@base/XhrParams';
import { LightBaseComponent } from '@base/BaseComponentLight';
import { RouteAwayComponent } from '@shared/components/route-away/route-away';
import { MessageConstant } from '@shared/constants/message.constant';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { CommonMaintenanceFunction } from '@app/base/CommonMaintenanceFunction';

@Component({
    templateUrl: 'iCABSAPNOLSiteReferenceMaintenance.html',
    providers: [CommonLookUpUtilsService]
})

export class PnolSiteReferenceMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;
    private httpParams: IXHRParams = {
        method: 'contract-management/maintenance',
        module: 'contract-admin',
        operation: 'Application/iCABSAPNOLSiteReferenceMaintenance'
    };
    private tempFormData: object = null;
    public pageId: string;
    public pageMode: string;
    public commonMaintenanceFunction: CommonMaintenanceFunction;
    public isDisabled: boolean = true;
    public inputParams: any = {
        contract: {
            parentMode: 'Lookup',
            showAddNew: false,
            showHeader: true,
            showCloseButton: true,
            contentComponent: ContractSearchComponent
        },
        premise: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'currentContractTypeURLParameter': this.pageParams.CurrentContractTypeURLParameter,
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false,
                'currentContractType': this.riExchange.getCurrentContractType()
            },
            component: PremiseSearchComponent
        }
    };
    public controls: IControls[] = [
        {name: 'ContractNumber', type: MntConst.eTypeCode, required: true},
        {name: 'ContractName', type: MntConst.eTypeText, disabled: true},
        {name: 'PremiseNumber', type: MntConst.eTypeInteger, required: true},
        {name: 'PremiseName', type: MntConst.eTypeText, disabled: true},
        {name: 'PNOLSiteRef', type: MntConst.eTypeText, required: true}
    ];

    constructor(injector: Injector, private commonLookupUtils: CommonLookUpUtilsService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAPNOLSITEREFERENCEMAINTENANCE;
        this.commonMaintenanceFunction = new CommonMaintenanceFunction(this, injector);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.pageMode = MntConst.eModeAdd;
        this.disableControl('PNOLSiteRef', true);
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
    }

    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.isRequesting = false;
        this.displayMessage(error, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
    }

    public onBtnClick(action: string): void {
        switch (action.toLowerCase()) {
            case 'save':
                this.commonMaintenanceFunction.onSaveClick(
                    () => { this.saveRecord(); } );
                break;
            case 'cancel':
            if (this.pageMode !== MntConst.eModeAdd) {
                this.setControlValue('PNOLSiteRef', this.tempFormData['PNOLSiteRef']);
            }
                this.formPristine();
                break;
        }
    }

    public onDataRecieved(event: any, type: string): void {
        this.isDisabled = true;
        this.disableControl('PNOLSiteRef', true);
        switch (type) {
            case 'premisePageDown':
                if (!this.getControlValue('PremiseNumber')) {
                    this.formPristine();
                }
                this.setControlValue('PremiseName', '');
                this.setControlValue('PNOLSiteRef', '');
                this.isDisabled = true;
                this.disableControl('PNOLSiteRef', true);
                this.commonLookupUtils.getPremiseName(this.getControlValue('PremiseNumber'), this.utils.getBusinessCode(), this.getControlValue('ContractNumber'))
                    .then(data => {
                        if (data[0][0]) {
                            this.setControlValue('PremiseName', data[0][0].PremiseName);
                            this.fetchPNOLSiteRef();
                        }
                    });
                break;
            case 'premise':
                this.setControlValue('PremiseNumber', event.PremiseNumber);
                this.setControlValue('PremiseName', event.PremiseName);
                this.fetchPNOLSiteRef();
                break;
            case 'contract':
                this.inputParams['premise'].childConfigParams['ContractNumber'] = event.ContractNumber || event.target.value;
                if (!this.getControlValue('ContractNumber')) {
                    this.formPristine();
                }
                if (event.ContractName) { // for ellipsis
                    this.clearControls(['ContractNumber', 'ContractName']);
                    this.setControlValue('ContractNumber', event.ContractNumber);
                    this.setControlValue('ContractName', event.ContractName);
                    this.inputParams['premise'].childConfigParams['ContractName'] = event.ContractName;
                } else { // for manual entry
                    this.commonLookupUtils.getContractName(this.getControlValue('ContractNumber'))
                        .then(data => {
                            this.clearControls(['ContractNumber', 'ContractName']);
                            if (data[0][0].ContractName) {
                                this.setControlValue('ContractName', data[0][0].ContractName);
                                this.inputParams['premise'].childConfigParams['ContractName'] = data[0][0].ContractName;
                            } else {
                                this.disableControl('PNOLSiteRef', true);
                                this.isDisabled = true;
                            }
                        });
                }
                break;
        }
    }

    public saveRecord(): void {
        this.isRequesting = true;
        const search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, 2);
        this.httpService.xhrPost(this.httpParams.method, this.httpParams.module, this.httpParams.operation, search, this.uiForm.getRawValue())
        .then(data => {
            this.isRequesting = false;
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.commonMaintenanceFunction.setPageMode(MntConst.eModeUpdate, this.uiForm.getRawValue());
            this.formPristine();
                if (this.hasError(data)) {
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.displayMessage(MessageConstant.Message.RecordSavedSuccessfully, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                this.tempFormData = this.uiForm.getRawValue();
                this.pageMode = MntConst.eModeSelect;
        }).catch((error) => {
            this.handleError(error);
        });
    }

    private fetchPNOLSiteRef(): void {
        this.isRequesting = true;
        const search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, 0);
        const formData: Object = {};
        formData['ContractNumber'] = this.getControlValue('ContractNumber');
        formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        this.httpService.xhrPost(this.httpParams.method, this.httpParams.module, this.httpParams.operation, search, formData)
            .then(data => {
                this.isRequesting = false;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (this.hasError(data)) {
                    this.pageMode = MntConst.eModeUpdate;
                    this.displayMessage(data.errorMessage || data.fullError || data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    return;
                }
                this.tempFormData = data;
                this.uiForm.patchValue(data);
                this.pageMode = MntConst.eModeSelect;
                this.isDisabled = false;
                this.disableControl('PNOLSiteRef', false);
            }).catch((error) => {
                this.pageMode = MntConst.eModeUpdate;
                this.handleError(error);
            });
    }
}
