import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { RouteAwayGuardService } from '@shared/services/route-away-guard.service';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';
import { WasteConsignmentNoteRangeGridComponent } from './TableMaintenanceBusiness/WasteGrid/iCABSBWasteConsignmentNoteRangeGrid.component';
import { WasteConsignmentNoteHistoryGridComponent } from './TableMaintenanceBusiness/WasteGrid/iCABSBWasteConsignmentNoteHistoryGrid.component';
import { WasteConsignmentNoteRangeTypelMaintenanceComponent } from '@internal/maintenance/iCABSBWasteConsignmentNoteRangeTypeMaintenance.component';
@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class WasteConsignRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}
@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: WasteConsignRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSBWASTECONSIGN_SUB, component: WasteConsignmentNoteRangeGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSBWASTECONSIGNNOTEHISTORYGRID, component: WasteConsignmentNoteHistoryGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSBWASTECONSIGNMENTNOTERANGETYPEMAINTENANCE_SUB, component: WasteConsignmentNoteRangeTypelMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
                ],
                data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        WasteConsignRootComponent,
        WasteConsignmentNoteRangeGridComponent,
        WasteConsignmentNoteHistoryGridComponent,
        WasteConsignmentNoteRangeTypelMaintenanceComponent
    ],
    exports: [
        WasteConsignmentNoteRangeGridComponent,
        WasteConsignmentNoteHistoryGridComponent,
        WasteConsignmentNoteRangeTypelMaintenanceComponent
    ],
    entryComponents: [
        WasteConsignmentNoteRangeGridComponent
    ]
})

export class WasteConsignComponent { }
