import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { RenewalExtractGenerationComponent } from './LettersAndLabels/iCABSRenewalExtractGeneration.component';
import { CustomerLetterByTypeComponent } from './LettersAndLabels/iCABSARCustomerLettersByTypeGrid.component';
import { BranchContractReportComponent } from './ContractAndJobReports/ContractForBranch/iCABSARBranchContractReport.component';
import { SalesStatsAdjustmentGridComponent } from './SalesAdjustments/StatsAdjustmentGrid/iCABSSSalesStatsAdjustmentGrid.component';
import { CustomerLettersForTypeGridComponent } from './LettersAndLabels/iCABSARCustomerLettersForTypeGrid.component';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class ReportsRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ReportsRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSRENEWALEXTRACTGENERATION_SUB, component: RenewalExtractGenerationComponent },
                    { path: ContractManagementModuleRoutes.ICABSCUSTOMERLETTERBYTYPE, component: CustomerLetterByTypeComponent },
                    { path: ContractManagementModuleRoutes.ICABSARBRANCHCONTRACTREPORT, component: BranchContractReportComponent },
                    { path: ContractManagementModuleRoutes.ICABSSSALESSTATSADJUSTMENTGRID_SUB, component: SalesStatsAdjustmentGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSARCUSTOMERLETTERSFORTYPEGRID_SUB, component: CustomerLettersForTypeGridComponent }

                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        ReportsRootComponent,
        RenewalExtractGenerationComponent,
        CustomerLetterByTypeComponent,
        BranchContractReportComponent,
        SalesStatsAdjustmentGridComponent,
        CustomerLettersForTypeGridComponent
    ],
    exports: [
        SalesStatsAdjustmentGridComponent,
        BranchContractReportComponent,
        RenewalExtractGenerationComponent,
        CustomerLetterByTypeComponent
    ],
    entryComponents: [
        BranchContractReportComponent,
        RenewalExtractGenerationComponent
    ]
})

export class ReportsModule { }
