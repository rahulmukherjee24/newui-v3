import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { ContractManagementModuleRoutes } from './../../base/PageRoutes';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../app/base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { BranchSearchComponent } from '../../../app/internal/search/iCABSBBranchSearch';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from './../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSARCustomerLettersByTypeGrid.html'
})

export class CustomerLetterByTypeComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    // URL Query Parameters
    private queryParams: any = {
        operation: 'ApplicationReport/iCABSARCustomerLettersByTypeGrid',
        module: 'letters',
        method: 'ccm/grid'
    };
    public pageId: string = '';
    public controls = [
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true },
        { name: 'ContractTypeCode', type: MntConst.eTypeText }
    ];

    //Branch Search
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };

    public validLetterCodes: Array<string> = ['01', '04', '10', '11', '17', '18', '19', '23', '24'];
    public gridConfig = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };

    public dropDownConfig = {
        ContractTypeCode: {
            ContractTypeCodeList: []
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCUSTOMERLETTERBYTYPE;
        this.browserTitle = this.pageTitle = 'Letters By Type';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onWindowLoad(): void {
        this.branchSearchDropDown.active = {
            id: this.utils.getBranchCode(),
            text: this.utils.getBranchText()
        };
        this.setControlValue('BranchNumber', this.branchSearchDropDown.active.id);
        this.setControlValue('BranchName', this.branchSearchDropDown.active.text);
        if (!this.isReturning()) {
            this.setControlValue('DateFrom', new Date());
            this.setControlValue('DateTo', this.getControlValue('DateFrom'));
        }
        this.getContractTypes();
    }

    public getContractTypes(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {};
        searchParams.set(this.serviceConstants.Action, '0');
        postData[this.serviceConstants.Function] = 'BuildContractTypeCombo';
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    if (data.ContractTypeList) {
                        let contractTypeCodes: Array<string> = data.ContractTypeList.split(',');
                        for (let i = 0; i < contractTypeCodes.length; i++) {
                            let contractType: any = contractTypeCodes[i].split('|');
                            this.dropDownConfig.ContractTypeCode.ContractTypeCodeList.push({ text: contractType[1], value: contractType[0] });
                            if (i === 0) {
                                this.setControlValue('ContractTypeCode', contractType[i]);
                            }
                        }
                    }
                    if (this.isReturning()) {
                        this.setControlValue('ContractTypeCode', this.pageParams.contractType);
                        this.gridConfig.currentPage = this.pageParams.gridCurrentPage;
                        this.isReturningFlag = false;
                    }
                    this.buildGrid();
                    this.populateGrid();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('LetterCode', 'LetterCode', 'LetterCode', MntConst.eTypeCode, 2, true);
        this.riGrid.AddColumn('LetterDesc', 'LetterDesc', 'LetterDesc', MntConst.eTypeText, 25, false);
        this.riGrid.AddColumn('Generated', 'Generated', 'Generated', MntConst.eTypeInteger, 2, false);
        this.riGrid.AddColumn('Printed', 'Printed', 'Printed', MntConst.eTypeInteger, 2, false);
        this.riGrid.AddColumn('UnPrinted', 'UnPrinted', 'UnPrinted', MntConst.eTypeInteger, 2, false);
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.getControlValue('DateFrom') && this.getControlValue('DateTo')) {
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
            let form: any = {};
            form['Level'] = 'Branch';
            form['BranchNumber'] = this.utils.getBranchCode();
            form['DateFrom'] = this.getControlValue('DateFrom');
            form['DateTo'] = this.getControlValue('DateTo');
            form['ContractFilterType'] = this.getControlValue('ContractTypeCode');
            form[this.serviceConstants.Function] = 'LettersByType';
            form[this.serviceConstants.GridMode] = '0';
            form[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
            form[this.serviceConstants.GridCacheRefresh] = true;
            form[this.serviceConstants.PageSize] = this.gridConfig.pageSize.toString();
            form[this.serviceConstants.PageCurrent] = this.gridConfig.currentPage.toString();
            form[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            form[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch, form)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            return;
                        }
                        this.riGrid.RefreshRequired();
                        this.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.gridConfig.currentPage);
                        }, 500);
                        this.riGridPagination.totalItems = this.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                        this.riGrid.Execute(data);
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public riGridBodyOnDblClick(event: any): void {
        this.buildLangDocumentList();
        this.serviceCoverFocus(event);
    }

    public buildLangDocumentList(): void {
        let searchParams: QueryParams;
        let postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        postData[this.serviceConstants.Function] = 'BuildLangDocumentList';
        postData['LetterTypeCode'] = this.riGrid.Details.GetValue('LetterCode');

        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation,
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    if (data.LangDocumentList) {
                        this.setAttribute('LangDocumentList', data.LangDocumentList);
                    }
                    if (data.MultiLanguageDocument) {
                        this.setAttribute('MultiLanguageDocument', data.MultiLanguageDocument);
                    }

                    if (this.riGrid.CurrentColumnName === 'LetterCode') {
                        let letterCode: string = this.riGrid.Details.GetValue('LetterCode');
                        if (this.validLetterCodes.indexOf(letterCode) > -1) {
                            this.pageParams.gridCurrentPage = this.gridConfig.currentPage;
                            this.navigate('', ContractManagementModuleRoutes.ICABSARCUSTOMERLETTERSFORTYPEGRID);
                        } else {
                            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }
    public serviceCoverFocus(event: any): void {
        if (!event.target.attributes.additionalProperty) {
            return;
        }
        this.setAttribute('Row', this.riGrid.CurrentRow);
        this.setAttribute('RowID', this.riGrid.Details.GetAttribute('LetterCode', 'RowID'));
        this.setAttribute('LetterTypeCode', this.riGrid.Details.GetValue('LetterCode'));
        this.setAttribute('LetterTypeDesc', this.riGrid.Details.GetValue('LetterDesc'));
        this.setAttribute('ContractType', this.getControlValue('ContractTypeCode'));
        this.pageParams.contractType = this.getControlValue('ContractTypeCode');

        let additionalData: string = event.target.attributes.additionalProperty.nodeValue;
        if (additionalData) {
            let additionalDataArray: any = additionalData.split(',');
            this.setAttribute('DocumentURL', additionalDataArray[0]);
            this.setAttribute('CrystalReport', additionalDataArray[1]);
            this.setAttribute('ConsumablesIndReqd', additionalDataArray[2]);
            this.setAttribute('ForegroundInd', additionalDataArray[3]);
        }
    }
}
