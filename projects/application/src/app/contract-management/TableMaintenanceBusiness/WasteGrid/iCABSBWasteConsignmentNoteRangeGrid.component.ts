import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceModuleRoutes } from '@base/PageRoutes';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { IExportOptions } from '@app/base/ExportConfig';
@Component({
    templateUrl: 'iCABSBWasteConsignmentNoteRangeGrid.html'
})

export class WasteConsignmentNoteRangeGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    public controls = [
        { name: 'BusinessCode', readonly: false, disabled: true, required: true, type: MntConst.eTypeCode, ignoreSubmit: false },
        { name: 'BusinessDesc', readonly: false, disabled: true, required: false, ignoreSubmit: true },
        { name: 'RangeActiveInd', readonly: false, disabled: false, required: false, value: true, ignoreSubmit: false },
        { name: 'menu', readonly: false, disabled: false, required: false, ignoreSubmit: true }
    ];
    public pageId: string = '';
    public xhrParams: any = {
        module: 'waste',
        method: 'service-delivery/grid',
        operation: 'Business/iCABSBWasteConsignmentNoteRangeGrid'
    };
    public expCfg: IExportOptions = {};

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBWASTECONSIGNMENTNOTERANGEGRID;
        this.browserTitle = 'Waste Congsignment Note Range Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Waste Consignment Note Range Details';
        this.pageParams.pageCurrent = 1;
        this.pageParams.totalRecords = 1;
    }

    ngAfterContentInit(): void {
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.DefaultTextColor = '0000FF';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.riGrid.FunctionUpdateSupport = true; // Updateable Grid;
        this.buildGrid();
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.windowOnLoad();
        }
        this.setControlValue('menu', 'Options');
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.setControlValue('BusinessCode', this.utils.getBusinessCode());
        this.disableControl('BusinessCode', true);
        this.setControlValue('BusinessDesc', this.utils.getBusinessText());
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('RegulatoryAuthorityNumber', 'WSNoteRange', 'RegulatoryAuthorityNumber', MntConst.eTypeInteger, 10, false);
        this.riGrid.AddColumnAlign('RegulatoryAuthorityNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('RegulatoryAuthorityNumber', true);
        this.riGrid.AddColumn('RegulatoryAuthorityName', 'WSNoteRange', 'RegulatoryAuthorityName', MntConst.eTypeText, 40, false);
        this.riGrid.AddColumn('ConsignmentNoteRangeTypeDesc', 'WSNoteRange', 'ConsignmentNoteRangeTypeDesc', MntConst.eTypeText, 40, false);
        this.riGrid.AddColumn('WasteTransferTypeDesc', 'WSNoteRange', 'WasteTransferTypeDesc', MntConst.eTypeText, 30, false);
        this.riGrid.AddColumn('BranchNumber', 'WSNoteRange', 'BranchNumber', MntConst.eTypeInteger, 15, false);
        this.riGrid.AddColumnAlign('BranchNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('BranchNumber', true);
        this.riGrid.AddColumn('BranchName', 'WSNoteRange', 'BranchName', MntConst.eTypeText, 14, false);
        this.riGrid.AddColumn('RangePrefix', 'WSNoteRange', 'RangePrefix', MntConst.eTypeText, 30, false);
        this.riGrid.AddColumnOrderable('RangePrefix', true);
        this.riGrid.AddColumn('RangeStart', 'WSNoteRange', 'RangeStart', MntConst.eTypeInteger, 9, false);
        this.riGrid.AddColumnAlign('RangeStart', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('RangeStart', true);
        this.riGrid.AddColumn('RangeEnd', 'WSNoteRange', 'RangeEnd', MntConst.eTypeInteger, 9, false);
        this.riGrid.AddColumnAlign('RangeEnd', MntConst.eAlignmentRight);
        this.riGrid.AddColumnOrderable('RangeEnd', true);
        this.riGrid.AddColumn('RangeNextNumber', 'WSNoteRange', 'RangeNextNumber', MntConst.eTypeInteger, 25, false);
        this.riGrid.AddColumnAlign('RangeNextNumber', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('RangeExpiryDate', 'WSNoteRange', 'RangeExpiryDate', MntConst.eTypeDate, 15, false);
        this.riGrid.AddColumnOrderable('RangeExpiryDate', true);
        this.riGrid.AddColumn('RangeActive', 'WSNoteRange', 'RangeActive', MntConst.eTypeImage, 15, false);
        this.riGrid.AddColumnOrderable('RangeActive', true);
        this.riGrid.Complete();
        this.expCfg.dateColumns = this.riGrid.getColumnIndexListFromFull(['RangeExpiryDate']);
    }

    private riGridBeforeExecute(): void {
        let search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('RangeActiveInd', this.riExchange.riInputElement.checked(this.uiForm, 'RangeActiveInd') ? 'True' : 'False');
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, (Math.floor(Math.random() * 900000) + 100000).toString());
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.PageCurrent, this.pageParams.pageCurrent.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, search)
            .subscribe(
                (data) => {
                    if (data) {
                        this.pageParams.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        if (data && data.errorMessage) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.riGrid.RefreshRequired();
                            if (this.riGrid.Update) {
                                this.riGrid.StartRow = this.getAttribute('Row');
                                this.riGrid.StartColumn = 0;
                                this.riGrid.RowID = this.getAttribute('RowID');
                                this.riGrid.UpdateHeader = false;
                                this.riGrid.UpdateBody = true;
                                this.riGrid.UpdateFooter = false;
                            }
                            this.riGrid.Execute(data);
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                error => {
                    this.pageParams.totalRecords = 1;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private gridFocus(rsrcElement: any): void {
        rsrcElement.select();
        this.setAttribute('Sequence', rsrcElement.value);
        this.setAttribute('RowID', rsrcElement.getAttribute('RowID'));
        this.setAttribute('Row', rsrcElement.parentElement.parentElement.parentElement.sectionRowIndex);
        rsrcElement.focus();
        rsrcElement.select();
    }

    public riGridAfterExecute(): void {
        if (!this.riGrid.Update) {
            if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children) { this.gridFocus(this.riGrid.HTMLGridBody.children[0].children[0].children[0].children[0]); }
        }
    }

    public tbodyOnDblclick(event: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'RegulatoryAuthorityNumber':
                this.gridFocus(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
                this.navigate('WCNoteRangeUpdate', InternalMaintenanceModuleRoutes.ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE,
                    {
                        RowID: this.getAttribute('RowID')
                    });
                break;
        }
    }

    public menuOnchange(): void {
        switch (this.getControlValue('menu')) {
            case 'Add':
                this.navigate('WCNoteRangeAdd', InternalMaintenanceModuleRoutes.ICABSBWASTECONSIGNMENTNOTERANGEMAINTENANCE);
                break;
        }
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public refresh(): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(event: any): void {
        if (this.pageParams.pageCurrent !== event.value) {
            this.pageParams.pageCurrent = event.value;
            this.riGridBeforeExecute();
        }
    }
}
