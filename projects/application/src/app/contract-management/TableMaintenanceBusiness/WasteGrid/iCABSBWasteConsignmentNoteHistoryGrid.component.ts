import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBWasteConsignmentNoteHistoryGrid.html'
})

export class WasteConsignmentNoteHistoryGridComponent extends LightBaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') riPagination: PaginationComponent;

    private riGridCache: string = 'false';

    public controls: IControls[] = [
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'DateFrom', required: false, disabled: false, type: MntConst.eTypeDate },
        { name: 'DateTo', required: false, disabled: false, type: MntConst.eTypeDate },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'ProductCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'Status', type: MntConst.eTypeText, value: 'All' },
        { name: 'parentMode', type: MntConst.eTypeText }
    ];
    public pageId: string = '';
    public parentMode: string = '';
    public bCode: string;
    public messageType: string;
    public xhrParams: Record<string, string> = {
        module: 'waste',
        method: 'service-delivery/maintenance',
        operation: 'Application/iCABSAWasteConsignmentNoteHistoryGrid'
    };
    private fetchReportURLParams: Record<string, string> = {
        action: '6',
        operation: 'Application/iCABSAWasteConsignmentNoteHistoryGrid'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBWASTECONSIGNMENTNOTEHISTORYGRID;
        this.browserTitle = 'Waste Congsignment Note History';
        this.pageTitle = 'Waste Consignment Note History';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.pageCurrent = 1;
        this.pageParams.totalRecords = 0;
        this.pageParams.pageSize = 10;
        this.pageParams.gridHandle = this.utils.gridHandle;
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (this.isReturning()) {
            this.parentMode = this.getControlValue('parentMode'); // from page cache

        } else {
            this.windowOnLoad();
        }
        this.parentMode = this.riExchange.getParentHTMLValue('parentMode');
        this.buildGrid();
        this.loadGrid();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        const date: Date = new Date();
        this.setControlValue('DateTo', new Date());
        this.setControlValue('DateFrom', new Date(date.setFullYear(date.getFullYear() - 1)));

        this.bCode = this.businessCode();
        switch (this.parentMode) {
            case 'Contract':
                this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                break;

            case 'Premise':
                this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
                this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName') || '');
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber') || '');
                this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                break;

            case 'Product':
                this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
                this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber') || '');
                this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName') || '');
                this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
                break;
        }
    }

    public onChangeStatus(e: Event): void {
        this.setControlValue('Status', e.target['value']);
        this.gridReset();
        this.pageParams.totalRecords = 0;
    }

    public gridReset(): void {
        this.riGrid.Clear();
        this.riGrid.RefreshRequired();
        this.buildGrid();
        this.pageParams.gridCurPage = 0;
        this.pageParams.totalRecords = 1;
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (this.parentMode === 'Contract') {
            this.riGrid.AddColumn('PremiseNumber', 'WasteConsignmentNoteHeader', 'PremiseNumber', MntConst.eTypeInteger, 7, false);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('PremiseName', 'WasteConsignmentNoteHeader', 'PremiseName', MntConst.eTypeInteger, 40, false);
            this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('WasteConsignmentNoteNumber', 'WasteConsignmentNoteHeader', 'WasteConsignmentNoteNumber', MntConst.eTypeText, 50, false);
        this.riGrid.AddColumnAlign('WasteConsignmentNoteNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('WasteTransferTypeDesc', 'WasteConsignmentNoteHeader', 'WasteTransferTypeDesc', MntConst.eTypeText, 50, false);
        this.riGrid.AddColumnAlign('WasteTransferTypeDesc', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProductionDate', 'WasteConsignmentNoteHeader', 'ProductionDate', MntConst.eTypeDate, 40, false);
        this.riGrid.AddColumnAlign('ProductionDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitDueDate', 'WasteConsignmentNoteHeader', 'VisitDueDate', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('VisitDueDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActualVisitDate', 'WasteConsignmentNoteHeader', 'ActualVisitDate', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('ActualVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeCode', 'WasteConsignmentNoteHeader', 'EmployeeCode', MntConst.eTypeCheckBox, 6, false);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeSurname', 'WasteConsignmentNoteHeader', 'EmployeeSurname', MntConst.eTypeText, 50, false);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('WasteConsignmentNoteStatusDesc', 'WasteConsignmentNoteHeader', 'WasteConsignmentNoteStatusDesc', MntConst.eTypeText, 50, false);
        this.riGrid.AddColumnAlign('WasteConsignmentNoteStatusDesc', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PaperworkReceived', 'WasteConsignmentNoteHeader', 'PaperworkReceived', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnAlign('PaperworkReceived', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ManualWasteConsignmentNote', 'WasteConsignmentNoteHeader', 'ManualWasteConsignmentNote', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnAlign('ManualWasteConsignmentNote', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PrintWasteConsignmentNote', 'WasteConsignmentNoteHeader', 'PrintWasteConsignmentNote', MntConst.eTypeImage, 1, false);
        this.riGrid.AddColumnAlign('PrintWasteConsignmentNote', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    private loadGrid(): void {
        let search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');

        const formData: Record<string, string> = {};

        formData[this.serviceConstants['ContractNumber']] = this.getControlValue('ContractNumber') || '';
        formData[this.serviceConstants['DateFrom']] = this.getControlValue('DateFrom');
        formData[this.serviceConstants['DateTo']] = this.getControlValue('DateTo');
        formData[this.serviceConstants['GridCacheRefresh']] = this.riGridCache;
        formData[this.serviceConstants['GridHandle']] = this.pageParams.gridHandle;
        formData[this.serviceConstants['GridHeaderClickedColumn']] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants['GridMode']] = '0';
        formData[this.serviceConstants['GridSortOrder']] = this.riGrid.SortOrder;
        formData[this.serviceConstants['Mode']] = this.parentMode;
        formData[this.serviceConstants['PageCurrent']] = this.pageParams.pageCurrent.toString();
        formData[this.serviceConstants['PageSize']] = this.pageParams.pageSize;
        formData[this.serviceConstants['Status']] = this.getControlValue('Status');
        if (this.parentMode === 'Premise') { formData['PremiseNumber'] = this.getControlValue('PremiseNumber'); }
        if (this.parentMode === 'Product') { formData['PremiseNumber'] = this.getControlValue('PremiseNumber'); formData['ProductCode'] = this.getControlValue('ProductCode'); }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData)
            .subscribe(
                (response) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                    if (response['hasError'] || response['errorMessage']) {
                        this.pageParams.totalRecords = 0;
                        this.pageParams.gridCurPage = 0;
                        this.displayMessage(response);
                    } else {
                        if (response && response['pageData']) {
                            this.riGrid.UpdateFooter = true;
                            this.riGrid.UpdateHeader = true;
                            this.pageParams.gridCurPage = response['pageData'] ? response['pageData'].pageNumber : 0;
                            this.pageParams.totalRecords = response['pageData'] ? response['pageData'].lastPageNumber * this.pageParams.pageSize : 0;

                            this.riGrid.RefreshRequired();
                            if (this.isReturning()) {
                                setTimeout(() => {
                                    this.riPagination.setPage(this.pageParams.gridCurPage);
                                }, 250);
                            }
                            this.riGrid.Execute(response);
                        } else {
                            this.displayMessage(response);
                        }
                    }
                },
                error => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.displayMessage(error);
                });
    }

    public refresh(): void {
        this.riGridCache = 'true';
        this.loadGrid();
    }

    public getCurrentPage(event: object): void {
        if (this.pageParams.pageCurrent !== event['value']) {
            this.pageParams.pageCurrent = event['value'];
            this.riGridCache = 'false';
            this.loadGrid();
        }
    }

    public tbodyOnDblclick(_val: Event): void {
        const colname: string = this.riGrid.Details.GetValue(this.riGrid.CurrentColumnName);
        let search = new QueryParams();

        search.set(this.serviceConstants.Action, this.fetchReportURLParams.action);
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set('WasteConsignmentNote', this.riGrid.Details.GetRowId(this.riGrid.CurrentColumnName, this.riGrid.CurrentRow));
        search.set('Function', 'Single');
        search.set(this.serviceConstants.CountryCode, this.countryCode());

        if (colname === 'sp') {  // 'sp' = print
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.fetchReportURLGET({ operation: this.fetchReportURLParams.operation, search: search })
                .then(_data => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                }).catch(error => {
                    this.displayMessage(error);
                });
        }
    }
}
