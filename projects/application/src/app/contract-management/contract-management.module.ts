import { HttpClientModule } from '@angular/common/http';
import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { MaintenanceTypeAComponent } from './ContractManagement/contract-maintenance-tabs/maintenance-type-a';
import { MaintenanceTypeBComponent } from './ContractManagement/contract-maintenance-tabs/maintenance-type-b';
import { MaintenanceTypeCComponent } from './ContractManagement/contract-maintenance-tabs/maintenance-type-c';
import { MaintenanceTypeDComponent } from './ContractManagement/contract-maintenance-tabs/maintenance-type-d';
import { MaintenanceTypeEComponent } from './ContractManagement/contract-maintenance-tabs/maintenance-type-e';

import { ContractMaintenanceComponent } from './ContractManagement/iCABSAContractMaintenance';
import { ContractInvoiceDetailGridComponent } from './ContractInvoiceDetailGrid/iCABSAContractInvoiceDetailGrid';
import { GeneralSearchGridComponent } from './generalSearch/iCABSCMGeneralSearchGrid';
import { ServiceCoverUnsuspendGridComponent } from './ServiceProcesses/DeliveryConfirmation/iCABSAServiceCoverUnsuspendGrid.component';
import { ContractSalesEmployeeReassignGridComponent } from './SalesAdjustments/NegEmployeeReassign/iCABSBContractSalesEmployeeReassignGrid.component';
import { ContractMaintenanceService } from './ContractManagement/ContractMaintenance.service';
import { ContractMaintenanceFieldService } from './ContractManagement/contract-maintenance-tabs/ContractMaintenanceField.service';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class ContractManagementRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ContractManagementRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSACONTRACTINVOICEDETAILGRID_SUB, component: ContractInvoiceDetailGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE_SUB, component: ContractMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE_SUB, component: ContractMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE_SUB, component: ContractMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSCMGENERALSEARCHGRID_SUB, component: GeneralSearchGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSASERVICECOVERUNSUSPENDGRID, component: ServiceCoverUnsuspendGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSBCONTRACTSALESEMPLOYEEREASSIGNGRID, component: ContractSalesEmployeeReassignGridComponent }
                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        ContractManagementRootComponent,
        ContractMaintenanceComponent,
        MaintenanceTypeAComponent,
        MaintenanceTypeBComponent,
        MaintenanceTypeCComponent,
        MaintenanceTypeDComponent,
        MaintenanceTypeEComponent,
        ContractInvoiceDetailGridComponent,
        GeneralSearchGridComponent,
        ServiceCoverUnsuspendGridComponent,
        ContractSalesEmployeeReassignGridComponent
    ],
    providers: [
        ContractMaintenanceService,
        ContractMaintenanceFieldService
    ],
    exports: [
        MaintenanceTypeAComponent,
        MaintenanceTypeBComponent,
        MaintenanceTypeCComponent,
        MaintenanceTypeDComponent,
        MaintenanceTypeEComponent,
        ContractInvoiceDetailGridComponent,
        ServiceCoverUnsuspendGridComponent,
        ContractSalesEmployeeReassignGridComponent
    ],
    entryComponents: [
        MaintenanceTypeAComponent,
        MaintenanceTypeBComponent,
        MaintenanceTypeCComponent,
        MaintenanceTypeDComponent,
        MaintenanceTypeEComponent,
        ServiceCoverUnsuspendGridComponent,
        ContractSalesEmployeeReassignGridComponent
    ]
})

export class ContractManagementModule { }
