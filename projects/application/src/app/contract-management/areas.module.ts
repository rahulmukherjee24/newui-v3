import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { PostcodeMaintenanceComponent } from './BranchGeography/PostalCode/iCABSBPostcodeMaintenance';
import { PostCodeGridComponent } from './BranchGeography/iCABSBPostcodesGrid';
import { SalesAreaGridComponent } from './BranchGeography/SalesArea/iCABSBSalesAreaGrid.component';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class AreasRootComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: AreasRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSBPOSTCODEMAINTENANCE_SUB, component: PostcodeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSBPOSTCODESGRID_SUB, component: PostCodeGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSBSALESAREAGRID_SUB, component: SalesAreaGridComponent }
                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        AreasRootComponent,
        PostcodeMaintenanceComponent,
        PostCodeGridComponent,
        SalesAreaGridComponent
    ]
})

export class AreasModule { }
