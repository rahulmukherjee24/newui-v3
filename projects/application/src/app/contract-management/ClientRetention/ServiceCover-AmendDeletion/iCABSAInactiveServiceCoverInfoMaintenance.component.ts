import { Component, OnInit, Injector, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { EmployeeSearchComponent } from './../../../internal/search/iCABSBEmployeeSearch';
import { ServiceCoverSearchComponent } from './../../../internal/search/iCABSAServiceCoverSearch';
import { LostBusinessDetailLanguageSearchComponent } from './../../../internal/search/iCABSBLostBusinessDetailLanguageSearch.component';
import { PremiseSearchComponent } from './../../../internal/search/iCABSAPremiseSearch';
import { ContractSearchComponent } from './../../../internal/search/iCABSAContractSearch';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { QueryParametersCallback } from './../../../base/Callback';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { ContractManagementModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from './../../../base/PageRoutes';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { RouteAwayComponent } from './../../../../shared/components/route-away/route-away';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAInactiveServiceCoverInfoMaintenance.html'
})

export class InactiveServiceCoverInfoMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy, QueryParametersCallback {
    @ViewChild('LostbusinessDetailDropDown') public lostbusinessDetailDropDown: LostBusinessDetailLanguageSearchComponent;
    @ViewChild('ContractNumber') public contractNumber: any;
    @ViewChild('ProductSearch') public productSearch: any;
    @ViewChild('routeAwayComponent') public routeAwayComponent: RouteAwayComponent;

    private isFetchMode: boolean = false;
    public isRecordSelected: boolean = false;
    private inactiveServiceCoverInfoStatus: string;
    public isReInstateOriginal: boolean;
    private functionName: string;
    private headerParams: any = {
        method: 'contract-management/maintenance',
        module: 'contract-admin',
        operation: 'Application/iCABSAInactiveServiceCoverInfoMaintenance'
    };

    public buttonCaption: string = 'Delete';
    public numberLab = 'Contract Number';

    public ellipsisConfig = {
        contract: {
            disabled: false,
            showHeader: true,
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'currentContractType': this.riExchange.getCurrentContractType(),
                'showAddNew': false
            },
            component: ContractSearchComponent
        },
        premise: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false
            },
            component: PremiseSearchComponent
        },
        product: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'Search',
                'ContractNumber': '',
                'PremiseNumber': '',
                'ContractName': '',
                'PremiseName': '',
                'ProductCode': '',
                'ProductDesc': ''
            },
            component: ServiceCoverSearchComponent
        },
        employee: {
            disabled: true,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp-Commission',
                'showAddNew': false
            },
            component: EmployeeSearchComponent
        }
    };

    public dropdownConfig: any = {
        lostBusiness: {
            isRequired: true,
            isDisabled: true,
            triggerValidate: false,
            inputParams: {
                'parentMode': 'LookUp-Active',
                'languageCode': this.riExchange.LanguageCode()
            },
            active: {
                id: '',
                text: ''
            }
        },
        lostBusinessDetail: {
            isRequired: true,
            isDisabled: true,
            triggerValidate: false,
            inputParams: {
                'parentMode': 'LookUp',
                'LostBusinessCode': '',
                'languageCode': this.riExchange.LanguageCode()
            },
            active: {
                id: '',
                text: ''
            }
        },
        visitNarrative: {
            isDisabled: true,
            inputParams: {
                operation: 'Business/iCABSBVisitNarrativeSearch',
                module: 'service',
                method: 'service-delivery/search'
            },
            displayFields: ['VisitNarrativeCode', 'VisitNarrativeDesc'],
            active: {
                id: '',
                text: ''
            }
        }
    };

    public pageId: string = '';
    public controls = [
        //main section
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true },
        { name: 'NegBranchNumber', disabled: true },
        { name: 'BranchName', disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true },
        { name: 'Status', disabled: true },
        //Part 1
        { name: 'ServiceVisitFrequency', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ServiceAnnualValue', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'ServiceQuantity', disabled: true, type: MntConst.eTypeInteger },
        { name: 'InvoiceAnnivDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'ServiceCommenceDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'InvoiceFrequencyCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'InvoiceFrequencyDesc', disabled: true },
        //Part 2
        { name: 'LostBusinessCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'LostBusinessDesc', disabled: true },
        { name: 'LostBusinessDetailCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'LostBusinessDetailDesc', disabled: true },
        { name: 'InactiveEffectDate', required: true, disabled: true, type: MntConst.eTypeDate },
        { name: 'CommissionEmployeeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'SalesEmployeeText', disabled: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'InactiveServiceCoverText', disabled: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'VisitNarrativeCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'VisitNarrativeDesc', disabled: true },
        { name: 'RemovalVisitText', disabled: true, type: MntConst.eTypeText, commonValidator: true },
        { name: 'CreateContact', disabled: true, type: MntConst.eTypeCheckBox },
        { name: 'CancelProRataChargeInd', disabled: true, type: MntConst.eTypeCheckBox },
        //hidden
        { name: 'ActionType' },
        { name: 'LostBusinessRequestNumber' },
        { name: 'ContractTypeCode' },
        { name: 'DetailRequiredInd', type: MntConst.eTypeCheckBox },
        { name: 'ErrorMessageDesc' },
        { name: 'TrialEnd' },
        { name: 'ProductUpgradeInd', type: MntConst.eTypeCheckBox },
        { name: 'ServiceCoverRowID' },
        { name: 'UninvoicedProRataExist', type: MntConst.eTypeCheckBox },
        { name: 'ReInstateOriginal' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAINACTIVESERVICECOVERINFOMAINTENANCE;
        this.setURLQueryParameters(this);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.isRecordSelected = true;
            this.setActiveLostBusiness();
            this.setActiveLostBusinessDetail();
            this.setActiveVisitNarrative();
            this.afterFetch();
            this.restoreFormData();
            this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
            this.ellipsisConfig.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
            this.ellipsisConfig.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
            this.ellipsisConfig.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
            this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
            this.ellipsisConfig.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
            this.ellipsisConfig.product.childConfigParams['ProductCode'] = this.getControlValue('ProductCode');

            this.dropdownConfig.lostBusinessDetail.inputParams.LostBusinessCode = this.getControlValue('LostBusinessCode');
            this.lostbusinessDetailDropDown.fetchDropDownData();
        } else {
            this.utils.getUserAccessType().subscribe((type) => {
                this.pageParams.userAccessType = type;
                this.pageParams.isLostBusinessVisible = true;
                this.pageParams.isEffectDateVisible = true;
                this.windowOnLoad();
                this.pageParams.pageFormData = [];
                this.pageParams.numberLab = 'Contract Number';
                this.pageParams.isBlankRowVisible = true;
                this.pageParams.isLostBusinessDetailVisible = false;
                this.pageParams.isTextVisible = true;
                this.pageParams.isContactVisible = true;
                this.pageParams.isVisitNarrativeVisible = true;
                this.pageParams.isRemovalVisitNotesVisible = true;
                this.pageParams.isServiceAnnualValueVisible = false;
                this.pageParams.isCancelProRataVisible = false;

                if (this.parentMode !== 'Contact-View' && this.parentMode !== 'Contact') {
                    this.ellipsisConfig.contract.autoOpen = true;
                }
            });
        }

        this.disableControl('ContractNumber', this.isParentModeContact);
        if (this.getControlValue('PremiseNumber')) {
            this.disableControl('PremiseNumber', this.isParentModeContact);
        }
        if (this.getControlValue('ProductCode')) {
            this.disableControl('ProductCode', this.isParentModeContact);
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public getURLQueryParameters(param: any): void {
        this.pageParams.actionType = param.actionType;
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
        this.ellipsisConfig.contract.childConfigParams.currentContractType = param.currentContractType;
    }

    private get isParentModeContact(): boolean {
        if (this.parentMode === 'Contact-View' || this.parentMode === 'Contact') return true;
        else return false;
    }

    private windowOnLoad(): void {
        this.isReInstateOriginal = false;
        this.pageParams.pageTitle = 'Service Cover Deletion Maintenance';
        this.setControlValue('ContractTypeCode', this.riExchange.getCurrentContractType());

        if (this.parentMode === 'Contact' || this.parentMode === 'Contact-View') {
            this.pageParams.cNumber = this.riExchange.getParentHTMLValue('ContractNumber');
            this.pageParams.cName = this.riExchange.getParentHTMLValue('ContractName');
            this.setControlValue('ContractNumber', this.pageParams.cNumber);
            this.setControlValue('ContractName', this.pageParams.cName);
            this.pageParams.pNumber = this.riExchange.getParentHTMLValue('PremiseNumber');
            this.pageParams.pName = this.riExchange.getParentHTMLValue('PremiseName');
            this.setControlValue('PremiseNumber', this.pageParams.pNumber);
            this.setControlValue('PremiseName', this.pageParams.pName);
            this.pageParams.pCode = this.riExchange.getParentHTMLValue('ProductCode');
            this.pageParams.pDesc = this.riExchange.getParentHTMLValue('ProductDesc');
            this.setControlValue('ProductCode', this.pageParams.pCode);
            this.setControlValue('ProductDesc', this.pageParams.pDesc);

            this.setControlValue('ServiceCoverRowID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
            this.setControlValue('LostBusinessRequestNumber', this.riExchange.getParentHTMLValue('LostBusinessRequestNumber'));
            this.setControlValue('TrialEnd', this.riExchange.getParentHTMLValue('TrialEnd')); //#14199

            this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
            this.ellipsisConfig.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
            this.ellipsisConfig.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
            this.ellipsisConfig.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
            this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
            this.ellipsisConfig.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
            this.ellipsisConfig.product.childConfigParams['ProductCode'] = this.getControlValue('ProductCode');

            this.checkParentMode();
        }

        if (this.pageParams.actionType === 'reinstate') {
            this.pageParams.pageTitle = this.pageParams.currentContractTypeLabel + ' Service Cover Reinstate Maintenance';
            this.pageParams.isLostBusinessVisible = false;
            this.pageParams.isTextVisible = false;
            this.pageParams.isContactVisible = false;
            this.pageParams.isVisitNarrativeVisible = false;
            this.pageParams.isRemovalVisitNotesVisible = false;
            this.setControlValue('ActionType', 'Reinstate');
        }

        if (this.pageParams.actionType === 'cancel') {
            this.pageParams.pageTitle = this.pageParams.currentContractTypeLabel + ' Service Cover Cancel Maintenance';
            this.pageParams.numberLab = this.pageParams.currentContractTypeLabel + ' Number';

            this.pageParams.isLostBusinessVisible = false;
            this.pageParams.isEffectDateVisible = false;
            this.setControlValue('ActionType', 'Cancel');
            this.pageParams.isContactVisible = false;
            this.setRequiredStatus('InactiveEffectDate', false);
        }
        this.utils.setTitle(this.pageParams.pageTitle);

        if (this.parentMode === 'Contact-View') {
            this.disableControls([]);
            this.dropdownConfig.lostBusiness.isDisabled = true;
            this.dropdownConfig.visitNarrative.isDisabled = true;
            this.isRecordSelected = true;
        }

        this.hideShowFields();
    }

    private beforeFetch(): void {
        if (this.getControlValue('ContractNumber')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            this.functionName = 'CheckContractType';

            let bodyParams: any = {
                ContractTypeCode: this.getControlValue('ContractTypeCode')
            };
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            bodyParams[this.serviceConstants.Function] = this.functionName;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    this.fetchRecord();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private fetchRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');

        searchParams.set(this.serviceConstants.Function, this.functionName);
        searchParams.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        searchParams.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        searchParams.set(this.serviceConstants.ProductCode, this.getControlValue('ProductCode'));
        searchParams.set(this.serviceConstants.ServiceCoverROWID, this.getControlValue('ServiceCoverRowID'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }

                for (let key in data) {
                    if (key) {
                        this.setControlValue(key, data[key]);
                    }
                }

                //this.setAttribute('ServiceCoverRowID', data['ServiceCover']);

                this.setActiveLostBusiness();
                this.setActiveLostBusinessDetail();
                this.onVisitNarrativeCodeChange();

                this.dropdownConfig.lostBusinessDetail.inputParams.LostBusinessCode = this.getControlValue('LostBusinessCode');
                this.lostbusinessDetailDropDown.fetchDropDownData();
                this.isFetchMode = true;
                this.isRecordSelected = true;
                this.lookupDescriptions();
                this.afterFetch();
                this.formPristine();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterFetch(): void {
        this.afterSave();
        this.displayFields();
        this.hideShowFields();
        this.enableDisableControls();
        this.beforeUpdateMode();
    }

    private lookupDescriptions(): void {
        this.ajaxSource.next(this.ajaxconstant.START);

        let lookupDetails: Array<any> = [{
            'table': 'Contract',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['ContractName', 'InvoiceAnnivDate', 'InvoiceFrequencyCode']
        },
        {
            'table': 'Premise',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'BusinessCode': this.utils.getBusinessCode(),
                'PremiseNumber': this.getControlValue('PremiseNumber')
            },
            'fields': ['PremiseName']
        },
        {
            'table': 'Product',
            'query': {
                'ProductCode': this.getControlValue('ProductCode'),
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['ProductDesc']
        },
        {
            'table': 'Branch',
            'query': {
                'BranchNumber': this.getControlValue('NegBranchNumber'),
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['BranchName']
        },
        {
            'table': 'Employee',
            'query': {
                'EmployeeCode': this.getControlValue('CommissionEmployeeCode'),
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['EmployeeSurname']
        },
        {
            'table': 'Employee',
            'query': {
                'EmployeeCode': this.getControlValue('VisitNarrativeCode'),
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['VisitNarrativeDesc']
        }
        ];

        this.LookUp.lookUpRecord(lookupDetails).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.length) {
                    let contract = data[0];
                    let premise = data[1];
                    let product = data[2];
                    let branch = data[3];
                    let employee = data[4];
                    let visit = data[4];

                    if (premise.length) {
                        this.pageParams.pName = premise[0].PremiseName;
                        this.setControlValue('PremiseName', premise[0].PremiseName);
                    }

                    if (product.length) this.setControlValue('ProductDesc', product[0].ProductDesc);

                    if (branch.length) this.setControlValue('BranchName', branch[0].BranchName);

                    if (employee.length) this.setControlValue('EmployeeSurname', employee[0].EmployeeSurname);

                    if (contract.length) {
                        this.pageParams.cName = contract[0].ContractName;
                        this.setControlValue('ContractName', contract[0].ContractName);
                        this.setControlValue('InvoiceAnnivDate', contract[0].InvoiceAnnivDate);
                        this.setControlValue('InvoiceFrequencyCode', contract[0].InvoiceFrequencyCode);
                        this.fetchInvoiceFrequencyDesc();
                    } else {
                        if (this.isFetchMode) {
                            this.isFetchMode = false;
                            this.storeFormData();
                        }
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private fetchInvoiceFrequencyDesc(): void {
        let invoiceDetails: Array<any> = [{
            'table': 'SystemInvoiceFrequency',
            'query': {
                'InvoiceFrequencyCode': this.getControlValue('InvoiceFrequencyCode'),
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['InvoiceFrequencyDesc']
        }];

        this.LookUp.lookUpRecord(invoiceDetails).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.length) {
                    let invoice = data[0];
                    if (invoice.length) {
                        this.setControlValue('InvoiceFrequencyDesc', invoice[0].InvoiceFrequencyDesc);
                    }
                    if (this.isFetchMode) {
                        this.isFetchMode = false;
                        this.storeFormData();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private hideShowFields(): void {
        if (this.pageParams.userAccessType === 'Full' || this.utils.getBranchCode() === this.getControlValue('NegBranchNumber')) {
            this.pageParams.isServiceAnnualValueVisible = true;
        } else {
            this.pageParams.isServiceAnnualValueVisible = false;
        }

        // Only show the 'Cancel Uninvoiced' when Uninvoiced charges can be found
        if (this.pageParams.actionType === 'cancel') {
            if (this.getControlValue('UninvoicedProRataExist')) {
                this.pageParams.isCancelProRataVisible = true;
            } else {
                this.pageParams.isCancelProRataVisible = false;
            }
        }
    }

    private enableDisableControls(): void {
        if (this.parentMode !== 'Contact-View') {
            let arrControl: Array<any> = ['LostBusinessDetailCode', 'InactiveEffectDate', 'CommissionEmployeeCode', 'SalesEmployeeText',
                'InactiveServiceCoverText', 'RemovalVisitText', 'CreateContact', 'CancelProRataChargeInd'];
            for (let key in arrControl) {
                if (key) {
                    this.disableControl(arrControl[key], !this.pageParams.isUpdateSupport);
                }
            }
            this.dropdownConfig.visitNarrative.isDisabled = !this.pageParams.isUpdateSupport;
            this.dropdownConfig.lostBusiness.isDisabled = !this.pageParams.isUpdateSupport;
            this.dropdownConfig.lostBusinessDetail.isDisabled = !this.pageParams.isUpdateSupport;
        }
    }

    private resetFields(fieldName: any): void {
        let arrNoResetControls: Array<any> = ['ActionType', 'LostBusinessRequestNumber', 'ContractTypeCode', 'DetailRequiredInd',
            'ErrorMessageDesc', 'TrialEnd', 'ProductUpgradeInd', 'UninvoicedProRataExist'];

        for (let key in this.controls) {
            if (key) {
                let controlName: string = this.controls[key]['name'];
                if (controlName === 'ContractTypeCode' || controlName === 'ActionType' || controlName === 'LostBusinessRequestNumber' || controlName === 'TrialEnd') continue;
                this.setControlValue(controlName, '');
            }
        }

        if (fieldName === 'contract') {
            this.setControlValue('ContractNumber', this.pageParams.cNumber);
            this.setControlValue('ContractName', this.pageParams.cName);
        } else if (fieldName === 'premise') {
            this.setControlValue('ContractNumber', this.pageParams.cNumber);
            this.setControlValue('ContractName', this.pageParams.cName);
            this.setControlValue('PremiseNumber', this.pageParams.pNumber);
            this.setControlValue('PremiseName', this.pageParams.pName);
        } else if (fieldName === 'product') {
            this.setControlValue('ContractNumber', this.pageParams.cNumber);
            this.setControlValue('ContractName', this.pageParams.cName);
            this.setControlValue('PremiseNumber', this.pageParams.pNumber);
            this.setControlValue('PremiseName', this.pageParams.pName);
            this.setControlValue('ProductCode', this.pageParams.pCode);
            this.setControlValue('ProductDesc', this.pageParams.pDesc);
        }
        this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
        this.ellipsisConfig.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
        this.ellipsisConfig.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
        this.ellipsisConfig.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
        this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
        this.ellipsisConfig.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
        this.ellipsisConfig.product.childConfigParams['ProductCode'] = this.getControlValue('ProductCode');

        this.dropdownConfig.lostBusiness.active = {
            id: '',
            text: ''
        };
        this.dropdownConfig.lostBusinessDetail.active = {
            id: '',
            text: ''
        };
        this.dropdownConfig.visitNarrative.active = {
            id: '',
            text: ''
        };
        this.pageParams.isUpdateSupport = false;
        this.isRecordSelected = false;
        this.dropdownConfig.lostBusiness.triggerValidate = false;
        this.dropdownConfig.lostBusinessDetail.triggerValidate = false;
        this.enableDisableControls();
        this.formPristine();
    }

    private setActiveVisitNarrative(): void {
        this.dropdownConfig.visitNarrative.active = {
            id: this.getControlValue('VisitNarrativeCode') ? this.getControlValue('VisitNarrativeCode') : '',
            text: this.getControlValue('VisitNarrativeCode') ? this.getControlValue('VisitNarrativeCode') + ' - ' + this.getControlValue('VisitNarrativeDesc') : ''
        };
    }
    private setActiveLostBusiness(): void {
        this.dropdownConfig.lostBusiness.active = {
            id: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') : '',
            text: this.getControlValue('LostBusinessCode') ? this.getControlValue('LostBusinessCode') + ' - ' + this.getControlValue('LostBusinessDesc') : ''
        };
    }

    private setActiveLostBusinessDetail(): void {
        this.dropdownConfig.lostBusinessDetail.active = {
            id: this.getControlValue('LostBusinessDetailCode') ? this.getControlValue('LostBusinessDetailCode') : '',
            text: this.getControlValue('LostBusinessDetailCode') ? this.getControlValue('LostBusinessDetailCode') + ' - ' + this.getControlValue('LostBusinessDetailDesc') : ''
        };
    }

    public createContactOnClick(): void {
        if (this.getControlValue('CreateContact') && !this.riExchange.riInputElement.isDisabled(this.uiForm, 'CreateContact')) {
            this.formPristine();
            this.navigate('New', InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1);
        }
    }

    public onContractChange(): void {
        this.pageParams.cName = '';
        this.pageParams.cNumber = '';
        this.pageParams.cNumber = this.getControlValue('ContractNumber');
        this.setControlValue('ContractName', '');
        this.resetFields('contract');
    }

    public onPremiseChange(): void {
        this.pageParams.pName = '';
        this.pageParams.pNumber = '';
        this.pageParams.pNumber = this.getControlValue('PremiseNumber');
        this.setControlValue('PremiseName', '');
        this.resetFields('premise');
    }

    public onProductChange(): void {
        this.pageParams.pCode = '';
        this.pageParams.pDesc = '';
        this.pageParams.pCode = this.getControlValue('ProductCode');
        this.setControlValue('ProductDesc', '');
        this.resetFields('product');
        this.productSearch.openModal();
    }

    public getEllipsisData(data: any, type: string): void {
        switch (type) {
            case 'contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.pageParams.cNumber = data.ContractNumber;
                this.pageParams.cName = data.ContractName;
                this.resetFields(type);
                break;
            case 'premise':
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.setControlValue('PremiseName', data.PremiseName);
                this.pageParams.pNumber = data.PremiseNumber;
                this.pageParams.pName = data.PremiseName;
                this.resetFields(type);
                break;
            case 'product':
                this.setControlValue('ProductCode', data.ProductCode);
                this.setControlValue('ProductDesc', data.ProductDesc);
                this.setControlValue('ServiceCoverRowID', data.row.ttServiceCover);
                this.pageParams.pCode = data.ProductCode;
                this.pageParams.pDesc = data.ProductDesc;
                this.beforeFetch();
                break;
            case 'employee':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'CommissionEmployeeCode');
                this.setControlValue('CommissionEmployeeCode', data.CommissionEmployeeCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                break;
        }
    }

    private checkParentMode(): void {
        if (this.parentMode === 'Contact') {
            if (!this.getControlValue('PremiseNumber') || !this.getControlValue('ProductCode')) {
                //Select Mode
            } else {
                this.beforeFetch();
            }
        }

        if (this.parentMode === 'Contact-View') {
            if (!this.getControlValue('PremiseNumber') || !this.getControlValue('ProductCode')) {
                this.getLostBusinessRequestDetails();
            }
            this.beforeFetch();
            this.pageParams.isUpdateSupport = false;
        }
    }

    private getLostBusinessRequestDetails(): void {
        this.functionName = 'GetLostBusinessRequestDetails';
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set(this.serviceConstants.Function, this.functionName);
        searchParams.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        if (this.getControlValue('PremiseNumber')) {
            searchParams.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.setControlValue('LostBusinessRequestNumber', data.LostBusinessRequestNumber);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private afterSave(): void {
        if (this.getControlValue('InactiveEffectDate')) {
            if (this.pageParams.actionType === 'reinstate') {
                this.inactiveServiceCoverInfoStatus = 'Reinst';
            } else {
                this.inactiveServiceCoverInfoStatus = 'Update';
            }
            this.pageParams.isUpdateSupport = true;
        }
        else {
            if (this.pageParams.actionType === 'cancel') {
                this.inactiveServiceCoverInfoStatus = 'Cancel';
                this.pageParams.isUpdateSupport = true;
            } else if (this.pageParams.actionType === 'reinstate') {
                this.pageParams.isUpdateSupport = false;
            } else {
                this.inactiveServiceCoverInfoStatus = 'Delete';
                this.pageParams.isUpdateSupport = true;
            }
        }
    }

    private beforeUpdateMode(): void {
        this.displayFields();
        if (this.pageParams.actionType !== 'cancel') {
            // 30/08/05 PG #14199 Set default effective date for trial service rejection

            if (this.parentMode === 'Contact') {
                this.setControlValue('InactiveEffectDate', this.getControlValue('TrialEnd')); // will be null if not rejected trial
            }
            if (this.getControlValue('InactiveEffectDate')) {
                this.disableControl('InactiveEffectDate', true);
            }
        }

        //If the button text is 'Update' then disable Commission Employee, as it cannot be updated.
        //Only when del, reinst or cancelling should you be able to update Commission Employee
        if (this.inactiveServiceCoverInfoStatus === 'Update') {
            this.disableControl('CommissionEmployeeCode', true);
            this.disableControl('SalesEmployeeText', true);
        }
    }

    private getReInstateOriginal(val: boolean): void {
        this.isReInstateOriginal = val;
        this.setControlValue('ReInstateOriginal', val);
        if (val) {
            this.confirmRecord();
        }
    }

    private confirmRecord(): void {
        this.lookupDescriptions();
        setTimeout(() => {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
        }, 1000);
    }

    private onAddServiceCoverConfirmCallBack(): void {
        if (this.parentMode === 'Contact') {
            setTimeout(() => {
                let addServiceCover: ICabsModalVO = new ICabsModalVO();
                addServiceCover.msg = MessageConstant.PageSpecificMessage.addServiceCoverDeletion;
                this.ellipsisConfig.contract.disabled = true;
                addServiceCover.confirmCallback = this.resetAddService.bind(this);
                addServiceCover.cancelCallback = this.disableForm.bind(this);
                addServiceCover.confirmLabel = 'Yes';
                addServiceCover.cancelLabel = 'No';
                this.modalAdvService.emitPrompt(addServiceCover);
            }, 0);
        }
    }

    private resetAddService(): void {
        this.resetFields('contract');
    }

    private disableForm(): void {
        this.ellipsisConfig.contract.disabled = true;
        this.ellipsisConfig.premise.disabled = true;
        this.ellipsisConfig.product.disabled = true;
        this.ellipsisConfig.employee.disabled = true;
        this.pageParams.isUpdateSupport = false;
        this.enableDisableControls();
        this.disableControls(['menu']);
    }

    private saveRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        let bodyParams: any = {};

        searchParams.set(this.serviceConstants.Action, '2');
        bodyParams[this.serviceConstants.Function] = this.functionName;

        let cancelControls: Array<any> = ['InactiveServiceCoverText', 'VisitNarrativeCode', 'RemovalVisitText'];
        let reinstateControls: Array<any> = ['InactiveEffectDate'];
        let deleteControls: Array<any> = ['LostBusinessCode', 'LostBusinessDetailCode', 'InactiveEffectDate', 'InactiveServiceCoverText', 'VisitNarrativeCode', 'RemovalVisitText'];

        let checkBoxList: Array<any> = ['CancelProRataChargeInd', 'CreateContact', 'DetailRequiredInd', 'ProductUpgradeInd', 'UninvoicedProRataExist'];
        let saveControls: Array<any> = [
            'CommissionEmployeeCode', 'ActionType', 'LostBusinessRequestNumber', 'ContractTypeCode',
            'ErrorMessageDesc', 'ServiceVisitFrequency', 'ServiceQuantity', 'ServiceAnnualValue', 'ServiceCommenceDate', 'NegBranchNumber', 'SalesEmployeeText',
            'CreateContact', 'UninvoicedProRataExist', 'CancelProRataChargeInd', 'ProductUpgradeInd', 'ServiceCoverRowID', 'ContractNumber', 'PremiseNumber',
            'ProductCode', 'ReInstateOriginal'];

        let arr: Array<any> = [];
        if (this.pageParams.actionType === 'cancel') {
            arr = cancelControls;
        } else if (this.pageParams.actionType === 'reinstate') {
            arr = reinstateControls;
        } else {
            arr = deleteControls;
        }

        for (let i = 0; i < arr.length; i++) {
            saveControls.push(arr[i]);
        }

        for (let i = 0; i < saveControls.length; i++) {
            if (checkBoxList.indexOf(saveControls[i]) > -1) {
                bodyParams[saveControls[i]] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue(saveControls[i]));
            } else {
                bodyParams[saveControls[i]] = this.getControlValue(saveControls[i]);
            }
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.errorMessage && data.fullError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    if (data.errorNumber === 2029) {
                        this.disableForm();
                    }
                    return;
                }
                let messageVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                messageVO.closeCallback = this.onAddServiceCoverConfirmCallBack.bind(this);
                this.modalAdvService.emitMessage(messageVO);
                for (let key in data) {
                    if (key) {
                        this.setControlValue(key, data[key]);
                    }
                }
                this.formPristine();
                this.storeFormData();
                this.afterSave();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private displayFields(): void {
        let isDisplayFields: boolean = this.getControlValue('DetailRequiredInd') && this.pageParams.isLostBusinessVisible;
        if (isDisplayFields) {
            this.pageParams.isLostBusinessDetailVisible = true;
            this.pageParams.isBlankRowVisible = false;
            if (this.parentMode === 'Contact') {
                this.pageParams.isContactVisible = false;
            } else {
                this.pageParams.isContactVisible = true;
            }
            this.pageParams.isLostBusinessDetailReq = true;
            this.dropdownConfig.lostBusinessDetail.isRequired = true;
            this.setRequiredStatus('LostBusinessDetailCode', true);
        } else {
            this.pageParams.isLostBusinessDetailVisible = false;
            this.pageParams.isBlankRowVisible = true;
            this.pageParams.isContactVisible = false;
            this.pageParams.isLostBusinessDetailReq = false;
            this.dropdownConfig.lostBusinessDetail.isRequired = false;
            this.setControlValue('LostBusinessDetailCode', '');
            this.setRequiredStatus('LostBusinessDetailCode', false);
        }
    }

    private onVisitNarrativeCodeChange(): void {
        if (this.getControlValue('VisitNarrativeCode')) {
            this.functionName = 'GetVisitNarrativeDesc';
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');

            let bodyParams: any = {
                VisitNarrativeCode: this.getControlValue('VisitNarrativeCode')
            };
            bodyParams[this.serviceConstants.Function] = this.functionName;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    this.setControlValue('VisitNarrativeDesc', data.VisitNarrativeDesc);
                    this.setActiveVisitNarrative();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private onLostBusinessCodeChange(): void {
        if (this.getControlValue('LostBusinessCode')) {
            this.functionName = 'LostBusinessDetailRequired';
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            searchParams.set(this.serviceConstants.Function, this.functionName);
            searchParams.set('LostBusinessCode', this.getControlValue('LostBusinessCode'));

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    this.setControlValue('DetailRequiredInd', data.DetailRequiredInd);
                    this.displayFields();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('LostBusinessDetailCode', '');
            this.setControlValue('LostBusinessDetailDesc', '');
        }
    }

    private storeFormData(): void {
        for (let control in this.controls) {
            if (control) {
                let controlName: string = this.controls[control]['name'];
                this.pageParams.pageFormData[controlName] = this.getControlValue(controlName);
            }
        }
    }

    private restoreFormData(): void {
        for (let control in this.controls) {
            if (control) {
                let controlName: string = this.controls[control]['name'];
                this.setControlValue(controlName, this.pageParams.pageFormData[controlName]);
            }
        }

        this.setActiveLostBusiness();
        this.setActiveLostBusinessDetail();
        this.setActiveVisitNarrative();
    }

    public onVisitNarrativeDataReceived(data: any): void {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'VisitNarrativeCode');
        this.setControlValue('VisitNarrativeCode', data.VisitNarrativeCode);
        this.setControlValue('VisitNarrativeDesc', data.VisitNarrativeDesc);
    }

    public onLostBusinessChanged(data: any): void {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'LostBusinessCode');
        this.setControlValue('LostBusinessCode', data['LostBusinessLang.LostBusinessCode']);
        this.setControlValue('LostBusinessDesc', data['LostBusinessLang.LostBusinessDesc']);
        this.setControlValue('LostBusinessDetailCode', '');
        this.setControlValue('LostBusinessDetailDesc', '');
        this.setActiveLostBusinessDetail();
        this.onLostBusinessCodeChange();

        this.dropdownConfig.lostBusinessDetail.inputParams.LostBusinessCode = data['LostBusinessLang.LostBusinessCode'];
        this.lostbusinessDetailDropDown.fetchDropDownData();
    }

    public onLostBusinessDetailChanged(data: any): void {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'LostBusinessDetailCode');
        this.setControlValue('LostBusinessDetailCode', data['LostBusinessDetailLang.LostBusinessDetailCode']);
        this.setControlValue('LostBusinessDetailDesc', data['LostBusinessDetailLang.LostBusinessDetailDesc']);
    }

    public onOptionChange(data: string): void {
        if (this.isRecordSelected) {
            switch (data) {
                case 'ServiceCover':
                    this.navigate('Request', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE,
                        {
                            ServiceCover: this.getControlValue('ServiceCoverRowID')
                        });
                    break;
            }
        }
        else {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.NoRecordSelected));
        }
    }

    public onInactiveEffectDateChange(): void {
        if (this.getControlValue('InactiveEffectDate')) {
            this.functionName = 'WarnAnniversaryDate';
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let bodyParams: any = {
                InactiveEffectDate: this.getControlValue('InactiveEffectDate')
            };
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            bodyParams[this.serviceConstants.Function] = this.functionName;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onSaveClick(): void {
        this.dropdownConfig.lostBusiness.triggerValidate = true;
        this.dropdownConfig.lostBusinessDetail.triggerValidate = true;
        if (!this.riExchange.validateForm(this.uiForm)) return;
        if (this.pageParams.actionType === 'cancel') {
            this.functionName = 'WarnCancel';
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            searchParams.set(this.serviceConstants.Function, this.functionName);

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);

                    if (data.fullError && data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }

                    if (data.ErrorMessageDesc !== '') {
                        let modalObj: ICabsModalVO = new ICabsModalVO(data.ErrorMessageDesc);
                        modalObj.closeCallback = this.confirmRecord.bind(this);
                        this.modalAdvService.emitMessage(modalObj);
                    }

                    if (this.getControlValue('ProductUpgradeInd')) {
                        let modalObj: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.reinstateServiceCover);
                        modalObj.confirmCallback = this.getReInstateOriginal.bind(this, true);
                        modalObj.cancelCallback = this.getReInstateOriginal.bind(this, false);
                        this.modalAdvService.emitPrompt(modalObj);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.confirmRecord();
        }
    }

    public onCancelClick(): void {
        this.dropdownConfig.lostBusiness.triggerValidate = false;
        this.dropdownConfig.lostBusinessDetail.triggerValidate = false;
        this.restoreFormData();
        this.hideShowFields();
        this.formPristine();
    }
}
