import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContractManagementModuleRoutes } from '@base/PageRoutes';
import { HttpClientModule } from '@angular/common/http';

import { InternalSearchEllipsisModule } from '@internal/search-ellipsis.module';
import { InternalSearchModule } from '@internal/search.module';
import { MultiPremisePurchaseOrderAmendComponent } from './ContractPremisesManagement/iCABSAMultiPremisePurchaseOrderAmend.component';
import { MultiPremiseSpecialComponent } from './ContractServiceCoverMaintenance/SpecialInstructions/iCABSAMultiPremiseSpecial.component';
import { PostcodeMoveBranchComponent } from './ContractPremisesManagement/iCABSAPostcodeMoveBranch';
import { PremiseContactChangeGridComponent } from './PDAReturns/iCABSSePremiseContactChangeGrid';
import { RouteAwayGuardService } from '@shared/services/route-away-guard.service';
import { SearchEllipsisBusinessModule } from '@internal/search-ellipsis-business.module';
import { SearchEllipsisDropdownModule } from '@internal/search-ellipsis-dropdown.module';
import { SharedModule } from '@shared/shared.module';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class PremisesAdminRootComponent {
    constructor(viewContainerRef: ViewContainerRef) { }
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchEllipsisModule,
        InternalSearchModule,
        SearchEllipsisBusinessModule,
        SearchEllipsisDropdownModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: PremisesAdminRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSSEPREMISECONTACTCHANGEGRID_SUB, component: PremiseContactChangeGridComponent },
                    { path: ContractManagementModuleRoutes.ICABSAPOSTCODEMOVEBRANCH_SUB, component: PostcodeMoveBranchComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: ContractManagementModuleRoutes.ICABSAMULTIPREMISEPURCHASEORDERAMEND_SUB, component: MultiPremisePurchaseOrderAmendComponent },
                    { path: ContractManagementModuleRoutes.ICABSAMULTIPREMISESPECIAL_SUB, component: MultiPremiseSpecialComponent }

                ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        MultiPremisePurchaseOrderAmendComponent,
        MultiPremiseSpecialComponent,
        PostcodeMoveBranchComponent,
        PremiseContactChangeGridComponent,
        PremisesAdminRootComponent
    ],
    exports: [
        MultiPremisePurchaseOrderAmendComponent,
        MultiPremiseSpecialComponent
    ],
    entryComponents: [
        MultiPremiseSpecialComponent
    ]
})

export class PremiseAdminModule { }
