import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, EventEmitter, OnDestroy } from '@angular/core';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { ContractManagementModuleRoutes, InternalMaintenanceSalesModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from './../../../../app/base/PageRoutes';
import { ContractSearchComponent } from './../../../../app/internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from './../../../../app/internal/search/iCABSAPremiseSearch';
import { ServiceCoverSearchComponent } from './../../../../app/internal/search/iCABSAServiceCoverSearch';

@Component({
    templateUrl: 'iCABSAServiceCoverUnsuspendGrid.html'
})

export class ServiceCoverUnsuspendGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private urlParams: any = {
        module: 'suspension',
        method: 'contract-management/maintenance',
        operation: 'Application/iCABSAServiceCoverUnsuspendGrid'
    };

    public setFocusOnContractNumber = new EventEmitter<boolean>();
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: false, type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: false, type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceVisitFrequency', disabled: true, type: MntConst.eTypeInteger },
        { name: 'DateFrom', disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'DateTo', disabled: false, required: true, type: MntConst.eTypeDate },
        // hidden Controls
        { name: 'ServiceCover' },
        { name: 'ContractRowID' },
        { name: 'PremiseRowID' },
        { name: 'ServiceCoverRowID' },
        { name: 'ServiceVisitRowID' }
    ];
    public ellipsis: any = {
        contractNumberSearch: {
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ContractSearchComponent,
            isShowHeader: true,
            isDisabled: false
        },
        premiseNumberSearch: {
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: PremiseSearchComponent,
            isShowHeader: true,
            isDisabled: false
        },
        productSearch: {
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'LookUp-Freq'
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: ServiceCoverSearchComponent,
            isShowHeader: true,
            isDisabled: false
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSASERVICECOVERUNSUSPENDGRID;
        this.browserTitle = this.pageTitle = 'Delivery Confirmation';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
            this.refresh();
            this.setEllipsisConfigParams();
        } else {
            this.pageParams.curPage = 1;
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private setCurrentContractType(): void {
        this.pageParams.currentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.currentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
    }

    private updateDatePicker(date: any): any {
        let getDate: any = false;
        if (date) {
            getDate = this.globalize.parseDateToFixedFormat(date);
            getDate = this.globalize.parseDateStringToDate(getDate);
        }
        return getDate;
    }

    private windowOnload(): void {
        this.setCurrentContractType();

        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.buildGrid();

        this.setControlValue('DateFrom', this.updateDatePicker(new Date(new Date().getFullYear(), 0, 1)));
        this.setControlValue('DateTo', this.updateDatePicker(new Date()));

        this.setFocusOnContractNumber.emit(true);

        this.getDateMessageStrings();
    }

    private getDateMessageStrings(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'GetDateString'
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.urlParams.method, this.urlParams.module, this.urlParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.pageParams.messageText = data.MessageText;
                this.pageParams.msgDateTitle = data.MsgDateTitle;
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private unsuspendGridData(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'Unsuspend',
            ServiceVisitRowID: this.getAttribute('ServiceVisitRowID') || ''
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.urlParams.method, this.urlParams.module, this.urlParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.riGridBeforeExecute();
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    private setEllipsisConfigParams(): void {
        this.ellipsis.premiseNumberSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.premiseNumberSearch.childConfigParams.ContractName = this.getControlValue('ContractName');
        this.ellipsis.productSearch.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.productSearch.childConfigParams.ContractName = this.getControlValue('ContractName');
        this.ellipsis.productSearch.childConfigParams.PremiseNumber = this.getControlValue('PremiseNumber');
        this.ellipsis.productSearch.childConfigParams.PremiseName = this.getControlValue('PremiseName');
        // Fix for IUI-18339
        this.ellipsis.productSearch.childConfigParams.ProductCode = this.getControlValue('ProductCode');
        this.ellipsis.productSearch.childConfigParams.ProductDesc = this.getControlValue('ProductDesc');
    }

    /*##############################################################
    # Grid Routines
    ############################################################## */

    private buildGrid(): void {
        this.pageParams.totalRecords = 0;
        this.pageParams.pageSize = 10;

        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'ServiceCoverGrid', 'ContractNumber', MntConst.eTypeCode, 11, true);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'ServiceCoverGrid', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseName', 'ServiceCoverGrid', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Postcode', 'ServiceCoverGrid', 'Postcode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductCode', 'ServiceCoverGrid', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCoverGrid', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'ServiceCoverGrid', 'VisitTypeCode', MntConst.eTypeCode, 3, true);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractCommenceDate', 'ServiceCoverGrid', 'ContractCommenceDate', MntConst.eTypeDate, 10, false);
        this.riGrid.AddColumnAlign('ContractCommenceDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceDateStart', 'ServiceCoverGrid', 'ServiceDateStart', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceCommenceDate', 'ServiceCoverGrid', 'ServiceCommenceDate', MntConst.eTypeDate, 10, true);
        this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Unsuspend', 'ServiceCoverGrid', 'Unsuspend', MntConst.eTypeImage, 3, true);
        this.riGrid.AddColumnAlign('Unsuspend', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('ServiceDateStart', true);

        this.riGrid.Complete();
    }

    private riExchangeUpdateHTMLDocument(): void {
        this.riGrid.Update = false;
        this.riGridBeforeExecute();
    }

    private riGridBeforeExecute(): void {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set('DateFrom', this.getControlValue('DateFrom') || '');
        gridQueryParams.set('DateTo', this.getControlValue('DateTo') || '');
        gridQueryParams.set('BranchNumber', this.utils.getBranchCode() || '');
        gridQueryParams.set('ContractNumber', this.getControlValue('ContractNumber') || '');
        gridQueryParams.set('PremiseNumber', this.getControlValue('PremiseNumber') || '');
        gridQueryParams.set('ProductCode', this.getControlValue('ProductCode') || '');
        gridQueryParams.set('ServiceCoverRowID', this.riGrid.Update ? this.getAttribute('ServiceCoverRowID') : null);
        gridQueryParams.set(this.serviceConstants.PageSize, this.pageParams.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
        gridQueryParams.set(this.serviceConstants.GridMode, '0');
        gridQueryParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridQueryParams.set(this.serviceConstants.GridCacheRefresh, 'true');
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridQueryParams.set('riHTMLPage', 'Application/iCABSAServiceCoverUnsuspendGrid.htm');
        gridQueryParams.set('riSortOrder', this.riGrid.SortOrder);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.urlParams.method, this.urlParams.module, this.urlParams.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.riGrid.ResetGrid();
                } else {
                    this.pageParams.curPage = data.pageData.pageNumber || 1;
                    this.pageParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                    this.riGrid.RefreshRequired();
                    if (this.riGrid.Update) {
                        this.riGrid.StartRow = this.getAttribute('Row');
                        this.riGrid.StartColumn = 0;
                        this.riGrid.RowID = this.getAttribute('ServiceCoverRowID');
                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                    }
                    this.riGrid.Execute(data);
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private serviceCoverFocus(rsrcElement: any): void {
        let oTR: any = rsrcElement.parentElement.parentElement.parentElement;
        rsrcElement.focus();
        this.setAttribute('Row', oTR.sectionRowIndex);
        this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'RowID'));
        this.setControlValue('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'RowID'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'RowID'));
        this.setControlValue('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'RowID'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setControlValue('ServiceCover', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setControlValue('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setAttribute('ContractNumberServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setAttribute('ServiceVisitRowID', this.riGrid.Details.GetAttribute('VisitTypeCode', 'RowID'));
        this.setControlValue('ServiceVisitRowID', this.riGrid.Details.GetAttribute('VisitTypeCode', 'RowID'));
        this.pageParams.ContractNumber = this.riGrid.Details.GetValue('ContractNumber').substr(2, 8);

        switch (this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty')) {
            case 'C':
                this.pageParams.currentContractTypeURLParameter = '';
                break;
            case 'J':
                this.pageParams.currentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.pageParams.currentContractTypeURLParameter = '<product>';
                break;
        }
    }

    private confirm(obj: Object): void {
        this.unsuspendGridData();
    }

    public getCurrentPage(event: Object): void {
        this.pageParams.curPage = event['value'];
        this.riExchangeUpdateHTMLDocument();
    }

    public refresh(): void {
        if (this.pageParams.curPage <= 0) this.pageParams.curPage = 1;
        this.riExchangeUpdateHTMLDocument();
    }

    public riGridSort(event: Object): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public riGridBodyOnClick(event: Object): void {
        if (this.riGrid.CurrentColumnName === 'Unsuspend') {
            this.serviceCoverFocus(event['srcElement']);
            //Prompt the user if they should change the service commence date
            if (this.riGrid.Details.GetValue('ServiceCommenceDate') !== this.riGrid.Details.GetValue('ServiceDateStart')) {
                let modalVO: ICabsModalVO = new ICabsModalVO(this.pageParams.messageText, this.pageParams.msgDateTitle, this.confirm.bind(this));
                this.modalAdvService.emitPrompt(modalVO);
            } else {
                // Fix for IUI-18338
                this.unsuspendGridData();
            }
        }
    }
    public riGridBodyOnDblClick(event: Object): void {
        this.serviceCoverFocus(event['srcElement']);
        if (this.utils.hasClass(event['srcElement'], 'pointer')) {
            switch (this.riGrid.CurrentColumnName) {
                case 'ContractNumber':
                    this.navigate('Release', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                        ContractRowID: this.getControlValue('ContractRowID'),
                        ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
                    });
                    break;
                case 'PremiseNumber':
                    this.navigate('Release', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                        ContractRowID: this.getControlValue('ContractRowID'),
                        PremiseRowID: this.getControlValue('PremiseRowID'),
                        ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
                    });
                    break;
                case 'ProductCode':
                    this.navigate('Release', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                        ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
                    });
                    break;
                case 'VisitTypeCode':
                    this.navigate('Release', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                        ServiceVisitRowID: this.getControlValue('ServiceVisitRowID'),
                        ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
                    });
                    break;
                case 'ServiceCommenceDate':
                    this.navigate('Release', InternalMaintenanceApplicationModuleRoutes.ICABSASERVICECOVERCOMMENCEDATEMAINTENANCE, {
                        CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                        ServiceCoverRowID: this.getControlValue('ServiceCoverRowID'),
                        ContractNumberServiceCoverRowID: this.getControlValue('ServiceCover'),
                        ServiceVisitRowID: this.getControlValue('ServiceVisitRowID')
                    });
                    break;
                case 'ContractCommenceDate':
                    this.navigate('Release', InternalMaintenanceSalesModuleRoutes.ICABSACONTRACTCOMMENCEDATEMAINTENANCE, {
                        CurrentContractType: this.pageParams.currentContractTypeURLParameter ? this.pageParams.currentContractTypeURLParameter : 'C',
                        ContractNumber: this.pageParams.ContractNumber,
                        ContractRowID: this.getControlValue('ContractRowID'),
                        ServiceVisitRowID: this.getControlValue('ServiceVisitRowID')
                    });
                    break;
            }
        }
    }
    public riGridBodyOnKeyDown(event: any): void {
        let cellindex: number = event.srcElement.parentElement.parentElement.cellIndex;
        let rowIndex: number = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
        let dataLength: number = this.riGrid.HTMLGridBody.children.length;
        if (this.utils.hasClass(event.srcElement, 'pointer')) {
            switch (event.keyCode) {
                case 38: //Up Arror
                    if ((rowIndex > 0) && (rowIndex < dataLength)
                        && this.riGrid.CurrentHTMLRow.previousSibling
                        && this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex]
                        && this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex].children[0]
                        && this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex].children[0].children[0]) {
                        this.serviceCoverFocus(this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex].children[0].children[0]);
                    }
                    break;
                case 40:
                case 9: // Down Arror Or Tab
                    if ((rowIndex >= 0) && (rowIndex < dataLength - 1)
                        && this.riGrid.CurrentHTMLRow.nextSibling
                        && this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex]
                        && this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex].children[0]
                        && this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex].children[0].children[0]) {
                        this.serviceCoverFocus(this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex].children[0].children[0]);
                    }
                    break;
            }
        }
    }

    public contractNumberOnReceive(data: Object): void {
        if (data) {
            this.setControlValue('ContractNumber', data['ContractNumber']);
            this.setControlValue('ContractName', data['ContractName']);
            this.contractNumberFormatOnChange();
            this.setEllipsisConfigParams();
        }
    }
    public premiseNumberOnReceive(data: Object): void {
        if (data) {
            this.setControlValue('PremiseNumber', data['PremiseNumber']);
            this.setControlValue('PremiseName', data['PremiseName']);
            this.setEllipsisConfigParams();
            this.populateDescriptions();
        }
    }
    public productCodeOnReceive(data: Object): void {
        // Fix for IUI-18339
        this.setAttribute('ServiceCoverRowID', '');
        if (data) {
            this.setControlValue('ProductCode', data['ProductCode']);
            this.setControlValue('ProductDesc', data['ProductDesc']);
            this.setControlValue('ServiceCoverRowID', data['row']['ttServiceCover']);
            this.setControlValue('ServiceCover', this.getControlValue('ServiceCoverRowID'));
            this.setAttribute('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
            this.populateDescriptions();
        }
    }

    public contractNumberFormatOnChange(): void {
        // Fix for IUI-18341
        if (this.getControlValue('ContractNumber')) this.setControlValue('ContractNumber', this.utils.numberPadding(this.getControlValue('ContractNumber'), 8));
        this.populateDescriptions();
    }
    // Fix for IUI-18339
    public productCodeOnChnage(): void {
        this.setAttribute('ServiceCoverRowID', '');
        this.populateDescriptions();
    }
    // Fix for IUI-18339
    public premiseNumberOnChnage(): void {
        this.populateDescriptions();
    }

    public populateDescriptions(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');

        let postData: Object = {
            Function: 'SetDisplayFields'
        };
        // Fix for IUI-18339
        if (this.getControlValue('ContractNumber')) postData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        if (this.getControlValue('PremiseNumber')) postData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        if (this.getControlValue('ProductCode')) postData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        if (this.getAttribute('ServiceCoverRowID')) postData['ServiceCoverRowID'] = this.getAttribute('ServiceCoverRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.urlParams.method, this.urlParams.module, this.urlParams.operation, search, postData).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                this.setControlValue('ContractName', data.ContractName || '');
                this.setControlValue('PremiseName', data.PremiseName || '');
                this.setControlValue('ProductDesc', data.ProductDesc || '');
                if (!data.ContractName) this.setControlValue('ContractNumber', '');
                if (!data.PremiseName) this.setControlValue('PremiseNumber', '');
                if (!data.ProductDesc) this.setControlValue('ProductCode', '');
                if (data.ServiceVisitFrequency === '0') this.setControlValue('ServiceVisitFrequency', '');
                else this.setControlValue('ServiceVisitFrequency', data.ServiceVisitFrequency);
                this.setEllipsisConfigParams();
                // Fix for IUI-18333
                // this.buildGrid();
                this.riGrid.RefreshRequired();
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
        });
    }

    public dateFromOnChange(value: any): void {
        this.riExchange.riInputElement.SetValue(this.uiForm, 'DateFrom', this.updateDatePicker(value.value));
    }
    public dateToOnChange(value: any): void {
        this.riExchange.riInputElement.SetValue(this.uiForm, 'DateTo', this.updateDatePicker(value.value));
    }
}
