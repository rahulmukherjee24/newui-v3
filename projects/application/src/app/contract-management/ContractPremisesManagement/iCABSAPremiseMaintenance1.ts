import { PremiseMaintenanceComponent } from './iCABSAPremiseMaintenance';
import { PremiseMaintenance0 } from './iCABSAPremiseMaintenance0';
import { PremiseMaintenance2 } from './iCABSAPremiseMaintenance2';
import { PremiseMaintenance3 } from './iCABSAPremiseMaintenance3';

import { RiMaintenance, MntConst, RiTab } from './../../../shared/services/riMaintenancehelper';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from '../../../shared/services/utility';
import { MessageConstant } from './../../../shared/constants/message.constant';

export class PremiseMaintenance1 {
    //Duplicated Parent Class objects
    public utils: Utils;
    private xhr: any;
    private xhrParams: any;
    private uiForm: any;
    private controls: any;
    private uiDisplay: any;
    private pageParams: any;
    private attributes: any;
    private formData: any;
    private LookUp: any;
    private logger: any;
    private riExchange: RiExchange;
    private riMaintenance: RiMaintenance;
    private riTab: RiTab;
    private viewChild: any;

    public pgPM0: PremiseMaintenance0;
    public pgPM1: PremiseMaintenance1;
    public pgPM2: PremiseMaintenance2;
    public pgPM3: PremiseMaintenance3;

    constructor(private parent: PremiseMaintenanceComponent) {
        this.utils = this.parent.utils;
        this.logger = this.parent.logger;
        this.xhr = this.parent.xhr;
        this.xhrParams = this.parent.xhrParams;
        this.LookUp = this.parent.LookUp;
        this.uiForm = this.parent.uiForm;
        this.controls = this.parent.controls;
        this.uiDisplay = this.parent.uiDisplay;
        this.viewChild = this.parent.viewChild;
        this.pageParams = this.parent.pageParams;
        this.attributes = this.parent.attributes;
        this.formData = this.parent.formData;
        this.riExchange = this.parent.riExchange;
        this.riMaintenance = this.parent.riMaintenance;
        this.riTab = this.parent.riTab;
    }

    public window_onload(): void {
        this.pgPM0 = this.parent.pgPM0;
        this.pgPM1 = this.parent.pgPM1;
        this.pgPM2 = this.parent.pgPM2;
        this.pgPM3 = this.parent.pgPM3;
        this.renderPrimaryFields();
    }

    public DeterminePostCodeDefaulting(pcdf_blnEnablePostCodeDefaulting: boolean): boolean {
        if (pcdf_blnEnablePostCodeDefaulting) { //this.pageParams.vEnablePostcodeDefaulting
            return (this.pageParams.excludedBranches.indexOf(this.utils.getBranchCode().toString()) === -1);
        }
    }

    public ZeroPadInt(i: number, numDigits: number): string {
        let str = '00000' + i;
        return str.slice(-numDigits);
    }

    public renderPrimaryFields(): void {
        this.logger.log('PremiseMnt -renderPrimaryFields',
            this.pageParams, this.formData, this.pageParams.ParentMode,
            'Mode:', this.riMaintenance.CurrentMode, this.parent.isReturning());

        if (!this.parent.isReturning()) {
            if (this.parent.CBBupdated) {
                this.parent.CBBupdated = false;
                this.parent.updateButton(); //Update UI buttons with text
            } else {
                switch (this.pageParams.ParentMode.trim()) {
                    case 'CSearch':
                    case 'CSearchAdd':
                    case 'IGSearch':
                    case 'Contract':
                    case 'Contract-Add':
                    case 'Contact':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'ContractName', 'value', this.riExchange.getParentHTMLValue('ContractName'));
                        break;
                    case 'ProductSalesDelivery':
                    case 'CallCentreSearch':
                    case 'WorkOrderMaintenance':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseNumber', 'value', this.riExchange.getParentHTMLValue('PremiseNumber'));
                        break;
                    case 'ServiceCover':
                    case 'LoadByKeyFields':
                    case 'PortfolioGeneralMaintenance':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'ContractName', 'value', this.riExchange.getParentHTMLValue('ContractName'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseNumber', 'value', this.riExchange.getParentHTMLValue('PremiseNumber'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseName', 'value', this.riExchange.getParentHTMLValue('PremiseName'));
                        break;
                    case 'Request':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'ContractName', 'value', this.riExchange.getParentHTMLValue('ContractName'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseNumber', 'value', this.riExchange.getParentHTMLValue('PremiseNumber'));
                        this.uiDisplay.thControl = false;
                        break;
                }
                switch (this.pageParams.ParentMode.trim()) {
                    case 'CSearch':
                    case 'Contract':
                    case 'Contract-Add':
                    case 'Contact':
                        this.pageParams.blnIgnore = true;
                        this.riMaintenance.AddMode();
                        this.pageParams.blnIgnore = false;
                        break;
                    case 'ContactMedium':
                    case 'GeneralSearch':
                    case 'AccountPremiseSearch':
                    case 'StockEstimatesSearch':
                    case 'TreatmentcardRecallSearch':
                        this.riMaintenance.RowID(this, 'Premise', this.riExchange.getParentAttributeValue('PremiseRowID'));
                        this.riExchange.updateCtrl(this.controls, 'Premise', 'value', this.riExchange.getParentHTMLValue('PremiseRowID'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseRowID', 'value', this.riExchange.getParentHTMLValue('PremiseRowID'));
                        this.riMaintenance.FetchRecord();
                        break;
                    case 'CSearchAdd':
                        this.parent.focusField('PremiseCommenceDate', true);
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseCommenceDate', false);
                        break;
                    case 'AddFromPremise':
                        this.riMaintenance.RowID(this, 'Premise', this.riExchange.getParentAttributeValue('NewPremiseRowID'));
                        this.riExchange.updateCtrl(this.controls, 'Premise', 'value', this.riExchange.getParentHTMLValue('NewPremiseRowID'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseRowID', 'value', this.riExchange.getParentHTMLValue('NewPremiseRowID'));
                        this.riExchange.updateCtrl(this.controls, 'NewPremiseRowID', 'value', this.riExchange.getParentHTMLValue('NewPremiseRowID'));
                        this.riMaintenance.FetchRecord();
                        this.riMaintenance.UpdateMode();
                        break;
                    case 'GridSearch':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'ContractName', 'value', this.riExchange.getParentHTMLValue('ContractName'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseNumber', 'value', this.riExchange.getParentHTMLValue('PremiseNumber'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseName', 'value', this.riExchange.getParentHTMLValue('PremiseName'));
                        this.riMaintenance.FetchRecord();
                        break;
                    case 'GeneralSearchProduct':
                    case 'GridCopySearch':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'GridPremiseNumber', 'value', this.riExchange.getParentHTMLValue('PremiseName'));
                        this.riMaintenance.FetchRecord();
                        break;
                    case 'CallCentreSearch':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseNumber', 'value', this.riExchange.getParentHTMLValue('PremiseNumber'));
                        this.riMaintenance.FetchRecord();
                        break;
                    case 'GridSearchAdd':
                        this.riExchange.updateCtrl(this.controls, 'ContractNumber', 'value', this.riExchange.getParentHTMLValue('ContractNumber'));
                        this.riExchange.updateCtrl(this.controls, 'ContractName', 'value', this.riExchange.getParentHTMLValue('ContractName'));
                        this.riMaintenance.AddMode();
                        this.parent.focusField('PremiseCommenceDate', true);
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PremiseCommenceDate', false);
                        break;
                    default:
                        this.logger.log('Mode Unknown', this.pageParams.ParentMode.trim());
                }
                switch (this.pageParams.ParentMode.trim()) {
                    case 'ServicePlanning':
                    case 'AreaReallocation':
                    case 'ServiceAreaSequence':
                    case 'GroupServiceVisit':
                    case 'VisitDateDiscrepancy':
                    case 'ServiceVisitHistory':
                    case 'ExchangesDue':
                    case 'InstallationsCommence':
                    case 'PortfolioNoTurnover':
                    case 'ServiceStatsAdjust':
                    case 'CancelledVisit':
                    case 'VisitRejection':
                    case 'SuspendServiceandInvoice':
                    case 'TechServiceVisit':
                    case 'DOWSentricon':
                    case 'PDAReconciliation':
                    case 'ContractExpiryGrid':
                    case 'ContractPOExpiryGrid':
                    case 'RepeatSales':
                    case 'AdditionalVisit':
                    case 'Release':
                    case 'Summary':
                    case 'ProRataCharge':
                    case 'ServiceVisitEntryGrid':
                    case 'ServiceValueEntryGrid':
                    case 'Accept':
                    case 'PremiseMatch':
                    case 'GridCopySearch':
                    case 'Bonus':
                    case 'LostBusinessAnalysis':
                    case 'InvoiceReleased':
                    case 'StaticVisit':
                    case 'PortfolioReports':
                    case 'ServiceVisitWorkIndex':
                    case 'RetainedServiceCovers':
                    case 'TechWorkSummary':
                    case 'ClientRetention':
                    case 'ComReqPlan':
                    case 'CustomerCCOReport':
                    case 'Entitlement':
                    case 'Verification':
                    case 'DespatchGrid':
                        this.riMaintenance.RowID(this, 'Premise', this.riExchange.getParentAttributeValue('PremiseRowID')); //TODO
                        this.riExchange.updateCtrl(this.controls, 'Premise', 'value', this.riExchange.getParentAttributeValue('PremiseRowID'));
                        this.riExchange.updateCtrl(this.controls, 'PremiseRowID', 'value', this.riExchange.getParentAttributeValue('PremiseRowID'));
                        break;
                }
            }
        } else {
            for (let i in this.formData) {
                if (i) {
                    this.riExchange.updateCtrl(this.controls, i, 'value', this.formData[i]);
                }
            }
        }

        this.parent.updateButton();

        //Disable primary fields based on various condition
        if (!this.pageParams.shouldOpen) {
            if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                this.parent.disablePremiseNumber();
            } else {
                this.parent.enablePremiseNumber();
            }
        }

        this.SetSCVariables();
        this.SetHTMLPageSettings();
        this.parent.pgPM2.AddTabs();
        this.parent.pgPM2.BuildMenuOptions();
        this.riExchange.renderForm(this.uiForm, this.controls);
        if (this.parent.pageParams.shouldOpen) {
            this.parent.isRequestingInitial = false;
            setTimeout(() => { this.parent.contractNumberEllipsis.openModal(); }, 200);
            this.pageParams.shouldOpen = false;
        }
        this.init();
    }

    public init(): void {
        this.logger.log('INIT inside PM1');
        this.parent.setControlValue('ContractTypeCode', this.pageParams.CurrentContractType);
        this.logger.log(this.riMaintenance.CurrentMode, '|', 'ParentMode', this.pageParams.ParentMode, '|',
            'ContractNumber', this.parent.getControlValue('ContractNumber'), '|',
            'PremiseNumber', this.parent.getControlValue('PremiseNumber'), '|',
            'PremiseRowID', this.parent.getControlValue('PremiseRowID'), '|',
            'ContractTypeCode', this.parent.getControlValue('ContractTypeCode'), '|',
            'CurrentContractType', this.pageParams.CurrentContractType
        );

        //Check if ContractNo & PremiseNo is not blank
        if (((this.riMaintenance.CurrentMode === MntConst.eModeAdd) && (this.parent.getControlValue('ContractNumber') === '') && (this.parent.getControlValue('PremiseRowID') === ''))
            || ((this.riMaintenance.CurrentMode === MntConst.eModeUpdate)
                && (this.parent.getControlValue('ContractNumber') === '' || this.parent.getControlValue('PremiseNumber') === '')
                && (this.parent.getControlValue('PremiseRowID') === ''))) {
            this.parent.initialFormState(true, false);
            return;
        }

        this.logger.log('-------------------------------------');
        this.parent.isRequestingInitial = true;
        this.parent.overrideOpenFieldCR();
        this.parent.setControlValue('ContractTypeCode', this.pageParams.CurrentContractType);
        this.parent.enableControls(); //Enable Basic Control
        this.parent.primaryFieldsEnableDisable();
        this.parent.setFormMode(this.parent.c_s_MODE_SELECT);
        this.parent.lErrorMessageDesc = [];

        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.pgPM3.TermiteServiceCheck();
        }

        this.pageParams.initialVtxGeocodeVal = '';
        this.riExchange.updateCtrl(this.controls, 'cmdGeocode', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'cmdVtxGeoCode', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'SelRoutingSource', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'SelPrintRequired', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'SelServiceNotifyTemplateEmail', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'SelServiceNotifyTemplateSMS', 'disabled', true);

        this.riMaintenance.clear();
        this.riMaintenance.BusinessObject = 'iCABSFindPropertyCareBranch.p';
        this.riMaintenance.PostDataAdd('Action', '6', MntConst.eTypeInteger);
        this.riMaintenance.PostDataAdd('Function', 'Request', MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeText);
        this.riMaintenance.PostDataAdd('BranchNumber', this.utils.getBranchCode(), MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('PropertyCareInd', MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('AllowUpdateInd', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.pageParams.boolPropertyCareInd = data['PropertyCareInd'];
            this.pageParams.boolAllowUpdateInd = data['AllowUpdateInd'];
        }, 'POST');

        this.riMaintenance.BusinessObject = 'iCABSCustomerInfoFunctions.p';
        this.riMaintenance.clear();
        this.riMaintenance.PostDataAdd('BusinessCode', this.utils.getBusinessCode(), MntConst.eTypeCode);
        this.riMaintenance.PostDataAdd('Mode', 'CheckBranchUserRights', MntConst.eTypeText);
        this.riMaintenance.ReturnDataAdd('WriteAccess', MntConst.eTypeText);
        this.riMaintenance.Execute(this, function (data: any): any {
            this.pageParams.boolUserWriteAccess = data['WriteAccess'];
        }, 'POST', 0, false);

        this.uiDisplay.tdCustomerInfo = false;

        this.pageParams.GridCacheTime = (new Date()).toTimeString().split(' ')[0];
        setTimeout(() => {
            this.parent.pgPM3.BuildSRAGrid();
        }, 500);
        this.riExchange.updateCtrl(this.controls, 'cmdSRAGenerateText', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'selCustomerIndicationNumber', 'disabled', true);

        this.uiDisplay.trPurchaseOrderDetails = (!this.pageParams.glSCPORefsAtServiceCover);

        this.riExchange.updateCtrl(this.controls, 'CallLogID', 'value', this.riExchange.getParentHTMLValue('CurrentCallLogID'));
        this.riExchange.updateCtrl(this.controls, 'CurrentCallLogID', 'value', this.riExchange.getParentHTMLValue('CurrentCallLogID'));
        this.riExchange.updateCtrl(this.controls, 'ClosedWithChanges', 'value', this.riExchange.getParentHTMLValue('ClosedWithChanges'));
        this.riExchange.updateCtrl(this.controls, 'EmployeeLimitChildDrillOptions', 'value', this.riExchange.getParentHTMLValue('EmployeeLimitChildDrillOptions'));

        if (!(this.pageParams.SCEnableHopewiserPAF || this.pageParams.SCEnableDatabasePAF || this.pageParams.SCEnableMarktSelect)) {
            this.uiDisplay.cmdGetAddress = false;
        }

        this.riExchange.updateCtrl(this.controls, 'RunningReadOnly', 'value', this.riExchange.getParentHTMLValue('RunningReadOnly'));

        switch (this.pageParams.CurrentContractType) {
            case 'J':
            case 'P':
                this.uiDisplay.labelDiscountCode = false;
                this.uiDisplay.DiscountCode = false;
                this.uiDisplay.DiscountDesc = false;
                this.uiDisplay.tdCustomerInfo = false;
                break;
        }

        this.pageParams.vSICCodeEnable = !!this.pageParams.vSICCodeEnable;
        this.pageParams.vSICCodeRequire = !!this.pageParams.vSICCodeRequire;

        this.uiDisplay.trSICCode = this.pageParams.vSICCodeEnable;

        this.pageParams.vbEnableRouteOptimisation = !!this.pageParams.vEnableRouteOptimisation;
        this.pageParams.vbEnablePremiseLinking = !!this.pageParams.vEnablePremiseLinking;
        this.pageParams.vbEnableAssociatedPremise = !!this.pageParams.vEnableAssociatedPremise;

        this.uiDisplay.trVehicleTypeNumber = this.pageParams.vbEnableRouteOptimisation;
        this.uiDisplay.cmdGeocode = this.pageParams.vbEnableRouteOptimisation;
        this.uiDisplay.tdGeonode = this.pageParams.vbEnableRouteOptimisation;
        this.uiDisplay.tdGPSScore = this.pageParams.vbEnableRouteOptimisation;

        this.uiDisplay.trEnableLinkedPremises = this.pageParams.vbEnablePremiseLinking;
        this.uiDisplay.trEnableAssociatedPremises = this.pageParams.vbEnableAssociatedPremise;

        this.uiDisplay.tdspanContractPostCode = !this.pageParams.SCHidePostCode;
        this.uiDisplay.tdContractPostCode = !this.pageParams.SCHidePostCode;
        this.uiDisplay.trRegulatoryAuthorityNumber = this.pageParams.vbEnableRegulatoryAuthority;

        this.uiDisplay.trMatchContract = this.pageParams.SCEnableRepeatSalesMatching;
        this.uiDisplay.trMatchPremise = this.pageParams.SCEnableRepeatSalesMatching;

        this.uiDisplay.trVtxGeoCode = this.pageParams.vbVtxGeoCode;
        this.uiDisplay.tdOutsideCityLimits = this.pageParams.vbVtxGeoCode;
        this.uiDisplay.tdOutsideCityLimitslab = this.pageParams.vbVtxGeoCode;

        this.uiDisplay.labelInactiveEffectDate = false;
        this.uiDisplay.InactiveEffectDate = false;
        this.uiDisplay.AnyPendingBelow = false;

        this.riExchange.updateCtrl(this.controls, 'cmdGetAddress', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'cmdCopyPremise', 'disabled', true);
        this.riExchange.updateCtrl(this.controls, 'cmdVtxGeoCode', 'disabled', true);

        this.riMaintenance.BusinessObject = 'riControl.p';
        this.riMaintenance.CustomBusinessObject = 'iCABSPremiseEntry.p';
        this.riMaintenance.CustomBusinessObjectSelect = true;
        this.riMaintenance.CustomBusinessObjectConfirm = false;
        this.riMaintenance.CustomBusinessObjectInsert = true;
        this.riMaintenance.CustomBusinessObjectUpdate = true;
        this.riMaintenance.CustomBusinessObjectDelete = false;
        this.riMaintenance.FunctionSnapShot = false;
        this.riMaintenance.FunctionDelete = false;
        this.riMaintenance.DisplayMessages = true;

        if (this.pageParams.ParentMode === 'ProductSalesDelivery') {
            this.uiDisplay.grdPremiseMaintenanceControl = false;
        }


        if (this.pageParams.ParentMode === 'CSearch' || this.pageParams.ParentMode === 'CSearchAdd' || this.pageParams.ParentMode === 'IGSearch') {
            this.riMaintenance.FunctionSearch = false;
        }

        if ((this.pageParams.ParentMode === 'ServiceCover')
            || (this.pageParams.ParentMode === 'CallCentreSearch')
            || (this.riExchange.URLParameterContains('pgm'))) {
            this.riMaintenance.FunctionAdd = false;
            this.riMaintenance.FunctionSelect = false;
        }

        this.riExchange.renderForm(this.uiForm, this.controls);

        this.riMaintenance.AddTable('Premise');
        switch (this.pageParams.ParentMode) {
            case 'CSearch':
            case 'CSearchAdd':
            case 'IGSearch':
            case 'Contract':
            case 'Contract-Add':
            case 'Contact':
            case 'GridSearch':
            case 'GridSearchAdd':
            case 'ServiceCover':
                this.riMaintenance.AddTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddTableKey('ContractNumber', MntConst.eTypeCode, MntConst.eKeyOptionNormal, MntConst.eKeyStateFixed, 'Key');
                break;
            default:
                this.riMaintenance.AddTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddTableKey('ContractNumber', MntConst.eTypeCode, MntConst.eKeyOptionNormal, MntConst.eKeyStateNormal, 'Key');
        }

        this.riMaintenance.AddTableKeyAlignment('ContractNumber', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableKey('PremiseNumber', MntConst.eTypeAutoNumber, MntConst.eKeyOptionNormal, MntConst.eKeyStateNormal, 'Key');
        this.riMaintenance.AddTableKey('PremiseRowID', MntConst.eTypeAutoNumber, MntConst.eKeyOptionNormal, MntConst.eKeyStateNormal, 'Key');

        this.riMaintenance.AddTableField('NationalAccount', MntConst.eTypeCheckBox, MntConst.eFieldOptionRequried, MntConst.eFieldStateReadOnly, 'ReadOnly');

        this.riMaintenance.AddTableField('AccountNumber', MntConst.eTypeText, MntConst.eFieldOptionRequired, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldAlignment('AccountNumber', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('PremiseCommenceDate', MntConst.eTypeDate, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableFieldAlignment('PremiseCommenceDate', MntConst.eAlignmentCenter);

        let addrFieldType = MntConst.eTypeText;
        if (this.pageParams.SCCapitalFirstLtr) {
            addrFieldType = MntConst.eTypeTextFree;
        }

        this.riMaintenance.AddTableField('PremiseName', addrFieldType, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableField('PremiseAddressLine1', addrFieldType, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableField('PremiseAddressLine2', addrFieldType, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseAddressLine3', addrFieldType, (this.pageParams.SCAddressLine3Logical ? MntConst.eFieldOptionRequired : MntConst.eFieldOptionNormal), MntConst.eFieldStateNormal, this.pageParams.SCAddressLine3Logical ? 'Required' : 'Optional');
        this.riMaintenance.AddTableField('PremiseAddressLine4', addrFieldType, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableField('PremiseAddressLine5', addrFieldType, (this.pageParams.SCAddressLine5Logical ? MntConst.eFieldOptionRequired : MntConst.eFieldOptionNormal), MntConst.eFieldStateNormal, this.pageParams.SCAddressLine5Logical ? 'Required' : 'Optional');
        this.riMaintenance.AddTableField('PremiseContactName', addrFieldType, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableField('PremiseContactPosition', addrFieldType, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableField('PremiseContactDepartment', addrFieldType, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('PremisePostcode', MntConst.eTypeCode, (this.pageParams.SCHidePostCode ? MntConst.eFieldOptionNormal : MntConst.eFieldOptionRequried), MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('DrivingChargeValue', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Required');

        this.riMaintenance.AddTableField('PremiseVtxGeoCode', MntConst.eTypeText, (this.pageParams.vbVtxGeoCode ? MntConst.eFieldOptionRequired : MntConst.eFieldOptionNormal), MntConst.eFieldStateNormal, 'Required');
        this.riMaintenance.AddTableField('OutsideCityLimits', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('PremiseContactTelephone', MntConst.eTypeText, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseContactMobile', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseContactFax', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseContactEmail', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('InvoiceGroupNumber', MntConst.eTypeInteger, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Lookup');
        this.riMaintenance.AddTableField('DiscountCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Lookup');
        this.riMaintenance.AddTableFieldAlignment('DiscountCode', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('ServiceBranchNumber', MntConst.eTypeInteger, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Lookup');
        this.riMaintenance.AddTableField('CustomerTypeCode', MntConst.eTypeCode, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableFieldAlignment('CustomerTypeCode', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('SalesAreaCode', MntConst.eTypeCode, MntConst.eFieldOptionRequried, MntConst.eFieldStateNormal, 'LookUp');
        this.riMaintenance.AddTableFieldAlignment('SalesAreaCode', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('PremiseSalesEmployee', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldAlignment('PremiseSalesEmployee', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('PurchaseOrderNo', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PurchaseOrderLineNo', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PurchaseOrderExpiryDate', MntConst.eTypeDate, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ClientReference', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PrintRequired', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseDirectoryName', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseDirectoryPage', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseDirectoryGridRef', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseSpecialInstructions', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseSRADate', MntConst.eTypeDate, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseSRAEmployee', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'LookUp');
        this.riMaintenance.AddTableFieldAlignment('PremiseSRAEmployee', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('LostBusinessRequestNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('InvoiceSuspendInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('InvoiceSuspendInd', false);
        this.riMaintenance.AddTableField('InvoiceSuspendText', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('InvoiceSuspendText', false);
        this.riMaintenance.AddTableField('RetainServiceWeekdayInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ServiceReceiptRequired', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ProofOfServiceRequired', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PNOL', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PNOLSiteRef', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'Optional');
        this.riMaintenance.AddTableField('PNOLiCABSLevel', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PNOLDescription', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('PNOLEffectiveDate', MntConst.eTypeDate, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ContractHasExpired', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PNOLUpliftAmount', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PNOLSetupChargeToApply', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PNOLEffectiveDefault', MntConst.eTypeDate, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('StoreType', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldOptionNormal, 'Optional');
        this.riMaintenance.AddTableField('StoreNumber', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldOptionNormal, 'Optional');
        this.riMaintenance.AddTableField('origPNOL', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('origPNOLiCABSLevel', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('origPNOLEffectiveDate', MntConst.eTypeDate, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('origPNOLSiteRef', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'Optional');
        this.riMaintenance.AddTableField('GPSCoordinateX', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('GPSCoordinateY', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('RoutingGeonode', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('RoutingScore', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('RoutingSource', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseFixedServiceTime', MntConst.eTypeTime, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('VehicleTypeNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('LanguageCode', MntConst.eTypeCode, MntConst.eFieldOptionRequired, MntConst.eFieldStateNormal, 'Lookup');
        this.riMaintenance.AddTableField('PremiseRegNumber', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseReference', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('WasteConsignmentNoteExemptInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('WasteConsignmentNoteExpiryDate', MntConst.eTypeDate, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('WasteRegulatoryPremiseRef', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('NextWasteConsignmentNoteNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        if (this.pageParams.vSICCodeEnable) {
            this.riMaintenance.AddTableField('SICCode', MntConst.eTypeCode, MntConst.eFieldOptionRequired, MntConst.eFieldStateNormal, 'Lookup');
            this.riMaintenance.AddTableFieldAlignment('SICCode', MntConst.eAlignmentRight);
        }

        for (let iCounter = 1; iCounter <= 14; iCounter++) {
            this.riMaintenance.AddTableField('WindowStart' + this.ZeroPadInt(iCounter, 2), MntConst.eTypeTime, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.riMaintenance.AddTableField('WindowEnd' + this.ZeroPadInt(iCounter, 2), MntConst.eTypeTime, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        }

        this.riMaintenance.AddTableField('ClosedCalendarTemplateNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Loopkup');
        this.riMaintenance.AddTableField('CallLogID', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseAliasName', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        if (this.pageParams.vbEnablePremiseLinking) {
            this.riMaintenance.AddTableField('LinkedToContractNumber', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.riMaintenance.AddTableFieldAlignment('LinkedToContractNumber', MntConst.eAlignmentRight);
            this.riMaintenance.AddTableField('LinkedToPremiseNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        }

        if (this.pageParams.vbEnableAssociatedPremise) {
            this.riMaintenance.AddTableField('AssociatedToPremiseNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        }

        this.riMaintenance.AddTableField('PlanningHighlightInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('AppointmentRequiredInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('AppointmentDetailDesc', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PremiseParkingNote', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('InitialTreatmentInstructions', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PaymentTypeCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('PaymentDesc', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');

        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.riMaintenance.AddTableField('CreateNewSRA', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.riMaintenance.AddTableField('GblSRAAdditionalSRA', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.riMaintenance.AddTableField('GblSRAAdditionalSRADocRef', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        }

        for (let iCounter = 1; iCounter <= 7; iCounter++) {
            this.riMaintenance.AddTableField(('WindowPreferredInd0' + iCounter), MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        }

        this.riMaintenance.AddTableField('PremisePlanningNote', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PreferredDayOfWeekReasonCode', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Lookup');
        this.riMaintenance.AddTableField('PreferredDayOfWeekNote', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('AdditionalComments1', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('AdditionalComments2', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('EnvironmentalRestrictedAreaText', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('RegulatoryAuthorityNumber', MntConst.eTypeInteger, (this.pageParams.vbEnableRegulatoryAuthority ? MntConst.eFieldOptionRequired : MntConst.eFieldOptionNormal), MntConst.eFieldStateNormal, this.pageParams.vbEnableRegulatoryAuthority ? 'Required' : 'Optional');

        this.riMaintenance.AddTableField('TechRetentionInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('TechRetentionReasonCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('PDACloseRecommendationInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('HyperSensitive', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('EnvironmentalRestrictedAreaInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('GuaranteeNumberBedrooms', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ListedCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('TelesalesEmployeeCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableFieldAlignment('TelesalesEmployeeCode', MntConst.eAlignmentRight);
        this.riMaintenance.AddTableField('CustomerIndicationNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ServiceNotifyTemplateEmail', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ServiceNotifyTemplateSMS', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('CICustRefReq', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CIRWOReq', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CICFWOReq', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CICFWOSep', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CICResponseSLA', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CIFirstSLAEscDays', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CISubSLAEscDays', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        if (this.pageParams.SCEnableRepeatSalesMatching) {
            this.riMaintenance.AddTableField('MatchedContractNumber', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableField('MatchedPremiseNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        }
        this.riMaintenance.AddTableCommit(this, (data: any): any => {
            if (data && data.hasError) { //DATA error
                this.parent.servicePrimaryError(data);
            }

            this.initiateVirtualTable();
            this.uiDisplay.tdPurchaseOrderLineNo = (this.parent.getControlValue('PurchaseOrderNo') !== '');
            this.uiDisplay.tdPurchaseOrderExpiryDate = (this.parent.getControlValue('PurchaseOrderNo') !== '');

            this.doLookupforClosedCalendarTemplate();

            //Update Ellipsis parameters
            this.parent.updateEllipsisParams();
        });

        this.riMaintenance.AddTable('*');
        this.riMaintenance.AddTableField('ContractNumber', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, '');
        this.riMaintenance.AddTableField('PremiseNumber', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, '');
        this.riMaintenance.AddTableField('PremiseRowID', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('Premise', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('CustomerInfoAvailable', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('CustomerInfoAvailable', false);

        this.riMaintenance.AddTableField('PremiseAnnualValue', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('PremiseAnnualValue', false);

        this.riMaintenance.AddTableField('NegBranchNumber', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('NegBranchNumber', false);

        this.riMaintenance.AddTableField('NegBranchName', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('NegBranchName', false);

        this.riMaintenance.AddTableField('NationalAccountBranch', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('NationalAccountBranch', false);

        this.riMaintenance.AddTableField('Status', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('Status', false);

        this.riMaintenance.AddTableField('InactiveEffectDate', MntConst.eTypeDateText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldAlignment('InactiveEffectDate', MntConst.eAlignmentCenter);
        this.riMaintenance.AddTableFieldPostData('InactiveEffectDate', false);

        this.riMaintenance.AddTableField('LostBusinessDesc', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('LostBusinessDesc', false);

        this.riMaintenance.AddTableField('LostBusinessDesc2', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('LostBusinessDesc2', false);

        this.riMaintenance.AddTableField('LostBusinessDesc3', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('LostBusinessDesc3', false);

        this.riMaintenance.AddTableField('DisableList', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('ContractTypeCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('AccountName', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('ShowValueButton', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('NewPremiseRowID', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldPostData('ShowValueButton', false);

        this.riMaintenance.AddTableField('NewContract', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');

        this.riMaintenance.AddTableField('AnyPendingBelow', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableFieldAlignment('AnyPendingBelow', MntConst.eAlignmentCenter);
        this.riMaintenance.AddTableFieldPostData('AnyPendingBelow', false);

        this.riMaintenance.AddTableField('ErrorMessageDesc', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableFieldPostData('ErrorMessageDesc', false);

        this.riMaintenance.AddTableField('CreateNewInvoiceGroupInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('InvoiceNarrativeText', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('DrivingChargeInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('PremiseServiceNote', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        for (let iCounter = 1; iCounter <= 14; iCounter++) {
            this.riMaintenance.AddTableField('DefaultWindowStart' + this.ZeroPadInt(iCounter, 2), MntConst.eTypeTime, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
            this.riMaintenance.AddTableField('DefaultWindowEnd' + this.ZeroPadInt(iCounter, 2), MntConst.eTypeTime, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        }

        this.riMaintenance.AddTableField('ProofScanRequiredInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('ProofSignatureRequiredInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('TelesalesInd', MntConst.eTypeCheckBox, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');
        this.riMaintenance.AddTableField('CustomerAvailTemplateID', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateNormal, 'Optional');

        this.riMaintenance.AddTableField('GblSRATypeCode', MntConst.eTypeCode, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
        this.riMaintenance.AddTableField('GblSRADesc', MntConst.eTypeText, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');

        this.riMaintenance.AddTableField('GridUniqueID', MntConst.eTypeTextFree, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');

        if (this.pageParams.vbEnableServiceCoverDispLev) {
            this.riMaintenance.AddTableField('DisplayQty', MntConst.eTypeInteger, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('DisplayQty', false);
            this.riMaintenance.AddTableField('WEDValue', MntConst.eTypeDecimal1, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('WEDValue', false);
            this.riMaintenance.AddTableField('MaterialsValue', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('MaterialsValue', false);
            this.riMaintenance.AddTableField('MaterialsCost', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('MaterialsCost', false);
            this.riMaintenance.AddTableField('LabourValue', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('LabourValue', false);
            this.riMaintenance.AddTableField('LabourCost', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('LabourCost', false);
            this.riMaintenance.AddTableField('ReplacementValue', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('ReplacementValue', false);
            this.riMaintenance.AddTableField('ReplacementCost', MntConst.eTypeCurrency, MntConst.eFieldOptionNormal, MntConst.eFieldStateReadOnly, 'ReadOnly');
            this.riMaintenance.AddTableFieldPostData('ReplacementCost', false);
        }

        // Fix for IUI-24927.
        //Timeout is set so that the service call get completed and InvoiceGroupNumber value get available to the callback function.
        if (this.pageParams.ParentMode === 'GridSearch' && this.riMaintenance.CurrentMode !== 'eModeAdd') {
            this.riMaintenance.AddTableCommit(this, setTimeout(() => {
                this.parent.pgPM2.ShowInvoiceNarrativeTab();
            }, 6000));
        } else {
            this.riMaintenance.AddTableCommit(this);
        }

        this.riMaintenance.Complete();

        this.riExchange.riInputElement.Disable(this.uiForm, 'Status');
        this.riExchange.riInputElement.Disable(this.uiForm, 'InactiveEffectDate'); this.parent.dateDisable('InactiveEffectDate', true, true);
        this.riExchange.riInputElement.Disable(this.uiForm, 'LostBusinessDesc');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PaymentTypeValue');

        this.riExchange.riInputElement.SetLookUpStatus(this.uiForm, 'CustomerTypeCode', true);

        this.parent.pgPM2.ShowInvoiceNarrativeTab();
        this.parent.pgPM2.HideQuickWindowSet(true);

        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.parent.pgPM3.PremGblSRADisable();
            this.uiDisplay.trCreateNewSRA = true;
        } else {
            this.uiDisplay.trCreateNewSRA = false;
        }

        if (this.riMaintenance.RecordSelected(false)) {
            this.riMaintenance.FetchRecord();
        }

        this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLiCABSLevel');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLSiteRef');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLEffectiveDate'); this.parent.dateDisable('PNOLEffectiveDate', true, true);
        this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLUpliftAmount');
        this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLSetupChargeToApply');

        this.riExchange.updateCtrl(this.controls, 'cmdResendPremises', 'disabled', true);
        this.riExchange.riInputElement.Disable(this.uiForm, 'cmdResendPremises');

        this.uiDisplay.trBatchProcessInformation = false;

        if (this.pageParams.ParentMode === 'Contract-Add') {
            this.parent.pgPM3.DefaultFromProspect();
        }

        if (this.riExchange.getParentHTMLValue('RunningReadOnly') === 'yes') {
            this.riMaintenance.FunctionAdd = false;
            this.riMaintenance.FunctionSelect = false;
            this.riMaintenance.FunctionDelete = false;
            this.riMaintenance.FunctionUpdate = false;
            this.riMaintenance.FunctionSnapShot = false;
        }

        if (this.riExchange.getParentHTMLValue('CurrentCallLogID') !== '') {
            this.riMaintenance.FunctionAdd = false;
            this.riMaintenance.FunctionSelect = false;
        }

        this.parent.pgPM2.BuildMenuOptions();
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.riMaintenance.DisableInput('menu');
        } else {
            this.riMaintenance.EnableInput('menu');
        }

        this.uiDisplay.trComment2 = this.pageParams.vbShowPremiseAdditionalTabLog;
        this.uiDisplay.trAdditionalComment2 = this.pageParams.vbShowPremiseAdditionalTabLog;

        if (!this.pageParams.lAllowUserAuthUpdate) {
            this.riMaintenance.FunctionAdd = false;
            this.riMaintenance.FunctionUpdate = false;
            this.riMaintenance.FunctionDelete = false;
        }

        if (this.riExchange.getParentHTMLValue('ProspectNumber') !== '') {
            this.riMaintenance.FunctionAdd = true;
        }

        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TelesalesEmployeeCode', this.riExchange.riInputElement.checked(this.uiForm, 'TelesalesInd'));

        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.parent.pgPM3.GetCustomerTypeDefault();
            this.parent.pgPM2.riExchange_CBORequest();
        }

        this.riMaintenance.execMode(this.riMaintenance.CurrentMode, [this.pgPM0, this.pgPM1, this.pgPM2, this.pgPM3, this.parent]);
    }

    public riMaintenance_BeforeUpdate(): void {

        this.parent.pgPM3.SelServiceNotifyTemplateEmail_OnChange();
        this.parent.pgPM3.SelServiceNotifyTemplateSMS_OnChange();

        if (this.pageParams.vSCMultiContactInd) {
            this.uiDisplay.tdBtnAmendContact = true;
        }

        if (!this.pageParams.GridCacheTime) this.pageParams.GridCacheTime = (new Date()).toTimeString().split(' ')[0];
        this.riExchange.riInputElement.Enable(this.uiForm, 'cmdSRAGenerateText');

        if (this.pageParams.SCEnablePestNetOnlineProcessing) {
            this.parent.setControlValue('PNOLUpliftAmount', '0');
            this.parent.setControlValue('PNOLSetUpChargeToApply', '0');
        }

        if ((this.pageParams.vbEnablePremiseLinking && this.parent.getControlValue('LinkedToContractNumber') !== '') ||
            (this.pageParams.vbEnableAssociatedPremise && this.parent.getControlValue('AssociatedToPremiseNumber') !== '')) {
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseServiceNote');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseParkingNote');
        }
        this.riExchange.riInputElement.Enable(this.uiForm, 'SelPrintRequired');


        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TelesalesEmployeeCode', this.riExchange.riInputElement.checked(this.uiForm, 'TelesalesInd'));

        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.parent.pgPM3.BuildPremGblSRACache();
            this.pageParams.vbGblSRAMode = '';
            this.parent.pgPM3.PremGblSRADisable();
        }
    }

    public riMaintenance_BeforeSave(): void {
        if (this.pageParams.initialVtxGeocodeVal) {
            this.parent.setControlValue('PremiseVtxGeoCode', this.pageParams.initialVtxGeocodeVal);
        }
    }

    public riMaintenance_AfterSave(): void {
        this.parent.setControlValue('ErrorMessageDesc', '');
        if (this.parent.actionSave === 1) this.parent.setControlValue('ContractTypeCode', this.pageParams.CurrentContractType);

        let fieldsArr = this.riExchange.getAllCtrl(this.controls);
        this.riMaintenance.clear();
        for (let i = 0; i < fieldsArr.length; i++) {
            let id = fieldsArr[i];
            let dataType = this.riMaintenance.getControlType(this.controls, id, 'type');
            let value = this.parent.getControlValue(id);
            switch (id) {
                case 'PremiseROWID':
                case 'PremiseRowID': // Subtle difference in CaSe from previous line
                    value = this.parent.getControlValue('Premise');
                    break;
                case 'GridUniqueID':
                    dataType = MntConst.eTypeText;
                    value = this.pageParams.GridCacheTime;
                    break;
            }
            this.riMaintenance.PostDataAdd(id, value, dataType);
        }
        this.riMaintenance.Execute(this, function (data: any): any {
            this.parent.riMaintenance.pendingFnObj = null;
            if (data.hasError) {
                if (data.errorMessage && data.errorMessage.trim() !== '') {
                    this.parent.showAlert(data.errorMessage);
                }
            } else {
                this.parent.markAsPrestine();
                this.parent.routeAwayGlobals.setSaveEnabledFlag(false);
                if (data.hasOwnProperty('InvoiceGroupDesc')) {
                    if (data.InvoiceGroupDesc !== '') {
                        this.parent.riMaintenance.renderResponseForCtrl(this, data);
                        if (this.parent.riMaintenance.CurrentMode !== MntConst.eModeSaveAdd) {
                            this.parent.showAlert(MessageConstant.Message.SavedSuccessfully, 1);
                            this.afterSaveComplete();
                        } else {
                            this.afterSaveComplete();
                        }
                    } else {
                        this.parent.showAlert(MessageConstant.Message.SaveUnSuccessful, 0);
                    }
                }
            }
        }, 'POST', this.parent.actionSave);

        /**********************/

        if (this.pageParams.vSCMultiContactInd) {
            this.uiDisplay.tdBtnAmendContact = true;
        }

        if (!this.pageParams.GridCacheTime) {
            this.pageParams.GridCacheTime = (new Date()).toTimeString().split(' ')[0];
        }

        this.riExchange.riInputElement.Disable(this.uiForm, 'SelPrintRequired');

        if (this.pageParams.SCEnablePestNetOnlineProcessing) {
            this.parent.setControlValue('PNOLUpliftAmount', '0');
            this.parent.setControlValue('PNOLSetUpChargeToApply', '0');
        }

        this.parent.pgPM2.HideQuickWindowSet(true);
        this.parent.pgPM1.SetOkToUpgradeToPNOL();

        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.pageParams.vbGblSRAMode = '';
            this.parent.pgPM3.PremGblSRADisable();
        }

        this.riExchange.riInputElement.Disable(this.uiForm, 'SelServiceNotifyTemplateEmail');
        this.riExchange.riInputElement.Disable(this.uiForm, 'SelServiceNotifyTemplateSMS');
    }

    /**
     * Added method to be called after the save is completed
     * So that the save successful message is called only if page does navigate after save
     */
    private afterSaveComplete(): void {
        if (this.parent.riMaintenance.CurrentMode === MntConst.eModeSaveAdd) {
            this.parent.pgPM2.riMaintenance_AfterSaveAdd_clbk();
        }

        if (this.parent.riMaintenance.CurrentMode === MntConst.eModeSaveUpdate && this.parent.parentMode === 'AddFromPremise') {
            this.parent.pgPM2.riMaintenance_AfterSaveAdd_clbk();
        }

        this.parent.riMaintenance.CurrentMode = MntConst.eModeUpdate;
    }

    public riMaintenance_AfterFetch(): void {
        if (!this.pageParams.GridCacheTime) {
            this.pageParams.GridCacheTime = (new Date()).toTimeString().split(' ')[0];
        }

        this.parent.setControlValue('SelServiceNotifyTemplateEmail', '');
        if (this.parent.getControlValue('ServiceNotifyTemplateEmail')) {
            this.parent.setControlValue('SelServiceNotifyTemplateEmail', this.parent.getControlValue('ServiceNotifyTemplateEmail'));
        }
        this.parent.pgPM3.SelServiceNotifyTemplateEmail_OnChange();

        this.parent.setControlValue('SelServiceNotifyTemplateSMS', '');
        if (this.parent.getControlValue('ServiceNotifyTemplateSMS')) {
            this.parent.setControlValue('SelServiceNotifyTemplateSMS', this.parent.getControlValue('ServiceNotifyTemplateSMS'));
        }
        this.parent.pgPM3.SelServiceNotifyTemplateSMS_OnChange();

        if (this.pageParams.vSCMultiContactInd) {
            this.uiDisplay.tdBtnAmendContact = true;
        }

        if (this.parent.getControlValue('RoutingSource').length === 0) {
            this.parent.setControlValue('SelRoutingSource', '');
        } else {
            this.parent.setControlValue('SelRoutingSource', this.parent.getControlValue('RoutingSource'));
        }

        if (this.parent.getControlValue('PrintRequired').length === 0) {
            this.parent.setControlValue('SelPrintRequired', 0);
        } else {
            this.parent.setControlValue('SelPrintRequired', this.parent.getControlValue('PrintRequired'));
        }

        if (this.parent.getControlValue('InactiveEffectDate') !== '') {
            this.uiDisplay.labelInactiveEffectDate = true;
            this.uiDisplay.InactiveEffectDate = true;

            if (this.parent.getControlValue('LostBusinessDesc') !== '') {
                this.uiDisplay.LostBusinessDesc = true;
                this.parent.LostBusinessText = this.parent.getControlValue('LostBusinessDesc2') + '\n' + this.parent.getControlValue('LostBusinessDesc3');
            } else {
                this.uiDisplay.LostBusinessDesc = false;
            }
        } else {
            this.uiDisplay.labelInactiveEffectDate = false;
            this.uiDisplay.InactiveEffectDate = false;
            this.uiDisplay.LostBusinessDesc = false;
        }

        this.uiDisplay.AnyPendingBelow = (this.parent.getControlValue('AnyPendingBelow') !== '');

        this.uiDisplay.cmdValue = (this.riExchange.riInputElement.checked(this.uiForm, 'ShowValueButton'));

        this.uiDisplay.tdPremiseAnnualValueLab = (this.riExchange.ClientSideValues.Fetch('FullAccess') === 'Full' ||
            this.riExchange.ClientSideValues.Fetch('BranchNumber') === this.parent.getControlValue('ServiceBranchNumber') ||
            this.riExchange.ClientSideValues.Fetch('BranchNumber') === this.parent.getControlValue('NegBranchNumber'));


        this.uiDisplay.tdNationalAccount = (this.riExchange.riInputElement.checked(this.uiForm, 'NationalAccountChecked') &&
            this.riExchange.riInputElement.checked(this.uiForm, 'NationalAccount'));

        this.uiDisplay.tdCustomerInfo = (this.riExchange.riInputElement.checked(this.uiForm, 'CustomerInfoAvailable'));

        if (this.parent.getControlValue('SCEnableDrivingCharges') &&
            this.parent.getControlValue('DrivingChargeInd') && this.pageParams.CurrentContractType !== 'P') {
            this.uiDisplay.tdDrivingChargeValueLab = true;
            this.uiDisplay.tdDrivingChargeValue = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'DrivingChargeValue', true);
        } else {
            this.uiDisplay.tdDrivingChargeValueLab = false;
            this.uiDisplay.tdDrivingChargeValue = false;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'DrivingChargeValue', false);
        }

        this.uiDisplay.tdPNOL = (this.riExchange.riInputElement.checked(this.uiForm, 'PNOL'));
        this.uiDisplay.tdHyperSens = (this.riExchange.riInputElement.checked(this.uiForm, 'HyperSensitive'));
        this.uiDisplay.tdEnvironmentalRestrictedAreaInd = (this.riExchange.riInputElement.checked(this.uiForm, 'EnvironmentalRestrictedAreaInd'));
        this.uiDisplay.cmdMatchPremise = (this.pageParams.SCEnableRepeatSalesMatching && this.parent.getControlValue('MatchedContractNumber') === '' &&
            this.parent.getControlValue('MatchedPremiseNumber') === '0');

        this.parent.pgPM2.AddTabs();
        this.parent.pgPM2.ShowInvoiceNarrativeTab();

        this.uiDisplay.tdContractHasExpired = (this.riExchange.riInputElement.checked(this.uiForm, 'ContractHasExpired'));
        this.uiDisplay.trBatchProcessInformation = false;

        this.parent.pgPM2.HideQuickWindowSet(true);
        this.parent.pgPM1.doLookupforLinkedContract();

        if (this.parent.getControlValue('LinkedToContractNumber') !== '' ||
            this.parent.getControlValue('AssociatedToPremiseNumber') !== '') {
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseServiceNote');
        }

        if (this.riExchange.riInputElement.checked(this.uiForm, 'WasteConsignmentNoteExemptInd')) { //Checkbox
            this.parent.setControlValue('NextWasteConsignmentNoteNumber', '');
        }

        if (this.pageParams.vbEnableServiceCoverDispLev) {
            this.parent.pgPM2.CalDisplayValues();
        }

        this.parent.pgPM3.setPurchaseOrderFields();
        this.parent.pgPM3.CustomerIndicationNumber_onChange();

        if (this.pageParams.vbEnableGlobalSiteRiskAssessment) {
            this.parent.pgPM3.BuildPremGblSRACache();
            this.pageParams.vbGblSRAMode = '';
            this.parent.pgPM3.PremGblSRADisable();
        }
    }

    public SetOkToUpgradeToPNOL(): void {
        if (this.pageParams.SCEnablePestNetOnlineProcessing) {
            this.parent.setControlValue('origPNOL', this.parent.getControlValue('PNOL')); //Checkbox
            this.parent.setControlValue('origPNOLiCABSLevel', this.parent.getControlValue('PNOLiCABSLevel'));
            this.parent.setControlValue('origPNOLEffectiveDate', this.parent.getControlValue('PNOLEffectiveDate'));
            this.parent.setControlValue('origPNOLSiteRef', this.parent.getControlValue('PNOLSiteRef'));

            this.uiDisplay.trAddToPNOLUpliftAmount = (this.riMaintenance.CurrentMode !== MntConst.eModeAdd);
            this.PNOL_onClick();
            this.parent.pgPM2.CheckCanUpdatePNOLDetails();
        }
    }

    public PNOL_onClick(): void {
        if (this.riMaintenance.CurrentMode === MntConst.eModeAdd || this.riMaintenance.CurrentMode === MntConst.eModeSaveAdd ||
            this.riMaintenance.CurrentMode === MntConst.eModeUpdate || this.riMaintenance.CurrentMode === MntConst.eModeSaveUpdate) {
            this.pageParams.vbDisablePNOLEffectDateDefaulting = !!this.pageParams.vDisablePNOLEffectDateDefaulting;
            if (this.riExchange.riInputElement.checked(this.uiForm, 'PNOL')) { //Checkbox
                this.riExchange.riInputElement.Enable(this.uiForm, 'PNOLSiteRef');
                this.riExchange.riInputElement.Enable(this.uiForm, 'PNOLiCABSLevel');
                this.riExchange.riInputElement.Enable(this.uiForm, 'PNOLEffectiveDate');
                this.parent.dateDisable('PNOLEffectiveDate', false, false);
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PNOLiCABSLevel', true);
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PNOLEffectiveDate', true); this.pageParams.dtPNOLEffectiveDate.required = true;

                if ((!this.riExchange.riInputElement.checked(this.uiForm, 'origPNOL')) && (this.pageParams.vbDisablePNOLEffectDateDefaulting)) {
                    if (this.pageParams.PNOLMode === 'Update' && this.pageParams.CurrentContractType === 'C') {
                        this.parent.setControlValue('PNOLEffectiveDate', this.parent.getControlValue('PNOLEffectiveDefault'));
                    } else {
                        this.pageParams.dtPNOLEffectiveDate.value = this.parent.getControlValue('PremiseCommenceDate');
                        this.parent.setControlValue('PNOLEffectiveDate', this.pageParams.dtPNOLEffectiveDate.value);
                    }
                }
            } else {
                let resetFlag = true;
                if (this.pageParams.PNOLMode === 'Add') {
                    this.parent.setControlValue('PNOLSiteRef', '');
                    this.parent.setControlValue('PNOLSiteRefDesc', '');
                    this.parent.setControlValue('PNOLiCABSLevel', '');
                    this.parent.setControlValue('PNOLEffectiveDate', '');
                    this.parent.selDate('', 'PNOLEffectiveDate');
                    resetFlag = false;
                    this.parent.setControlValue('PNOLUpliftAmount', '0');
                    this.parent.setControlValue('PNOLSetUpChargeToApply', '0');
                }

                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PNOLiCABSLevel', false);
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PNOLEffectiveDate', false); this.pageParams.dtPNOLEffectiveDate.required = false;
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'PNOLSiteRef', false);

                this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLSiteRef');
                this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLiCABSLevel');
                this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLEffectiveDate'); this.parent.dateDisable('PNOLEffectiveDate', true, resetFlag);
                this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLUpliftAmount');
                this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLSiteRef');
                this.riExchange.riInputElement.Disable(this.uiForm, 'PNOLSetUpChargeToApply');
            }
        }
    }

    public SetSCVariables(): void {
        this.pageParams.SCEnableHopewiserPAF = this.pageParams.vSCEnableHopewiserPAF;
        this.pageParams.SCEnableDatabasePAF = this.pageParams.vSCEnableDatabasePAF;
        this.pageParams.SCEnableMarktSelect = this.pageParams.vSCEnableMarktSelect;
        this.pageParams.SCAddressLine3Logical = this.pageParams.vSCAddressLine3Logical;
        this.pageParams.SCAddressLine4Required = this.pageParams.vSCAddressLine4Required;
        this.pageParams.SCAddressLine5Required = this.pageParams.vSCAddressLine5Required;
        this.pageParams.SCAddressLine5Logical = this.pageParams.vSCAddressLine5Logical;
        this.pageParams.SCPostCodeRequired = this.pageParams.vSCPostCodeRequired;
        this.pageParams.SCPostCodeMustExistInPAF = this.pageParams.vSCPostCodeMustExistInPAF;
        this.pageParams.SCRunPAFSearchOnFirstAddressLine = this.pageParams.vSCRunPAFSearchOn1stAddressLine;
        this.pageParams.SCServiceReceiptRequired = this.pageParams.vSCServiceReceiptRequired;
        this.pageParams.SCProofOfServiceRequired = this.pageParams.vSCServiceReceiptRequired;
        this.pageParams.SCServiceCoverUpdateViaGrid = this.pageParams.vSCServiceCoverUpdateViaGrid;
    }

    public SetHTMLPageSettings(): void {
        this.uiDisplay.labelDiscountCode = (!!this.pageParams.vEnableDiscountCode);
        this.uiDisplay.DiscountCode = (!!this.pageParams.vEnableDiscountCode);
        this.uiDisplay.DiscountDesc = (!!this.pageParams.vEnableDiscountCode);
        this.uiDisplay.trPremiseAddressLine3 = (!!this.pageParams.vEnableAddressLine3);
        this.uiDisplay.trPremiseDirectoryName = (!!this.pageParams.vEnableMapGridReference);
        this.uiDisplay.trPremiseDirectoryNameBlank = (!!this.pageParams.vEnableMapGridReference);
        this.uiDisplay.trRetainServiceWeekday = (!!this.pageParams.vEnableRetentionOfServiceWeekDay);
        this.uiDisplay.trRetainServiceWeekdayBlank = (!!this.pageParams.vEnableRetentionOfServiceWeekDay);
        this.uiDisplay.trServiceReceiptRequired = (!!this.pageParams.vSCServiceReceiptRequired);
        this.uiDisplay.trServiceReceiptRequiredBlank = (!!this.pageParams.vSCServiceReceiptRequired);
        this.uiDisplay.tdProofOfServiceRequired = (!!this.pageParams.vSCProofOfServiceRequired);
        this.uiDisplay.tdProofOfServiceRequired1 = (!!this.pageParams.vSCProofOfServiceRequired);
        this.uiDisplay.trPaymentType = (!!this.pageParams.vSCEnablePayTypeInvGroupLevel);

        this.pageParams.vbEnableInstallsRemovals = (!!this.pageParams.vEnableInstallsRemovals);
        this.pageParams.vbEnableLocations = (!!this.pageParams.vEnableLocations);
        this.pageParams.vbEnablePremiseLocInsert = (!!this.pageParams.vEnablePremiseLocInsert);
        this.pageParams.vbEnableLocations = (!!this.pageParams.vEnableLocations);

        let vEnableNationalAccountWarning = (!!this.pageParams.vEnableNationalAccountWarning);
        this.riExchange.updateCtrl(this.controls, 'NationalAccountChecked', 'value', vEnableNationalAccountWarning);
        this.parent.setControlValue('NationalAccountChecked', vEnableNationalAccountWarning);

        this.pageParams.SCEnableDrivingCharges = (!!this.pageParams.vSCEnableDrivingCharges);
        this.pageParams.SCCapitalFirstLtr = (!!this.pageParams.vSCCapitalFirstLtr);
        this.pageParams.SCEnablePostcodeDefaulting = (!!this.pageParams.vEnablePostcodeDefaulting);
        this.pageParams.SCEnableServiceBranchUpdate = (!!this.pageParams.vSCEnableServiceBranchUpdate);
        this.pageParams.SCEnablePestNetOnlineProcessing = (!!this.pageParams.vSCEnablePestNetOnline);
        this.pageParams.SCEnablePestNetOnlineDefaults = (!!this.pageParams.vSCEnablePestNetOnlineDefaults);
        this.pageParams.SCFixedServiceTimeRequired = (!!this.pageParams.vSCFixedServiceTimeRequired);
        this.pageParams.vbEnableRegulatoryAuthority = (!!this.pageParams.vEnableRegulatoryAuthority);
        this.pageParams.vbShowWasteConsignmentNoteHistory = (!!this.pageParams.vShowWasteConsignmentNoteHistory);
        this.pageParams.vbShowPremiseWasteTab = (!!this.pageParams.vShowPremiseWasteTab);
        this.pageParams.vbEnableServiceCoverDispLev = (!!this.pageParams.vEnableServiceCoverDispLev);
        this.pageParams.SCEnableBarcodes = (!!this.pageParams.vSCEnableBarcodes);
        this.pageParams.SCRequireBarcodes = (!!this.pageParams.vSCRequireBarcodes);
        this.pageParams.SCEnableSignatures = (!!this.pageParams.vSCEnableSignatures);
        this.pageParams.SCRequireSignatures = (!!this.pageParams.vSCRequireSignatures);
        this.pageParams.vbShowPremiseAdditionalTab = (!!this.pageParams.vShowPremisesAdditionalTabReq);
        this.pageParams.vbShowPremiseAdditionalTabLog = (!!this.pageParams.vShowPremisesAdditionalTabLog);
        this.pageParams.vbEnableProductSaleCommenceDate = (!!this.pageParams.vEnableProductSaleCommenceDate);
        this.pageParams.SCEnableAccountAddressMessage = (!!this.pageParams.vSCEnableAccountAddressMessage);
        this.pageParams.vbEnableGlobalSiteRiskAssessment = (!!this.pageParams.glEnableGlobalSiteRiskAssessment);

        if (this.pageParams.vbEnableServiceCoverDispLev) {
            this.uiDisplay.tdWEDLabel = (!!this.pageParams.vEnableWED);
            this.uiDisplay.WEDValue = (!!this.pageParams.vEnableWED);
        }

        this.uiDisplay.tdPremiseFixedServiceTime = this.pageParams.SCFixedServiceTimeRequired;
        this.uiDisplay.tdPremiseFixedServiceTime1 = this.pageParams.SCFixedServiceTimeRequired;

        if (this.pageParams.SCEnablePestNetOnLineProcessing) {
            this.uiDisplay.trPNOL = true;
            this.uiDisplay.trPNOLiCABSLevel = true;
        }

        this.uiDisplay.tdDrivingChargeIndLab = (this.pageParams.SCEnableDrivingCharges && this.pageParams.CurrentContractType !== 'P');
        this.uiDisplay.tdDrivingChargeInd = (this.pageParams.SCEnableDrivingCharges && this.pageParams.CurrentContractType !== 'P');

        if (this.pageParams.SCEnableBarcodes) {
            this.uiDisplay.trBarcodeRequired = true;
        } else {
            this.uiDisplay.trBarcodeRequired = false;
            this.parent.setControlValue('ProofScanRequiredInd', false);
        }

        if (this.pageParams.SCEnableSignatures) {
            this.uiDisplay.trSignatureRequired = true;
        } else {
            this.uiDisplay.trSignatureRequired = false;
            this.parent.setControlValue('ProofSignatureRequiredInd', false);
        }
    }

    public GetRegistrySetting(pcRegSection: string, pcRegKey: string): any {
        this.LookUp.GetRegistrySetting(pcRegSection, pcRegKey).then(function (data: any): any {
            this.logger.log('GetRegistrySetting ****', data);
        });
    }

    public doLookupforLinkedContract(): void {
        if (this.parent.getControlValue('LinkedToContractNumber')) {
            this.LookUp.lookUpPromise([{
                'table': 'Contract',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'ContractNumber': this.parent.getControlValue('LinkedToContractNumber')
                },
                'fields': ['ContractNumber', 'ContractName']
            }, {
                'table': 'Premise',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'ContractNumber': this.parent.getControlValue('LinkedToContractNumber'),
                    'PremiseNumber': this.parent.getControlValue('LinkedToPremiseNumber')
                },
                'fields': ['PremiseName']
            }]).then((data) => {
                if (data) {
                    if (data[0].length > 0) {
                        this.parent.setControlValue('LinkedToContractName', data[0][0].ContractName);
                    }
                    if (data[1].length > 0) {
                        this.parent.setControlValue('LinkedToPremiseName', data[1][0].PremiseName);
                    }
                }
            });
        }
    }

    public doLookupforClosedCalendarTemplate(): void {
        this.LookUp.lookUpPromise([
            {
                'table': 'BranchClosedCalendarTemplate',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'BranchNumber': this.utils.getBranchCode()
                },
                'fields': ['ClosedCalendarTemplateNumber']
            },
            {
                'table': 'ClosedCalendarTemplate',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['ClosedCalendarTemplateNumber', 'TemplateName']
            }
        ]).then((data) => {
            let recordSet_BranchClosedCalendarTemplate = data[0];
            let recordSet_ClosedCalendarTemplate = data[1];

            let arrClosedCalendarTemplateNumber = [];
            if (recordSet_BranchClosedCalendarTemplate && recordSet_BranchClosedCalendarTemplate.length > 0) {
                for (let i = 0; i < recordSet_BranchClosedCalendarTemplate.length; i++) {
                    arrClosedCalendarTemplateNumber.push(recordSet_BranchClosedCalendarTemplate[i].ClosedCalendarTemplateNumber);
                }
            }

            if (recordSet_ClosedCalendarTemplate && recordSet_ClosedCalendarTemplate.length > 0) {
                this.parent.dropDown.ClosedCalendarTemplate = [];
                for (let i = 0; i < recordSet_ClosedCalendarTemplate.length; i++) {
                    if (arrClosedCalendarTemplateNumber.indexOf(recordSet_ClosedCalendarTemplate[i].ClosedCalendarTemplateNumber) > -1) {
                        this.parent.dropDown.ClosedCalendarTemplate.push({
                            value: recordSet_ClosedCalendarTemplate[i].ClosedCalendarTemplateNumber,
                            label: recordSet_ClosedCalendarTemplate[i].ClosedCalendarTemplateNumber + ' - ' + recordSet_ClosedCalendarTemplate[i].TemplateName
                        });
                    }
                }
            }

        });
    }

    private initiateVirtualTable(): void {
        switch (this.pageParams.ParentMode) {
            case 'CSearch':
            case 'CSearchAdd':
            case 'IGSearch':
            case 'Contract':
            case 'Contract-Add':
            case 'Contact':
            case 'GridSearch':
            case 'GridSearchAdd':
                this.riMaintenance.setIndependentVTableLookup(true);
                this.riMaintenance.AddVirtualTable('Contract');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('ContractNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateFixed, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('ContractName', MntConst.eTypeText, MntConst.eVirtualFieldStateFixed, 'Virtual');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
                break;
            default:
                this.riMaintenance.setIndependentVTableLookup(true);
                this.riMaintenance.AddVirtualTable('Contract');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('ContractNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('ContractName', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
        }

        if (this.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.riMaintenance.AddVirtualTable('InvoiceGroup');
            this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.AddVirtualTableKey('AccountNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateFixed, '', '', '', 'Virtual');
            this.riMaintenance.AddVirtualTableKey('InvoiceGroupNumber', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
            this.riMaintenance.AddVirtualTableField('InvoiceGroupDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
            this.riMaintenance.AddVirtualTableCommit(this, false, false);

            this.riMaintenance.AddVirtualTable('Account');
            this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.AddVirtualTableKey('AccountNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
            this.riMaintenance.AddVirtualTableField('NationalAccount', MntConst.eTypeCheckBox, MntConst.eVirtualFieldStateNormal, 'Virtual');
            this.riMaintenance.AddVirtualTableCommit(this, (data: any): any => {
                setTimeout(() => {
                    this.riExchange.updateCtrl(this.controls, 'NationalAccountChecked', 'value', this.pageParams.vEnableNationalAccountWarning);
                    this.parent.setControlValue('NationalAccountChecked', this.pageParams.vEnableNationalAccountWarning);

                    if (this.riExchange.riInputElement.checked(this.uiForm, 'NationalAccountChecked') //Checkbox
                        && this.riExchange.riInputElement.checked(this.uiForm, 'NationalAccount')) { //Checkbox
                        this.uiDisplay.tdNationalAccount = true;
                    } else {
                        this.uiDisplay.tdNationalAccount = false;
                    }
                    this.parent.updateEllipsisParams();
                }, 500);
            }, false);

            this.riMaintenance.AddVirtualTable('SalesArea');
            this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.AddVirtualTableKey('BranchNumber', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', 'ServiceBranchNumber', 'Virtual');
            this.riMaintenance.AddVirtualTableKey('SalesAreaCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
            this.riMaintenance.AddVirtualTableField('SalesAreaDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
            this.riMaintenance.AddVirtualTableCommit(this, false, false);

            if (this.parent.getControlValue('DiscountCode')) {
                this.riMaintenance.AddVirtualTable('Discount');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('DiscountCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('DiscountDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            this.riMaintenance.AddVirtualTable('Branch');
            this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.AddVirtualTableKey('BranchNumber', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', 'ServiceBranchNumber', 'Virtual');
            this.riMaintenance.AddVirtualTableField('BranchName', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
            this.riMaintenance.AddVirtualTableCommit(this, false, false);

            if (this.parent.getControlValue('PremiseSalesEmployee')) {
                this.riMaintenance.AddVirtualTable('Employee');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('EmployeeCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', 'PremiseSalesEmployee', 'Virtual');
                this.riMaintenance.AddVirtualTableField('EmployeeSurname', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', 'SalesEmployeeSurname');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.parent.getControlValue('PremiseSRAEmployee')) {
                this.riMaintenance.AddVirtualTable('Employee', 'PremiseSRAEmployee');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('EmployeeCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', 'PremiseSRAEmployee', 'Virtual');
                this.riMaintenance.AddVirtualTableField('EmployeeSurname', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', 'SRAEmployeeSurname');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.parent.getControlValue('TelesalesEmployeeCode')) {
                this.riMaintenance.AddVirtualTable('Employee', 'TelesalesEmployeeCode');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('EmployeeCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', 'TelesalesEmployeeCode', 'Virtual');
                this.riMaintenance.AddVirtualTableField('EmployeeSurname', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', 'TelesalesEmployeeName');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            this.riMaintenance.AddVirtualTable('CustomerTypeLanguage');
            this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.AddVirtualTableKey('LanguageCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '=', this.riExchange.LanguageCode(), '', 'Virtual');
            this.riMaintenance.AddVirtualTableKey('CustomerTypeCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
            this.riMaintenance.AddVirtualTableField('CustomerTypeDesc', MntConst.eTypeText, MntConst.eVirtualFieldStateNormal, 'Virtual');
            this.riMaintenance.AddVirtualTableCommit(this, false, false);

            if (this.parent.getControlValue('VehicleTypeNumber')) {
                this.riMaintenance.AddVirtualTable('VehicleType');
                this.riMaintenance.AddVirtualTableKey('VehicleTypeNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateFixed, '=', 'vehicleTypeNumber', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('VehicleTypeDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            this.riMaintenance.AddVirtualTable('Language');
            this.riMaintenance.AddVirtualTableKey('LanguageCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateFixed, '=', this.riExchange.LanguageCode(), '', 'Virtual');
            this.riMaintenance.AddVirtualTableField('LanguageDescription', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
            this.riMaintenance.AddVirtualTableCommit(this, false, false);

            if (this.parent.getControlValue('CustomerAvailTemplateID')) {
                this.riMaintenance.AddVirtualTable('CustomerAvailTemplate');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('CustomerAvailTemplateID', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('CustomerAvailTemplateDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', '');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.parent.getControlValue('ClosedCalendarTemplateNumber')) {
                this.riMaintenance.AddVirtualTable('ClosedCalendarTemplate');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('ClosedCalendarTemplateNumber', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('TemplateName', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', 'ClosedTemplateName');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.parent.getControlValue('PreferredDayOfWeekReasonCode')) {
                this.riMaintenance.AddVirtualTable('PreferredDayOfWeekReasonLang');
                this.riMaintenance.AddVirtualTableKey('PreferredDayOfWeekReasonCode', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableKey('LanguageCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '=', this.riExchange.LanguageCode(), '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('PreferredDayOfWeekReasonLangDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.pageParams.vSICCodeEnable) {
                this.riMaintenance.AddVirtualTable('SICCodeLang');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('LanguageCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '=', this.riExchange.LanguageCode(), '', 'Virtual');
                this.riMaintenance.AddVirtualTableKey('SICCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('SICDescription', MntConst.eTypeText, MntConst.eVirtualFieldStateNormal, 'Virtual', 'SICDesc');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.parent.getControlValue('RegulatoryAuthorityNumber')) {
                this.riMaintenance.AddVirtualTable('RegulatoryAuthority');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('RegulatoryAuthorityNumber', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('RegulatoryAuthorityName', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', '');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            this.riMaintenance.AddVirtualTable('PestNetOnLineLevel');
            this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
            this.riMaintenance.AddVirtualTableKey('PNOLiCABSLevel', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
            this.riMaintenance.AddVirtualTableField('PNOLDescription', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', '');
            this.riMaintenance.AddVirtualTableCommit(this, false, false);

            if (this.parent.getControlValue('TechRetentionReasonCode')) {
                this.riMaintenance.AddVirtualTable('PremiseTechRetentionReasonsLang');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('LanguageCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '=', this.riExchange.LanguageCode(), '', 'Virtual');
                this.riMaintenance.AddVirtualTableKey('TechRetentionReasonCode', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', '', 'Virtual');
                this.riMaintenance.AddVirtualTableField('TechRetentionReasonDesc', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', '');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }

            if (this.pageParams.SCEnableRepeatSalesMatching) {
                this.riMaintenance.AddVirtualTable('Contract', 'MatchedContract');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('ContractNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', 'MatchedContractNumber', 'Virtual');
                this.riMaintenance.AddVirtualTableField('ContractName', MntConst.eTypeText, MntConst.eVirtualKeyStateNormal, 'Virtual', 'MatchedContractName');
                this.riMaintenance.AddVirtualTableCommit(this, () => {
                    setTimeout(() => {
                        if (this.parent.getControlValue('MatchedContractNumber') === '') {
                            this.parent.setControlValue('MatchedContractName', '');
                        }
                    }, 200);
                }, false);

                this.riMaintenance.AddVirtualTable('Premise', 'MatchedPremise');
                this.riMaintenance.AddVirtualTableKeyCS('BusinessCode', MntConst.eTypeCode);
                this.riMaintenance.AddVirtualTableKey('ContractNumber', MntConst.eTypeCode, MntConst.eVirtualKeyStateNormal, '', '', 'MatchedContractNumber', 'Virtual');
                this.riMaintenance.AddVirtualTableKey('PremiseNumber', MntConst.eTypeInteger, MntConst.eVirtualKeyStateNormal, '', '', 'MatchedPremiseNumber', 'Virtual');
                this.riMaintenance.AddVirtualTableField('PremiseName', MntConst.eTypeText, MntConst.eVirtualFieldStateNormal, 'Virtual', 'MatchedPremiseName');
                this.riMaintenance.AddVirtualTableCommit(this, false, false);
            }
        }
    }
}
