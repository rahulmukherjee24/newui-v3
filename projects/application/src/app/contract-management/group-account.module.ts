import { NgModule, Component, ViewContainerRef } from '@angular/core';
import { ContractManagementModuleRoutes } from './../base/PageRoutes';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { GroupAccountMoveComponent } from './AccountAndGroupAccountManagement/iCABSSGroupAccountMove';
import { GroupAccountMaintenanceComponent } from './AccountAndGroupAccountManagement/iCABSSGroupAccountMaintenance';

@Component({
    template: `<router-outlet></router-outlet>
    `
})

export class GroupAccountRootComponent {
    constructor(viewContainerRef: ViewContainerRef) {}
}

@NgModule({
    imports: [
        HttpClientModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: GroupAccountRootComponent, children: [
                    { path: ContractManagementModuleRoutes.ICABSSGROUPACCOUNTMOVE_SUB, component: GroupAccountMoveComponent },
                    { path: ContractManagementModuleRoutes.ICABSSGROUPACCOUNTMAINTENANCE_SUB, component: GroupAccountMaintenanceComponent, canDeactivate: [RouteAwayGuardService] }
            ], data: { domain: 'CONTRACT MANAGEMENT' }
            }

        ])
    ],
    declarations: [
        GroupAccountRootComponent,
        GroupAccountMoveComponent,
        GroupAccountMaintenanceComponent
    ],
    exports: [
        GroupAccountMoveComponent,
        GroupAccountMaintenanceComponent
    ]
})

export class GroupAccountModule { }
