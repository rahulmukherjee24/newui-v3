import { LookUpData } from './../../../shared/services/lookup';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { UserInformationSearchComponent } from './../../internal/search/riMUserInformationSearch.component';
import { ScreenNotReadyComponent } from '../../../shared/components/screenNotReady';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { BusinessSearchComponent } from './../../internal/search/iCABSSBusinessSearch.component';

@Component({
    templateUrl: 'iCABSBUserAuthorityMaintenance.html',
    styles: [
        `.red-bdr span {border-color: red}
    `]
})
export class UserAuthorityMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('promptModal') public promptConfirmModal;
    @ViewChild('errorModal') public errorModal;
    @ViewChild('businessDropDown') businessDropDown: BusinessSearchComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('userInform') public userInform: EllipsisComponent;
    public lookUpSubscription: Subscription;
    public businessOption: Array<any> = [];
    public search: QueryParams;
    public method: string = 'it-functions/admin';
    public module: string = 'user';
    public operation: string = 'Business/iCABSBUserAuthorityMaintenance';
    public inputParams: any = {};
    public isAddMode: boolean = false;
    public showErrorHeader: boolean = true;
    public showMessageHeader: boolean = true;
    public promptContent: string;
    public deleteMode: boolean = false;
    public pageTitle: string;
    public isRequesting: boolean = false;
    public disabledButton: boolean = true;
    public isDeleteDisabled: boolean = true;
    public screenNotReadyComponent: any = ScreenNotReadyComponent;
    public autoOpenUser: boolean = false;
    public isBranchDetailDisabled: boolean = true;
    public validateUserValue: boolean = false;
    public defaultBusinessvalue: string;
    public controls = [
        { name: 'UserCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'UserName', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'DefaultBusinessInd', readonly: false, disabled: false, required: false },
        { name: 'FullAccessInd', readonly: false, disabled: false, required: false },
        { name: 'AllowViewOfSensitiveInfoInd', readonly: false, disabled: false, required: false },
        { name: 'AllowUpdateOfContractInfoInd', readonly: false, disabled: false, required: false },
        { name: 'AllowExportOfInformationInd', readonly: false, disabled: false, required: false },
        { name: 'ContactCreateSecurityLevel', readonly: false, disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'ClosureProcessInd', readonly: false, disabled: false, required: false },
        { name: 'ContactTypeDetailAmendInd', readonly: false, disabled: false, required: false },
        { name: 'ContactTypeDetailAmendInd', readonly: false, disabled: false, required: false },
        { name: 'CreateCallLogInCCMInd', readonly: false, disabled: false, required: false },
        { name: 'BusinessCode', disabled: false, required: true },
        { name: 'BusinessDesc' },
        { name: 'ROWID' }
    ];
    public pageId: string;
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsis: any = {
        userInformation: {
            inputParams: {
                parentMode: 'Search',
                showAddNew: true,
                BranchNumber: ''
            },
            autoOpen: false,
            showHeader: true,
            isEllipsisDisabled: false,
            contentComponent: UserInformationSearchComponent
        }
    };
    public dropDown: any = {
        inputParams: {
            parentMode: 'LookUp'
        },
        active: {
            id: '',
            text: ''
        }
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBUSERAUTHORITYMAINTENANCE;
        this.browserTitle = this.pageTitle = 'User Authority Maintenance';
    }
    ngOnInit(): void {
        super.ngOnInit();
        //     this.pageTitle = 'User Authority Maintenance';
        this.inputParams.method = this.method;
        this.inputParams.module = this.module;
        this.inputParams.operation = this.operation;
        if (this.isReturning()) {
            this.dropDown.active = {
                id: this.getControlValue('BusinessCode'),
                text: this.getControlValue('BusinessCode') + ' - ' + this.getControlValue('BusinessDesc')
            };
            this.isAddMode = false;
            this.enableControls(['UserName']);
            this.disabledButton = false;
            this.isDeleteDisabled = false;
            this.isBranchDetailDisabled = false;
            this.doLookUpForUserAuthority();
        } else {
            this.pageParams.businessList = [];
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    public ngAfterContentInit(): void {
        this.ellipsis['userInformation']['autoOpen'] = !this.isReturning();
    }

    public onUserCodeBlur(event: any): void {
        this.validateUserValue = false;
        if (!this.isAddMode) {
            this.setControlValue('UserName', '');
            this.setControlValue('DefaultBusinessInd', false);
            this.setControlValue('FullAccessInd', false);
            this.setControlValue('AllowViewOfSensitiveInfoInd', false);
            this.setControlValue('AllowUpdateOfContractInfoInd', false);
            this.setControlValue('AllowExportOfInformationInd', false);
            this.setControlValue('ContactCreateSecurityLevel', '');
            this.setControlValue('ClosureProcessInd', false);
            this.setControlValue('ContactTypeDetailAmendInd', false);
            this.setControlValue('CreateCallLogInCCMInd', false);
            this.setControlValue('BusinessCode', '');
            if (event.target.value === '?' || event.target.value === '"') {
                this.validateUserValue = true;
                this.disabledButton = true;
                this.isDeleteDisabled = true;
                this.isBranchDetailDisabled = true;
                this.disableControls(['UserCode']);
                this.setControlValue('UserName', '');
            } else {
                this.validateUserValue = false;
                this.riExchange.riInputElement.SetValue(this.uiForm, 'UserCode', event.target.value.toUpperCase());
                this.dolookUCallForUserCode();
            }
        }

    }

    public onBusinessCodeRecevied(value: any): void {
        if (this.isAddMode) {
            this.businessDropDown.isRequired = true;
            this.businessDropDown.isTriggerValidate = true;
        }
        this.riExchange.riInputElement.SetValue(this.uiForm, 'BusinessCode', value.businessCode);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'BusinessDesc', value.businessDesc);
        this.uiForm.controls['BusinessCode'].markAsDirty();
    }

    public onBusinessCodeChanged(): void {
        if (!this.isAddMode) {
            this.setControlValue('DefaultBusinessInd', false);
            this.setControlValue('FullAccessInd', false);
            this.setControlValue('AllowViewOfSensitiveInfoInd', false);
            this.setControlValue('AllowUpdateOfContractInfoInd', false);
            this.setControlValue('AllowExportOfInformationInd', false);
            this.setControlValue('ContactCreateSecurityLevel', '');
            this.setControlValue('ClosureProcessInd', false);
            this.setControlValue('ContactTypeDetailAmendInd', false);
            this.setControlValue('CreateCallLogInCCMInd', false);
            this.setControlValue('BusinessDesc', this.utils.getBusinessText(this.getControlValue('BusinessCode')));
            this.doLookUpForUserAuthority();
        }
    }

    private dolookUCallForUserCode(): void {
        let lookupIP = [
            {
                'table': 'UserInformation',
                'query': {
                    'UserCode': this.riExchange.riInputElement.GetValue(this.uiForm, 'UserCode')
                },
                'fields': ['UserName']
            }];
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            let UserInformation = data[0][0];
            this.validateUserValue = false;
            if (UserInformation) {
                this.riExchange.riInputElement.SetValue(this.uiForm, 'UserName', UserInformation.UserName);
                this.getBusinessForUser();
                if (!this.isAddMode)
                    this.doLookUpForUserAuthority();
                else {
                    this.promptContent = MessageConstant.Message.ConfirmRecord;
                    this.promptConfirmModal.show();
                }
            } else {
                if (!this.isAddMode) {
                    this.uiForm.reset();
                    //    this.setControlValue('BusinessCode', this.defaultBusinessvalue);
                    this.riExchange.riInputElement.Enable(this.uiForm, 'UserCode');
                    this.disableControls(['UserCode']);
                    this.isRequesting = false;
                    this.disabledButton = true;
                    this.isDeleteDisabled = true;
                    this.isBranchDetailDisabled = true;
                    this.errorModal.show({ msg: MessageConstant.Message.noRecordFound }, false);
                } else {
                    this.validateUserValue = true;
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'UserName', '');
                }
            }
        }).catch(() => {
            this.errorModal.show({ msg: MessageConstant.Message.noRecordFound }, false);
        });
    }

    public onAddClick(): void {
        this.isAddMode = true;
        this.setControlValue('DefaultBusinessInd', false);
        this.setControlValue('FullAccessInd', false);
        this.setControlValue('AllowViewOfSensitiveInfoInd', false);
        this.setControlValue('AllowUpdateOfContractInfoInd', false);
        this.setControlValue('AllowExportOfInformationInd', false);
        this.setControlValue('ContactCreateSecurityLevel', '');
        this.setControlValue('ClosureProcessInd', false);
        this.setControlValue('ContactTypeDetailAmendInd', false);
        this.setControlValue('CreateCallLogInCCMInd', false);
    }

    public onBranchDetailsClick(): void {
        if (this.getControlValue('ROWID')) {
            this.navigate('', InternalGridSearchSalesModuleRoutes.ICABSBUSERAUTHORITYBRANCHGRID);
        } else {
            this.errorModal.show({ msg: MessageConstant.Message.noRecordSelected }, false);
        }
    }

    public doLookUpForUserAuthority(): void {
        this.isRequesting = true;
        let lookupIP = [
            {
                'table': 'UserAuthority',
                'query': {
                    'UserCode': this.riExchange.riInputElement.GetValue(this.uiForm, 'UserCode'),
                    'BusinessCode': this.riExchange.riInputElement.GetValue(this.uiForm, 'BusinessCode')
                },
                'fields': ['DefaultBusinessInd', 'FullAccessInd', 'AllowViewOfSensitiveInfoInd', 'AllowUpdateOfContractInfoInd', 'AllowExportOfInformationInd', 'ContactCreateSecurityLevel', 'ClosureProcessInd', 'ContactTypeDetailAmendInd', 'CreateCallLogInCCMInd']
            }];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let UserAuthority = data[0][0];
            if (UserAuthority) {
                this.isRequesting = false;
                this.riExchange.riInputElement.SetValue(this.uiForm, 'DefaultBusinessInd', UserAuthority.DefaultBusinessInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'FullAccessInd', UserAuthority.FullAccessInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'AllowViewOfSensitiveInfoInd', UserAuthority.AllowViewOfSensitiveInfoInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'AllowUpdateOfContractInfoInd', UserAuthority.AllowUpdateOfContractInfoInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'AllowExportOfInformationInd', UserAuthority.AllowExportofInformationInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'ContactCreateSecurityLevel', UserAuthority.ContactCreateSecurityLevel);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'ClosureProcessInd', UserAuthority.ClosureProcessInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'ContactTypeDetailAmendInd', UserAuthority.ContactTypeDetailAmendInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'CreateCallLogInCCMInd', UserAuthority.CreateCallLogInCCMInd);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'ROWID', UserAuthority.ttUserAuthority);
                this.pageParams['DefaultBusinessInd'] = UserAuthority.DefaultBusinessInd;
                this.pageParams['FullAccessInd'] = UserAuthority.FullAccessInd;
                this.pageParams['AllowViewOfSensitiveInfoInd'] = UserAuthority.AllowViewOfSensitiveInfoInd;
                this.pageParams['AllowUpdateOfContractInfoInd'] = UserAuthority.AllowUpdateOfContractInfoInd;
                this.pageParams['AllowExportOfInformationInd'] = UserAuthority.AllowExportofInformationInd;
                this.pageParams['ContactCreateSecurityLevel'] = UserAuthority.ContactCreateSecurityLevel;
                this.pageParams['ClosureProcessInd'] = UserAuthority.ClosureProcessInd;
                this.pageParams['ContactTypeDetailAmendInd'] = UserAuthority.ContactTypeDetailAmendInd;
                this.pageParams['CreateCallLogInCCMInd'] = UserAuthority.CreateCallLogInCCMInd;
                this.pageParams['ROWID'] = UserAuthority.ttUserAuthority;
                this.disabledButton = false;
                this.isDeleteDisabled = false;
                this.isBranchDetailDisabled = false;
                this.enableControls(['UserName']);
            } else {
                this.riExchange.riInputElement.Enable(this.uiForm, 'BusinessCode');
                this.riExchange.riInputElement.Enable(this.uiForm, 'UserCode');
                this.disableControls(['UserCode', 'BusinessCode']);
                this.isRequesting = false;
                this.disabledButton = true;
                this.isDeleteDisabled = true;
                this.isBranchDetailDisabled = true;
                if (this.getControlValue('BusinessCode') !== '')
                    this.errorModal.show({ msg: MessageConstant.Message.recordNotFound }, false);
            }
            this.formPristine();
        });
    }

    private postDataResponse(): void {
        this.formPristine();
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.BusinessCode, this.getControlValue('BusinessCode'));
        this.search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (!this.deleteMode) {
            if (this.isAddMode) {
                this.search.set(this.serviceConstants.Action, '1');
            } else {
                this.search.set(this.serviceConstants.Action, '2');
            }
            let formdata: Object = {};
            formdata['Table'] = 'UserAuthority';
            formdata['UserCode'] = this.getControlValue('UserCode');
            formdata['DefaultBusinessInd'] = ((this.getControlValue('DefaultBusinessInd') === true) ? 'true' : 'false');
            formdata['FullAccessInd'] = ((this.getControlValue('FullAccessInd') === true) ? 'true' : 'false');
            formdata['AllowViewOfSensitiveInfoInd'] = ((this.getControlValue('AllowViewOfSensitiveInfoInd') === true) ? 'true' : 'false');
            formdata['AllowUpdateOfContractInfoInd'] = ((this.getControlValue('AllowUpdateOfContractInfoInd') === true) ? 'true' : 'false');
            formdata['AllowExportOfInformationInd'] = ((this.getControlValue('AllowExportOfInformationInd') === true) ? 'true' : 'false');
            formdata['ContactCreateSecurityLevel'] = this.getControlValue('ContactCreateSecurityLevel');
            formdata['ClosureProcessInd'] = ((this.getControlValue('ClosureProcessInd') === true) ? 'true' : 'false');
            formdata['ContactTypeDetailAmendInd'] = ((this.getControlValue('ContactTypeDetailAmendInd') === true) ? 'true' : 'false');
            formdata['CreateCallLogInCCMInd'] = ((this.getControlValue('CreateCallLogInCCMInd') === true) ? 'true' : 'false');
            if (!this.isAddMode) {
                formdata['ROWID'] = this.getControlValue('ROWID');
            }
            this.inputParams.search = this.search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.isRequesting = true;
            this.httpService.makePostRequest(this.method, this.module, this.operation, this.search, formdata)
                .subscribe(
                    (data) => {
                        if (data.hasError) {
                            this.errorModal.show(data, true);
                        } else {
                            this.setControlValue('ROWID', data.ttUserAuthority);
                            this.errorModal.show({ msg: MessageConstant.Message.SavedSuccessfully, title: MessageConstant.Message.MessageTitle }, false);
                            this.pageParams.DefaultBusinessInd = this.getControlValue('DefaultBusinessInd');
                            this.pageParams['FullAccessInd'] = this.getControlValue('FullAccessInd');
                            this.pageParams['AllowViewOfSensitiveInfoInd'] = this.getControlValue('AllowViewOfSensitiveInfoInd');
                            this.pageParams['AllowUpdateOfContractInfoInd'] = this.getControlValue('AllowUpdateOfContractInfoInd');
                            this.pageParams['AllowExportOfInformationInd'] = this.getControlValue('AllowExportOfInformationInd');
                            this.pageParams.ContactCreateSecurityLevel = this.getControlValue('ContactCreateSecurityLevel');
                            this.pageParams['ClosureProcessInd'] = this.getControlValue('ClosureProcessInd');
                            this.pageParams['ContactTypeDetailAmendInd'] = this.getControlValue('ContactTypeDetailAmendInd');
                            this.pageParams['CreateCallLogInCCMInd'] = this.getControlValue('CreateCallLogInCCMInd');
                            this.isAddMode = false;
                            this.ellipsis['userInformation']['isEllipsisDisabled'] = false;
                            this.ellipsis['userInformation']['autoOpen'] = false;
                            this.doLookUpForUserAuthority();
                            this.getBusinessForUser();
                        }
                        this.isRequesting = false;
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    },
                    () => {
                        this.isRequesting = false;
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        } else {
            this.search.set(this.serviceConstants.Action, '3');
            let formdata: Object = {};
            formdata['Table'] = 'UserAuthority';
            formdata['ROWID'] = this.getControlValue('ROWID');
            this.inputParams.search = this.search;
            this.isRequesting = true;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.method, this.module, this.operation, this.search, formdata)
                .subscribe(
                    (data) => {
                        this.deleteMode = false;
                        //  this.isAddMode = false;
                        if (data.hasError) {
                            this.errorModal.show(data, true);
                        } else {
                            this.errorModal.show({ msg: MessageConstant.Message.RecordDeletedSuccessfully }, false);
                            this.uiForm.reset();
                            this.disabledButton = true;
                            this.isDeleteDisabled = true;
                            this.isBranchDetailDisabled = true;
                        }
                        this.isRequesting = false;
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    },
                    () => {
                        this.isRequesting = false;
                        this.deleteMode = false;
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }


    public onSaveClick(event: any): void {
        if (!this.cbbService.getBranchListByCountryAndBusiness(this.countryCode(), this.getControlValue('BusinessCode')).length) {
            this.errorModal.show({
                errorMessage: MessageConstant.PageSpecificMessage.noAccessToBusiness.msg,
                fullError: MessageConstant.PageSpecificMessage.noAccessToBusiness.fullError
            }, true);
            return;
        }
        if (this['uiForm'].valid) {
            if (!this.isAddMode) {
                event.preventDefault();
                this.promptContent = MessageConstant.Message.ConfirmRecord;
                this.promptConfirmModal.show();
            } else {
                this.dolookUCallForUserCode();
            }
        } else {
            this.riExchange.riInputElement.isError(this.uiForm, 'ContactCreateSecurityLevel');
            this.riExchange.riInputElement.isError(this.uiForm, 'UserCode');
        }

    }

    public onCancelClick(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'UserCode', true);
        if (!this.isAddMode) {
            this.setControlValue('DefaultBusinessInd', this.pageParams.DefaultBusinessInd);
            this.setControlValue('FullAccessInd', this.pageParams['FullAccessInd']);
            this.setControlValue('AllowViewOfSensitiveInfoInd', this.pageParams['AllowViewOfSensitiveInfoInd']);
            this.setControlValue('AllowUpdateOfContractInfoInd', this.pageParams['AllowUpdateOfContractInfoInd']);
            this.setControlValue('AllowExportOfInformationInd', this.pageParams['AllowExportOfInformationInd']);
            this.setControlValue('ContactCreateSecurityLevel', this.pageParams.ContactCreateSecurityLevel);
            this.setControlValue('ClosureProcessInd', this.pageParams['ClosureProcessInd']);
            this.setControlValue('ContactTypeDetailAmendInd', this.pageParams['ContactTypeDetailAmendInd']);
            this.setControlValue('CreateCallLogInCCMInd', this.pageParams['CreateCallLogInCCMInd']);
            this.setControlValue('ROWID', this.pageParams['ROWID']);
            this.ellipsis['userInformation']['isEllipsisDisabled'] = false;
            this.formPristine();
        } else {
            this.isAddMode = false;
            this.uiForm.reset();
            this.ellipsis['userInformation']['isEllipsisDisabled'] = false;
            this.ellipsis['userInformation']['autoOpen'] = true;
            this.disableControls(['UserCode']);
            this.disabledButton = true;
            this.isDeleteDisabled = true;
            this.isBranchDetailDisabled = true;
        }
    }

    public promptSave(): void {
        this.postDataResponse();
    }

    public onDeleteClick(): void {
        if (this.getControlValue('ROWID')) {
            this.deleteMode = true;
            this.promptContent = MessageConstant.Message.DeleteRecord;
            this.promptConfirmModal.show();
        } else {
            this.errorModal.show({ msg: MessageConstant.Message.noRecordSelected }, false);
        }
    }

    public userInformationOnDataRecieved(data: Object): void {
        this.validateUserValue = false;
        if (data) {
            if (data === 'AddModeOn') {
                this.setControlValue('UserCode', '');
                this.setControlValue('UserName', '');
                this.enableControls(['UserName']);
                this.disabledButton = false;
                this.isDeleteDisabled = true;
                this.isBranchDetailDisabled = true;
                this.isAddMode = true;
                this.ellipsis['userInformation']['isEllipsisDisabled'] = true;
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'UserCode', true);
                this.dropDown.active = {
                    id: '',
                    text: ''
                };
                this.onAddClick();
            } else {
                this.isAddMode = false;
                this.setControlValue('UserCode', data['UserCode']);
                this.setControlValue('UserName', data['UserName']);
                this.uiForm.controls['UserCode'].markAsDirty();
                this.disabledButton = true;
                this.isBranchDetailDisabled = true;
                this.isDeleteDisabled = true;
                this.ellipsis['userInformation']['isEllipsisDisabled'] = false;
                this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'UserCode', true);
                this.onUserCodeBlur({
                    target: {
                        value: data['UserCode']
                    }
                });
            }
        }
    }

    public onModalCloas(): void {
        if (this.isAddMode) {
            this.ellipsis['userInformation']['isEllipsisDisabled'] = true;
        }
    }

    private getBusinessForUser(): void {
        let userAuthQuery = [
            {
                'table': 'UserAuthority',
                'query': {
                    'UserCode': this.getControlValue('UserCode')
                },
                'fields': ['BusinessCode']
            }
        ];
        this.LookUp.lookUpPromise(userAuthQuery).then(data => {
            const structure: LookUpData = {
                table: 'Business',
                fields: ['BusinessCode', 'BusinessDesc'],
                query: {}
            };
            let businessQuery: Array<LookUpData> = [];
            if (data && data[0] && data[0].length) {
                data[0].forEach(item => {
                    businessQuery.push({
                        table: 'Business',
                        fields: ['BusinessCode', 'BusinessDesc'],
                        query: {
                            'BusinessCode': item.BusinessCode
                        }
                    });
                });
            }
            this.LookUp.lookUpPromise(businessQuery).then(businessData => {
                this.pageParams.businessList = [];
                if (businessData && businessData[0]) {
                    businessData.forEach(business => {
                        if (business && business[0]) {
                            this.pageParams.businessList.push({
                                value: business[0].BusinessCode,
                                text: business[0].BusinessCode + ' - ' + business[0].BusinessDesc
                            });
                        }
                    });
                }
            }).catch(error => {
                console.log(error);
            });
        }).catch(error => {
            console.log(error);
        });
    }
}
