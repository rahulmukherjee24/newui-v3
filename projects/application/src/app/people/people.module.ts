import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';
import { PeopleRootComponent } from './people.component';
import { ChangeEmployeeNumberComponent } from './TableMaintenanceBusiness/iCABSAChangeEmployeeNumber';
import { EmployeeMaintenanceComponent } from './TableMaintenanceBusiness/iCABSBEmployeeMaintenance';
import { PeopleRouteDefinitions } from './people.route';
import { UserAuthorityMaintenanceComponent } from './TableMaintenanceBusiness/iCABSBUserAuthorityMaintenance';
import { EmployeeExportGridComponent } from './Exports/EmployeeExportBranch/iCABSAREmployeeExportGrid.component';
import { PrepChargeRateMaintenanceComponent } from './TableMaintenanceBusiness/MaterialChargeRate/iCABSBPrepChargeRateMaintenance.component';

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        PeopleRouteDefinitions
    ],
    declarations: [
        PeopleRootComponent,
        ChangeEmployeeNumberComponent,
        EmployeeMaintenanceComponent,
        UserAuthorityMaintenanceComponent,
        EmployeeExportGridComponent,
        PrepChargeRateMaintenanceComponent,
        EmployeeExportGridComponent
    ]
})

export class PeopleModule { }
