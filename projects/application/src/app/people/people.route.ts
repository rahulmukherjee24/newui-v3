import { RouteAwayGuardService } from './../../shared/services/route-away-guard.service';
import { EmployeeMaintenanceComponent } from './TableMaintenanceBusiness/iCABSBEmployeeMaintenance';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleRootComponent } from './people.component';
import { ChangeEmployeeNumberComponent } from './TableMaintenanceBusiness/iCABSAChangeEmployeeNumber';
import { UserAuthorityMaintenanceComponent } from './TableMaintenanceBusiness/iCABSBUserAuthorityMaintenance';
import { PeopleModuleRoutes } from './../base/PageRoutes';
import { EmployeeExportGridComponent } from './Exports/EmployeeExportBranch/iCABSAREmployeeExportGrid.component';
import { PrepChargeRateMaintenanceComponent } from './TableMaintenanceBusiness/MaterialChargeRate/iCABSBPrepChargeRateMaintenance.component';

const routes: Routes = [{
    path: '',
    component: PeopleRootComponent,
    children: [{
        path: 'tablemaintenance/changeemployeenumber',
        component: ChangeEmployeeNumberComponent
    }, {
        path: 'business/authoritymaintainance',
        component: UserAuthorityMaintenanceComponent,
        canDeactivate: [RouteAwayGuardService]
    }, {
        path: 'business/employeemaintenance',
        component: EmployeeMaintenanceComponent,
        canDeactivate: [RouteAwayGuardService]
    }, {
        path: PeopleModuleRoutes.ICABSAREMPLOYEEEXPORTGRIDBUSINESS,
        component: EmployeeExportGridComponent
    }, {
        path: PeopleModuleRoutes.ICABSAREMPLOYEEEXPORTGRIDREGION,
        component: EmployeeExportGridComponent
    }, {
        path: PeopleModuleRoutes.ICABSAREMPLOYEEEXPORTGRIDBRANCH,
        component: EmployeeExportGridComponent
    }, {
        path: PeopleModuleRoutes.ICABSBPREPCHARGERATEMAINTENANCE,
        component: PrepChargeRateMaintenanceComponent,
        canDeactivate: [RouteAwayGuardService]
    }],
    data: {
        domain: 'PEOPLE'
    }
}];

export const PeopleRouteDefinitions: ModuleWithProviders = RouterModule.forChild(routes);
