export interface IXHRParams {
    method: string;
    module: string;
    operation: string;
}
