export interface IControls {
    name: string;
    value?: string | number | boolean;
    type?: string;
    required?: boolean;
    disabled?: boolean;
}
