
import { ElementRef, HostListener, OnInit, OnDestroy } from '@angular/core';
import { NgControl, ControlContainer, FormControl } from '@angular/forms';

export abstract class BaseDirective implements OnInit, OnDestroy {
    private valueSubscription: any;
    private statusSubscription: any;

    abstract updateFormControl(value: any, calledFromChange?: boolean): void;
    constructor(public el: ElementRef, public control: NgControl, public controlContainer: ControlContainer) { }

    @HostListener('change', ['$event']) onChange(event: any): void {
        let value = this.el.nativeElement.value;
        this.updateFormControl(value, true);
    }

    @HostListener('focus', ['$event']) onFocus(event: any): void {
        this.removeValueSubscription();
    }

    @HostListener('blur', ['$event']) onBlur(event: any): void {
        this.addValueSubscription();
    }

    public ngOnInit(): void {
        this.initialCall();
        this.addValueSubscription();
    }

    public ngOnDestroy(): void {
        this.removeValueSubscription();
        if (this.statusSubscription) {
            this.statusSubscription.unsubscribe();
        }
    }

    private initialCall(): void {
        if (this.controlContainer && this.controlContainer['form'] && this.control && this.controlContainer['form'].controls[this.control.name]) {
            this.updateFormControl(this.controlContainer['form'].controls[this.control.name].value);
        }
    }

    private addValueSubscription(): void {
        this.removeValueSubscription();
        this.valueSubscription = this.control.valueChanges.distinctUntilChanged().subscribe((value) => {
            this.updateFormControl(value);
        });
    }

    private removeValueSubscription(): void {
        if (this.valueSubscription) {
            this.valueSubscription.unsubscribe();
        }
    }

    public setError(): void {
        this.controlContainer['form'].controls[this.control.name].markAsDirty();
        this.controlContainer['form'].controls[this.control.name].setErrors({
            status: 'INVALID'
        });
    }

    public required(control: any): boolean {
        let _validator: any = control.validator && control.validator(new FormControl());
        return _validator && _validator.required;
    }

    public sanitizeValue(value: any, calledFromChange?: boolean): any {
        if (!calledFromChange) {
            let commaIndex = value.toString().indexOf(',');
            let dotIndex = value.toString().indexOf('.');
            if (commaIndex >= 0 && dotIndex >= 0 && dotIndex > commaIndex) {
                value = value.toString().replace(/,/g, '');
            }
        }
        return value;
    }
}

