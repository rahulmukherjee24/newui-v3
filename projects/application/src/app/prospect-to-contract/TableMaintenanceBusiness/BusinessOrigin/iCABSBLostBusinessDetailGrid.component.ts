import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { PaginationComponent } from '@shared/components/pagination/pagination';

import { AjaxConstant } from '@shared/constants/AjaxConstants';
import { BaseComponent } from '@base/BaseComponent';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { InternalMaintenanceSalesModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from '@base/PageRoutes';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBLostBusinessDetailGrid.html'
})
export class LostBusinessDetailGridComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    [x: string]: any;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('pagination') pagination: PaginationComponent;

    public controls: Array<any> = [
        { name: 'ActiveOnly', readonly: false, disabled: false, required: false },
        { name: 'BusinessDesc', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'LostBusinessSearchType', readonly: false, disabled: false, required: false },
        { name: 'LostBusinessSearchValue', readonly: false, disabled: false, required: false },
        { name: 'menu', readonly: false, disabled: false, required: false }
    ];
    public currPage: number = 1;
    public isRequesting: boolean = false;
    public itemsPerPage: number = 10;
    public maxColumn: number = 5;
    public pageId: string = '';
    public totalRecords: number = 1;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBLOSTBUSINESSDETAILGRID;
        this.browserTitle = this.pageTitle = 'Lost Business Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.currentPage = this.pageParams.currentPage || 1;
        this.currPage = parseInt(this.pageParams.currentPage, 10);
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public ngAfterContentInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.setControlValue('ActiveOnly', true);
            this.setControlValue('LostBusinessSearchType', 'LostBusinessCode');
        }
        this.setControlValue('BusinessDesc', this.utils.getBusinessCode() + '-' + this.utils.getBusinessText());
        this.setControlValue('menu', '');
        this.buildGrid();
    }

    //Grid
    public buildGrid(): void {
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.DefaultTextColor = '0000FF';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;

        this.riGrid.Clear();
        this.riGrid.AddColumn('LostBusinessCode', 'LostBusinessDetail', 'LostBusinessCode', MntConst.eTypeCode, 8, false, '');
        this.riGrid.AddColumnOrderable('LostBusinessCode', true);
        this.riGrid.AddColumn('LostBusinessDesc', 'LostBusinessDetail', 'LostBusinessDesc', MntConst.eTypeText, 40, false, '');
        this.riGrid.AddColumnOrderable('LostBusinessDesc', true);
        this.riGrid.AddColumn('LostBusinessDetailCode', 'LostBusinessDetail', 'LostBusinessDetailCode', MntConst.eTypeCode, 8, false, '');
        this.riGrid.AddColumnOrderable('LostBusinessDetailCode', true);
        this.riGrid.AddColumn('LostBusinessDetailDesc', 'LostBusinessDetail', 'LostBusinessDetailDesc', MntConst.eTypeText, 40, false, '');
        this.riGrid.AddColumnOrderable('LostBusinessDetailDesc', true);
        this.riGrid.AddColumn('InvalidForNew', 'LostBusinessDetail', 'InvalidForNew', MntConst.eTypeImage, 1, false, '');
        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    public riGridBeforeExecute(): void {
        let postParams: any = {};
        let search: QueryParams = this.getURLSearchParamObject();
        postParams.methodtype = 'grid';
        postParams.Level = 'Detail';
        search.set(this.serviceConstants.Action, '2');
        search.set('ActiveOnly', this.getControlValue('ActiveOnly'));
        search.set('SearchType', this.getControlValue('LostBusinessSearchType'));
        search.set('SearchValue', this.getControlValue('LostBusinessSearchValue'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.PageCurrent, this.pageParams.currentPage.toString());
        search.set(this.serviceConstants.GridHandle, this.utils.gridHandle);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        let sortOrder: any = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set('riSortOrder', sortOrder);

        this.ajaxSource.next(AjaxConstant.START);
        let xhr = {
            module: 'retention',
            method: 'ccm/maintenance',
            operation: 'Business/iCABSBLostBusinessDetailGrid'
        };
        this.httpService.makePostRequest(xhr.method, xhr.module, xhr.operation, search, postParams)
            .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.errorService.emitError(data);
                    } else {
                        this.pageParams.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.currPage = parseInt(this.pageParams.currentPage, 10);
                        this.pagination.currentPage = this.currPage;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                        this.ajaxSource.next(AjaxConstant.COMPLETE);
                    }
                },
                (error) => {
                    this.errorService.emitError(error);
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                });
    }
    //Grid Refresh
    public btnRefresh(): void {
        this.pageParams.currentPage = this.pageParams.currentPage || 1;
        this.currPage = parseInt(this.pageParams.currentPage, 10);
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    // pagination current page
    public getCurrentPage(currentPage: any): void {
        if (this.pageParams.currentPage === currentPage.value) {
            return;
        }
        this.pageParams.currentPage = currentPage.value;
        this.currPage = parseInt(this.pageParams.currentPage, 10);
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public onDblClick(event: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'LostBusinessDetailCode':
                this.navigate('LostBusinessDetailUpdate', InternalMaintenanceApplicationModuleRoutes.ICABSBLOSTBUSINESSDETAILMAINTENANCE, { 'ROWID': this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid') });
                break;
            case 'LostBusinessCode':
                this.navigate('LostBusinessUpdate', InternalMaintenanceSalesModuleRoutes.ICABSBLOSTBUSINESSMAINTENANCE, { 'ROWID': this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'rowid') });
                break;
        }
    }

    public menuOnchange(event: any): void {
        switch (event) {
            case 'Add':
                this.navigate('LostBusinessDetailAdd', InternalMaintenanceApplicationModuleRoutes.ICABSBLOSTBUSINESSDETAILMAINTENANCE, {});
                break;
        }
    }

    public riGrid_Sort(event: any): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
}
