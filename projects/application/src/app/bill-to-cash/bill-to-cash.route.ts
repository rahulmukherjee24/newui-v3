import { InvoiceDetailsMaintainanceComponent } from './../internal/maintenance/iCABSAInvoiceDetailsMaintenance';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillToCashRootComponent } from './bill-to-cash.component';
import { BillToCashModuleRoutes } from '../base/PageRoutes';
import { ARGenerateNextInvoiceRunForecastComponent } from './InvoiceAndAPIReporting/NextInvoiceRun-ForecastGeneration/iCABSARGenerateNextInvoiceRunForecast.component';
import { CreditAndReInvoiceGridComponent } from './InvoicedAndCreditReporting/iCABSACreditAndReInvoiceGrid';
import { ApiDateMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAAPIDateMaintenance';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';
import { ApiReverseMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAAPIReverse';
import { ApiGenerationMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAApplyAPIGeneration';
import { ApplyApiGridComponent } from './InvoiceProductionAndAPI/iCABSAApplyApiGrid';
import { ContractAPIMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSAContractAPIMaintenance.component';
import { ServiceCoverAPIGridComponent } from './InvoiceProductionAndAPI/iCABSAServiceCoverAPIGrid';
import { APICodeMaintenanceComponent } from './InvoiceProductionAndAPI/iCABSBAPICodeMaintenance.component';
import { InvoiceRunDatesGridComponent } from './InvoiceProductionAndAPI/iCABSBInvoiceRunDatesGrid';
import { ReleaseForInvoiceGridComponent } from './JobReleaseInvoice/Employee/iCABSReleaseForInvoiceGrid.component';
import { PortfolioGeneralMaintenanceComponent } from './Portfolio/iCABSAPortfolioGeneralMaintenance.component';
import { ActualVsContractualBranchComponent } from './Portfolio/iCABSSeActualVsContractualBranch';
import { ActualVsContractualBusinessComponent } from './Portfolio/iCABSSeActualVsContractualBusiness';
import { ActualVsContractualServiceCoverComponent } from './Portfolio/iCABSSeActualVsContractualServiceCover.component';
import { CreditAndReInvoiceMaintenanceComponent } from './PostInvoiceManagement/CreditAndReInvoice/iCABSACreditAndReInvoiceMaintenance.component';
import { ContractInvoiceGridComponent } from './PostInvoiceManagement/iCABSAContractInvoiceGrid';
import { ServiceCoverAcceptGridComponent } from './PostInvoiceManagement/iCABSAServiceCoverAcceptGrid.component';
import { ServiceVisitReleaseGridComponent } from './PreInvoiceManagement/SharedVisitRelease/iCABSSeServiceVisitReleaseGrid.component';
import { InvoiceGroupMaintenanceComponent } from './PreInvoiceManagement/iCABSAInvoiceGroupMaintenance.component';
import { TaxRegistrationChangeComponent } from './PreInvoiceManagement/iCABSATaxRegistrationChange.component';
import { VarianceFromPriceCostGridComponent } from './SalesReport/iCABSARVarianceFromPriceCostGrid.component';

const routes: Routes = [
    {
        path: '', component: BillToCashRootComponent, children: [
            { path: BillToCashModuleRoutes.ICABSARGENERATENEXTINVOICERUNFORECAST, component: ARGenerateNextInvoiceRunForecastComponent },
            { path: 'InvoicedAndCreditReporting/CreditAndReInvoiceGrid', component: CreditAndReInvoiceGridComponent },
            { path: 'apidate', component: ApiDateMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'contract/apireverse', component: ApiReverseMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'premise/apireverse', component: ApiReverseMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'servicecover/apireverse', component: ApiReverseMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'apigeneration', component: ApiGenerationMaintenanceComponent },
            { path: 'apigrid', component: ApplyApiGridComponent },
            { path: 'contract/apiexempt', component: ContractAPIMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'serviceCoverApiGrid', component: ServiceCoverAPIGridComponent },
            { path: 'apicodemaintenance', component: APICodeMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'rundatesgrid', component: InvoiceRunDatesGridComponent },
            { path: 'rundatesgrid/view', component: InvoiceRunDatesGridComponent },
            { path: 'application/releaseforinvoiceGrid', component: ReleaseForInvoiceGridComponent },
            { path: 'portfolio/generalMaintenance', component: PortfolioGeneralMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'actualvscontractualbranch', component: ActualVsContractualBranchComponent },
            { path: 'actualvscontractualbusiness', component: ActualVsContractualBusinessComponent },
            { path: 'actualvscontractualservicecover', component: ActualVsContractualServiceCoverComponent },
            { path: 'postInvoiceManagement/creditAndReInvoiceMaintenance', component: CreditAndReInvoiceMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'contract/invoice', component: ContractInvoiceGridComponent },
            { path: BillToCashModuleRoutes.ICABSASERVICECOVERACCEPTGRID, component: ServiceCoverAcceptGridComponent },
            { path: BillToCashModuleRoutes.ICABSAINVOICEDETAILSMAINTENANCE, component: InvoiceDetailsMaintainanceComponent },
            { path: BillToCashModuleRoutes.ICABSSESERVICEVISITRELEASEGRID, component: ServiceVisitReleaseGridComponent },
            { path: BillToCashModuleRoutes.ICABSAINVOICEGROUPMAINTENANCE, component: InvoiceGroupMaintenanceComponent, canDeactivate: [RouteAwayGuardService] },
            { path: 'application/taxregistrationchange', component: TaxRegistrationChangeComponent, canDeactivate: [RouteAwayGuardService] },
            { path: BillToCashModuleRoutes.ICABSARVARIANCEFROMPRICECOSTGRID, component: VarianceFromPriceCostGridComponent }
        ], data: { domain: 'BILL TO CASH' }
    }
];

export const BillToCashRouteDefinitions: ModuleWithProviders = RouterModule.forChild(routes);
