import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BranchServiceAreaSearchComponent } from './../../internal/search/iCABSBBranchServiceAreaSearch';
import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSARVarianceFromPriceCostGrid.html'
})

export class VarianceFromPriceCostGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('ReportType') reportType: any;

    private loadNewData: boolean = false;
    private curPage: number = 1;
    private gridHandle: string;
    private headerParams: any = {
        operation: 'ApplicationReport/iCABSARVarianceFromPriceCostGrid',
        module: 'report',
        method: 'bill-to-cash/grid'
    };
    public totalRecords: number = 1;
    public itemsPerPage: number = 10;
    public pageId: string = '';
    public controls: any = [
        { name: 'ReportType' },
        { name: 'ContractorJob' },
        { name: 'ServiceBranchNumber', required: true, disabled: true, type: MntConst.eTypeInteger },
        { name: 'ServiceBranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'PercentageOverUnder', required: true, type: MntConst.eTypeDecimal2 }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARVARIANCEFROMPRICECOSTGRID;
    }

    public ellipsisConfig: any = {
        contract: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showCountry': false,
                'showBusiness': false,
                'showAddNew': false
            },
            component: ContractSearchComponent
        },
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'BranchNumberServiceBranchNumber': this.utils.getBranchCode(),
                'ServiceBranchNumber': this.utils.getBranchCode(),
                'BranchName': this.utils.getBranchTextOnly(this.utils.getBranchCode())
            },
            component: BranchServiceAreaSearchComponent
        }
    };

    public dropdownConfig: any = {
        negBranch: {
            inputParams: {
                'parentMode': 'LookUp'
            },
            triggerValidate: false,
            active: {
                id: '',
                text: ''
            }
        }
    };


    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Variance From Price/Cost Equality';
        this.utils.setTitle(this.pageTitle);
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.riGrid.HighlightBar = true;
        this.buildGrid();

        this.gridHandle = this.utils.gridHandle;
        this.setControlValue('ReportType', 'Actual');
        this.setControlValue('ContractorJob', 'Contract');
        this.setControlValue('ServiceBranchNumber', this.utils.getBranchCode());
        this.setControlValue('ServiceBranchName', this.utils.getBranchTextOnly(this.utils.getBranchCode()));
        this.setControlValue('PercentageOverUnder', '0.00');

        this.ellipsisConfig.contract.childConfigParams.currentContractType = this.riExchange.getCurrentContractType();
        this.ellipsisConfig.serviceArea.childConfigParams.ServiceBranchNumber = this.utils.getBranchCode();
        this.ellipsisConfig.serviceArea.childConfigParams.ServiceBranchName = this.utils.getBranchText();

        this.reportType.nativeElement.focus();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'Variance', 'BranchServiceAreaSeqNo', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractYearEndDate', 'Variance', 'ContractYearEndDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ContractYearEndDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceCommenceDate', 'Variance', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('InvoiceAnniversaryDate', 'Variance', 'InvoiceAnniversaryDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('InvoiceAnniversaryDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('InvoiceAnniversaryDate', false);

        this.riGrid.AddColumn('NegBranchNumber', 'Variance', 'NegBranchNumber', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumnAlign('NegBranchNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContractNumber', 'Variance', 'ContractNumber', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'Variance', 'PremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseAddressLine1', 'Variance', 'PremiseAddressLine1', MntConst.eTypeText, 15);

        this.riGrid.AddColumn('ProductCode', 'Variance', 'ProductCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitsDue', 'Variance', 'VisitsDue', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('VisitsDue', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('RoutineVisitsDone', 'Variance', 'RoutineVisitsDone', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('RoutineVisitsDone', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('OtherVisitsDone', 'Variance', 'OtherVisitsDone', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('OtherVisitsDone', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('CreditVisitsDone', 'Variance', 'CreditVisitsDone', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('CreditVisitsDone', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ExpectedInvoiceValue', 'Variance', 'ExpectedInvoiceValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumn('ForecastCostValue', 'Variance', 'ForecastCostValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumn('DifferenceValue', 'Variance', 'DifferenceValue', MntConst.eTypeDecimal2, 10);
        this.riGrid.AddColumnNoWrap('DifferenceValue', true);

        this.riGrid.AddColumn('Percentage', 'Variance', 'Percentage', MntConst.eTypeDecimal2, 5);
        this.riGrid.AddColumnNoWrap('Percentage', true);

        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('NegBranchNumber', true);

        this.riGrid.Complete();
    }

    private updateHTMLDocument(): void {
        this.riGrid.Update = true;
        this.fetchGridData();
    }

    private selectedRowFocus(srcElement: any): void {
        this.setAttribute('SelectedRow', this.riGrid.CurrentRow);
        this.setAttribute('Cell', this.riGrid.CurrentCell);
        this.setAttribute('SelectedRowID', srcElement.getAttribute('RowID'));
        srcElement.focus();
    }

    private get validateScreenParameters(): boolean {
        let blnReturn: boolean = true;
        blnReturn = this.riExchange.validateForm(this.uiForm);

        return blnReturn;
    }

    public fetchGridData(): void {
        //if (this.validateScreenParameters) {
        this.riGrid.RefreshRequired();
        let gridParams: QueryParams = this.getURLSearchParamObject();
        gridParams.set(this.serviceConstants.Action, '6');

        gridParams.set('ServiceBranchNumber', this.getControlValue('ServiceBranchNumber'));
        gridParams.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridParams.set('BranchNumber', this.getControlValue('BranchNumber'));
        gridParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        gridParams.set('PercentageOverUnder', this.getControlValue('PercentageOverUnder'));
        gridParams.set('ContractorJob', this.getControlValue('ContractorJob'));
        gridParams.set('ReportType', this.getControlValue('ReportType'));

        gridParams.set(this.serviceConstants.GridMode, '0');
        gridParams.set(this.serviceConstants.GridHandle, this.gridHandle);
        if (this.loadNewData) {
            gridParams.set(this.serviceConstants.GridCacheRefresh, 'true');
            this.loadNewData = false;
        }
        gridParams.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        gridParams.set(this.serviceConstants.PageCurrent, this.curPage.toString());
        gridParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridParams.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, gridParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.curPage = 1;
                    this.totalRecords = 1;
                    this.riGrid.ResetGrid();
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                } else {
                    if (this.riGrid.Update) {
                        this.riGrid.StartRow = this.getAttribute('SelectedRow');
                        this.riGrid.StartColumn = 0;
                        this.riGrid.RowID = this.getAttribute('SelectedRowID');
                        this.riGrid.UpdateHeader = false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                    } else {
                        this.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    }
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.errorService.emitError(error);
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
        //}
    }

    public gridAfterExecute(): void {
        if (!this.riGrid.Update) {
            if (this.riGrid.HTMLGridBody.children[0]) {
                this.selectedRowFocus(this.riGrid.HTMLGridBody.children[0].children[0].children[0]);
            }
        }
    }

    public gridBodyOnClick(event: any): void {
        this.selectedRowFocus(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
    }

    public negBranchOnChange(data: any): void {
        this.setControlValue('BranchNumber', data.BranchNumber);
        this.setControlValue('BranchName', data.BranchName);
    }

    public branchServiceAreaCodeOnChange(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '0');

            let bodyParams: any = {};
            bodyParams['PostDesc'] = 'BranchServiceArea';
            bodyParams[this.serviceConstants.ServiceBranchNumber] = this.getControlValue('ServiceBranchNumber');
            bodyParams[this.serviceConstants.BranchServiceAreaCode] = this.getControlValue('BranchServiceAreaCode');
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }

                    this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                });
        } else {
            this.setControlValue('BranchServiceAreaCode', '');
            this.setControlValue('BranchServiceAreaDesc', '');
        }
    }

    public contractNumberOnChange(): void {
        if (this.getControlValue('ContractNumber')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '0');

            let bodyParams: any = {};
            bodyParams['PostDesc'] = 'Contract';
            bodyParams[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchParams, bodyParams).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }

                    this.setControlValue('ContractName', data.ContractName);
                });
        } else {
            this.setControlValue('ContractNumber', '');
            this.setControlValue('ContractName', '');
        }
    }

    public percentageOverUnderOnChange(): void {
        this.riGrid.RefreshRequired();
    }

    public fetchEllipsisData(data: any, type: string): void {
        switch (type) {
            case 'contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                break;

            case 'serviceArea':
                this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                break;
        }
        this.fetchGridData();
    }

    public getCurrentPage(data: any): void {
        this.curPage = data.value;
        this.fetchGridData();
    }

    public refreshGrid(): void {
        this.loadNewData = true;
        this.fetchGridData();
    }
}
