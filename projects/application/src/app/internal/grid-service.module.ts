import { Component, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { InternalGridSearchServiceModuleRoutesConstant } from '../base/PageRoutes';
import { InternalSearchModule } from './search.module';
import { SharedModule } from './../../shared/shared.module';
import { RouteAwayGuardService } from '../../shared/services/route-away-guard.service';

import { CustomerContactHistoryGridComponent } from './grid-search/iCABSCMCustomerContactHistoryGrid';
import { ServiceVisitSummaryComponent } from './grid-search/iCABSAServiceVisitSummary';
import { InfestationToleranceGridComponent } from './grid-search/iCABSSInfestationToleranceGrid';
import { WorkOrderGridComponent } from './grid-search/iCABSCMWorkOrderGrid.component';
import { ServiceCoverDetailLocationEntryGridComponent } from './grid-search/iCABSAServiceCoverDetailLocationEntryGrid';
import { ServicePlannedDatesGridComponent } from './grid-search/iCABSSeServicePlannedDatesGrid.component';
import { ServiceVisitPlanningGridComponent } from './grid-search/iCABSAServiceVisitPlanningGrid.component';
import { SeHCANewLocationGridComponent } from './grid-search/iCABSSeHCANewLocationGrid.component';
import { SOProspectGridComponent } from './grid-search/iCABSSSOProspectGrid';
import { ClosedTemplateBranchAccessGridComponent } from './grid-search/iCABSAClosedTemplateBranchAccessGrid.component';
import { DiaryYearGridComponent } from './grid-search/iCABSADiaryYearGrid.component';
import { ServiceCoverDisplayEntryComponent } from './grid-search/iCABSAServiceCoverDisplayEntry';
import { PlanVisitTabularComponent } from './grid-search/iCABSAPlanVisitTabular';
import { SeasonalTemplateDetailGridComponent } from './grid-search/iCABSASeasonalTemplateDetailGrid.component';
import { ContractServiceSummaryComponent } from './grid-search/iCABSAContractServiceSummary/iCABSAContractServiceSummary';
import { SeServiceAreaDetailGridComponent } from './grid-search/iCABSSeServiceAreaDetailGrid.component';
import { ServicePlanningGridComponent } from './grid-search/iCABSSeServicePlanningGrid.component';
import { BranchSummaryPlanReserveStockComponent } from './grid-search/branchSummaryPlanReserveStock/iCABSABranchSummaryPlanReserveStock.component';
import { ServiceVisitAnnivDateMaintenanceComponent } from './grid-search/iCABSSeServiceVisitAnnivDateMaintenance.component';
import { VisitDateDiscrepancyGridComponent } from './grid-search/iCABSSeVisitDateDiscrepancyGrid.component';

import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule } from '../internal/search-ellipsis-business.module';

@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridServiceComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridServiceComponent, children: [
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSCMCUSTOMERCONTACTHISTORYGRID, component: CustomerContactHistoryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICEVISITSUMMARY, component: ServiceVisitSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSINFESTATIONTOLERANCEGRID, component: InfestationToleranceGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSCMWORKORDERGRID, component: WorkOrderGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICECOVERDETAILLOCATIONENTRYGRID, component: ServiceCoverDetailLocationEntryGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNEDDATESGRID, component: ServicePlannedDatesGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICEVISITPLANNINGGRID, component: ServiceVisitPlanningGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEHCANEWLOCATIONGRID, component: SeHCANewLocationGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSSOPROSPECTGRID, component: SOProspectGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSACLOSEDTEMPLATEBRANCHACCESSGRID, component: ClosedTemplateBranchAccessGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSADIARYYEARGRID, component: DiaryYearGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSASERVICECOVERDISPLAYENTRY, component: ServiceCoverDisplayEntryComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSAPLANVISITTABULAR, component: PlanVisitTabularComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSASEASONALTEMPLATEDETAILGRID, component: SeasonalTemplateDetailGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSACONTRACTSERVICESUMMARY, component: ContractServiceSummaryComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEAREADETAILGRID, component: SeServiceAreaDetailGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEPLANNINGGRID, component: ServicePlanningGridComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSABRANCHSESERVICEPLANSUMMARYRESERVESTOCK, component: BranchSummaryPlanReserveStockComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSESERVICEVISITANNIVDATEMAINTENANCE, component: ServiceVisitAnnivDateMaintenanceComponent },
                    { path: InternalGridSearchServiceModuleRoutesConstant.ICABSSEVISITDATEDISCREPANCYGRID, component: VisitDateDiscrepancyGridComponent }
                ]
            }
        ])
    ],
    declarations: [
        InternalGridServiceComponent,
        CustomerContactHistoryGridComponent,
        ServiceVisitSummaryComponent,
        InfestationToleranceGridComponent,
        WorkOrderGridComponent,
        ServiceCoverDetailLocationEntryGridComponent,
        ServicePlannedDatesGridComponent,
        ServiceVisitPlanningGridComponent,
        SeHCANewLocationGridComponent,
        SOProspectGridComponent,
        ClosedTemplateBranchAccessGridComponent,
        DiaryYearGridComponent,
        ServiceCoverDisplayEntryComponent,
        PlanVisitTabularComponent,
        SeasonalTemplateDetailGridComponent,
        ContractServiceSummaryComponent,
        SeServiceAreaDetailGridComponent,
        ServicePlanningGridComponent,
        BranchSummaryPlanReserveStockComponent,
        ServiceVisitAnnivDateMaintenanceComponent,
        VisitDateDiscrepancyGridComponent
    ]
})

export class InternalGridServiceModule { }
