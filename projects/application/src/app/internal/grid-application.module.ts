import { LeadGridComponent } from './grid-search/iCABSCMHCALeadGrid';

import { HttpClientModule } from '@angular/common/http';
import { Component, ViewChild, NgModule, ViewContainerRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CMCustomerContactDetailGridComponent } from './grid-search/iCABSCMCustomerContactDetailGrid.component';
import { TeleSalesOrderLineGridComponent } from './grid-search/iCABSATeleSalesOrderLineGrid.component';
import { CallCentreGridEmployeeViewComponent } from './grid-search/iCABSCMCallCentreGridEmployeeView.component';
import { ServiceCoverWasteGridComponent } from './grid-search/iCABSAServiceCoverWasteGrid.component';
import { ServiceCoverSeasonGridComponent } from './grid-search/iCABSAServiceCoverSeasonGrid';
import { CustomerContactNotificationsGridComponent } from './grid-search/iCABSCMCustomerContactNotificationsGrid.component';
import { ServiceCoverClosedDateGridComponent } from './grid-search/iCABSAServiceCoverClosedDateGrid.component';
import { ServiceCoverCalendarDateComponent } from './grid-search/iCABSAServiceCoverCalendarDateGrid.component';
import { RouteAwayGuardService } from './../../shared/services/route-away-guard.service';
import { CalendarTemplateBranchAccessGridComponent } from './grid-search/iCABSACalendarTemplateBranchAccessGrid.component';
import { PNOLLevelHistoryComponent } from './grid-search/iCABSAPNOLLevelHistory.component';
import { SePremisePDAICABSActivityGridComponent } from './grid-search/iCABSSePremisePDAICABSActivityGrid.component';
import { InvoiceByAccountGridComponent } from './grid-search/iCABSAInvoiceByAccountGrid.component';
import { AccountReviewGridComponent } from './../customer-contact-management/ReportsContact/iCABSCMAccountReviewGrid.component';
import { CallCentreSearchEmployeeGridComponent } from './grid-search/iCABSCMCallCentreSearchEmployeeGrid.component';
import { InternalGridSearchApplicationModuleRoutesConstant } from './../base/PageRoutes';
import { PremisePostcodeRezoneGridComponent } from './grid-search/iCABSBPremisePostcodeRezoneGrid.component';
import { InternalSearchModule } from './search.module';
import { SharedModule } from './../../shared/shared.module';


@Component({
    template: `<router-outlet></router-outlet>
    <icabs-modal #errorModal="child" [(showHeader)]="showErrorHeader" [config]="{backdrop: 'static'}"></icabs-modal>`
})

export class InternalGridApplicationComponent {
    @ViewChild('errorModal') public errorModal;
    public showErrorHeader: boolean = true;
    constructor(viewContainerRef: ViewContainerRef) {
    }
}

@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        RouterModule.forChild([
            {
                path: '', component: InternalGridApplicationComponent, children: [
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSBPREMISEPOSTCODEREZONEGRID, component: PremisePostcodeRezoneGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTRESEARCHEMPLOYEEGRID, component: CallCentreSearchEmployeeGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMACCOUNTREVIEWGRID, component: AccountReviewGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSAINVOICEBYACCOUNTGRID, component: InvoiceByAccountGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSSEPREMISEPDAICABSACTIVITYGRID, component: SePremisePDAICABSActivityGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSAPNOLLEVELHISTORY, component: PNOLLevelHistoryComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSACALENDARTEMPLATEBRANCHACCESSGRID, component: CalendarTemplateBranchAccessGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERCALENDARDATEGRID, component: ServiceCoverCalendarDateComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERCLOSEDDATEGRID, component: ServiceCoverClosedDateGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCUSTOMERCONTACTNOTIFICATIONSGRID, component: CustomerContactNotificationsGridComponent, canDeactivate: [RouteAwayGuardService] },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERSEASONGRID, component: ServiceCoverSeasonGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSASERVICECOVERWASTEGRID, component: ServiceCoverWasteGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCALLCENTERGRIDEMPLOYEEVIEW, component: CallCentreGridEmployeeViewComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMHCALEADGRID, component: LeadGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSATELESALESORDERLINEGRID, component: TeleSalesOrderLineGridComponent },
                    { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSCMCUSTOMERCONTACTDETAILGRID, component: CMCustomerContactDetailGridComponent }
                ]
            }
        ])
    ],
    declarations: [
        InternalGridApplicationComponent,
        PremisePostcodeRezoneGridComponent,
        CallCentreSearchEmployeeGridComponent,
        AccountReviewGridComponent,
        InvoiceByAccountGridComponent,
        SePremisePDAICABSActivityGridComponent,
        PNOLLevelHistoryComponent,
        CalendarTemplateBranchAccessGridComponent,
        ServiceCoverCalendarDateComponent,
        ServiceCoverClosedDateGridComponent,
        CustomerContactNotificationsGridComponent,
        ServiceCoverSeasonGridComponent,
        ServiceCoverWasteGridComponent,
        CallCentreGridEmployeeViewComponent,
        LeadGridComponent,
        TeleSalesOrderLineGridComponent,
        CMCustomerContactDetailGridComponent
    ]
})

export class InternalGridApplicationModule { }
