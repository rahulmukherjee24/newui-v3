import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';

import { APICodeSearchComponent } from './search/iCABSBAPICodeSearch';
import { ContactTypeSearchComponent } from './search/iCABSSContactTypeSearch';
import { CountryCodeComponent } from './search/iCABSACountryCodeSearch';
import { BusinessSearchComponent } from './search/iCABSSBusinessSearch.component';
import { BCompanySearchComponent } from './search/iCABSBCompanySearch';
import { TierSearchComponent } from './search/iCABSBTierSearch';
import { LogoTypeSearchComponent } from './search/iCABSBLogoTypeSearch';
import { VisitTypeSearchComponent } from './search/iCABSBVisitTypeSearch.component';
import { BusinessOriginLangSearchComponent } from './search/iCABSBBusinessOriginLanguageSearch.component';
import { LostBusinessRequestOriginLanguageSearchComponent } from './search/iCABSSLostBusinessRequestOriginLanguageSearch.component';
import { LostBusinessDetailLanguageSearchComponent } from './search/iCABSBLostBusinessDetailLanguageSearch.component';
import { LostBusinessLanguageSearchComponent } from './search/iCABSBLostBusinessLanguageSearch.component';
import { ServiceTypeSearchComponent } from './search/iCABSBServiceTypeSearch.component';
import { ContractTypeLanguageSearchComponent } from './search/iCABSBContractTypeLanguageSearch.component';
import { ProductServiceGroupSearchComponent } from './search/iCABSBProductServiceGroupSearch.component';
import { ComponentTypeLanguageSearchComponent } from './search/iCABSBComponentTypeLanguageSearch.component';
import { CustomerCategorySearchComponent } from './search/iCABSBCustomerCategorySearch.component';
import { InvoiceCreditReasonLanguageSearchComponent } from './search/iCABSSInvoiceCreditReasonLanguageSearch.component';
import { RMMCategoryLanguageSearchComponent } from './search/iCABSARMMCategoryLanguageSearch.component';
import { LOSSearchComponent } from './search/iCABSSLOSSearch.component';
import { NotificationGroupSearchComponent } from './search/iCABSSNotificationGroupSearch.component';
import { ContactTypeDetailSearchDropDownComponent } from './search/iCABSSContactTypeDetailSearch.component';

@NgModule({
    exports: [
        NotificationGroupSearchComponent,
        CountryCodeComponent,
        LOSSearchComponent,
        RMMCategoryLanguageSearchComponent,
        InvoiceCreditReasonLanguageSearchComponent,
        CustomerCategorySearchComponent,
        ComponentTypeLanguageSearchComponent,
        BusinessSearchComponent,
        ProductServiceGroupSearchComponent,
        ContractTypeLanguageSearchComponent,
        ServiceTypeSearchComponent,
        LostBusinessLanguageSearchComponent,
        APICodeSearchComponent,
        LostBusinessDetailLanguageSearchComponent,
        LostBusinessRequestOriginLanguageSearchComponent,
        ContactTypeDetailSearchDropDownComponent,
        ContactTypeSearchComponent,
        BusinessOriginLangSearchComponent,
        VisitTypeSearchComponent,
        TierSearchComponent,
        BCompanySearchComponent,
        LogoTypeSearchComponent
    ],
    imports: [
        SharedModule
    ],

    declarations: [
        NotificationGroupSearchComponent,
        LOSSearchComponent,
        RMMCategoryLanguageSearchComponent,
        TierSearchComponent,
        InvoiceCreditReasonLanguageSearchComponent,
        CustomerCategorySearchComponent,
        LostBusinessRequestOriginLanguageSearchComponent,
        ProductServiceGroupSearchComponent,
        ContractTypeLanguageSearchComponent,
        ServiceTypeSearchComponent,
        LostBusinessLanguageSearchComponent,
        LostBusinessDetailLanguageSearchComponent,
        BusinessSearchComponent,
        BCompanySearchComponent,
        ContactTypeDetailSearchDropDownComponent,
        BusinessOriginLangSearchComponent,
        VisitTypeSearchComponent,
        CountryCodeComponent,
        ContactTypeSearchComponent,
        APICodeSearchComponent,
        LogoTypeSearchComponent,
        ComponentTypeLanguageSearchComponent
    ]
})

export class InternalSearchModule {
    /*static forRoot(): ModuleWithProviders {
        return {
            ngModule: InternalSearchModule,
            providers: [
                RouteAwayGuardService, RouteAwayGlobals
            ]
        };
    }*/
}
