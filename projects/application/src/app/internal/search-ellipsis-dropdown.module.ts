import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModuleRoutes } from './../base/PageRoutes';

import { BranchSearchComponent } from './search/iCABSBBranchSearch';
import { PostCodeSearchComponent } from './search/iCABSBPostcodeSearch.component';
import { EmployeeSearchComponent } from './search/iCABSBEmployeeSearch';
import { ContractSearchComponent } from './search/iCABSAContractSearch';
import { PremiseSearchComponent } from './search/iCABSAPremiseSearch';
import { BranchServiceAreaSearchComponent } from './search/iCABSBBranchServiceAreaSearch';
import { EmployeeGridComponent } from './search/iCABSAEmployeeGrid.component';

@NgModule({
    exports: [
        BranchSearchComponent,
        PostCodeSearchComponent,
        EmployeeSearchComponent,
        ContractSearchComponent,
        PremiseSearchComponent,
        BranchServiceAreaSearchComponent,
        EmployeeGridComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'contractmanagement/account/employeeSearch', component: EmployeeSearchComponent },
            { path: 'application/contractsearch', component: ContractSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBRANCHSERVICEAREASEARCH, component: BranchServiceAreaSearchComponent }
        ])
    ],
    declarations: [
        BranchSearchComponent,
        PostCodeSearchComponent,
        EmployeeSearchComponent,
        ContractSearchComponent,
        PremiseSearchComponent,
        BranchServiceAreaSearchComponent,
        EmployeeGridComponent
    ],
    entryComponents: [
        PostCodeSearchComponent,
        EmployeeSearchComponent,
        ContractSearchComponent,
        PremiseSearchComponent,
        BranchServiceAreaSearchComponent,
        EmployeeGridComponent
    ]
})
export class SearchEllipsisDropdownModule { }
