import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchSummaryPlanReserveStockComponent } from './iCABSABranchSummaryPlanReserveStock.component';

describe('BranchSummaryPlanReserveStockComponent', () => {
  let component: BranchSummaryPlanReserveStockComponent;
  let fixture: ComponentFixture<BranchSummaryPlanReserveStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BranchSummaryPlanReserveStockComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchSummaryPlanReserveStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
