import { Component, OnInit, OnDestroy, Injector, ViewChild, HostListener } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ServicePlanningModuleRoutes, AppModuleRoutes, InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSSeServicePlanningDetailGridHg.html'
})

export class ServicePlanningDetailGridHgComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riPagination') riPagination: PaginationComponent;

    private muleConfig: any = {
        method: 'service-planning/maintenance',
        module: 'planning',
        operation: 'Service/iCABSSeServicePlanningDetailGridHg',
        contentType: 'application/x-www-form-urlencoded'
    };
    private planVisitRowID: string = '';
    private subscriptionManager: Subscription;
    public pageId: string = '';
    public gridConfig: any = {
        totalRecords: 1,
        pageSize: 10,
        actualPageSize: 11,
        gridCurPage: 1
    };
    public fieldVisibility: any = {
        tdUnPlanAll: true,
        tdSplitService: true
    };
    public legend: Array<any> = [];
    public isPaginationEnabled: boolean = true;
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'StartDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'EndDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'WeekNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PlanVisitNumber' }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGDETAILGRIDHG;
        this.browserTitle = this.pageTitle = 'Service Planning Detail';
        this.subscriptionManager = new Subscription();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getSysCharValue();
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }

    /**
     * This method is being used to control +/- keypress functionality
     * @param void
     * @return void
     */
    @HostListener('document:keypress', ['$event']) keyboardInput(event: KeyboardEvent): void {
        let vbNewDate: any;
        if (this.parentMode !== 'ServicePlan') {
            let sdate: Date = new Date(this.getControlValue('StartDate')), edate: Date = new Date(this.getControlValue('EndDate'));
            switch (event.keyCode) {
                case 43: //' + 7 Days
                    this.setControlValue('StartDate', new Date(sdate.getFullYear(), sdate.getMonth(), sdate.getDate() + 7));
                    this.setControlValue('EndDate', new Date(edate.getFullYear(), edate.getMonth(), edate.getDate() + 7));
                    this.getLatestWeekNumber();
                    this.refresh();
                    break;
                case 45: //' - 7 Days
                    vbNewDate = new Date(sdate.getFullYear(), sdate.getMonth(), sdate.getDate() - 7);
                    if (vbNewDate >= this.pageParams.vbStartDate) {
                        this.setControlValue('StartDate', new Date(sdate.getFullYear(), sdate.getMonth(), sdate.getDate() - 7));
                        this.setControlValue('EndDate', new Date(edate.getFullYear(), edate.getMonth(), edate.getDate() - 7));
                        this.getLatestWeekNumber();
                        this.refresh();
                    }
            }
        }
    }

    private windowOnLoad(): void {
        this.planVisitRowID = this.riExchange.getParentAttributeValue('PlanVisitRowID');
        this.setControlValue('PlanVisitNumber', this.riExchange.getParentHTMLValue('PlanVisitNumber'));
        this.riMaintenanceFetchRecord(this, (d) => {
            let servicePlanNumber: number = parseInt(this.riExchange.getParentAttributeValue('BranchServiceAreaCodeServicePlanNumber'), 0);
            if (this.parentMode === 'ServicePlan' || servicePlanNumber) {
                let postData: Object = {};
                let query: QueryParams = this.getURLSearchParamObject();
                query.set(this.serviceConstants.Action, '0');
                postData['Function'] = 'GetServicePlanDates';
                postData['BusinessCode'] = this.utils.getBusinessCode();
                postData['PlanVisitRowID'] = this.planVisitRowID;

                this.ajaxSource.next(this.ajaxconstant.START);
                let httService: Subscription = this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query, postData)
                    .subscribe((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        }
                        else {
                            this.setControlValue('StartDate', data['ServicePlanStartDate']);
                            this.setControlValue('EndDate', data['ServicePlanEndDate']);
                            this.getLatestWeekNumber();
                        }
                    },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        });
                this.subscriptionManager.add(httService);

            } else {
                this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('StartDate'));
                this.setControlValue('EndDate', this.riExchange.getParentHTMLValue('EndDate'));
                this.setControlValue('WeekNumber', this.riExchange.getParentHTMLValue('WeekNumber'));
            }

            this.disableControl('StartDate', true);
            this.disableControl('EndDate', true);
            this.disableControl('WeekNumber', true);
            this.pageParams.vSCEnableSinglePlanPerBranch = false;
            this.pageParams.vbStartDate = this.utils.convertDate(this.getControlValue('StartDate'));
            this.pageParams.vbEndDate = this.utils.convertDate(this.getControlValue('EndDate'));
            this.pageParams.vbNoOfDays = this.utils.dateDiffInDays(this.pageParams.vbStartDate, this.pageParams.vbEndDate) + 1;

            if (this.parentMode === 'ServicePlan') {
                this.fieldVisibility.tdUnPlanAll = false;
                this.fieldVisibility.tdSplitService = false;
            }
            else {
                if (this.getControlValue('ServicePlanNumber') !== '' && this.getControlValue('ServicePlanNumber') !== '0') {
                    this.fieldVisibility.tdSplitService = false;
                }
            }

            this.buildGrid();
            this.processLookUpData();
        });

        this.legend = [{ label: 'Contains Times For Selected Visit', color: 'rgb(255, 255, 204)' }];
    }

    /**
     * Get syschar value from service and assign it into page params
     */
    private getSysCharValue(): void {
        let syscharService: Subscription = this.fetchSysChar().subscribe(
            (e) => {
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    return false;
                }
                if (e.records && e.records.length > 0) {
                    this.pageParams.vSCEnableSinglePlanPerBranch = e.records[0].Required ? e.records[0].Required : false;
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(syscharService);
    }

    private fetchSysChar(): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        let sysCharList: Array<any> = [
            this.sysCharConstants.SystemCharSingleServicePlanPerBranch
        ];
        let sysCharNumbers: string = sysCharList.join(',');
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(querySysChar);
    }

    private getLatestWeekNumber(): void {
        let postData: Object = {};
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '6');
        postData['Function'] = 'GetLatestWeekNumber';
        postData['BusinessCode'] = this.utils.getBusinessCode();
        postData['PlanVisitRowID'] = this.planVisitRowID;
        postData['StartDate'] = this.getControlValue('StartDate');
        postData['EndDate'] = this.getControlValue('EndDate');

        this.ajaxSource.next(this.ajaxconstant.START);
        let httService: Subscription = this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query, postData)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.setControlValue('WeekNumber', data['WeekNumber']);
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        this.subscriptionManager.add(httService);
    }

    // Build initial structure of the Grid
    private buildGrid(): any {
        let vbCol: number;
        let vbDayTime: string;
        let vbDayConVal: string;
        let vbDayJobVal: string;

        let vbDateStart;
        let vbDateEnd;

        this.riGrid.Clear();
        this.riGrid.HighlightBar = true;
        this.riGrid.AddColumn('EmployeeCode', 'ServiceCoverPlanningDet', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('EmployeeSurname', 'ServiceCoverPlanningDet', 'EmployeeSurname', MntConst.eTypeText, 6);

        this.riGrid.AddColumn('WeekTimeTotal', 'ServiceCoverPlanningDet', 'WeekTimeTotal', MntConst.eTypeText, 5);
        this.riGrid.AddColumn('WeekContractTotal', 'ServiceCoverPlanningDet', 'WeekContractTotal', MntConst.eTypeCurrency, 5);
        this.riGrid.AddColumn('WeekJobTotal', 'ServiceCoverPlanningDet', 'WeekJobTotal', MntConst.eTypeCurrency, 5);

        for (vbCol = 1; vbCol <= this.pageParams.vbNoOfDays; vbCol++) {
            vbDayTime = 'Day' + vbCol + 'Time';
            vbDayConVal = 'Day' + vbCol + 'ConVal';
            vbDayJobVal = 'Day' + vbCol + 'JobVal';

            this.riGrid.AddColumn(vbDayTime, 'ServiceCoverPlanningDet', vbDayTime, MntConst.eTypeText, 5);
            this.riGrid.AddColumn(vbDayConVal, 'ServiceCoverPlanningDet', vbDayConVal, MntConst.eTypeCurrency, 5);
            this.riGrid.AddColumn(vbDayJobVal, 'ServiceCoverPlanningDet', vbDayJobVal, MntConst.eTypeCurrency, 5);
        }

        this.riGrid.AddColumn('EmployeeCodeRC', 'ServiceCoverPlanningDet', 'EmployeeCodeRC', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('EmployeeSurnameRC', 'ServiceCoverPlanningDet', 'EmployeeSurnameRC', MntConst.eTypeText, 6);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    //populate data into the grid and update the pagination control accordingly
    private riGridBeforeExecute(): any {
        let gridQueryParams: QueryParams = this.getURLSearchParamObject();
        let sortOrder: any = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }

        gridQueryParams.set(this.serviceConstants.GridPageSize, this.gridConfig.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.Action, '2');
        gridQueryParams.set('riGridMode', '0');
        gridQueryParams.set('riCacheRefresh', 'True');
        gridQueryParams.set('riGridHandle', this.utils.gridHandle);
        gridQueryParams.set('BranchNumber', this.utils.getBranchCode());
        gridQueryParams.set('riSortOrder', sortOrder);
        gridQueryParams.set('PlanVisitRowid', this.planVisitRowID);
        gridQueryParams.set('StartDate', this.getControlValue('StartDate') ? this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate')).toString() : '');
        gridQueryParams.set('EndDate', this.getControlValue('EndDate') ? this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate')).toString() : '');
        gridQueryParams.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        gridQueryParams.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.gridConfig.gridCurPage.toString());

        this.ajaxSource.next(this.ajaxconstant.START);
        let httService: Subscription = this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    if (data.hasError || data['error_description']) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.gridConfig.totalRecords = 0;
                        this.isPaginationEnabled = false;
                        this.riGrid.ResetGrid();
                    } else {
                        this.gridConfig.gridCurPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalRecords = data.pageData ? (data.pageData.lastPageNumber * this.gridConfig.pageSize) : 1;
                        this.isPaginationEnabled = (this.gridConfig.totalRecords > 0) ? true : false;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.Execute(data);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(httService);
    }

    public getGridInfo(info: any): void {
        this.gridConfig.totalRecords = info.totalRows;
    }

    // Get page specific grid data from service end
    public getCurrentPage(data: any): void {
        this.gridConfig.gridCurPage = data.value;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    // refresh the grid content
    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    /**
     * Grid body on Double click event
     */
    public riGridBodyOnDblClick(event: any): void {
        let objSrcName: any;
        let objSrc: any;
        let mode: string;

        objSrc = event.srcElement;
        objSrcName = this.utils.Left(this.riGrid.CurrentColumnName, 3) + this.utils.Right(this.riGrid.CurrentColumnName, 4);
        switch (objSrcName) {
            case 'DayTime':
                if (this.riGrid.Details.GetAttribute('EmployeeCode', 'RowID') !== 'Totals') {
                    this.setAttributes(objSrc);
                    if (this.getControlValue('ServicePlanNumber') && (this.parentMode === 'ServicePlanning')) {
                        mode = 'ServicePlan';
                    }
                    else {
                        mode = this.parentMode;
                    }
                    this.navigate(mode, InternalGridSearchSalesModuleRoutes.ICABSSESERVICEPLANNINGEMPLOYEETIMEGRIDHG, {
                        PlanVisitRowID: this.planVisitRowID,
                        EmployeeRowID: this.riGrid.Details.GetAttribute('EmployeeCode', 'RowID'),
                        PlannedVisitDate: this.getAttribute('ContractNumberVisitDate')
                    });
                }
        }
    }

    // handles grid sort functionality
    public gridSort(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public setAttributes(objSrc: any): void {
        this.setAttribute('ContractNumberEmployeeRowID', this.riGrid.Details.GetAttribute('EmployeeCode', 'RowID'));
        this.setAttribute('ContractNumberVisitDate', objSrc.getAttribute('AdditionalProperty'));
        this.setAttribute('ContractNumberPlanVisitRowID', this.planVisitRowID);
        this.setAttribute('EmployeeRowID', this.riGrid.Details.GetAttribute('EmployeeCode', 'RowID'));
        this.setAttribute('Row', objSrc.parentElement.sectionRowIndex);
    }

    public splitServiceOnClick(): void {
        this.setAttribute('ContractNumberPlanVisitRowID', this.planVisitRowID);
        //Navigate to screen iCABSSeServicePlanningSplitServiceMaintenanceHg.htm
        this.navigate('ServicePlanning', AppModuleRoutes.SERVICEPLANNING + ServicePlanningModuleRoutes.ICABSSESERVICEPLANNINGSPLITSERVICEMAINTENANCEHG,
            {
                PlanVisitRowID: this.planVisitRowID,
                PlanVisitNumber: this.getControlValue('PlanVisitNumber'),
                ServiceCoverNumber: this.getControlValue('ServiceCoverNumber')
            });
    }

    public unPlanAllOnClick(): void {
        let vbMsgResult: string = '';
        if (this.utils.customTruthyCheck(this.getControlValue('ServicePlanNumber')) && this.getControlValue('ServicePlanNumber') !== '0') {
            vbMsgResult = MessageConstant.PageSpecificMessage.servicePlanningDetailGridHg.This_Visit_Is_Assigned_To_A_Plan;
        } else {
            vbMsgResult = MessageConstant.PageSpecificMessage.servicePlanningDetailGridHg.Are_You_Sure_You_Wish_To_Continue;
        }

        let yesSuspension: any = function yesSuspension(): any {
            let postData: Object = {};
            let query: QueryParams = this.getURLSearchParamObject();
            query.set(this.serviceConstants.Action, '6');
            postData['Function'] = 'UnPlanAll';
            //postData['BusinessCode'] = this.utils.getBusinessCode();
            postData['PlanVisitRowID'] = this.planVisitRowID;

            this.ajaxSource.next(this.ajaxconstant.START);
            let httService: Subscription = this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query, postData)
                .subscribe((data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        if (this.getControlValue('ServicePlanNumber') !== '' && this.getControlValue('ServicePlanNumber') !== '0') {
                            this.fieldVisibility.tdSplitService = true;
                            this.setControlValue('ServicePlanNumber', '0');
                        }
                        this.refresh();
                    }
                },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
            this.subscriptionManager.add(httService);
        }.bind(this);

        let promptVO: ICabsModalVO = new ICabsModalVO(vbMsgResult, null, yesSuspension, null);
        promptVO.title = MessageConstant.PageSpecificMessage.servicePlanningDetailGridHg.Unplan_All;
        promptVO.confirmLabel = MessageConstant.Message.ConfirmTitle;
        promptVO.cancelLabel = MessageConstant.Message.Cancel;
        this.modalAdvService.emitPrompt(promptVO);
    }

    public riMaintenanceFetchRecord(obj: any, callbackResponse: any): any {
        let postData: Object = {};
        let query: QueryParams = this.getURLSearchParamObject();
        query.set(this.serviceConstants.Action, '0');
        query.set('ROWID', this.planVisitRowID);
        this.ajaxSource.next(this.ajaxconstant.START);
        let httService: Subscription = this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, query)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.setControlValue('ContractNumber', data['ContractNumber']);
                    this.setControlValue('PremiseNumber', data['PremiseNumber']);
                    this.setControlValue('ProductCode', data['ProductCode']);
                    this.setControlValue('ServiceCoverNumber', data['ServiceCoverNumber']);
                    this.setControlValue('ServicePlanNumber', data['ServicePlanNumber']);
                    this.setControlValue('BranchNumber', data['BranchNumber']);
                    this.setControlValue('BranchServiceAreaCode', data['BranchServiceAreaCode']);
                }
                return callbackResponse(data);
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        this.subscriptionManager.add(httService);
    }

    //This method is used to process the look up response
    public processLookUpData(): void {
        let gLanguageCode: string = this.riExchange.LanguageCode();
        let data: Array<any> = [
            {
                'table': 'Contract',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'ContractNumber': this.getControlValue('ContractNumber') },
                'fields': ['ContractName']
            },
            {
                'table': 'Premise',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'ContractNumber': this.getControlValue('ContractNumber'), 'PremiseNumber': this.getControlValue('PremiseNumber') },
                'fields': ['PremiseName']
            },
            {
                'table': 'Product',
                'query': { 'BusinessCode': this.utils.getBusinessCode(), 'ProductCode': this.getControlValue('ProductCode') },
                'fields': ['ProductDesc']
            }
        ];

        this.ajaxSource.next(this.ajaxconstant.START);
        let lookupScp: Subscription = this.LookUp.lookUpRecord(data, 500).subscribe((e) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (e) {
                this.setControlValue('ContractName', (e.length >= 1 && e[0]) ? e[0][0]['ContractName'] : '');
                this.setControlValue('PremiseName', (e.length >= 2 && e[1]) ? e[1][0]['PremiseName'] : '');
                this.setControlValue('ProductDesc', (e.length >= 3 && e[2]) ? e[2][0]['ProductDesc'] : '');
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(lookupScp);
    }

}
