import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';

@Component({
    templateUrl: 'iCABSSSalesStatisticsServiceValueDetailAdjustGrid.html'
})

export class SalesStatisticsSericeValueDetailAdjustGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('TransferToEmployeeCode1') public transferToEmployeeCode1;

    private search: QueryParams = new QueryParams();
    private xhrParams: any = {
        operation: 'Sales/iCABSSSalesStatisticsServiceValueDetailAdjustGrid',
        module: 'salesstats',
        method: 'prospect-to-contract/maintenance'
    };
    private lHideGrid: boolean = true;
    private lCalcLeftToAllocate: boolean = true;

    public employeeSearchComponent = EmployeeSearchComponent;
    public employeeSearchParams: any = {
        parentMode: 'TransferTo'
    };
    public pageId: string = '';
    public totalRecords: number = 1;
    public pageCurrent: number = 1;
    public controls: Array<any> = [
        { name: 'ContractNumber', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'Reason', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'Employee', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'Processed', disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'AnnualValueChange', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'Effective', disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'SalesStatsValue', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'btnAdjustment', disabled: false, required: false },
        { name: 'TransferToEmployeeCodeMain', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeNameMain', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValueMain', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValueMain', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode1', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName1', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue1', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue1', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode2', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName2', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue2', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue2', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode3', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName3', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue3', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue3', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode4', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName4', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue4', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue4', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode5', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName5', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue5', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue5', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode6', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName6', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue6', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue6', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode7', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName7', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue7', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue7', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode8', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName8', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue8', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue8', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode9', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName9', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue9', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue9', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'TransferToEmployeeCode10', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TransferToEmployeeName10', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AdjustValue10', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'PercentValue10', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'LeftToAllocate', disabled: true, required: false, type: MntConst.eTypeDecimal2 },
        { name: 'menu' },
        { name: 'ServiceCoverNumber' },
        { name: 'ServiceValueNumber' }
    ];

    public mandatory: any = {
        TransferToEmployeeCode1: false,
        TransferToEmployeeCode2: false,
        TransferToEmployeeCode3: false,
        TransferToEmployeeCode4: false,
        TransferToEmployeeCode5: false,
        TransferToEmployeeCode6: false,
        TransferToEmployeeCode7: false,
        TransferToEmployeeCode8: false,
        TransferToEmployeeCode9: false,
        TransferToEmployeeCode10: false
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSSALESSTATISTICSSERVICEVALUEDETAILADJUSTGRID;
        this.pageTitle = this.browserTitle = 'Sales Statistics Service Value';
    }

    /**
     * No child pages, hence state retention not required
     */
    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.riGrid = true;
        this.pageParams.btnSaveAdjustment = false;
        this.pageParams.tableGrid = true;
        this.pageParams.tableEmployees = false;
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;

        this.riExchange.getParentHTMLValue('ServiceCoverNumber');
        this.setControlValue('ServiceValueNumber', this.riExchange.getParentAttributeValue('ServiceValueNumber'));

        this.riExchange.getParentHTMLValue('ContractNumber');
        this.riExchange.getParentHTMLValue('PremiseNumber');
        this.riExchange.getParentHTMLValue('ProductCode');

        this.riExchange.getParentHTMLValue('ContractName');
        this.riExchange.getParentHTMLValue('PremiseName');
        this.riExchange.getParentHTMLValue('ProductDesc');
        this.setControlValue('Processed', this.riExchange.getParentAttributeValue('Processed'));
        this.setControlValue('Effective', this.riExchange.getParentAttributeValue('Effective'));
        this.setControlValue('Reason', this.riExchange.getParentAttributeValue('Reason'));
        this.setControlValue('Employee', this.riExchange.getParentAttributeValue('Employee'));
        this.setControlValue('AnnualValueChange', this.riExchange.getParentAttributeValue('AnnualValueChange'));
        this.setControlValue('SalesStatsValue', this.riExchange.getParentAttributeValue('SalesStatsValue'));
        this.setControlValue('menu', 'Option');

        let cEmployeeCode = this.getControlValue('Employee').split(' ');
        this.setControlValue('TransferToEmployeeCodeMain', (cEmployeeCode[0]).trim());
        this.setControlValue('TransferToEmployeeNameMain', (cEmployeeCode[1]).trim());
        this.buildGrid();
        this.riGridBeforeExecute();
        this.menuOnchange(true);
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Adjustment', 'Grid', 'Adjustment', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('Adjustment', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('OriginalValue', 'Grid', 'OriginalValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('OriginalValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('TransferToEmployeeCode', 'Grid', 'TransferToEmployeeCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('TransferValue', 'Grid', 'TransferValue', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('TransferValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumn('TransferDate', 'Grid', 'TransferDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('TransferTime', 'Grid', 'TransferTime', MntConst.eTypeTime, 10);
        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): void {
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set('level', 'ServiceValueAdjust');
        this.search.set('RowID', this.riExchange.getParentAttributeValue('RowID'));
        this.search.set('LanguageCode', this.riExchange.LanguageCode());
        this.search.set('RequestMode', 'Grid');
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
        this.search.set(this.serviceConstants.GridCacheRefresh, 'True');
        this.search.set(this.serviceConstants.PageSize, '10');
        this.search.set(this.serviceConstants.GridPageCurrent, this.pageCurrent.toString());
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, this.search)
            .subscribe(
            (data) => {
                if (data) {
                    this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        this.riGrid.Execute(data);
                    }
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            error => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.totalRecords = 1;
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private resetMenu(): void {
        this.disableControl('menu', false);
        this.setControlValue('menu', 'Option');
    }


    private disableOption(): void {
        for (let j = 1; j <= 10; j++) {
            this.disableControl('AdjustValue' + j, true);
            this.disableControl('PercentValue' + j, true);
            this.disableControl('TransferToEmployeeCode' + j, true);
            this.disableControl('TransferToEmployeeName' + j, true);
        }
    }

    private disablePercent(): void {
        for (let j = 1; j <= 10; j++) {
            this.disableControl('AdjustValue' + j, true);
            this.disableControl('PercentValue' + j, false);
            this.disableControl('TransferToEmployeeCode' + j, false);
            this.disableControl('TransferToEmployeeName' + j, false);
        }
    }

    private disableValue(): void {
        for (let j = 1; j <= 10; j++) {
            this.disableControl('PercentValue' + j, true);
            this.disableControl('AdjustValue' + j, false);
            this.disableControl('TransferToEmployeeCode' + j, false);
            this.disableControl('TransferToEmployeeName' + j, false);
        }
    }

    private clearEmployeeData(): void {
        for (let iCount = 1; iCount <= 10; iCount++) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TransferToEmployeeCode' + iCount, false);
            this.mandatory['TransferToEmployeeCode' + iCount] = false;
            this.setControlValue('TransferToEmployeeCode' + iCount, '');
            this.setControlValue('TransferToEmployeeName' + iCount, '');
            this.setControlValue('AdjustValue' + iCount, 0);
            this.setControlValue('PercentValue' + iCount, 0);
        }
        this.calcAdjustValueMain();
    }

    private selectedRowFocus(rsrcElement: any): void {
        rsrcElement.select();
        this.setAttribute('Row', rsrcElement.parentElement.parentElement.parentElement.sectionRowIndex);
        this.setAttribute('Cell', rsrcElement.parentElement.parentElement.cellIndex);
        this.setAttribute('RowID', rsrcElement.getAttribute('RowID'));
        rsrcElement.focus();
    }

    public btnNewAdjustmentOnclick(): void {
        if (this.lCalcLeftToAllocate) {
            this.lCalcLeftToAllocate = false;
            this.setControlValue('LeftToAllocate', this.getControlValue('SalesStatsValue'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.search = this.getURLSearchParamObject();
            this.search.set(this.serviceConstants.Action, '6');
            let formData = {
                RequestFunction: 'GetValueLeftToAllocate',
                ContractNumber: this.getControlValue('ContractNumber'),
                PremiseNumber: this.getControlValue('PremiseNumber'),
                ProductCode: this.getControlValue('ProductCode'),
                ServiceCoverNumber: this.getControlValue('ServiceCoverNumber'),
                ServiceValueNumber: this.getControlValue('ServiceValueNumber')
            };
            this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
                this.xhrParams.operation, this.search, formData)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('LeftToAllocate', data['LeftToAllocate']);
                        this.hideGrid();
                        this.resetMenu();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
        } else {
            this.hideGrid();
            this.resetMenu();
        }
    }

    public hideGrid(): void {
        // Hides the grid && shows the 10 employeecode fields;
        if (this.lHideGrid) {
            this.lHideGrid = false;
            this.pageParams.riGrid = false;
            this.pageParams.btnSaveAdjustment = true;
            this.clearEmployeeData();
            this.pageParams.tableGrid = false;
            this.pageParams.tableEmployees = true;
            this.transferToEmployeeCode1.nativeElement.focus();
        } else {
            this.lHideGrid = true;
            this.pageParams.riGrid = true;
            this.pageParams.btnSaveAdjustment = false;
            this.pageParams.tableGrid = true;
            this.pageParams.tableEmployees = false;
            this.uiForm.markAsPristine();
        }
    }

    public riGridAfterExecute(event: any): void {
        if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children[0]) {
            if (this.riGrid.HTMLGridBody.children[0].children[0]) {
                if (this.riGrid.HTMLGridBody.children[0].children[0].children[0]) {
                    if (this.riGrid.HTMLGridBody.children[0].children[0].children[0].children[0]) {
                        this.selectedRowFocus(this.riGrid.HTMLGridBody.children[0].children[0].children[0].children[0]);
                    }
                }
            }
        }

    }

    public riGridBodyOnClick(event: any): void {
        this.selectedRowFocus(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
    }

    public menuOnchange(isInit?: boolean): void {
        switch (this.getControlValue('menu')) {
            case 'Option':
                this.disableOption();
                break;
            case 'Percent':
                this.disablePercent();
                this.disableControl('menu', true);
                break;
            case 'Value':
                this.disableValue();
                this.disableControl('menu', true);
                break;
        }
        if (!isInit) {
            this.uiForm.markAsDirty();
        }
    }

    public refresh(): void {
        this.riGridBeforeExecute();
    }

    public riGridSort(event: any): void {
        this.riGridBeforeExecute();
    }

    public getCurrentPage(event: any): void {
        if (this.pageCurrent !== event.value) {
            this.pageCurrent = event.value;
            this.riGridBeforeExecute();
        }
    }

    public onDataReceived(event: any, id: string): void {
        this.setControlValue('TransferToEmployeeCode' + id, event.TransferToEmployeeCode);
        this.setControlValue('TransferToEmployeeName' + id, event.TransferToEmployeeName);
        this.uiForm.markAsDirty();
    }

    public transferToEmployeeCodeOnChange(iCount: any): void {
        if (!this.fieldHasValue('TransferToEmployeeCode' + iCount)) {
            this.setControlValue('TransferToEmployeeName' + iCount, '');
        } else {
            this.getTransferToEmployeeName(iCount);
        }
    }

    public percentValueOnChange(iCount: any): void {
        if (!this.fieldHasValue('PercentValue' + iCount)) {
            this.setControlValue('PercentValue' + iCount, this.globalize.formatDecimalToLocaleFormat(0, 2));
            this.setControlValue('AdjustValue' + iCount, this.globalize.formatDecimalToLocaleFormat(0, 2));
        } else {
            let percentage = this.getControlValue('PercentValue' + iCount);
            let leftAllocate = this.CDbl(this.getControlValue('LeftToAllocate'));
            this.setControlValue('AdjustValue' + iCount, (leftAllocate * percentage / 100));
        }
        this.adjustValueOnChange(iCount);
    }

    public adjustValueOnChange(iCount: any): void {
        if (!this.fieldHasValue('AdjustValue' + iCount)) {
            this.setControlValue('AdjustValue' + iCount, this.globalize.formatDecimalToLocaleFormat(0, 2));
        }
        if (this.getControlValue('AdjustValue' + iCount).toString() === '0') {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TransferToEmployeeCode' + iCount, false);
            this.mandatory['TransferToEmployeeCode' + iCount] = false;
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'TransferToEmployeeCode' + iCount, true);
            this.mandatory['TransferToEmployeeCode' + iCount] = true;
        }
        this.calcAdjustValueMain();
    }

    public calcAdjustValueMain(): void {
        this.setControlValue('AdjustValueMain', (this.CDbl(this.getControlValue('LeftToAllocate')) -
            this.CDbl(this.getControlValue('AdjustValue1')) - this.CDbl(this.getControlValue('AdjustValue2')) - this.CDbl(this.getControlValue('AdjustValue3')) - this.CDbl(this.getControlValue('AdjustValue4')) - this.CDbl(this.getControlValue('AdjustValue5')) -
            this.CDbl(this.getControlValue('AdjustValue6')) - this.CDbl(this.getControlValue('AdjustValue7')) - this.CDbl(this.getControlValue('AdjustValue8')) - this.CDbl(this.getControlValue('AdjustValue9')) - this.CDbl(this.getControlValue('AdjustValue10'))));
    }

    public getTransferToEmployeeName(iEmployee: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.search = this.getURLSearchParamObject();
        this.search.set(this.serviceConstants.Action, '6');
        let formData = {
            RequestFunction: 'GetTransferToEmployeeName',
            EmployeeCode: this.getControlValue('TransferToEmployeeCode' + iEmployee)
        };
        this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
            this.xhrParams.operation, this.search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('TransferToEmployeeName' + iEmployee, data['EmployeeName']);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            });
    }

    public handleCancelClick(): void {
        this.hideGrid();
        this.uiForm.markAsPristine();
    }

    public btnSaveAdjustmentOnclick(): void {
        let lCanProcess: boolean = true;
        if (this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode1') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode2') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode3') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode4') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode5') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode6') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode7') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode8') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode9') ||
            this.riExchange.riInputElement.isError(this.uiForm, 'TransferToEmployeeCode10')) {
            lCanProcess = false;
        }
        if (lCanProcess) {
            /** the following variable so the amount left to allocate is correctly calculated
             *  if the user decides to do more adjustments;
             */
            //this.lCalcLeftToAllocate = true;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.search = this.getURLSearchParamObject();
            this.search.set(this.serviceConstants.Action, '6');
            let formData = {
                RequestFunction: 'UpdateSalesStatistics',
                EmployeeCode: this.getControlValue('TransferToEmployeeCodeMain'),
                EmployeeValue: this.getControlValue('LeftToAllocate'),
                ContractNumber: this.getControlValue('ContractNumber'),
                PremiseNumber: this.getControlValue('PremiseNumber'),
                ProductCode: this.getControlValue('ProductCode'),
                ServiceCoverNumber: this.getControlValue('ServiceCoverNumber'),
                ServiceValueNumber: this.getControlValue('ServiceValueNumber'),
                TransferToEmployeeCode1: this.getControlValue('TransferToEmployeeCode1'),
                AdjustValue1: this.getControlValue('AdjustValue1'),
                TransferToEmployeeCode2: this.getControlValue('TransferToEmployeeCode2'),
                AdjustValue2: this.getControlValue('AdjustValue2'),
                TransferToEmployeeCode3: this.getControlValue('TransferToEmployeeCode3'),
                AdjustValue3: this.getControlValue('AdjustValue3'),
                TransferToEmployeeCode4: this.getControlValue('TransferToEmployeeCode4'),
                AdjustValue4: this.getControlValue('AdjustValue4'),
                TransferToEmployeeCode5: this.getControlValue('TransferToEmployeeCode5'),
                AdjustValue5: this.getControlValue('AdjustValue5'),
                TransferToEmployeeCode6: this.getControlValue('TransferToEmployeeCode6'),
                AdjustValue6: this.getControlValue('AdjustValue6'),
                TransferToEmployeeCode7: this.getControlValue('TransferToEmployeeCode7'),
                AdjustValue7: this.getControlValue('AdjustValue7'),
                TransferToEmployeeCode8: this.getControlValue('TransferToEmployeeCode8'),
                AdjustValue8: this.getControlValue('AdjustValue8'),
                TransferToEmployeeCode9: this.getControlValue('TransferToEmployeeCode9'),
                AdjustValue9: this.getControlValue('AdjustValue9'),
                TransferToEmployeeCode10: this.getControlValue('TransferToEmployeeCode10'),
                AdjustValue10: this.getControlValue('AdjustValue10')
            };

            this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module,
                this.xhrParams.operation, this.search, formData)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else if (data && data['ErrorMessage']) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['ErrorMessage']));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this.hideGrid();
                        this.riGridBeforeExecute();
                    }
                    this.resetMenu();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
                });
        }
    }
}
