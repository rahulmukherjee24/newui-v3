import { Component, Injector, OnInit , AfterContentInit, ViewChild } from '@angular/core';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { BranchServiceAreaSearchComponent } from '../search/iCABSBBranchServiceAreaSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { MessageConstant } from '@shared/constants/message.constant';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { PaginationComponent } from 'ngx-bootstrap';
import { ContractManagementModuleRoutes, InternalGridSearchServiceModuleRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSeVisitDateDiscrepancyGrid.html',
    providers: [CommonLookUpUtilsService]
})

export class VisitDateDiscrepancyGridComponent extends LightBaseComponent implements OnInit , AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    public commonGridFunction: CommonGridFunction;
    public pageId: string;
    public hasGridData: boolean = false;
    public currentContractTypeURLParameter: string = '';

    protected controls: any = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', readonly: true, disabled: true, type: MntConst.eTypeText },
        { name: 'DiscrepType', required: true, value: 'AnnivDateDiscrep' }
    ];

    public discrepTypeValues: Array<any> = [
        { value: 'AnnivDateDiscrep', text: 'Visit Anniversary Date Discrepancy' },
        { value: 'PlanVisitDiscrep', text: 'Plan Visit Date Discrepancy' }
    ];

    public ellipsConf: any = {
        serviceArea: {
            childConfigParams: {
                'parentMode': 'LookUpEmp'
            },
            contentComponent: BranchServiceAreaSearchComponent
        }
    };

    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };

    constructor(injector: Injector,private commonUtil: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }

    ngOnInit(): void {
        this.pageId = PageIdentifier.ICABSSEVISITDATEDISCREPANCYGRID;
        this.pageTitle = this.browserTitle = 'Visit Mismatch';
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.windowOnload();
    }

    private windowOnload(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
            this.commonGridFunction.onRiGridRefresh();
        }
        else {
            this.riExchange.setCurrentContractType();
            this.buildGrid();
            this.pageParams.gridConfig = {
                totalItem: 1
            };
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.valueColumnsCount = [];
            this.pageParams.viewModeValues = [];
        }
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ContractPremise','ServiceCover','ContractPremise',MntConst.eTypeText,16);
        this.riGrid.AddColumnAlign('ContractPremise',MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName','ServiceCover','PremiseName',MntConst.eTypeText,14);
        this.riGrid.AddColumnAlign('PremiseName',MntConst.eAlignmentLeft);
        if (this.getControlValue('DiscrepType') === 'AnnivDateDiscrep') {
            this.riGrid.AddColumn('ProductCode1','ServiceCover','ProductCode1',MntConst.eTypeCode,10);
            this.riGrid.AddColumnAlign('ProductCode1',MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ServiceVisitFrequency1','ServiceCover','ServiceVisitFrequency1',MntConst.eTypeInteger,5);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency1',MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ServiceVisitAnnivDate1','ServiceCover','ServiceVisitAnnivDate1',MntConst.eTypeDate,10);
            this.riGrid.AddColumnAlign('ServiceVisitAnnivDate1',MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ProductCode2','ServiceCover','ProductCode2',MntConst.eTypeCode,10);
            this.riGrid.AddColumnAlign('ProductCode2',MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ServiceVisitFrequency2','ServiceCover','ServiceVisitFrequency2',MntConst.eTypeInteger,5);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency2',MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ServiceVisitAnnivDate2','ServiceCover','ServiceVisitAnnivDate2',MntConst.eTypeDate,10);
            this.riGrid.AddColumnAlign('ServiceVisitAnnivDate2',MntConst.eAlignmentCenter);
        }
        else {
            this.riGrid.AddColumn('ServiceVisitFrequency','ServiceCover','ServiceVisitFrequency',MntConst.eTypeInteger,5);
            this.riGrid.AddColumnAlign('ServiceVisitFrequency',MntConst.eAlignmentCenter);
            this.riGrid.AddColumn('ProductCode1','ServiceCover','ProductCode1',MntConst.eTypeCode,10);
            this.riGrid.AddColumnAlign('ProductCode1',MntConst.eAlignmentLeft);
            this.riGrid.AddColumn('ProductCode2','ServiceCover','ProductCode2',MntConst.eTypeCode,10);
            this.riGrid.AddColumnAlign('ProductCode2',MntConst.eAlignmentLeft);
        }
        this.riGrid.AddColumn('BranchServiceAreaCode','ServiceCover','BranchServiceAreaCode',MntConst.eTypeCode,4);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode',MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ContractPremise',true);
        this.riGrid.Complete();
    }

    public onChangeBranchServiceAreaCode(brServAreaEvent: any): void {
        this.setControlValue('BranchServiceAreaCode',brServAreaEvent['BranchServiceAreaCode'] ? brServAreaEvent['BranchServiceAreaCode'] : brServAreaEvent);
        this.setControlValue('EmployeeSurname', '');
        if (this.getControlValue('BranchServiceAreaCode') !== '') {
            this.commonUtil.getBranchServiceAreaDescWithEmp(this.getControlValue('BranchServiceAreaCode'), this.utils.getBranchCode())
            .then(data => {
                if (data && data[0] && data[0][0]) {
                    this.commonUtil.getEmployeeSurname(data[0][0].EmployeeCode).then(res => {
                        if (res && res[0] && res[0][0]) {
                            this.setControlValue('EmployeeSurname', res[0][0].EmployeeSurname);
                        } else {
                            this.setControlValue('BranchServiceAreaCode', '');
                            this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area Code');
                        }
                    }).catch(error => {
                        this.displayMessage(error);
                    });
                } else {
                    this.setControlValue('BranchServiceAreaCode', '');
                    this.displayMessage(MessageConstant.Message.RecordNotFound + ' - Service Area Code');
                }
            })
            .catch(error => {
                this.displayMessage(error);
            });
        }
        this.commonGridFunction.resetGrid();
    }

    public onRiGridRefresh(): void {
        this.buildGrid();
        this.commonGridFunction.onRefreshClick();
    }

    public populateGrid(): void {
        let formData = {
            'BusinessCode': this.businessCode(),
            'BranchNumber': this.utils.getBranchCode(),
            'Action': '2'
        };
        if (!this.riExchange.riInputElement.isError(this.uiForm,'BranchServiceAreaCode')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            search.set(this.serviceConstants.BusinessCode, this.businessCode());
            search.set(this.serviceConstants.CountryCode, this.countryCode());
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
            formData['DiscrepType'] = this.getControlValue('DiscrepType');
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.PageSize] = '10';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('bi/reports', 'reports', 'Service/iCABSSeVisitDateDiscrepancyGrid', search, formData).then(data => {
              this.ajaxSource.next(this.ajaxconstant.COMPLETE);
              this.commonGridFunction.setPageData(data);
            },
              (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.hasGridData = false;
                this.displayMessage(error);
              });
        }
    }

    public riGridBodyOnDblClick(event: any): void {
        let columnName = this.riGrid.CurrentColumnName;
        this.serviceCoverFocus(event.srcElement);
        switch (columnName) {
            case 'ContractPremise':
                this.navigate('VisitDateDiscrepancy',
                ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE ,
                {
                    CurrentContractTypeURLParameter: this.currentContractTypeURLParameter
                });
            break;
            case 'ProductCode1':
            case 'ProductCode2':
                    if (columnName === 'ProductCode1') {
                        this.setAttribute('ServiceCoverRowID', this.getAttribute('ServiceCover1RowID'));
                    }
                    else {
                        this.setAttribute('ServiceCoverRowID', this.getAttribute('ServiceCover2RowID'));
                    }
                this.navigate('VisitDateDiscrepancy',
                ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT ,
                {
                    CurrentContractTypeURLParameter: this.currentContractTypeURLParameter
                });
            break;
            case 'ServiceVisitAnnivDate1':
            case 'ServiceVisitAnnivDate2':
                    if (columnName === 'ServiceVisitAnnivDate1') {
                        this.setAttribute('ServiceCoverRowID', this.getAttribute('ServiceCover1RowID'));
                    }
                    else {
                        this.setAttribute('ServiceCoverRowID', this.getAttribute('ServiceCover2RowID'));
                    }
                this.navigate('VisitDateDiscrepancy',
                InternalGridSearchServiceModuleRoutes.ICABSSESERVICEVISITANNIVDATEMAINTENANCE);
            break;
        }

    }

    private serviceCoverFocus(srcElement: any): void {
        let oTR = this.riGrid.CurrentHTMLRow;
        srcElement.focus();
        this.setAttribute('Row', oTR.sectionRowIndex);
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('ContractPremise', 'RowID'));
        this.setAttribute('ServiceCover1RowID',this.riGrid.Details.GetAttribute('ProductCode1', 'RowID'));
        this.setAttribute('ServiceCover2RowID',this.riGrid.Details.GetAttribute('ProductCode2', 'RowID'));
        switch (this.riGrid.Details.GetAttribute('ContractPremise', 'AdditionalProperty')) {
            case 'C':
                this.currentContractTypeURLParameter = '';
                break;
            case 'J':
                this.currentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.currentContractTypeURLParameter = '<product>';
                break;
        }
    }
}
