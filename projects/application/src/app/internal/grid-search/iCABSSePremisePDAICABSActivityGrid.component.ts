import { Component, OnInit, OnDestroy, Injector, ViewChild } from '@angular/core';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from '../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { InternalGridSearchApplicationModuleRoutes, InternalGridSearchSalesModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalMaintenanceApplicationModuleRoutes } from '../../base/PageRoutes';
import { MessageConstant } from './../../../shared/constants/message.constant';


@Component({
    templateUrl: 'iCABSSePremisePDAICABSActivityGrid.html'
})

export class SePremisePDAICABSActivityGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('TabContent') public tabContent;
    @ViewChild('formActualVisitDate') public formActualVisitDate;
    private noBarCodeText: string;
    private noSignText: string;
    private noPremiseVisitText: string;
    private queryParams: any = {
        operation: 'Service/iCABSSePremisePDAICABSActivityGrid',
        module: 'pda',
        method: 'service-delivery/maintenance'
    };
    private search: any = this.getURLSearchParamObject();
    private pageCurrent: number = 1;
    private promptTitle: string = '';
    public pageId: string = '';
    public signatureSrc: string;
    public trSignature: boolean = false;
    public itemsPerPage: number = 10;
    public totalRecords: number = 1;
    public NSRStringArr: Array<any> = [];
    public NBCStringArr: Array<any> = [];
    public NPVStringArr: Array<any> = [];
    public isCancelDisplay: boolean = false;
    public isSaveDisplay: boolean = false;
    public isUpdateDisp: boolean = true;
    public isWasteTabVisible: boolean = false;
    public isMakemandatory: boolean = false;
    public isDisplayGridSection: boolean = true;
    public isDisplayRefPageSection: boolean = true;
    public isCancelDisabled: boolean = false;
    public isSaveDisabled: boolean = false;
    public isUpdateDisabled: boolean = false;
    public pageSize = 10;
    public controls = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, disabled: true, commonValidator: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'ActualStartTime', type: MntConst.eTypeTime, commonValidator: true },
        { name: 'ActualEndTime', type: MntConst.eTypeTime, commonValidator: true },
        { name: 'EmployeeCode', type: MntConst.eTypeCode, disabled: true, commonValidator: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'PlannedVisitDate', type: MntConst.eTypeDate, disabled: true, commonValidator: true },
        { name: 'NoBarcodeReasonCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ActivityStatusDesc', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true, commonValidator: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'NoSignatureReasonNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true, commonValidator: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'StatusDesc', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'NoPremiseVisitReasonCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProofSignatureRequiredInd', type: MntConst.eTypeCheckBox, disabled: true, commonValidator: true },
        { name: 'ProofScanRequiredInd', type: MntConst.eTypeCheckBox, disabled: true, commonValidator: true },
        { name: 'NoPremiseVisitReasonNote', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseAddressLine1', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseContactName', type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'PremiseAddressLine2', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseContactPosition', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseAddressLine3', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseContactTelephone', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseAddressLine4', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseContactFax', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseAddressLine5', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremiseContactEmail', type: MntConst.eTypeText, commonValidator: true },
        { name: 'PremisePostcode', type: MntConst.eTypeText, commonValidator: true },
        { name: 'GeneralNotes', type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'PremiseContactSignature', commonValidator: true },
        { name: 'PDAPremiseContactName', type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'WasteConsignmentNoteNumber', type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'HiddenNoServiceManualCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'HiddenManualVisitReasonCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'DebriefNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'ActualVisitDate', type: MntConst.eTypeDate, commonValidator: true },
        { name: 'ServicePlanNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PDAManualVisitReasonCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'NoPremiseVisitManualCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'NoBarcodeManualCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'NoSignatureManualNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, commonValidator: true },
        { name: 'ErrorMessageDesc', type: MntConst.eTypeText, commonValidator: true },
        { name: 'NoBarcodeReasonString', type: MntConst.eTypeText, commonValidator: true },
        { name: 'NoSignatureReasonString', type: MntConst.eTypeText, commonValidator: true },
        { name: 'NoPremiseVisitReasonString', type: MntConst.eTypeText, commonValidator: true },
        { name: 'AllowUpdate', type: MntConst.eTypeText, commonValidator: true },
        { name: 'GotSignature', type: MntConst.eTypeCheckBox, commonValidator: true },
        { name: 'PremiseContactSignatureURL', type: MntConst.eTypeTextFree, commonValidator: true },
        { name: 'NoBarcodeReasonSelect', disabled: true, commonValidator: true },
        { name: 'NoSignatureReasonSelect', disabled: true, commonValidator: true },
        { name: 'NoPremiseVisitReasonSelect', disabled: true, commonValidator: true },
        { name: 'NoBarcodeManualSelect', commonValidator: true },
        { name: 'NoSignatureManualSelect', commonValidator: true },
        { name: 'NoPremiseVisitManualSelect', commonValidator: true },
        { name: 'PDAManualVisitReasonDesc', type: MntConst.eTypeText, commonValidator: true },
        { name: 'NoBarcodeReasonCount', type: MntConst.eTypeText },
        { name: 'NoSignatureReasonCount', type: MntConst.eTypeText },
        { name: 'NoPremiseVisitReasonCount', type: MntConst.eTypeText }
    ];
    public uiDisplay: any = {
        tab: {
            tab1: { visible: true, active: true },
            tab2: { visible: true, active: false },
            tab3: { visible: true, active: false },
            tab4: { visible: this.isWasteTabVisible, active: false }
        }
    };
    public dropdown = {
        manualVisitReasonSearch: {
            isRequired: false,
            isTriggerValidate: false,
            isDisabled: true,
            params: {
                method: 'service-delivery/search',
                module: 'manual-service',
                operation: 'Business/iCABSBManualVisitReasonSearch'
            },
            active: {
                id: '',
                text: ''
            },
            property: {
                selected: { id: '', text: '' }
            },
            displayFields: ['ManualVisitReasonCode', 'ManualVisitReasonDesc']
        }
    };
    public selectBox: any = {
        NoBarcodeReasonSelect: [],
        NoSignatureReasonSelect: [],
        NoPremiseVisitReasonSelect: []
    };

    ngOnInit(): void {
        super.ngOnInit();
        this.promptTitle = MessageConstant.Message.ConfirmRecord;
        this.getSysCharDtetails();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPREMISEPDAICABSACTIVITYGRID;
        this.browserTitle = this.pageTitle = 'Premises PDA Visit';

    }

    private getSysCharDtetails(): any {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharShowPremiseWasteTab
        ];
        let sysCharIP: any = {
            module: this.queryParams.module,
            operation: this.queryParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            let record = data.records;
            this.pageParams.vShowPremiseWasteTab = record[0]['Required'];
            this.windowOnLoad();
        });
    }

    private windowOnLoad(): void {
        this['uiForm'].disable();
        this.riGrid.PageSize = 10;
        this.riGrid.FunctionPaging = true;
        this.riGrid.FunctionUpdateSupport = true;
        if (this.pageParams.vShowPremiseWasteTab) {
            this.isWasteTabVisible = true;
        }
        if (this.parentMode === 'General') {
            this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
            this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentHTMLValue('BranchServiceAreaDesc'));
            this.setControlValue('DebriefNumber', this.riExchange.getParentHTMLValue('DebriefNumber'));
            this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
            this.setControlValue('PlannedVisitDate', this.riExchange.getParentAttributeValue('PlannedVisitDate'));
            this.setControlValue('ActualVisitDate', this.riExchange.getParentAttributeValue('ActualVisitDate'));
            this.setControlValue('ActualStartTime', this.riExchange.getParentAttributeValue('ActualStartTime'));
            this.setControlValue('ActualEndTime', this.riExchange.getParentAttributeValue('ActualEndTime'));
            this.setControlValue('ServicePlanNumber', this.riExchange.getParentAttributeValue('ServicePlanNumber'));
            if (!this.getControlValue('EmployeeCode')) {
                this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
                this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentAttributeValue('BranchServiceAreaDesc'));
            }
            if (this.getControlValue('ActualVisitDate')) {
                this.disableControl('ActualVisitDate', true);
                this.disableControl('ActualStartTime', true);
                this.disableControl('ActualEndTime', true);
            }
        }
        this.fetchTableDataPremise();
        this.fetchTableDataALL();
        this.buildGrid();
        if (this.isReturning()) {
            this.populateUIFromFormData();
        }
    }

    private fetchTableDataPremise(): void {
        if (this.getControlValue('ContractNumber')) {
            let postSearchParams = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '0');
            postSearchParams.set('ContractNumber', this.getControlValue('ContractNumber'));
            postSearchParams.set('PremiseNumber', this.getControlValue('PremiseNumber'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if ((data['hasError'])) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.setControlValue('ContractNumber', data['ContractNumber']);
                            this.setControlValue('PremiseNumber', data['PremiseNumber']);
                            this.setControlValue('ContractName', data['ContractName']);
                            this.setControlValue('PremiseName', data['PremiseName']);
                            this.setControlValue('StatusDesc', data['StatusDesc']);
                            this.setControlValue('PremiseAddressLine1', data['PremiseAddressLine1']);
                            this.setControlValue('PremiseAddressLine2', data['PremiseAddressLine2']);
                            this.setControlValue('PremiseAddressLine3', data['PremiseAddressLine3']);
                            this.setControlValue('PremiseAddressLine4', data['PremiseAddressLine4']);
                            this.setControlValue('PremiseAddressLine5', data['PremiseAddressLine5']);
                            this.setControlValue('PremisePostcode', data['PremisePostcode']);
                            this.setControlValue('PremiseContactName', data['PremiseContactName']);
                            this.setControlValue('PremiseContactPosition', data['PremiseContactPosition']);
                            this.setControlValue('PremiseContactTelephone', data['PremiseContactTelephone']);
                            this.setControlValue('PremiseContactFax', data['PremiseContactFax']);
                            this.setControlValue('PremiseContactEmail', data['PremiseContactEmail']);
                            this.setControlValue('ProofSignatureRequiredInd', this.utils.convertResponseValueToCheckboxInput(data['ProofSignatureRequiredInd']));
                            this.setControlValue('ProofScanRequiredInd', this.utils.convertResponseValueToCheckboxInput(data['ProofScanRequiredInd']));
                        }
                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }

    }
    /**
     * This function validates the active tab for form errors
     */
    private validateTabs(): void {
        if (this.uiDisplay['tab'].tab1.active) {
            this.utils.makeTabsRedById(['grdVisits']);
        } else if (this.uiDisplay['tab'].tab2.active) {
            this.utils.makeTabsRedById(['grdAddress']);
        } else if (this.uiDisplay['tab'].tab3.active) {
            this.utils.makeTabsRedById(['grdSignature']);
        } else if (this.uiDisplay['tab'].tab4.active) {
            this.utils.makeTabsRedById(['grdWaste']);
        }
    }

    private fetchTableDataMVR(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        let postParams: any = {};
        postParams.ManualVisitReasonCode = this.getControlValue('PDAManualVisitReasonCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('PDAManualVisitReasonDesc', data['ManualVisitReasonDesc']);
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private fetchTableDataALL(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'GetFields';
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        postParams.PremiseNumber = this.getControlValue('PremiseNumber');
        postParams.EmployeeCode = this.getControlValue('EmployeeCode');
        postParams.PlannedVisitDate = this.getControlValue('PlannedVisitDate');
        postParams.ActualVisitDate = this.getControlValue('ActualVisitDate');
        postParams.DebriefNumber = this.getControlValue('DebriefNumber');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('ActualVisitDate', data['ActualVisitDate']);
                        this.setControlValue('ActualStartTime', data['ActualStartTime']);
                        this.setControlValue('ActualEndTime', data['ActualEndTime']);
                        this.setControlValue('PDAManualVisitReasonCode', data['PDAManualVisitReasonCode']);
                        this.setControlValue('NoPremiseVisitReasonNote', data['NoPremiseVisitReasonNote']);
                        this.setControlValue('NoPremiseVisitReasonCode', data['NoPremiseVisitReasonCode']);
                        this.setControlValue('NoPremiseVisitManualCode', data['NoPremiseVisitManualCode']);
                        this.setControlValue('NoBarcodeReasonCode', data['NoBarcodeReasonCode']);
                        this.setControlValue('NoBarcodeManualCode', data['NoBarcodeManualCode']);
                        this.setControlValue('NoSignatureReasonNumber', data['NoSignatureReasonNumber']);
                        this.setControlValue('NoSignatureManualNumber', data['NoSignatureManualNumber']);
                        this.setControlValue('ActivityStatusDesc', data['ActivityStatusDesc']);
                        this.setControlValue('NoBarcodeReasonString', data['NoBarcodeReasonString']);
                        this.setControlValue('NoSignatureReasonString', data['NoSignatureReasonString']);
                        this.setControlValue('NoPremiseVisitReasonString', data['NoPremiseVisitReasonString']);
                        this.setControlValue('AllowUpdate', data['AllowUpdate']);
                        this.setControlValue('GotSignature', data['GotSignature']);
                        this.setControlValue('PremiseContactSignatureURL', data['PremiseContactSignatureURL']);
                        this.setControlValue('PDAPremiseContactName', data['PDAPremiseContactName']);
                        this.setControlValue('GeneralNotes', data['GeneralNotes']);
                        this.setControlValue('NoPremiseVisitReasonCount', data['NoPremiseVisitReasonCount']);
                        this.setControlValue('NoBarcodeReasonCount', data['NoBarcodeReasonCount']);
                        this.setControlValue('NoSignatureReasonCount', data['NoSignatureReasonCount']);
                        this.buildDropDownOptions();
                        if (data['PDAManualVisitReasonCode']) {
                            this.dropdown.manualVisitReasonSearch.property.selected = { id: data['PDAManualVisitReasonCode'], text: data['PDAManualVisitReasonCode'] + ' - ' + data['PDAManualVisitReasonDesc'] };
                        }
                        this.afterFetch();
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private afterFetch(): void {
        if (this.getControlValue('GotSignature')) {
            this.trSignature = true;
            if (this.getControlValue('PremiseContactSignatureURL')) {
                this.signatureSrc = this.getControlValue('PremiseContactSignatureURL');
            } else {
                this.signatureSrc = '';
            }
        } else {
            this.trSignature = false;
        }
    }

    private riGridBeforeExecute(): void {
        this.riGrid.RefreshRequired();
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set('BranchNumber', this.utils.getBranchCode());
        this.search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        this.search.set('DebriefNumber', this.getControlValue('DebriefNumber'));
        this.search.set('PlannedVisitDate', this.getControlValue('PlannedVisitDate'));
        this.search.set('ActualVisitDate', this.getControlValue('ActualVisitDate'));
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        this.search.set(this.serviceConstants.PageSize, (this.itemsPerPage).toString());
        this.search.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        this.search.set('riSortOrder', 'Descending');
        this.search.set('HeaderClickedColumn', '');
        this.search.set('riCacheRefresh', 'True');
        this.queryParams.search = this.search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, this.queryParams.search)
            .subscribe(
                (data) => {
                    if (data) {
                        try {
                            this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                            if (data['hasError']) {
                                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            } else {
                                this.riGrid.Execute(data);
                            }

                        } catch (e) {
                            this.logger.log('Problem in grid load', e);
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                error => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.totalRecords = 1;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private confirmSingleVisit(): void {
        if (this.validateVisitParameters()) {
            let postSearchParams = this.getURLSearchParamObject();
            if (this.pageParams.gridLoad) {
                postSearchParams.set(this.serviceConstants.Action, '2');
                this.pageParams.gridLoad = false;

            }
            else {
                postSearchParams.set(this.serviceConstants.Action, '6');
            }
            let formData: any = {};
            formData.Function = 'ConfirmSingleVisit';
            formData.BranchNumber = this.utils.getBranchCode();
            formData.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            formData.ServicePlanDetailRowID = this.getAttribute('ProductCodeServicePlanDetailRowID');
            formData.EmployeeCode = this.getControlValue('EmployeeCode');
            formData.ActualVisitDate = this.getControlValue('ActualVisitDate');
            formData.ActualStartTime = this.getControlValue('ActualStartTime');
            formData.ActualEndTime = this.getControlValue('ActualEndTime');
            formData.ManualVisitReasonCode = this.getControlValue('PDAManualVisitReasonCode');
            formData.NoBarcodeReasonCode = this.getControlValue('NoBarcodeReasonSelect');
            formData.NoBarcodeManualCode = this.getControlValue('NoBarcodeManualCode');
            formData.NoSignatureReasonNumber = this.getControlValue('NoSignatureReasonSelect');
            formData.NoSignatureManualNumber = this.getControlValue('NoSignatureManualNumber');
            formData.NoPremiseVisitReasonCode = this.getControlValue('NoPremiseVisitReasonSelect');
            formData.NoPremiseVisitManualCode = this.getControlValue('NoPremiseVisitManualCode');
            formData.NoPremiseVisitReasonNote = this.getControlValue('NoPremiseVisitReasonNote');
            formData.GeneralNotes = this.getControlValue('GeneralNotes');
            formData.WasteConsignmentNoteNumber = this.getControlValue('WasteConsignmentNoteNumber');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, formData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.isDisplayGridSection = this.isDisplayRefPageSection = true;
                        if ((data['hasError'])) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            if (data['ErrorMessageDesc']) {
                                let arrError: any = data['ErrorMessageDesc'].split('|');
                                for (let i = 0; i < arrError.length; i++) {
                                    this.modalAdvService.emitMessage(new ICabsModalVO(arrError[i]));
                                }
                            }
                            if (data['ActivityStatusDesc']) {
                                this.setControlValue('ActivityStatusDesc', data['ActivityStatusDesc']);
                            }
                        }
                        this.refresh();

                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    private setErrorStatusOfFormElement(frmCtrlName: string, flag: boolean): void {
        this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, frmCtrlName, flag);
        if (flag) {
            this.riExchange.riInputElement.markAsError(this.uiForm, frmCtrlName);
        }
    }

    private validateVisitParameters(): boolean {
        let blnReturn: boolean = true;
        if (!this.getControlValue('ActualVisitDate') || this.riExchange.riInputElement.isError(this.uiForm, 'ActualVisitDate') || !this.getControlValue('PDAManualVisitReasonCode') || this.riExchange.riInputElement.isError(this.uiForm, 'PDAManualVisitReasonCode')) {
            this.formMode = this.c_s_MODE_UPDATE;
            this.disableControl('ActualVisitDate', false);
            this.disableControl('PDAManualVisitReasonCode', false);
            this.dropdown.manualVisitReasonSearch.isDisabled = false;
            this.disableControl('ActualStartTime', false);
            this.disableControl('ActualEndTime', false);
            this.isSaveDisplay = this.isCancelDisplay = true;
            this.isUpdateDisp = this.isDisplayGridSection = this.isDisplayRefPageSection = false;

            if (!this.getControlValue('ActualVisitDate')) {
                this.setErrorStatusOfFormElement('ActualVisitDate', true);
                this.isMakemandatory = true;
            }
            if (!this.getControlValue('PDAManualVisitReasonCode')) {
                this.setErrorStatusOfFormElement('PDAManualVisitReasonCode', true);
                this.dropdown.manualVisitReasonSearch.isRequired = true;
                this.dropdown.manualVisitReasonSearch.isTriggerValidate = true;
            }
            this.disableControl('NoBarcodeManualSelect', false);
            this.disableControl('NoSignatureManualSelect', false);
            this.disableControl('NoPremiseVisitManualSelect', false);
            this.modalAdvService.emitMessage(new ICabsModalVO('Update Manual Visit Reason, Visit Date And Times Before Creating Activity'));
        }
        if (!this.getControlValue('ActualVisitDate') || this.riExchange.riInputElement.isError(this.uiForm, 'ActualVisitDate')) {
            blnReturn = false;
            this.setErrorStatusOfFormElement('ActualVisitDate', true);
        }
        if (!this.getControlValue('ActualStartTime') || this.riExchange.riInputElement.isError(this.uiForm, 'ActualStartTime')) {
            blnReturn = false;
            this.setErrorStatusOfFormElement('ActualStartTime', true);
        }
        if (!this.getControlValue('ActualEndTime') || this.riExchange.riInputElement.isError(this.uiForm, 'ActualEndTime')) {
            blnReturn = false;
            this.setErrorStatusOfFormElement('ActualEndTime', true);
        }
        if (!this.getControlValue('PDAManualVisitReasonCode') || this.riExchange.riInputElement.isError(this.uiForm, 'PDAManualVisitReasonCode')) {
            blnReturn = false;
            this.setErrorStatusOfFormElement('PDAManualVisitReasonCode', true);
            this.dropdown.manualVisitReasonSearch.isRequired = true;
            this.dropdown.manualVisitReasonSearch.isTriggerValidate = true;
        }

        return blnReturn;
    }

    private buildDropDownOptions(): void {
        if (this.getControlValue('NoBarcodeReasonString')) {
            this.noBarCodeText = this.getControlValue('NoBarcodeReasonString');
            if (this.noBarCodeText) {
                let array1: any = this.noBarCodeText.split('|');
                for (let i = 0; i < array1.length; i++) {
                    let optionVal = array1[i].split(',')[0];
                    let optionText = array1[i].split(',')[1];
                    this.NBCStringArr.push({ 'text': optionText, 'value': optionVal });
                    if (optionVal === String(this.getControlValue('NoBarcodeReasonCode'))) {
                        this.selectBox['NoBarcodeReasonSelect'] = [{
                            value: optionVal,
                            text: optionText
                        }];
                        this.setControlValue('NoBarcodeReasonSelect', optionVal);
                    }
                }
                this.setControlValue('NoBarcodeManualSelect', this.getControlValue('NoBarcodeManualCode'));
            }
        }

        if (this.getControlValue('NoSignatureReasonString')) {
            this.noSignText = this.getControlValue('NoSignatureReasonString');
            if (this.noSignText) {
                let array2: any = this.noSignText.split('|');
                for (let i = 0; i < array2.length; i++) {
                    let optionVal = array2[i].split(',')[0];
                    let optionText = array2[i].split(',')[1];
                    this.NSRStringArr.push({ 'text': optionText, 'value': optionVal });
                    if (optionVal === String(this.getControlValue('NoSignatureReasonNumber'))) {
                        this.selectBox['NoSignatureReasonSelect'] = [{
                            value: optionVal,
                            text: optionText
                        }];
                        this.setControlValue('NoSignatureReasonSelect', optionVal);
                    }
                }
                this.setControlValue('NoSignatureManualSelect', this.getControlValue('NoSignatureManualNumber'));
            }
        }

        if (this.getControlValue('NoPremiseVisitReasonString')) {
            this.noPremiseVisitText = this.getControlValue('NoPremiseVisitReasonString');
            if (this.noPremiseVisitText) {
                let array3: any = this.noPremiseVisitText.split('|');
                for (let i = 0; i < array3.length; i++) {
                    let optionVal = array3[i].split(',')[0];
                    let optionText = array3[i].split(',')[1];
                    this.NPVStringArr.push({ 'text': optionText, 'value': optionVal });
                    if (optionVal === String(this.getControlValue('NoPremiseVisitReasonCode'))) {
                        this.selectBox['NoPremiseVisitReasonSelect'] = [{
                            value: optionVal,
                            text: optionText
                        }];
                        this.setControlValue('NoPremiseVisitReasonSelect', optionVal);
                    }
                }
                this.setControlValue('NoPremiseVisitManualSelect', this.getControlValue('NoPremiseVisitManualCode'));
            }
        }
    }


    public confirmCallBack(instance: Object): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '2');
        let postParams: any = {};
        postParams.PremiseROWID = this.getAttribute('ProductCodeServiceCoverRowID');
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        postParams.PremiseNumber = this.getControlValue('PremiseNumber');
        postParams.ContractName = this.getControlValue('ContractName');
        postParams.PremiseName = this.getControlValue('PremiseName');
        postParams.PremiseAddressLine1 = this.getControlValue('PremiseAddressLine1');
        postParams.PremiseAddressLine2 = this.getControlValue('PremiseAddressLine2');
        postParams.PremiseAddressLine3 = this.getControlValue('PremiseAddressLine3');
        postParams.PremiseAddressLine4 = this.getControlValue('PremiseAddressLine4');
        postParams.PremiseAddressLine5 = this.getControlValue('PremiseAddressLine5');
        postParams.PremisePostcode = this.getControlValue('PremisePostcode');
        postParams.PremiseContactName = this.getControlValue('PremiseContactName');
        postParams.PremiseContactPosition = this.getControlValue('PremiseContactPosition');
        postParams.PremiseContactTelephone = this.getControlValue('PremiseContactTelephone');
        postParams.PremiseContactFax = this.getControlValue('PremiseContactFax');
        postParams.PremiseContactEmail = this.getControlValue('PremiseContactEmail');
        postParams.ProofSignatureRequiredInd = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ProofSignatureRequiredInd'));
        postParams.ProofScanRequiredInd = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ProofScanRequiredInd'));
        postParams.ActualVisitDate = this.getControlValue('ActualVisitDate');
        postParams.ActualStartTime = this.getControlValue('ActualStartTime');
        postParams.ActualEndTime = this.getControlValue('ActualEndTime');
        postParams.PDAManualVisitReasonCode = this.getControlValue('PDAManualVisitReasonCode');
        postParams.NoPremiseVisitReasonNote = this.getControlValue('NoPremiseVisitReasonNote');
        postParams.NoPremiseVisitReasonCode = this.getControlValue('NoPremiseVisitReasonCode');
        postParams.NoPremiseVisitManualCode = this.getControlValue('NoPremiseVisitManualCode');
        postParams.NoBarcodeReasonCode = this.getControlValue('NoBarcodeReasonCode');
        postParams.NoBarcodeManualCode = this.getControlValue('NoBarcodeManualCode');
        postParams.NoSignatureReasonNumber = this.getControlValue('NoSignatureReasonNumber');
        postParams.NoSignatureManualNumber = this.getControlValue('NoSignatureManualNumber');
        postParams.EmployeeCode = this.getControlValue('EmployeeCode');
        postParams.EmployeeSurname = this.getControlValue('EmployeeSurname');
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.BranchServiceAreaDesc = this.getControlValue('BranchServiceAreaDesc');
        postParams.StatusDesc = this.getControlValue('StatusDesc');
        postParams.ActivityStatusDesc = this.getControlValue('ActivityStatusDesc');
        postParams.PlannedVisitDate = this.getControlValue('PlannedVisitDate');
        postParams.ProductCode = this.getControlValue('ProductCode');
        postParams.ProductDesc = this.getControlValue('ProductDesc');
        postParams.ErrorMessageDesc = this.getControlValue('ErrorMessageDesc');
        postParams.NoBarcodeReasonString = this.getControlValue('NoBarcodeReasonString');
        postParams.NoSignatureReasonString = this.getControlValue('NoSignatureReasonString');
        postParams.NoPremiseVisitReasonString = this.getControlValue('NoPremiseVisitReasonString');
        postParams.AllowUpdate = this.getControlValue('AllowUpdate');
        postParams.DebriefNumber = this.getControlValue('DebriefNumber');
        postParams.GotSignature = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('GotSignature'));
        postParams.PremiseContactSignatureURL = this.getControlValue('PremiseContactSignatureURL');
        postParams.PDAPremiseContactName = this.getControlValue('PDAPremiseContactName');
        postParams.GeneralNotes = this.getControlValue('GeneralNotes');
        postParams.NoPremiseVisitReasonCount = this.getControlValue('NoPremiseVisitReasonCount');
        postParams.NoBarcodeReasonCount = this.getControlValue('NoBarcodeReasonCount');
        postParams.NoSignatureReasonCount = this.getControlValue('NoSignatureReasonCount');
        postParams.WasteConsignmentNoteNumber = this.getControlValue('WasteConsignmentNoteNumber');
        postParams.ServicePlanNumber = this.getControlValue('ServicePlanNumber');
        postParams.Function = 'GetFields';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                        this['uiForm'].disable();
                        this.dropdown.manualVisitReasonSearch.isDisabled = true;
                        this.setControlValue('ActivityStatusDesc', data['ActivityStatusDesc']);
                        this.setControlValue('ErrorMessageDesc', data['ErrorMessageDesc']);
                        this.isUpdateDisp = this.isDisplayGridSection = this.isDisplayRefPageSection = true;
                        this.isSaveDisplay = this.isCancelDisplay = false;
                        this.refresh();
                        this.pageParams.gridLoad = true;
                        this['uiForm'].markAsPristine();
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('ProdCode', 'Grid', 'ProdCode', MntConst.eTypeCode, 10, false);
        this.riGrid.AddColumnAlign('ProdCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProdDesc', 'Grid', 'ProdDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumnAlign('ProdDesc', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'Grid', 'ServiceVisitFrequency', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceCommenceDate', 'Grid', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceAreaCode', 'Grid', 'ServiceAreaCode', MntConst.eTypeCode, 1);
        this.riGrid.AddColumnAlign('ServiceAreacode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceTypeCode', 'Grid', 'ServiceTypeCode', MntConst.eTypeCode, 1);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlannedVisDate', 'Grid', 'PlannedVisDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('PlannedVisDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlannedVisitType', 'Grid', 'PlannedVisitType', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('PlannedVisitType', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlannedQuantity', 'Grid', 'PlannedQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PlannedQuantity', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('ServiceDateStart', 'Grid', 'ServiceDateStart', MntConst.eTypeDate, 15);
        this.riGrid.AddColumnAlign('ServiceDateStart', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ServiceDateStart', true);

        this.riGrid.AddColumn('VisitTypeCode', 'Grid', 'VisitTypeCode', MntConst.eTypeCode, 5);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('VisitTypeCode', true);

        this.riGrid.AddColumn('ServicedQuantity', 'Grid', 'ServicedQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServicedQuantity', MntConst.eAlignmentRight);
        this.riGrid.AddColumnUpdateSupport('ServicedQuantity', true);

        this.riGrid.AddColumn('FullServiceInd', 'Grid', 'FullServiceInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('FullServiceInd', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NoServiceReasonCode', 'Grid', 'NoServiceReasonCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('NoServiceReasonCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NoServiceManualCode', 'Grid', 'NoServiceManualCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('NoServiceManualCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('NoServiceManualCode', true);

        this.riGrid.AddColumn('VisitNote', 'Grid', 'VisitNote', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('VisitNote', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ManualVisitReasonCode', 'Grid', 'ManualVisitReasonCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('ManualVisitReasonCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ManualVisitReasonCode', true);

        this.riGrid.AddColumn('CreateVisit', 'Grid', 'CreateVisit', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('CreateVisit', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceVisitNumber', 'Grid', 'ServiceVisitNumber', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ServiceVisitNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Cancelled', 'Grid', 'Cancelled', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Cancelled', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PlanVisitStatusDesc', 'Grid', 'PlanVisitStatusDesc', MntConst.eTypeCode, 40);
        this.riGrid.AddColumnAlign('PlanVisitStatusDesc', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }


    private selectedRowFocus(event: any, rsrcElement: any): void {
        this.setControlValue('ProductCode', this.riGrid.Details.GetValue('ProdCode'));
        this.setControlValue('ProductDesc', this.riGrid.Details.GetValue('ProdDesc'));
        this.setAttribute('ProductCodeServiceCoverRowID', this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty'));
        this.setAttribute('ProductCodeServicePlanDetailRowID', this.riGrid.Details.GetAttribute('CreateVisit', 'additionalproperty'));
        this.setAttribute('BranchServiceAreaCodeServiceCoverRowID', this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty'));
        this.setAttribute('ContacTypeCode', this.riGrid.Details.GetAttribute('ProdDesc', 'additionalproperty'));
        this.setAttribute('BranchServiceAreaCodeServiceVisitRowID', this.riGrid.Details.GetAttribute('ServiceVisitNumber', 'additionalproperty'));
        this.setAttribute('ContractNumberPlanVisitRowID', this.riGrid.Details.GetAttribute('PlanVisitStatusDesc', 'additionalproperty'));
        switch (this.getAttribute('ContacTypeCode')) {
            case 'C':
                this.pageParams.currentContractTypeURLParameter = '';
                break;
            case 'J':
                this.pageParams.currentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.pageParams.currentContractTypeURLParameter = '<product>';
                break;
            default:
                break;
        }
        switch (rsrcElement.parentElement.parentElement.getAttribute('name')) {
            case 'ServiceVisitFrequency':
                this.navigate('ServiceVisitMaintenance', InternalGridSearchSalesModuleRoutes.ICABSAPLANVISITGRIDYEAR, {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'ContractName': this.getControlValue('ContractName'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'PremiseName': this.getControlValue('PremiseName'),
                    'ProductCode': this.riGrid.Details.GetValue('ProdCode'),
                    'ProductDesc': this.riGrid.Details.GetValue('ProdDesc'),
                    'ServiceCoverRowID': this.getAttribute('ProductCodeServiceCoverRowID')
                });
                break;
            case 'ProdCode':
                switch (this.pageParams.currentContractTypeURLParameter) {
                    case '':
                        this.navigate('ServicePlanning', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                            'CurrentContractTypeURLParameter': this.pageParams.currentContractTypeURLParameter,
                            'currentContractType': this.getAttribute('ContacTypeCode'),
                            'ServiceCoverRowID': this.getAttribute('ProductCodeServiceCoverRowID')
                        });
                        break;
                    case '<job>':
                        this.navigate('ServicePlanning', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                            'CurrentContractTypeURLParameter': this.pageParams.currentContractTypeURLParameter,
                            'currentContractType': this.getAttribute('ContacTypeCode'),
                            'ServiceCoverRowID': this.getAttribute('ProductCodeServiceCoverRowID')
                        });
                        break;
                    case '<product>':
                        this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE, {
                            'CurrentContractTypeURLParameter': this.pageParams.currentContractTypeURLParameter,
                            'currentContractType': this.getAttribute('ContacTypeCode'),
                            'ServiceCoverRowID': this.getAttribute('ProductCodeServiceCoverRowID')
                        });
                        break;
                    default:
                        break;
                }
                break;

            case 'ServiceVisitNumber':
                let parentMode: string = '';
                if (this.riGrid.Details.GetValue('ServiceVisitNumber') === 'yes') {
                    parentMode = 'ServiceVisit';
                }
                else {
                    parentMode = 'ServiceVisitEntryGrid';
                }
                this.navigate(parentMode, InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                    'CurrentContractTypeURLParameter': this.pageParams.currentContractTypeURLParameter,
                    'ServiceVisitRowID': this.getAttribute('BranchServiceAreaCodeServiceVisitRowID'),
                    'ServiceCoverRowID': this.getAttribute('ProductCodeServiceCoverRowID'),
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'ContractName': this.getControlValue('ContractName'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'PremiseName': this.getControlValue('PremiseName'),
                    'ProductDesc': this.riGrid.Details.GetValue('ProdDesc')
                });
                break;

            case 'NoServiceManualCode':
                // iCABSBNoServiceReasonSearch
                break;
            case 'ManualVisitReasonCode':
                // iCABSBManualVisitReasonSearch
                break;
            case 'CreateVisit':
                if (this.riGrid.Details.GetAttribute('CreateVisit', 'rowid') === 'Yes') {
                    this.confirmSingleVisit();
                }
                else {
                    if (event.srcElement.style.cursor !== '') {  //hand
                        this.modalAdvService.emitMessage(new ICabsModalVO('Service Activity Already Exists'));
                    }
                }
                break;

            case 'PlanVisitStatusDesc':
                if (this.riGrid.Details.GetValue('Cancelled') !== 'yes') {
                    this.navigate('Debrief', InternalMaintenanceApplicationModuleRoutes.ICABSAPLANVISITMAINTENANCE, {
                        'CurrentContractTypeURLParameter': this.pageParams.currentContractTypeURLParameter,
                        'currentContractType': this.getAttribute('ContacTypeCode'),
                        'PlanVisitRowID': this.getAttribute('ContractNumberPlanVisitRowID')
                    });
                }

                break;
            default:
                break;

        }
    }

    public renderTab(tabindex: number, isEvent?: boolean): void {
        this.isDisplayGridSection = this.isDisplayRefPageSection = true;
        if (isEvent) {
            this.validateTabs();
        }
        switch (tabindex) {
            case 1:
                this.uiDisplay.tab.tab1.active = true;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = false;
                break;
            case 2:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = true;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = false;
                break;
            case 3:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = true;
                this.uiDisplay.tab.tab4.active = false;
                break;
            case 4:
                this.uiDisplay.tab.tab1.active = false;
                this.uiDisplay.tab.tab2.active = false;
                this.uiDisplay.tab.tab3.active = false;
                this.uiDisplay.tab.tab4.active = true;
                break;
        }
    }
    public activityDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('ActualVisitDate', value.value);
        }
    }
    public visitDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('PlannedVisitDate', value.value);
        }
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageCurrent = currentPage.value;
        this.riGridBeforeExecute();
    }

    public onClickLeadAlert(): void {
        this.navigate('PremisesPDAVisit', InternalGridSearchApplicationModuleRoutes.ICABSCMHCALEADGRID, {
            parentMode: 'PremisesPDAVisit',
            EmployeeCode: this.getControlValue('EmployeeCode'),
            EmployeeSurname: this.getControlValue('EmployeeSurname'),
            ContractNumber: this.getControlValue('ContractNumber'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            ActualVisitDate: this.getControlValue('ActualVisitDate')
        });
    }
    public onClickBarcodes(): void {
        // iCABSSeScannedBarcodes
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
    }
    public onClickPremise(): void {
        this.navigate('GridSearch', this.ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
            CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
            contractTypeCode: this.riGrid.Details.GetAttribute('ProdDesc', 'additionalproperty'),
            parentMode: 'GridSearch',
            PremiseNumber: this.getControlValue('PremiseNumber'),
            ContractNumber: this.getControlValue('ContractNumber')
        });
    }

    /**
     * This function gets called when keyup event is triggered on save button
     * @param event
     */
    public onKeyUpSwitch(event: Object): void {
        this.isDisplayGridSection = this.isDisplayRefPageSection = true;
        this.validateTabs();
        this.utils.tabSwitchOnTab(event);
    }

    public onClickUpdate(event: Object): void {
        this.isCancelDisplay = this.isSaveDisplay = true;
        this.isUpdateDisp = this.isDisplayGridSection = false;
        this.disableControl('ActualVisitDate', false);
        this.disableControl('PDAManualVisitReasonCode', false);
        this.dropdown.manualVisitReasonSearch.isDisabled = false;
        this.disableControl('ActualStartTime', false);
        this.disableControl('ActualEndTime', false);
        this.disableControl('GeneralNotes', false);
        this.disableControl('PDAPremiseContactName', false);
        this.disableControl('WasteConsignmentNoteNumber', false);
        this.disableControl('NoBarcodeManualSelect', false);
        this.disableControl('NoSignatureManualSelect', false);
        this.disableControl('NoPremiseVisitManualSelect', false);
        this.formActualVisitDate.el.nativeElement.children[0].children[0].children[0].focus();
    }

    public onClickCancel(event: Object): void {
        this.isCancelDisplay = this.isSaveDisplay = false;
        this.isUpdateDisp = this.isDisplayGridSection = this.isDisplayRefPageSection = true;
        this.setControlValue('PDAManualVisitReasonCode', '');
        this.dropdown.manualVisitReasonSearch.property.selected = { id: '', text: '' };
        this.dropdown.manualVisitReasonSearch.isRequired = false;
        this.dropdown.manualVisitReasonSearch.isTriggerValidate = false;
        this.dropdown.manualVisitReasonSearch.isDisabled = true;
        this['uiForm'].disable();
        this.fetchTableDataPremise();
        this.fetchTableDataALL();
        this['uiForm'].markAsPristine();
    }

    public onClickSave(): void {
        //this.fetchTableDataMVR();
        this.modalAdvService.emitPrompt(new ICabsModalVO(this.promptTitle, null, this.confirmCallBack.bind(this)));
    }

    public riGridBodyOnDBLClick(ev: any): void {
        this.selectedRowFocus(ev, ev.srcElement);
    }

    public riGridBodyOnBlur(ev: any): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '0');
        postSearchParams.set(this.serviceConstants.GridHeaderClickedColumn, '');
        postSearchParams.set(this.serviceConstants.GridSortOrder, 'Ascending');
        postSearchParams.set(this.serviceConstants.GridMode, '3');
        postSearchParams.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        let postParams: any = {};
        postParams.ProdCode = this.riGrid.Details.GetValue('ProdCode');
        postParams.ProdDesc = this.riGrid.Details.GetValue('ProdDesc');
        postParams.ServiceVisitFrequency = this.riGrid.Details.GetValue('ServiceVisitFrequency');
        postParams.ServiceCommenceDate = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ServiceCommenceDate'));
        postParams.ServiceAreaCode = this.riGrid.Details.GetValue('ServiceAreaCode');
        postParams.ServiceTypeCode = this.riGrid.Details.GetValue('ServiceTypeCode');
        postParams.PlannedVisDate = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('PlannedVisDate'));
        postParams.PlannedVisitType = this.riGrid.Details.GetValue('PlannedVisitType');
        postParams.PlannedQuantity = this.riGrid.Details.GetValue('PlannedQuantity');
        postParams.ServiceDateStartRowID = this.riGrid.Details.GetAttribute('ServiceDateStart', 'rowid');
        postParams.ServiceDateStart = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('ServiceDateStart'));
        postParams.VisitTypeCodeRowID = this.riGrid.Details.GetAttribute('VisitTypeCode', 'rowid');
        postParams.VisitTypeCode = this.riGrid.Details.GetValue('VisitTypeCode');
        postParams.ServicedQuantityRowID = this.riGrid.Details.GetAttribute('ServicedQuantity', 'rowid');
        postParams.ServicedQuantity = this.riGrid.Details.GetValue('ServicedQuantity');
        postParams.NoServiceReasonCode = this.riGrid.Details.GetValue('NoServiceReasonCode');
        postParams.NoServiceManualCode = this.riGrid.Details.GetValue('NoServiceManualCode');
        postParams.ManualVisitReasonCodeRowID = this.riGrid.Details.GetAttribute('ManualVisitReasonCode', 'rowid');
        postParams.ManualVisitReasonCode = this.riGrid.Details.GetValue('ManualVisitReasonCode');
        postParams.PlanVisitStatusDesc = this.riGrid.Details.GetValue('PlanVisitStatusDesc');
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        postParams.EmployeeCode = this.getControlValue('EmployeeCode');
        postParams.DebriefNumber = this.getControlValue('DebriefNumber');
        postParams.PlannedVisitDate = this.getControlValue('PlannedVisitDate');
        postParams.ActualVisitDate = this.getControlValue('ActualVisitDate');
        postParams.ContractNumber = this.getControlValue('ContractNumber');
        postParams.PremiseNumber = this.getControlValue('PremiseNumber');
        postParams.ServicePlanNumber = this.getControlValue('ServicePlanNumber');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        let modalVO: ICabsModalVO = new ICabsModalVO(data.errorMessage, data.fullError);
                        modalVO.closeCallback = this.focusOnGrid.bind(this, ev);
                        this.modalAdvService.emitMessage(modalVO);
                    } else {
                        this.isCancelDisabled = this.isUpdateDisabled = this.isSaveDisabled = false;
                        this.riGrid.Mode = MntConst.eModeNormal;
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    public riGridBodyOnKeyDown(ev: any): void {
        this.isCancelDisabled = this.isUpdateDisabled = this.isSaveDisabled = true;
    }

    public focusOnGrid(ev: any): void {
        this.riGrid.setFocusBack(ev.srcElement);
    }

    public onChangeNoPremiseVisitManualSelect(): void {
        this.setControlValue('NoPremiseVisitManualCode', this.getControlValue('NoPremiseVisitManualSelect'));
    }

    public onChangeNoSignatureManualSelect(): void {
        this.setControlValue('NoSignatureManualNumber', this.getControlValue('NoSignatureManualSelect'));
    }

    public onChangeNoBarcodeManualSelect(): void {
        this.setControlValue('NoBarcodeManualCode', this.getControlValue('NoBarcodeManualSelect'));
    }

    public onManualVisitReceived(data: any): void {
        this.setControlValue('PDAManualVisitReasonCode', data['ManualVisitReasonCode']);
        this.setControlValue('PDAManualVisitReasonDesc', data['ManualVisitReasonDesc']);
        this.uiForm.controls['PDAManualVisitReasonCode'].markAsDirty();
    }

}
