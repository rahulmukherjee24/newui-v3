import { MessageConstant } from '../../../shared/constants/message.constant';

export const GridSearchModuleConstants = {
    branchServicePlanningSummary: {
        btnBranchSummary: 'Branch Summary',
        btnConfirmSelcetedPlans: 'Confirm Selected Plans',
        btnReserveStock: 'Reserve Stock',
        btnSelectAll: 'Select All',
        btnSelectNone: 'Select None',
        btnStockCheckSelected: 'Stock Check Selected',
        btnDisabledTitle: 'Please select at least one row from grid above',
        confirmSelectAPIAction: 'ConfirmServicePlan',
        gridColCountPerRow: 19,
        pageTitle: 'Branch Service Planning Summary'
    },
    reserveStockReport: {
        lblTime: 'Time',
        lblBranch: 'Branch',
        lblPlanDtFrom: 'Plan Date From',
        lblPlanDtTo: 'Plan Date To',
        lblProdCode: 'Product Code',
        lblDepot: 'Depot',
        lblCompanyCode: 'Company Code',
        dropDownDefault: 'Select one',
        pageId: 'ReserveStock',
        pageTitle: 'Branch Service Planning Reserve Stock',
        MAX_CHUNK_SIZE: 250,
        noRecordFound: MessageConstant.Message.noRecordFound,
        defaultSortColumn: 'QtrDifference'
    }
};

