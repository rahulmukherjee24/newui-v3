import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs';
import { Subscriber } from 'rxjs/Subscriber';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { GlobalConstant } from './../../../shared/constants/global.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { VariableService } from './../../../shared/services/variable.service';
import { ServiceVisitPlanningGridHelper } from './iCABSAServiceVisitPlanningGridHelper.service';
import { InternalMaintenanceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSAServiceVisitPlanningGrid.html',
    providers: [ServiceVisitPlanningGridHelper]
})
export class ServiceVisitPlanningGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: Object = {
        method: 'service-delivery/maintenance',
        module: 'service',
        operation: 'Application/iCABSAServiceVisitPlanningGrid'
    };
    private vbNoAnnualTimeValidation: string;
    private isVBSaveAbandonClose: boolean;
    private canDeactivateObservable: Observable<boolean>;
    private isNavigateToChildPage: boolean;
    private isGridClicked: boolean;
    private goBackSucscriber: Subscriber<boolean>;

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'SelectedYear', type: MntConst.eTypeInteger },
        { name: 'PremiseNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceVisitFrequency', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalVisits', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ProductCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceAnnualTime', disabled: true, type: MntConst.eTypeText },
        { name: 'TotalHours', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceVisitAnnivDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'ServiceAnnualValue', disabled: true, type: MntConst.eTypeCurrency },
        //hidden fields
        { name: 'UpdateCalendar' },
        { name: 'SelectedDay' },
        { name: 'SelectedMonth' },
        { name: 'SCRowID' },
        { name: 'SCCalRowID' },
        { name: 'CalendarUpdateAllowed' },
        { name: 'CalendarMode' },
        { name: 'LastChangeEffectDate' },//type=checkbox as per vbscript
        { name: 'RequireAnnualTimeInd' },//type=checkbox as per vbscript
        { name: 'CloseWithoutSave' },
        { name: 'CurrentYearStartDate' },
        { name: 'CurrentYearEndDate' }
    ];
    public legends: Array<Object>;
    public isIvalidTotalVisits: boolean = false;
    public isIvalidTotalHours: boolean = false;
    //Grid Component variables
    public pageSize: number = 11;
    public curPage: number = 1;
    //Dropdown
    public selectedYearList: Array<Object> = new Array<Object>();

    constructor(injector: Injector, public helper: ServiceVisitPlanningGridHelper, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSASERVICEVISITPLANNINGGRID;
        this.browserTitle = this.pageTitle = MessageConstant.PageSpecificMessage.aServiceVisitPlanningGrid.pageTitle;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.selectedYearList = this.helper.buildYearOptions();
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.applyStateRetation();
        } else {
            this.helper.init(this.pageParams);
            this.windowOnload();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * Used to set initial data on page load.
     */
    private windowOnload(): void {
        //select current year
        this.setControlValue('SelectedYear', this.utils.year(new Date()));

        //set data from parent page
        this.riExchange.getParentHTMLValue('ContractNumber');
        this.riExchange.getParentHTMLValue('ContractName');
        this.riExchange.getParentHTMLValue('PremiseNumber');
        this.riExchange.getParentHTMLValue('PremiseName');
        this.riExchange.getParentHTMLValue('ProductCode');
        this.riExchange.getParentHTMLValue('ProductDesc');
        //this.riExchange.getParentHTMLValue('CalendarUpdateAllowed');
        this.riExchange.getParentHTMLValue('ServiceVisitAnnivDate');
        this.riExchange.getParentHTMLValue('ServiceAnnualValue');
        this.riExchange.getParentHTMLValue('LastChangeEffectDate');
        this.setControlValue('CalendarMode', this.riExchange.getParentAttributeValue('Mode'));
        this.setControlValue('CalendarUpdateAllowed', true);
        this.setControlValue('SCRowID', this.riExchange.getParentHTMLValue('ServiceCover'));//Need to check with parent page
        //check annual time is required or not
        this.requireAnnualTime();
    }

    /**
     * Used to imply the previous state data on return back to this page.
     */
    private applyStateRetation(): void {
        //Grid
        this.initGrid();
    }

    //Start: Grid Private functionality
    private initGrid(): void {
        this.riGrid.DefaultBorderColor = 'DDDDDD';
        this.riGrid.HidePageNumber = true;
        this.buildGrid();
        this.riGridBeforeExecute();
    }
    /**
     * Used to build the grid.
     */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Month', 'Visit', 'Month', MntConst.eTypeText, 20);
        for (let iLoop: number = 1; iLoop <= 31; iLoop++) {
            this.riGrid.AddColumn(iLoop.toString(), 'Visit', iLoop.toString(), MntConst.eTypeText, 4);
            this.riGrid.SetColumnFixedWidth(iLoop.toString(), 30);
            this.riGrid.AddColumnAlign(iLoop.toString(), MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('Total', 'Visit', 'Total', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('Total', MntConst.eAlignmentCenter);
        if (this.getControlValue('RequireAnnualTimeInd')) {
            this.riGrid.AddColumn('TimeTotal', 'Visit', 'TimeTotal', MntConst.eTypeText, 20);
            this.riGrid.AddColumnAlign('TimeTotal', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('NettValue', 'Visit', 'NettValue', MntConst.eTypeCurrency, 20);
        this.riGrid.AddColumnAlign('NettValue', MntConst.eAlignmentCenter);
        this.riGrid.Complete();
    }

    /**
     * Fetch Grid data.
     */
    private riGridBeforeExecute(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '2');
        //grid parameters
        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.utils.randomSixDigitString();
        formData[this.serviceConstants.GridPageSize] = this.pageSize;
        formData[this.serviceConstants.GridPageCurrent] = this.curPage;
        formData['riSortOrder'] = this.riGrid.SortOrder;
        formData['HeaderClickedColumn'] = this.riGrid.HeaderClickedColumn;
        formData['riCacheRefresh'] = 'true';

        // set parameters
        formData['methodtype'] = 'grid';
        formData['SCRowID'] = this.getControlValue('SCRowID');
        formData['SelectedDay'] = this.getControlValue('SelectedDay');
        formData['SelectedMonth'] = this.getControlValue('SelectedMonth');
        formData['SelectedYear'] = this.getControlValue('SelectedYear');
        formData['UpdateCalendar'] = this.getControlValue('UpdateCalendar');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData).subscribe(
            (data) => {
                if (!this.handleSuccessError(data)) {
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(data);
                }
            },
            (error) => this.handleError(error));
    }
    //End: Grid Private functionality

    //Start: API call functionality
    private handleError(error: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
    }

    private handleSuccessError(data: any): boolean {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        if (data.hasError) {
            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
        }
        return data.hasError;
    }

    private requireAnnualTime(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['methodtype'] = 'maintenance';
        formData['Function'] = 'RequireAnnualTime';
        formData['ProductCode'] = this.getControlValue('ProductCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe((data) => {
                if (!this.handleSuccessError(data)) {
                    this.pageParams.isTrAnnualTime = (data.RequireAnnualTimeInd === GlobalConstant.Configuration.Yes.toLowerCase());
                    this.setControlValue('RequireAnnualTimeInd', this.pageParams.isTrAnnualTime);
                    this.initGrid();
                }
            }, (error) => this.handleError(error));
    }

    private getSelectedRowID(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['methodtype'] = 'maintenance';
        formData['SelectedYear'] = this.getControlValue('SelectedYear');
        formData['SelectedMonth'] = this.getControlValue('SelectedMonth');
        formData['SelectedDay'] = this.getControlValue('SelectedDay');
        formData['Function'] = 'GetSelectedRowID';
        formData['SCRowID'] = this.getControlValue('SCRowID');
        formData['CalendarMode'] = this.getControlValue('CalendarMode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe((data) => {
                let mode: string;
                if (!this.handleSuccessError(data)) {
                    this.isGridClicked = true;
                    this.setAttribute('CalDate', data.SCCalDate);
                    this.setAttribute('RowID', data.SCCalRowID);
                    this.setAttribute('ServiceCoverNumber', data.ServiceCoverNumber);
                    mode = (data.SCCalRowID) ? 'Update' : 'Add';
                    this.navigate(mode, InternalMaintenanceModuleRoutes.ICABSASERVICEVISITPLANNINGMAINTENANCE);
                }
            }, (error) => this.handleError(error));
    }
    private validateCurrentYear(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['methodtype'] = 'maintenance';
        formData['Function'] = 'ValidateCurrentYear';
        formData['SCRowID'] = this.getControlValue('SCRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe((data) => {
                let mode: string;
                if (!this.handleSuccessError(data)) {
                    if (data.MisMatchedTimes !== 'no') {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.aServiceVisitPlanningGrid.misMatchedTimes, null, this.save.bind(this)));
                    } else {
                        this.save();
                    }
                }
            }, (error) => this.handleError(error));
    }
    private save(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['methodtype'] = 'maintenance';
        formData['SelectedYear'] = this.getControlValue('SelectedYear');
        formData['Function'] = 'Save';
        formData['SCRowID'] = this.getControlValue('SCRowID');
        formData['CalendarMode'] = this.getControlValue('CalendarMode');
        formData['LastChangeEffectDate'] = this.getControlValue('LastChangeEffectDate');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe((data) => {
                if (!this.handleSuccessError(data)) {
                    this.isVBSaveAbandonClose = true;
                    this.variableService.setBackClick(true);
                    this.location.back();
                }
            }, (error) => this.handleError(error));
    }

    private undoChanges(isClosePage: boolean = false): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['methodtype'] = 'maintenance';
        formData['Function'] = 'UndoChanges';
        formData['SCRowID'] = this.getControlValue('SCRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe((data) => {
                if (!this.handleSuccessError(data)) {
                    if (isClosePage) {
                        this.variableService.setBackClick(true);
                        this.location.back();
                    } else {
                        this.riGrid.RefreshRequired();
                        this.riGridBeforeExecute();
                    }
                }
            }, (error) => this.handleError(error));
    }
    private validateRecords(): void {
        let search: QueryParams = this.getURLSearchParamObject(), formData: Object = {};
        search.set(this.serviceConstants.Action, '6');

        formData['methodtype'] = 'maintenance';
        formData['Function'] = 'ValidateRecords';
        formData['SCRowID'] = this.getControlValue('SCRowID');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, formData)
            .subscribe((data) => {
                if (!this.handleSuccessError(data)) {
                    let modalVO: ICabsModalVO;
                    if (data.CloseWithoutSave !== 'no') {
                        modalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.aServiceVisitPlanningGrid.msg1, null, this.riExchangeUnloadHTMLDocument.bind(this));
                        modalVO.title = 'Planning Changes';
                        this.modalAdvService.emitPrompt(modalVO);
                    } else {
                        modalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.aServiceVisitPlanningGrid.msg2);
                        modalVO.title = 'Warning';
                        this.modalAdvService.emitMessage(modalVO);
                    }
                }
            }, (error) => this.handleError(error));
    }
    //End: API call functionality

    //Start: Grid functionality
    public riGridAfterExecute(): void {
        let iBeforeCurrentMonth: number, iAfterCurrentMonth: number, currentMonth: number, dt: Date = new Date(), totalString: any, addProp: string;

        //Prepare legend data
        addProp = this.riGrid.Details.GetAttribute('Month', 'additionalProperty');
        if (addProp) {
            setTimeout(() => {
                this.legends = [];
                let legend: Array<string> = addProp.split('<td'), str: string, color: string, label: string, text: string;
                for (let i: number = 1; i < legend.length; i++) {
                    str = legend[i];
                    color = '';
                    text = '';
                    label = '';
                    //color
                    if (str.indexOf('"#') >= 0) {
                        color = str.substr(str.indexOf('"#') + 1, 7);
                    } else {
                        text = str.substr(str.indexOf('">') + 2, str.substr(str.indexOf('">') + 2).indexOf('</td>'));
                    }
                    i = i + 1;
                    str = legend[i];
                    label = str.substr(str.indexOf('>') + 1, str.substr(str.indexOf('>') + 1).indexOf('&'));
                    this.legends.push({
                        color: color,
                        text: text,
                        label: label
                    });
                }
            }, 100);
        }

        //this.riGrid.SetDefaultFocus();
        currentMonth = dt.getMonth();
        iBeforeCurrentMonth = dt.getMonth();
        iAfterCurrentMonth = dt.getMonth() + 1;

        //'SH 18/01/2005 - QRSAUS1620 Bug:  If in January, cannot create marker line for previous month.
        let objList = document.querySelectorAll('.gridtable tbody > tr');
        if (objList && objList.length >= currentMonth) {
            let tr = objList[currentMonth];
            if (tr) {
                tr.setAttribute('class', 'currentMonth');
            }
        }

        //'Set Total value fields
        totalString = this.riGrid.HTMLGridBody.children[1].children[0].getAttribute('additionalproperty').split('|');
        this.setControlValue('TotalVisits', totalString[0]);
        this.setControlValue('TotalHours', totalString[1]);
        this.setControlValue('ServiceVisitFrequency', totalString[2]);
        this.setControlValue('ServiceAnnualTime', totalString[3]);
        this.vbNoAnnualTimeValidation = totalString[4];


        this.uiForm.controls['TotalVisits'].markAsUntouched();
        this.isIvalidTotalVisits = false;
        this.uiForm.controls['TotalHours'].markAsUntouched();
        this.isIvalidTotalHours = false;

        if (this.getControlValue('TotalVisits') !== this.getControlValue('ServiceVisitFrequency')) {
            this.uiForm.controls['TotalVisits'].markAsTouched();
            this.isIvalidTotalVisits = true;
        }
        if (this.getControlValue('TotalHours') !== this.getControlValue('ServiceAnnualTime')) {
            this.uiForm.controls['TotalHours'].markAsTouched();
            this.isIvalidTotalHours = true;
        }
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public riGridBodyDblClick(e: Event): void {
        if (this.getControlValue('CalendarUpdateAllowed')) {
            if (this.utils.hasClass(e['srcElement'], 'pointer')) {
                this.setControlValue('SelectedDay', this.riGrid.CurrentColumnName);
                this.setControlValue('SelectedMonth', this.riGrid.Details.GetAttribute(this.riGrid.CurrentColumnName, 'additionalproperty'));
                this.getSelectedRowID();
            }
        }

    }
    //End: Grid functionality

    public btnSaveOnClick(): void {
        if (this.getControlValue('ServiceVisitFrequency') === this.getControlValue('TotalVisits') && (this.getControlValue('ServiceAnnualTime') === this.getControlValue('TotalHours') || this.vbNoAnnualTimeValidation === 'Yes')) {
            this.validateCurrentYear();
        }
    }

    public btnUndoChangesOnClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.aServiceVisitPlanningGrid.undoChanges, null, () => { this.undoChanges(false); }));
    }

    public riExchangeQueryUnloadHTMLDocument(): void {
        if (!this.isVBSaveAbandonClose) {
            this.validateRecords();
        } else {
            this.riExchangeUnloadHTMLDocument();
        }
    }

    public riExchangeUnloadHTMLDocument(): void {
        this.isNavigateToChildPage = true;
        this.undoChanges(true);
    }

    /*
     *  Alerts user when user is moving away without saving the changes. //CR implementation
     */
    public canDeactivate(): Observable<boolean> {
        if (!this.isNavigateToChildPage) {
            this.routeAwayGlobals.setSaveEnabledFlag(false);
            this.canDeactivateObservable = new Observable((observer) => {
                this.goBackSucscriber = observer;
                if (!this.isGridClicked) {
                    this.riExchangeQueryUnloadHTMLDocument();
                    this.dontGoBack(observer);
                } else {
                    observer.next(true);
                }
            });
            return this.canDeactivateObservable;
        } else {
            this.routeAwayGlobals.setSaveEnabledFlag(false);
            if (this.routeAwayComponent) {
                return this.routeAwayComponent.canDeactivate();
            }
        }
    }
    public dontGoBack(observer?: any): void {
        if (this.variableService.getBackClick() === true) {
            this.variableService.setBackClick(false);
            this.location.go(this.utils.getCurrentUrl());
        }
        if (observer) {
            observer.next(false);
        }
        setTimeout(() => {
            this.router.navigate([], { skipLocationChange: true, preserveQueryParams: true });
        }, 0);
    }
}
