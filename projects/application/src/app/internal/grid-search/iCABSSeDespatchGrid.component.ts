import { Component, Injector, OnInit, OnDestroy, ViewChild, HostListener } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs/Rx';

import { PageIdentifier } from './../../base/PageIdentifier';
import {
    ContractManagementModuleRoutes, InternalGridSearchApplicationModuleRoutes,
    InternalGridSearchSalesModuleRoutes, InternalGridSearchServiceModuleRoutes, AppModuleRoutes, InternalMaintenanceServiceModuleRoutesConstant
} from './../../base/PageRoutes';
import { BaseComponent } from '../../base/BaseComponent';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ListGroupSearchComponent } from './iCABSBListGroupSearch.component';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { PremiseSearchComponent } from './../search/iCABSAPremiseSearch';
import { ProductSearchGridComponent } from './../search/iCABSBProductSearch';
import { BranchServiceAreaSearchComponent } from './../search/iCABSBBranchServiceAreaSearch';
import { BranchSearchComponent } from './../search/iCABSBBranchSearch';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { ScreenNotReadyComponent } from './../../../shared/components/screenNotReady';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { ContractActionTypes } from '../../actions/contract';
import { MessageConstant } from './../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSeDespatchGrid.html'
})

export class DespatchGridComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('branchDropdown') branchDropdown: BranchSearchComponent;
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('despatchPagination') despatchPagination: PaginationComponent;

    private inputParams: any = {
        method: 'service-delivery/maintenance',
        module: 'service',
        operation: 'Service/iCABSSeDespatchGrid'
    };

    public pageId: string = '';
    public controls = [
        { name: 'ServiceAreaListCode', value: '' },
        { name: 'ServiceAreaList', value: '' },
        { name: 'VisitTypeGroupCode', value: '' },
        { name: 'VisitTypeList', value: '' },
        { name: 'WeekNumber', disabled: true, value: '' },
        { name: 'GridPageSize', value: '11' },
        { name: 'CManualDates', value: '' },
        { name: 'ContractNumber', value: '' },
        { name: 'ContractName', value: '', disabled: true },
        { name: 'PremiseNumber', value: '' },
        { name: 'PremiseName', value: '', disabled: true },
        { name: 'VisitStatus', value: '' },
        { name: 'DespatchStatus', value: '' },
        { name: 'ProductCode', value: '' },
        { name: 'ProductComponentCode', value: '' },
        { name: 'ContractTypeFilter', value: '' },
        { name: 'ContractNameSearch', value: '' },
        { name: 'TownBegins', value: '' },
        { name: 'PostcodeSearch', value: '' },
        { name: 'ClientReference', value: '' },
        { name: 'ConfApptOnly', value: '' },
        { name: 'BranchServiceAreaCodeChange', value: '' },
        { name: 'EmployeeSurnameChange', value: '', disabled: true },
        { name: 'AssignServiceArea', value: '' },
        { name: 'AssignServiceEmployee', value: '', disabled: true },
        { name: 'UpdateComponentCode', value: '' },
        { name: 'UpdateComponentDesc', value: '', disabled: true },
        { name: 'StartDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'EndDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'EmployeeSurname', disabled: true, value: '' },
        { name: 'CurrentContractType', value: '' },
        { name: 'NegBranchNumber', value: '' },
        { name: 'InServiceTypeCode', value: '' },
        { name: 'TotPlannedQty', value: '' },
        { name: 'TotSelectedPlannedQty', value: '' },
        { name: 'ChangeVisitDate', type: MntConst.eTypeDate },
        { name: 'SelectedRowID' },
        { name: 'ServiceVisitRowID' },
        { name: 'PlanVisitRowID' }
    ];
    public focusElement: any;
    public isUpdateSelect: boolean = false;
    public isUpdateProcess: boolean = false;
    public vbOrigSelected: string = 'no';
    public vbUpdateRecord: string = '';
    public visitStatusOption: Array<any> = [];
    public contractTypeFilterArray: Array<any> = [];
    public totalRecords: number;
    public isDivMain3: boolean = false;
    public despatchStatusArray: Array<any> = [{ name: 'All', value: 'All' }];
    public selectedPlannedQty: string;
    public toSelectedPlannedQty: string;
    public isContractActarisk: boolean = true;
    public isServiceListActarisk: boolean = true;
    public activeBranchService: any = {};
    public screenNotReadyComponent: any = ScreenNotReadyComponent;
    public isHidePagination: boolean = true;
    public isServiceMandatory: boolean = true;
    public isContractmandatory: boolean = true;
    public  isFooterHide: boolean = true;
    public ellipsis: any = {
        serviceAreaList: {
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            inputParams: {
                LGListTypeCodeVal: '1',
                LGListGroupCodeFld: 'ServiceAreaListCode',
                LGListGroupDescFld: '',
                LGListDetailsFld: 'ServiceAreaList',
                parentMode: 'LookUp'
            },
            contentComponent: ListGroupSearchComponent
        },
        visitTypeGroupSearch: {
            inputParams: {
                LGListTypeCodeVal: '2',
                LGListGroupCodeFld: 'VisitTypeGroupCode',
                LGListGroupDescFld: '',
                LGListDetailsFld: 'VisitTypeList',
                parentMode: 'LookUp'
            },
            contentComponent: ListGroupSearchComponent
        },
        contractTypeSearch: {
            inputParams: {
                currentContractType: '',
                parentMode: 'LookUp'
            },
            contentComponent: ContractSearchComponent
        },
        premiseTypeSearch: {
            inputParams: {
                parentMode: 'LookUp',
                ContractNumber: '',
                ContractName: ''
            },
            contentComponent: PremiseSearchComponent
        },
        productCodeSearch: {
            inputParams: {
                parentMode: 'LookUp'
            },
            contentComponent: ProductSearchGridComponent
        },
        productComponentCodeSearch: {
            inputParams: {
                parentMode: 'LookUp-ProductComponentCode'
            },
            contentComponent: ProductSearchGridComponent
        },
        updateComponentCodeSearch: {
            inputParams: {
                parentMode: 'LookUp-UpdateComponentCode'
            },
            contentComponent: ProductSearchGridComponent
        },
        assignServiceAreaSearch: {
            inputParams: {
                parentMode: 'LookUp-AssignSADespatchGrid'
            },
            contentComponent: BranchServiceAreaSearchComponent
        },
        branchnServiceAreaSearch: {
            inputParams: {
                parentMode: 'LookUp-DespatchGrid'
            },
            contentComponent: BranchServiceAreaSearchComponent
        },
        dsespatchStatusCodeSearch: {
            component: this.screenNotReadyComponent,
            showHeader: true
        }
    };

    public dropDown: any = {
        branchSearch: {
            inputParamsBranch: {
                parentMode: 'LookUp-NegBranch'
            }
        },
        serviceTypeSearch: {
            inputParamsService: {
                params: {
                    parentMode: 'LookUpC'
                }
            }
        }
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEDESPATCHGRID;
        this.browserTitle = this.pageTitle = 'Despatch Grid';
    }


    ngOnInit(): void {
        super.ngOnInit();
        this.riGrid.FunctionUpdateSupport = true;
        if (this.isReturning()) {
            this.setAttribute('PlanVisitRowID', this.pageParams.planVisitRowID);
            this.setAttribute('Row', this.pageParams.rowID);
            this.setControlValue('GridPageSize', this.pageParams.pageSize);
            this.addVisitStatus();
            this.buildContractTypeFilterOption();
            this.beforeExecute();
            if (this.getControlValue('CManualDates')) {
                this.riExchange.riInputElement.Enable(this.uiForm, 'StartDate');
                this.riExchange.riInputElement.Enable(this.uiForm, 'EndDate');
            }
            this.loadData();
        } else {
            this.pageParams.curPage = 1;
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.windowsOnLoad();
            Observable.forkJoin(
                [this.getLookUpCallService(), this.doLookUpCallForContactType()]).subscribe((data) => {
                    if (data[0]) {
                        let record: Array<any> = data[0][0];
                        if (record.length > 0) {
                            this.pageParams.vCancelledDesc = record[0].PlanVisitStatusDesc;
                            this.pageParams.vInPlanningDesc = record[1].PlanVisitStatusDesc;
                            this.pageParams.vPlannedDesc = record[2].PlanVisitStatusDesc;
                            this.pageParams.vUnplannedDesc = record[3].PlanVisitStatusDesc;
                            this.pageParams.vVisitedDesc = record[4].PlanVisitStatusDesc;
                        }
                    }
                    if (data[1]) {
                        let record: Array<any> = data[1][0], recordLang: Array<any> = data[1][1], recordSystemParam: Array<any> = data[1][2];
                        if (record.length > 0) {
                            if (record[0]) {
                            this.pageParams.vbContractTypeConDesc = record[0].ContactTypeDesc ? record[0].ContactTypeDesc : '';
                        }
                        if (record[1]) {
                            this.pageParams.vbContractTypeJobDesc = record[1].ContactTypeDesc ? record[1].ContactTypeDesc : '';
                        }
                        if (record[2]) {
                            this.pageParams.vbContractTypeProdDesc = record[2].ContactTypeDesc ? record[2].ContactTypeDesc : '';
                        }
                        }
                        if (recordLang.length > 0) {
                            if (recordLang[0]) {
                            this.pageParams.vbContractTypeConDesc = recordLang[0].ContractTypeDesc ? recordLang[0].ContractTypeDesc : '';
                        }
                           if (recordLang[1]) {
                            this.pageParams.vbContractTypeJobDesc = recordLang[1].ContractTypeDesc ? recordLang[1].ContractTypeDesc : '';
                           }
                           if (recordLang[2]) {
                            this.pageParams.vbContractTypeProdDesc = recordLang[2].ContractTypeDesc ? recordLang[2].ContractTypeDesc : '';
                           }
                        }
                        if (recordSystemParam.length > 0) {
                            if (recordSystemParam[0].SystemParameterEndOfWeekDay > 7) {
                                recordSystemParam[0].SystemParameterEndOfWeekDay = 1;
                            }
                            this.pageParams.vbEndofWeekDate = recordSystemParam[0].SystemParameterEndOfWeekDay;
                            this.setDate();
                        }
                    }
                    this.addVisitStatus();
                    this.setControlValue('VisitStatus', 'All');
                    this.buildContractTypeFilterOption();
                    this.setControlValue('ContractTypeFilter', 'All');
                    this.setControlValue('DespatchStatus', 'All');
                    this.getLatestWeekNumber();
                    this.beforeExecute();
                });
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }


    @HostListener('window:keydown', ['$event'])
    keyboardInput(e: KeyboardEvent): void {
        this.onKeyDownDocumnt(e);
    }

    private onKeyDownDocumnt(e: any): void {
        if (!this.getControlValue('CManualDates')) {
            switch (e.keyCode) {
                case 106:
                    this.setDate();
                    this.getLatestWeekNumber();
                    this.riGrid.Clear();
                    this.isHidePagination = true;
                    break;
                case 107:
                    //+ 7 Days
                    this.setPlusMinusDate(7, 7);
                    this.getLatestWeekNumber();
                    this.riGrid.Clear();
                    this.isHidePagination = true;
                    break;
                case 109:
                    //- 7 Days
                    this.setPlusMinusDate(-7, -7);
                    if (this.utils.convertDate(this.getControlValue('EndDate')) < new Date()) {
                       this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.seServicePlanningGrid.planWeekPassed));
                       let promptVO: ICabsModalVO = new ICabsModalVO();
                       promptVO.msg = MessageConstant.PageSpecificMessage.seServicePlanningGrid.planWeekPassed;
                       promptVO.closeCallback = this.onModalClose.bind(this);
                       this.modalAdvService.emitMessage(promptVO);
                    } else {
                    this.getLatestWeekNumber();
                    this.riGrid.Clear();
                    this.isHidePagination = true;
                    }
                    break;
            }
        }
    }

    private onModalClose(): void {
       this.getLatestWeekNumber();
       this.riGrid.Clear();
       this.isHidePagination = true;
    }

    private windowsOnLoad(): void {
        const controlsSize: number = this.controls.length;
        for (let i = 0; i < controlsSize; i++) {
            this.riExchange.getParentHTMLValue(this.controls[i].name);
        }
        this.setControlValue('CurrentContractType', this.riExchange.getParentHTMLValue('CurrentContractTypeURLParameter'));
        this.setControlValue('GridPageSize', '11');
        this.pageParams.pageSize = this.getControlValue('GridPageSize');
        this.ellipsis.contractTypeSearch.inputParams.currentContractType = this.getControlValue('CurrentContractType');
        this.beforeExecute();
    }

    private setAsterisk(): void {
        if (this.getControlValue('ContractNumber') && this.getControlValue('ServiceAreaList')) {
            this.isContractActarisk = true;
            this.isServiceListActarisk = true;
        }
    }

    private getLookUpCallService(): any {
        let lookupIP: Array<any> = [
            {
                'table': 'PlanVisitStatusLang',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'LanguageCode': this.riExchange.LanguageCode()
                },
                'fields': ['PlanVisitStatusDesc']
            }
        ];
        return this.LookUp.lookUpPromise(lookupIP);
    }

    private setDate(): void {
        let startDate: any = new Date(), endDate: any = new Date(), dayofWeek: number = 7 - (new Date().getDay() + 1),
            totaldayofweek: number = dayofWeek + Number(this.pageParams.vbEndofWeekDate);
        startDate = this.utils.addDays(new Date(), (totaldayofweek + 1));
        endDate = this.utils.addDays(new Date(), (totaldayofweek + 7));
        this.setControlValue('StartDate', this.globalize.parseDateToFixedFormat(startDate) as string);
        this.setControlValue('EndDate', this.globalize.parseDateToFixedFormat(endDate) as string);
    }

    private setPlusMinusDate(startInterval: number, endInterval: number): void {
        let startDate: any = this.utils.addDays(new Date(this.getControlValue('StartDate')), startInterval);
        let endDate: any = this.utils.addDays(new Date(this.getControlValue('EndDate')), endInterval);
        this.setControlValue('StartDate', this.globalize.parseDateToFixedFormat(startDate) as string);
        this.setControlValue('EndDate', this.globalize.parseDateToFixedFormat(endDate) as string);
    }

    private printPlanVisit(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ContractNumber: this.riGrid.Details.GetValue('ContractNumberCol'),
            PremiseNumber: this.riGrid.Details.GetValue('PremiseNumberCol'),
            ProductCode: this.riGrid.Details.GetValue('ProductCodeCol'),
            ServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCodeCol', 'additionalproperty'),
            PlanVisitNumber: this.riGrid.Details.GetValue('PlanNumber')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                window.open(data.url);
                this.setPrintStatus();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private setPrintStatus(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ActionType: 'SetToPrintStatus',
            PlanVisitRowID: this.riGrid.Details.GetAttribute('Select', 'additionalproperty')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.loadData();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private doLookUpCallForContactType(): any {
        let lookupIP: any = [
            {
                'table': 'ContractType',
                'query': {
                    'BusinessCode': this.businessCode()
                },
                'fields': ['ContractTypeCode', 'ContractTypeDesc']
            },
            {
                'table': 'ContractTypeLang',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'LanguageCode': this.riExchange.LanguageCode()
                },
                'fields': ['ContractTypeDesc']
            },
            {
                'table': 'SystemParameter',
                'query': {
                    'BusinessCode': this.businessCode()
                },
                'fields': ['SystemParameterEndOfWeekDay']
            }
        ];
        return this.LookUp.lookUpPromise(lookupIP);
    }

    private addVisitStatus(): void {
        this.visitStatusOption.push({ name: 'All', value: 'All' });
        this.visitStatusOption.push({ name: 'U', value: this.pageParams.vUnplannedDesc });
        this.visitStatusOption.push({ name: 'I', value: this.pageParams.vInPlanningDesc });
        this.visitStatusOption.push({ name: 'C', value: this.pageParams.vCancelledDesc });
        this.visitStatusOption.push({ name: 'P', value: this.pageParams.vPlannedDesc });
        this.visitStatusOption.push({ name: 'V', value: this.pageParams.vVisitedDesc });
        this.visitStatusOption.push({ name: 'Outstanding', value: 'Outstanding' });
    }

    private buildContractTypeFilterOption(): void {
        this.contractTypeFilterArray.push({ name: 'All', value: 'All' });
        if (this.pageParams.vbContractTypeConDesc !== '') {
            this.contractTypeFilterArray.push({ name: this.pageParams.vbContractTypeConDesc, value: 'C' });
        }
        if (this.pageParams.vbContractTypeJobDesc !== '') {
            this.contractTypeFilterArray.push({ name: this.pageParams.vbContractTypeJobDesc, value: 'J' });
        }
        if (this.pageParams.vbContractTypeProdDesc !== '') {
            this.contractTypeFilterArray.push({ name: this.pageParams.vbContractTypeProdDesc, value: 'P' });
        }
        this.setDespatchStatus();
    }

    private setDespatchStatus(): void {
        let DespatchStatuses: string;
        let despatchArray: Array<any>;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ActionType: 'GetDespatchStatuses'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                if (data.DespatchStatuses) {
                    despatchArray = data.DespatchStatuses.split(',');
                    if (despatchArray.length > 1) {
                        for (let i = 0; i < despatchArray.length; i++) {
                            let codeDesc: Array<any> = despatchArray[i].split('|');
                            let despatchCode: any = codeDesc[0];
                            let despatchDesc: any = codeDesc[1];
                            this.despatchStatusArray.push({ name: despatchDesc, value: despatchCode });
                        }
                    }
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private getValidationDate(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', true);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', true);
        if (this.getControlValue('ContractNumber') && this.getControlValue('ClientReference')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', false);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', false);
        }
    }

    private onNegBranchNumberChange(): void {
        if (this.getControlValue('NegBranchNumber')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'GetBranchName',
                BranchNumber: this.utils.getBranchCode(),
                NegBranchNumber: this.getControlValue('NegBranchNumber')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.ErrorMessageDesc) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.ErrorMessageDesc));
                        this.setControlValue('NegBranchNumber', '');
                        this.pageParams.activeBranch = {
                            id: '',
                            text: ''
                        };
                        return;
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('NegBranchNumber', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private customValidation(): boolean {
        return (this['uiForm'].controls['ServiceAreaList'].valid && this['uiForm'].controls['ContractNumber'].valid);
    }

    private getLatestWeekNumber(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ActionType: 'GetLatestWeekNumber',
            StartDate: this.getControlValue('StartDate'),
            EndDate: this.getControlValue('EndDate')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.setControlValue('WeekNumber', data.WeekNumber);
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private beforeExecute(): void {
        this.riGrid.Update = false;
        this.riGrid.Clear();
        this.riGrid.AddColumn('VisitDate', 'PlanVisit', 'VisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('WeekNumberCol', 'PlanVisit', 'WeekNumberCol', MntConst.eTypeInteger, 2);
        this.riGrid.AddColumn('Status', 'PlanVisit', 'Status', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('PlanNumber', 'PlanVisit', 'PlanNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('Branch', 'PlanVisit', 'Branch', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('PlanVisitServiceArea', 'PlanVisit', 'PlanVisitServiceArea', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('VisitType', 'PlanVisit', 'VisitType', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ContractNumberCol', 'PlanVisit', 'ContractNumberCol', MntConst.eTypeText, 10, true, '');
        this.riGrid.AddColumn('PremiseNumberCol', 'PlanVisit', 'PremiseNumberCol', MntConst.eTypeInteger, 5, true, '');
        this.riGrid.AddColumn('PremiseName', 'PlanVisit', 'PremiseName', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('PremiseAddressLine1', 'PlanVisit', 'PremiseAddressLine1', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PremiseAddressLine2', 'PlanVisit', 'PremiseAddressLine2', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PremiseAddressLine3', 'PlanVisit', 'PremiseAddressLine3', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('TownPostCode', 'PlanVisit', 'TownPostCode', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('Town', 'PlanVisit', 'Town', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('PremiseAddressLine5', 'PlanVisit', 'PremiseAddressLine5', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PostCode', 'PlanVisit', 'PostCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumn('ContactName', 'PlanVisit', 'ContactName', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ContactPosition', 'PlanVisit', 'ContactPosition', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('ContactDept', 'PlanVisit', 'ContactDept', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ContactTelephone', 'PlanVisit', 'ContactTelephone', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('ContactMobile', 'PlanVisit', 'ContactMobile', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('ContactEmail', 'PlanVisit', 'ContactEmail', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ContactFax', 'PlanVisit', 'ContactFax', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('ProductCodeCol', 'PlanVisit', 'ProductCodeCol', MntConst.eTypeCode, 10, true, '');
        this.riGrid.AddColumn('SCoverServiceArea', 'PlanVisit', 'SCoverServiceArea', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('ServiceTypeCol', 'PlanVisit', 'ServiceTypeCol', MntConst.eTypeText, 2);
        this.riGrid.AddColumn('PlannedQty', 'PlanVisit', 'PlannedQty', MntConst.eTypeInteger, 5, true, '');
        this.riGrid.AddColumn('ActualQty', 'PlanVisit', 'ActualQty', MntConst.eTypeInteger, 5, true, '');
        this.riGrid.AddColumn('DespatchStsCode', 'PlanVisit', 'DespatchStsCode', MntConst.eTypeText, 4);
        this.riGrid.AddColumn('DespatchStsDesc', 'PlanVisit', 'DespatchStsDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('Select', 'PlanVisit', 'Select', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('Print', 'PlanVisit', 'Print', MntConst.eTypeImage, 1, true, '');
        this.riGrid.AddColumn('Process', 'PlanVisit', 'Process', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('ClientReferenceCol', 'PlanVisit', 'ClientReferenceCol', MntConst.eTypeText, 15);
        this.riGrid.AddColumn('ServiceVisitText', 'PlanVisit', 'ServiceVisitText', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ProductComponentCode', 'PlanVisit', 'ProductComponentCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('ProductComponentDesc', 'PlanVisit', 'ProductComponentDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('VisitNarrativeCode', 'PlanVisit', 'VisitNarrativeCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumn('ProductComponentRemoved', 'PlanVisit', 'ProductComponentRemoved', MntConst.eTypeCode, 6);
        this.riGrid.AddColumn('ProductComponentRemDesc', 'PlanVisit', 'ProductComponentRemDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PremiseLocationNumber', 'PlanVisit', 'PremiseLocationNumber', MntConst.eTypeInteger, 9);
        this.riGrid.AddColumn('PremiseLocationDesc', 'PlanVisit', 'PremiseLocationDesc', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('PlanRemQuantity', 'PlanVisit', 'PlanRemQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'PlanVisit', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumn('ServiceSpecialInstructions', 'PlanVisit', 'ServiceSpecialInstructions', MntConst.eTypeText, 40);
        this.riGrid.AddColumn('ServicePlanNumber', 'PlanVisit', 'ServicePlanNumber', MntConst.eTypeInteger, 5);

        this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('WeekNumberCol', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PlanNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Branch', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('VisitType', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ContractNumberCol', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseNumberCol', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceTypeCol', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PlannedQty', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ActualQty', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('DespatchStsCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Select', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Print', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('Process', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnScreen('PremiseAddressLine1', false);
        this.riGrid.AddColumnScreen('PremiseAddressLine2', false);
        this.riGrid.AddColumnScreen('PremiseAddressLine3', false);
        this.riGrid.AddColumnScreen('TownPostCode', true);
        this.riGrid.AddColumnScreen('Town', false);
        this.riGrid.AddColumnScreen('PremiseAddressLine5', false);
        this.riGrid.AddColumnScreen('PostCode', false);
        this.riGrid.AddColumnScreen('ContactName', false);
        this.riGrid.AddColumnScreen('ContactPosition', false);
        this.riGrid.AddColumnScreen('ContactDept', false);
        this.riGrid.AddColumnScreen('ContactTelephone', false);
        this.riGrid.AddColumnScreen('ContactMobile', false);
        this.riGrid.AddColumnScreen('ContactEmail', false);
        this.riGrid.AddColumnScreen('ContactFax', false);
        this.riGrid.AddColumnScreen('ServiceVisitText', false);
        this.riGrid.AddColumnScreen('ProductComponentCode', false);
        this.riGrid.AddColumnScreen('ProductComponentDesc', false);
        this.riGrid.AddColumnScreen('VisitNarrativeCode', false);
        this.riGrid.AddColumnScreen('ProductComponentRemoved', false);
        this.riGrid.AddColumnScreen('ProductComponentRemDesc', false);
        this.riGrid.AddColumnScreen('PremiseLocationNumber', false);
        this.riGrid.AddColumnScreen('PremiseLocationDesc', false);
        this.riGrid.AddColumnScreen('PlanRemQuantity', false);
        this.riGrid.AddColumnScreen('BranchServiceAreaSeqNo', false);
        this.riGrid.AddColumnScreen('ServiceSpecialInstructions', false);
        this.riGrid.AddColumnScreen('ServicePlanNumber', false);
        this.riGrid.AddColumnUpdateSupport('DespatchStsCode', true);
        this.riGrid.Complete();
    }

    private loadData(): void {
        if (!this.getControlValue('ServiceAreaList') && !this.getControlValue('ContractNumber')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceAreaList', true);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ContractNumber', true);
            this.riExchange.riInputElement.isError(this.uiForm, 'ServiceAreaList');
            this.riExchange.riInputElement.isError(this.uiForm, 'ContractNumber');
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceAreaList', false);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ContractNumber', false);
        }
        if (!this.getControlValue('StartDate') && !this.getControlValue('ContractNumber') && !this.getControlValue('ClientReference')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', true);
            this.riExchange.riInputElement.isError(this.uiForm, 'StartDate');
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', false);
        }
        if (!this.getControlValue('EndDate') && !this.getControlValue('ContractNumber') && !this.getControlValue('ClientReference')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', true);
            this.riExchange.riInputElement.isError(this.uiForm, 'EndDate');
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', false);
        }
        if (this.uiForm.valid) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set('StartDate', this.getControlValue('StartDate'));
            search.set('EndDate', this.getControlValue('EndDate'));
            search.set('ServiceAreaList', this.getControlValue('ServiceAreaList'));
            search.set('VisitTypeGroup', this.getControlValue('VisitTypeList'));
            search.set('ContractNumber', this.getControlValue('ContractNumber'));
            search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
            search.set('VisitStatus', this.getControlValue('VisitStatus'));
            search.set('DespatchStatus', this.getControlValue('DespatchStatus'));
            search.set('ProductCode', this.getControlValue('ProductCode'));
            search.set('ProductComponentCode', this.getControlValue('ProductComponentCode'));
            search.set('NegBranchNumber', this.getControlValue('NegBranchNumber'));
            search.set('ContractType', this.getControlValue('ContractTypeFilter'));
            search.set('ServiceType', this.getControlValue('InServiceTypeCode'));
            search.set('NameBegins', this.getControlValue('ContractNameSearch'));
            search.set('TownBegins', this.getControlValue('TownBegins'));
            search.set('PostCodeBegins', this.getControlValue('PostcodeSearch'));
            search.set('ClientRef', this.getControlValue('ClientReference'));
            search.set('TotPlannedQty', this.getControlValue('TotPlannedQty'));
            search.set('TotSelectedPlannedQty', this.getControlValue('TotSelectedPlannedQty'));
            search.set('UpdateSelect', this.isUpdateSelect.toString());
            search.set('UpdateProcess', this.isUpdateProcess.toString());
            search.set('UpdateRecord', this.vbUpdateRecord);
            if (this.riGrid.Update) {
                search.set('RowID', this.pageParams.planVisitRowID);
            }
            if (this.getControlValue('ConfApptOnly'))
                search.set('ConfApptOnly', 'Yes');
            if (this.isUpdateSelect) {
                search.set('OrigSelected', this.vbOrigSelected);
                search.set('SelectedRowID', this.getControlValue('SelectedRowID'));
            }
            search.set(this.serviceConstants.GridMode, '0');
            search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
            search.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
            search.set(this.serviceConstants.PageSize, this.pageParams.pageSize);
            search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
            search.set('riSortOrder', this.riGrid.SortOrder);
            if (!this.isReturning())
            search.set('riCacheRefresh', 'True');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isDivMain3 = false;
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceAreaList', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ContractNumber', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', false);

                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.isHidePagination = true;
                        return;
                    }
                    this.isHidePagination = false;
                    if (data.body.cells.length) {
                        this.isDivMain3 = true;
                    }
                    if (!this.riGrid.Update) {
                        this.pageParams.curPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                        this.despatchPagination.totalItems = this.totalRecords;
                    }
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    if (this.riGrid.Update) {
                        if (this.hasValue(this.getAttribute('Row'))) {
                            this.riGrid.StartRow = this.getAttribute('Row');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('PlanVisitRowID');
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = true;
                        }
                    }
                    this.riGrid.Execute(data);
                    if (data.footer.rows[0].text) {
                        let footerDetails: Array<any> = data.footer.rows[0].text.split('|');
                        this.setControlValue('SelectedRowID', footerDetails[1]);
                        this.setControlValue('TotPlannedQty', footerDetails[2]);
                        this.setControlValue('TotSelectedPlannedQty', footerDetails[3]);
                        this.selectedPlannedQty = footerDetails[3];
                        this.toSelectedPlannedQty = footerDetails[2];
                    }
                    this.riGrid.Update = false;
                    this.isUpdateSelect = false;
                    this.isUpdateProcess = false;
                    if (this.isReturning()) {
                        this.riGrid.Update = true;
                        this.isUpdateProcess = true;
                        this.loadData();
                        this.isReturningFlag = false;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceAreaList', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ContractNumber', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', false);
                    this.isHidePagination = true;
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onStartDateSelectedValue(value: any): void {
        if (value && value.value)
            this.setControlValue('StartDate', value.value);
    }

    public onEndDateSelectedValue(value: any): void {
        if (value && value.value)
            this.setControlValue('EndDate', value.value);
    }

    public onListGroupSearchDataReturn(event: any): void {
        this.isContractActarisk = false;
        this.isServiceListActarisk = true;
        this.setControlValue('ServiceAreaListCode', event.vbLGListGroupCodeFld);
        this.setControlValue('ServiceAreaList', event.vbLGListGroupDescFld);
        this.setAsterisk();
        this.serviceAreaListOnChange();
    }

    public onVisitTypeGroupSearchDataReturn(event: any): void {
        this.setControlValue('VisitTypeGroupCode', event.vbLGListGroupCodeFld);
        this.setControlValue('VisitTypeList', event.vbLGListGroupDescFld);
    }

    public onContractSearchDataReturn(event: any): void {
        this.isContractActarisk = true;
        this.isServiceListActarisk = this.getControlValue('ServiceAreaList') ? true : false;
        this.setControlValue('ContractNumber', event.ContractNumber);
        this.setControlValue('ContractName', event.ContractName);
        this.setAsterisk();
        this.ellipsis.premiseTypeSearch.inputParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.premiseTypeSearch.inputParams.ContractName = this.getControlValue('ContractName');
    }

    public onContractNumberChnage(event: any): void {
        if (event.target.value) {
            this.isContractActarisk = true;
            this.isServiceListActarisk = this.getControlValue('ServiceAreaList') ? true : false;
            this.setAsterisk();
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'GetContractName',
                ContractNumber: event.target.value
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'StartDate', false);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', false);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('ContractName', '');
                        return;
                    }
                    this.setControlValue('ContractNumber', event.target.value);
                    this.setControlValue('ContractName', data.ContractName);
                    this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'ServiceAreaList', false);
                    this.getValidationDate();
                    if (this.riExchange.riInputElement.isError(this.uiForm, 'StartDate') && !this.getControlValue('StartDate') && this.getControlValue('ContractName')) {
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'StartDate', false);
                    }
                    if (this.riExchange.riInputElement.isError(this.uiForm, 'EndDate') && !this.getControlValue('StartDate') && this.getControlValue('ContractName')) {
                        this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'EndDate', false);
                    }

                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('ContractName', '');
            this.isContractActarisk = this.getControlValue('ServiceAreaList') ? false : true;
            this.isServiceListActarisk = true;
        }
        this.ellipsis.premiseTypeSearch.inputParams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.premiseTypeSearch.inputParams.ContractName = this.getControlValue('ContractName');
    }


    public serviceAreaListOnChange(): void {
        if (this.getControlValue('ServiceAreaList')) {
            this.isContractActarisk = this.getControlValue('ContractNumber') ? true : false;
            this.isServiceListActarisk = true;
            if (this.riExchange.riInputElement.isError(this.uiForm, 'ContractNumber') && !this.getControlValue('ContractNumber')) {
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ContractNumber', false);
            }
            if (this.riExchange.riInputElement.isError(this.uiForm, 'ServiceAreaList') && !this.getControlValue('ServiceAreaList')) {
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'ServiceAreaList', false);
            }
        } else {
            this.isContractActarisk = true;
            this.isServiceListActarisk = this.getControlValue('ContractNumber') ? false : true;
        }
    }

    public serviceAreaListCodeOnChange(event: any): void {
        if (event.target.value) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'GetListGroupsList',
                ListTypeCode: '1',
                ListGroupCode: event.target.value
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('ServiceAreaList', '');
                        this.setControlValue('ServiceAreaListCode', '');
                        return;
                    }
                    this.setControlValue('ServiceAreaListCode', event.target.value);
                    this.setControlValue('ServiceAreaList', data.ListDetails);
                    this.serviceAreaListOnChange();
                    // Call BuildGrid()

                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('ServiceAreaList', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('ServiceAreaList', '');
        }
    }

    public visitTypeGroupCodeOnChange(event: any): void {
        if (event.target.value) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'GetListGroupsList',
                ListTypeCode: '2',
                ListGroupCode: event.target.value
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('VisitTypeList', '');
                        this.setControlValue('VisitTypeGroupCode', '');
                        return;
                    }
                    this.setControlValue('VisitTypeGroupCode', event.target.value);
                    this.setControlValue('VisitTypeList', data.ListDetails);
                    // Call BuildGrid()
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('VisitTypeList', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('VisitTypeList', '');
        }
    }

    public onPremiseSearchDataReturn(event: any): void {
        this.setControlValue('PremiseNumber', event.PremiseNumber);
        this.setControlValue('PremiseName', event.PremiseName);
    }

    public onProductSearchDataReturn(event: any): void {
        this.setControlValue('ProductCode', event.ProductCode);
    }

    public onProductComponentSearchDataReturn(event: any): void {
        this.setControlValue('ProductComponentCode', event.ProductCode);
    }

    public onUpdateComponentSearchDataReturn(event: any): void {
        this.setControlValue('UpdateComponentCode', event.ProductCode);
        this.setControlValue('UpdateComponentDesc', event.ProductDesc);
    }

    public onAssignServiceAreaDataReturn(event: any): void {
        this.setControlValue('AssignServiceArea', event.AssignServiceArea);
        this.setControlValue('AssignServiceEmployee', event.AssignServiceEmployee);
        this.assignServiceAreaOnChange();
    }

    public onBranchServiceAreaDataReturn(event: any): void {
        this.setControlValue('BranchServiceAreaCodeChange', event.BranchServiceAreaCodeChange);
        this.setControlValue('EmployeeSurnameChange', event.EmployeeSurnameChange);
        this.onBranchServiceAreaCodeChange();
    }

    public onBranchDataReceived(event: any): void {
        this.setControlValue('NegBranchNumber', event.BranchNumber);
        if (event.BranchNumber && event.BranchName) {
            this.pageParams.activeBranch = {
                id: event.BranchNumber,
                text: event.BranchNumber + ' - ' + event.BranchName
            };
            this.onNegBranchNumberChange();
        }
    }

    public assignServiceAreaOnChange(): void {
        if (this.getControlValue('AssignServiceArea')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'GetUpdateEmployeeSurname',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('AssignServiceArea'),
                MultiBranch: 'False'
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('AssignServiceEmployee', '');
                        this.setControlValue('AssignServiceArea', '');
                        return;
                    }
                    this.setControlValue('AssignServiceEmployee', data.EmployeeSurname);
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('AssignServiceEmployee', '');
                    this.setControlValue('AssignServiceArea', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('AssignServiceEmployee', '');
        }
    }

    public onProductCodeChnage(event: any): void {
        if (event.target.value) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'ValidProductCode',
                ProductCode: event.target.value
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('ProductCode', '');
                        return;
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('ProductCode', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onProductComponentCodeChnage(event: any): void {
        if (event.target.value) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'ValidProductCode',
                ProductCode: event.target.value
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('ProductComponentCode', '');
                        return;
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('ProductComponentCode', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onUpdateComponentCodeChange(event: any): void {
        if (event.target.value) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'ValidProductCode',
                ProductCode: event.target.value
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('UpdateComponentCode', '');
                        return;
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('UpdateComponentCode', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public refresh(): void {
        this.beforeExecute();
        this.loadData();
        this.riGrid.RefreshRequired();
    }

    public onChangeCManualDates(): void {
        this.riGrid.Clear();
        this.isHidePagination = true;
        this.riGrid.RefreshRequired();
        if (this.getControlValue('CManualDates')) {
            this.riExchange.riInputElement.Enable(this.uiForm, 'StartDate');
            this.riExchange.riInputElement.Enable(this.uiForm, 'EndDate');
            this.setControlValue('StartDate', '');
            this.setControlValue('EndDate', '');
            this.setControlValue('WeekNumber', '');
            return;
        }
        this.riExchange.riInputElement.Disable(this.uiForm, 'StartDate');
        this.riExchange.riInputElement.Disable(this.uiForm, 'EndDate');
        this.setDate();
        this.getLatestWeekNumber();
    }


    public getCurrentPage(event: any): void {
        this.pageParams.curPage = event.value;
        this.loadData();
    }

    public onBranchServiceAreaCodeChange(): void {
        if (this.getControlValue('BranchServiceAreaCodeChange')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'GetUpdateEmployeeSurname',
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCodeChange'),
                MultiBranch: 'True'
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('EmployeeSurnameChange', '');
                        this.setControlValue('BranchServiceAreaCodeChange', '');
                        return;
                    }
                    this.setControlValue('EmployeeSurnameChange', data.EmployeeSurname);
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('EmployeeSurnameChange', '');
                    this.setControlValue('BranchServiceAreaCodeChange', '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('EmployeeSurnameChange', '');
        }
    }

    public onCellClick(event: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'Process':
                this.setControlValue('CurrentContractType', this.riGrid.Details.GetAttribute('ContractNumberCol', 'additionalproperty'));
                this.navigate('DespatchGrid', AppModuleRoutes.MAINTENANCE_SERVICE + InternalMaintenanceServiceModuleRoutesConstant.ICABSSESERVICEVISITMAINTENANCE, {
                    ServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCodeCol', 'additionalproperty'),
                    ServiceVisitRowID: this.riGrid.Details.GetAttribute('ActualQty', 'additionalproperty'),
                    PlanVisitRowID: this.riGrid.Details.GetAttribute('Select', 'additionalproperty')
                });
                this.isUpdateProcess = true;
                this.riGrid.Update = true;
                this.pageParams.planVisitRowID = this.riGrid.Details.GetAttribute('Select', 'additionalproperty');
                this.pageParams.rowID = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
                this.setAttribute('PlanVisitRowID', this.pageParams.planVisitRowID);
                this.setAttribute('Row', this.pageParams.rowID);
                break;
        }
    }

    public onGridRowClick(event: any): void {
        this.setControlValue('CurrentContractType', this.riGrid.Details.GetAttribute('ContractNumberCol', 'additionalproperty'));
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumberCol':
                let destPath: string;
                let query: any = {
                    ContractRowID: this.riGrid.Details.GetAttribute('VisitType', 'additionalproperty')
                };
                switch (this.getControlValue('CurrentContractType')) {
                    case 'C':
                        destPath = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                        break;
                    case 'J':
                        destPath = ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                        break;
                    case 'P':
                        destPath = ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                        break;
                }
                this.navigate('DespatchGrid', destPath, query);
                break;
            case 'PremiseNumberCol':
                this.navigate('DespatchGrid', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    PremiseRowID: this.riGrid.Details.GetAttribute('PremiseNumberCol', 'additionalproperty'),
                    contractTypeCode: this.getControlValue('CurrentContractType')
                });
                break;
            case 'ProductCodeCol':
                this.navigate('DespatchGrid', InternalGridSearchApplicationModuleRoutes.ICABSSASERVICECOVERDISPLAYGRID, {
                    ServiceAreaListCodeServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCodeCol', 'additionalproperty'),
                    ContractNumber: this.riGrid.Details.GetValue('ContractNumberCol'),
                    PremiseNumber: this.riGrid.Details.GetValue('PremiseNumberCol'),
                    ProductCode: this.riGrid.Details.GetValue('ProductCodeCol')
                });
                break;
            case 'PlannedQty':
                this.navigate('DespatchGrid', InternalGridSearchSalesModuleRoutes.ICABSAPLANVISITGRIDYEAR, {
                    ServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCodeCol', 'additionalproperty'),
                    ContractNumber: this.riGrid.Details.GetValue('ContractNumberCol'),
                    PremiseNumber: this.riGrid.Details.GetValue('PremiseNumberCol'),
                    ProductCode: this.riGrid.Details.GetValue('ProductCodeCol')
                });
                break;
            case 'ActualQty':
                this.store.dispatch({
                    type: ContractActionTypes.SAVE_DATA,
                    payload: {
                        'CurrentContractTypeURLParameter': this.getControlValue('CurrentContractType'),
                        'ContractNumber': this.riGrid.Details.GetValue('ContractNumberCol'),
                        'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumberCol'),
                        'ProductCode': this.riGrid.Details.GetValue('ProductCodeCol'),
                        'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProductCodeCol', 'additionalproperty'),
                        'currentContractType': this.getControlValue('CurrentContractType')
                    }
                });
                this.navigate('DespatchGrid', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITSUMMARY);

                break;
            case 'Select':
                this.vbOrigSelected = 'Yes';
                if (event.srcElement.tagName === 'IMG') {
                    this.pageParams.planVisitRowID = this.riGrid.Details.GetAttribute('Select', 'additionalproperty');
                    this.pageParams.rowID = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
                    this.setAttribute('PlanVisitRowID', this.pageParams.planVisitRowID);
                    this.setAttribute('Row', this.pageParams.rowID);
                }
                this.isUpdateSelect = true;
                if (this.riGrid.Details.GetValue('Select') === 'no') {
                    this.vbOrigSelected = 'No';
                } else {
                    this.vbOrigSelected = 'Yes';
                }
                this.riGrid.Update = true;
                this.loadData();
                break;
            case 'Print':
                if (this.riGrid.Details.GetValue('Print') !== '') {
                    this.printPlanVisit();
                }
                break;
        }
    }

    public onChangeVisitDateChange(event: any): void {
        if (event && event.value) {
            this.setControlValue('ChangeVisitDate', event.value);
        }
    }

    public onUpdateDatesClick(): void {
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'ChangeVisitDate')) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '6');
            let formdata: Object = {
                ActionType: 'UpdateVisitDates',
                UpdateVisitDate: this.getControlValue('ChangeVisitDate') ? this.getControlValue('ChangeVisitDate') : '',
                SelectedRowIDs: this.getControlValue('SelectedRowID')
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
                this.inputParams.operation, search, formdata)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        return;
                    }
                    this.loadData();
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onClickUpdateVisitServiceArea(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ActionType: 'UpdateVisitServiceArea',
            BranchNumber: this.utils.getBranchCode(),
            UpdateVisitServiceArea: this.getControlValue('BranchServiceAreaCodeChange') ? this.getControlValue('BranchServiceAreaCodeChange') : '',
            SelectedRowIDs: this.getControlValue('SelectedRowID')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.loadData();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }


    public onCellBlur(data: any): void {
        this.focusElement = event.srcElement;
        let oldValue: string = this.riGrid.previousValues[16].value;
        let newValue: string = data.target.value;
        if (oldValue !== newValue) {
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '0');
            search.set('VisitDate', this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('VisitDate')) as string);
            search.set('WeekNumberCol', this.riGrid.Details.GetValue('WeekNumberCol'));
            search.set('Status', this.riGrid.Details.GetValue('Status'));
            search.set('PlanNumber', this.getControlValue('GridPageSize'));
            search.set('Branch', this.riGrid.Details.GetValue('Branch'));
            search.set('PlanVisitServiceArea', this.riGrid.Details.GetValue('PlanVisitServiceArea'));
            search.set('VisitType', this.riGrid.Details.GetValue('VisitType'));
            search.set('ContractNumberCol', this.riGrid.Details.GetValue('ContractNumberCol'));
            search.set('PremiseNumberCol', this.riGrid.Details.GetValue('PremiseNumberCol'));
            search.set('PremiseName', this.riGrid.Details.GetValue('PremiseName'));
            search.set('TownPostCode', this.riGrid.Details.GetValue('TownPostCode'));
            search.set('ProductCodeCol', this.riGrid.Details.GetValue('ProductCodeCol'));
            search.set('SCoverServiceArea', this.riGrid.Details.GetValue('SCoverServiceArea'));
            search.set('ServiceTypeCol', this.riGrid.Details.GetValue('ServiceTypeCol'));
            search.set('PlannedQty', this.riGrid.Details.GetValue('PlannedQty'));
            search.set('ActualQty', this.riGrid.Details.GetValue('ActualQty'));
            search.set('DespatchStsCodeRowID', this.riGrid.Details.GetAttribute('DespatchStsCode', 'rowID'));
            search.set('DespatchStsCode', data.target.value);
            search.set('DespatchStsDesc', this.riGrid.Details.GetValue('DespatchStsDesc'));
            search.set('ClientReferenceCol', this.riGrid.Details.GetValue('ClientReferenceCol'));
            search.set(this.serviceConstants.Action, '2');
            search.set('BranchNumber', this.utils.getBranchCode());
            search.set('StartDate', this.getControlValue('StartDate'));
            search.set('EndDate', this.getControlValue('EndDate'));
            search.set('ServiceAreaList', this.getControlValue('ServiceAreaList'));
            search.set('VisitTypeGroup', this.getControlValue('VisitTypeList'));
            search.set('ContractNumber', this.getControlValue('ContractNumber'));
            search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
            search.set('VisitStatus', this.getControlValue('VisitStatus'));
            search.set('DespatchStatus', this.getControlValue('DespatchStatus'));
            search.set('ProductCode', this.getControlValue('ProductCode'));
            search.set('ProductComponentCode', this.getControlValue('ProductComponentCode'));
            search.set('NegBranchNumber', this.getControlValue('NegBranchNumber'));
            search.set('ContractType', this.getControlValue('ContractTypeFilter'));
            search.set('ServiceType', this.getControlValue('InServiceTypeCode'));
            search.set('NameBegins', this.getControlValue('ContractNameSearch'));
            search.set('TownBegins', this.getControlValue('TownBegins'));
            search.set('PostCodeBegins', this.getControlValue('PostcodeSearch'));
            search.set('ClientRef', this.getControlValue('ClientReference'));
            search.set('TotPlannedQty', this.getControlValue('TotPlannedQty'));
            search.set('TotSelectedPlannedQty', this.getControlValue('TotSelectedPlannedQty'));
            search.set('UpdateSelect', this.isUpdateSelect.toString());
            search.set('UpdateProcess', this.isUpdateProcess.toString());
            search.set('UpdateRecord', this.vbUpdateRecord);
            if (this.riGrid.Update) {
                search.set('RowID', this.pageParams.planVisitRowID);
            }
            if (this.getControlValue('ConfApptOnly'))
                search.set('ConfApptOnly', 'Yes');
            if (this.isUpdateSelect) {
                search.set('OrigSelected', this.vbOrigSelected);
                search.set('SelectedRowID', this.getControlValue('SelectedRowID'));
                //add rowid
            }
            search.set(this.serviceConstants.GridMode, '3');
            search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);
            search.set(this.serviceConstants.PageCurrent, this.pageParams.curPage.toString());
            search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
            search.set('riSortOrder', this.riGrid.SortOrder);
            if (!this.isReturning)
            search.set('riCacheRefresh', 'True');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        let modalVO: ICabsModalVO = new ICabsModalVO(data.errorMessage, data.fullError);
                        modalVO.closeCallback = this.onErrorCloseCallback.bind(this);
                        this.modalAdvService.emitError(modalVO);
                        return;
                    }
                    this.riGrid.Mode = MntConst.eModeNormal;
                    this.loadData();
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onErrorCloseCallback(): void {
        this.riGrid.setFocusBack(this.focusElement);
    }

    public onAssignServiceClick(event: any): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ActionType: 'AssignServiceArea',
            AssignServiceArea: this.getControlValue('AssignServiceArea') ? this.getControlValue('AssignServiceArea') : '',
            SelectedRowIDs: this.getControlValue('SelectedRowID')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.loadData();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onUpdateComponentCodeClick(event: any): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        let formdata: Object = {
            ActionType: 'UpdateComponentCode',
            BranchNumber: this.utils.getBranchCode(),
            UpdateComponentCode: this.getControlValue('UpdateComponentCode') ? this.getControlValue('UpdateComponentCode') : '',
            SelectedRowIDs: this.getControlValue('SelectedRowID')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.loadData();
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onServiceTypeDataReceived(event: any): void {
        this.setControlValue('InServiceTypeCode', event.inServiceTypeCode);
        if (event.inServiceTypeCode && event.ServiceTypeDesc) {
            this.pageParams.activeServiceType = {
                id: event.inServiceTypeCode,
                text: event.inServiceTypeCode + ' - ' + event.ServiceTypeDesc
            };
        }
    }

    public onGridPageSizeChange(event: any): void {
        if (!event.target.value) {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'GridPageSize');
        }
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'GridPageSize')) {
            this.pageParams.pageSize = this.getControlValue('GridPageSize');
        }
    }

}
