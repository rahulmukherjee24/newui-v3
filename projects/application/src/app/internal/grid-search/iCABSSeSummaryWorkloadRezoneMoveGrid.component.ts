import { Component, Injector, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs/Rx';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { BranchServiceAreaSearchComponent } from './../../internal/search/iCABSBBranchServiceAreaSearch';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { AppModuleRoutes, ContractManagementModuleRoutes, InternalGridSearchServiceModuleRoutesConstant, InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';
import { ErrorConstant } from './../../../shared/constants/error.constant';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { GlobalConstant } from './../../../shared/constants/global.constant';

@Component({

    templateUrl: 'iCABSSeSummaryWorkloadRezoneMoveGrid.html',
    styles: [`
            .legend {border: 1px solid #b7bfc7 }
            `]
})

export class SummaryWorkloadRezoneMoveGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('sumWorkRezonePagination') sumWorkRezonePagination: PaginationComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private gridHandle: any;

    public branchServiceAreaSearchComponent = BranchServiceAreaSearchComponent;
    public pageId: string = '';
    public dayModeData: Array<any> = [{ 'text': 'Visits Due On', 'value': '' }];
    public inputParams: any = {
        method: 'service-planning/maintenance',
        module: 'areas',
        operation: 'Service/iCABSSeSummaryWorkloadRezoneMoveGrid'
    };
    public productServiceGroupStringeData: Array<any> = [{ 'text': '', 'value': '' }];
    public inputParamsBranch: any = {};
    public inServiceTypeCodeeData: Array<any> = [{ 'text': '', 'value': '' }];
    public contractSearchComponent = ContractSearchComponent;
    public pageCurrent: number = 1;
    public isRerouteMode: boolean = false;
    public pageTitle: string = '';
    public pageParams: any;
    public inputParamsBranchServiceAreaSearch: any = {
        parentMode: 'LookUp-ToEmp',
        BranchNumberServiceBranchNumber: '',
        BranchName: ''
    };
    public negBranchNumberSelected: Object = {
        id: '',
        text: ''
    };
    public relatedVisitseData: Array<any> = [{
        text: 'Do Not Move Related Visits',
        value: ''
    },
    {
        text: 'Move Related Visits (Same Weekday)',
        value: 'Same'
    },
    {
        text: 'Move Related Visits (Any Day)',
        value: 'Any'
    }
    ];
    public contractTypeFiltereData: any = [
        {
            text: 'All',
            value: ''
        },
        {
            text: 'Contract',
            value: 'C'
        },
        {
            text: 'Job',
            value: 'J'
        },
        {
            text: 'Product Sale',
            value: 'P'
        }
    ];

    public dropDown: any = {
        productGroupSearch: {
            inputParams: {
                params: {
                    parentMode: 'LookUp-String',
                    ProductServiceGroupString: ''
                }
            },
            active: {
                id: '',
                text: ''
            },
            isDisabled: false,
            isRequired: false
        },
        serviceTypeSearch: {
            inputParams: {
                params: {
                    parentMode: 'LookUpC'
                }
            },
            active: {
                id: '',
                text: ''
            }
        }
    };
    public legend: Array<any> = [
        { label: 'AvailAble to move', color: '#FFFFFF' },
        { label: 'Already moved', color: '#CCFFCC' }
    ];
    public controls: Array<any> = [
        { name: 'BranchServiceAreaCode', disabled: true, required: false, value: '' },
        { name: 'EmployeeSurname', disabled: true, required: false },
        { name: 'BranchServiceAreaCodeTo', disabled: false, required: false },
        { name: 'EmployeeSurnameTo', disabled: true, required: false },
        { name: 'ProductServiceGroupString', required: false },
        { name: 'ContractTypeFilter', disabled: false, required: false },
        { name: 'InServiceTypeCode', disabled: false, required: false },
        { name: 'DayMode', disabled: false, required: false, value: '' },
        { name: 'RelatedVisits', disabled: false, required: false },
        { name: 'Frequency', disabled: false, required: false },
        { name: 'StartDate', disabled: true, type: MntConst.eTypeDate },
        { name: 'EndDate', type: MntConst.eTypeDate },
        { name: 'NegBranchNumber', disabled: false, required: false },
        { name: 'SequenceNumber', disabled: false, required: false },
        { name: 'ContractNumberSearch', disabled: false, required: false, commonValidator: true },
        { name: 'GridPageSize', disabled: false, required: false },
        { name: 'TotalNoOfCalls', disabled: true, required: false },
        { name: 'TotalNoOfExchanges', disabled: true, required: false },
        { name: 'TotalWED', readonly: true, disabled: true, required: false },
        { name: 'TotalTime', readonly: true, disabled: true, required: false },
        { name: 'DisplayAverageWeight' },
        { name: 'DisplayTimes' },
        { name: 'DisplayServiceType' },
        { name: 'ErrorMessageDesc' },
        { name: 'MoveCheckAll', value: false },
        { name: 'MoveCheckAllValue', value: '' },
        { name: 'reRouteMode' },
        { name: 'BranchName' },
        { name: 'BranchNumber' }
    ];
    constructor(private injector: Injector) {
        super(injector);
        if (window.location.href.includes('destmovegrid'))
            this.pageId = PageIdentifier.ICABSSESUMMARYWORKLOADREZONEMOVEGRIDDEST;
        else
            this.pageId = PageIdentifier.ICABSSESUMMARYWORKLOADREZONEMOVEGRID;
        this.pageTitle = 'Summary Workload Static Date View';
        this.browserTitle = 'Summary Workload Rezone Move Grid';
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.gridHandle = this.utils.randomSixDigitString();
        if (this.URLParameterContains('reroute')) {
            this.isRerouteMode = true;
        }

        if (this.parentMode !== 'ToGrid') {
            this.riGrid.FunctionUpdateSupport = true;
        }
        this.riGrid.FunctionPaging = true;

        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.beforeExecute();
            this.loadData();
            this.fetchProductServiceGroup();
            if (this.getControlValue('NegBranchNumber') && this.getControlValue('BranchName')) {
                this.negBranchNumberSelected = {
                    id: this.getControlValue('NegBranchNumber'),
                    text: this.getControlValue('NegBranchNumber') + ' - ' + this.getControlValue('BranchName')
                };
            }
            if (this.getControlValue('ProductServiceGroupString')) {
                this.dropDown.productGroupSearch.active = {
                    id: this.getControlValue('ProductServiceGroupString'),
                    text: this.getControlValue('ProductServiceGroupString') + ' - ' + this.pageParams.productSearchGroupDesc
                };
            }
            if (this.getControlValue('InServiceTypeCode')) {
                this.dropDown.serviceTypeSearch.active = {
                    id: this.getControlValue('InServiceTypeCode'),
                    text: this.getControlValue('InServiceTypeCode') + ' - ' + this.pageParams.serviceTypeDesc
                };
            }
            this.getWeekDay();

        } else {
            this.pageParams.pageSize = 11;
            this.pageParams.isShowDest = false;
            this.pageParams.isShowFilter = true;
            this.pageParams.isCheckAll = false;
            this.pageParams.moveRowId = '';
            this.pageParams.planMoveRowId = '';
            this.pageParams.showDestValue = 'Show Destination Fields';
            this.pageParams.showFilterValue = 'Hide Filters';
            Observable.forkJoin(
                [this.getSysCharDtetails(), this.setPlanningDiaryOptions()]).subscribe((data) => {
                    let record = data[0]['records'];
                    if (record) {
                        this.pageParams.vEnableInstallsRemovals = record[0]['Required'];
                        this.pageParams.vEnablePostcodeDefaulting = record[1]['Required'];
                        this.pageParams.vEnableWeeklyVisitPattern = record[2]['Required'];
                        this.pageParams.vEnableWED = record[3]['Required'];
                        if (this.pageParams.vEnableWeeklyVisitPattern) {
                            this.setControlValue('RelatedVisits', 'Same');
                        } else {
                            this.setControlValue('RelatedVisits', 'Any');
                        }
                    }
                    if (data[1]) {
                        this.setControlValue('DisplayTimes', data[1]['DisplayTimes']);
                        this.setControlValue('DisplayAverageWeight', data[1]['DisplayAverageWeight']);
                        this.setControlValue('DisplayServiceType', data[1]['DisplayServiceType']);
                        this.setControlValue('ErrorMessageDesc', data[1]['ErrorMessageDesc']);
                    }
                    this.beforeExecute();
                    this.setupPage();
                });
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private beforeExecute(): void {
        this.riGrid.Clear();
        if (this.getControlValue('BranchServiceAreaCode') === null) {
            this.riGrid.AddColumn('ColBranchServiceAreaCode', 'ServiceCover', 'ColBranchServiceAreaCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumnAlign('ColBranchServiceAreaCode', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceCover', 'BranchServiceAreaSeqNo', MntConst.eTypeText, 6, this.parentMode === 'ServicePlan');
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ContractNum', 'ServiceCover', 'ContractNum', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ContractNum', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNum', 'ServiceCover', 'PremiseNum', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('PremiseNum', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PremiseName', 'ServiceCover', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Address', 'ServiceCover', 'Address', MntConst.eTypeText, 40);
        this.riGrid.AddColumnScreen('Address', false);
        this.riGrid.AddColumn('Town', 'ServiceCover', 'Town', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('Town', MntConst.eAlignmentLeft);
        if (this.pageParams.vEnablePostcodeDefaulting) {
            this.riGrid.AddColumn('Postcode', 'ServiceCover', 'Postcode', MntConst.eTypeText, 10);
            this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        }
        this.riGrid.AddColumn('ProdServGrpCode', 'ServiceCover', 'ProdServGrpCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('ProdServGrpCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProdCode', 'ServiceCover', 'ProdCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('ProdCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCover', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceQuantity', 'ServiceCover', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        if (this.getControlValue('DisplayAverageWeight').toUpperCase() === GlobalConstant.Configuration.Yes) {
            this.riGrid.AddColumn('AverageWeight', 'ServiceCover', 'AverageWeight', MntConst.eTypeDecimal2, 5);
            this.riGrid.AddColumnAlign('AverageWeight', MntConst.eAlignmentCenter);
        }
        if (this.pageParams.vEnableWED) {
            this.riGrid.AddColumn('WEDValue', 'ServiceCover', 'WEDValue', MntConst.eTypeDecimal1, 5);
            this.riGrid.AddColumnAlign('WEDValue', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumn('ServiceTypeCode', 'ServiceCover', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceTime', 'ServiceCover', 'ServiceTime', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('ServiceTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitsDueAndCompleted', 'ServiceCover', 'VisitsDueAndCompleted', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('VisitsDueAndCompleted', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('StaticRelatedVisits', 'ServiceCover', 'StaticRelatedVisits', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('StaticRelatedVisits', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NextServiceVisitDate', 'ServiceCover', 'NextServiceVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('NextServiceVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitTypeCode', 'ServiceCover', 'VisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('LastVisitDate', 'ServiceCover', 'LastVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('LastVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('LastVisitTypeCode', 'ServiceCover', 'LastVisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('LastVisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NextVisitDate', 'ServiceCover', 'NextVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('NextVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NextVisitTypeCode', 'ServiceCover', 'NextVisitTypeCode', MntConst.eTypeCode, 3);
        this.riGrid.AddColumnAlign('NextVisitTypeCode', MntConst.eAlignmentCenter);
        if (this.URLParameterContains('reroute') && this.parentMode !== 'ToGrid') {
            this.riGrid.AddColumn('PlanMove', 'ServiceCover', 'PlanMove', MntConst.eTypeCheckBox, 1, false, '');
            this.riGrid.AddColumnAlign('PlanMove', MntConst.eAlignmentCenter);
        }
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('ContractNum', true);
        this.riGrid.AddColumnOrderable('ProdCode', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        this.riGrid.AddColumnOrderable('NextServiceVisitDate', true);
        this.riGrid.AddColumnOrderable('VisitsDueAndCompleted', true);
        this.riGrid.AddColumnOrderable('PremiseName', true);
        this.riGrid.AddColumnOrderable('Town', true);
        this.riGrid.AddColumnOrderable('ServiceTypeCode', true);
        this.riGrid.AddColumnOrderable('ServiceTime', true);
        this.riGrid.AddColumnOrderable('StaticRelatedVisits', true);
        if (this.pageParams.vEnablePostcodeDefaulting) {
            this.riGrid.AddColumnOrderable('Postcode', true);
        }
        this.riGrid.Complete();
    }

    private getSysCharDtetails(): any {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableInstallsRemovals,
            this.sysCharConstants.SystemCharEnablePostcodeDefaulting,
            this.sysCharConstants.SystemCharEnableWeeklyVisitPattern,
            this.sysCharConstants.SystemCharEnableWED
        ];
        let sysCharIP = {
            module: this.inputParams.module,
            operation: this.inputParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        return this.sysCharRequest(sysCharIP);
    }

    private sysCharRequest(params: any): Observable<any> {
        let search = new QueryParams();
        search.set(this.serviceConstants.Action, params.action);
        search.set(this.serviceConstants.BusinessCode, params.businessCode);
        search.set(this.serviceConstants.CountryCode, params.countryCode);
        search.set('systemCharNumber', params.SysCharList);

        let xhrParams: any = {
            method: 'settings/data',
            module: params.module,
            operation: params.operation,
            search: search
        };

        return this.httpService.makeGetRequest(
            xhrParams.method,
            xhrParams.module,
            xhrParams.operation,
            xhrParams.search
        );

    }

    private setupPage(): void {
        this.fetchProductServiceGroup();
        this.inputParamsBranchServiceAreaSearch.BranchNumberServiceBranchNumber = this.utils.getBranchCode();
        this.inputParamsBranchServiceAreaSearch.BranchNumberServiceBranchNumber = this.utils.getBranchText();
        this.setControlValue('GridPageSize', this.pageParams.pageSize);
        switch (this.parentMode) {
            case 'ToGrid':
                this.setControlValue('StartDate', this.riExchange.getParentHTMLValue('EndDate'));
                this.riExchange.getParentHTMLValue('EndDate');
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCodeTo'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurnameTo'));
                this.riExchange.getParentHTMLValue('NegBranchNumber');
                this.riExchange.getParentHTMLValue('BranchName');
                this.riExchange.getParentHTMLValue('SequenceNumber');
                this.riExchange.getParentHTMLValue('InServiceTypeCode');
                this.riExchange.getParentHTMLValue('ContractNumberSearch');
                this.riExchange.getParentHTMLValue('Frequency');
                if (this.getControlValue('InServiceTypeCode')) {
                    this.dropDown.serviceTypeSearch.active = {
                        id: this.getControlValue('InServiceTypeCode'),
                        text: this.getControlValue('InServiceTypeCode') + ' - ' + this.pageParams.serviceTypeDesc
                    };
                }
                if (this.getControlValue('NegBranchNumber') && this.getControlValue('BranchName')) {
                    this.negBranchNumberSelected = {
                        id: this.getControlValue('NegBranchNumber'),
                        text: this.getControlValue('NegBranchNumber') + ' - ' + this.getControlValue('BranchName')
                    };
                }
                this.contractTypeFiltereData = [];
                this.getWeekDay();
                this.loadData();
                break;
            case 'SummaryWorkload':
                this.setControlValue('Frequency', this.riExchange.getParentHTMLValue('VisitFrequencyFilter'));
                this.riExchange.getParentHTMLValue('ProductServiceGroupString');
                this.riExchange.getParentHTMLValue('ContractTypeFilter');
                this.setControlValue('StartDate', this.riExchange.getParentAttributeValue('BusinessCodeSelectedDate'));
                this.setControlValue('EndDate', this.riExchange.getParentAttributeValue('BusinessCodeSelectedDate'));
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BusinessCodeSelectedServiceArea'));
                this.setControlValue('BranchServiceAreaCodeTo', this.riExchange.getParentAttributeValue('BusinessCodeSelectedServiceArea'));
                this.loadEmployeeSurnameData();
                this.loadEmployeeSurnameData('To');
                this.getWeekDay();
                this.dropDown.productGroupSearch.inputParams.ProductServiceGroupString = this.getControlValue('ProductServiceGroupString');
                if (this.isRerouteMode) {
                    this.pageParams.isCheckAll = true;
                } else {
                    this.pageParams.isCheckAll = false;
                }
                this.pageParams.productSearchGroupDesc = this.riExchange.getParentHTMLValue('productSearchGroupDesc');
                if (this.getControlValue('ProductServiceGroupString')) {
                    this.dropDown.productGroupSearch.active = {
                        id: this.getControlValue('ProductServiceGroupString'),
                        text: this.getControlValue('ProductServiceGroupString') + ' - ' + this.pageParams.productSearchGroupDesc
                    };
                }
                this.loadData();
                break;
            default:
                this.loadData();
                break;
        }

    }

    private loadEmployeeSurnameData(serviceCode: string = ''): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.ActionType, 'GetEmployeeSurname');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode' + serviceCode));
        search.set('StartDate', this.getControlValue('StartDate'));
        this.inputParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, this.inputParams.search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('EmployeeSurname' + serviceCode, '');
                    } else {
                        let EmployeeSurname = data['EmployeeSurname'];
                        this.setControlValue('EmployeeSurname' + serviceCode, EmployeeSurname);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('EmployeeSurname' + serviceCode, '');
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));

                }
            );
    }

    private getWeekDay(): void {
        let d: Date = this.utils.convertDate(this.getControlValue('StartDate'));
        let day = d.getDay();
        let selectedDate = '';
        switch (day) {
            case 0: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.sunday_visit;
                break;
            case 1: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.monday_visit;
                break;
            case 2: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.tues_visit;
                break;
            case 3: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.wed_visit;
                break;
            case 4: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.thurs_visit;
                break;
            case 5: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.fri_visit;
                break;
            case 6: selectedDate = MessageConstant.PageSpecificMessage.selectedDate.sat_visit;
                break;
        }
        this.dayModeData.push({ 'text': selectedDate, 'value': 'Weekday' });
    }

    private setPlanningDiaryOptions(): any {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.ActionType, 'GetPlanningDiaryOptions');
        this.inputParams.search = search;
        return this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, this.inputParams.search);
    }

    private loadData(): void {
        let search: QueryParams = this.setFilterValuesForGrid();
        this.inputParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, this.inputParams.search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('MoveCheckAllValue', '');
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        let additionalData: any = data.footer.rows[0].text.split('|');
                        this.setControlValue('TotalNoOfCalls', additionalData[1]);
                        this.setControlValue('TotalNoOfExchanges', additionalData[2]);
                        this.setControlValue('TotalTime', additionalData[3]);
                        this.setControlValue('TotalWED', additionalData[4]);
                        this.pageParams.moveRowId = additionalData[5];
                        this.pageParams.planMoveRowId = additionalData[5];
                        this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.sumWorkRezonePagination.totalItems = data.pageData ? data.pageData.lastPageNumber * this.pageParams.pageSize : 1;
                    }
                    if (!this.riGrid.Update) {
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                        this.pageParams.moveRowId = '';
                        this.pageParams.planMoveRowId = '';
                    }
                    this.riGrid.Execute(data);
                    this.riGrid.Update = false;
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));

                }
            );
    }

    private setFilterValuesForGrid(): QueryParams {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('StartDate', this.getControlValue('StartDate'));
        search.set('EndDate', this.getControlValue('EndDate'));
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('NegBranchNumber', this.getControlValue('NegBranchNumber'));
        search.set('ServiceTypeCode', this.getControlValue('InServiceTypeCode'));
        search.set('SequenceNumber', this.getControlValue('SequenceNumber'));
        search.set('ContractNumber', this.getControlValue('ContractNumberSearch'));
        search.set('ProductServiceGroupString', this.getControlValue('ProductServiceGroupString'));
        search.set('MoveRowid', this.pageParams.moveRowId);
        search.set('PlanMoveRowid', this.pageParams.planMoveRowId);
        search.set('Frequency', this.getControlValue('Frequency'));
        search.set('ParentMode', this.parentMode);
        search.set('DayMode', this.getControlValue('DayMode'));
        search.set('MoveCheckAllDesc', this.getControlValue('MoveCheckAllValue'));
        search.set('ContractTypeFilter', this.getControlValue('ContractTypeFilter'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set('riCacheRefresh', 'True');
        search.set('PageSize', this.getControlValue('GridPageSize'));
        search.set('PageCurrent', this.pageCurrent.toString());
        search.set('riSortOrder', this.riGrid.SortOrder);
        search.set(this.serviceConstants.GridHandle, this.gridHandle);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        if (this.isRerouteMode) {
            search.set('Reroute', 'TRUE');
        }
        if (this.riGrid.Update) {
            if (this.hasValue(this.getAttribute('Row'))) {
                this.riGrid.StartRow = this.getAttribute('Row');
                this.riGrid.StartColumn = 0;
                this.riGrid.RowID = this.getAttribute('ServiceCoverRowID');
                search.set('StaticVisitRowID', this.riGrid.Details.GetAttribute('NextServiceVisitDate', 'additionalproperty'));
            }
        }
        return search;
    }

    private setFilterValuesForMoveNext(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.ActionType, 'MoveStatic');
        search.set('MoveRowid', this.pageParams.moveRowId);
        search.set('Frequency', this.getControlValue('Frequency'));
        search.set('EndDate', this.getControlValue('EndDate'));
        search.set('RelatedVisits', this.getControlValue('RelatedVisits'));
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('BranchServiceAreaCodeTo', this.getControlValue('BranchServiceAreaCodeTo'));
        search.set(this.serviceConstants.Action, '0');
        this.inputParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, this.inputParams.search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.loadData();
                    }

                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    private setFilterValuesForMove(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.ActionType, 'Validate');
        search.set('MoveRowid', this.pageParams.moveRowId);
        search.set('Frequency', this.getControlValue('Frequency'));
        search.set('EndDate', this.getControlValue('EndDate'));
        search.set(this.serviceConstants.Action, '0');
        this.inputParams.search = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, this.inputParams.search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        if (data.ValidateError !== 'FALSE')
                            this.modalAdvService.emitMessage(new ICabsModalVO(data.ValidateReason));
                        this.setFilterValuesForMoveNext();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));

                }
            );
    }

    private fetchProductServiceGroup(): void {
        let lookupIP = [
            {
                'table': 'ProductServiceGroup',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode()

                },
                'fields': ['ProductServiceGroupCode', 'ProductServiceGroupDesc']
            },
            {
                'table': 'ServiceType',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['ServiceTypeCode', 'ServiceTypeDesc']
            }];
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            if (data.length) {
                let productServiceGroup: any = data[0];
                if (productServiceGroup) {
                    for (let i = 0; i < productServiceGroup.length; i++)
                        this.productServiceGroupStringeData.push({ 'text': productServiceGroup[i]['ProductServiceGroupCode'] + '-' + productServiceGroup[i]['ProductServiceGroupDesc'], 'value': productServiceGroup[i]['ProductServiceGroupCode'] });
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(ErrorConstant.Message.RecordNotFound));
                }
                let serviceType: any = data[1];
                if (serviceType) {
                    for (let i = 0; i < serviceType.length; i++) {
                        this.inServiceTypeCodeeData.push({ 'text': serviceType[i]['ServiceTypeCode'] + '-' + serviceType[i]['ServiceTypeDesc'], 'value': serviceType[i]['ServiceTypeCode'] });
                    }
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(ErrorConstant.Message.RecordNotFound));
                }
            }
        }).catch(e => {
            this.modalAdvService.emitError(new ICabsModalVO(ErrorConstant.Message.RecordNotFound));
        });
    }

    private URLParameterContains(parameter: string): boolean {
        let url = window.location.href;
        if (url.indexOf('/' + parameter + '?') !== -1)
            return true;
        return false;
    }

    private checkDestination(): void {
        if (!this.getControlValue('BranchServiceAreaCodeTo')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'BranchServiceAreaCodeTo', true);
        }
        if (!this.getControlValue('EndDate')) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EndDate', true);
        }
    }


    public onBodyDblClick(event: any): void {
        let contactTypeCode = this.riGrid.Details.GetAttribute('Town', 'AdditionalProperty');
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNum':
                let destPath: string;
                let query: any = {
                    ContractRowID: this.riGrid.Details.GetAttribute('ContractNum', 'additionalproperty')
                };
                switch (contactTypeCode) {
                    case 'C':
                        destPath = ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                        break;
                    case 'J':
                        destPath = ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                        break;
                    case 'P':
                        destPath = ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                        break;
                }
                this.navigate('ServicePlanning', destPath, query);
                break;
            case 'PremiseNum':
                this.navigate('ServicePlanning', AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE_SUB,
                    {
                        'PremiseRowID': this.riGrid.Details.GetAttribute('PremiseNum', 'additionalproperty'),
                        'ContractTypeCode': contactTypeCode
                    });
                break;
            case 'ProdCode':
                if (contactTypeCode === 'P') {
                    this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE,
                        {
                            'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty'), 'currentContractType': contactTypeCode
                        });
                } else {
                    switch (contactTypeCode) {
                        case 'C':
                            destPath = AppModuleRoutes.SERVICECOVERMAINTENANCE + ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCECONTRACT_SUB;
                            break;
                        case 'J':
                            destPath = AppModuleRoutes.SERVICECOVERMAINTENANCE + ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCEJOB_SUB;
                            break;
                    }
                    this.navigate('ServicePlanning', destPath,
                        {
                            'ServiceCoverRowID': this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty'), 'currentContractType': contactTypeCode
                        });
                }
                break;

            case 'StaticRelatedVisits':
                // iCABSARelatedVisitGrid
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
                break;
        }
    }

    public onMoveCheckAllClick(event: any): void {
        this.pageParams.moveRowId = '';
        this.pageParams.planMoveRowId = '';
        if (this.getControlValue('MoveCheckAll')) {
            this.setControlValue('MoveCheckAllValue', 'Check');
        } else {
            this.setControlValue('MoveCheckAllValue', 'Uncheck');
        }
        this.loadData();
    }

    public onCmdToDateClick(): void {
        this.checkDestination();
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'BranchServiceAreaCodeTo') && !this.riExchange.riInputElement.isError(this.uiForm, 'EndDate')) {
            this.isRerouteMode = false;
            this.navigate('ToGrid', AppModuleRoutes.GRID_SERVICE + InternalGridSearchServiceModuleRoutesConstant.ICABSSESUMMARYWORKLOADREZONEMOVEGRID.URL_3, {
                EndDate: this.getControlValue('EndDate'),
                BranchServiceAreaCodeTo: this.getControlValue('BranchServiceAreaCodeTo'),
                EmployeeSurnameTo: this.getControlValue('EmployeeSurnameTo'),
                NegBranchNumber: this.getControlValue('NegBranchNumber'),
                Sequencenumber: this.getControlValue('Sequencenumber'),
                InServiceTypeCode: this.getControlValue('InServiceTypeCode'),
                ContractNumberSearch: this.getControlValue('ContractNumberSearch'),
                Frequency: this.getControlValue('Frequency')
            });
        }
    }

    public onClickHeader(event: any): void {
        this.riGrid.RefreshRequired();
        this.loadData();
    }

    public onBranchDataReceived(obj: any): void {
        this.setControlValue('NegBranchNumber', obj.BranchNumber);
        this.setControlValue('BranchName', obj.BranchName);
        this.setControlValue('BranchNumber', obj.BranchNumber);
    }

    public getCurrentPage(event: any): void {
        this.pageCurrent = event.value;
        this.loadData();

    }

    public onDataReceived(data: any): void {
        this.setControlValue('ContractNumberSearch', data.ContractNumber);
    }

    public refresh(event?: any): void {
        this.riGrid.RefreshRequired();
        this.loadData();
    }

    public onDestinationClick(event: any): void {
        if (this.pageParams.isShowDest) {
            this.pageParams.showDestValue = 'Show Destination Fields';
            this.pageParams.isShowDest = false;
        } else {
            this.pageParams.showDestValue = 'Hide Destination Fields';
            this.pageParams.isShowDest = true;
        }
    }

    public endDateSelectedValue(value: any): void {
        if (value && value.value) {
            this.setControlValue('EndDate', value.value);
        }
    }

    public onProductSearchReceived(event: any): void {
        this.setControlValue('ProductServiceGroupString', event.ProductServiceGroupString);
        this.pageParams.productSearchGroupDesc = event.ProductServiceGroupDesc;
    }

    public onCmdPlanMoveClick(event: any): void {
        this.checkDestination();
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'BranchServiceAreaCodeTo') && !this.riExchange.riInputElement.isError(this.uiForm, 'EndDate')) {
            this.setFilterValuesForMove();
        }
    }

    public onCmdShowFilterClick(event?: any): void {
        if (this.pageParams.isShowFilter) {
            this.pageParams.showFilterValue = 'Show Filters';
            this.pageParams.isShowFilter = false;
        } else {
            this.pageParams.showFilterValue = 'Hide Filters';
            this.pageParams.isShowFilter = true;
        }
    }

    public onCellClick(data: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'PlanMove':
                this.setRowAttribute(data);
                if (data.target.checked) {
                    if (this.pageParams.moveRowId === '') {
                        this.pageParams.moveRowId = this.pageParams.moveRowId + this.riGrid.Details.GetAttribute('PlanMove', 'additionalproperty');
                    } else {
                        this.pageParams.moveRowId = this.pageParams.moveRowId + ';' + this.riGrid.Details.GetAttribute('PlanMove', 'additionalproperty');
                    }
                } else {
                    if (this.pageParams.moveRowId.includes(this.riGrid.Details.GetAttribute('PlanMove', 'additionalproperty'))) {
                        this.pageParams.moveRowId = this.pageParams.moveRowId.replace(this.riGrid.Details.GetAttribute('PlanMove', 'additionalproperty'), '');
                        this.pageParams.moveRowId = this.pageParams.moveRowId.replace(';;', ';');
                        if (this.pageParams.moveRowId.charAt(0) === ';')
                            this.pageParams.moveRowId = this.pageParams.moveRowId.substr(1, this.pageParams.moveRowId.length - 1);
                        if (this.pageParams.moveRowId.charAt(this.pageParams.moveRowId.length - 1) === ';')
                            this.pageParams.moveRowId = this.pageParams.moveRowId.substr(0, this.pageParams.moveRowId.length - 1);
                    }
                }
                this.pageParams.planMoveRowId = this.pageParams.moveRowId;
                this.riGrid.Update = true;
                this.loadData();
                break;
        }
    }

    public setRowAttribute(event: any): void {
        this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNum', 'additionalproperty'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNum', 'additionalproperty'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty'));
        this.setAttribute('NextServiceVisitDate', this.riGrid.Details.GetAttribute('ProdCode', 'additionalproperty'));
        this.setAttribute('Row', event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex);
    }

    public onBranchServiceArea(obj: any): void {
        this.setControlValue('BranchServiceAreaCodeTo', obj.BranchServiceAreaCodeTo);
        this.setControlValue('EmployeeSurnameTo', obj.EmployeeSurnameTo);
    }

    public onChangeBranchServiceAreaCodeTo(event: any): void {
        if (event.target.value)
            this.loadEmployeeSurnameData('To');
        else
            this.setControlValue('EmployeeSurnameTo', '');
    }

    public onServiCeTypeReceived(data: any): void {
        this.setControlValue('InServiceTypeCode', data.inServiceTypeCode);
        this.pageParams.serviceTypeDesc = data.ServiceTypeDesc;
    }

}
