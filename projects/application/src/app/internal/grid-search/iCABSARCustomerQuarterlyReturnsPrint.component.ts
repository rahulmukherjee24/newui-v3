import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { BranchServiceAreaSearchComponent } from './../../internal/search/iCABSBBranchServiceAreaSearch';
import { IExportOptions } from '@app/base/ExportConfig';

@Component({
    templateUrl: 'iCABSARCustomerQuarterlyReturnsPrint.html'
})

export class CustomerQuarterlyReturnsPrintComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('formBranchServiceAreaCode') formBranchServiceAreaCode;
    @ViewChild('formDateFrom') formDateFrom;
    @ViewChild('formDateTo') formDateTo;
    private queryParams: any = {
        operation: 'ApplicationReport/iCABSARCustomerQuarterlyReturnsPrint',
        module: 'waste',
        method: 'service-delivery/batch'
    };
    private itemsPerPage: number = 10;
    private clientSideValidation: boolean;
    private search: any = this.getURLSearchParamObject();
    private lookupParams: any = this.getURLSearchParamObject();
    private riGridHandle: string;
    private pageCurrent: number = 1;
    public pageId: string = '';
    public isDatePickerRequired: boolean = true;
    public totalRecords: number = 0;
    public pageSize: number = 10;
    public isPrintButton: boolean = true;
    public regulatoryArr: Array<any> = [];
    public showInformation: boolean;
    public showInformationData: any;
    public controls = [
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeTextFree },
        { name: 'PlanVisitsDue', type: MntConst.eTypeCheckBox },
        { name: 'RegulatoryAuthorityNumber', type: MntConst.eTypeInteger },
        { name: 'DateFrom', type: MntConst.eTypeDate, required: true },
        { name: 'DateTo', type: MntConst.eTypeDate, required: true }
    ];

    public ellipsis = {
        serviceAreaEllipsis: {
            showCloseButton: true,
            showHeader: true,
            childparams: {
                parentMode: 'LookUp-PlanVisit',
                BranchNumberServiceBranchNumber: this.utils.getBranchCode(),
                ServiceBranchNumber: this.utils.getBranchCode(),
                BranchName: this.utils.getBranchTextOnly(this.utils.getBranchCode())
            },
            component: BranchServiceAreaSearchComponent
        }
    };
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public expCfg: IExportOptions = {};

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARCUSTOMERQUARTERLYRETURNSPRINT;
        this.pageTitle = this.browserTitle = 'Customer Quarterly Returns';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.buildDropDown();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * buildDropDown(): This function gets called during ON INIT , forms the dynamic dropdown on page load
     */

    private buildDropDown(): void {
        let ContractTypeData: Array<any> = [{
            'table': 'RegulatoryAuthority',
            'query': {
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': [
                'RegulatoryAuthorityNumber', 'RegulatoryAuthorityName'
            ]
        }];

        this.ajaxSource.next(this.ajaxconstant.START);
        this.lookUpRecord(ContractTypeData, 100).subscribe(
            (e) => {
                let datafetched = e['results'];
                if (datafetched[0]) {
                    for (let i = 0; i < datafetched[0].length; i++) {
                        this.utils.sortByKey(datafetched[0], 'RegulatoryAuthorityName', false);
                        this.regulatoryArr.push({ 'text': datafetched[0][i].RegulatoryAuthorityName, 'value': datafetched[0][i].RegulatoryAuthorityNumber });
                    }
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.windowOnLoad();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));

            });
    }

    private lookUpRecord(data: any, maxresults: any): any {
        this.lookupParams.set(this.serviceConstants.Action, '0');
        if (maxresults) {
            this.lookupParams.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        if (data)
            return this.httpService.lookUpRequest(this.lookupParams, data);
    }

    private setButtonDisabled(buttonState: boolean): void {
        this.isPrintButton = buttonState;
    }
    private setDates(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'DefaultDates';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        if (data.DateFrom || data.DateTo) {
                            this.setControlValue('DateFrom', data.DateFrom);
                            this.setControlValue('DateTo', data.DateTo);
                        }
                    }
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.riGrid.AddColumn('BranchServiceAreaCode', 'Letter', 'BranchServiceAreaCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumn('EmployeeSurname', 'Letter', 'EmployeeSurname', MntConst.eTypeCode, 30);
        }
        this.riGrid.AddColumn('ContractNumber', 'Letter', 'ContractNumber', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'Letter', 'PremiseNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'Letter', 'PremiseName', MntConst.eTypeTextFree, 30);
        this.riGrid.AddColumn('LetterDate', 'Letter', 'LetterDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('LetterDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ExpiryDate', 'Letter', 'ExpiryDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ExpiryDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Printed', 'Letter', 'Printed', MntConst.eTypeImage, 1, true);
        this.riGrid.Complete();
        this.expCfg.dateColumns = this.riGrid.getColumnIndexListFromFull(['LetterDate','ExpiryDate']);
        this.riGridBeforeExecute();
    }

    /**
     * riGridBeforeExecute(): This function gets called during Grid Execution
     */


    private riGridBeforeExecute(): void {
        if (this.getClientSideValidation()) {
            this.riGrid.RefreshRequired();
            this.search.set(this.serviceConstants.Action, '2');
            this.search.set('BranchNumber', this.utils.getBranchCode());
            this.search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            this.search.set('DateFrom', this.getControlValue('DateFrom'));
            this.search.set('DateTo', this.getControlValue('DateTo'));
            this.search.set('PlanVisitsDue', this.getControlValue('PlanVisitsDue'));
            this.search.set('RegulatoryAuthorityNumber', this.getControlValue('RegulatoryAuthorityNumber'));
            this.search.set(this.serviceConstants.PageSize, (this.itemsPerPage).toString());
            this.search.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
            this.search.set(this.serviceConstants.GridMode, '0');
            this.search.set(this.serviceConstants.GridHandle, this.riGridHandle);
            this.search.set('riSortOrder', 'Descending');
            this.search.set('HeaderClickedColumn', '');
            this.search.set('riCacheRefresh', 'True');
            this.queryParams.search = this.search;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
                this.queryParams.operation, this.queryParams.search)
                .subscribe(
                    (data) => {
                        if (data) {
                            try {
                                this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                                this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                                if (data['hasError']) {
                                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                                } else {
                                    this.riGrid.Execute(data);
                                }

                            } catch (e) {
                                this.logger.log('Problem in grid load', e);
                            }
                        }
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    },
                    error => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.totalRecords = 1;
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
    }

    private selectedRowFocus(rsrcElement: any): void {
        this.pageParams.SelectedRowid = this.riGrid.Details.GetAttribute('ContractNumber', 'rowID');
        this.pageParams.SelectedReportNumber = this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty');
    }

    private showReport(lSingleReport: boolean): void {
        let strURL: any;
        let cPlanVisits: any;
        let cFromBranchServiceAreaCode: any;
        let cToBranchServiceAreaCode: any;
        let cFromRegulatoryAuthorityNumber: any;
        let cToRegulatoryAuthorityNumber: any;
        if (!this.getControlValue('BranchServiceAreaCode')) {
            cFromBranchServiceAreaCode = '!';
            cToBranchServiceAreaCode = 'ZZZZZZZZZ';
        }
        else {
            cFromBranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            cToBranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        }
        cPlanVisits = 'Y';
        if ((this.getControlValue('RegulatoryAuthorityNumber') === 0)) {
            cFromRegulatoryAuthorityNumber = '0';
            cToRegulatoryAuthorityNumber = '999999';
        }
        else {
            cFromRegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');
            cToRegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');
        }
        strURL = 'iCABSCustomerQuarterlyReturnsURL.p';
        let searchParams: any = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        let postParams: any = {};
        postParams.BranchNumber = this.utils.getBranchCode();
        postParams.FromBranchServiceAreaCode = cFromBranchServiceAreaCode;
        postParams.ToBranchServiceAreaCode = cToBranchServiceAreaCode;
        postParams.PlanVisitsDue = cPlanVisits;
        postParams.FromDate = this.getControlValue('DateFrom');
        postParams.ToDate = this.getControlValue('DateTo');
        postParams.FromRegulatoryAuthorityNumber = cFromRegulatoryAuthorityNumber;
        postParams.ToRegulatoryAuthorityNumber = cToRegulatoryAuthorityNumber;

        if (lSingleReport) {
            postParams.FromReportNumber = this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty');
            postParams.ToReportNumber = this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty');
        }
        else {
            postParams.FromReportNumber = '0';
            postParams.ToReportNumber = '99999999';
        }

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, postParams).subscribe(
            (data) => {
                if ((data['errorMessage'])) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage']));

                } else if (data.url) {
                    window.open(data.url, '_blank');
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private getClientSideValidation(): boolean {
        this.clientSideValidation = true;
        this.utils.setDatePickerCustomError('DateFrom', false);
        this.utils.setDatePickerCustomError('DateTo', false);
        if (this.riExchange.riInputElement.isError(this.uiForm, 'DateFrom') || this.riExchange.riInputElement.isError(this.uiForm, 'DateTo')) {
            this.clientSideValidation = false;
        }
        if ((this.getControlValue('DateTo')) < (this.getControlValue('DateFrom')) || !this.getControlValue('DateFrom') || !this.getControlValue('DateTo')) {
            this.clientSideValidation = false;
            this.utils.setDatePickerCustomError('DateFrom', true);
            this.utils.setDatePickerCustomError('DateTo', true);
            this.totalRecords = 0;
        }
        return this.clientSideValidation;
    }

    private windowOnLoad(): void {
        this.riGridHandle = this.utils.randomSixDigitString();
        this.setControlValue('RegulatoryAuthorityNumber', '0');
        this.setControlValue('BranchServiceAreaCode', '');
        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.setControlValue('BranchName', this.utils.getBranchTextOnly(this.utils.getBranchCode()));
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.disableControl('BranchName', true);
        this.disableControl('BranchNumber', true);
        this.disableControl('RegulatoryAuthorityName', true);
        this.disableControl('EmployeeSurname', true);
        this.setButtonDisabled(true);
        this.setDates();
        this.setControlValue('PlanVisitsDue', true);
        this.formBranchServiceAreaCode.nativeElement.focus();
    }

    public gridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.pageCurrent = currentPage.value;
        this.riGridBeforeExecute();
    }

    public onClickPrint(): void {
        if (this.getClientSideValidation()) {
            this.showReport(false);
        }

    }

    public onChangeRAN(): void {
        this.setButtonDisabled(true);
    }
    public onChangePlanVisit(): void {
        this.gridRefresh();
    }

    /**
     * onClickGenerate() : This function gets called when Generate button is clicked to generate batch process
     */
    public onClickGenerate(): void {
        let cPlanVisits: any;
        if (this.getClientSideValidation()) {
            cPlanVisits = true;
            if (!this.riExchange.riInputElement.isError(this.uiForm, 'DateFrom') && !this.riExchange.riInputElement.isError(this.uiForm, 'DateTo')) {
                let postSearchParams = this.getURLSearchParamObject();
                postSearchParams.set(this.serviceConstants.Action, '6');
                let postParams: any = {};
                postParams.Function = 'GenerateBatch';
                postParams.BranchNumber = this.utils.getBranchCode();
                postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
                postParams.DateFrom = this.getControlValue('DateFrom');
                postParams.DateTo = this.getControlValue('DateTo');
                postParams.PlanVisitsDue = cPlanVisits;
                postParams.RegulatoryAuthorityNumber = this.getControlValue('RegulatoryAuthorityNumber');

                this.ajaxSource.next(this.ajaxconstant.START);
                this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
                    .subscribe(
                        (data) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            if ((data['hasError'])) {
                                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            } else {
                                if (data.ReturnHTML) {
                                    this.showInformation = true;
                                    this.showInformationData = data.ReturnHTML;
                                }
                            }
                        },
                        (error) => {
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        });

            }

        }


    }

    public fromDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('DateFrom', value.value);
        }
        this.setButtonDisabled(true);
    }

    public toDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('DateTo', value.value);
        }
        this.setButtonDisabled(true);
    }

    public riGridBodyOnDBLClick(event: any): void {
        this.selectedRowFocus(event.srcElement);
        if (event.srcElement.parentElement.parentElement.getAttribute('name') === 'Printed') {
            this.showReport(true);
        }
    }

    public riGridBodyOnClick(event: any): void {
        this.selectedRowFocus(event.srcElement);
    }

    /**
     * onChangeBranchServiceAreaCode(): This function gets called when there is any change in BranchServiceAreaCode fields
     */

    public onChangeBranchServiceAreaCode(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let postSearchParams = this.getURLSearchParamObject();
            postSearchParams.set(this.serviceConstants.Action, '6');
            let postParams: any = {};
            postParams.Function = 'GetEmployeeSurname';
            postParams.BranchNumber = this.utils.getBranchCode();
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if ((data['hasError'])) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('BranchServiceAreaCode', '');
                            this.setControlValue('EmployeeSurname', '');
                        } else {
                            if (data.EmployeeSurname) {
                                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                            }
                        }
                    },
                    (error) => {
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    });
        }
        else {
            this.setControlValue('BranchServiceAreaCode', '');
            this.setControlValue('EmployeeSurname', '');
        }
        this.setButtonDisabled(true);
    }

    public onDataReceivedServiceSearch(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        }
        this.setButtonDisabled(true);
    }

    public riGridAfterExecute(): void {
        this.setButtonDisabled(false);

    }

    public riGridBodyOnKeyDown(event: any): void {
        let cellindex = event.srcElement.parentElement.parentElement.cellIndex;
        let rowIndex = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
        switch (event.keyCode) {
            case 13:
                break;
            case 38:
                event.returnValue = 0;
                if ((rowIndex > 0) && (rowIndex < 10)) {
                    this.selectedRowFocus(this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex].children[0].children[0]);
                }
                break;
            case 40:
            case 9:
                event.returnValue = 0;
                if ((rowIndex >= 0) && (rowIndex < 9)) {
                    this.selectedRowFocus(this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex].children[0].children[0]);
                }
                break;
            default:
                break;
        }
    }


}

