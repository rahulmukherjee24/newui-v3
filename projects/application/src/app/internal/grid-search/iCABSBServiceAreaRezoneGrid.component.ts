import { Component, OnInit, Injector, ViewChild, OnDestroy, EventEmitter } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { BranchServiceAreaSearchComponent } from '../search/iCABSBBranchServiceAreaSearch';
import { ProductServiceGroupSearchComponent } from '../search/iCABSBProductServiceGroupSearch.component';
import { PostCodeSearchComponent } from '../search/iCABSBPostcodeSearch.component';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { InternalMaintenanceModuleRoutes } from './../../base/PageRoutes';


@Component({
    templateUrl: 'iCABSBServiceAreaRezoneGrid.html'
})

export class ServiceAreaRezoneGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('productServiceGroupDropdown') productServiceGroupDropdown: ProductServiceGroupSearchComponent;

    private rezoneErrorMessage: any;
    private isRezoneUpdate: boolean = true;
    private isTechRetentionUpdate: boolean;
    private queryParams: any = {
        operation: 'Business/iCABSBServiceAreaRezoneGrid',
        module: 'service',
        method: 'service-delivery/maintenance'
    };
    public pageId: string = '';
    public controls: any = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractTypeCode' },
        { name: 'BranchServiceAreaCodeTo', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDescTo', type: MntConst.eTypeText, disabled: true },
        { name: 'PortfolioStatusType' },
        { name: 'ProductServiceGroupCode', type: MntConst.eTypeCode },
        { name: 'ProductServiceGroupDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'Postcode', type: MntConst.eTypeCode },
        { name: 'State', type: MntConst.eTypeText },
        { name: 'Town', type: MntConst.eTypeText },
        { name: 'UndoRowids', type: MntConst.eTypeText },
        { name: 'PostcodeDefaultingEnabled', type: MntConst.eTypeText }
    ];
    public gridConfig: any = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };
    public isUndoDisplayed: boolean = false;
    public portfolioStatus: any = {
        isAllSelected: false,
        isCurrentSelected: true,
        isNonCurrentSelected: false
    };
    public contractType: any = {
        isAllSelected: true,
        isContractSelected: false,
        isJobSelcted: false,
        isProductSelected: false
    };
    public ellipsisConfig: any = {
        branchServiceArea: {
            isDisabled: false,
            parentMode: 'LookUp',
            isShowAddNew: false,
            component: BranchServiceAreaSearchComponent
        },
        branchServiceAreaTo: {
            isDisabled: false,
            parentMode: 'LookUp-To',
            isShowAddNew: false,
            component: BranchServiceAreaSearchComponent
        },
        postCodeSearch: {
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp',
                PostCode: '',
                State: '',
                Town: ''
            },
            component: PostCodeSearchComponent
        }
    };
    public dropdownConfig: any = {
        productServiceGroupSearch: {
            isDisabled: false,
            isRequired: true,
            params: {
                parentMode: 'LookUp',
                ProductServiceGroupString: '',
                SearchValue3: '',
                ProductServiceGroupCode: ''
            },
            active: {
                id: '',
                text: ''
            }
        }
    };
    public setFocusBranchServiceAreaCode = new EventEmitter<boolean>();

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBSERVICEAREAREZONEGRID;
        this.pageTitle = this.browserTitle = 'Service Area Postcode Rezoning';
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (this.isReturning()) {
            if (this.getControlValue('ProductServiceGroupCode')) {
                this.dropdownConfig.productServiceGroupSearch.active = {
                    id: this.getControlValue('ProductServiceGroupCode'),
                    text: this.getControlValue('ProductServiceGroupDesc')
                };
            }
            let prevPortfolio: string = this.getControlValue('PortfolioStatusType');
            if (prevPortfolio) {
                switch (prevPortfolio) {
                    case 'All':
                        this.portfolioStatus = {
                            isAllSelected: true
                        };
                        break;
                    case 'Current':
                        this.portfolioStatus = {
                            isCurrentSelected: true
                        };
                        break;
                    case 'NonCurrent':
                        this.portfolioStatus = {
                            isNonCurrentSelected: true
                        };
                        break;
                }
            }
            let prevContract: string = this.getControlValue('ContractTypeCode');
            if (prevContract) {
                switch (prevContract) {
                    case 'All':
                        this.contractType = {
                            isAllSelected: true
                        };
                        break;
                    case 'C':
                        this.contractType = {
                            isContractSelected: true
                        };
                        break;
                    case 'J':
                        this.contractType = {
                            isJobSelected: true
                        };
                        break;
                    case 'P':
                        this.contractType = {
                            isProductSelected: true
                        };
                        break;
                }
            }
            this.buildGrid();
        } else {
            this.onWindowLoad();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        setTimeout(() => {
            this.setFocusBranchServiceAreaCode.emit(true);
        }, 0);
        this.setControlValue('ContractTypeCode', 'All');
        this.setControlValue('PortfolioStatusType', 'Current');
        //check if postcode defaulting is enabled for this business and branch
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'PostcodeDefaultingEnabled';
        formData['BranchNumber'] = this.utils.getBranchCode();
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('PostcodeDefaultingEnabled', e.PostcodeDefaultingEnabled);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.buildGrid();
    }

    private rezoneFocus(data: any): void {
        this.setAttribute('Postcode', this.riGrid.Details.GetValue('Postcode'));
        this.setAttribute('State', this.riGrid.Details.GetValue('State'));
        this.setAttribute('Town', this.riGrid.Details.GetValue('Town'));
        this.setAttribute('ProductServiceGroupCode', this.riGrid.Details.GetValue('ProductServiceGroupCode'));
        this.setAttribute('ProductServiceGroupDesc', this.riGrid.Details.GetValue('ProductServiceGroupDesc'));
        this.setAttribute('BranchServiceAreaCode', this.riGrid.Details.GetValue('BranchServiceAreaCode'));
        this.setAttribute('BranchServiceAreaDesc', this.riGrid.Details.GetValue('BranchServiceAreaDesc'));
        this.setAttribute('OldDetailRowID', this.riGrid.Details.GetAttribute('Rezoned', 'rowID'));
        this.setAttribute('Row', data.parentElement.parentElement.parentElement.sectionRowIndex);
    }

    private rezone(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'RezonePostcodeProduct';
        formData['BranchNumber'] = this.utils.getBranchCode();
        if (this.riGrid.Details.GetAttribute('Rezoned', 'text') === 'Yes') {
            formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCode');
        } else {
            formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCodeTo');
        }
        formData['Postcode'] = this.riGrid.Details.GetValue('Postcode');
        formData['Town'] = this.riGrid.Details.GetValue('Town');
        formData['State'] = this.riGrid.Details.GetValue('State');
        formData['ProductServiceGroupCode'] = this.riGrid.Details.GetValue('ProductServiceGroupCode');
        formData['BranchServiceAreaCode'] = this.riGrid.Details.GetValue('BranchServiceAreaCode');
        if (this.isRezoneUpdate) {
            if (this.isTechRetentionUpdate) {
                formData['UpdateTechRetentionInd'] = 'True';
            }
            formData['AllowUndo'] = 'True';
        } else {
            formData['CheckPremiseTechRetentionInd'] = 'True';
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    if (e.MessageDesc) {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.confirmPremiseTechnicianRetention, null, this.promptConfirm.bind(this), this.promptCancel.bind(this)));
                    }
                    this.isRezoneUpdate = true;
                    this.rezoneAgain();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private rezoneAgain(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'RezonePostcodeProduct';
        formData['BranchNumber'] = this.utils.getBranchCode();
        if (this.riGrid.Details.GetAttribute('Rezoned', 'text') === 'Yes') {
            formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCode');
        } else {
            formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCodeTo');
        }
        formData['Postcode'] = this.riGrid.Details.GetValue('Postcode');
        formData['Town'] = this.riGrid.Details.GetValue('Town');
        formData['State'] = this.riGrid.Details.GetValue('State');
        formData['ProductServiceGroupCode'] = this.riGrid.Details.GetValue('ProductServiceGroupCode');
        formData['BranchServiceAreaCode'] = this.riGrid.Details.GetValue('BranchServiceAreaCode');
        if (this.isRezoneUpdate) {
            if (this.isTechRetentionUpdate) {
                formData['UpdateTechRetentionInd'] = 'True';
            }
            formData['AllowUndo'] = 'True';
        } else {
            formData['CheckPremiseTechRetentionInd'] = 'True';
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    if (!this.getControlValue('UndoRowids')) {
                        this.setControlValue('UndoRowids', e.UndoRowids);
                    } else {
                        this.setControlValue('UndoRowids', this.getControlValue('UndoRowids') + e.UndoRowids);
                    }
                    this.onRiGridRefresh();
                    this.isUndoDisplayed = true;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private promptConfirm(): void {
        this.isTechRetentionUpdate = true;
    }

    private promptCancel(): void {
        this.isTechRetentionUpdate = false;
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('Postcode', 'Rezone', 'Postcode', MntConst.eTypeCode, 12, true);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('State', 'Rezone', 'State', MntConst.eTypeText, 15, true);
        this.riGrid.AddColumnAlign('State', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Town', 'Rezone', 'Town', MntConst.eTypeText, 13, true);
        this.riGrid.AddColumnAlign('Town', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductServiceGroupCode', 'Rezone', 'ProductServiceGroupCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProductServiceGroupCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProductServiceGroupDesc', 'Rezone', 'ProductServiceGroupDesc', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('ProductServiceGroupDesc', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('NumOfPremises', 'Rezone', 'NumOfPremises', MntConst.eTypeInteger, 8);
        this.riGrid.AddColumnAlign('NumOfPremises', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PortfolioValue', 'Rezone', 'PortfolioValue', MntConst.eTypeCurrency, 10);

        this.riGrid.AddColumn('BranchServiceAreaCode', 'Rezone', 'BranchServiceAreaCode', MntConst.eTypeCode, 8);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('BranchServiceAreaDesc', 'Rezone', 'BranchServiceAreaDesc', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('BranchServiceAreaDesc', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('EmployeeCode', 'Rezone', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeSurname', 'Rezone', 'EmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeSurname', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Rezoned', 'Rezone', 'Rezoned', MntConst.eTypeImage, 1, false, 'Click here to rezone');
        this.riGrid.AddColumnAlign('Rezoned', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumnOrderable('Town', true);
        this.riGrid.AddColumnOrderable('State', true);
        this.riGrid.AddColumnOrderable('ProductServiceGroupCode', true);

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set('BranchNumber', this.utils.getBranchCode());
        gridSearch.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridSearch.set('BranchServiceAreaCodeTo', this.getControlValue('BranchServiceAreaCodeTo'));
        gridSearch.set('ContractTypeCode', this.getControlValue('ContractTypeCode'));
        gridSearch.set('PortfolioStatusType', this.getControlValue('PortfolioStatusType'));
        gridSearch.set('Postcode', this.getControlValue('Postcode'));
        gridSearch.set('Town', this.getControlValue('Town'));
        gridSearch.set('State', this.getControlValue('State'));
        gridSearch.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroupCode'));

        gridSearch.set(this.serviceConstants.GridMode, '0');
        gridSearch.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridSearch.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
        gridSearch.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
        gridSearch.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridSearch.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.riGridPagination.currentPage = this.gridConfig.currentPage = e.pageData ? e.pageData.pageNumber : 1;
                    this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                    this.riGrid.Execute(e);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public onRiGridBodyDblClick(event: any): void {
        if (this.riGrid.CurrentColumnName === 'Rezoned') {
            this.rezoneFocus(event.srcElement);
            if (this.getControlValue('PostcodeDefaultingEnabled') === 'Y') {
                this.navigate('Rezone', InternalMaintenanceModuleRoutes.ICABSBBRANCHSERVICEAREAGROUPDETAILMAINTENANCE);
            } else {
                this.isRezoneUpdate = false;
                this.rezone();
            }
        }
    }

    public onBranchServiceAreaCodeChange(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'BranchServiceAreaCode', false);
        this.uiForm.controls['BranchServiceAreaCode'].markAsPristine();
        this.populateDescriptions();
    }

    public onBranchServiceAreaCodeToChange(): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'BranchServiceAreaCodeTo', false);
        this.uiForm.controls['BranchServiceAreaCodeTo'].markAsPristine();
        this.populateDescriptions();
    }

    public populateDescriptions(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'ReturnServiceAreaDescs';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCodeTo');
        formData['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('BranchServiceAreaDesc', e.BranchServiceAreaDesc);
                    this.setControlValue('BranchServiceAreaDescTo', e.BranchServiceAreaDescTo);
                    this.setControlValue('ProductServiceGroupDesc', e.ProductServiceGroupDesc);
                    this.isUndoDisplayed = false;
                    this.setControlValue('UndoRowids', '');
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onBranchServiceAreaDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaCode', false);
            this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'BranchServiceAreaCode');
        }
    }

    public onBranchServiceAreaToDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCodeTo', data.BranchServiceAreaCodeTo);
            this.setControlValue('BranchServiceAreaDescTo', data.BranchServiceAreaDescTo);
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaCodeTo', false);
            this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'BranchServiceAreaCodeTo');
        }
    }

    public setProductServiceGroup(data: any): void {
        if (data.ProductServiceGroupCode) {
            this.setControlValue('ProductServiceGroupCode', data.ProductServiceGroupCode);
        }
        if (data.ProductServiceGroupDesc) {
            this.setControlValue('ProductServiceGroupDesc', data.ProductServiceGroupDesc);
        }
    }

    public setEllipsisData(data: any): void {
        if (data) {
            if (data.Postcode) {
                this.setControlValue('Postcode', data.Postcode);
                this.ellipsisConfig.postCodeSearch.childConfigParams.PostCode = data.Postcode;
            } else {
                this.ellipsisConfig.postCodeSearch.childConfigParams.PostCode = '';
                this.setControlValue('Postcode', '');
            }
            if (data.State) {
                this.setControlValue('State', data.State);
                this.ellipsisConfig.postCodeSearch.childConfigParams.State = data.State;
            } else {
                this.ellipsisConfig.postCodeSearch.childConfigParams.State = '';
                this.setControlValue('State', '');
            }
            if (data.Town) {
                this.setControlValue('Town', data.Town);
                this.ellipsisConfig.postCodeSearch.childConfigParams.Town = data.Town;
            } else {
                this.ellipsisConfig.postCodeSearch.childConfigParams.Town = '';
                this.setControlValue('Town', '');
            }
        }
    }

    public onRezoneClick(): void {
        let isErrorFlag: boolean = false;
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaCode', true);
            this.riExchange.riInputElement.markAsError(this.uiForm, 'BranchServiceAreaCode');
            isErrorFlag = true;
        }
        if (!this.getControlValue('BranchServiceAreaCodeTo')) {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaCodeTo', true);
            this.riExchange.riInputElement.markAsError(this.uiForm, 'BranchServiceAreaCodeTo');
            isErrorFlag = true;
        }
        if (!isErrorFlag) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let formData: any = {};
            formData[this.serviceConstants.Function] = 'RezoneGroup';
            formData['BranchNumber'] = this.utils.getBranchCode();
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
            formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCodeTo');
            formData['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
            formData['Postcode'] = this.getControlValue('Postcode');
            formData['State'] = this.getControlValue('State');
            formData['Town'] = this.getControlValue('Town');
            formData['ContractTypeCode'] = this.getControlValue('ContractTypeCode');
            formData['PortfolioStatusType'] = this.getControlValue('PortfolioStatusType');
            formData['AllowUndo'] = 'True';
            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.setControlValue('UndoRowids', e.UndoRowids);
                        if (e.MessageDesc) {
                            let modalVO: ICabsModalVO = new ICabsModalVO(e.MessageDesc);
                            modalVO.closeCallback = this.onRiGridRefresh.bind(this);
                            this.modalAdvService.emitMessage(modalVO);
                        }
                        this.isUndoDisplayed = true;
                        this.onRiGridRefresh();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    public onUndoRezoneClick(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'UndoRezone';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCodeTo');
        formData['BranchServiceAreaCodeTo'] = this.getControlValue('BranchServiceAreaCode');
        formData['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
        formData['Postcode'] = this.getControlValue('Postcode');
        formData['State'] = this.getControlValue('State');
        formData['Town'] = this.getControlValue('Town');
        formData['ContractTypeCode'] = this.getControlValue('ContractTypeCode');
        formData['PortfolioStatusType'] = this.getControlValue('PortfolioStatusType');
        formData['AllowUndo'] = 'True';
        formData['UndoRowids'] = this.getControlValue('UndoRowids');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    if (e.MessageDesc) {
                        let modalVO: ICabsModalVO = new ICabsModalVO(e.MessageDesc);
                        modalVO.closeCallback = this.onRiGridRefresh.bind(this);
                        this.modalAdvService.emitMessage(modalVO);
                    }
                    this.isUndoDisplayed = false;
                    this.onRiGridRefresh();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public setPortfolioStatus(data: any): void {
        if (data) {
            this.setControlValue('PortfolioStatusType', data.target.value);
        }
    }

    public setContractType(data: any): void {
        if (data) {
            this.setControlValue('ContractTypeCode', data.target.value);
        }
    }

    public onPostCodeChange(data: any): void {
        if (data) {
            this.ellipsisConfig.postCodeSearch.childConfigParams.PostCode = data.target.value;
        }
    }

    public onStateChange(data: any): void {
        if (data) {
            this.ellipsisConfig.postCodeSearch.childConfigParams.State = data.target.value;
        }
    }

    public onTownChange(data: any): void {
        if (data) {
            this.ellipsisConfig.postCodeSearch.childConfigParams.Town = data.target.value;
        }
    }
}
