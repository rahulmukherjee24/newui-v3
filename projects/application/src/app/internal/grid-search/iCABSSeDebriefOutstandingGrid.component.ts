import { Component, OnInit, AfterViewInit, OnDestroy, Injector, ViewChild, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { AppModuleRoutes, ServiceDeliveryModuleRoutes, InternalGridSearchServiceModuleRoutes } from './../../base/PageRoutes';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { EmployeeSearchComponent } from './../../internal/search/iCABSBEmployeeSearch';

@Component({
    templateUrl: 'iCABSSeDebriefOutstandingGrid.html'
})

export class DebriefOutstandingGridComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private queryParams: any = {
        operation: 'Service/iCABSSeDebriefOutstandingGrid',
        module: 'sam',
        method: 'service-delivery/grid'
    };

    public pageId: string = '';
    private gridCacheRefresh: boolean = true;
    public controls: Array<any> = [
        { name: 'SupervisorEmployeeCode', type: MntConst.eTypeCode },
        { name: 'SupervisorEmployeeName', type: MntConst.eTypeText, disabled: true }
    ];
    public ellipsisParams: any = {
        supervisor: {
            isShowCloseButton: true,
            isShowHeader: true,
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp-Supervisor'
            },
            contentComponent: EmployeeSearchComponent
        },
        common: {
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            }
        }
    };
    public setFocusSupervisorEmployeeCode: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEDEBRIEFOUTSTANDINGGRID;

        this.pageTitle = this.browserTitle = 'Service Action Monitor - Summary of Outstanding Tasks';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngAfterViewInit(): void {
        this.setFocusSupervisorEmployeeCode.emit(true);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    // Initializes data into different controls on page load
    private onWindowLoad(): void {
        this.buildGrid();
        if (this.isReturning()) {
            this.gridCacheRefresh = false;
            this.populateGrid();
        } else {
            this.pageParams.gridConfig = {
                pageSize: 10,
                currentPage: 1,
                totalRecords: 1,
                gridHandle : this.utils.randomSixDigitString()
            };
        }
    }

    // Builds the structure of the grid
    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('SupervisorEmployeeCode', 'Grid', 'SupervisorEmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('SupervisorEmployeeCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('SupervisorEmployeeName', 'Grid', 'SupervisorEmployeeName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('SupervisorEmployeeName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ActionsNumber', 'Grid', 'ActionsNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('ActionsNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitRejectionsNumber', 'Grid', 'VisitRejectionsNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('VisitRejectionsNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('OpenPlansPassedEndDateNumber', 'Grid', 'OpenPlansPassedEndDateNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('OpenPlansPassedEndDateNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('AlertsNumber', 'Grid', 'AlertsNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('AlertsNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NoServiceVisitsNumber', 'Grid', 'NoServiceVisitsNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('NoServiceVisitsNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('NoProofVisitsNumber', 'Grid', 'NoProofVisitsNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('NoProofVisitsNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseVisitsToDoNumber', 'Grid', 'PremiseVisitsToDoNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('PremiseVisitsToDoNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseVisitsEstimatedToDoNumber', 'Grid', 'PremiseVisitsEstimatedToDoNumber', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('PremiseVisitsEstimatedToDoNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('SupervisorEmployeeCode', true);

        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('SupervisorEmployeeCode', (this.getControlValue('SupervisorEmployeeCode').toString().trim().length === 0 ? '' : this.getControlValue('SupervisorEmployeeCode')));

        // set grid building parameters
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.pageParams.gridConfig.gridHandle);
        search.set(this.serviceConstants.PageSize, this.pageParams.gridConfig.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams.gridConfig.currentPage.toString());
        search.set(this.serviceConstants.GridCacheRefresh, this.gridCacheRefresh);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                    this.gridCacheRefresh = false;
                    this.ref.detectChanges();
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    // Populate SupervisorEmployeeName
    private populateDescriptions(): void {
        let formData: any = {};
        let search: QueryParams = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.Function] = 'GetDescriptions';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue('SupervisorEmployeeName', data.SupervisorEmployeeName);

                        if (String(data.ErrorMessageDesc).trim().length > 0) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.ErrorMessageDesc));
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    // Updates the pagelevel attributes on grid row activity
    private onGridFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('SupervisorEmployeeCode', this.riGrid.Details.GetValue('SupervisorEmployeeCode'));
        this.setAttribute('SupervisorEmployeeName', this.riGrid.Details.GetValue('SupervisorEmployeeName'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(event: any): void {
        if (this.pageParams.gridConfig.currentPage <= 0) {
            this.pageParams.gridConfig.currentPage = 1;
        }
        if (event) {
            this.riGrid.HeaderClickedColumn = '';
        }
        this.populateGrid();
    }

    // Callback to retrieve the current page on user clicks
    public getCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh(null);
    }

    // Populate data from ellipsis
    public onEllipsisDataReceived(type: string, data: any): void {
        switch (type) {
            case 'Supervisor':
                this.setControlValue('SupervisorEmployeeCode', data['SupervisorEmployeeCode'] || '');
                this.setControlValue('SupervisorEmployeeName', data['SupervisorSurname'] || '');
                break;
            default:
        }
    }

    // SupervisorEmployeeCode OnChange event
    public onSupervisorEmployeeCodeChange(): void {
        this.populateDescriptions();
    }

    // Grid keydown on "Up Arror", "Down Arrow" & "Tab"
    public onRiGridBodyKeyDown(event: any): void {
        switch (event.keyCode) {
            // Up Arror
            case 38:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.previousSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                        if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                            this.onGridFocus(event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                        }
                    }
                }
                break;
            // Down Arror Or Tab
            case 40:
            case 9:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.nextSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                        if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                            this.onGridFocus(event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                        }
                    }
                }
                break;
        }
    }

    public onTBodyGridDblClick(event: any): void {
        if (event.srcElement.parentElement.parentElement.style.cursor !== '') {
            this.onGridFocus(event.srcElement);
            switch (event.srcElement.parentElement.parentElement.getAttribute('name')) {
                case 'SupervisorEmployeeCode':
                    this.navigate('DebriefOutstanding', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSSEDEBRIEFBRANCHGRID);
                    break;
                case 'ActionsNumber':
                    this.navigate('DebriefOutstanding', InternalGridSearchServiceModuleRoutes.ICABSSEDEBRIEFACTIONSGRID);
                    break;
            }
        }
    }

    // Handles grid sort functionality
    public onRiGridSort(event: any): void {
        this.onRiGridRefresh(null);
    }
}
