import { Component, Injector, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { InternalMaintenanceSalesModuleRoutes } from './../../base/PageRoutes';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSSSOServiceCoverGrid.html'
})

export class ServiceCoverComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private queryParams: Object = {
        operation: 'Sales/iCABSSSOServiceCoverGrid',
        module: 'advantage',
        method: 'prospect-to-contract/maintenance'
    };
    public controls: Array<Object> = [
        { name: 'dlContractRef', disabled: true, type: MntConst.eTypeCode },
        { name: 'dlPremiseRef', disabled: true, type: MntConst.eTypeCode },
        { name: 'menu', value: 'Options' },
        { name: 'dlBatchRef' },
        { name: 'Misc' },
        { name: 'SubSystem' },
        { name: 'UpdateableInd' },
        { name: 'AutoCloseWindow' },
        { name: 'PNOLSetupChargeRequired' },
        { name: 'ContractTypeCode' },
        { name: 'dlPremiseROWID' }
    ];
    public gridParams: Object = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };
    public pageId: string = '';
    public isHidePagination: boolean = true;

    constructor(public injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSSOSERVICECOVERGRID;
        this.browserTitle = this.pageTitle = 'Advantage Service Covers';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.buildGrid();
            this.setControlValue('menu', 'Options');
        }
        else
            this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.riGrid.FunctionPaging = true;
        this.setControlValue('dlBatchRef', this.riExchange.getParentHTMLValue('dlBatchRef'));
        this.setControlValue('dlContractRef', this.riExchange.getParentHTMLValue('dlContractRef'));
        this.setControlValue('dlPremiseRef', this.riExchange.getParentAttributeValue('dlPremiseRef'));
        this.setControlValue('ContractTypeCode', this.riExchange.getParentHTMLValue('ContractTypeCode'));
        this.setControlValue('SubSystem', this.riExchange.getParentHTMLValue('SubSystem'));
        this.getUpdateable();
    }

    private getUpdateable(): any {

        let search: QueryParams = this.getURLSearchParamObject(), postParams: Object = {};

        postParams['Function'] = 'Updateable';
        search.set(this.serviceConstants.Action, '6');
        search.set('dlBatchRef', this.getControlValue('dlBatchRef'));
        search.set('dlContractRef', this.getControlValue('dlContractRef'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search, postParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data['UpdateableInd'] === 'yes')
                        this.setControlValue('UpdateableInd', true);
                    else
                        this.setControlValue('UpdateableInd', false);
                    if (this.getControlValue('dlPremiseRef') !== 'All' && this.getControlValue('UpdateableInd'))
                        this.pageParams.isOptions = true;
                    else
                        this.pageParams.isOptions = false;
                    if (this.getControlValue('dlPremiseROWID'))
                        this.attributes.dlPremiseROWID = this.riExchange.getParentHTMLValue('dlPremiseROWID');
                    this.buildGrid();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('PremiseNumber', 'SOServiceCover', 'PremiseNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('PremiseName', 'SOServiceCover', 'PremiseName', MntConst.eTypeTextFree, 20);
        this.riGrid.AddColumn('PremiseAddressLine1', 'SOServiceCover', 'PremiseAddressLine1', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('PremisePostCode', 'SOServiceCover', 'PremisePostCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumn('ServiceCoverNumber', 'SOServiceCover', 'ServiceCoverNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('SCContractTypeCode', 'SOServiceCover', 'SCContractTypeCode', MntConst.eTypeCode, 1);
        this.riGrid.AddColumn('dlStatusCode', 'SOServiceCover', 'dlStatusCode', MntConst.eTypeCode, 1);
        this.riGrid.AddColumn('ServiceCommenceDate', 'SOServiceCover', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('ProductCode', 'SOServiceCover', 'ProductCode', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceCoverNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('SCContractTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('dlStatusCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremiseAddressLine1', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('PremisePostCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('PremiseNumber', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumnOrderable('PremisePostCode', true);
        this.riGrid.Complete();
        this.loadGridData();
    }

    private loadGridData(): void {
        let search: QueryParams = this.getURLSearchParamObject(), sortOrder: string = 'Descending';

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage']);
        search.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent']);
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        search.set(this.serviceConstants.GridSortOrder, sortOrder);
        search.set('dlBatchRef', this.getControlValue('dlBatchRef'));
        search.set('dlContractRef', this.getControlValue('dlContractRef'));
        search.set('dlPremiseRef', this.getControlValue('dlPremiseRef'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * this.gridParams['itemsPerPage'] : 1;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.RefreshRequired();
                    this.riGrid.Execute(data);
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                        this.isHidePagination = false;
                    else
                        this.isHidePagination = true;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private serviceCoverFocus(rsrcElement: any): void {
        let oTR: any;
        oTR = rsrcElement.parentElement.parentElement.parentElement.children[4].children[0].children[0];
        this.attributes.dlPremiseROWID = this.riGrid.Details.GetAttribute('PremiseNumber', 'rowid');
        this.attributes.dlServiceCoverRowID = this.riGrid.Details.GetAttribute('ServiceCoverNumber', 'rowid');
        this.attributes.dlServiceCoverRef = this.riGrid.Details.GetAttribute('ServiceCoverNumber', 'additionalproperty');
        this.attributes.ProductCode = this.riGrid.Details.GetValue('ProductCode');
        this.attributes.ProductDesc = this.riGrid.Details.GetAttribute('ProductCode', 'additionalproperty');
        this.attributes.ServiceCommenceDate = this.riGrid.Details.GetValue('ServiceCommenceDate');
        this.setControlValue('ContractTypeCode', this.riGrid.Details.GetValue('SCContractTypeCode'));
        oTR.focus();
        oTR.select();
    }

    public onGridBodyClick(data: any): void {
        if (this.riGrid.CurrentColumnName === 'PremiseNumber' || this.riGrid.CurrentColumnName === 'ServiceCoverNumber')
            this.serviceCoverFocus(data.srcElement);
    }

    public onGridDblClick(data: any): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'PremiseNumber':
                this.serviceCoverFocus(data.srcElement);
                this.navigate('SOPremise', InternalMaintenanceSalesModuleRoutes.ICABSSDLPREMISEMAINTENANCE,
                    {
                        'dlPremiseROWID': this.attributes.dlPremiseROWID,
                        'dlBatchRef': this.getControlValue('dlBatchRef'),
                        'dlContractRef': this.getControlValue('dlContractRef'),
                        'dlPremiseRef': this.getControlValue('dlPremiseRef')
                    });
                break;
            case 'ServiceCoverNumber':
                this.serviceCoverFocus(data.srcElement);
                this.navigate('ServiceCoverUpdate', InternalMaintenanceSalesModuleRoutes.ICABSSDLSERVICECOVERMAINTENANCE,
                    {
                        dlServiceCoverRowID: this.attributes.dlServiceCoverRowID,
                        ContractTypeCode: this.getControlValue('ContractTypeCode'),
                        dlPremiseRef: this.getControlValue('dlPremiseRef'),
                        SubSystem: this.getControlValue('SubSystem')
                    });
                break;
            default:
                break;
        }
    }

    public riGridBodyOnKeyDown(event: any): void {
        switch (event.keyCode) {
            case 38:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.previousSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.serviceCoverFocus(event.srcElement.parentElement.parentElement.parentElement.previousSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }
                }
                break;
            case 40:
            case 9:
                event.returnValue = 0;
                if (event.srcElement.parentElement.parentElement.parentElement.nextSibling) {
                    if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children) {
                        if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex]) {
                            if (event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]) {
                                this.serviceCoverFocus(event.srcElement.parentElement.parentElement.parentElement.nextSibling.children[event.srcElement.parentElement.parentElement.cellIndex].children[0].children[0]);
                            }
                        }
                    }
                }
                break;
        }
    }

    public selectedOption(event: string): void {
        this.setControlValue('menu', event);
        if (event)
            this.navigate('ServiceCoverAdd', InternalMaintenanceSalesModuleRoutes.ICABSSDLSERVICECOVERMAINTENANCE);
    }

    public getCurrentPage(currentPage: any): void {
        this.gridParams['pageCurrent'] = currentPage.value;
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public gridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }
}
