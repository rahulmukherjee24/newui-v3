import { Injectable } from '@angular/core';

import { GlobalizeService } from './../../../shared/services/globalize.service';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from './../../../shared/services/utility';

@Injectable()
export class ServiceVisitPlanningGridHelper {
    /*public static readonly PLAN_VISIT_STATUS_DESC: number = 1;
    public static readonly SERVICE_PLAN: string = 'ServicePlan';
    public static readonly SERVICE_PLANNING: string = 'ServicePlanning';
    public static readonly PRODUCTIVITY_REVIEW: string = 'ProductivityReview';

    public gLanguageCode: any;*/

    constructor(private utils: Utils, private riExchange: RiExchange, private globalize: GlobalizeService) {
        //this.gLanguageCode = this.riExchange.LanguageCode();
    }

    public init(pageParams: any): void {
        //show/hide
        pageParams.isTrAnnualTime = true;

        pageParams.isBlnChangesMade = false;
        pageParams.isVbSaveAbandonClose = false;
        pageParams.vbNoAnnualTimeValidation = '';
    }

    public buildYearOptions(): Array<Object> {
        let selectedYearList: Array<Object> = new Array<Object>(), year: number;
        year = this.utils.year(new Date()) - 5;
        for (let index: number = 1; index <= 7; index++) {
            year = year + 1;
            selectedYearList.push({ value: year, text: year.toString() });
        }
        return selectedYearList;
    }
}
