import { Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { InternalGridSearchServiceModuleRoutes } from './../../base/PageRoutes';
import { BranchServiceAreaSearchComponent } from './../../internal/search/iCABSBBranchServiceAreaSearch';
import { ContractSearchComponent } from './../../internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from './../../internal/search/iCABSAPremiseSearch';
import { ProductSearchGridComponent } from './../../internal/search/iCABSBProductSearch';
import { ServiceCoverSearchComponent } from './../../internal/search/iCABSAServiceCoverSearch';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { ContractActionTypes } from '../../actions/contract';

@Component({
    templateUrl: 'iCABSSeServiceVisitEntitlementEntryGrid.html'
})

export class ServiceVisitEntitlementEntryGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('formContractNumber') public formContractNumber;
    @ViewChild('ProductEllipsis') ProductEllipsis: EllipsisComponent;
    @ViewChild('PremiseEllipsis') PremiseEllipsis: EllipsisComponent;
    private pageCurrent: number = 1;
    private search = this.getURLSearchParamObject();
    private queryParams: any = {
        operation: 'Service/iCABSSeServiceVisitEntitlementEntryGrid',
        module: 'service',
        method: 'service-delivery/maintenance'
    };
    public pageId: string = '';
    public isServiceDate: boolean;
    public isDeliveryNoteNumber: boolean;
    public itemsPerPage: number = 10;
    public pageSize = 10;
    public totalRecords: number = 1;
    public controls = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'ShowType' },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'ServiceDate', type: MntConst.eTypeDate, commonValidator: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'BranchServiceAreaSeqNo', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, commonValidator: true },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText, disabled: true, commonValidator: true },
        { name: 'DeliveryNoteNumber', type: MntConst.eTypeInteger, commonValidator: true },
        { name: 'BranchNumber', type: MntConst.eTypeInteger, disabled: true, commonValidator: true },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true, commonValidator: true }
    ];
    public ellipsis = {
        searchValueEllipsis: {
            showCloseButton: true,
            showHeader: true,
            childparams: {
                'parentMode': 'LookUp',
                'showAddNew': false,
                'showAddNewDisplay': false
            },
            component: BranchServiceAreaSearchComponent
        },
        contractEllipsis: {
            showCloseButton: true,
            showHeader: true,
            childparams: {
                parentMode: 'LookUp-All',
                currentContractType: this.riExchange.getCurrentContractType()

            },
            component: ContractSearchComponent
        },
        premise: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp',
                'currentContractTypeURLParameter': this.pageParams.CurrentContractTypeURLParameter,
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false,
                'currentContractType': this.riExchange.getCurrentContractType()
            },
            component: PremiseSearchComponent
        },
        product: {
            disabled: false,
            showHeader: true,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': '',
                'ContractNumber': '',
                'ContractName': '',
                'PremiseNumber': '',
                'PremiseName': '',
                'currentContractTypeURLParameter': this.pageParams.CurrentContractTypeURLParameter,
                'currentContractType': this.riExchange.getCurrentContractType()
            },
            component: null
        }

    };
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITENTITLEMENTENTRYGRID;
        this.browserTitle = this.pageTitle = 'Entitlement Service Visit Entry';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.riGrid.PageSize = 50;
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchName', this.utils.getBranchText(this.utils.getBranchCode()));
        this.setControlValue('BranchServiceAreaSeqNo', '0');
        this.setControlValue('ShowType', 'All');

        this.isServiceDate = this.isDeliveryNoteNumber = false;

        this.formContractNumber.nativeElement.focus();
        if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            this.ellipsis.product.childConfigParams['parentMode'] = 'LookUp';
            this.ellipsis.product.component = ServiceCoverSearchComponent;
            this.ProductEllipsis.contentComponent = ServiceCoverSearchComponent;
            this.ProductEllipsis.updateComponent();
        }
        else {
            this.ellipsis.product.childConfigParams['parentMode'] = 'LookUp-Entitlement';
            this.ellipsis.product.component = ProductSearchGridComponent;
            this.ProductEllipsis.contentComponent = ProductSearchGridComponent;
            this.ProductEllipsis.updateComponent();
        }
    }

    private buildGrid(): void {

        this.riGrid.Clear();

        this.riGrid.AddColumn('ContractNumber', 'ServiceVisit', 'ContractNumber', MntConst.eTypeCode, 11, true);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'ServiceVisit', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ProductCode', 'ServiceVisit', 'ProductCode', MntConst.eTypeCode, 8, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('BranchServiceAreaCode', 'ServiceVisit', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceVisit', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'ServiceVisit', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EntitlementAnnualQuantity', 'ServiceCover1', 'EntitlementAnnualQuantity', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('EntitlementAnnualQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EntitlementYTDQuantity', 'ServiceCover1', 'EntitlementYTDQuantity', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('EntitlementYTDQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ExceededByQuantity', 'ServiceCover1', 'ExceededByQuantity', MntConst.eTypeInteger, 3);
        this.riGrid.AddColumnAlign('ExceededByQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('UsagePercentage', 'ServiceCover1', 'UsagePercentage', MntConst.eTypeDecimal1, 5);
        this.riGrid.AddColumnAlign('UsagePercentage', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EntitlementPricePerUnit', 'ServiceCover1', 'EntitlementPricePerUnit', MntConst.eTypeCurrency, 10);
        this.riGrid.AddColumnAlign('EntitlementPricePerUnit', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('ViewServiceVisit', 'ServiceVisit', 'ViewServiceVisit', MntConst.eTypeImage, 1, true);
        this.riGrid.AddColumnAlign('ViewServiceVisit', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('AddServiceVisit', 'ServiceVisit', 'AddServiceVisit', MntConst.eTypeImage, 1, true);
        this.riGrid.AddColumnAlign('AddServiceVisit', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('PremiseNumber', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);

        this.riGrid.Complete();
        this.riGridBeforeExecute();

    }

    private riGridBeforeExecute(): void {
        this.riGrid.RefreshRequired();
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('ProductCode', this.getControlValue('ProductCode'));
        this.search.set('ServiceCoverRowID', this.getAttribute('ProductCodeServiceCoverRowID'));
        this.search.set('BranchNumber', this.getControlValue('BranchNumber'));
        this.search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.search.set('BranchServiceAreaSeqNo', this.getControlValue('BranchServiceAreaSeqNo'));
        this.search.set('ShowType', this.getControlValue('ShowType'));
        this.search.set(this.serviceConstants.PageSize, (this.itemsPerPage).toString());
        this.search.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        this.search.set('riCacheRefresh', 'True');
        this.queryParams.search = this.search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, this.queryParams.search)
            .subscribe(
                (data) => {
                    if (data) {
                        try {
                            this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                            if (data['hasError']) {
                                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            } else {
                                this.riGrid.Execute(data);
                            }

                        } catch (e) {
                            this.logger.log('Problem in grid load', e);
                        }
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                error => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.totalRecords = 1;
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private selectedRowFocus(rsrcElement: any): void {
        let oTR = rsrcElement.parentElement.parentElement.parentElement;
        this.setAttribute('ContractNumberContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'rowID'));
        this.setAttribute('ContractNumberPremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'rowID'));
        this.setAttribute('ContractNumberServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'rowID'));

        this.setAttribute('BranchServiceAreaCodeRow', oTR.sectionRowIndex);
        this.setAttribute('BranchServiceAreaCodeContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'rowID'));
        this.setAttribute('BranchServiceAreaCodePremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'rowID'));
        this.setAttribute('BranchServiceAreaCodeServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'rowID'));
        this.setAttribute('BranchServiceAreaCodeContractNumber', this.riGrid.Details.GetValue('ContractNumber'));
        this.setAttribute('BranchServiceAreaCodeContractName', this.riGrid.Details.GetAttribute('ContractNumber', 'additionalproperty'));
        this.setAttribute('BranchServiceAreaCodePremiseNumber', this.riGrid.Details.GetValue('PremiseNumber'));
        this.setAttribute('BranchServiceAreaCodePremiseName', this.riGrid.Details.GetAttribute('PremiseNumber', 'additionalproperty'));
        this.setAttribute('BranchServiceAreaCodeProductCode', this.riGrid.Details.GetValue('ProductCode'));
        this.setAttribute('BranchServiceAreaCodeProductDesc', this.riGrid.Details.GetAttribute('ProductCode', 'additionalproperty'));

        this.setAttribute('grdServiceVisitServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'rowID'));
        this.setAttribute('grdServiceVisitRow', oTR.sectionRowIndex);
        switch (this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'additionalproperty')) {
            case 'C':
                this.pageParams.currentContractTypeURLParameter = '';
                break;
            case 'J':
                this.pageParams.currentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.pageParams.currentContractTypeURLParameter = '<product>';
                break;
            default:
                break;
        }

    }

    public getCurrentPage(currentPage: any): void {
        this.pageCurrent = currentPage.value;
        this.riGridBeforeExecute();
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public riGridBodyOnDBLClick(event: any): void {
        this.selectedRowFocus(event.srcElement);
        switch (event.srcElement.parentElement.parentElement.getAttribute('name')) {

            case 'ContractNumber':
                let objNavigation: any = {
                    ContractNumber: this.riGrid.Details.GetValue('ConNumber'),
                    CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                    parentMode: 'ServiceVisitEntryGrid',
                    ContractRowID: this.getAttribute('ContractNumberContractRowID')
                };
                switch (this.pageParams.currentContractTypeURLParameter) {
                    case '':
                        this.navigate(objNavigation.parentMode, this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, objNavigation);
                        break;
                    case '<job>':
                        this.navigate(objNavigation.parentMode, this.ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE, objNavigation);
                        break;
                    case '<product>':
                        this.navigate(objNavigation.parentMode, this.ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE, objNavigation);
                        break;
                    default:
                        break;
                }
                break;
            case 'PremiseNumber':
                this.navigate('ServiceVisitEntryGrid', this.ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                    PremiseRowID: this.getAttribute('ContractNumberPremiseRowID'),
                    ContractTypeCode: this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'additionalproperty'),
                    PremiseNumber: this.riGrid.Details.GetValue('PremiseNumber')
                });
                break;

            case 'ProductCode':
                this.navigate('ServiceVisitEntryGrid', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                    currentContractType: this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'additionalproperty'),
                    ServiceCoverRowID: this.getAttribute('ContractNumberServiceCoverRowID')
                });
                break;

            case 'ViewServiceVisit':
                this.store.dispatch({
                    type: ContractActionTypes.SAVE_DATA,
                    payload: {
                        CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                        currentContractType: this.riGrid.Details.GetAttribute('BranchServiceAreaCode', 'additionalproperty'),
                        ServiceCoverRowID: this.getAttribute('ContractNumberServiceCoverRowID'),
                        PremiseNumber: this.riGrid.Details.GetValue('PremiseNumber'),
                        ProductCode: this.riGrid.Details.GetValue('ProductCode'),
                        ProductDesc: this.riGrid.Details.GetAttribute('ProductCode', 'additionalproperty'),
                        ContractNumber: this.riGrid.Details.GetValue('ContractNumber'),
                        ServiceVisitAnnivDate: this.riGrid.Details.GetValue('ServiceVisitAnnivDate')
                    }
                });
                this.navigate('ServiceVisitEntryGrid', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITSUMMARY);
                break;

            case 'AddServiceVisit':
                // iCABSSeServiceVisitEntitlementMaintenance
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
                break;


        }
    }

    public riGridBodyOnKeyDown(event: any): void {
        let cellindex = event.srcElement.parentElement.parentElement.cellIndex;
        let rowIndex = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
        switch (event.keyCode) {
            case 38:
                event.returnValue = 0;
                if ((rowIndex > 0) && (rowIndex < 10)) {
                    this.selectedRowFocus(this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex].children[0].children[0]);
                }
                break;
            case 40:
            case 9:
                event.returnValue = 0;
                if ((rowIndex >= 0) && (rowIndex < 9)) {
                    this.selectedRowFocus(this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex].children[0].children[0]);
                }
                break;
            default:
                break;
        }
    }

    public onChangePopulateDesc(): void {
        let postSearchParams = this.getURLSearchParamObject();
        postSearchParams.set(this.serviceConstants.Action, '6');
        let postParams: any = {};
        postParams.Function = 'SetDisplayFields';
        if (this.getControlValue('BranchNumber')) {
            postParams.BranchNumber = this.utils.getBranchCode();
        }
        if (this.getControlValue('BranchServiceAreaCode')) {
            postParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        }
        if (this.getControlValue('ContractNumber')) {
            postParams.ContractNumber = this.getControlValue('ContractNumber');
        }
        if (this.getControlValue('PremiseNumber')) {
            postParams.PremiseNumber = this.getControlValue('PremiseNumber');
        }
        if (this.getControlValue('ProductCode')) {
            postParams.ProductCode = this.getControlValue('ProductCode');
        }
        if (this.getAttribute('ProductCodeServiceCoverRowID')) {
            postParams.ServiceCoverRowID = this.getAttribute('ProductCodeServiceCoverRowID');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if ((data['hasError'])) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setControlValue('BranchName', '');
                        this.setControlValue('BranchServiceAreaDesc', '');
                        this.setControlValue('ContractName', '');
                        this.setControlValue('PremiseName', '');
                        this.setControlValue('ProductDesc', '');
                        this.setControlValue('ProductCode', '');
                        this.setControlValue('PremiseNumber', '');
                        this.setControlValue('BranchServiceAreaCode', '');
                    } else {
                        this.setControlValue('BranchName', data['BranchName']);
                        this.setControlValue('BranchServiceAreaDesc', data['BranchServiceAreaDesc']);
                        this.setControlValue('ContractName', data['ContractName']);
                        this.setControlValue('PremiseName', data['PremiseName']);
                        this.setControlValue('ProductDesc', data['ProductDesc']);
                        this.refresh();
                    }
                    this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                    this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                    this.PremiseEllipsis.updateComponent();
                    this.ellipsis.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                    this.ellipsis.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                    this.ProductEllipsis.updateComponent();

                    if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
                        this.ellipsis.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                        this.ellipsis.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
                        this.ellipsis.product.childConfigParams['parentMode'] = 'LookUp';
                        this.ellipsis.product.component = ServiceCoverSearchComponent;
                        this.ProductEllipsis.contentComponent = ServiceCoverSearchComponent;
                        this.ProductEllipsis.updateComponent();
                    }
                    else {
                        this.ellipsis.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                        this.ellipsis.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
                        this.ellipsis.product.childConfigParams['parentMode'] = 'LookUp-Entitlement';
                        this.ellipsis.product.component = ProductSearchGridComponent;
                        this.ProductEllipsis.contentComponent = ProductSearchGridComponent;
                        this.ProductEllipsis.updateComponent();
                    }
                },
                (error) => {
                    this.modalAdvService.emitMessage(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });

    }
    public onDataReceivedBSA(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
            this.onChangePopulateDesc();
        }
    }

    public onChangeContract(): void {
        if (this.getControlValue('ContractNumber')) {
            this.isServiceDate = this.isDeliveryNoteNumber = true;
        }
        else {
            this.isServiceDate = this.isDeliveryNoteNumber = false;
            this.setControlValue('PremiseNumber', '');
        }
        this.onChangePopulateDesc();

    }

    public onDataReceivedContract(data: any): void {
        if (data) {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            if (this.getControlValue('ContractNumber')) {
                this.isServiceDate = this.isDeliveryNoteNumber = true;
            }
            else {
                this.isServiceDate = this.isDeliveryNoteNumber = false;
            }
        }
        this.onChangePopulateDesc();
    }

    public onChangeProduct(): void {
        this.setAttribute('ProductCodeServiceCoverRowID', '');
        this.onChangePopulateDesc();
    }

    public onPremiseDataReceived(data: any): void {
        if (data) {
            this.setControlValue('PremiseNumber', data.PremiseNumber);
            this.setControlValue('PremiseName', data.PremiseName);
        }
        this.onChangePopulateDesc();

    }
    public onProductDataReceived(data: any): void {
        if (data) {
            this.setControlValue('ProductCode', data.ProductCode);
            this.setControlValue('ProductDesc', data.ProductDesc);
        }
    }

    public fromDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('ServiceDate', value.value);
        }
    }
}
