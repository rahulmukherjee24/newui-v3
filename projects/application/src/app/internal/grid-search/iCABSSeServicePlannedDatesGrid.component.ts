import { Component, Injector, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import 'rxjs/add/operator/takeWhile';
import { Subscription } from 'rxjs/Subscription';

import { InternalMaintenanceApplicationModuleRoutes, InternalMaintenanceServiceModuleRoutes } from './../../base/PageRoutes';
import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { AjaxConstant } from './../../../shared/constants/AjaxConstants';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { GlobalizeService } from './../../../shared/services/globalize.service';

@Component({
    templateUrl: 'iCABSSeServicePlannedDatesGrid.html'
})

export class ServicePlannedDatesGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riGrid') public riGrid: GridAdvancedComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    public pageId: string = '';
    public controls = [
        { name: 'ContractNumber', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServiceVisitFrequency', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseNumber', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, required: false, type: MntConst.eTypeTextFree },
        { name: 'DisplayLines', disabled: false, required: true, type: MntConst.eTypeInteger },
        { name: 'DueDateFrom', disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'DueDateTo', disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'PlanningStatus' }
    ];

    private headerParams: any = {
        method: 'service-planning/maintenance',
        operation: 'Service/iCABSSeServicePlannedDatesGrid',
        module: 'planning'
    };
    private isAlive: boolean = true;
    private lookUpSubscription: Subscription;
    private serviceCoverRowID: string = '';
    public requestParams: any = {
        ServiceCoverRowID: '',
        BranchServiceAreaCode: '',
        VisitTypeCode: '',
        PlanningStatus: 'Outstanding'
    };
    private planVisitRowID: string = '';

    public pageCurrent: number = 1;
    public pageSize: number = 10;
    public itemsPerPage: number = 10;
    public totalRecords: number;
    public dueDateFrom: string;
    public dueDateTo: string;

    constructor(injector: Injector,
        public globalize: GlobalizeService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNEDDATESGRID;
        this.pageTitle = this.browserTitle = 'Visits To Be Planned';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.lokupServiceCover();
        this.windowOnLoad();
    }

    ngAfterViewInit(): void {
        if (this.pageParams['PlanningStatus'] && this.pageParams['PlanningStatus'] !== '') {
            this.setControlValue('PlanningStatus', this.pageParams['PlanningStatus']);
        } else {
            this.setControlValue('PlanningStatus', this.requestParams['PlanningStatus']);
            this.requestParams['PlanningStatus'] = this.pageParams['PlanningStatus'];
            this.pageParams['PlanningStatus'] = '';
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.doCleanUp();
    }

    private doCleanUp(): void {
        this.serviceConstants = null;
        this.httpService = null;
        this.errorService = null;
        this.utils = null;
        this.ajaxSource = null;
        this.isAlive = false;
    }

    private windowOnLoad(): void {
        this.setCurrentContractType();
        if (this.parentMode === 'ServicePlanningHardSlot') {
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
        } else {
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
        }
        this.setControlValue('VisitTypeCode', this.riExchange.getParentAttributeValue('VisitTypeCode'));
        this.riGrid.PageSize = 10;
        this.setUIElementValues();
        this.serviceCoverRowID = this.riExchange.getParentHTMLValue('ServiceCoverRowID');
        this.requestParams['ServiceCoverRowID'] = this.riExchange.getParentHTMLValue('ServiceCoverRowID');
        this.requestParams['BranchServiceAreaCode'] = this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
        this.requestParams['VisitTypeCode'] = this.riExchange.getParentHTMLValue('VisitTypeCode');
        this.setControlValue('DisplayLines', this.pageSize);
        let d = new Date();
        let year = d.getFullYear();
        this.dueDateFrom = '01/01/' + year;
        this.dueDateTo = '31/12/' + year;
        this.setControlValue('DueDateFrom', this.dueDateFrom);
        this.setControlValue('DueDateTo', this.dueDateTo);
        this.fetchGriddata();
        this.buildGrid();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('OriginalVisitDueDate', 'PlanVisit', 'OriginalVisitDueDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('OriginalVisitDueDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('OriginalVisitDueDate', true);
        this.riGrid.AddColumn('VisitDesc', 'PlanVisit', 'VisitDesc', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('VisitDesc', MntConst.eAlignmentLeft);
        if (this.pageParams['PlanningStatus'] && this.pageParams['PlanningStatus'] !== '') {
            this.requestParams['PlanningStatus'] = this.pageParams['PlanningStatus'];
        }
        if (this.requestParams['PlanningStatus'] !== 'U' && this.requestParams['PlanningStatus'] !== 'C') {
            this.riGrid.AddColumn('PlannedVisitDate', 'PlanVisit', 'PlannedVisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('PlannedVisitDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('PlannedVisitDate', true);
        }
        if (this.requestParams['PlanningStatus'] !== 'U' && this.requestParams['PlanningStatus'] !== 'I' && this.requestParams['PlanningStatus'] !== 'C') {
            this.riGrid.AddColumn('ServicePlanNumber', 'PlanVisit', 'ServicePlanNumber', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ServicePlanNumber', MntConst.eAlignmentCenter);
        }
        if (this.requestParams['PlanningStatus'] === 'All' || this.requestParams['PlanningStatus'] === 'V') {
            this.riGrid.AddColumn('ActualVisitDate', 'PlanVisit', 'ActualVisitDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumnAlign('ActualVisitDate', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('ActualVisitDate', true);
            this.riGrid.AddColumn('ActualVisit', 'PlanVisit', 'ActualVisit', MntConst.eTypeText, 20);
            this.riGrid.AddColumnAlign('ActualVisit', MntConst.eAlignmentLeft);
        }
        this.riGrid.AddColumn('PlannedQuantity', 'PlanVisit', 'PlannedQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('PlannedQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PlanVisitStatus', 'PlanVisit', 'PlanVisitStatus', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('PlanVisitStatus', MntConst.eAlignmentLeft);
        if (this.requestParams['PlanningStatus'] !== 'All' && this.requestParams['PlanningStatus'] !== 'V') {
            this.riGrid.AddColumn('Cancel', 'PlanVisit', 'Cancel', MntConst.eTypeImage, 1);
            this.riGrid.AddColumnAlign('Cancel', MntConst.eAlignmentCenter);
        }
        this.riGrid.Complete();
    }

    private riGridBeforeExecute(): void {
        if (this.riGrid.Update) {
            this.riGrid.StartColumn = 0;
            this.riGrid.UpdateHeader = false;
            this.riGrid.UpdateBody = true;
            this.riGrid.UpdateFooter = false;
        }
    }

    private lokupServiceCover(): void {
        let lookupIP = [
            {
                'table': 'ServiceCover',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ROWID': this.riExchange.getParentAttributeValue('ServiceCoverRowID')
                },
                'fields': ['ContractNumber', 'PremiseNumber', 'ProductCode', 'ServiceVisitFrequency']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            this.setControlValue('ContractNumber', '');
            this.setControlValue('PremiseNumber', '');
            this.setControlValue('ProductCode', '');
            this.setControlValue('ServiceVisitFrequency', '');
            if (data && data[0] && data[0][0]) {
                let dataObj: any = data[0][0];
                if (dataObj['ContractNumber']) {
                    this.setControlValue('ContractNumber', dataObj['ContractNumber']);
                }
                if (dataObj['PremiseNumber']) {
                    this.setControlValue('PremiseNumber', dataObj['PremiseNumber']);
                }
                if (dataObj['ProductCode']) {
                    this.setControlValue('ProductCode', dataObj['ProductCode']);
                }
                if (dataObj['ServiceVisitFrequency']) {
                    this.setControlValue('ServiceVisitFrequency', dataObj['ServiceVisitFrequency']);
                }
            }
            this.lookUpServiceCoverDetails();
        });
    }

    private lookUpServiceCoverDetails(): void {
        let lookupIP = [
            {
                'table': 'Contract',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber')
                },
                'fields': ['ContractName']
            },
            {
                'table': 'Premise',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber')
                },
                'fields': ['PremiseName']
            },
            {
                'table': 'ProductLanguage',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ProductCode': this.getControlValue('ProductCode'),
                    'Language': this.riExchange.LanguageCode()
                },
                'fields': ['ProductLanguageDesc']
            },
            {
                'table': 'Product',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ProductCode': this.getControlValue('ProductCode')
                },
                'fields': ['RequiresVisitDetailTextInd']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0] && data[0][0]) {
                this.setControlValue('ContractName', data[0][0]['ContractName']);
            } else {
                this.setControlValue('ContractName', '');
            }
            if (data && data[1] && data[1][0]) {
                this.setControlValue('PremiseName', data[1][0]['PremiseName']);
            } else {
                this.setControlValue('PremiseName', '');
            }
            if (data && data[2] && data[2][0]) {
                this.setControlValue('ProductDesc', data[2][0]['ProductLanguageDesc']);
            } else {
                this.setControlValue('ProductDesc', '');
            }
        });
    }

    private setUIElementValues(): void {
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
        this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
    }

    private setCurrentContractType(): void {
        if (this.riExchange.URLParameterContains('job')) {
            this.pageParams.currentContractType = 'J';
            this.pageParams.currentContractTypeURLParameter = '<job>';
        } else if (this.riExchange.URLParameterContains('product')) {
            this.pageParams.currentContractType = 'P';
            this.pageParams.currentContractTypeURLParameter = '<product>';
        } else {
            this.pageParams.currentContractType = 'C';
            this.pageParams.currentContractTypeURLParameter = '';
        }
        let count: number;
        if (this.riExchange.ClientSideValues.Fetch('ContractTypes') && this.riExchange.ClientSideValues.Fetch('ContractTypes').contains(',')) {
            this.pageParams.contractTypesList = this.riExchange.ClientSideValues.Fetch('ContractTypes').split(',');
            count = 0;

            while (count <= this.pageParams.contractTypesList.length) {

                if (this.pageParams.contractTypesList[count] === this.pageParams.currentContractType) {
                    this.pageParams.currentContractTypeLabel = this.pageParams.contractTypesList[count + 1];
                    count = this.pageParams.contractTypesList.length;
                }

                count = count + 2;
            }

        }

    }

    private getServiceVisitFrequency(): void {
        let formData: Object = {};
        let postSearchParams: QueryParams = this.getURLSearchParamObject();
        formData[this.serviceConstants.Function] = 'GetFrequency';
        formData['ServiceCoverRowID'] = this.requestParams['ServiceCoverRowID'];
        formData['EndDate'] = this.globalize.parseDateToFixedFormat('24/12/2017').toString();
        formData['VisitTypeCode'] = this.riExchange.getParentHTMLValue('VisitTypeCode');
        postSearchParams.set(this.serviceConstants.Action, '6');
        postSearchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        postSearchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, postSearchParams, formData)
            .takeWhile(() => this.isAlive).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    }
                    else {
                        if (e['ServiceVisitFrequency']) {
                            this.setControlValue('ServiceVisitFrequency', e['ServiceVisitFrequency']);
                        } else {
                            this.setControlValue('ServiceVisitFrequency', '');
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private fetchGriddata(): void {
        this.dueDateFrom = this.getControlValue('DueDateFrom');
        this.dueDateTo = this.getControlValue('DueDateTo');
        this.pageSize = parseInt(this.getControlValue('DisplayLines'), 10);
        let searchParam: QueryParams = this.getURLSearchParamObject();
        searchParam.set(this.serviceConstants.Action, '2');
        searchParam.set('BranchNumber', this.cbbService.getBranchCode());
        searchParam.set('ServiceCoverRowID', this.riExchange.getParentAttributeValue('ServiceCoverRowID'));
        searchParam.set('BranchServiceAreaCode', this.requestParams['BranchServiceAreaCode']);
        searchParam.set('ServicePlanNumber', '0');
        searchParam.set('VisitTypeCode', this.riExchange.getParentAttributeValue('VisitTypeCode'));
        if (this.pageParams['PlanningStatus'] && this.pageParams['PlanningStatus'] !== '') {
            searchParam.set('PlanningStatus', this.pageParams['PlanningStatus']);
        } else if (this.requestParams['PlanningStatus'] && this.requestParams['PlanningStatus'] !== '') {
            searchParam.set('PlanningStatus', this.requestParams['PlanningStatus']);
        } else {
            this.requestParams['PlanningStatus'] = this.getControlValue('PlanningStatus');
            searchParam.set('PlanningStatus', this.requestParams['PlanningStatus']);
        }
        searchParam.set('DueDateFrom', this.globalize.parseDateToFixedFormat(this.dueDateFrom).toString());
        searchParam.set('DueDateTo', this.globalize.parseDateToFixedFormat(this.dueDateTo).toString());
        searchParam.set(this.serviceConstants.GridMode, '0');
        searchParam.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        searchParam.set(this.serviceConstants.PageSize, this.pageSize.toString());
        searchParam.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        searchParam.set(this.serviceConstants.GridHeaderClickedColumn, '');
        searchParam.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makeGetRequest(this.headerParams.method, this.headerParams.module,
            this.headerParams.operation, searchParam).takeWhile(() => this.isAlive)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        try {
                            this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageSize : 1;
                            this.riGridBeforeExecute();
                            this.riGrid.Execute(data);
                        } catch (e) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        }
                    }

                },
                (error) => {
                    this.ajaxSource.next(AjaxConstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    /** This method checks if the supplied value is a number */
    private checkNumeric(dataStr: string): void {
        let numbers: any = /^[0-9]+$/;
        if (!dataStr.match(numbers)) {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.dataNotNumeric));
        }
    }

    public dueDateFromSelectedValue(value: any): void {
        if (value && value.value) {
            this.setControlValue('DueDateFrom', value.value);
        } else {
            this.setControlValue('DueDateFrom', '');
        }
    }

    public dueDateToSelectedValue(value: any): void {
        if (value && value.value) {
            this.setControlValue('DueDateTo', value.value);
        } else {
            this.setControlValue('DueDateTo', '');
        }
    }

    /** Handle grid body click */
    public riGridBodyOnClick(event: any): void {
        let rowObj: any = event.srcElement.parentElement.parentElement;
        let cellInfo = rowObj.getAttribute('name');
        this.planVisitRowID = rowObj.parentElement.getElementsByTagName('td')[0].getAttribute('additionalproperty');
        if (cellInfo === 'Cancel') {
            if (rowObj.getAttribute('additionalproperty') === 'P') {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.PageSpecificMessage.unplanConfirmedPlanVisit, null, this.promptConfirm.bind(this)));
            } else {
                this.promptConfirm();
            }
        }
    }

    public tbodyServicePlannedDatesOnDblClick(event: any): void {
        let colName: string = this.riGrid.CurrentColumnName;
        this.planVisitRowID = this.riGrid.Details.GetAttribute(colName, 'additionalproperty');
        switch (colName) {
            case 'OriginalVisitDueDate':
                if (this.requestParams['PlanningStatus'] !== 'V' && this.getControlValue('ServicePlanNumber') === '0' || this.getControlValue('ServicePlanNumber') === '') {
                    this.routeAwayGlobals.setSaveEnabledFlag(false);
                    this.navigate('ServicePlanning', InternalMaintenanceServiceModuleRoutes.ICABSSEPLANVISITMAINTENANCE,
                        {
                            PlanVisitRowID: this.planVisitRowID,
                            ServiceCoverRowID: this.riExchange.getParentAttributeValue('ServiceCoverRowID'),
                            StartDate: this.riExchange.getParentHTMLValue('StartDate'),
                            EndDate: this.riExchange.getParentHTMLValue('EndDate')
                        });
                }
                break;
        }
    }

    /** Check the changed value set on planning status */
    public planningStatusOnchange(data: any): void {
        this.requestParams['PlanningStatus'] = data;
        this.pageParams['PlanningStatus'] = data;
        this.buildGrid();
        this.riGrid.RefreshRequired();
        this.fetchGriddata();
    }

    public promptConfirm(): void {
        this.routeAwayGlobals.setSaveEnabledFlag(false);
        this.pageParams['PlanningStatus'] = this.requestParams['PlanningStatus'];
        this.navigate('Cancel', InternalMaintenanceApplicationModuleRoutes.ICABSAPLANVISITMAINTENANCE,
            {
                PlanVisitRowID: this.planVisitRowID
            });
    }

    public onRiGridSort(): void {
        this.riGrid.RefreshRequired();
        this.refresh(null);
    }

    public refresh(event: any): void {
        this.riGrid.RefreshRequired();
        if (this.pageCurrent <= 0) {
            this.pageCurrent = 1;
        }
        if (event !== null) {
            this.riGrid.HeaderClickedColumn = '';
        }
        this.fetchGriddata();
    }

    public getCurrentPage(event: any): void {
        this.pageCurrent = event.value;
        this.riGrid.RefreshRequired();
        this.fetchGriddata();
    }

    public gridPageSizeOnchange(event: any): void {
        this.checkNumeric(event.target.value);
    }

}
