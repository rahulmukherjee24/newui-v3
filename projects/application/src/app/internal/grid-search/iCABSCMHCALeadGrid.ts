import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { GridComponent } from './../../../shared/components/grid/grid';
import { ErrorCallback } from './../../base/Callback';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { InternalMaintenanceApplicationModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSCMHCALeadGrid.html',
    styles: [`
    :host /deep/ .gridtable.table-bordered>thead>tr>th:nth-child(8)
    {
        width: 30%;
    }`]
})

export class LeadGridComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy, ErrorCallback {
    @ViewChild('LeadGrid') LeadGrid: GridComponent;
    @ViewChild('LeadPagination') LeadPagination: PaginationComponent;
    @ViewChild('errorModal') public errorModal;

    private LeadDateFromString: string;
    private search: any;
    private LeadDateToString: string;
    public LeadDateFrom: Date | boolean;
    public LeadDateTo: Date | boolean;
    private messages: any = {
        gridNotLoadedError: 'Grid Not Loaded'
    };

    public pageTitle: string;
    public pageId: string = '';
    public currentPage: number = 1;
    public PageSize: number = 10;
    public maxColumn: any = 9;
    public totalItems: number;

    public griddata: any;
    public controls = [
        { name: 'EmployeeCode' },
        { name: 'EmployeeSurname', disabled: true },
        { name: 'ContractNumber' },
        { name: 'ContractName', disabled: true },
        { name: 'PremiseNumber' },
        { name: 'PremiseName', disabled: true },
        { name: 'LeadDateFrom', required: true, type: MntConst.eTypeDate },
        { name: 'LeadDateTo', required: true, type: MntConst.eTypeDate },
        { name: 'LeadTypeCode', value: 'All' },
        { name: 'ActiveOnly' }
    ];
    public headerParams: any = {
        method: 'prospect-to-contract/maintenance',
        operation: 'ContactManagement/iCABSCMHCALeadGrid',
        module: 'prospect'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSCMHCALEADGRID;
        this.browserTitle = 'PDA Lead/Alert';
        this.pageTitle = 'PDA Lead/Alert Details';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.setErrorCallback(this);
        this.windowOnLoad();
        this.setDate();
    }

    ngAfterViewInit(): void {
        this.setControlValue('LeadTypeCode', 'All');
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        if (this.parentMode === 'PremisesPDAVisit' || this.parentMode === 'Debrief') {
            this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
            this.disableControl('EmployeeCode', true);
            this.disableControl('ContractNumber', true);
            this.disableControl('PremiseNumber', true);
            this.disableControl('LeadDateFrom', true);
            this.disableControl('LeadDateTo', true);
            if (this.parentMode === 'Debrief') {
                if (this.getControlValue('EmployeeCode') === '') {
                    this.setControlValue('EmployeeCode', this.riExchange.getParentAttributeValue('EmployeeCode'));
                    this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
                }
                this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
                this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
                this.setControlValue('LeadDateFrom', this.riExchange.getParentAttributeValue('ActualVisitDate'));
                this.setControlValue('LeadDateTo', this.riExchange.getParentAttributeValue('ActualVisitDate'));
                this.getRequest();
            } else {
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
                this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
                this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                this.setControlValue('LeadDateFrom', this.riExchange.getParentHTMLValue('ActualVisitDate'));
                this.setControlValue('LeadDateTo', this.riExchange.getParentHTMLValue('ActualVisitDate'));
            }
        } else if (this.parentMode === 'Debrief-All') {
            this.setControlValue('EmployeeCode', this.riExchange.getParentHTMLValue('EmployeeCode'));
            this.setControlValue('EmployeeSurname', this.riExchange.getParentHTMLValue('EmployeeSurname'));
            this.setControlValue('LeadDateFrom', this.riExchange.getParentHTMLValue('DebriefFromDate'));
            this.setControlValue('LeadDateTo', this.riExchange.getParentHTMLValue('DebriefToDate'));
            this.disableControl('EmployeeCode', true);
        }
        this.buildGrid();
    }

    private buildGrid(): void {
        let search: QueryParams;
        search = this.getURLSearchParamObject();
        search.set('EmployeeCode', this.getControlValue('EmployeeCode'));
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('LeadDateFrom', this.getControlValue('LeadDateFrom'));
        search.set('LeadDateTo', this.getControlValue('LeadDateTo'));
        search.set('LeadTypeCode', this.getControlValue('LeadTypeCode'));
        search.set('ActiveOnly', this.getControlValue('ActiveOnly'));
        search.set('riGridHandle', this.getControlValue('riGridHandle'));
        search.set('riCacheRefresh', this.getControlValue('riCacheRefresh'));
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.PageSize, '10');
        search.set(this.serviceConstants.PageCurrent, this.currentPage.toString());
        search.set(this.serviceConstants.GridMode, '0');
        search.set('riSortOrder', 'Descending');
        this.headerParams.search = search;
        this.LeadGrid.loadGridData(this.headerParams);
    }

    private getRequest(): void {
        let searchPost: QueryParams = this.getURLSearchParamObject();
        searchPost.set(this.serviceConstants.Action, '6');
        let postParams: any = {
            BusinessCode: this.businessCode(),
            ContractNumber: this.getControlValue('ContractNumber'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            Function: 'GetDescriptions'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.headerParams.method, this.headerParams.module, this.headerParams.operation, searchPost, postParams)
            .subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.setControlValue('ContractName', data.ContractName);
                    this.setControlValue('PremiseName', data.PremiseName);
                }
            }, error => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.recordNotFound));
            });
    }

    private fetchTranslatedContent(): void {
        this.getTranslatedValue(this.messages.gridNotLoadedError, null).subscribe((res: string) => {
            this.zone.run(() => {
                if (res) {
                    this.messages.gridNotLoadedError = res;
                }
            });
        });
    }

    private setDate(): void {
        //dateFrom
        if (this.getControlValue('LeadDateFrom')) {
            let getFromDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('LeadDateFrom'));
            this.LeadDateFrom = this.globalize.parseDateStringToDate(getFromDate);

        } else {
            this.LeadDateFrom = null;
        }
        //dateTo
        if (this.getControlValue('LeadDateTo')) {
            let getFromDate: any = this.globalize.parseDateToFixedFormat(this.getControlValue('LeadDateTo'));
            this.LeadDateTo = this.globalize.parseDateStringToDate(getFromDate);
        } else {
            this.LeadDateTo = null;
        }
    }

    public refresh(): void {
        this.buildGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.currentPage = currentPage.value;
        this.buildGrid();
    }

    public showErrorModal(data: any): void {
        this.errorModal.show({ msg: data.msg, title: 'Error' }, false);
    }

    public onGridRowDblClick(data: any): void {
        this.griddata = data;
        switch (data.cellIndex) {
            case 1:
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
                break;
            case 2:
                this.navigate('PDAICABSLead', InternalMaintenanceApplicationModuleRoutes.ICABSSCMCUSTOMERCONTACTMAINTENANCE.URL_1, { //page yet to be developed
                    parentMode: 'PDAICABSLead',
                    CustomerContactNumber: data.trRowData[2].text
                });
                break;
        }
    }

    public LeadDateFromSelectedValue(event: any): void {
        if (event && event.value) {
            this.setControlValue('LeadDateFrom', event.value);
        }
    }
    public LeadDateToSelectedValue(event: any): void {
        if (event && event.value) {
            this.setControlValue('LeadDateTo', event.value);
        }
    }

    public getGridInfo(info: any): void {
        this.totalItems = info.totalRows;
    }
    public validateProperties: Array<any> = [{
        'type': MntConst.eTypeCode,
        'index': 0
    }, {
        'type': MntConst.eTypeText,
        'index': 1
    }, {
        'type': MntConst.eTypeInteger,
        'index': 2
    }, {
        'type': MntConst.eTypeCode,
        'index': 3
    }, {
        'type': MntConst.eTypeInteger,
        'index': 4
    }, {
        'type': MntConst.eTypeText,
        'index': 5
    }, {
        'type': MntConst.eTypeDate,
        'index': 6
    }, {
        'type': MntConst.eTypeTextFree,
        'index': 7
    }, {
        'type': MntConst.eTypeImage,
        'index': 8
    }];
}
