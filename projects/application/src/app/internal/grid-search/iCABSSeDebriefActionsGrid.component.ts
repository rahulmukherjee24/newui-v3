import { Component, OnInit, OnDestroy, AfterViewInit, Injector, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from '../../base/PageIdentifier';
import { AppModuleRoutes, ContractManagementModuleRoutes, InternalGridSearchApplicationModuleRoutes, ServiceDeliveryModuleRoutes } from './../../base/PageRoutes';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { BranchServiceAreaSearchComponent } from './../search/iCABSBBranchServiceAreaSearch';
import { PremiseSearchComponent } from './../search/iCABSAPremiseSearch';
import { EmployeeSearchComponent } from './../search/iCABSBEmployeeSearch';

@Component({
    templateUrl: 'iCABSSeDebriefActionsGrid.html'
})

export class DebriefActionsGridComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private blnCacheRefresh: boolean = true;
    private muleConfig: any = {
        method: 'service-delivery/maintenance',
        module: 'sam',
        operation: 'Service/iCABSSeDebriefActionsGrid'
    };

    public pageId: string = '';
    public controls = [
        { name: 'SupervisorEmployeeCode', required: false, disabled: true, type: MntConst.eTypeCode },
        { name: 'SupervisorEmployeeName', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumber', required: false, disabled: false, type: MntConst.eTypeCode },
        { name: 'ContractName', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', required: false, disabled: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', required: false, disabled: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'EmployeeCode', required: false, disabled: false, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ServicePlanNumber', required: false, disabled: false, type: MntConst.eTypeInteger },
        { name: 'ActionFromDate', required: false, disabled: false, type: MntConst.eTypeDate },
        { name: 'ActionToDate', required: false, disabled: false, type: MntConst.eTypeDate },
        { name: 'FilterStatus', required: false, disabled: false },
        { name: 'DisplayNumberOfRows', required: false, disabled: false, type: MntConst.eTypeInteger },
        { name: 'DebriefNumber', required: false, disabled: false, type: MntConst.eTypeInteger }
    ];
    public searchConfigs: any = {
        contractSearchComponent: {
            params: {
                parentMode: 'LookUp-All'
            },
            component: ContractSearchComponent
        },
        branchServiceAreaSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp'
            },
            component: BranchServiceAreaSearchComponent
        },
        premiseSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp',
                ContractNumber: '',
                ContractName: ''
            },
            component: PremiseSearchComponent
        },
        employeeSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp'
            },
            component: EmployeeSearchComponent
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        }
    };
    public queryPost: QueryParams = this.getURLSearchParamObject();
    public isAutoOpen: boolean = false;
    public gridParams: any = {
        totalRecords: 0,
        pageCurrent: 1,
        itemsPerPage: 10,
        riGridMode: 0
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEDEBRIEFACTIONSGRID;
        this.browserTitle = this.pageTitle = 'Service Action Monitor - Actions';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
        } else {
            this.setControlValue('FilterStatus', 'All');
            this.setControlValue('DisplayNumberOfRows', 10);
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams['currentPage'] = 1;
        }
        this.buildGrid();
        this.riGrid.FunctionUpdateSupport = true; // Updatable grid
        this.riGrid.FunctionTabSupport = true;
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        if (this.parentMode === 'DebriefOutstanding') {
            this.setControlValue('SupervisorEmployeeCode', this.riExchange.getParentAttributeValue('SupervisorEmployeeCode'));
            this.setControlValue('SupervisorEmployeeName', this.riExchange.getParentAttributeValue('SupervisorEmployeeName'));
            this.riGridOnRefresh();
        }
    }

    private populateContract(): void {
        this.queryPost.set(this.serviceConstants.Action, '6');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            ContractNumber: this.getControlValue('ContractNumber'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            Function: 'GetContract'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
                (data) => {
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.setControlValue('ContractName', data.ContractName);
                        this.setControlValue('PremiseName', data.PremiseName);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private populateDescriptions(): void {
        this.queryPost.set(this.serviceConstants.Action, '6');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            BranchNumber: this.utils.getBranchCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            EmployeeCode: this.getControlValue('EmployeeCode'),
            Function: 'GetDescriptions'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
                (data) => {
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                        this.setControlValue('EmployeeCode', data.EmployeeCode);
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        this.riGridOnRefresh();
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('BranchServiceArea', 'Grid', 'BranchServiceArea', MntConst.eTypeCode, 5);
        this.riGrid.AddColumnAlign('BranchServiceArea', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('EmployeeDetails', 'Grid', 'EmployeeDetails', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('EmployeeDetails', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNum', 'Grid', 'PremiseNum', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseNum', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseDetails', 'Grid', 'PremiseDetails', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PremiseDetails', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('PlannedVisitDate', 'Grid', 'PlannedVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('PlannedVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicePlanNum', 'Grid', 'ServicePlanNum', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServicePlanNum', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActualVisitDate', 'Grid', 'ActualVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ActualVisitDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActualStartTime', 'Grid', 'ActualStartTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('ActualStartTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActualEndTime', 'Grid', 'ActualEndTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('ActualEndTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('CurrentStatus', 'Grid', 'CurrentStatus', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('CurrentStatus', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('SignatureRequired', 'Grid', 'SignatureRequired', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('SignatureRequired', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('BarcodeScanRequired', 'Grid', 'BarcodeScanRequired', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('BarcodeScanRequired', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('SignatureCaptured', 'Grid', 'SignatureCaptured', MntConst.eTypeImage, 10);
        this.riGrid.AddColumnAlign('SignatureCaptured', MntConst.eAlignmentCenter);

        /* this.riGrid.AddColumn('NameCaptured', 'Grid', 'NameCaptured', MntConst.eTypeImage, 10);
        this.riGrid.AddColumnAlign('NameCaptured', MntConst.eAlignmentCenter); */

        this.riGrid.AddColumn('PremiseContactName', 'Grid', 'PremiseContactName', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PremiseContactName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('SignatureApprovedInd', 'Grid', 'SignatureApprovedInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('SignatureApprovedInd', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('BarcodeScanCaptured', 'Grid', 'BarcodeScanCaptured', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('BarcodeScanCaptured', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActionRequiredInd', 'Grid', 'ActionRequiredInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('ActionRequiredInd', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActionCreatedDate', 'Grid', 'ActionCreatedDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ActionCreatedDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ActionCreatedTime', 'Grid', 'ActionCreatedTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('ActionCreatedTime', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('LeadsAlerts', 'Grid', 'LeadsAlerts', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('LeadsAlerts', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitRejectionsNumber', 'Grid', 'VisitRejectionsNumber', MntConst.eTypeInteger, 1);
        this.riGrid.AddColumnAlign('VisitRejectionsNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('DebriefNum', 'Grid', 'DebriefNum', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('DebriefNum', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('DebriefDate', 'Grid', 'DebriefDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('DebriefDate', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('BranchServiceArea', true);
        this.riGrid.AddColumnOrderable('ActualVisitDate', true);
        this.riGrid.AddColumnOrderable('ActionCreatedDate', true);

        this.riGrid.Complete();
    }

    private loadGridData(rowId?: string): void {
        let search: QueryParams = this.getURLSearchParamObject();
        // TODO: Null check of dates
        if (new Date(this.getControlValue('ActionFromDate')) < new Date(this.getControlValue('ActionToDate'))) {
            // TODO set error in date
        }
        if (rowId) {
            search.set('ROWID', rowId);
            this.riGrid.Update = true;
        }
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        search.set(this.serviceConstants.BranchNumber, this.utils.getBranchCode());
        search.set('SupervisorEmployeeCode', this.getControlValue('SupervisorEmployeeCode'));
        search.set('SupervisorEmployeeName', this.getControlValue('SupervisorEmployeeName'));
        search.set(this.serviceConstants.BranchServiceAreaCode, this.getControlValue('BranchServiceAreaCode'));
        search.set(this.serviceConstants.EmployeeCode, this.getControlValue('EmployeeCode'));
        search.set('ActionFromDate', this.getControlValue('ActionFromDate'));
        search.set('ActionToDate', this.getControlValue('ActionToDate'));
        search.set('FilterStatus', this.getControlValue('FilterStatus'));
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        search.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        search.set('ServicePlanNumber', this.getControlValue('ServicePlanNumber'));
        if (this.blnCacheRefresh) {
            search.set(this.serviceConstants.GridCacheRefresh, true);
        }
        search.set(this.serviceConstants.PageSize, this.gridParams.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams['currentPage'].toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.GridMode, this.gridParams.riGridMode);
        search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module,
            this.muleConfig.operation, search)
            .subscribe(
                (data) => {
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage));
                    else {
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.getAttribute('grdGridRow');
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.getAttribute('grdGridRowID');
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = false;
                        } else {
                            this.pageParams['currentPage'] = data.pageData ? data.pageData.pageNumber : 1;
                            this.gridParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                            this.blnCacheRefresh = false;
                        }
                        this.riGrid.RefreshRequired();
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams['currentPage']);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.gridParams.totalRecords = 1;
                    this.modalAdvService.emitError(new ICabsModalVO(error));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    private onSignatureApprovedIndDblClick(rowId: any): void {
        this.queryPost.set(this.serviceConstants.Action, '6');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            RowID: rowId,
            Function: 'UpdateSignatureApprovedInd'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
                (data) => {
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.setAttribute('grdGridRow', this.riGrid.CurrentRow);
                        this.setAttribute('grdGridRowID', rowId);
                        this.riGrid.Update = true;
                        this.loadGridData(rowId);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private onActionRequiredIndDblClick(rowId: any): void {
        this.queryPost.set(this.serviceConstants.Action, '6');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            RowID: rowId,
            Function: 'UpdateActionRequiredInd'
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
                (data) => {
                    if (data.hasError)
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    else {
                        this.setAttribute('grdGridRow', this.riGrid.CurrentRow);
                        this.setAttribute('grdGridRowID', rowId);
                        this.riGrid.Update = true;
                        this.loadGridData(rowId);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public gridFocus(rsrcElement: any): void {
        this.setAttribute('BranchServiceAreaCode', this.riGrid.Details.GetValue('BranchServiceArea'));
        this.setAttribute('BranchServiceAreaDesc', this.riGrid.Details.GetAttribute('BranchServiceArea', 'AdditionalProperty'));
        this.setAttribute('EmployeeCode', this.riGrid.Details.GetAttribute('SignatureRequired', 'AdditionalProperty'));
        this.setAttribute('EmployeeSurname', this.riGrid.Details.GetAttribute('BarcodeScanRequired', 'AdditionalProperty'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseDetails', 'AdditionalProperty'));
        this.setAttribute('ContractNumber', this.riGrid.Details.GetAttribute('ActualStartTime', 'AdditionalProperty'));
        this.setAttribute('PremiseNumber', this.riGrid.Details.GetAttribute('ActualEndTime', 'AdditionalProperty'));
        this.setAttribute('PlannedVisitDate', this.riGrid.Details.GetValue('PlannedVisitDate'));
        this.setAttribute('ActualVisitDate', this.riGrid.Details.GetValue('ActualVisitDate'));
        this.setAttribute('ActualStartTime', this.riGrid.Details.GetValue('ActualStartTime'));
        this.setAttribute('ActualEndTime', this.riGrid.Details.GetValue('ActualEndTime'));
        this.setAttribute('ServicePlanNumber', this.riGrid.Details.GetValue('ServicePlanNum'));
        this.setControlValue('DebriefNumber', this.riGrid.Details.GetValue('DebriefNum'));
    }

    public sortGrid(): void {
        this.loadGridData();
    }

    public trDoubleClick(event: any): void {
        if (this.utils.hasClass(event.srcElement, 'pointer')) {
            this.gridFocus(event.srcElement.target);
            let currentContractTypeURLParameter: string;
            switch (this.riGrid.Details.GetAttribute('CurrentStatus', 'AdditionalProperty')) {
                case 'C':
                    currentContractTypeURLParameter = '';
                    break;
                case 'J':
                    currentContractTypeURLParameter = '<job>';
                    break;
                case 'P':
                    currentContractTypeURLParameter = '<product>';
                    break;
            }

            switch (this.riGrid.CurrentColumnName) {
                case 'PremiseNum':
                    this.navigate('ServicePlanning', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, currentContractTypeURLParameter);
                    break;
                case 'CurrentStatus':
                    this.navigate('General', InternalGridSearchApplicationModuleRoutes.ICABSSEPREMISEPDAICABSACTIVITYGRID, currentContractTypeURLParameter);
                    break;
                case 'BarcodeScanCaptured':
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
                    // this.navigate('DebriefBranchGrid', Service/iCABSSeScannedBarcodes.htm);
                    break;
                case 'LeadsAlerts':
                    this.navigate('Debrief', InternalGridSearchApplicationModuleRoutes.ICABSCMHCALEADGRID);
                    break;
                case 'VisitRejectionsNumber':
                    this.navigate('DebriefAction', AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSSESERVICEVISITREJECTIONSGRID);
                    break;
                case 'SignatureApprovedInd':
                    this.onSignatureApprovedIndDblClick(this.riGrid.Details.GetAttribute('SignatureApprovedInd', 'rowid'));
                    break;
                case 'ActionRequiredInd':
                    this.onActionRequiredIndDblClick(this.riGrid.Details.GetAttribute('ActionRequiredInd', 'rowid'));
                    break;
            }
        }
    }

    public riGridOnRefresh(): void {
        this.blnCacheRefresh = true;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    // To get the current page in grid
    public getCurrentPage(currentPage: any): void {
        if (this.pageParams['currentPage'] !== currentPage.value) {
            this.pageParams['currentPage'] = currentPage.value;
            this.riGridOnRefresh();
        }
    }

    public setContract(data: any): void {
        if (data) {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            this.searchConfigs.premiseSearch.params.ContractNumber = data.ContractNumber;
            this.searchConfigs.premiseSearch.params.ContractName = data.ContractName;
        }
    }

    public contractNumberOnChange(): void {
        if (this.getControlValue('ContractNumber') === '') {
            this.setControlValue('ContractName', '');
            this.setControlValue('PremiseNumber', '');
            this.setControlValue('PremiseName', '');
        } else
            this.populateContract();
        this.searchConfigs.premiseSearch.params.ContractNumber = this.getControlValue('ContractNumber');
        this.searchConfigs.premiseSearch.params.ContractName = this.getControlValue('ContractName');
    }

    public setBranchServiceArea(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
        }
    }

    public branchServiceAreaCodeOnChange(): void {
        if (this.getControlValue('BranchServiceAreaCode') !== '') {
            this.setControlValue('EmployeeCode', '');
            this.populateDescriptions();
        } else
            this.setControlValue('BranchServiceAreaDesc', '');
    }

    public setPremise(data: any): void {
        if (data) {
            this.setControlValue('PremiseNumber', data.PremiseNumber);
            this.setControlValue('PremiseName', data.PremiseName);
        }
    }

    public premiseNumberOnChange(): void {
        if (this.getControlValue('PremiseNumber') === '')
            this.setControlValue('PremiseName', '');
        else
            this.populateContract();
    }

    public setEmployee(data: any): void {
        if (data) {
            this.setControlValue('EmployeeCode', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
    }

    public employeeCodeOnChange(): void {
        if (this.getControlValue('EmployeeCode') !== '') {
            this.setControlValue('BranchServiceAreaCode', '');
            this.populateDescriptions();
        } else
            this.setControlValue('EmployeeSurname', '');
    }

    public actionFromDateOnChange(value: any): void {
        if (value && value.value) {
            this.setControlValue('ActionFromDate', value.value);
        }
    }

    public actionToDateOnChange(value: any): void {
        if (value && value.value) {
            this.setControlValue('ActionToDate', value.value);
        }
    }

    public displayNumberOfRowsOnChange(): void {
        this.gridParams.itemsPerPage = this.getControlValue('DisplayNumberOfRows');
    }
}
