import { Component, Injector, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { BaseComponent } from '@app/base/BaseComponent';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '@shared/constants/message.constant';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { Subscription } from 'rxjs';
import { ContractSearchComponent } from '../search/iCABSAContractSearch';
import { PremiseSearchComponent } from '../search/iCABSAPremiseSearch';
import { ServiceCoverSearchComponent } from '../search/iCABSAServiceCoverSearch';

@Component({
    templateUrl: 'iCABSSeServiceVisitAnnivDateMaintenance.html'
})
export class ServiceVisitAnnivDateMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    public pageId: string;
    public lookUpSub: Subscription;
    public controls: any = [
        { name: 'BusinessCode',  disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ContractNumber', disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'ContractName',  disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'PremiseName',  disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'ProductDesc',  disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServiceQuantity',  disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ServiceVisitFrequency',  disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ServiceAnnualValue',  disabled: true, required: false, type: MntConst.eTypeCurrency },
        { name: 'ServiceCommenceDate',  disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'ServiceVisitAnnivDate',  disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'Status',  disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'InactiveEffectDate',  disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'LastChangeEffectDate', required: true, type: MntConst.eTypeDate },
        { name: 'ServiceCoverROWID',  disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'NewAnnivDate', disabled: false, required: true, type: MntConst.eTypeDate }
    ];
    public ellipsisConfig = {
        contract: {
            showHeader: true,
            autoOpen: false,
            showCloseButton: true,
            disabled: false,
            childConfigParams: {
                'parentMode': 'LookUp',
                'showAddNew': false
            },
            component: ContractSearchComponent
        },
        premise: {
            showHeader: true,
            showCloseButton: true,
            disabled: false,
            childConfigParams: {
                'parentMode': 'LookUp',
                'ContractNumber': '',
                'ContractName': '',
                'showAddNew': false
            },
            component: PremiseSearchComponent
        },
        product: {
            showHeader: true,
            showCloseButton: true,
            disabled: false,
            childConfigParams: {
                'parentMode': 'Search',
                'ContractNumber': '',
                'PremiseNumber': '',
                'ContractName': '',
                'PremiseName': '',
                'ProductCode': '',
                'ProductDesc': ''
            },
            component: ServiceCoverSearchComponent
        }
    };
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public xhrParams = {
        operation: 'Service/iCABSSeServiceVisitAnnivDateMaintenance',
        module: 'reports',
        method: 'bi/reports'
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITANNIVDATEMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Service Visit Anniversary Date Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.windowOnload();
    }

    ngOnDestroy(): void {
        if (this.lookUpSub) {
            this.lookUpSub.unsubscribe();
        }
    }

    public riMaintenance_Search(data: any, type: string): void {
        switch (type) {
            case 'contract':
                this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'ContractNumber');
                this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = data.ContractNumber;
                this.ellipsisConfig.premise.childConfigParams['ContractName'] = data.ContractName;
                this.ellipsisConfig.product.childConfigParams['ContractNumber'] = data.ContractNumber;
                this.ellipsisConfig.product.childConfigParams['ContractName'] = data.ContractName;
                this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = '';
                this.ellipsisConfig.product.childConfigParams['PremiseName'] = '';
                this.pageParams.bCode = data.BusinessCode;
                this.pageParams.countryCode = data.CountryCode;
                this.pageParams.cNumber = data.ContractNumber;
                this.pageParams.cName = data.ContractName;
                this.resetFields(type);
                break;
            case 'premise':
                this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'PremiseNumber');
                this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = data.PremiseNumber;
                this.ellipsisConfig.product.childConfigParams['PremiseName'] = data.PremiseName;
                this.pageParams.pNumber = data.PremiseNumber;
                this.pageParams.pName = data.PremiseName;
                this.resetFields(type);
                break;
            case 'product':
                this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'ProductCode');
                this.attributes.ServiceCoverRowID = data.row.ttServiceCover;
                this.pageParams.serviceCode = data.row.ttServiceCover;
                this.pageParams.pCode = data.row.ProductCode;
                this.pageParams.pDesc = data.row.ProductDesc;
                this.pageParams.ServiceCoverNumber = data.row.ServiceCoverNumber;
                this.resetFields(type);
                this.populateFields();
                break;
        }
    }

    public onContractChange(cNumber: string): void {
        this.uiForm.reset();
        this.uiForm.markAsPristine();
        if (cNumber) {
            this.pageParams.cNumber = cNumber;
            this.pageParams.cName = '';
            this.setControlValue('ContractNumber', cNumber);
            this.doLookupformData();
        }
    }

    public onSaveClick(): void {
        setTimeout(() => {
            let promptVO: ICabsModalVO = new ICabsModalVO();
            promptVO.msg = MessageConstant.Message.ConfirmRecord;
            promptVO.confirmCallback = this.save.bind(this);
            this.modalAdvService.emitPrompt(promptVO);
        }, 0);
    }

    public cancel(): void {
        if (this.parentMode === 'VisitDateDiscrepancy') {
            this.location.back();
        }
    }

    private save(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.BusinessCode, this.pageParams.bCode);
        search.set(this.serviceConstants.CountryCode, this.pageParams.countryCode);

        let formData: Object = {
            'ACTION': '2',
            'BusinessCode': this.getControlValue('BusinessCode')
        };
        let saveArray = ['BusinessCode','ContractNumber','PremiseNumber','ProductCode','ServiceCoverROWID','ServiceQuantity','ServiceVisitFrequency',
        'ServiceAnnualValue','ServiceCommenceDate','ServiceVisitAnnivDate','InactiveEffectDate','NewAnnivDate','LastChangeEffectDate'];

        saveArray.forEach(formKey => {
            formData = {
                ...formData,
                [formKey]: this.getControlValue(formKey)
            };
        });

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpSubscription = this.httpService.makePostRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData)
            .subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.uiForm.markAsPristine();
                    this.location.back();
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private windowOnload(): void {
        this.setControlValue('BusinessCode', this.businessCode());
        this.pageParams.bCode = this.businessCode();
        this.pageParams.countryCode = this.utils.getCountryCode();
        if (this.riMaintenance.RecordSelected(false)) {
            this.riMaintenance.FetchRecord();
            if (this.parentMode === 'VisitDateDiscrepancy') {
                this.disableControls(['NewAnnivDate','LastChangeEffectDate']);
                this.ellipsisConfig.contract.disabled =  true;
                this.ellipsisConfig.premise.disabled = true;
                this.ellipsisConfig.product.disabled = true;
                this.riMaintenance.UpdateMode();
                let date = new Date().getUTCFullYear() + '/01' + '/01';
                this.setControlValue('LastChangeEffectDate', this.globalize.parseDateToFixedFormat(date));
            }
        }
        if (this.parentMode === 'VisitDateDiscrepancy') {
            this.riMaintenance.RowID(this, 'ServiceCoverROWID', this.riExchange.getParentAttributeValue('ServiceCoverRowID'));
        }
        this.populateFields();
    }

    private resetFields(fieldName: any): void {
        this.uiForm.reset();
        switch (fieldName) {
            case 'contract' :
                this.setControlValue('ContractNumber', this.pageParams.cNumber);
                this.setControlValue('ContractName', this.pageParams.cName);
            break;
            case 'premise' :
                this.setControlValue('ContractNumber', this.pageParams.cNumber);
                this.setControlValue('ContractName', this.pageParams.cName);
                this.setControlValue('PremiseNumber', this.pageParams.pNumber);
                this.setControlValue('PremiseName', this.pageParams.pName);
            break;
            case 'product':
                this.setControlValue('ContractNumber', this.pageParams.cNumber);
                this.setControlValue('ContractName', this.pageParams.cName);
                this.setControlValue('PremiseNumber', this.pageParams.pNumber);
                this.setControlValue('PremiseName', this.pageParams.pName);
                this.setControlValue('ProductCode', this.pageParams.pCode);
                this.setControlValue('ProductDesc', this.pageParams.pDesc);
                this.setControlValue('ServiceCoverROWID', this.pageParams.serviceCode);
            break;
        }
    }

    private populateFields(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.BusinessCode, this.pageParams.bCode);
        searchParams.set(this.serviceConstants.CountryCode, this.pageParams.countryCode);
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('ContractNumber', this.getControlValue('ContractNumber'));
        searchParams.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        searchParams.set('ProductCode', this.getControlValue('ProductCode'));
        searchParams.set('ServiceCoverROWID', this.getControlValue('ServiceCoverROWID'));
        this.httpService.makeGetRequest(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, searchParams).subscribe(
            (data) => {
                if ((data.errorMessage && data.errorMessage !== '') || data.fullError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                }
                else {
                    this.setControlValue('ContractNumber', data.ContractNumber);
                    this.setControlValue('PremiseNumber', data.PremiseNumber);
                    this.setControlValue('ProductCode', data.ProductCode);
                    this.doLookupformData();
                    this.setControlValue('Status', data.Status);
                    this.setControlValue('ServiceAnnualValue', data.ServiceAnnualValue);
                    this.setControlValue('ServiceQuantity', data.ServiceQuantity);
                    this.setControlValue('ServiceCommenceDate', data.ServiceCommenceDate);
                    this.setControlValue('ServiceVisitFrequency', data.ServiceVisitFrequency);
                    this.setControlValue('ServiceVisitAnnivDate', data.ServiceVisitAnnivDate);
                    this.setControlValue('LastChangeEffectDate', this.globalize.parseDateToFixedFormat(new Date().getUTCFullYear() + '/01' + '/01'));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                }
            });
    }

    private doLookupformData(): void {
        let lookup = [
            {
                'table': 'Contract',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber')
                },
                'fields': ['ContractName']
            },
            {
                'table': 'Premise',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber')
                },
                'fields': ['PremiseName']
            },
            {
                'table': 'Product',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'ProductCode': this.getControlValue('ProductCode')
                },
                'fields': ['ProductDesc']
            }
        ];

        this.lookUpSub = this.LookUp.lookUpRecord(lookup).subscribe((data) => {
            if (data[0].length > 0) {
                this.pageParams.cName = data[0][0].ContractName;
                this.setControlValue('ContractName', data[0][0].ContractName);
                this.ellipsisConfig.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                this.ellipsisConfig.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                this.ellipsisConfig.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                this.ellipsisConfig.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
            }
            else {
                this.pageParams.cNumber = '';
                this.setControlValue('ContractNumber', '');
                this.modalAdvService.emitError(new ICabsModalVO('No Record Found'));
            }
            if (data[1].length > 0) {
                 this.setControlValue('PremiseName', data[1][0].PremiseName);
                 this.ellipsisConfig.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                 this.ellipsisConfig.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
            }
            if (data[2].length > 0) {
                this.setControlValue('ProductDesc', data[2][0].ProductDesc);
            }
        });
    }
}
