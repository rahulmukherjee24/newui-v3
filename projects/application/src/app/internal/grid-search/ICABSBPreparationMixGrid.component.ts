/**
 * The PreparationMixGridComponent to display preparation mix grid
 * @author icabs ui team
 * @version 1.0
 * @since   22/01/2018
 */
import { Component, OnInit, Injector, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { BusinessSearchComponent } from './../search/iCABSSBusinessSearch.component';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { InternalMaintenanceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'ICABSBPreparationMixGrid.html'
})



export class PreparationMixGridComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('prepBranchSearch') prepBranchSearch: BusinessSearchComponent;
    @ViewChild('prepMixPagination') prepMixPagination: PaginationComponent;

    private inputParams: any = {
        method: 'service-delivery/maintenance',
        module: 'preps',
        operation: 'Business/ICABSBPreparationMixGrid'
    };

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'PrepCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'PrepDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'menu' }
    ];

    public isBranchDisabled: boolean;
    public inputParamsBranch: any = {};
    public negBranchNumberSelected: Object = {
        id: '',
        text: ''
    };
    public search: QueryParams = new QueryParams();
    public itemsPerPage: number = 10;
    public pageCurrent: number = 1;
    public totalRecords: number = 1;
    public SelectedLine: any = {};

    //Lifecycles/Constructor methods definition
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPREPARATIONMIXGRID;
        this.browserTitle = this.pageTitle = 'Prep Mix';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    /**
     * This method used to initialize variables
     * @param void
     * @return void
     */
    public windowOnLoad(): void {
        if (this.isReturning()) {
            this.refresh();
        }
        this.setControlValue('menu', '');
        this.utils.getBusinessDesc(this.utils.getBusinessCode()).subscribe((data) => {
            this.negBranchNumberSelected = {
                id: this.utils.getBusinessCode(),
                text: this.utils.getBusinessCode() + ' - ' + data.BusinessDesc
            };
        });

        this.isBranchDisabled = true;
        this.setControlValue('PrepCode', this.riExchange.getParentHTMLValue('PrepCode'));
        this.setControlValue('PrepDesc', this.riExchange.getParentHTMLValue('PrepDesc'));

        this.riGrid.DefaultBorderColor = 'ADD8E6';
        this.riGrid.DefaultTextColor = '0000FF';
        this.riGrid.HighlightBar = true;
        this.riGrid.FunctionPaging = true;
        this.riGrid.PageSize = 10;
        this.riGrid.FunctionUpdateSupport = true;
        this.buildGrid();
    }

    /**
     * This method is used to configure grid(add columns/alignment setting etc)
     * @param void
     * @return void
     */
    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('PrepCodeGrd', 'PrepMix', 'PrepCodeGrd', MntConst.eTypeCode, 8, false, '');
        this.riGrid.AddColumn('PrepMixCodeGrd', 'PrepMix', 'PrepMixCodeGrd', MntConst.eTypeCode, 10, false, '');
        this.riGrid.AddColumn('PrepMixDescGrd', 'PrepMix', 'PrepMixDescGrd', MntConst.eTypeText, 40, false, '');
        this.riGrid.Complete();
    }

    /**
     * This method is called before execute grid
     * @param void
     * @return void
     */
    public riGridBeforeExecute(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('PrepCode', this.getControlValue('PrepCode'));
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        search.set(this.serviceConstants.GridCacheRefresh, 'True');
        search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageCurrent.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.Action, '2');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module,
            this.inputParams.operation, search)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    try {
                        this.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = true;
                        if (this.riGrid.Update) {
                            this.riGrid.StartRow = this.SelectedLine.Row;
                            this.riGrid.StartColumn = 0;
                            this.riGrid.RowID = this.SelectedLine.RowID;
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = false;
                        }
                        this.riGrid.Execute(data);
                        this.riGridAfterExecute();
                    } catch (e) {
                        this.logger.warn(e);
                    }
                }
            },
            (error) => {
                this.totalRecords = 1;
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    /**
     * This method is called after grid execute
     * @param void
     * @return void
     */
    public riGridAfterExecute(): void {
        if (!this.riGrid.Update) {
            if (!this.riGrid.HTMLGridBody.children(0)) {
                if (!this.riGrid.HTMLGridBody.children(0).children(0)) {
                    if (!this.riGrid.HTMLGridBody.children(0).children(0).children(0)) {
                        this.gridFocus(this.riGrid.HTMLGridBody.children(0).children(0).children(0));
                    }
                }
            }
        }
    }

    public gridFocus(rsrcElement: any): void {
        let oTR: any = rsrcElement;
        oTR.select();
        this.SelectedLine.RowID = oTR.RowID;
        this.SelectedLine.Row = oTR.parentElement.parentElement.sectionRowIndex;
        oTR.focus();
        oTR.select();
    }

    public onGridRowClick(data: any): void {
        if (this.riGrid.CurrentColumnName === 'PrepCodeGrd') {
            this.gridFocus(data.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
            this.navigate('PrepMixUpdate', InternalMaintenanceModuleRoutes.ICABSBPREPARATIONMIXMAINTENANCE, {
                RowID: this.riGrid.Details.GetAttribute('PrepCodeGrd', 'RowID')
            });
        }
    }

    /**
     * This method is called refresh button click
     * @param void
     * @return void
     */
    public refresh(): void {
        this.riGridBeforeExecute();
    }

    /**
     * This method is called pagination click
     * @param currentPage is the current page number
     * @return void
     */
    public getCurrentPage(currentPage: any): void {
        this.pageCurrent = currentPage.value;
        this.riGridBeforeExecute();
    }

    public menuOnchange(data: any): void {
        if (data.target.value === 'Add') {
            this.navigate('PrepMixAdd', InternalMaintenanceModuleRoutes.ICABSBPREPARATIONMIXMAINTENANCE, {
                PrepCode: this.getControlValue('PrepCode'),
                PrepDesc: this.getControlValue('PrepDesc')
            });
        }
    }
}
