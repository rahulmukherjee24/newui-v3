import { Component, ViewChild, OnInit, Injector, OnDestroy, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';


import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from '../../../shared/components/pagination/pagination';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { InternalGridSearchServiceModuleRoutes, InternalMaintenanceServiceModuleRoutes } from './../../../app/base/PageRoutes';
import { ContractSearchComponent } from '../search/iCABSAContractSearch';
import { PremiseSearchComponent } from '../search/iCABSAPremiseSearch';
import { ServiceCoverSearchComponent } from '../search/iCABSAServiceCoverSearch';
import { BranchServiceAreaSearchComponent } from '../search/iCABSBBranchServiceAreaSearch';
import { ProductSearchGridComponent } from '../search/iCABSBProductSearch';
import { ContractActionTypes } from '../../actions/contract';

@Component({
    templateUrl: 'iCABSSeServiceVisitEntryGrid.html'
})

export class ServiceVisitEntryGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    private queryParams: any = {
        operation: 'Service/iCABSSePlanVisitGrid',
        module: 'plan-visits',
        method: 'service-planning/maintenance'
    };
    private currentContractType: string = '';
    public pageId: string = '';
    public pageTitle: string = '';
    public controls: any = [
        { name: 'ContractNumber', readonly: true, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', readonly: true, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'PremiseName', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', readonly: true, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'ProductDesc', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', readonly: true, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', readonly: true, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaSeqNo', readonly: true, disabled: false, required: false, type: MntConst.eTypeInteger },
        { name: 'ServiceVisitFrequency', readonly: true, disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', type: MntConst.eTypeText }
    ];
    public setFocusOnContractNumber = new EventEmitter<boolean>();
    public gridConfig = {
        pageSize: 10,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };
    public serviceCoverSearchComponent: any;
    public ellipsisConfig: any = {
        contract: {
            isAutoOpen: false,
            isShowCloseButton: true,
            isShowHeader: true,
            isDisabled: false,
            childConfigParams: {
                parentMode: 'Search',
                ContractNumber: '',
                ContractName: '',
                isShowAddNew: false,
                currentContractType: ''
            },
            contentComponent: ContractSearchComponent
        },
        premise: {
            isAutoOpen: false,
            isShowCloseButton: true,
            isShowHeader: true,
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp',
                ContractNumber: '',
                ContractName: '',
                isShowAddNew: false
            },
            contentComponent: PremiseSearchComponent
        },
        serviceCoverSearchParams: {
            isDisabled: false,
            isShowHeader: true,
            isShowCloseButton: true,
            parentMode: 'LookUp',
            ContractNumber: '',
            PremiseNumber: '',
            ContractName: '',
            PremiseName: '',
            ProductCode: '',
            ProductDesc: '',
            isShowAddNew: false,
            serviceCoverComponent: Component
        },
        branchServiceArea: {
            isDisabled: false,
            isShowCloseButton: true,
            isShowHeader: true,
            parentMode: 'LookUp',
            ServiceBranchNumber: '',
            BranchName: '',
            isShowAddNew: false,
            component: BranchServiceAreaSearchComponent
        }
    };

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITENTRYGRID;
        this.browserTitle = this.pageTitle = 'Service Visit Entry';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        setTimeout(() => {
            this.setFocusOnContractNumber.emit(true);
        }, 0);
        this.setControlValue('BranchServiceAreaSeqNo', '0');
        let branchCode: string = this.utils.getBranchCode();
        this.setControlValue('BranchNumber', branchCode);
        let branchCodeWithDesc: string = this.utils.getBranchText(branchCode);
        let index: number = branchCodeWithDesc.indexOf('-');
        let branchDesc: string = branchCodeWithDesc.substring(index + 1);
        this.ellipsisConfig.branchServiceArea.BranchName = branchDesc;
        this.setControlValue('BranchName', branchDesc);
        if (this.riExchange.URLParameterContains('contract') || this.riExchange.URLParameterContains('job') || this.riExchange.URLParameterContains('product')) {
            this.currentContractType = '';
        }
        if (this.currentContractType === '') {
            this.ellipsisConfig.contract.childConfigParams.parentMode = 'LookUp-All';
        } else {
            this.ellipsisConfig.contract.childConfigParams.parentMode = 'Search';
        }
        this.triggerFetchSysChar();
        this.toggleServiceAndProductSearch();
    }

    private triggerFetchSysChar(): void {
        if (this.isReturning()) {
            this.gridConfig = this.pageParams;
            this.buildGrid();
            this.populateGrid();
        } else {
            let sysCharNumber = this.sysCharConstants.SystemCharShowWasteConsNumInVisitEntry;
            this.fetchSysChar(sysCharNumber).subscribe((data) => {
                if (data.records && data.records.length > 0) {
                }
                this.buildGrid();
            });
        }
    }

    private fetchSysChar(sysCharNumber: any): any {
        let querySysChar: QueryParams = this.getURLSearchParamObject();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumber);
        return this.httpService.sysCharRequest(querySysChar);
    }

    public buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionPaging = true;
        this.riGrid.AddColumn('ContractNumber', 'ServiceVisit', 'ContractNumber', MntConst.eTypeCode, 11, true);
        this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseNumber', 'ServiceVisit', 'PremiseNumber', MntConst.eTypeInteger, 5, true);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ProductCode', 'ServiceVisit', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceVisit', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceQuantity', 'ServiceVisit', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceCommenceDate', 'ServiceVisit', 'ServiceCommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceCommenceDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceVisitAnnivDate', 'ServiceVisit', 'ServiceVisitAnnivDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('ServiceVisitAnnivDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PortfolioStatusDesc', 'ServiceVisit', 'PortfolioStatusDesc', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PortfolioStatusDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('BranchServiceAreaCode', 'ServiceVisit', 'BranchServiceAreaCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('EmployeeCode', 'ServiceVisit', 'EmployeeCode', MntConst.eTypeCode, 6);
        this.riGrid.AddColumnAlign('EmployeeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceVisit', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ViewServiceVisit', 'ServiceVisit', 'ViewServiceVisit', MntConst.eTypeImage, 1, true);
        this.riGrid.AddColumnAlign('ViewServiceVisit', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('AddServiceVisit', 'ServiceVisit', 'AddServiceVisit', MntConst.eTypeImage, 1, true);
        this.riGrid.AddColumnAlign('AddServiceVisit', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ContractNumber', true);
        this.riGrid.AddColumnOrderable('PremiseNumber', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaCode', true);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        let gridSearch: QueryParams = this.getURLSearchParamObject();
        gridSearch.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        gridSearch.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        gridSearch.set(this.serviceConstants.ProductCode, this.getControlValue('ProductCode'));
        gridSearch.set('BranchNumber', this.getControlValue('BranchNumber'));
        gridSearch.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        gridSearch.set('BranchServiceAreaSeqNo', this.getControlValue('BranchServiceAreaSeqNo'));

        gridSearch.set(this.serviceConstants.GridMode, '0');
        gridSearch.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());
        gridSearch.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
        gridSearch.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
        gridSearch.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        gridSearch.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());

        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch)
            .subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.riGrid.RefreshRequired();
                        this.riGridPagination.currentPage = this.gridConfig.currentPage = e.pageData ? e.pageData.pageNumber : 1;
                        this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.gridConfig.pageSize : 1;
                        this.riGrid.Execute(e);
                        this.ref.detectChanges();
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public onRiGridRefresh(): void {
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh();
    }

    public serviceCoverFocus(rsrcElement: any): void {
        rsrcElement.focus();
        this.setAttribute('ContractRowID', this.riGrid.Details.GetAttribute('ContractNumber', 'RowID'));
        this.setAttribute('PremiseRowID', this.riGrid.Details.GetAttribute('PremiseNumber', 'RowID'));
        this.setAttribute('ServiceCoverRowID', this.riGrid.Details.GetAttribute('ProductCode', 'RowID'));
        this.setAttribute('Row', this.riGrid.CurrentRow);
        let strContractNumber: string = this.riGrid.Details.GetValue('ContractNumber');
        strContractNumber = this.utils.mid(strContractNumber, 3);
        this.setAttribute('ContractNumber', strContractNumber);
        this.setAttribute('ContractName', this.riGrid.Details.GetAttribute('ContractNumber', 'AdditionalProperty'));
        this.setAttribute('PremiseNumber', this.riGrid.Details.GetValue('PremiseNumber'));
        this.setAttribute('PremiseName', this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty'));
        this.setAttribute('ProductCode', this.riGrid.Details.GetValue('ProductCode'));
        this.setAttribute('ProductDesc', this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty'));
        this.currentContractType = this.riGrid.Details.GetAttribute('ServiceVisitFrequency', 'AdditionalProperty');
    }

    public riGridBodyOnDblClick(event: any): void {
        this.pageParams = this.gridConfig;
        this.serviceCoverFocus(event.srcElement.parentElement);
        switch (this.riGrid.CurrentColumnName) {
            case 'ContractNumber':
                let url = this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                switch (this.currentContractType) {
                    case 'P':
                        url = this.ContractManagementModuleRoutes.ICABSAPRODUCTSALEMAINTENANCE;
                        break;
                    case 'J':
                        url = this.ContractManagementModuleRoutes.ICABSAJOBMAINTENANCE;
                        break;
                    case 'C':
                        url = this.ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE;
                        break;
                }
                this.navigate('ServiceVisitEntryGrid', url,
                    {
                        ContractNumber: this.getAttribute('ContractNumber')
                    });
                break;
            case 'PremiseNumber':
                this.navigate('ServiceVisitEntryGrid', this.ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE,
                    {
                        contracttypecode: this.currentContractType,
                        PremiseRowID: this.getAttribute('PremiseRowID')
                    });
                break;
            case 'ProductCode':
                this.navigate('ServiceVisitEntryGrid', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE,
                    {
                        currentContractType: this.currentContractType
                    });
                break;
            case 'ViewServiceVisit':
                this.store.dispatch({
                    type: ContractActionTypes.SAVE_DATA,
                    payload: {
                        currentContractType: this.currentContractType,
                        ContractNumber: this.getAttribute('ContractNumber'),
                        ContractName: this.getAttribute('ContractName'),
                        PremiseNumber: this.getAttribute('PremiseNumber'),
                        PremiseName: this.getAttribute('PremiseName'),
                        ProductCode: this.getAttribute('ProductCode'),
                        ProductDesc: this.getAttribute('ProductDesc'),
                        ServiceCoverRowID: this.getAttribute('ServiceCoverRowID')
                    }
                });
                this.navigate('ServiceVisitEntryGrid', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITSUMMARY);
                break;
            case 'AddServiceVisit':
                this.navigate('ServiceVisitEntryGrid', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                    currentContractType: this.currentContractType,
                    ContractNumber: this.getAttribute('ContractNumber'),
                    ContractName: this.getAttribute('ContractName'),
                    PremiseNumber: this.getAttribute('PremiseNumber'),
                    PremiseName: this.getAttribute('PremiseName'),
                    ProductCode: this.getAttribute('ProductCode'),
                    ProductDesc: this.getAttribute('ProductDesc'),
                    ServiceCoverRowID: this.getAttribute('ServiceCoverRowID')
                });
                break;
        }
    }

    public onContractDataReceived(data: any): void {
        if (data) {
            this.setControlValue('ContractNumber', data.ContractNumber);
            this.setControlValue('ContractName', data.ContractName);
            this.currentContractType = data.ContractTypePrefix;
            this.ellipsisConfig.premise.childConfigParams.ContractNumber = data.ContractNumber;
            this.ellipsisConfig.premise.childConfigParams.ContractName = data.ContractName;
            this.toggleServiceAndProductSearch();
        }
    }

    public onPremiseDataReceived(data: any): void {
        if (data) {
            this.setControlValue('PremiseNumber', data.PremiseNumber);
            this.setControlValue('PremiseName', data.PremiseName);
            this.toggleServiceAndProductSearch();
        }
    }

    public onProductDataReceived(data: any): void {
        if (data && this.ellipsisConfig.serviceCoverSearchParams.parentMode === 'LookUp-Freq') {
            this.setControlValue('ProductCode', data.row.ProductCode);
            this.setControlValue('ProductDesc', data.row.ProductDesc);
            this.setControlValue('ServiceVisitFrequency', data.row.ServiceVisitFrequency);
            this.toggleServiceAndProductSearch();
        } else {
            this.setControlValue('ProductCode', data.ProductCode);
            this.setControlValue('ProductDesc', data.ProductDesc);
        }
    }

    public onServiceAreaDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
        }
    }

    private toggleServiceAndProductSearch(): void {
        if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            this.ellipsisConfig.serviceCoverSearchParams.parentMode = 'LookUp-Freq';
            this.ellipsisConfig.serviceCoverSearchParams.ContractNumber = this.getControlValue('ContractNumber');
            this.ellipsisConfig.serviceCoverSearchParams.ContractName = this.getControlValue('ContractName');
            this.ellipsisConfig.serviceCoverSearchParams.PremiseNumber = this.getControlValue('PremiseNumber');
            this.ellipsisConfig.serviceCoverSearchParams.PremiseName = this.getControlValue('PremiseName');
            this.ellipsisConfig.serviceCoverSearchParams.ProductCode = this.getControlValue('ProductCode');
            this.ellipsisConfig.serviceCoverSearchParams.ProductDesc = this.getControlValue('ProductDesc');
            this.serviceCoverSearchComponent = ServiceCoverSearchComponent;
        } else {
            this.ellipsisConfig.serviceCoverSearchParams.parentMode = 'LookUp';
            this.serviceCoverSearchComponent = ProductSearchGridComponent;
        }
    }

    public onPopulateDescriptions(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'SetDisplayFields';
        formData['ContractTypeCode'] = this.currentContractType;
        if (this.getControlValue('BranchNumber')) {
            formData['BranchNumber'] = this.getControlValue('BranchNumber');
        }
        if (this.getControlValue('BranchServiceAreaCode')) {
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        }
        if (this.getControlValue('ContractNumber')) {
            formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
            this.ellipsisConfig.premise.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
        } else {
            this.ellipsisConfig.premise.childConfigParams.ContractNumber = '';
        }
        if (this.getControlValue(this.serviceConstants.PremiseNumber)) {
            formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
        }
        if (this.getControlValue(this.serviceConstants.ProductCode)) {
            formData['ProductCode'] = this.getControlValue('ProductCode');
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.setControlValue('BranchName', e.BranchName);
                    this.setControlValue('BranchServiceAreaDesc', e.BranchServiceAreaDesc);
                    this.setControlValue('ContractName', e.ContractName);
                    this.ellipsisConfig.premise.childConfigParams.ContractName = e.ContractName;
                    this.setControlValue('PremiseName', e.PremiseName);
                    this.setControlValue('ProductDesc', e.ProductDesc);
                    if (!e.BranchName) {
                        this.setControlValue('BranchNumber', '');
                    }
                    if (!e.BranchServiceAreaDesc) {
                        this.setControlValue('BranchServiceAreaCode', '');
                    }
                    if (!e.ContractName) {
                        this.setControlValue('ContractNumber', '');
                    }
                    if (!e.PremiseName) {
                        this.setControlValue('PremiseNumber', '');
                    }
                    if (!e.ProductDesc) {
                        this.setControlValue('ProductCode', '');
                    }
                    this.toggleServiceAndProductSearch();
                    this.getServiceVisitFrequencyData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public getServiceVisitFrequencyData(): void {
        let lookupIP = [
            {
                'table': 'ServiceCover',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'ProductCode': this.getControlValue('ProductCode')
                },
                'fields': ['ServiceVisitFrequency']
            }
        ];

        let queryLookUp: QueryParams = this.getURLSearchParamObject();
        queryLookUp.set(this.serviceConstants.Action, '0');
        queryLookUp.set(this.serviceConstants.MaxResults, '100');
        this.httpService.lookUpRequest(queryLookUp, lookupIP).subscribe((res) => {
            let data = res.results[0][0];
            if (!data || data.ServiceVisitFrequency === '0') {
                this.setControlValue('ServiceVisitFrequency', '');
            } else {
                this.setControlValue('ServiceVisitFrequency', data.ServiceVisitFrequency);
            }
        });
    }
}

