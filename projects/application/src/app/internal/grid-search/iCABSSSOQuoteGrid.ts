
import { Component, OnInit, Injector, ViewChild, OnDestroy, Renderer } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs/Subscription';

import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { InternalGridSearchSalesModuleRoutes, InternalMaintenanceSalesModuleRoutes } from './../../base/PageRoutes';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { SOQuoteReviewReportComponent } from '../maintenance/iCABSSOQuoteReviewReport.component';

@Component({
    templateUrl: 'iCABSSSOQuoteGrid.html'
})

export class QuoteGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('quotePagination') quotePagination: PaginationComponent;

    private muleConfig: any = {
        operation: 'Sales/iCABSSSOQuoteGrid',
        module: 'advantage',
        method: 'prospect-to-contract/maintenance'
    };
    private subscriptionManager: Subscription;
    public itemsPerPage: number;
    public pageSize: number;
    public gridCurPage: number;
    public totalRecords: number = 0;
    public displayProspectMessage = true;
    public pageId: string = '';


    public controls: Array<any> = [
        { name: 'ProspectNumber', readonly: true, disabled: false, required: false },
        { name: 'ProspectName', readonly: true, disabled: false, required: false },
        { name: 'menu', value: '', readonly: false, disabled: false, required: false },
        { name: 'dlBatchRef' },
        { name: 'dlContractRef' },
        { name: 'ContractTypeCode' },
        { name: 'PaymentInfoRequired' },
        { name: 'SubSystem' },
        { name: 'QuoteNumber' }
    ];

    private refSOQuoteReviewReportComponent: SOQuoteReviewReportComponent;

    constructor(
        private renderer: Renderer,
        injector: Injector
    ) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSSOQUOTEGRID;
        this.browserTitle = this.pageTitle = 'Advantage Quotes';
        this.subscriptionManager = new Subscription();
        this.refSOQuoteReviewReportComponent = new SOQuoteReviewReportComponent(this.utils, this.httpService, this.serviceConstants, this.modalAdvService);
    }


    ngOnInit(): void {
        super.ngOnInit();
        this.itemsPerPage = 10;
        this.gridCurPage = 1;
        this.pageSize = 10;
        this.riExchange.riInputElement.Disable(this.uiForm, 'ProspectNumber');
        this.riExchange.riInputElement.Disable(this.uiForm, 'ProspectName');

        this.riExchange.riInputElement.SetValue(this.uiForm, 'ProspectNumber', this.riExchange.getParentHTMLValue('ProspectNumber'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ProspectName', this.riExchange.getParentHTMLValue('ProspectName'));
        let subSystem = this.riExchange.getParentHTMLValue('SubSystem');
        this.setControlValue('menu', '');
        this.buildGrid();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subscriptionManager) {
            this.subscriptionManager.unsubscribe();
        }
    }

    public menuOnchange(val: string): void {
        if (this.riGrid && !this.riGrid.CurrentRow) {
            if (this.riGrid.bodyArray.length > 0) {
                this.riGrid.SetDefaultFocus();
                this.riGrid.Details.Focus('QuoteNumber');
                this.sOQuoteFocus();
            }
        }

        switch (val) {
            case 'AddContract':
            case 'AddJob':
                this.addQuote();
                break;
            case 'CopyToContract':
            case 'CopyToJob':
                this.copyQuote();
                break;
            case 'CustomerQuote':
                this.cmdCustomerQuote_onclick();
                break;
            case 'History':
                this.cmdHistory_onclick();
                break;
        }

        this.setControlValue('menu', '');
    }

    public buildGrid(): any {
        this.riGrid.Clear();
        this.riGrid.AddColumn('QuoteNumber', 'SOQuote', 'QuoteNumber', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('ContractTypeCode', 'SOQuote', 'ContractTypeCode', MntConst.eTypeCode, 1);
        this.riGrid.AddColumn('CreatedDate', 'SOQuote', 'CreatedDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('StatusDesc', 'SOQuote', 'StatusDesc', MntConst.eTypeTextFree, 10);
        this.riGrid.AddColumn('NumPremises', 'SOQuote', 'NumPremises', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('NumServiceCovers', 'SOQuote', 'NumServiceCovers', MntConst.eTypeInteger, 4);
        this.riGrid.AddColumn('Submit', 'SOQuote', 'Submit', MntConst.eTypeButton, 4);
        this.riGrid.AddColumn('Print', 'SOQuote', 'Print', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('Info', 'dlHistory', 'Info', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('QuoteNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('ContractTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('CreatedDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('NumPremises', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnAlign('NumServiceCovers', MntConst.eAlignmentCenter);

        this.riGrid.AddColumnOrderable('QuoteNumber', true);
        this.riGrid.AddColumnOrderable('CreatedDate', true);

        this.riGrid.Complete();
        this.riGridBeforeExecute();
    }

    public riGridBeforeExecute(): void {
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateFooter = true;
        this.riGrid.UpdateHeader = true;

        let gridHandle = (Math.floor(Math.random() * 900000) + 100000).toString();
        let gridQueryParams: QueryParams = new QueryParams();

        let strGridData = true;
        gridQueryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        gridQueryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        gridQueryParams.set(this.serviceConstants.GridPageSize, this.pageSize.toString());
        gridQueryParams.set(this.serviceConstants.Action, '2');

        gridQueryParams.set('ProspectNumber', this.riExchange.riInputElement.GetValue(this.uiForm, 'ProspectNumber'));
        gridQueryParams.set('LanguageCode', this.riExchange.LanguageCode());
        let sortOrder = 'Descending';
        if (!this.riGrid.DescendingSort) {
            sortOrder = 'Ascending';
        }
        gridQueryParams.set('riSortOrder', sortOrder);
        gridQueryParams.set('riCacheRefresh', 'true');
        gridQueryParams.set('riGridMode', '0');
        gridQueryParams.set('riGridHandle', gridHandle);
        gridQueryParams.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        gridQueryParams.set(this.serviceConstants.PageCurrent, this.gridCurPage.toString());

        this.ajaxSource.next(this.ajaxconstant.START);
        let httpService: Subscription = this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, gridQueryParams).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.gridCurPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(httpService);
    }

    public sOQuoteFocus(): any {
        this.attributes.dlContractRowID = this.riGrid.Details.GetAttribute('QuoteNumber', 'RowID');
        this.attributes.ContractTypeCode = this.riGrid.Details.GetValue('ContractTypeCode');
        this.attributes.ProspectName = this.riExchange.riInputElement.GetValue(this.uiForm, 'ProspectName');
        this.attributes.QuoteNumber = this.riGrid.Details.GetValue('QuoteNumber');
        this.riExchange.riInputElement.SetValue(this.uiForm, 'dlContractRef', this.riGrid.Details.GetAttribute('QuoteNumber', 'AdditionalProperty'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'dlBatchRef', this.riGrid.Details.GetAttribute('ContractTypeCode', 'AdditionalProperty'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'PaymentInfoRequired', this.riGrid.Details.GetAttribute('CreatedDate', 'AdditionalProperty'));
        this.riExchange.riInputElement.SetValue(this.uiForm, 'ContractTypeCode', this.riGrid.Details.GetValue('ContractTypeCode'));
        this.riGrid.Details.Focus('QuoteNumber');
    }

    public riGridAfterExecute(): any {
        //' RG - Show a message if no records exist within the grid - first time in
        if (this.displayProspectMessage === true) {
            this.displayProspectMessage = false;
            //' The following returns '0' when no records present
            if (this.riGrid.bodyArray.length === 0) {
                let modalVo: ICabsModalVO = new ICabsModalVO(MessageConstant.PageSpecificMessage.Please_ensure_that_you_have_verified_the_prospect_details_before_proceeding);
                modalVo.title = MessageConstant.Message.Information;
                this.modalAdvService.emitMessage(modalVo);
            }
        }

        setTimeout(() => {
            if (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children[0] && this.riGrid.bodyArray.length > 1) {
                if (this.riGrid.HTMLGridBody.children[0].children[0]) {
                    this.riGrid.SetDefaultFocus();
                    this.sOQuoteFocus();
                }
            }
        }, 0);
    }

    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public updateColumnData(ev: Event): void {
        // TODO:
    }

    public getCurrentPage(currentPage: any): void {
        this.gridCurPage = currentPage.value;
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }

    public cmdPremisesOnclick(): any {
        //Navigate to screen Sales/iCABSSSOPremiseGrid.htm" with mode 'Quote'
        this.navigate('Quote', InternalGridSearchSalesModuleRoutes.ICABSSSOPREMISEGRID);
    }

    public cmdCustomerQuote_onclick(): any {
        let urlPostData: Object = {
            Function: 'Single',
            dlContractRowID: this.attributes['dlContractRowID'] ? this.attributes['dlContractRowID'] : '',
            LanguageCode: this.riExchange.LanguageCode(),
            Mode: 'Quote'
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        let httpService: Subscription = this.fetchRecordData('', urlPostData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data.url) {
                        window.open(data.url, '_blank');
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(httpService);
    }

    public cmdHistory_onclick(): any {
        //Navigate to screen Sales/iCABSSdlHistoryGrid.htm with Mode = "SOQuote",
        this.navigate('SOQuote', InternalGridSearchSalesModuleRoutes.ICABSSDLHISTORYGRID);
    }

    public addQuote(): any {
        let option = this.riExchange.riInputElement.GetValue(this.uiForm, 'menu');
        if (option === 'AddContract') {
            this.attributes.ContractTypeCode = 'C';
        } else {
            this.attributes.ContractTypeCode = 'J';
        }

        let functionName = option;

        let postDataAdd: Object = {
            Function: functionName,
            NegBranchNumber: this.utils.getBranchCode(),
            ProspectNumber: this.riExchange.riInputElement.GetValue(this.uiForm, 'ProspectNumber')
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        let httpService: Subscription = this.fetchRecordData(functionName, {}, postDataAdd).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data) {
                        this.attributes.dlContractRowID = data['dlContractRowID'];
                        this.attributes.dlPremiseRowID = data['dlPremiseRowID'];
                        this.attributes.ContractSearchPostCode = data['ContractSearchPostCode'];
                        //Navigate to screen Sales/iCABSSdlContractMaintenance.htm' with mode 'AddQuote'
                        this.navigate('AddQuote', InternalMaintenanceSalesModuleRoutes.ICABSSDLCONTRACTMAINTENANCE, { dlContractRowID: this.attributes.dlContractRowID });
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(httpService);
    }

    public copyQuote(): any {
        let functionName = this.riExchange.riInputElement.GetValue(this.uiForm, 'menu');
        if (functionName === 'AddContract') {
            this.attributes.ContractTypeCode = 'C';
        } else {
            this.attributes.ContractTypeCode = 'J';
        }

        let postDataAdd: Object = {
            Function: functionName,
            NegBranchNumber: this.utils.getBranchCode(),
            ProspectNumber: this.riExchange.riInputElement.GetValue(this.uiForm, 'ProspectNumber'),
            QuoteNumber: this.riGrid.Details.GetValue('QuoteNumber')
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        let httpService: Subscription = this.fetchRecordData(functionName, {}, postDataAdd).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            }
            else {
                this.refresh();
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        this.subscriptionManager.add(httpService);
    }

    public riGridBodyOnClick(ev: any): any {
        switch (this.riGrid.CurrentColumnName) {
            case 'Submit':
                this.sOQuoteFocus();
                //' RG 30/04/2008 - If no payment information is required then submit the quote now
                if (this.riExchange.riInputElement.GetValue(this.uiForm, 'PaymentInfoRequired') === '0') {
                    let postDataAdd: Object = {
                        Function: 'Submit',
                        ProspectNumber: this.riExchange.riInputElement.GetValue(this.uiForm, 'ProspectNumber'),
                        QuoteNumber: this.riGrid.Details.GetValue('QuoteNumber')
                    };

                    this.ajaxSource.next(this.ajaxconstant.START);
                    let submitSub: Subscription = this.fetchRecordData('Submit', {}, postDataAdd).subscribe(
                        (data) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            if (data.hasError) {
                                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            }
                            else {
                                if (data.ReturnMessage) { this.modalAdvService.emitMessage(new ICabsModalVO(data.ReturnMessage)); }
                                this.refresh();
                            }
                        },
                        (error) => {
                            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                            this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                        });
                    this.subscriptionManager.add(submitSub);
                }
                else {
                    //'TODO:: richy
                    //' RG 30/04/2008 - Payment details are required
                    //  WindowPath='/wsscripts/riHTMLWrapper.p?riFileName=Sales/iCABSSSOQuoteSubmitMaintenance.htm<bottom>'
                    //  riExchange.Mode = 'SOQuote' : window.location = WindowPath
                    this.navigate('SOQuote', InternalMaintenanceSalesModuleRoutes.ICABSSSOQUOTESUBMITMAINTENANCE, {
                        ProspectNumber: this.attributes.ProspectNumber,
                        QuoteNumber: this.attributes.QuoteNumber,
                        dlContractRowID: this.attributes.dlContractRowID
                    });
                }
                break;
            case 'Print':
                this.sOQuoteFocus();
                //' Do not allow print if local offline system
                if ((ev.srcElement && ev.srcElement.tagName === 'IMG') && ev.srcElement.getAttribute('src')) {
                    this.refSOQuoteReviewReportComponent.loadPrintContent('', this.attributes['dlContractRowID']);
                }
                break;
            case 'Info':
                if ((ev.srcElement && ev.srcElement.tagName === 'IMG') && ev.srcElement.getAttribute('src')) {  //' hand
                    let msg = this.riGrid.Details.GetAttribute('Info', 'AdditionalProperty') || '';
                    if (msg) {
                        this.sOQuoteFocus();
                        let modalVo: ICabsModalVO = new ICabsModalVO(msg);
                        modalVo.title = MessageConstant.Message.Information;
                        this.modalAdvService.emitMessage(modalVo);
                    }
                }
        }

    }

    public riGridBodyOnDblClick(ev: any): any {
        this.sOQuoteFocus();
        switch (this.riGrid.CurrentColumnName) {
            case 'QuoteNumber':
                //Navigate to screen Sales/iCABSSdlContractMaintenance.htm' with Mode = 'SOQuote'
                this.navigate('SOQuote', InternalMaintenanceSalesModuleRoutes.ICABSSDLCONTRACTMAINTENANCE, {
                    dlContractRowID: this.attributes.dlContractRowID
                });
                break;
            case 'NumPremises':
                this.cmdPremisesOnclick();
                break;
            case 'NumServiceCovers':
                this.attributes['dlPremiseRef'] = 'All';
                //Navigate to screen Sales/iCABSSSOServiceCoverGrid.htm' with mode 'SOQuote'
                this.navigate('SOQuote', InternalGridSearchSalesModuleRoutes.ICABSSSOSERVICECOVERGRID);
                break;
            case 'StatusDesc':
                // Navigate to screen  Sales/iCABSSSOQuoteStatusMaintenance.htm' with Mode = 'SOQuote'
                this.navigate('SOQuote', InternalMaintenanceSalesModuleRoutes.ICABSSSOQUOTESTATUSMAINTENANCE,
                    { ReportParams: this.attributes.dlContractRowID, QuoteNumber: this.riGrid.Details.GetValue('QuoteNumber') });
                break;
        }

    }

    public riGridBodyOnKeyDown(ev: any): any {
        switch (ev.KeyCode) {
            case 38: //'Up Arror
                if (ev.srcElement.parentElement.parentElement.previousSibling) {
                    this.sOQuoteFocus();
                }
                break;
            case 40:
            case 9: //'Down Arror Or Tab
                if (ev.srcElement.parentElement.parentElement.NextSibling) {
                    this.sOQuoteFocus();
                }
                break;
        }
    }

    public fetchRecordData(functionName: any, params: any, postdata?: any): any {
        let queryParams = new QueryParams();
        queryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        queryParams.set(this.serviceConstants.Action, '0');

        if (functionName !== '') {
            queryParams.set(this.serviceConstants.Action, '6');
        }
        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                queryParams.set(key, params[key]);
            }
        }

        if (postdata) {
            return this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams, postdata);
        }
        else {
            return this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryParams);
        }
    }

}
