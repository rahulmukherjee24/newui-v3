import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PaginationComponent } from './../../../shared/components/pagination/pagination';
import { BaseComponent } from '../../../app/base/BaseComponent';
import { AjaxConstant } from './../../../shared/constants/AjaxConstants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PremiseLocationSearchComponent } from '../../internal/search/iCABSAPremiseLocationSearch.component';
import { RefreshComponent } from './../../../shared/components/refresh/refresh';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { InternalGridSearchServiceModuleRoutes } from './../../base/PageRoutes';
import { MessageConstant } from './../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSAServiceVisitDisplayGrid.html',
    styles: [` :host /deep/ .gridtable tbody tr td:nth-child(8){
        width:8%;
    }
    :host /deep/ .gridtable tbody tr td:nth-child(8) icabs-grid-cell>input{
        width:60%;
        float:left;
    }
  `]
})

// Class definition starts here
export class ICABSAServiceVisitDisplayGridComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('serviceVisitDisplayGridPagination') serviceVisitDisplayGridPagination: PaginationComponent;
    @ViewChild('refreshBtn') public refreshBtn: RefreshComponent;

    private queryParams: any = {
        operation: 'Application/iCABSAServiceVisitDisplayGrid',
        module: 'contract-admin',
        method: 'contract-management/maintenance'
    };
    private gridOperation: any = {
        vbUpdateRecord: '',
        vbPremiseLocationNumber: '',
        vbGridMode: '',
        vbAcceptDisplay: '',
        Update: false
    };
    private riCacheRefresh: boolean = false;

    public pageId: string = '';
    public tableTitle: string = '';
    public itemsPerPage: number = 10;
    public currentPage: number = 1;
    public totalItems: number = 0;
    public premiseLocationEllipsis: any = {
        disabled: false,
        showHeader: true,
        showCloseButton: true,
        childConfigParams: {
            'parentMode': 'DisplayGrid'
        },
        component: PremiseLocationSearchComponent
    };
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };
    public controls: Array<Object> = [
        { name: 'ContractNumber', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ProductCode', disabled: true, required: false, value: '', type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'TotalQty', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'TotalValue', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'TotalWEDValue', disabled: true, required: false, type: MntConst.eTypeDecimal1 },
        { name: 'ServiceCoverNumber', disabled: true, required: false, type: MntConst.eTypeInteger },
        { name: 'ServiceCoverItemNumber', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'riGridHandle', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServiceVisitRowID', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServiceCoverMode', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'EffectiveDate', disabled: true, required: false, type: MntConst.eTypeDate },
        { name: 'SelPremiseLocationNumber', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'SelPremiseLocationDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'ServiceCommenceDate', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AccountNumber', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'ServiceBranchNumber', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'NegBranchNumber', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'RequiresManualVisitPlanningInd', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'AnnualCalendarInd', disabled: true, required: false, type: MntConst.eTypeCheckBox },
        { name: 'EmployeeLimitChildDrillOptions', disabled: true, required: false },
        { name: 'VisitTypeCode', disabled: false, required: false, type: MntConst.eTypeText },
        { name: 'PlanVisitNumber', disabled: false, required: false },
        { name: 'ServiceCoverROWID', disabled: false, required: false }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSASERVICEVISITDISPLAYGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Service Cover Displays';
        this.tableTitle = 'Service Cover Display Maintenance';
        this.initData();
    }

    ngAfterViewInit(): void {
        this.utils.setTitle(this.pageTitle);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * This method is used to initialize all parameters during page load
     * @param void
     * @return void
     */
    private initData(): void {
        this.pageParams.CurrentContractType = this.riExchange.getCurrentContractType();
        this.pageParams.CurrentContractTypeLabel = this.riExchange.getCurrentContractTypeLabel();
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
        this.setControlValue('ServiceBranchNumber', this.riExchange.getParentHTMLValue('ServiceBranchNumber'));
        this.setControlValue('NegBranchNumber', this.riExchange.getParentHTMLValue('NegBranchNumber'));
        this.setControlValue('EmployeeLimitChildDrillOptions', this.riExchange.getParentHTMLValue('EmployeeLimitChildDrillOptions'));
        this.setControlValue('VisitTypeCode', this.riExchange.getParentHTMLValue('VisitTypeCode'));
        this.setControlValue('PlanVisitNumber', this.riExchange.getParentHTMLValue('PlanVisitNumber'));
        this.riCacheRefresh = false;
        if (this.riExchange.getParentHTMLValue('riCacheRefresh')) {
            this.riCacheRefresh = true;
        }
        this.setControlValue('riGridHandle', this.riExchange.getParentHTMLValue('riGridHandle'));
        this.setControlValue('BusinessCode', this.businessCode());
        this.setControlValue('ServiceVisitRowID', this.riExchange.getParentHTMLValue('ServiceVisitRowID'));
        this.setControlValue('ServiceCoverROWID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
        this.riGrid.FunctionUpdateSupport = true;
        this.setControlValue('ServiceCoverMode', 'ServiceVisit');
        this.lookupData();
    }

    /**
     * This method is used to configure grid before execute
     * @param void
     * @return void
     */
    private beforeExecute(): void {
        this.riGrid.Clear();
        this.gridOperation.update = false;
        this.riGrid.AddColumn('ItemDescription', 'Display', 'ItemDescription', MntConst.eTypeCode, 20);
        this.riGrid.AddColumn('Component1', 'Display', 'Component1', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('Component2', 'Display', 'Component2', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('Component3', 'Display', 'Component3', MntConst.eTypeText, 20);
        this.riGrid.AddColumn('CommenceDate', 'Display', 'CommenceDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('WEDValue', 'Display', 'WEDValue', MntConst.eTypeDecimal1, 6);
        this.riGrid.AddColumn('AnnualValue', 'Display', 'AnnualValue', MntConst.eTypeCurrency, 6);
        this.riGrid.AddColumn('PremiseLocationNumber', 'Display', 'PremiseLocationNumber', MntConst.eTypeEllipsis, 4);
        this.riGrid.AddColumn('PremiseLocationDesc', 'Display', 'PremiseLocationDesc', MntConst.eTypeText, 30);
        this.riGrid.AddColumn('RemovalDate', 'Display', 'RemovalDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumn('Accept', 'Display', 'Accept', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('AnnualValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('WEDValue', MntConst.eAlignmentRight);
        this.riGrid.AddColumnAlign('PremiseLocationNumber', MntConst.eAlignmentRight);
        //CR Code
        /*if(this.riGrid.FunctionUpdateSupport){
        this.riGrid.AddColumnUpdateSupport('PremiseLocationNumber', true);
        this.riGrid.AddEllipsisControl('PremiseLocationNumber', this.premiseLocationEllipsis, 'PremiseLocationNumber');
        }else{
            this.premiseLocationEllipsis.disabled=true;
             this.riGrid.AddColumnUpdateSupport('PremiseLocationNumber', false);
        this.riGrid.AddEllipsisControl('PremiseLocationNumber', this.premiseLocationEllipsis, 'PremiseLocationNumber');
        }*/
        this.riGrid.AddColumnUpdateSupport('PremiseLocationNumber', true);
        this.riGrid.AddEllipsisControl('PremiseLocationNumber', this.premiseLocationEllipsis, 'PremiseLocationNumber');
        this.riGrid.Complete();
    }

    /**
     * This method to populate data in specific controls after grid execute
     * @param data build data from service call
     * @return void
     */
    private afterExecute(data: any): void {
        setTimeout(() => {
            // if (data && data.body.cells.length > 0) {
            //     this.setControlValue('riGridHandle', this.riGrid.Details.GetAttribute('Component1', 'AdditionalProperty'));
            // }
            if (this.gridOperation.update === false && this.gridOperation.vbAcceptDisplay === '') {
                if (data) {
                    let totalInfo: Array<any> = data.footer.rows[0].text.split('|');
                    this.setControlValue('TotalQty', totalInfo[1]);
                    this.setControlValue('TotalValue', totalInfo[2]);
                    this.setControlValue('TotalWEDValue', totalInfo[3]);
                } else {
                    this.setControlValue('TotalQty', '0');
                    this.setControlValue('TotalValue', '0');
                    this.setControlValue('TotalWEDValue', '0');
                }
            }
            this.gridOperation.update = false;
            this.gridOperation.vbAcceptDisplay = '';
            this.riGrid.Update = false;
            this.riGrid.Mode = MntConst.eModeNormal;
        }, 100);
    }

    /**
     * This method is used to execute grid
     * @param void
     * @return void
     */
    private buildGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.getControlValue('ProductCode'));
        search.set('ServiceCoverNumber', this.getControlValue('ServiceCoverNumber'));
        search.set('ServiceCoverMode', this.getControlValue('ServiceCoverMode'));
        search.set('ServiceVisitRowID', this.getControlValue('ServiceVisitRowID'));
        search.set('VisitTypeCode', this.getControlValue('VisitTypeCode'));
        search.set('PlanVisitNumber', this.getControlValue('PlanVisitNumber'));
        search.set('AcceptDisplay', this.gridOperation.vbAcceptDisplay);
        if (this.gridOperation.vbAcceptDisplay) {
            search.set('ROWID', this.attributes['ServiceCoverItemRowID']);
        }
        if (this.riCacheRefresh) {
            search.set('riCacheRefresh', 'True');
            this.riCacheRefresh = false;
        }
        search.set('GridType', 'Main');
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.getControlValue('riGridHandle'));
        search.set(this.serviceConstants.PageSize, this.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.currentPage.toString());
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search)
            .subscribe(
            (data) => {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    try {
                        if (!this.gridOperation.vbAcceptDisplay) {
                            this.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                            this.totalItems = data.pageData ? data.pageData.lastPageNumber * this.itemsPerPage : 1;
                        }
                        if (this.riGrid.Update) {
                            if (this.hasValue(this.getAttribute('Row'))) {
                                this.riGrid.StartRow = this.getAttribute('Row');
                                this.riGrid.StartColumn = 0;
                                this.riGrid.RowID = this.getAttribute('ServiceCoverItemRowID');
                            }
                        }
                        this.riGrid.UpdateHeader = true;
                        this.riGrid.UpdateBody = true;
                        this.riGrid.UpdateFooter = false;
                        this.riGrid.Execute(data);
                        this.afterExecute(data);
                    } catch (e) {
                        this.logger.warn(e);
                    }
                }
                this.ajaxSource.next(AjaxConstant.COMPLETE);

            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                this.ajaxSource.next(AjaxConstant.COMPLETE);

            }
            );
    }

    /**
     * This method to update premise location
     * @param data new data from grid input
     * @return void
     */
    private updateLocation(): void {
        let search: QueryParams = this.getURLSearchParamObject(), data: Object = {};
        data['PremiseLocationNumber'] = this.gridOperation.vbPremiseLocationNumber;
        data['PremiseLocationNumberRowID'] = this.pageParams.premiseLocationNumberRowID;
        search.set(this.serviceConstants.Action, '2');
        search.set('ContractNumber', this.getControlValue('ContractNumber'));
        search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        search.set('ProductCode', this.getControlValue('ProductCode'));
        search.set('ServiceCoverNumber', this.getControlValue('ServiceCoverNumber'));
        search.set('ServiceCoverMode', this.getControlValue('ServiceCoverMode'));
        search.set('ServiceVisitRowID', this.getControlValue('ServiceVisitRowID'));
        search.set('VisitTypeCode', this.getControlValue('VisitTypeCode'));
        search.set('PlanVisitNumber', this.getControlValue('PlanVisitNumber'));
        search.set('AcceptDisplay', this.gridOperation.vbAcceptDisplay);
        search.set('AcceptDisplay', this.gridOperation.vbAcceptDisplay);
        search.set('GridType', 'Main');
        search.set(this.serviceConstants.GridMode, '3');
        search.set(this.serviceConstants.GridHandle, this.getControlValue('riGridHandle'));
        this.ajaxSource.next(AjaxConstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, data)
            .subscribe(
            (e) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e['errorMessage'], e['fullError']));
                } else {
                    this.riGrid.Mode = MntConst.eModeNormal;
                    this.buildGrid();
                }

            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            });
    }

    /**
     * This method is used to lookup call
     * @param void
     * @return void
     */
    private lookupData(): void {
        let lookupIP: any = [
            {
                'table': 'ServiceCover',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'ProductCode': this.getControlValue('ProductCode'),
                    'ROWID': this.getControlValue('ServiceCoverROWID')

                },
                'fields': ['ServiceCoverNumber', 'ServiceCommenceDate', 'AnnualCalendarInd']
            },
            {
                'table': 'Premise',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'BusinessCode': this.getControlValue('BusinessCode'),
                    'PremiseNumber': this.getControlValue('PremiseNumber')
                },
                'fields': ['AccountNumber']
            },
            {
                'table': 'Product',
                'query': {
                    'ProductCode': this.getControlValue('ProductCode'),
                    'BusinessCode': this.getControlValue('BusinessCode')
                },
                'fields': ['ProductDesc', 'RequiresManualVisitPlanningInd']
            }
        ];

        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data) {
                if (data[0][0]) {
                    this.setControlValue('ServiceCoverNumber', data[0][0].ServiceCoverNumber);
                    this.setControlValue('ServiceCommenceDate', data[0][0].ServiceCommenceDate);
                    this.setControlValue('AnnualCalendarInd', data[0][0].AnnualCalendarInd);
                }
                if (data[1][0])
                    this.pageParams.AccountNumber = data[1][0].AccountNumber;
                if (data[2][0]) {
                    this.setControlValue('ProductDesc', data[2][0].ProductDesc);
                    this.setControlValue('RequiresManualVisitPlanningInd', data[2][0].AnnualCalendarInd);
                }
                this.beforeExecute();
                this.buildGrid();
            } else {
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.recordNotFound));
            }

        });
    }

    /**
     * This method is called after changing ContractNumber
     * @param void
     * @return void
     */
    public contractonChange(): void {
        this.buildGrid();
    }

    /**
     * This method is called on row click
     * @param void
     * @return void
     */
    public onGridRowClick(event: any): void {
        let parentMode: string = this.riExchange.getParentMode();
        if (parentMode === 'ServiceVisitUpdate') {
            switch (this.riGrid.CurrentColumnName) {
                case 'Accept':
                    this.riGrid.Update = true;
                    this.riGrid.Mode = MntConst.eModeNormal;
                    this.attributes['Row'] = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
                    this.attributes['ServiceCoverItemRowID'] = event.srcElement.parentElement.parentElement.parentElement.children[0].getAttribute('AdditionalProperty');
                    this.gridOperation.vbAcceptDisplay = 'AcceptDisplay';
                    this.riGrid.Update = true;
                    this.buildGrid();
                    break;
            }
        }
    }

    /**
     * This method is called on row double click
     * @param void
     * @return void
     */
    public onGridRowDblClick(): void {
        if (this.riGrid.CurrentColumnName === 'ItemDescription') {
            this.attributes.ServiceCoverItemRowID = this.riGrid.Details.GetAttribute('ItemDescription', 'additionalproperty');
            this.navigate('DisplayUpd', InternalGridSearchServiceModuleRoutes.ICABSASERVICECOVERDISPLAYENTRY, { currentContractType: this.pageParams.CurrentContractType });
        }
    }

    /**
     * This method returns grid data
     * @param void
     * @return returns grid data
     */
    public getGridInfo(info: any): void {
        this.serviceVisitDisplayGridPagination.totalItems = info.totalRows;
        this.afterExecute(info);

    }

    /**
     * This method is called on click specific page
     * @param void
     * @return void
     */
    public getCurrentPage(currentPage: any): void {
        this.currentPage = currentPage.value;
        this.buildGrid();
    }

    /**
     * This method is called on refresh button click
     * @param void
     * @return void
     */
    public refresh(): void {
        this.buildGrid();
    }

    /**
     * This method is called on blur of grid input
     * @param void
     * @return void
     */
    public onCellKeyDown(): void {
        try {
            this.pageParams.SelPremiseLocationNumber = this.riGrid.Details.GetValue('PremiseLocationNumber');
            this.attributes.grdServiceCoverItemServiceCoverItemRowID = this.riGrid.Details.GetAttribute('PremiseLocationNumber', 'additionalproperty');
            this.attributes.grdServiceCoverItemRow = this.riGrid.Details.GetAttribute('PremiseLocationNumber', 'Rowid');
            this.gridOperation.vbPremiseLocationNumber = this.riGrid.Details.GetValue('PremiseLocationNumber');
            this.pageParams.premiseLocationNumberRowID = this.riGrid.Details.GetAttribute('PremiseLocationNumber', 'Rowid');
            this.gridOperation.vbUpdateRecord = 'update';
            let oldValue: string = this.riGrid.previousValues[7].value;
            this.pageParams.ServiceCoverMode = 'Service';
            if (this.gridOperation.vbUpdateRecord === 'update' && this.gridOperation.vbPremiseLocationNumber !== oldValue) {
                this.riCacheRefresh = true;
                this.setControlValue('riGridHandle', this.utils.gridHandle);
                this.updateLocation();
                this.gridOperation.vbUpdateRecord = '';
                this.setControlValue('ServiceCoverMode', 'ServiceVisit');
            }
        } catch (e) {
            this.logger.warn(e);
        }
    }
    public get isDisabled(): any {
        return MntConst.eModeUpdate;
    }
}
// Class END

