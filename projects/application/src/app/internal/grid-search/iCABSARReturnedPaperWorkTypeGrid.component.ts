import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { GridAdvancedComponent } from './../../../shared/components/grid-advanced/grid-advanced';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { AppModuleRoutes, ContractManagementModuleRoutes, InternalGridSearchSalesModuleRoutes, InternalMaintenanceModuleRoutes } from '../../../app/base/PageRoutes';


@Component({
    templateUrl: 'iCABSARReturnedPaperWorkTypeGrid.html'
})

export class ReturnedPaperWorkTypeGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;

    private queryParams: any = {
        method: 'service-delivery/grid',
        module: 'waste',
        operation: 'ApplicationReport/iCABSARReturnedPaperWorkTypeGrid'
    };
    public pageId: string = '';
    public controls = [
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'LetterTypeCode', type: MntConst.eTypeCode },
        { name: 'LetterTypeDesc', type: MntConst.eTypeText },
        { name: 'ViewBy' },
        { name: 'BranchNumberText' },
        { name: 'BranchNumber' },
        { name: 'Level' },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate }
    ];
    public gridParams: any = {
        totalRecords: 0,
        pageCurrent: 1,
        itemsPerPage: 10,
        riGridMode: 0
    };
    public isRefreshDisabled: boolean = false;
    public isHidePagination: boolean = true;
    public blnRefreshRequired: boolean = true;
    public currentContractTypeURLParameter: string = '';

    public disableObject: any = {
        DateFrom: null,
        DateTo: null
    };

    public dateFromDt: any;
    public dateToDt: any;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSARRETURNEDPAPERWORKTYPEGRID;
        this.browserTitle = 'Non-Returned Paperwork by Type Audit';
        this.pageTitle = 'Non-Returned Paperwork Audit';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.riGrid.DescendingSort = true;
        this.setControlValue('Level', 'Detail');
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchNumberText', this.utils.getBranchText());
        if (this.riExchange.getParentHTMLValue('Level')) {
            this.riExchange.getParentHTMLValue('Level');
        } else {
            this.setControlValue('Level', 'Detail');
        }
        if (this.riExchange.getParentHTMLValue('LetterTypeCode')) {
            this.riExchange.getParentHTMLValue('LetterTypeCode');
            this.riExchange.getParentHTMLValue('LetterTypeDesc');
        }
        if (this.riExchange.getParentHTMLValue('DateTo')) {
            this.riExchange.getParentHTMLValue('DateTo');
            this.dateToDt = this.utils.formatDate(this.getControlValue('DateTo'));
        } else {
            this.dateToDt = null;
            this.setControlValue('DateTo', '');
        }
        if (this.riExchange.getParentHTMLValue('DateFrom')) {
            this.riExchange.getParentHTMLValue('DateFrom');
            this.dateFromDt = this.utils.formatDate(this.getControlValue('DateFrom'));
        } else {
            this.dateFromDt = null;
            this.setControlValue('DateFrom', '');
        }
        if (this.riExchange.getParentHTMLValue('ViewBy')) {
            this.riExchange.getParentHTMLValue('ViewBy');
        }
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.dateFromDt = this.utils.formatDate(this.getControlValue('DateFrom'));
            this.dateToDt = this.utils.formatDate(this.getControlValue('DateTo'));
        }
        this.buildGrid();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        this.riGrid.AddColumn('Contract', 'ReturnedPaperwork', 'Contract', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('Contract', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('PremiseNumber', 'ReturnedPaperwork', 'PremiseNumber', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('Name', 'ReturnedPaperwork', 'Name', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('Name', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Postcode', 'ReturnedPaperwork', 'Postcode', MntConst.eTypeCode, 9);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ProductCode', 'ReturnedPaperwork', 'ProductCode', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ContactName', 'ReturnedPaperwork', 'ContactName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('ContactName', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContactPosition', 'ReturnedPaperwork', 'ContactPosition', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('ContactPosition', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('ContactTelephone', 'ReturnedPaperwork', 'ContactTelephone', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('ContactTelephone', MntConst.eAlignmentRight);

        this.riGrid.AddColumn('DateLetterGenerated', 'ReturnedPaperwork', 'DateLetterGenerated', MntConst.eTypeDate, 15);
        this.riGrid.AddColumnAlign('DateLetterGenerated', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('RecievedInd', 'ReturnedPaperwork', 'RecievedInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('RecievedInd', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('DateReturned', 'ReturnedPaperwork', 'DateReturned', MntConst.eTypeDate, 15);
        this.riGrid.AddColumnAlign('DateReturned', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.loadGridData();
    }

    //riGrid_BeforeExecute functionalities
    private loadGridData(): void {
        let search: QueryParams = new QueryParams();
        this.blnRefreshRequired = this.riGrid.Update ? false : true;
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        search.set(this.serviceConstants.PageSize, this.gridParams.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.gridParams.pageCurrent.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.GridMode, this.gridParams.riGridMode);
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());

        search.set('BranchNumber', this.getControlValue('BranchNumber'));
        search.set('Level', this.getControlValue('Level'));
        search.set('ViewBy', this.getControlValue('ViewBy'));
        search.set('LetterTypeCode', this.getControlValue('LetterTypeCode'));
        search.set('DateFrom', this.getControlValue('DateFrom'));
        search.set('DateTo', this.getControlValue('DateTo'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.gridParams.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                            this.isHidePagination = false;
                        else
                            this.isHidePagination = true;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.gridParams.totalRecords = 1;
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridParams.pageCurrent = currentPage.value;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    public riGridSort(event: any): void {
        this.loadGridData();
    }

    public tbodyServicePlanningOnDblClick(event: any): void {
        let objSrc: any = event['srcElement'];
        let objTr: any = this.riGrid.CurrentHTMLRow;
        let vRouteOptimisation: string = 'No';
        let columnName: any = this.riGrid.CurrentColumnName;
        let currentRowIndex: any = this.riGrid.CurrentRow;
        let rowData: any = this.riGrid.bodyArray[currentRowIndex];
        let exlFromOptRowID: any;
        let currentColumnValue: any = this.riGrid.CurrentColumnValue;

        switch (this.getAttribute('ContractTypeCode')) {
            case 'C':
                this.currentContractTypeURLParameter = '';
                break;
            case 'J':
                this.currentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                this.currentContractTypeURLParameter = '<product>';
                break;
        }

        switch (columnName) {
            case 'Contract':
                if (this.riGrid.Details.GetAttribute('Contract', 'AdditionalProperty')) {
                    this.navigate('PortfolioReports', AppModuleRoutes.CONTRACTMANAGEMENT + ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE_SUB,
                        {
                            CurrentContractTypeURLParameter: this.currentContractTypeURLParameter,
                            ContractRowID: this.riGrid.Details.GetAttribute('Contract', 'AdditionalProperty'),
                            ContractTypeCode: this.getAttribute('ContractTypeCode')
                        });
                }
                break;
            case 'PremiseNumber':
                if (this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty')) {
                    let contractTypeCode: string = (this.riGrid.Details.GetAttribute('Name', 'AdditionalProperty')) ? (this.riGrid.Details.GetAttribute('Name', 'AdditionalProperty')) : (this.riGrid.Details.GetValue('Contract').split('/')[0]);
                    this.navigate('PortfolioReports', AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE_SUB,
                        {
                            contractTypeCode: contractTypeCode,
                            PremiseRowID: this.riGrid.Details.GetAttribute('PremiseNumber', 'AdditionalProperty')
                        });
                }
                break;
            case 'ProductCode':
                if (this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty')) {
                    this.navigate('PortfolioReports', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE,
                        {
                            CurrentContractTypeURLParameter: this.currentContractTypeURLParameter,
                            ContractRowID: this.riGrid.Details.GetAttribute('ProductCode', 'AdditionalProperty'),
                            ContractTypeCode: this.getAttribute('ContractTypeCode')
                        });
                }
                break;
            case 'RecievedInd':
                this.navigate('', InternalMaintenanceModuleRoutes.ICABSBRETURNEDPAPERWORKMAINTENANCE,
                    {
                        LetterRowID: this.riGrid.Details.GetAttribute('RecievedInd', 'AdditionalProperty')
                    });
                break;
        }
    }

    public startDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('DateFrom', value.value);
        }
    }

    public endDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('DateTo', value.value);
        }
    }
}
