import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModuleRoutes, InternalGridSearchApplicationModuleRoutesConstant } from './../base/PageRoutes';

import { ClosedTemplateSearchComponent } from './search/iCABSBClosedTemplateSearch.component';
import { BusinessOriginDetailLanguageSearchComponent } from './search/iCABSBBusinessOriginDetailLanguageSearch';
import { ContractDurationComponent } from './search/iCABSBContractDurationSearch';
import { ProductDetailSearchComponent } from './search/iCABSBProductDetailSearch.component';
import { CampaignSearchComponent } from './search/iCABSBCampaignSearch/iCABSBCampaignSearch';
import { SalesAreaSearchComponent } from './search/iCABSBSalesAreaSearch.component';
import { InvoiceFeeSearchComponent } from './search/iCABSBInvoiceFeeSearch';
import { PaymentSearchComponent } from './search/iCABSBPaymentTypeSearch';
import { InvoiceFrequencySearchComponent } from './search/iCABSBBusinessInvoiceFrequencySearch';
import { ICABSBAPICodeSearchComponent } from './search/iCABSBAPICodeSearchComponent';
import { ApiRateSearchComponent } from './search/iCABSBApiRateSearch';
import { ProductExpenseSearchComponent } from './search/iCABSBProductExpenseSearch';
import { BusinessTierSearchComponent } from './search/iCABSBTierSearchComponent';
import { RegulatoryAuthoritySearchComponent } from './search/iCABSBRegulatoryAuthoritySearch.component';
import { ProductCoverSearchComponent } from './search/iCABSBProductCoverSearch.component';
import { DeportSearchComponent } from './search/iCABSBDepotSearch.component';
import { CalendarTemplateSearchComponent } from './search/iCABSBCalendarTemplateSearch.component';
import { DlRejectionSearchComponent } from './search/iCABSBdlRejectionSearch';
import { PaymentTermSearchComponent } from './search/iCABSBPaymentTermSearch.component';
import { InvoiceRunDateSelectPrintComponent } from './search/iCABSBInvoiceRunDateSelectPrint.component';
import { InvoiceRunDateSelectPrint2Component } from './search/iCABSBInvoiceRunDateSelectPrint2.component';
import { ProductSearchGridComponent } from './search/iCABSBProductSearch';
import { PestNetOnLineLevelSearchComponent } from './search/iCABSBPestNetOnLineLevelSearch.component';
import { LostBusinessDetailSearchComponent } from './search/iCABSBLostBusinessDetailSearch.component';
import { ListGroupSearchComponent } from './grid-search/iCABSBListGroupSearch.component';
import { ProductAttributeValuesSearchComponent } from './search/iCABSBProductAttributeValuesSearch.component';
import { ProductSalesGroupSearchComponent } from './search/iCABSBProductSalesGroupSearch.component';
import { InfestationLevelSearchComponent } from './search/iCABSBInfestationLevelSearch.component';
import { SeasonalTemplateSearchComponent } from './search/iCABSBSeasonalTemplateSearch';
import { PrepChargeRateSearchComponent } from './search/iCABSBPrepChargeRateSearch.component';
import { BranchPcodeServiceGrpEntrySearchComponent } from './search/iCABSBBranchPcodeServiceGrpEntrySearch.component';
import { PreparationSearchComponent } from './search/iCABSBPreparationSearch.component';
import { SystemBusinessVisitTypeSearchComponent } from './search/iCABSBSystemBusinessVisitTypeSearch.component';
import { RegulatoryAuthoritySearchTableComponent } from './search/iCABSBRegulatoryAuthoritySearch';
import { WasteTransferTypeSearchComponent } from './search/iCABSBWasteTransferTypeSearch.component';
import { WasteConsignmentNoteRangeTypeSearchComponent } from './search/iCABSBWasteConsignmentNoteRangeTypeSearch.component';
import { VisitActionSearchComponent, VisitActionSearchDropDownComponent } from './search/iCABSBVisitActionSearch.component';
import { BranchProdServiceGrpEntrySearchComponent } from './search/iCABSBBranchProdServiceGrpEntrySearch.component';
import { BusinessOriginSearchComponent } from './search/iCABSBBusinessOriginSearch.component';
import { ExpenseCodeSearchComponent } from './search/iCABSBExpenseCodeSearch.component';

@NgModule({
    exports: [
        ClosedTemplateSearchComponent,
        BusinessOriginDetailLanguageSearchComponent,
        ContractDurationComponent,
        ProductDetailSearchComponent,
        CampaignSearchComponent,
        SalesAreaSearchComponent,
        InvoiceFeeSearchComponent,
        WasteTransferTypeSearchComponent,
        PaymentSearchComponent,
        InvoiceFrequencySearchComponent,
        ICABSBAPICodeSearchComponent,
        ApiRateSearchComponent,
        ProductExpenseSearchComponent,
        BusinessTierSearchComponent,
        RegulatoryAuthoritySearchComponent,
        ProductCoverSearchComponent,
        DeportSearchComponent,
        CalendarTemplateSearchComponent,
        DlRejectionSearchComponent,
        PaymentTermSearchComponent,
        InvoiceRunDateSelectPrintComponent,
        InvoiceRunDateSelectPrint2Component,
        ProductSearchGridComponent,
        PestNetOnLineLevelSearchComponent,
        LostBusinessDetailSearchComponent,
        ListGroupSearchComponent,
        ProductAttributeValuesSearchComponent,
        ProductSalesGroupSearchComponent,
        SeasonalTemplateSearchComponent,
        PrepChargeRateSearchComponent,
        BranchPcodeServiceGrpEntrySearchComponent,
        PreparationSearchComponent,
        SystemBusinessVisitTypeSearchComponent,
        RegulatoryAuthoritySearchTableComponent,
        WasteConsignmentNoteRangeTypeSearchComponent,
        VisitActionSearchComponent,
        VisitActionSearchDropDownComponent,
        BranchProdServiceGrpEntrySearchComponent,
        BusinessOriginSearchComponent,
        ExpenseCodeSearchComponent,
        InfestationLevelSearchComponent
    ],
    declarations: [
        ClosedTemplateSearchComponent,
        BusinessOriginDetailLanguageSearchComponent,
        ContractDurationComponent,
        ProductDetailSearchComponent,
        CampaignSearchComponent,
        SalesAreaSearchComponent,
        InvoiceFeeSearchComponent,
        PaymentSearchComponent,
        InvoiceFrequencySearchComponent,
        ICABSBAPICodeSearchComponent,
        ApiRateSearchComponent,
        ProductExpenseSearchComponent,
        BusinessTierSearchComponent,
        RegulatoryAuthoritySearchComponent,
        ProductCoverSearchComponent,
        DeportSearchComponent,
        CalendarTemplateSearchComponent,
        DlRejectionSearchComponent,
        PaymentTermSearchComponent,
        InvoiceRunDateSelectPrintComponent,
        InvoiceRunDateSelectPrint2Component,
        ProductSearchGridComponent,
        PestNetOnLineLevelSearchComponent,
        LostBusinessDetailSearchComponent,
        ListGroupSearchComponent,
        ProductAttributeValuesSearchComponent,
        ProductSalesGroupSearchComponent,
        InfestationLevelSearchComponent,
        SeasonalTemplateSearchComponent,
        PrepChargeRateSearchComponent,
        BranchPcodeServiceGrpEntrySearchComponent,
        PreparationSearchComponent,
        SystemBusinessVisitTypeSearchComponent,
        RegulatoryAuthoritySearchTableComponent,
        WasteTransferTypeSearchComponent,
        WasteConsignmentNoteRangeTypeSearchComponent,
        VisitActionSearchComponent,
        VisitActionSearchDropDownComponent,
        BranchProdServiceGrpEntrySearchComponent,
        BusinessOriginSearchComponent,
        ExpenseCodeSearchComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'application/payment', component: PaymentSearchComponent },
            { path: 'application/invoicefreqsearch', component: InvoiceFrequencySearchComponent },
            { path: 'application/campaignsearchcomponent', component: CampaignSearchComponent },
            { path: 'application/invoicefeesearch', component: InvoiceFeeSearchComponent },
            { path: 'application/apiRateSearch', component: ApiRateSearchComponent },
            { path: 'application/productExpenseSearch', component: ProductExpenseSearchComponent },
            { path: 'application/depotSearch', component: DeportSearchComponent },
            { path: 'application/CalendarTemplateSearch', component: CalendarTemplateSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBDLREJECTIONSEARCH, component: DlRejectionSearchComponent },
            { path: 'application/invoiceRunDate/print', component: InvoiceRunDateSelectPrintComponent },
            { path: InternalSearchModuleRoutes.ICABSBINVOICERUNDATESELECTPRINT2, component: InvoiceRunDateSelectPrint2Component },
            { path: InternalSearchModuleRoutes.ICABSBCLOSEDTEMPLATESEARCH, component: ClosedTemplateSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTSEARCH, component: ProductSearchGridComponent },
            { path: InternalSearchModuleRoutes.ICABSBLOSTBUSINESSDETAILSEARCH, component: LostBusinessDetailSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPESTNETONLINELEVELSEARCH, component: PestNetOnLineLevelSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBINFESTATIONLEVELSEARCH, component: InfestationLevelSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTDETAILSEARCH, component: ProductDetailSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTCOVERSEARCH, component: ProductCoverSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBPRODUCTSALESGROUPSEARCH, component: ProductSalesGroupSearchComponent },
            { path: InternalGridSearchApplicationModuleRoutesConstant.ICABSBLISTGROUPSEARCH, component: ListGroupSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBSEASONALTEMPLATESEARCH, component: SeasonalTemplateSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH, component: BranchPcodeServiceGrpEntrySearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBWASTETRANSFERTYPESEARCH, component: WasteTransferTypeSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBWASTECONSIGNMENTNOTERANGETYPESEARCH, component: WasteConsignmentNoteRangeTypeSearchComponent },
            { path: InternalSearchModuleRoutes.ICABSBVISITACTIONSEARCH, component: VisitActionSearchComponent }
        ])
    ],
    entryComponents: [
        ClosedTemplateSearchComponent,
        BusinessOriginDetailLanguageSearchComponent,
        ContractDurationComponent,
        ProductDetailSearchComponent,
        CampaignSearchComponent,
        SalesAreaSearchComponent,
        InvoiceFeeSearchComponent,
        PaymentSearchComponent,
        InvoiceFrequencySearchComponent,
        ICABSBAPICodeSearchComponent,
        ApiRateSearchComponent,
        ProductExpenseSearchComponent,
        BusinessTierSearchComponent,
        RegulatoryAuthoritySearchComponent,
        ProductCoverSearchComponent,
        DeportSearchComponent,
        CalendarTemplateSearchComponent,
        DlRejectionSearchComponent,
        PaymentTermSearchComponent,
        InvoiceRunDateSelectPrintComponent,
        InvoiceRunDateSelectPrint2Component,
        ProductSearchGridComponent,
        PestNetOnLineLevelSearchComponent,
        LostBusinessDetailSearchComponent,
        ListGroupSearchComponent,
        ProductAttributeValuesSearchComponent,
        ProductSalesGroupSearchComponent,
        SeasonalTemplateSearchComponent,
        PrepChargeRateSearchComponent,
        BranchPcodeServiceGrpEntrySearchComponent,
        PreparationSearchComponent,
        SystemBusinessVisitTypeSearchComponent,
        RegulatoryAuthoritySearchTableComponent,
        WasteTransferTypeSearchComponent,
        WasteConsignmentNoteRangeTypeSearchComponent,
        VisitActionSearchComponent,
        VisitActionSearchDropDownComponent,
        BranchProdServiceGrpEntrySearchComponent,
        BusinessOriginSearchComponent,
        ExpenseCodeSearchComponent,
        InfestationLevelSearchComponent
    ]
})

export class SearchEllipsisBusinessModule { }

