import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs';

import { PageIdentifier } from './../../base/PageIdentifier';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from './../../../shared/services/utility';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { TableComponent } from './../../../shared/components/table/table';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ModalAdvService } from './../../../shared/components/modal-adv/modal-adv.service';
import { InternalMaintenanceModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSBBranchPcodeServiceGrpEntrySearch.html'
})

export class BranchPcodeServiceGrpEntrySearchComponent extends SelectedDataEvent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riTable') riTable: TableComponent;

    private parentMode: string;
    private inputParams: any = {};
    private modalAdvService: ModalAdvService;
    private activatedRouteSubscription: Subscription;
    private activatedRoute: ActivatedRoute;
    private router: Router;
    private riExchange: RiExchange;
    private isRowClick: boolean = false;
    private utils: Utils;
    private serviceConstants: ServiceConstants;
    public uiForm: FormGroup;
    public formBuilder: FormBuilder;
    public pageTitle: string;
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc' },
        { name: 'menu' }
    ];
    public tableParams: Object = {
        columns: [],
        itemsPerPage: '10'
    };

    constructor(injector: Injector) {
        super();
        this.injectService(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH;
        this.activatedRouteSubscription = this.activatedRoute.queryParams.subscribe(
            (param: any) => {
                if (param) {
                    this.riExchange.setRouterParams(param);
                }
            });
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.pageTitle = 'Postcode Group Entry Details';
        this.utils.setTitle('Postcode Group Entry Search');
    }

    ngAfterViewInit(): void {
        this.windowOnload();
        this.buildTableColumns();
    }

    ngOnDestroy(): void {
        this.utils = null;
        this.serviceConstants = null;
        this.formBuilder = null;
        this.riExchange = null;
        this.router = null;
        this.activatedRoute = null;
        this.modalAdvService = null;
        if (this.activatedRouteSubscription)
            this.activatedRouteSubscription.unsubscribe();
    }

    private injectService(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.router = injector.get(Router);
        this.modalAdvService = injector.get(ModalAdvService);
    }

    private windowOnload(): void {
        this.parentMode = this.riExchange.getParentMode();
        if (this.parentMode === MessageConstant.PageSpecificMessage.addPostcode) {
            this.riExchange.riInputElement.SetValue(this.uiForm, 'BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
            this.riExchange.riInputElement.SetValue(this.uiForm, 'BranchServiceAreaDesc', this.riExchange.getParentHTMLValue('BranchServiceAreaDesc'));
        } else {
            this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
            this.riExchange.getParentHTMLValue('BranchServiceAreaDesc');
        }
    }

    public updateView(params?: any): void {
        this.inputParams = params;
        this.parentMode = this.inputParams.parentMode;
        this.windowOnload();
        this.buildTableColumns();
    }

    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('Postcode', MntConst.eTypeCode, 'Key', 'Postcode', 15);
        this.riTable.AddTableField('State', MntConst.eTypeCode, 'Optional', 'State', 30);
        this.riTable.AddTableField('Town', MntConst.eTypeCode, 'Optional', 'Town', 30);
        this.riTable.AddTableField('SortOrder', MntConst.eTypeInteger, 'Optional', 'SortOrder', 9);
        this.buildTable();
    }

    public buildTable(): void {
        let xhr: Object = {
            operation: 'Business/iCABSBBranchPcodeServiceGrpEntrySearch',
            module: 'validation',
            method: 'contract-management/search'
        };
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '0');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.riExchange.riInputElement.GetValue(this.uiForm, 'BranchServiceAreaCode'));
        search.set('search.sortby', 'SortOrder');
        xhr['search'] = search;
        this.riTable.loadTableData(xhr);
    }

    public selectedData(event: any): void {
        let returnObj: Object;
        switch (this.riExchange.getParentMode()) {
            case MessageConstant.PageSpecificMessage.addPostcode:
            case 'BranchPcode':
                //To Do
                this.router.navigate([InternalMaintenanceModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE], {
                    queryParams: {
                        isRowClick: 'true',
                        parentMode: MessageConstant.PageSpecificMessage.addPostcode,
                        Postcode: event['row']['Postcode'],
                        State: event['row']['State'],
                        Town: event['row']['Town'],
                        SortOrder: event['row']['SortOrder'],
                        BranchServiceAreaCode: this.riExchange.riInputElement.GetValue(this.uiForm, 'BranchServiceAreaCode'),
                        BranchServiceAreaDesc: this.riExchange.riInputElement.GetValue(this.uiForm, 'BranchServiceAreaDesc'),
                        ROWID: event['row']['ttBranchPcodeServiceGrpEntry']
                    }
                });
                break;
            case 'LookUp':
                returnObj = {
                    Postcode: event['row']['Postcode'],
                    State: event['row']['State'],
                    Town: event['row']['Town']
                };
                break;
            default:
                returnObj = {
                    Postcode: event['row']['Postcode'],
                    State: event['row']['State'],
                    Town: event['row']['Town']
                };
                break;
        }
        this.emitSelectedData(returnObj);
    }

    public selectedOption(event: string): void {
        if (event === MessageConstant.PageSpecificMessage.addPostcode) {
            // To Do
            this.router.navigate([InternalMaintenanceModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE], {
                queryParams: {
                    getParentMode: 'AddPostcode',
                    BranchServiceAreaCode: this.riExchange.riInputElement.GetValue(this.uiForm, 'BranchServiceAreaCode'),
                    BranchServiceAreaDesc: this.riExchange.riInputElement.GetValue(this.uiForm, 'BranchServiceAreaDesc')
                }
            });
        }
    }

    public onDataChange(event: any): void {
        switch (event.target.id) {
            case 'BranchServiceAreaCode':
                this.riExchange.riInputElement.SetValue(this.uiForm, 'BranchServiceAreaCode', event.target.value);
                break;
            case 'BranchServiceAreaDesc':
                this.riExchange.riInputElement.SetValue(this.uiForm, 'BranchServiceAreaDesc', event.target.value);
                break;
            default:
                break;
        }
    }

}
