import { Component, Injector, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSBWasteConsignmentNoteRangeTypeSearch.html'
})

export class WasteConsignmentNoteRangeTypeSearchComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    @Input() inputParams: any;
    @Input() showAddNew: boolean = false;

    public pageId: string = '';
    public controls = [];
    public queryParams: any = {
        operation: 'Business/iCABSBWasteConsignmentNoteRangeTypeSearch',
        module: 'waste',
        method: 'service-delivery/search'
    };
    public page: number = 1;
    public columns: Array<any> = [];
    public tableheading: string;
    public itemsPerPage: number = 10;
    public search = new QueryParams();

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBWASTECONSIGNMENTNOTERANGETYPESEARCH;
        this.pageTitle = 'Waste Consignment Note Range Type Search';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('RegulatoryAuthorityNumber', MntConst.eTypeInteger, 'Key', 'Regulatory Authority Number', 30);
        this.riTable.AddTableField('WasteConsignmentNoteRangeType', MntConst.eTypeCode, 'Key', 'Waste Transfer Type Code', 30);
        this.riTable.AddTableField('ConsignmentNoteRangeTypeDesc', MntConst.eTypeText, 'Required', 'Description', 50);
        if (this.inputParams.parentMode === 'LookUp-WCNoteRangeType') {
            if (this.inputParams.RegulatoryAuthorityNumber) {
                this.pageParams.vRegulatoryNumber = this.inputParams.RegulatoryAuthorityNumber;
            } else {
                this.pageParams.vRegulatoryNumber = 0;
            }
            this.riTable.AddTableField('RegulatoryAuthorityName', MntConst.eTypeText, 'Virtual', 'Description', 30);
        }
        this.buildTable();
    }

    public buildTable(): void {
        this.search.set(this.serviceConstants.Action, '0');
        this.search.set(this.serviceConstants.BusinessCode, this.businessCode());
        this.search.set(this.serviceConstants.CountryCode, this.countryCode());
        this.search.set('RegulatoryAuthorityNumber', this.pageParams.vRegulatoryNumber);
        this.search.set('search.sortby', 'WasteConsignmentNoteRangeType DESC');
        this.search.set('jsonSortField', 'WasteConsignmentNoteRangeType');
        this.queryParams.search = this.search;
        this.riTable.loadTableData(this.queryParams);
    }

    public onTableRecordSelected(event: any): void {
        let returnObj: any;
        let vntReturnData = event.row;
        switch (this.inputParams.parentMode) {
            case 'LookUp':
                returnObj = {
                    'WasteConsignmentNoteRangeType': vntReturnData.WasteConsignmentNoteRangeType,
                    'ConsignmentNoteRangeTypeDesc': vntReturnData.ConsignmentNoteRangeTypeDesc
                };
                break;
            default:
                if (this.inputParams.parentMode === 'LookUp-WCNoteRangeType') {
                    returnObj = {
                        'WasteConsignmentNoteRangeType': vntReturnData.WasteConsignmentNoteRangeType,
                        'ConsignmentNoteRangeTypeDesc': vntReturnData.ConsignmentNoteRangeTypeDesc
                    };
                } else {
                    returnObj = {
                        'WasteConsignmentNoteRangeType': vntReturnData.WasteConsignmentNoteRangeType
                    };
                }
                break;
        }

        this.emitSelectedData(returnObj);
    }

    public refresh(): void {
        this.buildTableColumns();
    }

    public updateView(params: any): void {
        this.inputParams = params;

        this.zone.run(() => {
            this.inputParams = params;
            if (this.inputParams && this.inputParams.showAddNew !== undefined) {
                this.showAddNew = true;
            }
            if (typeof params.showAddNew !== 'undefined') {
                this.showAddNew = params.showAddNew;
            }
        });
        this.buildTableColumns();
    }

    public onAddNew(): void {
        this.emitSelectedData('AddModeOn');
    }
}
