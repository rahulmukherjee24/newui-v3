import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { BaseComponent } from '@app/base/BaseComponent';
import { PageIdentifier } from '@base/PageIdentifier';
import { TableComponent } from '@shared/components/table/table';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { InternalSearchModuleRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSePDAiCABSPrepUsedSearch.html'
})

export class PDAiCABSPrepUsedSearchComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('icabsSePdaicabsPrepUsedSearch') icabsSePdaicabsPrepUsedSearch: TableComponent;
    public pageId: string = '';
    public itemsPerPage: string = '10';
    public page: number = 1;
    public activityRowID: any = '';
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeAutoNumber, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeTextFree, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'menu', type: '', value: '' }
    ];
    // URL Query Parameters
    public queryParams: any = {
        operation: 'Service/iCABSSePDAiCABSPrepUsedSearch',
        module: 'pda',
        method: 'service-delivery/search'
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPDAICABSPREPUSEDSEARCH;
        this.browserTitle = 'Prep Used Search';
        this.pageTitle = 'Prep Used Details';
    }
    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
        this.loadTableData();
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    private windowOnload(): void {
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));

        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));

        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
        this.activityRowID = this.riExchange.GetParentRowID(this.uiForm, 'PDAICABSActivity');

        if (!this.activityRowID) {
            this.activityRowID = this.riExchange.getParentHTMLValue('PDAICABSActivity');
        }
        this.setAttribute('PDAICABSActivityRowID', this.activityRowID);
        this.setControlValue('menu', '');
    }
    //This method is responsible to load table data
    private loadTableData(): void {
        let search: QueryParams = new QueryParams(), searchParams: any = {};

        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('PDAICABSActivityRowID', this.activityRowID);
        search.set('search.sortby', 'PrepCode');
        searchParams.search = search;
        searchParams.module = this.queryParams.module;
        searchParams.method = this.queryParams.method;
        searchParams.operation = this.queryParams.operation;

        this.buildTableColumns();
        this.icabsSePdaicabsPrepUsedSearch.loadTableData(searchParams);
    }
    //This methos is responsible to show columns for the table
    private buildTableColumns(): void {
        this.icabsSePdaicabsPrepUsedSearch.clearTable();
        this.icabsSePdaicabsPrepUsedSearch.AddTableField('PrepCode', MntConst.eTypeCode, 'Key', 'Prep Code', 3);
        this.icabsSePdaicabsPrepUsedSearch.AddTableField('PrepDesc', MntConst.eTypeText, 'Virtual', 'Description', 30);
        this.icabsSePdaicabsPrepUsedSearch.AddTableField('PrepVolume', MntConst.eTypeDecimal2, 'Required', 'Volume', 10);
        this.icabsSePdaicabsPrepUsedSearch.AddTableField('PrepValue', MntConst.eTypeCurrency, 'Required', 'Prep Value', 10);
    }
    //When clicked on particular row of the table, this method get executed
    public selectedData(event: any): void {
        this.attributeSetup();
        this.setAttribute('PrepCode', event.row.PrepCode);
        this.setAttribute('PrepDesc', event.row.PrepDesc);

        this.navigate('Search', InternalSearchModuleRoutes.ICABSSEPDAICABSPREPUSED, {
            Activity: event.row.ttPDAICABSActivity,
            PrepUsed: event.row.ttPDAICABSPrepUsed
        });
    }
    public menuOnchange(value: string): void {
        if (value === 'AddPrep') {
            this.attributeSetup();
            this.navigate('SearchAdd', InternalSearchModuleRoutes.ICABSSEPDAICABSPREPUSED, {
                Activity: this.activityRowID
            });
        }
    }

    private attributeSetup(): void {
        this.setAttribute('ContractNumber', this.getControlValue('ContractNumber'));
        this.setAttribute('ContractName', this.getControlValue('ContractName'));
        this.setAttribute('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.setAttribute('PremiseName', this.getControlValue('PremiseName'));
        this.setAttribute('ProductCode', this.getControlValue('ProductCode'));
        this.setAttribute('ProductDesc', this.getControlValue('ProductDesc'));
    }
}
