import { Component, OnInit, OnChanges, Input, EventEmitter, ViewChild, Output, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs/Subscription';

import { DropdownComponent } from './../../../shared/components/dropdown/dropdown';
import { Utils } from '../../../shared/services/utility';
import { HttpService } from './../../../shared/services/http-service';
import { RiExchange } from './../../../shared/services/riExchange';
import { ServiceConstants } from './../../../shared/constants/service.constants';

@Component({
    selector: 'icabs-notification-group-search',
    template: `<icabs-dropdown #notificationgroupDropDown
        [itemsToDisplay]="displayFields" [disabled]="isDisabled" [triggerValidate]="triggerValidate" [isRequired]="isRequired" [active]="active" (selectedValue)="onSelectedValue($event)">
    </icabs-dropdown>`
})

export class NotificationGroupSearchComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('notificationgroupDropDown') notificationgroupDropDown: DropdownComponent;
    @Input() public inputParams: any;
    @Input() public isDisabled: any;
    @Input() public active: any;
    @Input() public isRequired: boolean;
    @Input() public triggerValidate: boolean;
    @Input() public itemsToDisplay: Array<string>;
    @Output() selectedValue: EventEmitter<any>;
    @Output() onError: EventEmitter<any>;

    private httpSubscription: Subscription;
    private queryParams: any = {
        method: 'ccm/search',
        module: 'rules',
        operation: 'System/iCABSSNotificationGroupSearch'
    };
    public requestdata: Array<any>;
    public displayFields: Array<string> = ['NotifyGroupCode', 'NotifyGroupSystemDesc'];

    constructor(
        private serviceConstants: ServiceConstants,
        private httpService: HttpService,
        private utils: Utils,
        private riExchange: RiExchange) {
        this.selectedValue = new EventEmitter();
        this.onError = new EventEmitter();
    }

     ngOnInit(): void {
        if (this.inputParams) {
            this.fetchData();
        }
    }

    ngOnChanges(data: any): void {
        if (data.inputParams) {
            this.fetchData();
        }
    }

    ngOnDestroy(): void {
        if (this.httpSubscription) {
            this.httpSubscription.unsubscribe();
        }
        this.httpSubscription = null;
        this.selectedValue = null;
        this.onError = null;
        this.requestdata = null;
    }

    public fetchData(params?: any): void {
        let search: QueryParams = new QueryParams();
        this.inputParams = params || this.inputParams;
        if (this.inputParams) {
            search.set(this.serviceConstants.Action, '0');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            this.httpSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
                this.queryParams.operation, search)
                .subscribe((data) => {
                    if (data) {
                        this.requestdata = data.records;
                    }
                    this.notificationgroupDropDown.updateComponent(this.requestdata);
                }, (error) => {
                    this.onError.emit(error);
                });
        }
    }

    public onSelectedValue(obj: any): void {
        this.selectedValue.emit(obj.value);
    }

}

