import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { RiExchange } from './../../../shared/services/riExchange';
import { Utils } from './../../../shared/services/utility';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { TableComponent } from './../../../shared/components/table/table';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'iCABSSSystemPDAActivityTypeLanguageSearch.html'
})
export class SystemPDAActivityTypeLanguageSearchComponent extends SelectedDataEvent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riTable') riTable: TableComponent;

    private riExchange: RiExchange;
    private utils: Utils;
    private serviceConstants: ServiceConstants;
    private parentMode: string;
    public uiForm: FormGroup;
    public formBuilder: FormBuilder;
    public pageTitle: string;
    public pageId: string = '';
    public inputParams: any;
    public isLanguageDetail: boolean = true;
    public controls: Array<Object> = [
        { name: 'LanguageCode', type: MntConst.eTypeCode },
        { name: 'LanguageDescription' }
    ];
    public tableParams: Object = {
        columns: [],
        itemsPerPage: '10'
    };

    constructor(injector: Injector) {
        super();
        this.injectService(injector);
        this.pageId = PageIdentifier.ICABSSSYSTEMPDAACTIVITYTYPELANGUAGESEARCH;
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.pageTitle = 'System PDA Activity Type Search';
    }

    ngOnDestroy(): void {
        this.utils = null;
        this.serviceConstants = null;
        this.formBuilder = null;
        this.riExchange = null;
    }

    ngAfterViewInit(): void {
        this.buildTableColumns();
    }

    private injectService(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
    }

    public updateView(params?: any): void {
        this.inputParams = params;
        this.parentMode = this.inputParams.parentMode;

        if (this.inputParams) {
            let languageCode: string = this.inputParams.LanguageCode ? this.inputParams.LanguageCode : '';
            let description: string = this.inputParams.LanguageDescription ? this.inputParams.LanguageDescription : '';
            this.riExchange.riInputElement.SetValue(this.uiForm, 'LanguageCode', languageCode);
            this.riExchange.riInputElement.SetValue(this.uiForm, 'LanguageDescription', description);

            if (!this.riExchange.riInputElement.GetValue(this.uiForm, 'LanguageCode')) {
                this.riExchange.riInputElement.SetValue(this.uiForm, 'LanguageCode', this.riExchange.LanguageCode());
                this.isLanguageDetail = false;
                this.buildTableColumns();
            }
        }
    }

    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('SystemPDAActivityTypeCode', MntConst.eTypeInteger, 'Key', 'Type Code', 3);
        this.riTable.AddTableField('SystemPDAActivityTypeDesc', MntConst.eTypeText, 'Required', 'Display Description', 40);
        this.riTable.AddTableField('SystemPDAActivityTypeShortDesc', MntConst.eTypeText, 'Required', 'Short Description', 10);
        this.buildTable();
    }

    public buildTable(): void {
        let xhr: Object = {
            operation: 'System/iCABSSSystemPDAActivityTypeLanguageSearch',
            module: 'pda',
            method: 'service-delivery/search'
        };
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '0');
        search.set('LanguageCode', this.riExchange.riInputElement.GetValue(this.uiForm, 'LanguageCode'));
        xhr['search'] = search;
        this.riTable.loadTableData(xhr);
    }

    public selectedData(event: any): void {
        let returnObj: Object;

        if (this.parentMode === 'LookUp' || this.parentMode === 'LookUp-Activity') {
            returnObj = {
                SystemPDAActivityTypeCode: event['row']['SystemPDAActivityTypeCode'],
                SystemPDAActivityTypeDesc: event['row']['SystemPDAActivityTypeDesc']
            };
        } else {
            returnObj = {
                SystemPDAActivityTypeCode: event['row']['SystemPDAActivityTypeCode']
            };
        }
        this.emitSelectedData(returnObj);
    }
}
