import { Component, OnInit,  OnDestroy, Injector, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Utils } from './../../../shared/services/utility';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { RiExchange } from '../../../shared/services/riExchange';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';

@Component({
    templateUrl: 'iCABSBBranchProdServiceGrpEntrySearch.html'
})

export class BranchProdServiceGrpEntrySearchComponent  implements OnInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;

    private isAddNew: boolean;
    public uiForm: FormGroup;
    public itemsPerPage: string = '10';
    public pageTitle: string = 'Product Group Entry Details';
    public inputParams: any = {
        method: 'service-delivery/search',
        operation: 'Business/iCABSBBranchProdServiceGrpEntrySearch',
        module: 'product'
    };
    public pageId: string = '';

    public controls: Array<any> = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', type: MntConst.eTypeText },
        {name: 'menu'}
    ];

    public columns: Array<any> = [
        { title: 'Product Group', name: 'ProductServiceGroupCode', type: MntConst.eTypeCode },
        { title: 'Description', name: 'ProductServiceGroupDesc', type: MntConst.eTypeText }
    ];

    constructor(injector: Injector, private formBuilder: FormBuilder, private riExchange: RiExchange, private ellipsis: EllipsisComponent, private utils: Utils, private serviceConstants: ServiceConstants) {
        this.pageId = PageIdentifier.ICABSBBRANCHPRODSERVICEGRPENTRYSEARCH;
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
    }

    private getTablePageData(): void {
        let search: any = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('BranchServiceAreaCode', this.uiForm.controls['BranchServiceAreaCode'].value
        );
        search.set(this.serviceConstants.Action, '0');
        this.inputParams.search = search;
        this.riTable.loadTableData(this.inputParams);
    }

    public updateView(params?: any): void {
        if (params && params.parentMode) {
            this.inputParams.parentMode = params.parentMode;
            this.uiForm.controls['BranchServiceAreaCode'].setValue(params.BranchServiceAreaCode ? params.BranchServiceAreaCode : '');
            this.uiForm.controls['BranchServiceAreaDesc'].setValue(params.BranchServiceAreaDesc ? params.BranchServiceAreaDesc : '');
        }
        this.getTablePageData();
    }

    /* Sends data to parent page ellipsis */
    public selectedData(event: any): void {
        this.ellipsis.sendDataToParent({
            productDetails: event.row,
            branchServiceAreaCode: this.uiForm.controls['BranchServiceAreaCode'].value,
            branchServiceAreaDesc: this.uiForm.controls['BranchServiceAreaDesc'].value,
            addNew: false
        }
            );
    }

    public onSearchClick(): void {
        this.getTablePageData();
    }

    public selectedOption(data: any): void {
        this.ellipsis.sendDataToParent({
            branchServiceAreaCode: this.uiForm.controls['BranchServiceAreaCode'].value,
            branchServiceAreaDesc: this.uiForm.controls['BranchServiceAreaDesc'].value,
            addNew: true
        }
            );
        this.ellipsis.closeModal();
    }
}

