import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { Utils } from './../../../shared/services/utility';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { TableComponent } from './../../../shared/components/table/table';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'iCABSCMProspectStatusSearch.html'
})
export class ProspectStatusSearchComponent extends SelectedDataEvent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;

    private parentMode: string;
    public pageTitle: string;
    public pageId: string = '';
    public inputParams: any;
    public controls: Array<Object> = [];
    public tableParams: Object = {
        columns: [],
        itemsPerPage: '10'
    };

    constructor(private utils: Utils, private serviceConstants: ServiceConstants) {
        super();
        this.pageId = PageIdentifier.ICABSCMPROSPECTSTATUSSEARCH;
    }

    ngOnInit(): void {
        this.pageTitle = 'Prospect Status Search';
    }

    ngAfterViewInit(): void {
        this.buildTableColumns();
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
    }

    private buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('ProspectStatusCode', MntConst.eTypeCode, 'Key', 'Code', 10);
        this.riTable.AddTableField('ProspectStatusDesc', MntConst.eTypeText, 'Required', 'Description', 40);
        this.riTable.AddTableField('ProspectConvertedInd', MntConst.eTypeCheckBox, 'Required', 'Converted', 15);
        this.riTable.AddTableField('UserSelectableInd', MntConst.eTypeCheckBox, 'Required', 'User Selectable', 15);
        this.buildTable();
    }

    private buildTable(): void {
        let xhr: Object = {
            operation: 'ContactManagement/iCABSCMProspectStatusSearch',
            module: 'prospect',
            method: 'prospect-to-contract/search'
        };
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '0');
        xhr['search'] = search;
        this.riTable.loadTableData(xhr);
    }

    public selectedData(event: any): void {
        let returnObj: Object;
        returnObj = {
            ProspectStatusCode: event['row']['ProspectStatusCode'],
            ProspectStatusDesc: event['row']['ProspectStatusDesc'],
            ProspectConvertedInd: event['row']['ProspectConvertedInd']
        };
        this.emitSelectedData(returnObj);
    }

    public updateView(params: any): void {
        this.buildTableColumns();
    }

}
