import { Component, Injector, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { TableComponent } from './../../../shared/components/table/table';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { Utils } from './../../../shared/services/utility';

@Component({
    templateUrl: 'iCABSBSystemBusinessVisitTypeSearch.html'
})

export class SystemBusinessVisitTypeSearchComponent extends SelectedDataEvent implements AfterViewInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    private parentMode: string;
    private queryParams: any = {
        operation: 'Business/iCABSBSystemBusinessVisitTypeSearch',
        module: 'service',
        method: 'service-delivery/search'
    };
    //Inject classes
    private utils: Utils;
    private serviceConstants: ServiceConstants;
    //Variables declared to use all classes
    private pageId: string = '';
    public pageTitle: string;
    public itemsPerPage: number = 10;
    public isRequesting: boolean = false;
    public isShowAddNew: boolean = false;

    constructor(injector: Injector) {
        super();
        this.injectServices(injector);
        this.pageId = PageIdentifier.ICABSBSYSTEMBUSINESSVISITTYPESEARCH;
        this.pageTitle = 'System Business Visit Type Search';
    }

    ngAfterViewInit(): void {
        this.buildTable();
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
    }

    // Function to inject services
    private injectServices(injector: Injector): void {
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
    }

    // Build table columns
    private buildTable(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('SystemVisitTypeCode', MntConst.eTypeCode, 'Key', 'System Visit Type Code', 6);
        this.riTable.AddTableField('VisitTypeCode', MntConst.eTypeText, 'Required', 'Visit Type Code', 6);
        this.riTable.AddTableField('VisitTypeDesc', MntConst.eTypeText, 'Virtual', 'Visit Type Description', 20);
    }

    // Load table data
    private loadTable(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.queryParams.search = search;
        this.riTable.loadTableData(this.queryParams);
    }

    // Table row click event
    public onTableRowClick(event: any): void {
        let returnObj: any;
        returnObj = event.row;
        this.emitSelectedData(returnObj);
    }

    // Add New Click event
    public onAddNew(): void {
        this.emitSelectedData('AddModeOn');
    }

    public updateView(params: any): void {
        if (params.parentMode) {
            this.parentMode = params.parentMode;
        }
        if (params.isShowAddNew) {
            this.isShowAddNew = params.isShowAddNew;
        }
        this.loadTable();
    }
}
