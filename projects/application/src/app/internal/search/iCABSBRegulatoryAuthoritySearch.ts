import { Component, OnInit, Injector, ViewChild, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { RiExchange } from './../../../shared/services/riExchange';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { Utils } from './../../../shared/services/utility';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'iCABSBRegulatoryAuthoritySearch.html'
})

export class RegulatoryAuthoritySearchTableComponent extends SelectedDataEvent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('riTable') riTable: TableComponent;
    @Input() showAddNew: boolean;

    private inputParams: any = {};
    private parentMode: string = '';
    private columnHeaders: any = {
        RegulatoryAuthorityNumber: 'Regulatory Authority Number',
        RegulatoryAuthorityName: 'Name',
        MaximumWeightPerDay: 'Max Weight Per Day (KG)',
        RequiresPremiseRegistrationInd: 'Requires Premises Registration',
        RequiresServiceCoverWasteInd: 'Requires Service Cover Waste',
        RequiresWasteTransferNotesInd: 'Requires Waste Transfer Notes',
        WasteRegulatoryCarrierName: 'Waste Regulatory Carrier Name',
        WasteRegulatoryRegistrationNum: 'Waste Regulatory Registration Number'
    };
    public isShow: boolean = false;
    public search = new QueryParams();
    public pageId: string = '';
    public page: any;
    public itemsPerPage: number = 10;
    public serviceConstants: ServiceConstants;
    public utils: Utils;
    public riExchange: RiExchange;
    public pageTitle: string = '';
    public queryParams: any = {
        operation: 'Business/iCABSBRegulatoryAuthoritySearch',
        module: 'waste',
        method: 'service-delivery/search'
    };
    public tableParams: Object = {
        columns: [],
        itemsPerPage: '10'
    };
    constructor(injector: Injector, private ellipsis: EllipsisComponent) {
        super();
        this.injectServices(injector);
        this.pageId = PageIdentifier.ICABSBREGULATORYAUTHORITYSEARCH;
    }

    ngOnInit(): void {
        this.pageTitle = 'Regulatory Authority Search';
    }
    ngOnDestroy(): void {
        this.serviceConstants = null;
    }
    ngAfterViewInit(): void {
        this.buildTableColumns();
    }

    private injectServices(injector: Injector): void {
        this.riExchange = injector.get(RiExchange);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
    }
    private buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('RegulatoryAuthorityNumber', MntConst.eTypeInteger, 'Key', this.columnHeaders.RegulatoryAuthorityNumber, 7);
        this.riTable.AddTableField('RegulatoryAuthorityName', MntConst.eTypeText, 'Required', this.columnHeaders.RegulatoryAuthorityName, 40);
        this.riTable.AddTableField('MaximumWeightPerDay', MntConst.eTypeDecimal2, 'Optional', this.columnHeaders.MaximumWeightPerDay, 9);
        this.riTable.AddTableField('RequiresPremiseRegistrationInd', MntConst.eTypeCheckBox, 'Optional', this.columnHeaders.RequiresPremiseRegistrationInd, 1);
        this.riTable.AddTableField('RequiresServiceCoverWasteInd', MntConst.eTypeCheckBox, 'Optional', this.columnHeaders.RequiresServiceCoverWasteInd, 1);
        this.riTable.AddTableField('RequiresWasteTransferNotesInd', MntConst.eTypeCheckBox, 'Optional', this.columnHeaders.RequiresWasteTransferNotesInd, 1);
        this.riTable.AddTableField('WasteRegulatoryCarrierName', MntConst.eTypeText, 'Optional', this.columnHeaders.WasteRegulatoryCarrierName, 25);
        this.riTable.AddTableField('WasteRegulatoryRegistrationNum', MntConst.eTypeText, 'Optional', this.columnHeaders.WasteRegulatoryRegistrationNum, 25);
        this.buildTable();
    }
    private buildTable(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('search.sortby', 'SeasonalTemplateNumber');
        this.inputParams.parentMode = this.parentMode;
        this.inputParams.search = search;
        this.inputParams.module = this.queryParams.module;
        this.inputParams.method = this.queryParams.method;
        this.inputParams.operation = this.queryParams.operation;
        this.riTable.loadTableData(this.inputParams);
    }

    public updateView(params?: any): void {
        if (params) {
            this.inputParams = params;
            this.parentMode = this.inputParams.parentMode;
        }
        if (this.inputParams && this.inputParams.showAddNew !== undefined) {
            this.showAddNew = this.inputParams.showAddNew;
        }
        this.buildTableColumns();
    }
    public selectedData(event: any): void {
        let returnObj: Object;
        switch (this.parentMode) {
            case 'LookUp':
                returnObj = {
                    RegulatoryAuthorityNumber: event['row']['RegulatoryAuthorityNumber'],
                    RegulatoryAuthorityName: event['row']['RegulatoryAuthorityName']
                };
                break;
            default:
                returnObj = {
                    RegulatoryAuthorityNumber: event['row']['RegulatoryAuthorityNumber']
                };
                break;
        }
        this.emitSelectedData(returnObj);
    }
    public onAddNew(): void {
        this.ellipsis.onAddNew(true);
        this.ellipsis.closeModal();
    }
}
