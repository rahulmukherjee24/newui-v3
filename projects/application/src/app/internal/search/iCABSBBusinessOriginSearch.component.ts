import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Utils } from './../../../shared/services/utility';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { TableComponent } from './../../../shared/components/table/table';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { RiExchange } from '../../../shared/services/riExchange';
import { MntConst } from './../../../shared/services/riMaintenancehelper';


@Component({
    templateUrl: 'iCABSBBusinessOriginSearch.html'
})

export class BusinessOriginSearchComponent extends SelectedDataEvent implements OnInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    private riExchange: RiExchange;
    private utils: Utils;
    private serviceConstants: ServiceConstants;
    private formBuilder: FormBuilder;
    private parentMode: string;
    private isActiveInd: boolean;
    private queryParams: any = {
        operation: 'Business/iCABSBBusinessOriginSearch',
        module: 'segmentation',
        method: 'prospect-to-contract/search'
    };
    private inputParams: any = {};
    public pageId: string = '';
    public itemsPerPage: number = 10;
    public page: number = 1;
    public uiForm: FormGroup;
    public isShowAddNew: boolean;
    public pageTitle: string = 'Business Origin Search';

    constructor(injector: Injector) {
        super();
        this.injectService(injector);
        this.pageId = PageIdentifier.ICABSBINFESTATIONLEVELSEARCH;
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
        this.riExchange = null;
    }

    private injectService(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
    }

    //Called during ellipsis search
    public updateView(params?: any): void {
        if (params) {
            this.parentMode = params.parentMode;
            this.isShowAddNew = params.showAddNew;
            this.isActiveInd = params.ActiveInd;
            this.buildTable();
        }
    }

    //Method to set filter values for table
    public buildTable(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set('LanguageCode', this.riExchange.LanguageCode());
        if (this.parentMode === 'LookUp') {
            search.set('ActiveInd', 'true');
        }
        search.set('search.sortby', 'BusinessOriginCode');
        this.inputParams.parentMode = this.parentMode;
        this.inputParams.search = search;
        this.inputParams.module = this.queryParams.module;
        this.inputParams.method = this.queryParams.method;
        this.inputParams.operation = this.queryParams.operation;
        this.buildTableColumns();
        this.riTable.loadTableData(this.inputParams);
    }

    //Method to build table columns
    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('BusinessOriginCode', MntConst.eTypeCode, 'Key', 'Code', 2);
        this.riTable.AddTableField('BusinessOriginSystemDesc', MntConst.eTypeText, 'Required', 'Description', 40);
    }

    //Method called on selecting a data from table
    public selectedData(event: any): void {
        let returnObj: any;
        switch (this.parentMode) {
            case 'LookUp':
                returnObj = {
                    'BusinessOriginCode': event.row['BusinessOriginCode'],
                    'BusinessOriginSystemDesc': event.row['BusinessOriginSystemDesc'],
                    'ActiveInd': event.row['ActiveInd']
                };
                break;
            default:
                returnObj = {
                    'BusinessOriginCode': event.row['BusinessOriginCode']
                };
                break;
        }
        this.emitSelectedData(returnObj);
    }

    public addNewRecord(event: any): void {
        this.emitSelectedData('AddNew');
    }
}
