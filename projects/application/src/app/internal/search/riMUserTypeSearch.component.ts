import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Utils } from './../../../shared/services/utility';
import { RiExchange } from './../../../shared/services/riExchange';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'riMUserTypeSearch.html'
})

export class UserTypeSearchComponent extends SelectedDataEvent implements OnInit, OnDestroy {
   @ViewChild('riTable') riTable: TableComponent;
    private formBuilder: FormBuilder;
    private utils: Utils;
    private riExchange: RiExchange;
    private serviceConstants: ServiceConstants;
    private parentMode: string;
    private queryParams: Object = {
        operation: 'Model/riMUserTypeSearch',
        module: 'user',
        method: 'it-functions/ri-model'
    };
    public pageTitle: string;
    public pageId: string = '';
    public isShowAddNew: boolean = false;
    public itemsPerPage: number = 10;
    public uiForm: FormGroup;
    public controls: any[] = [];
    public inputParams: any = {};

    constructor(injector: Injector) {
        super();
        this.injectServices(injector);
        this.pageId = PageIdentifier.RIMUSERTYPESEARCH;
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.pageTitle = 'User Type Search';
        this.parentMode = this.riExchange.getParentMode();
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
        this.riExchange = null;
    }

    public injectServices(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
    }

    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('UserTypeCode', MntConst.eTypeText, 'Key', 'Code', 3);
        this.riTable.AddTableField('UserTypeDescription', MntConst.eTypeText, 'Required', 'Description', 20);
        this.buildTable();
    }

    public buildTable(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.inputParams.parentMode = this.parentMode;
        this.inputParams.search = search;
        this.inputParams.module = this.queryParams['module'];
        this.inputParams.method = this.queryParams['method'];
        this.inputParams.operation = this.queryParams['operation'];
        this.riTable.loadTableData(this.inputParams);
    }

    //On clicking of table row
    public tableRowClick(event: any): void {
        let vntReturnData: Object, strProducts: string = '', returnObj: Object = {};
        vntReturnData = event.row;
        returnObj['UserTypeCode'] = vntReturnData['UserTypeCode'];
        switch (this.parentMode) {
            case 'LookUp':
                returnObj['UserTypeDescription'] = vntReturnData['UserTypeDescription'];
                break;
            default:
                returnObj['UserTypeCode'] = vntReturnData['UserTypeCode'];
                break;
        }
        this.emitSelectedData(returnObj);
    }

    // Add New Click event
    public onAddNew(): void {
        this.emitSelectedData('AddModeOn');
    }

    //Updating View to Maintenance screen
    public updateView(params: any): void {
        if (params.parentMode) {
            this.parentMode = params.parentMode;
        }
        if (params.isShowAddNew) {
            this.isShowAddNew = params.isShowAddNew;
        }
        this.buildTableColumns();
    }
}
