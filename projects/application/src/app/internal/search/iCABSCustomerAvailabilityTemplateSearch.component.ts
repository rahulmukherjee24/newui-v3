import { Component, OnDestroy, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Utils } from './../../../shared/services/utility';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'iCABSCustomerAvailabilityTemplateSearch.html'
})

export class CustomerAvailabilityTemplateSearchComponent extends SelectedDataEvent implements OnDestroy {
    @ViewChild('customerAvailabilityTable') customerAvailabilityTable: TableComponent;
    public itemsPerPage: string = '10';
    public pageId: string = '';
    public queryParams: any = {
        operation: 'Business/iCABSCustomerAvailabilityTemplateSearch',
        module: 'template',
        method: 'service-planning/search'
    };
    public page: number = 1;
    public pageTitle: string = 'Customber Availability Template Search';
    public columns: Array<any> = [
        { title: 'Code', name: 'CustomerAvailTemplateID', type: MntConst.eTypeCode },
        { title: 'Description', name: 'CustomerAvailTemplateDesc', type: MntConst.eTypeText }
    ];

    constructor(
        private serviceConstants: ServiceConstants,
        private utils: Utils
    ) {
        super();
        this.pageId = PageIdentifier.ICABSCUSTOMERAVAILABILITYTEMPLATESEARCH;
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
    }

    public updateView(params?: any): void {
        if (params && params.parentMode) {
            this.queryParams.parentMode = params.parentMode;
        }
        this.getTablePageData();
    }

    public getTablePageData(): void {
        let search: any = new QueryParams();
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        search.set(this.serviceConstants.Action, '0');
        this.queryParams.search = search;
        this.customerAvailabilityTable.loadTableData(this.queryParams);
    }

    public selectedData(event: any): void {
        let returnObj: any;
        let vntReturnData = event.row;
        returnObj = {
            'CustomerAvailTemplateID': vntReturnData.CustomerAvailTemplateID
        };
        if (this.queryParams.parentMode === 'LookUp') {
            returnObj['CustomerAvailTemplateDesc'] = vntReturnData.CustomerAvailTemplateDesc;

        }
        this.emitSelectedData(returnObj);
    }
}
