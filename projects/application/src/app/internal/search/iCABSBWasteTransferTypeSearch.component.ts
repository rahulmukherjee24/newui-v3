import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Utils } from './../../../shared/services/utility';
import { RiExchange } from './../../../shared/services/riExchange';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { PageIdentifier } from './../../base/PageIdentifier';
import { TableComponent } from './../../../shared/components/table/table';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';

@Component({
    templateUrl: 'iCABSBWasteTransferTypeSearch.html'
})

export class WasteTransferTypeSearchComponent extends SelectedDataEvent implements OnInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    private formBuilder: FormBuilder;
    private utils: Utils;
    private riExchange: RiExchange;
    private serviceConstants: ServiceConstants;
    private parentMode: string;
    private queryParams: Object = {
        operation: 'Business/iCABSBWasteTransferTypeSearch',
        module: 'waste',
        method: 'service-delivery/search'
    };
    public pageTitle: string;
    public pageId: string = '';
    public isTrProductCode: boolean = true;
    public isShowAddNew: boolean = false;
    public isCode: string = 'Code';
    public itemsPerPage: number = 10;
    public uiForm: FormGroup;
    public controls: any[] = [];
    public inputParams: any = {};

    constructor(injector: Injector) {
        super();
        this.injectServices(injector);
        this.pageId = PageIdentifier.ICABSBWASTETRANSFERTYPESEARCH;
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group({});
        this.riExchange.renderForm(this.uiForm, this.controls);
        this.pageTitle = 'Waste Transfer Type Search';
        this.parentMode = this.riExchange.getParentMode();
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.formBuilder = null;
        this.riExchange = null;
    }

    public injectServices(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
    }

    public buildTableColumns(): void {
        this.riTable.clearTable();
        this.riTable.AddTableField('WasteTransferTypeCode', MntConst.eTypeCode, 'Key', 'Waste Transfer Type Code', 20);
        this.riTable.AddTableField('WasteTransferTypeDesc', MntConst.eTypeText, 'Required', 'Description', 40);
        this.riTable.AddTableField('WasteConsignmentNoteRequiredInd', MntConst.eTypeCheckBox, 'Required', 'Waste Consignment Note Required', 20);
        this.buildTable();
    }

    public buildTable(): void {
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.inputParams.parentMode = this.parentMode;
        this.inputParams.search = search;
        this.inputParams.module = this.queryParams['module'];
        this.inputParams.method = this.queryParams['method'];
        this.inputParams.operation = this.queryParams['operation'];
        this.riTable.loadTableData(this.inputParams);
    }

    public tableRowClick(event: any): void {
        let vntReturnData: Object, strProducts: string = '', returnObj: Object = {};
        vntReturnData = event.row;
        switch (this.parentMode) {
            case 'LookUp':
                returnObj['WasteTransferTypeCode'] = vntReturnData['WasteTransferTypeCode'];
                returnObj['WasteTransferTypeDesc'] = vntReturnData['WasteTransferTypeDesc'];
                break;

            default:
                returnObj['WasteTransferTypeCode'] = vntReturnData['WasteTransferTypeCode'];
                break;
        }
        this.emitSelectedData(returnObj);
    }

    public updateView(params: any): void {
        if (params.parentMode) {
            this.parentMode = params.parentMode;
        }
        this.buildTableColumns();
    }
}
