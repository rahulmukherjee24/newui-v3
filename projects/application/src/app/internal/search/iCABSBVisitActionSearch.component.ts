import { Component, OnInit, OnDestroy, AfterViewInit, Input, Output, Renderer, EventEmitter, ViewChild, Injector } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs/Subscription';

import { Utils } from './../../../shared/services/utility';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ServiceConstants } from './../../../shared/constants/service.constants';
import { RiExchange } from './../../../shared/services/riExchange';
import { DropdownComponent } from '../../../shared/components/dropdown/dropdown';
import { HttpService } from '../../../shared/services/http-service';
import { TableComponent } from './../../../shared/components/table/table';
import { ModalAdvService } from './../../../shared/components/modal-adv/modal-adv.service';
import { SelectedDataEvent } from './../../../shared/events/ellipsis-event-emitter';
import { AppModuleRoutes, ServiceDeliveryModuleRoutes } from './../../base/PageRoutes';

@Component({
    templateUrl: 'iCABSBVisitActionSearch.html'
})
export class VisitActionSearchComponent extends SelectedDataEvent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riTable') riTable: TableComponent;
    public pageId: string = '';
    public inputParams: any = {};
    public page: number = 1;
    public itemsPerPage: number = 10;
    public columns: Array<any> = [];
    public rowmetadata: Array<any> = [];
    public rows: Array<any> = [];

    public formBuilder: FormBuilder;
    public utils: Utils;
    public serviceConstants: ServiceConstants;
    public riExchange: RiExchange;
    public uiForm: FormGroup;
    public router: Router;
    public parentMode: string;
    public modalAdvService: ModalAdvService;
    public activatedRoute: ActivatedRoute;
    public activatedRouteSubscription: Subscription;

    public controls: Array<any> = [
        { name: 'VisitTypeCode', disabled: true },
        { name: 'VisitTypeDesc', disabled: true },
        { name: 'menu' }
    ];

    constructor(injector: Injector, private renderer: Renderer) {
        super();
        this.injectServices(injector);
        this.pageId = PageIdentifier.ICABSBPRODUCTCOVERSEARCH;
    }

    public injectServices(injector: Injector): void {
        this.formBuilder = injector.get(FormBuilder);
        this.utils = injector.get(Utils);
        this.serviceConstants = injector.get(ServiceConstants);
        this.riExchange = injector.get(RiExchange);
        this.router = injector.get(Router);
        this.modalAdvService = injector.get(ModalAdvService);
        this.activatedRoute = injector.get(ActivatedRoute);
    }

    ngOnInit(): void {
        this.uiForm = this.formBuilder.group(this.controls);
        this.riExchange.renderForm(this.uiForm, this.controls);
    }

    ngAfterViewInit(): void {
        this.activatedRouteSubscription = this.activatedRoute.queryParams
            .subscribe((param: any) => {
                if (param && param.fromPageNav) {
                    let params: any = {
                        parentMode: this.riExchange.getParentMode(),
                        VisitTypeCode: this.riExchange.getParentHTMLValue('ProductCode'),
                        VisitTypeDesc: this.riExchange.getParentHTMLValue('ProductDesc')
                    };
                    this.utils.setTitle('Visit Action Search');
                    this.updateView(param);
                }
            });
    }

    ngOnDestroy(): void {
        this.formBuilder = null;
        this.utils = null;
        this.serviceConstants = null;
        this.riExchange = null;
        this.router = null;
        this.activatedRoute = null;
        if (this.activatedRouteSubscription) {
            this.activatedRouteSubscription.unsubscribe();
        }
    }


    public updateView(params?: any): void {
        this.parentMode = params.parentMode;
        this.inputParams = params;
        this.uiForm.controls['VisitTypeCode'].setValue(this.inputParams.VisitTypeCode);
        this.uiForm.controls['VisitTypeDesc'].setValue(this.inputParams.VisitTypeDesc);
        this.loadTable();
    }

    public selectedData(obj: any): void {
        let returnObj = {
            SystemVisitActionCode: obj.row.SystemVisitActionCode,
            SystemVisitActionDesc: obj.row.SystemVisitActionDesc
        };
        switch (this.parentMode) {
            case 'VisitType':
                this.router.navigate([AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSBVISITACTIONMAINTENANCE],
                    {
                        queryParams: {
                            parentMode: 'Search',
                            VisitTypeCode: this.riExchange.riInputElement.GetValue(this.uiForm, 'VisitTypeCode'),
                            VisitTypeDesc: this.riExchange.riInputElement.GetValue(this.uiForm, 'VisitTypeDesc'),
                            SystemVisitActionCode: returnObj.SystemVisitActionCode
                        }
                    });
                break;
            case 'LookUp':
                this.emitSelectedData(returnObj);
                break;
            default:
                this.emitSelectedData(returnObj);
                break;
        }
    }

    public loadTable(): void {
        let search = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        if (this.inputParams.VisitTypeCode) { search.set('VisitTypeCode', this.inputParams.VisitTypeCode); }

        let xhr = {
            operation: 'Business/iCABSBVisitActionSearch',
            module: 'service',
            method: 'service-delivery/search',
            search: search
        };
        this.columns = [];
        this.columns.push({ name: 'SystemVisitActionCode', title: 'Visit Actions', size: 2 });
        this.columns.push({ name: 'SystemVisitActionDesc', title: 'Description', size: 40 });

        this.riTable.loadTableData(xhr);
    }
    public menuOnChange(obj: any): void {
        switch (obj.target.value) {
            case 'AddVisitAction':
                this.cmdAddVisitActionOnclick();
                break;
        }
    }
    public cmdAddVisitActionOnclick(): void {
        this.router.navigate([AppModuleRoutes.SERVICEDELIVERY + ServiceDeliveryModuleRoutes.ICABSBVISITACTIONMAINTENANCE],
            {
                queryParams: {
                    parentMode: 'SearchAdd',
                    VisitTypeCode: this.riExchange.riInputElement.GetValue(this.uiForm, 'VisitTypeCode'),
                    VisitTypeDesc: this.riExchange.riInputElement.GetValue(this.uiForm, 'VisitTypeDesc')
                }
            });
    }
}


@Component({
    selector: 'icabs-visit-action-search',
    template: ` <icabs-dropdown #customDropDown [itemsToDisplay]="displayFields" [disabled]="isDisabled" [isRequired]="isRequired" [triggerValidate]="isTriggerValidate" [active]="active" (selectedValue)="onDataReceived($event)" [isFirstItemSelected]="isFirstItemSelected"> </icabs-dropdown> `
})
export class VisitActionSearchDropDownComponent implements OnInit, OnDestroy {
    @ViewChild('customDropDown') public customDropDown: DropdownComponent;
    @Input() public inputParams: any;
    @Input() public isDisabled: boolean = false;
    @Input() public isFirstItemSelected: boolean;
    @Input() public active: any;
    @Input() public isRequired: boolean = false;
    @Input() public isTriggerValidate: boolean;
    @Output() getDefaultInfo = new EventEmitter();
    @Output() receivedVisitActionSearch: EventEmitter<any> = new EventEmitter();
    @Output() receivedVisitActionAdd: EventEmitter<any> = new EventEmitter();
    public requestdata: Array<any>;

    public displayFields: Array<string> = ['VisitAction.SystemVisitActionCode', 'SystemVisitActionLang.SystemVisitActionDesc'];
    public httpSubscription: Subscription;

    constructor(
        private serviceConstants: ServiceConstants,
        private httpService: HttpService,
        private utils: Utils,
        private riExchange: RiExchange) {
    }

    ngOnInit(): void {
        this.fetchData();
    }

    ngOnDestroy(): void {
        this.serviceConstants = null;
        this.utils = null;
        this.httpSubscription = null;
    }

    public fetchData(): void {
        let search = new QueryParams();
        search.set(this.serviceConstants.Action, '0');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        if (this.inputParams) { search.set('VisitTypeCode', this.inputParams.VisitTypeCode); }

        let xhr = {
            operation: 'Business/iCABSBVisitActionSearch',
            module: 'service',
            method: 'service-delivery/search',
            search: search
        };

        this.httpService.xhrGet(xhr.method, xhr.module, xhr.operation, xhr.search).then(data => {
            this.requestdata = data.records;
            if (this.isFirstItemSelected) {
                this.getDefaultInfo.emit({ totalRecords: this.requestdata ? this.requestdata.length : 0, firstRow: (this.requestdata && this.requestdata.length > 0) ? this.requestdata[0] : {} });
            }
            if (!data.records) { return; }
            this.customDropDown.updateComponent(data.records);
        });
    }

    public onDataReceived(obj: any): void {
        this.receivedVisitActionSearch.emit(obj.value);
    }
}
