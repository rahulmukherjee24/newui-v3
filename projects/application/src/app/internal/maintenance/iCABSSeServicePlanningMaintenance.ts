
import { OnInit, Injector, Component, ElementRef, ViewChild } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from '../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { BranchServiceAreaSearchComponent } from '../../../app/internal/search/iCABSBBranchServiceAreaSearch';
import { EllipsisComponent } from '../../../shared/components/ellipsis/ellipsis';
import { BranchSearchComponent } from '../../../app/internal/search/iCABSBBranchSearch';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { ServiceTypeSearchComponent } from '../../../app/internal/search/iCABSBServiceTypeSearch.component';
import { DropdownStaticComponent } from '../../../shared/components/dropdown-static/dropdownstatic';
import { AppModuleRoutes, ContractManagementModuleRoutes, InternalGridSearchSalesModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalMaintenanceApplicationModuleRoutes, InternalGridSearchServiceModuleRoutes, GoogleMapPagesModuleRoutes } from '../../../app/base/PageRoutes';


@Component({
    templateUrl: 'iCABSSeServicePlanningMaintenance.html'
})

export class ServicePlanningMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('branchserviceareacodeEllipsis') branchserviceareacodeEllipsis: EllipsisComponent;
    @ViewChild('branchDropdown') branchDropdown: BranchSearchComponent;
    @ViewChild('serviceTypeCodeDropDown') public serviceTypeCodeDropDown: ServiceTypeSearchComponent;
    @ViewChild('contractTypeDropdown') contractTypeDropdown: DropdownStaticComponent;
    @ViewChild('selCustomerIndicationNumber') selCustomerIndicationNumber: DropdownStaticComponent;
    @ViewChild('visitTypeFilterDropdown') visitTypeFilterDropdown: DropdownStaticComponent;
    @ViewChild('confirmedStatus') confirmedStatus: DropdownStaticComponent;
    @ViewChild('behindStatus') behindStatus: DropdownStaticComponent;
    @ViewChild('visitStatus') visitStatus: DropdownStaticComponent;
    @ViewChild('startDate') startDate;

    private vBusinessCode: string;
    private isVEnableInstallsRemovals: boolean = false;
    private isVEnablePostcodeDefaulting: boolean = false;
    private isVEnableWED: boolean = false;
    private vUnplannedDesc: string;
    private vInPlanningDesc: string;
    private vCancelledDesc: string;
    private vPlannedDesc: string;
    private isVEnableServiceCoverDispLev: boolean = false;
    private isVSAreaSeqGroupAvail: boolean = false;
    private vCustomerIndicationNumber: string;

    private lookUpSubscription: Subscription;
    private routeParams: any;

    //iCABSSeServicePlanningMaintenance1 unmatched variables
    private vVisitedDesc: string;
    private vEscapeFunction: string;
    private vContractTypeDesc: string;
    private vContractTypeProdDesc: string;
    private vContractTypeConDesc: string;
    private vContractTypeJobDesc: string;
    private vRouteOptimisation: boolean = false;
    private vEndofWeekDay: number;
    private vEndofWeekDate: string;
    private vSCEnableTechDiary: boolean = false;
    private sub: Subscription;

    private queryParams: any = {
        method: 'service-planning/maintenance',
        module: 'planning',
        operation: 'Service/iCABSSeServicePlanningMaintenance'
    };
    //iCABSSeServicePlanningMaintenance2 unmatched variables

    public selCustomerIndicationNumberDropDown: Array<Object> = [];
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BranchNumber', disabled: true, required: true, type: MntConst.eTypeInteger },
        { name: 'ServBranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchName', disabled: true, type: MntConst.eTypeText },
        { name: 'StartDate', type: MntConst.eTypeDate },
        { name: 'EndDate', type: MntConst.eTypeDate },
        { name: 'ServicePlanNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'ConfApptOnly' },
        { name: 'NegBranchNumber', type: MntConst.eTypeInteger },
        { name: 'InServiceTypeCode', type: MntConst.eTypeCode },
        { name: 'ServiceTypeDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractNumberSearch', type: MntConst.eTypeCode },
        { name: 'VisitTypeFilter' },
        { name: 'ContractTypeFilter', value: 'All' },
        { name: 'selCustomerIndicationNumber' },
        { name: 'ContractNameSearch', type: MntConst.eTypeText, commonValidator: true },
        { name: 'ConfirmedStatus' },
        { name: 'TownSearch', type: MntConst.eTypeText, commonValidator: true },
        { name: 'selBehindStatus' },
        { name: 'PostCode' },
        { name: 'selVisitStatus' },
        { name: 'TotalNoOfCalls', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalNoOfExchanges', disabled: true, type: MntConst.eTypeInteger },
        { name: 'TotalTime', disabled: true, type: MntConst.eTypeText },
        { name: 'TotalNettValue', disabled: true, type: MntConst.eTypeCurrency },
        { name: 'ContractNumber', disabled: true, type: MntConst.eTypeCode },
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', disabled: true, type: MntConst.eTypeInteger },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode },
        { name: 'ProductDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'GetWarnMessage', disabled: true, type: MntConst.eTypeText },
        { name: 'ServiceTypeCode' },
        { name: 'TownName' },
        { name: 'AllCustomerIndicationNumber' },
        { name: 'VisitStatus' },
        { name: 'BehindStatus' },
        { name: 'EmployeeCode' },
        { name: 'EmployeeCode1' },
        { name: 'EmployeeCode2' },
        { name: 'EmployeeSurname0' },
        { name: 'EmployeeSurname1' },
        { name: 'EmployeeSurname2' },
        { name: 'BranchServiceAreaDesc' },
        { name: 'NegBranchName' },
        { name: 'AllBranchServiceAreas' },
        { name: 'ExportReport' },
        { name: 'BranchNumberText' },
        { name: 'ExlFromOptInd' },
        { name: 'ExlFromOptRowID' },
        { name: 'CustomerIndicationNumber' },
        { name: 'BranchServiceAreaCount' },
        { name: 'BatchProcessInformation' }
    ];
    public parentMode: string;
    public cmdPlanCancelText: string;
    public uiDisplayObject: any = {
        tdServicePlanNumber: false,
        trNewPlan: true,
        tdUnplannedTotals: false,
        TechDiaryView: false,
        BatchProcessInformation: false,
        cmdShowFilterText: false,
        trAdjustPlan: false,
        pageLoad: true
    };
    public gridParams: any = {
        totalRecords: 0,
        pageCurrent: 1,
        itemsPerPage: 10,
        riGridMode: 0
    };

    public ellipsisConfig: any = {
        branchServiceAreaCode: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp-EmpManpowerList',
                'currentContractType': this.riExchange.getCurrentContractType(),
                'currentContractTypeURLParameter': this.riExchange.getCurrentContractTypeLabel()
            },
            contentComponent: BranchServiceAreaSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        }
    };
    public dropDownConfig: any = {
        branch: {
            childConfigParams: {
                'parentMode': 'LookUp-NegBranch'
            },
            contentComponent: BranchSearchComponent,
            disabled: false
        }
    };
    public pageParams: any = {
        servicebranchSelected: { id: '', text: '' },
        ServiceTypeCodeSelected: { id: '', text: '' },
        contractTypeDropdownValues: [],
        customerIndicationNumberDropdownValues: [],
        visitTypeFilterDropdownValues: [],
        confirmedStatusDropdownValues: [],
        behindStatusDropdownValues: [],
        visitStatusDropdownValues: [{ value: 'All', text: 'All' }, { value: 'NS', text: 'Not Scheduled' }]
    };
    public isRefreshDisabled: boolean = false;
    public blnRefreshRequired: boolean = true;
    public serviceTypeCodeSearchParams: any = {
        params: {
            'parentMode': 'LookUp-SC',
            'businessCode': this.utils.getBusinessCode(),
            'countryCode': this.utils.getCountryCode()
        }
    };

    public disabledControls: any = {
        ServiceTypeCode: false
    };

    public cmdShowFilterText: string = 'Hide Filters';
    public currentContractTypeURLParameter: string = '';
    public uiDisabled: any = {
        cmdSummary: true,
        BranchNumberText: true,
        cmdVisitConflicts: true
    };
    public startDateDt: any;
    public endDateDt: any;
    public isHidePagination: boolean = true;
    public batchInformation: any = '';

    constructor(injector: Injector, private route: ActivatedRoute, private el: ElementRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGMAINTENANCE;
        this.pageTitle = 'Create Service Plan';
        this.browserTitle = 'Service Planning';
        this.vBusinessCode = this.utils.getBusinessCode();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.triggerFetchSysChar();
        this.sub = this.route.queryParams.subscribe(
            (params: any) => {
                this.routeParams = params;
            }
        );
        this.setControlValue('StartDate', this.startDateDt);
        this.setControlValue('EndDate', this.endDateDt);
        this.setControlValue('ExportReport', 0);
        this.setControlValue('ExlFromOptRowID', '');
        this.riGrid.DescendingSort = false;
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.startDateDt = this.getControlValue('StartDate');
            this.endDateDt = this.getControlValue('EndDate');
            if (this.getControlValue('BranchServiceAreaCode')) {
                this.uiDisplayObject.TechDiaryView = true;
            }
        } else {
            this.startDateDt = this.utils.formatDate(new Date());
            this.endDateDt = this.utils.formatDate(new Date());
        }
    }

    ngOnDestroy(): void {
        this.lookUpSubscription.unsubscribe();
        this.sub.unsubscribe();
    }

    private onSysCharDataReceive(e: any): void {
        if (e.records && e.records.length) {
            this.isVEnableInstallsRemovals = e.records[0]['Required'];
            this.isVEnablePostcodeDefaulting = e.records[1]['Required'];
            this.isVEnableWED = e.records[2]['Required'];
            this.isVEnableServiceCoverDispLev = e.records[3]['Required'];
            this.vRouteOptimisation = e.records[4]['Required']; //vb1
            this.vSCEnableTechDiary = e.records[5]['Required']; //vb1
        }
        this.getPlanVisitStatusDesc();
    }

    private fetchSysChar(sysCharNumbers: any): any {
        let querySysChar: QueryParams = new QueryParams();
        querySysChar.set(this.serviceConstants.Action, '0');
        querySysChar.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        querySysChar.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        querySysChar.set(this.serviceConstants.SystemCharNumber, sysCharNumbers);
        return this.httpService.sysCharRequest(querySysChar);
    }

    private sysCharParameters(): string {
        let sysCharList: Array<any> = [
            this.sysCharConstants.SystemCharEnableInstallsRemovals,
            this.sysCharConstants.SystemCharEnablePostcodeDefaulting,
            this.sysCharConstants.SystemCharEnableWED,
            this.sysCharConstants.SystemCharEnableServiceCoverDisplayLevel,
            this.sysCharConstants.SystemCharEnableRouteOptimisationSoftwareIntegration,
            this.sysCharConstants.SystemCharEnableTechDiary
        ];
        return sysCharList.join(',');
    }

    private triggerFetchSysChar(): any {
        let sysCharNumbers: string = this.sysCharParameters();
        this.fetchSysChar(sysCharNumbers).subscribe((data) => {
            this.onSysCharDataReceive(data);
        });
    }

    //implementation for i_PlanVisitStatusDesc
    private getPlanVisitStatusDesc(): void {
        let lookupIP: any = [
            {
                'table': 'PlanVisitStatusLang',
                'query': {
                    'LanguageCode': this.utils.getDefaultLang()
                },
                'fields': ['PlanVisitStatusDesc', 'PlanVisitStatusCode']
            },
            {
                'table': 'PlanVisitStatus',
                'query': {},
                'fields': ['PlanVisitSystemDesc', 'PlanVisitStatusCode']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data.length) {
                if (data[0].length)
                    this.populatePlanVisitStatusDesc(data[0]);
                else
                    this.populatePlanVisitStatusDesc(data[1]);
            }
        });
    }

    private populatePlanVisitStatusDesc(data: Array<Object>): void {
        for (let i: number = 0; i < data.length; i++) {
            switch (data[i]['PlanVisitStatusCode']) {
                case 'U':
                    this.vUnplannedDesc = data[i]['PlanVisitSystemDesc'] || data[i]['PlanVisitStatusDesc'];
                    break;
                case 'I':
                    this.vInPlanningDesc = data[i]['PlanVisitSystemDesc'] || data[i]['PlanVisitStatusDesc'];
                    break;
                case 'C':
                    this.vCancelledDesc = data[i]['PlanVisitSystemDesc'] || data[i]['PlanVisitStatusDesc'];
                    break;
                case 'P':
                    this.vPlannedDesc = data[i]['PlanVisitSystemDesc'] || data[i]['PlanVisitStatusDesc'];
                    break;
                case 'V':
                    this.vVisitedDesc = data[i]['PlanVisitSystemDesc'] || data[i]['PlanVisitStatusDesc'];
                    break;
                default:
                    break;
            }
        }
        this.sAreaSeqGroup();
    }

    //operations for SAreaSeqGroup table
    private sAreaSeqGroup(): void {
        let lookupIP: any = [
            {
                'table': 'SAreaSeqGroup',
                'query': {},
                'fields': ['SequenceGroupNo']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data.length) {
                if (data[0].length)
                    this.isVSAreaSeqGroupAvail = true;
                else
                    this.isVSAreaSeqGroupAvail = false;
            }
            this.customerIndication();
        });
    }

    //operations for CustomerIndication table
    private customerIndication(): void {
        let lookupIP: any = [
            {
                'table': 'CustomerIndication',
                'query': { 'BusinessCode': this.utils.getBusinessCode() },
                'fields': ['CustomerIndicationNumber', 'CustomerIndicationDesc']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data.length) {
                data[0].forEach(element => {
                    this.selCustomerIndicationNumberDropDown.push({ text: element['CustomerIndicationDesc'], value: element['CustomerIndicationNumber'] });
                });
                this.pageParams.customerIndicationNumberDropdownValues = this.selCustomerIndicationNumberDropDown;
            }
            this.contractTypeLang();
        });
    }

    //operations for ContractTypeLang table
    private contractTypeLang(): void {
        let lookupIP: any = [
            {
                'table': 'ContractTypeLang',
                'query': {
                    'BusinessCode': this.businessCode,
                    'LanguageCode': this.riExchange.LanguageCode()

                },
                'fields': ['ContractTypeDesc', 'ContractTypeCode']
            }
        ];

        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0].length) {
                this.populateContractTypeDesc(data[0]);
            } else {
                this.contractType();
            }
        });
    }

    //operations for contractType table
    private contractType(): void {
        let lookupIP: any = [
            {
                'table': 'ContractType',
                'query': {
                    'BusinessCode': this.businessCode
                },
                'fields': ['ContractTypeDesc', 'ContractTypeCode']
            }
        ];

        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data && data[0].length) {
                this.populateContractTypeDesc(data[0]);
            }
        });
    }

    //populating contract type descriptions
    private populateContractTypeDesc(data: Array<Object>): void {
        for (let i = 0; i < data.length; i++) {
            switch (data[i]['ContractTypeCode']) {
                case 'C':
                    this.vContractTypeConDesc = data[i]['ContractTypeDesc'];
                    break;
                case 'J':
                    this.vContractTypeJobDesc = data[i]['ContractTypeDesc'];
                    break;
                case 'P':
                    this.vContractTypeProdDesc = data[i]['ContractTypeDesc'];
                    break;
            }
        }
        this.systemParameter();
    }

    //operations for systemParameter table
    private systemParameter(): void {
        let lookupIP: any = [
            {
                'table': 'SystemParameter',
                'query': {},
                'fields': ['SystemParameterEndOfWeekDay']
            }
        ];

        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            let newDate: Date = new Date();
            if (data && data[0].length && data[0][0]['SystemParameterEndOfWeekDay'] < 7) {
                this.vEndofWeekDay = data[0][0]['SystemParameterEndOfWeekDay'];
            } else {
                this.vEndofWeekDay = 1;
            }
            let weekDiff: number = (7 - newDate.getDay() + this.vEndofWeekDay);
            this.vEndofWeekDate = this.utils.formatDate(this.utils.addDays(newDate, weekDiff));
            this.firstLoadItems();
        });
    }

    //iCABSSeServicePlanningMaintenance1 window load items
    private firstLoadItems(): void {
        this.setControlValue('BranchNumber', this.utils.getBranchCode());
        this.setControlValue('BranchNumberText', this.utils.getBranchText());
        this.parentMode = this.riExchange.getParentMode();
        if (this.parentMode === 'ServicePlan') {
            this.cmdPlanCancelText = 'Remove From Plan';
            this.utils.setTitle('Confirmed Service Plan');
            this.disableControl('BranchServiceAreaCode', true);
            if (this.riExchange.getParentAttributeValue('BranchServiceArea')) {
                this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
                this.setControlValue('EmployeeSurname', this.riExchange.getParentAttributeValue('EmployeeSurname'));
            } else {
                this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
                this.riExchange.getParentHTMLValue('EmployeeSurname');
            }
            this.setControlValue('StartDate', this.riExchange.getParentAttributeValue('ServicePlanStartDate'));
            this.setControlValue('EndDate', this.riExchange.getParentAttributeValue('ServicePlanEndDate'));
            this.setControlValue('TalNoOfCalls', this.riExchange.getParentAttributeValue('ServicePlanNoOfCalls'));
            this.setControlValue('TotalNoOfExchanges', this.riExchange.getParentAttributeValue('ServicePlanNoOfExchanges'));
            this.setControlValue('TotalTime', this.riExchange.getParentAttributeValue('ServicePlanTime'));
            this.setControlValue('TotalNettValue', this.riExchange.getParentAttributeValue('ServicePlanNettValue'));
            this.uiDisplayObject.tdServicePlanNumber = true;
            this.uiDisplayObject.trNewPlan = false;
            this.uiDisplayObject.trAdjustPlan = true;
            this.uiDisplayObject.tdUnplannedTotals = false;
            this.loadGridData();
        } else if (this.parentMode === 'ProductivityReview') {
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
            let newDate: Date = new Date();
            this.setControlValue('StartDate', this.utils.formatDate(this.utils.addDays(newDate, 1)));
            this.setControlValue('EndDate', this.utils.formatDate(this.utils.addDays(newDate, 7)));
        } else {
            this.uiDisplayObject.trNewPlan = true;
            this.uiDisplayObject.trAdjustPlan = false;
        }
        if (this.isReturning()) {
            if (this.getControlValue('ServiceTypeCode'))
                this.pageParams.ServiceTypeCodeSelected = { id: this.getControlValue('ServiceTypeCode'), text: this.getControlValue('ServiceTypeDesc') };
            if (this.getControlValue('NegBranchNumber'))
                this.pageParams.servicebranchSelected = { id: this.getControlValue('NegBranchNumber').toString(), text: this.utils.getBranchText(this.getControlValue('NegBranchNumber').toString()) };
            this.buildGrid();
        } else {
            this.setControlValue('ContractTypeFilter', 'All');
            this.setContractTypedropDownValues();
            this.setConfirmStatusDropdownValues();
            this.setBehindStatusDropdownValues();
            this.setVisitTypeFilterDropdownValues();
            this.setDropDownDefaultValues();
        }
        this.uiDisplayObject.pageLoad = false;
    }

    private setDropDownDefaultValues(): void {
        if (!this.getControlValue('ConfirmedStatus')) this.setControlValue('ConfirmedStatus', 'All');
        if (!this.getControlValue('VisitTypeFilter')) this.setControlValue('VisitTypeFilter', 'All');
        if (!this.getControlValue('CustomerIndicationNumber')) this.setControlValue('CustomerIndicationNumber', 'All');
        if (!this.getControlValue('BehindStatus')) this.setControlValue('BehindStatus', 'All');
        if (!this.getControlValue('VisitStatus')) this.setControlValue('VisitStatus', 'All');
    }

    private setContractTypedropDownValues(): void {

        //load contract type dropdwon values
        this.pageParams.contractTypeDropdownValues.push({ value: 'All', text: 'All' });
        if (this.vContractTypeConDesc)
            this.pageParams.contractTypeDropdownValues.push({ value: 'C', text: this.vContractTypeConDesc });
        if (this.vContractTypeJobDesc)
            this.pageParams.contractTypeDropdownValues.push({ value: 'J', text: this.vContractTypeJobDesc });
        if (this.vContractTypeProdDesc)
            this.pageParams.contractTypeDropdownValues.push({ value: 'P', text: this.vContractTypeProdDesc });
        //this.setControlValue('ContractTypeFilter', 'All');
    }

    private setVisitTypeFilterDropdownValues(): void {
        this.pageParams.visitTypeFilterDropdownValues.push({ value: 'All', text: 'All' });
        this.pageParams.visitTypeFilterDropdownValues.push({ value: 'Routine', text: 'Routine' });
        this.pageParams.visitTypeFilterDropdownValues.push({ value: 'CancelRemove', text: 'Cancel And Remove' });
        this.pageParams.visitTypeFilterDropdownValues.push({ value: 'Urgent', text: 'Urgent' });
        this.pageParams.visitTypeFilterDropdownValues.push({ value: 'Upgrade', text: 'Upgrade' });
        this.pageParams.visitTypeFilterDropdownValues.push({ value: 'FollowUp', text: 'Follow Up' });
    }

    private setConfirmStatusDropdownValues(): void {
        this.pageParams.confirmedStatusDropdownValues.push({ value: 'All', text: 'All' });
        this.pageParams.confirmedStatusDropdownValues.push({ value: 'AP', text: 'Confirmed Appointments' });
    }

    private setBehindStatusDropdownValues(): void {
        this.pageParams.behindStatusDropdownValues.push({ value: 'All', text: 'All' });
        this.pageParams.behindStatusDropdownValues.push({ value: 'Behind', text: 'Behind' });
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        let branchServiceAreaCode: any = this.getControlValue('BranchServiceAreaCode');
        let tempBranchFlag: any = branchServiceAreaCode.length;
        if (tempBranchFlag > 0) {
            tempBranchFlag = branchServiceAreaCode.indexOf(',');
            if (tempBranchFlag === -1) {
                tempBranchFlag = 1;
            } else {
                tempBranchFlag = 2;
            }
        }
        if (tempBranchFlag === 0 || tempBranchFlag > 1) {
            this.riGrid.AddColumn('GBranchServiceAreaCode', 'ServiceCover', 'GBranchServiceAreaCode', MntConst.eTypeCode, 4);
            this.riGrid.AddColumnAlign('GBranchServiceAreaCode', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('GBranchServiceAreaCode', true);
        }
        this.riGrid.AddColumn('GVisitDate', 'ServiceCover', 'GVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('GVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GVisitDate', true);

        this.riGrid.AddColumn('GStartTime', 'ServiceCover', 'GStartTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('GStartTime', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GStartTime', true);

        this.riGrid.AddColumn('GDurationService', 'ServiceCover', 'GDurationService', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('GDurationService', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GEndTime', 'ServiceCover', 'GEndTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnScreen('GEndTime', false);

        this.riGrid.AddColumn('GContractNumber', 'ServiceCover', 'GContractNumber', MntConst.eTypeText, 12);
        this.riGrid.AddColumnAlign('GContractNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GContractNumber', true);

        this.riGrid.AddColumn('GPremiseNumber', 'ServiceCover', 'GPremiseNumber', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('GPremiseNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GPremiseNumber', true);

        this.riGrid.AddColumn('GContractName', 'ServiceCover', 'GContractName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('GContractName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GContractName', true);

        this.riGrid.AddColumn('GPremiseName', 'ServiceCover', 'GPremiseName', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('GPremiseName', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GPremiseName', true);

        this.riGrid.AddColumn('GPremiseFullAddress', 'ServiceCover', 'GPremiseFullAddress', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('GPremiseFullAddress', false);

        this.riGrid.AddColumn('GPremiseContactTelephone', 'ServiceCover', 'GPremiseContactTelephone', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('GPremiseContactTelephone', false);

        this.riGrid.AddColumn('GProductCode', 'ServiceCover', 'GProductCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('GProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GProductCode', true);

        this.riGrid.AddColumn('GQuantity', 'ServiceCover', 'GQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('GQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GQuantity', true);

        this.riGrid.AddColumn('GServiceCreditValue', 'ServiceCover', 'GServiceCreditValue', MntConst.eTypeDecimal2, 15);
        this.riGrid.AddColumnAlign('GServiceCreditValue', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GServiceCreditValue', true);

        this.riGrid.AddColumn('GVisitType', 'ServiceCover', 'GVisitType', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('GVisitType', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GFreq', 'ServiceCover', 'GFreq', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnAlign('GFreq', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GRAGStatus', 'ServiceCover', 'GRAGStatus', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('GRAGStatus', false);

        this.riGrid.AddColumn('GNET', 'ServiceCover', 'GNET', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('GNET', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GNLT', 'ServiceCover', 'GNLT', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('GNLT', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GConfirmationRequired', 'ServiceCover', 'GConfirmationRequired', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('GConfirmationRequired', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('GConfirmationRequired', true);

        this.riGrid.AddColumn('GApptConfirmed', 'ServiceCover', 'GApptConfirmed', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnScreen('GApptConfirmed', false);

        this.riGrid.AddColumn('GLastVisitDate', 'ServiceCover', 'GLastVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnScreen('GLastVisitDate', false);

        this.riGrid.AddColumn('GVisitNote', 'ServiceCover', 'GVisitNote', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('GVisitNote', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('GSOS', 'ServiceCover', 'GSOS', MntConst.eTypeInteger, 10);
        this.riGrid.AddColumnScreen('GSOS', false);

        this.riGrid.AddColumn('GVisitInOpt', 'ServiceCover', 'GVisitInOpt', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('GVisitInOpt', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('GPlannerStatus', 'ServiceCover', 'GPlannerStatus', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('GPlannerStatus', false);

        this.riGrid.AddColumn('GSelection', 'ServiceCover', 'GSelection', MntConst.eTypeCheckBox, 1, false, '');
        this.riGrid.AddColumnAlign('GSelection', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
        this.loadGridData();
    }

    //riGrid_BeforeExecute functionalities
    private loadGridData(): void {
        let areaCode: any, allAreas: any, strPostData: any, vcError: any, allCustomerIndicationNumber: any, exportScheduledReport: any, dateTo: any, search: QueryParams = new QueryParams(), customerIndicationNumber: any;

        if (!this.validateDates()) {
            this.isHidePagination = true;
            this.riGrid.RefreshRequired();
            return;
        }

        this.blnRefreshRequired = this.riGrid.Update ? false : true;
        this.setControlValue('AllBranchServiceAreas', this.getControlValue('BranchServiceAreaCode') ? 'No' : 'Yes');
        if (this.getControlValue('ExportReport').toString() === '0') {
            exportScheduledReport = 'True';
        } else if (this.getControlValue('ExportReport').toString() === '1') {
            exportScheduledReport = 'False';
        }
        customerIndicationNumber = this.getControlValue('CustomerIndicationNumber') === 'All' ? '' : this.getControlValue('CustomerIndicationNumber');
        allCustomerIndicationNumber = this.getControlValue('CustomerIndicationNumber') === 'All' ? 'Yes' : 'No';

        dateTo = this.getControlValue('EndDate') ? this.getControlValue('EndDate') : dateTo = this.utils.formatDate(new Date());

        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.BusinessCode, this.businessCode());
        search.set(this.serviceConstants.CountryCode, this.countryCode());
        search.set(this.serviceConstants.LanguageCode, this.riExchange.LanguageCode());
        search.set(this.serviceConstants.PageSize, this.gridParams.itemsPerPage.toString());
        search.set(this.serviceConstants.PageCurrent, this.gridParams.pageCurrent.toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.GridMode, this.gridParams.riGridMode);
        search.set(this.serviceConstants.GridHandle, this.utils.randomSixDigitString());

        search.set('BranchNumber', this.getControlValue('BranchNumber'));
        search.set('StartDate', this.getControlValue('StartDate'));
        search.set('EndDate', dateTo);
        search.set('VisitTypeFilter', this.getControlValue('VisitTypeFilter'));
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('AllBranchServiceAreas', this.getControlValue('AllBranchServiceAreas'));
        search.set('NegBranchNumber', this.getControlValue('NegBranchNumber'));
        search.set('ServiceTypeCode', this.getControlValue('ServiceTypeCode'));
        search.set('ContractName', this.getControlValue('ContractNameSearch'));
        search.set('TownName', this.getControlValue('TownSearch'));
        search.set('PostCode', this.getControlValue('PostCode'));
        search.set('AllCustomerIndicationNumber', allCustomerIndicationNumber);
        search.set('ContractTypeFilter', this.getControlValue('ContractTypeFilter'));
        search.set('ConfirmedStatus', this.getControlValue('ConfirmedStatus'));
        search.set('VisitStatus', this.getControlValue('VisitStatus'));
        search.set('BehindStatus', this.getControlValue('BehindStatus'));
        search.set('ExportScheduledReport', exportScheduledReport);
        search.set('CustomerIndicationNumber', customerIndicationNumber);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.gridParams.pageCurrent = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridParams.totalRecords = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        this.setControlValue('ExlFromOptRowID', '');
                        this.uiDisplayObject.BatchProcessInformation = false;
                        this.batchInformation = '';
                        if (data['footer'] && data['footer']['rows'] && data['footer']['rows'][0]) {
                            let gridDataArr: any = data['footer']['rows'][0]['text'].split('|');
                            this.setControlValue('TotalNoOfCalls', gridDataArr[1]);
                            this.setControlValue('TotalNoOfExchanges', gridDataArr[2]);
                            this.setControlValue('TotalTime', gridDataArr[4]);
                            this.setControlValue('TotalNettValue', gridDataArr[5]);
                            data['footer'] = '';
                        }
                        this.riGrid.Execute(data);
                        if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                            this.isHidePagination = false;
                        else
                            this.isHidePagination = true;
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.gridParams.totalRecords = 1;
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private setStartDateErrorStatus(status: boolean): void {
        if (!this.startDate.randomId) return;
        if (status) {
            this.el.nativeElement.querySelector('#' + this.startDate.randomId).classList.remove('ng-valid');
            this.el.nativeElement.querySelector('#' + this.startDate.randomId).classList.add('ng-invalid');
            this.el.nativeElement.querySelector('#' + this.startDate.randomId).focus();
        } else {
            this.el.nativeElement.querySelector('#' + this.startDate.randomId).classList.add('ng-valid');
            this.el.nativeElement.querySelector('#' + this.startDate.randomId).classList.remove('ng-invalid');
        }
    }

    private validateDates(): boolean {
        let startDate: any, endDate: any, currentDate: any, validateDates: any = false;
        startDate = this.getControlValue('StartDate');
        endDate = this.getControlValue('EndDate');
        currentDate = new Date();
        this.setStartDateErrorStatus(false);
        if (startDate) {
            startDate = this.utils.convertDate(startDate);
            endDate = this.utils.convertDate(endDate);
            if (startDate > endDate) {
                this.startDate.validateDateField();
                this.setStartDateErrorStatus(true);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.PageSpecificMessage.servicePlanningMaintenance.startDateCannotBeGreaterThanEndDate));
            } else {
                validateDates = true;
            }
        } else if (startDate === '' && endDate === '') {
            if (startDate > currentDate) {
                this.setStartDateErrorStatus(true);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.PageSpecificMessage.servicePlanningMaintenance.startDateCannotBeGreaterThanToday));
            } else {
                validateDates = true;
            }
        } else {
            validateDates = true;
        }
        return validateDates;
    }

    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    public branchServiceAreaCodeEllispseReturnData(obj: any, call: boolean): void {
        if (call) {
            this.uiDisabled.cmdVisitConflicts = null;
            this.uiDisabled.cmdSummary = null;
            let branchServiceAreaCode: any = this.getControlValue('BranchServiceAreaCode');
            let employeeSurname: any = this.getControlValue('EmployeeSurname');
            branchServiceAreaCode = branchServiceAreaCode.split(',');
            employeeSurname = employeeSurname.split(',');
            if (branchServiceAreaCode[0] && branchServiceAreaCode[0].length > 0) {
                branchServiceAreaCode = this.getControlValue('BranchServiceAreaCode') + ',' + obj['BranchServiceAreaCode'];
                this.setControlValue('BranchServiceAreaCode', branchServiceAreaCode);
                employeeSurname = this.getControlValue('EmployeeSurname') + ',' + obj['EmployeeSurname'];
                this.setControlValue('EmployeeSurname', employeeSurname);
            } else {
                this.setControlValue('BranchServiceAreaCode', obj['BranchServiceAreaCode']);
                this.setControlValue('EmployeeSurname', obj['EmployeeSurname']);
                this.setControlValue('BranchServiceAreaDesc', obj['BranchServiceAreaDesc']);
                this.setControlValue('EmployeeCode', obj['EmployeeCode']);
            }
            this.branchServiceAreaCodeOnChange();
        }
    }

    public branchServiceAreaCodeOnChange(): void {
        let searchParams: QueryParams = new QueryParams();
        this.setControlValue('EmployeeCode', '');
        this.setControlValue('EmployeeCode1', '');
        this.setControlValue('EmployeeCode2', '');
        this.setControlValue('EmployeeSurname0', '');
        this.setControlValue('EmployeeSurname1', '');
        this.setControlValue('EmployeeSurname2', '');

        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.uiDisplayObject.TechDiaryView = false;
        } else {
            this.uiDisplayObject.TechDiaryView = true;
        }
        if (this.getControlValue('BranchServiceAreaCode')) {
            let postData: any = {};
            searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
            searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
            searchParams.set(this.serviceConstants.Action, '6');
            postData[this.serviceConstants.ActionType] = 'GetEmployeeSurname';
            postData['BranchNumber'] = this.utils.getBranchCode();
            postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
            postData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate')).toString();
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module,
                this.queryParams.operation, searchParams, postData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                            this.setControlValue('BranchServiceAreaCode', '');
                            this.setControlValue('EmployeeSurname', '');
                        } else {
                            this.setControlValue('EmployeeSurname', data['EmployeeSurname']);
                            let vEmployeeCodes: any, vEmployeeSurnames: any;
                            vEmployeeCodes = data['EmployeeCodes'].split(',');
                            vEmployeeSurnames = data['EmployeeSurname'].split(',');
                            if (vEmployeeCodes.length > 1) {
                                for (let i = 0; i < vEmployeeCodes.length; i++) {
                                    switch (i) {
                                        case 0:
                                            this.setControlValue('EmployeeCode', vEmployeeCodes[0]);
                                            this.setControlValue('EmployeeSurname0', vEmployeeSurnames[0]);
                                            break;
                                        case 1:
                                            this.setControlValue('EmployeeCode1', vEmployeeCodes[1]);
                                            this.setControlValue('EmployeeSurname1', vEmployeeSurnames[1]);
                                            break;
                                        case 2:
                                            this.setControlValue('EmployeeCode2', vEmployeeCodes[2]);
                                            this.setControlValue('EmployeeSurname2', vEmployeeSurnames[2]);
                                            break;
                                    }
                                }
                            } else if (vEmployeeCodes.length === 1) {
                                this.setControlValue('EmployeeCode', vEmployeeCodes[0]);
                                this.setControlValue('EmployeeSurname0', vEmployeeSurnames[0]);
                            }
                            if (data['EmployeeSurname'].trim().length < 0) {
                                this.setControlValue('EmployeeCode', '');
                                this.setControlValue('EmployeeSurname0', '');
                                this.setControlValue('EmployeeCode1', '');
                                this.setControlValue('EmployeeSurname1', '');
                                this.setControlValue('EmployeeCode2', '');
                                this.setControlValue('EmployeeSurname2', '');
                                this.setControlValue('BranchServiceAreaCode', '');
                            } else if (data['WarningMessageDesc'].trim().length > 0) {
                                this.modalAdvService.emitError(new ICabsModalVO(data['WarningMessageDesc']));
                            }
                            this.setControlValue('TotalNoOfCalls', '0');
                            this.setControlValue('TotalNoOfExchanges', '0');
                            this.setControlValue('TotalTime', '00:00');
                            this.setControlValue('TotalNettValue', '0');
                            this.riGrid.RefreshRequired();
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        } else {
            this.uiDisabled.cmdVisitConflicts = true;
            this.uiDisabled.cmdSummary = true;
            this.setControlValue('EmployeeSurname', '');
        }
    }

    public negBranchNumberOnChange(obj: any): void {
        if (obj) {
            this.setControlValue('NegBranchNumber', obj['BranchNumber']);
        } else {
            this.setControlValue('NegBranchNumber', '');
        }
        this.riGrid.RefreshRequired();
    }

    public cmdVisitConflictsOnClick(): void {
        //To Do: iCABSSeVisitConflictsGrid.htm
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
    }

    public refresh(): void {
        this.riGrid.RefreshRequired();
        this.buildGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridParams.pageCurrent = currentPage.value;
        this.riGrid.RefreshRequired();
        this.loadGridData();
    }

    public startDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('StartDate', value.value);
            if (!value.value)
                this.setStartDateErrorStatus(false);
        }
    }

    public endDateSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('EndDate', value.value);
            if (!value.value)
                this.setStartDateErrorStatus(false);
        }
    }

    public serviceTypeCodeChange(data: any): void {
        if (data) {
            this.setControlValue('InServiceTypeCode', data['ServiceTypeCode']);
            this.setControlValue('ServiceTypeCode', data['ServiceTypeCode']);
            this.setControlValue('ServiceTypeDesc', data['ServiceTypeDesc']);
        }
    }

    public dropDownMarkError(fieldName: string): boolean {
        return (this.uiForm.controls[fieldName].invalid && this.uiForm.controls[fieldName].touched);
    }

    public onContractTypeDropdownChange(data: any): void {
        this.setControlValue('ContractTypeFilter', data);
    }

    //implementation for selCustomerIndicationNumber_OnChange
    public oncustomerIndicationNumberChange(data: any): void {
        this.setControlValue('CustomerIndicationNumber', data);
    }

    public onVisitTypeFilterDropdownChange(data: any): void {
        this.setControlValue('VisitTypeFilter', data);
    }

    public onConfirmedStatusChange(data: any): void {
        this.setControlValue('ConfirmedStatus', data);
    }

    public onBehindStatusChange(data: any): void {
        this.setControlValue('BehindStatus', data);
    }

    public onVisitStatusChange(data: any): void {
        this.setControlValue('VisitStatus', data);
    }

    public updateExlFromOpt(): void {
        let searchParams: QueryParams = new QueryParams();
        let exlFromOptInd: string = 'yes';
        if (!this.getControlValue('ExlFromOptInd')) exlFromOptInd = 'no';
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        searchParams.set(this.serviceConstants.ActionType, 'UpdateExcFromOpt');
        searchParams.set('ExlFromOptRowID', this.getControlValue('ExlFromOptRowID'));
        searchParams.set('ExlFromOptInd', exlFromOptInd);
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, searchParams)
            .subscribe(
                (data) => {
                    this.riGrid.RefreshRequired();
                    this.loadGridData();
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
    }

    public planEvents(): void {
        let columnName: any = this.riGrid.CurrentColumnName;
        let currentRowIndex: any = this.riGrid.CurrentRow;
        let rowData: any = this.riGrid.bodyArray[currentRowIndex];
        let exlFromOptRowID: any;
        let vRowid: any = this.riGrid.Details.GetAttribute('GSelection', 'RowID');
        let currentColumnValue: any = this.riGrid.CurrentColumnValue;
        if (columnName) {
            switch (columnName) {
                case 'GVisitInOpt':
                    this.setControlValue('ExlFromOptRowID', vRowid);
                    if (this.riGrid.Details.GetAttribute('GVisitInOpt', 'AdditionalProperty') === 'No') {
                        this.setControlValue('ExlFromOptInd', false);
                    } else {
                        this.setControlValue('ExlFromOptInd', true);
                    }
                    this.updateExlFromOpt();
                    break;
                case 'GSelection':
                    let exlFromOptRowIDL: any = this.getControlValue('ExlFromOptRowID');
                    if (exlFromOptRowIDL.indexOf(vRowid) === -1) {
                        if (exlFromOptRowIDL === '') {
                            exlFromOptRowIDL = vRowid + ';';
                        } else {
                            exlFromOptRowIDL = exlFromOptRowIDL + vRowid + ';';
                        }
                    } else {
                        exlFromOptRowIDL = exlFromOptRowIDL.replace(vRowid + ';', '');
                    }
                    this.setControlValue('ExlFromOptRowID', exlFromOptRowIDL);
                    break;
            }
        }
    }

    public cmdExcFromOptOnClick(flag: boolean): void {
        //cmdIncInOpt button functionalaities
        let promptText: string = MessageConstant.PageSpecificMessage.servicePlanningMaintenance.cmdIncInOptConfirmText;
        this.setControlValue('ExlFromOptInd', false);
        if (flag) {
            //cmdExcFromOpt button functionalaities
            this.setControlValue('ExlFromOptInd', true);
            promptText = MessageConstant.PageSpecificMessage.servicePlanningMaintenance.cmdExcFromOptConfirmText;
        }
        if (this.getControlValue('ExlFromOptRowID')) {
            let promptVO1: ICabsModalVO = new ICabsModalVO(promptText, null, this.updateExlFromOpt.bind(this));
            this.modalAdvService.emitPrompt(promptVO1);
        } else {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.servicePlanningMaintenance.noRecordSelected));
        }
    }

    public tbodyServicePlanningOnClick(data: any): void {
        this.planEvents();
    }

    public cmdSummaryOnClick(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
        }
    }

    public cmdTechDiaryViewOnClick(): void {
        this.parentMode = 'ServicePlanning';
        let iCount: any = 0;
        if (this.getControlValue('BranchServiceAreaCount').length)
            iCount = parseInt(this.getControlValue('BranchServiceAreaCount'), 20);
        if (iCount === 1) {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotDeveloped));
            //this.navigate('', 'iCABSAMultiTechVisitDiaryGrid.htm');//page not developed
        } else {
            this.navigate(this.parentMode, GoogleMapPagesModuleRoutes.ICABSATECHNICIANVISITDIARYGRID);
        }

    }

    public validatePlanDate(): boolean {
        let validatePlanDate: boolean = false;
        if (!this.getControlValue('StartDate')) {
            this.setControlValue('StartDate', this.utils.formatDate(new Date()));
        }
        if (this.utils.convertDate(this.getControlValue('EndDate')) <= (new Date())) {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.servicePlanningMaintenance.planDateMustBeGreaterThanToday));
        } else if (this.utils.convertDate(this.getControlValue('StartDate')) > (new Date())) {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.servicePlanningMaintenance.planDateMustBeGreaterThanTodayAnd12Months));
        } else {
            validatePlanDate = true;
        }
        return validatePlanDate;
    }

    public submitReportRequest(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams.set(this.serviceConstants.BusinessCode, this.businessCode());
        searchParams.set(this.serviceConstants.CountryCode, this.countryCode());
        searchParams.set(this.serviceConstants.Action, '6');
        let postData: any = {};
        postData[this.serviceConstants.ActionType] = 'SubmitBatchProcess';
        postData['BranchNumber'] = this.getControlValue('BranchNumber');
        postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postData['StartDate'] = this.getControlValue('StartDate');
        postData['EndDate'] = this.getControlValue('EndDate');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module,
            this.queryParams.operation, searchParams, postData)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.uiDisplayObject.BatchProcessInformation = false;
                        this.batchInformation = '';
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        if (data['BatchProcessInformation']) {
                            this.uiDisplayObject.BatchProcessInformation = true;
                            this.batchInformation = data['BatchProcessInformation'];
                            this.setControlValue('BatchProcessInformation', data['BatchProcessInformation']); //for BatchProcessInformation legend display
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    public cmdOptimiseOnClick(): void {
        let branchText: string;
        if (this.validatePlanDate()) {
            if (!this.getControlValue('BranchServiceAreaCode')) {
                branchText = 'Branch: ' + this.getControlValue('BranchNumber');
            } else {
                branchText = 'Techs: ' + this.getControlValue('EmployeeSurname');
            }

            this.getTranslatedValuesBatch((data: any) => {
                if (data) {
                    let promptVO1: ICabsModalVO = new ICabsModalVO(data[0].toString() + branchText + data[1].toString() + this.globalize.formatDateToLocaleFormat(this.getControlValue('StartDate')) + data[2].toString() + this.globalize.formatDateToLocaleFormat(this.getControlValue('EndDate')) + data[3].toString(), null, this.submitReportRequest.bind(this));
                    this.modalAdvService.emitPrompt(promptVO1);
                }
            },
                [MessageConstant.PageSpecificMessage.servicePlanningMaintenance.cmdOptimiseText1], [MessageConstant.PageSpecificMessage.servicePlanningMaintenance.cmdOptimiseText2], [MessageConstant.PageSpecificMessage.servicePlanningMaintenance.cmdOptimiseText3], [MessageConstant.PageSpecificMessage.servicePlanningMaintenance.cmdOptimiseText4]
            );


        }
    }

    public cmdShowFilterOnClick(): void {
        if (this.uiDisplayObject.cmdShowFilterText) {
            this.cmdShowFilterText = 'Hide Filters';
            this.uiDisplayObject.cmdShowFilterText = false;
        } else {
            this.cmdShowFilterText = 'Show Filters';
            this.uiDisplayObject.cmdShowFilterText = true;
        }
    }

    public setAttributes(rowData: any): void {
        let cProp: any, branchServiceAreaCode: any, addElementNumber: any, addElementNumber2: any, gVisitDataAdditionalProperty: any;
        gVisitDataAdditionalProperty = this.riGrid.Details.GetAttribute('GVisitDate', 'AdditionalProperty');
        cProp = gVisitDataAdditionalProperty.split('^');
        if (!this.getControlValue('BranchServiceAreaCode')) {
            addElementNumber = 1;
            this.setAttribute('BranchServiceAreaCode', rowData[0]['text']);
        } else {
            addElementNumber = 0;
            this.setAttribute('BranchServiceAreaCode', '');
        }
        if (this.isVEnablePostcodeDefaulting) {
            addElementNumber2 = 1;
        } else {
            addElementNumber2 = 0;
        }
        if (cProp) {
            this.setAttribute('ContractRowID', cProp[1]);
            this.setAttribute('PremiseRowID', cProp[2]);
            this.setAttribute('ServiceCoverRowID', cProp[3]);
            this.setAttribute('VisitTypeCode', this.riGrid.Details.GetValue('GVisitType'));
            this.setAttribute('PlanVisitRowID', cProp[0]);
            this.setAttribute('Row', cProp[4]);
            this.setAttribute('ContractTypeCode', cProp[5]);
            this.setAttribute('BranchServiceAreaCode', cProp[6]);
        }
    }

    public tbodyServicePlanningOnDblClick(event: any): void {
        let objSrc: any = event['srcElement'];
        let objTr: any = this.riGrid.CurrentHTMLRow;
        let vRouteOptimisation: string = 'No';
        let columnName: any = this.riGrid.CurrentColumnName;
        let currentRowIndex: any = this.riGrid.CurrentRow;
        let rowData: any = this.riGrid.bodyArray[currentRowIndex];
        let exlFromOptRowID: any;
        let vRowid: any = this.riGrid.Details.GetAttribute('GSelection', 'RowID');
        let currentColumnValue: any = this.riGrid.CurrentColumnValue;
        if (this.vRouteOptimisation) {
            vRouteOptimisation = 'Yes';
        }
        switch (columnName) {
            case 'GPremiseNumber':
            case 'GContractNumber':
            case 'GContractName':
            case 'GVisitDate':
            case 'GVisitNote':
            case 'GConfirmationRequired':
            case 'GProductCode':
                this.setAttributes(rowData);
                switch (this.getAttribute('ContractTypeCode')) {
                    case 'C':
                        this.currentContractTypeURLParameter = '';
                        break;
                    case 'J':
                        this.currentContractTypeURLParameter = '<job>';
                        break;
                    case 'P':
                        this.currentContractTypeURLParameter = '<product>';
                        break;
                }
                switch (columnName) {
                    case 'GContractName':
                    case 'GContractNumber':
                        this.navigate('ServicePlanning', AppModuleRoutes.CONTRACTMANAGEMENT + ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE_SUB,
                            {
                                CurrentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                ContractRowID: this.getAttribute('ContractRowID'),
                                ContractTypeCode: this.getAttribute('ContractTypeCode')
                            });
                        break;
                    case 'GPremiseNumber':
                        this.navigate('ServicePlanning', AppModuleRoutes.CONTRACTMANAGEMENT + AppModuleRoutes.PREMISESMAINTENANCE + ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE_SUB,
                            {
                                contractTypeCode: this.getAttribute('ContractTypeCode')
                            });
                        break;
                    case 'GProductCode':
                        if (this.currentContractTypeURLParameter === '<product>') {
                            this.navigate('ServicePlanning', InternalGridSearchSalesModuleRoutes.ICABSAPRODUCTSALESSCDETAILMAINTENANCE,
                                {
                                    currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                    contractTypeCode: this.getAttribute('ContractTypeCode')
                                });
                        } else {
                            this.navigate('ServicePlanning', AppModuleRoutes.SERVICECOVERMAINTENANCE + ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE_SUB,
                                {
                                    currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                    contractTypeCode: this.getAttribute('ContractTypeCode')
                                });
                        }
                        break;
                    case 'GConfirmationRequired':
                        this.navigate('ServicePlanningHardSlot', InternalGridSearchServiceModuleRoutes.ICABSSESERVICEPLANNEDDATESGRID,
                            {
                                currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                contractTypeCode: this.getAttribute('ContractTypeCode'),
                                BranchServiceAreaCode: this.riGrid.Details.GetValue('GBranchServiceAreaCode'),
                                EmployeeSurname: this.riGrid.Details.GetValue('GContractName')
                            });
                        break;
                    case 'BranchServiceAreaSeqNo':
                        if (this.parentMode === 'ServicePlan') {
                            this.navigate('ConfirmedPlan', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE,
                                {
                                    CurrentContractTypeURLParameter: this.currentContractTypeURLParameter
                                });
                        }
                        break;
                    case 'VisitTypeCode':
                        if (this.parentMode === 'ServicePlan') {
                            this.navigate('ServicePlan', InternalGridSearchServiceModuleRoutes.ICABSSESERVICEPLANNEDDATESGRID,
                                {
                                    currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                    contractTypeCode: this.getAttribute('ContractTypeCode')
                                });
                        } else {
                            this.navigate('ServicePlanning', InternalGridSearchServiceModuleRoutes.ICABSSESERVICEPLANNEDDATESGRID,
                                {
                                    currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                    contractTypeCode: this.getAttribute('ContractTypeCode')
                                });
                        }
                        break;
                    case 'GVisitDate':
                        let mode: string = 'ServicePlanning';
                        if (this.parentMode === 'ServicePlan') {
                            mode = 'ServicePlan';
                        }
                        this.navigate(mode, InternalMaintenanceApplicationModuleRoutes.ICABSAPLANVISITMAINTENANCE,
                            {
                                currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                PlanVisitROWID: this.getAttribute('PlanVisitRowID')
                            });
                        break;
                    case 'GVisitNote':
                        mode = 'ServicePlanningNote';
                        if (this.parentMode === 'ServicePlan') {
                            mode = 'ServicePlanNote';
                        }
                        this.navigate(mode, InternalMaintenanceApplicationModuleRoutes.ICABSAPLANVISITMAINTENANCE,
                            {
                                currentContractTypeURLParameter: this.currentContractTypeURLParameter,
                                PlanVisitROWID: this.getAttribute('PlanVisitRowID')
                            });
                        break;
                }
                break;
        }
    }

    public onCellBlur(event: any): void {

        //TODO
    }
}
