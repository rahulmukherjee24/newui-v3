import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBBranchServiceAreaGroupDetailMaintenance.html'
})

export class BranchServiceAreaGroupDetailMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterContentInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    // URL Query Parameters
    private queryParams: Object = {
        operation: 'Business/iCABSBBranchServiceAreaGroupDetailMaintenance',
        module: 'service',
        method: 'service-delivery/admin'
    };

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BranchServiceAreaCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'BranchServiceAreaCodeTo', disabled: true, type: MntConst.eTypeCode },
        { name: 'BranchServiceAreaDescTo', disabled: true, type: MntConst.eTypeText },
        { name: 'Town', disabled: true, type: MntConst.eTypeText },
        { name: 'State', disabled: true, type: MntConst.eTypeText },
        { name: 'Postcode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ProductServiceGroupCode', disabled: true, type: MntConst.eTypeCode },
        { name: 'ProductServiceGroupDesc', disabled: true, type: MntConst.eTypeText },
        { name: 'OldDetailRowID', type: MntConst.eTypeText },
        { name: 'MessageDesc', type: MntConst.eTypeText }
    ];

    //Global variables
    public productServiceGroupDesc: string;
    public productServiceTemplate: string;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHSERVICEAREAGROUPDETAILMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Postcode/Product Group Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        this.setControlValue('BranchServiceAreaCodeTo', this.riExchange.getParentHTMLValue('BranchServiceAreaCodeTo'));
        this.setControlValue('BranchServiceAreaDescTo', this.riExchange.getParentHTMLValue('BranchServiceAreaDescTo'));
        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentAttributeValue('BranchServiceAreaCode'));
        this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentAttributeValue('BranchServiceAreaDesc'));
        this.setControlValue('Town', this.riExchange.getParentAttributeValue('Town'));
        this.setControlValue('State', this.riExchange.getParentAttributeValue('State'));
        this.setControlValue('Postcode', this.riExchange.getParentAttributeValue('Postcode'));
        this.setControlValue('ProductServiceGroupCode', this.riExchange.getParentAttributeValue('ProductServiceGroupCode'));
        this.setControlValue('ProductServiceGroupDesc', this.riExchange.getParentAttributeValue('ProductServiceGroupDesc'));
        this.setControlValue('OldDetailRowID', this.riExchange.getParentAttributeValue('OldDetailRowID'));
        this.fetchRecord();
    }

    private fetchRecord(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroupCode'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.productServiceGroupDesc = data.ProductServiceGroupDesc;
                    this.productServiceTemplate = data.ProductServiceTemplate;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
            );
    }

    // Clicking on Save button
    public onClickSave(): void {
        if (this.getControlValue('BranchServiceAreaCodeTo') === '') {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.mandatoryField));
        }
        else {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, '', this.promptConfirmSave.bind(this)));
        }
    }

    // Clicking on Cancel button
    public onClickCancel(): void {
        this.location.back();
    }

    // Confirming the records to be saved
    public promptConfirmSave(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '1');
        let formdata: Object = {};
        formdata = {
            Postcode: this.getControlValue('Postcode'),
            ProductServiceGroupCode: this.getControlValue('ProductServiceGroupCode'),
            BranchNumber: this.utils.getBranchCode(),
            State: this.getControlValue('State'),
            Town: this.getControlValue('Town'),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            BranchServiceAreaDesc: this.getControlValue('BranchServiceAreaDesc'),
            BranchServiceAreaCodeTo: this.getControlValue('BranchServiceAreaCodeTo'),
            BranchServiceAreaDescTo: this.getControlValue('BranchServiceAreaDescTo'),
            OldDetailRowID: this.getControlValue('OldDetailRowID'),
            MessageDesc: this.getControlValue('MessageDesc')
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.setControlValue('MessageDesc', data.MessageDesc);
                    this.location.back();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }
}
