import { Component, OnInit, Injector, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { CommonDropdownComponent } from './../../../shared/components/common-dropdown/common-dropdown.component';
import { ScreenNotReadyComponent } from './../../../shared/components/screenNotReady';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBDetectorMaintenance.html'
})

export class DetectorMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('detectorSearch') detectorSearch: CommonDropdownComponent;

    private queryPost: QueryParams = this.getURLSearchParamObject();
    private muleConfig = {
        method: 'extranets-connect/admin',
        module: 'pnol',
        operation: 'Business/iCABSBDetectorMaintenance'
    };

    public pageId: string = '';
    public controls = [
        { name: 'DetectorID', required: true, disabled: false, type: MntConst.eTypeCode },
        { name: 'Description', required: true, disabled: true, type: MntConst.eTypeText },
        { name: 'DetectorTypeID', required: true, disabled: true, type: MntConst.eTypeCode },
        { name: 'DetectorTypeDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'InfestationLocationCode', required: true, disabled: false, type: MntConst.eTypeCode },
        { name: 'InfestationLocationDesc', required: false, disabled: true, type: MntConst.eTypeText },
        { name: 'ValidForSingleBarcode', required: false, disabled: true },
        { name: 'SortOrder', required: false, disabled: true, type: MntConst.eTypeInteger },
        { name: 'HasBattery', required: false, disabled: true },
        { name: 'HasGas', required: false, disabled: true },
        { name: 'ConnectUnit', required: false, disabled: true },
        { name: 'PassToPDAInd', required: false, disabled: true },
        { name: 'BarcodeRef', required: false, disabled: true, type: MntConst.eTypeInteger }
    ];
    public searchConfigs: any = {
        detectorSearch: {
            isRequired: true,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                operation: 'Business/iCABSBDetectorSearch',
                module: 'pnol',
                method: 'extranets-connect/search'
            },
            columns: ['Detector.DetectorID', 'Detector.Description'],
            active: {
                id: '',
                text: ''
            }
        },
        detectorTypeSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp'
            },
            component: ScreenNotReadyComponent
        },
        infestationLocationSearch: {
            isRequired: true,
            isDisabled: true,
            isTriggerValidate: false,
            params: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBInfestationLocationSearch'
            },
            columns: ['InfestationLocationCode', 'InfestationLocationDesc'],
            active: {
                id: '',
                text: ''
            }
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        }
    };

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.setFormMode(this.c_s_MODE_SELECT);
        if (this.isReturning()) {
            this.populateUIFromFormData();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBDETECTORMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Detector Maintenance';
    }

    // Fetching Visit Type Data
    private fetchDetectorData(): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.formPristine();
            let queryGet: QueryParams = this.getURLSearchParamObject();
            queryGet.set(this.serviceConstants.Action, '0');
            queryGet.set('DetectorID', this.getControlValue('DetectorID'));
            queryGet.set('DetectorTypeID', this.getControlValue('DetectorTypeID'));
            queryGet.set('InfestationLocationCode', this.getControlValue('InfestationLocationCode'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryGet)
                .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.setFormMode(this.c_s_MODE_SELECT);
                    } else {
                        this.setFormMode(this.c_s_MODE_UPDATE);
                        this.setControlValue('HasBattery', data.HasBattery);
                        this.setControlValue('HasGas', data.HasGas);
                        this.setControlValue('ConnectUnit', data.ConnectUnit);
                        this.setControlValue('BarcodeRef', data.BarcodeRef);
                        this.setControlValue('ValidForSingleBarcode', data.ValidForSingleBarcode);
                        this.setControlValue('PassToPDAInd', data.PassToPDAInd);
                        this.setControlValue('SortOrder', data.SortOrder);

                        this.pageParams.ttDetector = data.ttDetector;
                        this.pageParams.HasBattery = data.HasBattery;
                        this.pageParams.HasGas = data.HasGas;
                        this.pageParams.ConnectUnit = data.ConnectUnit;
                        this.pageParams.BarcodeRef = data.BarcodeRef;
                        this.pageParams.ValidForSingleBarcode = data.ValidForSingleBarcode;
                        this.pageParams.PassToPDAInd = data.PassToPDAInd;
                        this.pageParams.SortOrder = data.SortOrder;

                        this.enableControls(['DetectorTypeID', 'DetectorTypeDesc', 'InfestationLocationCode', 'InfestationLocationDesc']);
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        }
    }

    private lookupApi(lookUp: boolean): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let lookupDetails: Array<any> = [{
            'table': 'DetectorType',
            'query': {
                'BusinessCode': this.businessCode(),
                'DetectorTypeID': this.getControlValue('DetectorTypeID')
            },
            'fields': ['Description']
        }];

        this.LookUp.lookUpPromise(lookupDetails).then((data) => {
            let isError: boolean = false;
            if (data.length > 0) {
                let detectorTypeData: Array<any> = data[0];
                let infestationLocationData: Array<any> = data[3];

                if (detectorTypeData.length > 0) {
                    this.setControlValue('DetectorTypeDesc', detectorTypeData[0].Description);
                } else if (this.checkValidData('DetectorTypeID')) {
                    this.setControlValue('DetectorTypeDesc', '');
                    isError = true;
                }

                if (lookUp && !isError)
                    this.promptModalForSave(false);
            }
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        });
    }

    private checkValidData(controlName: string): boolean {
        if (this.getControlValue(controlName)) {
            this.uiForm.controls[controlName].setErrors([{ 'invalidValue': true }]);
            return true;
        }
        return false;
    }

    // Marking Controls as Touched
    private uiFormMarkAsTouched(): void {
        for (let control of this.controls) {
            this.uiForm.controls[control.name].markAsTouched();
        }
    }

    // Saving Visit Type Data
    private saveDetector(): void {
        let formdata: any = {};
        if (this.formMode === this.c_s_MODE_ADD) {
            this.queryPost.set(this.serviceConstants.Action, '1');
            formdata = {
                DetectorID: this.getControlValue('DetectorID'),
                DetectorTypeID: this.getControlValue('DetectorTypeID'),
                InfestationLocationCode: this.getControlValue('InfestationLocationCode'),
                Description: this.getControlValue('Description'),
                HasBattery: this.getControlValue('HasBattery'),
                HasGas: this.getControlValue('HasGas'),
                ConnectUnit: this.getControlValue('ConnectUnit'),
                ValidForSingleBarcode: this.getControlValue('ValidForSingleBarcode'),
                PassToPDAInd: this.getControlValue('PassToPDAInd'),
                BarcodeRef: this.getControlValue('BarcodeRef'),
                SortOrder: this.getControlValue('SortOrder')
            };
        } else {
            this.queryPost.set(this.serviceConstants.Action, '2');
            formdata = {
                Description: this.getControlValue('Description'),
                HasBattery: this.getControlValue('HasBattery'),
                HasGas: this.getControlValue('HasGas'),
                ConnectUnit: this.getControlValue('ConnectUnit'),
                ValidForSingleBarcode: this.getControlValue('ValidForSingleBarcode'),
                PassToPDAInd: this.getControlValue('PassToPDAInd'),
                BarcodeRef: this.getControlValue('BarcodeRef'),
                SortOrder: this.getControlValue('SortOrder'),
                ROWID: this.pageParams.ttDetector
            };
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    if (this.formMode === this.c_s_MODE_UPDATE) {
                        this.pageParams.ttDetector = data.ttDetector;
                        this.pageParams.HasBattery = data.HasBattery;
                        this.pageParams.HasGas = data.HasGas;
                        this.pageParams.ConnectUnit = data.ConnectUnit;
                        this.pageParams.BarcodeRef = data.BarcodeRef;
                        this.pageParams.ValidForSingleBarcode = data.ValidForSingleBarcode;
                        this.pageParams.PassToPDAInd = data.PassToPDAInd;
                        this.pageParams.SortOrder = data.SortOrder;

                        this.searchConfigs.detectorSearch.active = {
                            id: this.getControlValue('DetectorID'),
                            text: this.getControlValue('DetectorID') + ' - ' + this.getControlValue('Description')
                        };
                    } else {
                        this.addNewDetector();
                    }
                    this.formPristine();
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public defaultDetectorSearchDataRecieved(data: any): void {
        if (data.totalRecords === 0) {
            this.addNewDetector();
        } else {
            this.onDetectorSearchDataRecieved(data.firstRow);
        }
    }

    public onDetectorSearchDataRecieved(data: any): void {
        if (data['Detector.DetectorID']) {
            this.setControlValue('DetectorID', data['Detector.DetectorID']);
            this.setControlValue('Description', data['Detector.Description']);
            this.setControlValue('DetectorTypeID', data['Detector.DetectorTypeID']);
            this.setControlValue('DetectorTypeDesc', data['DetectorType.Description']);
            this.setControlValue('InfestationLocationCode', data['Detector.InfestationLocationCode']);
            this.setControlValue('InfestationLocationDesc', data['InfestationLocation.InfestationLocationDesc']);
            this.pageParams.DetectorID = data['Detector.DetectorID'];
            this.pageParams.Description = data['Detector.Description'];
            this.pageParams.DetectorTypeID = data['Detector.DetectorTypeID'];
            this.pageParams.DetectorTypeDesc = data['DetectorType.Description'];
            this.pageParams.InfestationLocationCode = data['Detector.InfestationLocationCode'];
            this.pageParams.InfestationLocationDesc = data['InfestationLocation.InfestationLocationDesc'];

            this.searchConfigs.detectorSearch.active = {
                id: this.getControlValue('DetectorID'),
                text: this.getControlValue('DetectorID') + ' - ' + this.getControlValue('Description')
            };
            this.searchConfigs.infestationLocationSearch.active = {
                id: this.getControlValue('InfestationLocationCode'),
                text: this.getControlValue('InfestationLocationCode') + ' - ' + this.getControlValue('InfestationLocationDesc')
            };
            this.fetchDetectorData();
        } else {
            this.searchConfigs.detectorSearch.active = {
                id: this.getControlValue('DetectorID'),
                text: this.getControlValue('DetectorID') + ' - ' + this.getControlValue('Description')
            };
        }
    }

    public onInfestationLocationDataRecieved(data: any): void {
        if (data) {
            this.setControlValue('InfestationLocationCode', data.InfestationLocationCode);
            this.setControlValue('InfestationLocationDesc', data.InfestationLocationDesc);
            this.uiForm.controls['InfestationLocationCode'].markAsDirty();
        }
    }

    public promptModalForSave(lookUp: boolean): void {
        if (lookUp) {
            this.lookupApi(lookUp);
        } else {
            if (this.uiForm.valid) {
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveDetector.bind(this));
                this.modalAdvService.emitPrompt(modalVO);
            } else {
                if (this.uiForm.controls['InfestationLocationCode'].invalid) {
                    this.searchConfigs.infestationLocationSearch.isTriggerValidate = true;
                } else {
                    this.searchConfigs.infestationLocationSearch.isTriggerValidate = false;
                }
                this.uiFormMarkAsTouched();
            }
        }
    }

    public addNewDetector(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.uiForm.reset();
        this.uiForm.controls['DetectorTypeID'].enable();
        this.searchConfigs.infestationLocationSearch.active = {
            id: '',
            text: ''
        };
        this.searchConfigs.detectorTypeSearch.isDisabled = false;
        this.searchConfigs.infestationLocationSearch.isDisabled = false;
        this.searchConfigs.infestationLocationSearch.isTriggerValidate = false;
        this.pageParams = {};
    }

    // Cancel Button operation
    public cancelDetector(): void {
        if (this.formMode !== this.c_s_MODE_SELECT) {
            if (this.formMode === this.c_s_MODE_ADD) {
                this.setFormMode(this.c_s_MODE_SELECT);
                this.uiForm.reset();
                this.searchConfigs.detectorSearch.active = {
                    id: '',
                    text: ''
                };
                this.searchConfigs.infestationLocationSearch.active = {
                    id: '',
                    text: ''
                };
                this.uiForm.controls['DetectorTypeID'].disable();
                this.searchConfigs.detectorTypeSearch.isDisabled = true;
                this.searchConfigs.infestationLocationSearch.isDisabled = true;
            } else {
                this.setFormMode(this.c_s_MODE_UPDATE);
                this.setControlValue('DetectorID', this.pageParams.DetectorID);
                this.setControlValue('Description', this.pageParams.Description);
                this.setControlValue('DetectorTypeID', this.pageParams.DetectorTypeID);
                this.setControlValue('DetectorTypeDesc', this.pageParams.DetectorTypeDesc);
                this.setControlValue('InfestationLocationCode', this.pageParams.InfestationLocationCode);
                this.setControlValue('InfestationLocationDesc', this.pageParams.InfestationLocationDesc);
                this.setControlValue('HasBattery', this.pageParams.HasBattery);
                this.setControlValue('HasGas', this.pageParams.HasGas);
                this.setControlValue('ConnectUnit', this.pageParams.ConnectUnit);
                this.setControlValue('BarcodeRef', this.pageParams.BarcodeRef);
                this.setControlValue('ValidForSingleBarcode', this.pageParams.ValidForSingleBarcode);
                this.setControlValue('PassToPDAInd', this.pageParams.PassToPDAInd);
                this.setControlValue('SortOrder', this.pageParams.SortOrder);
            }
        }
        this.formPristine();
    }
}
