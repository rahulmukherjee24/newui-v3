import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { BaseComponent } from './../../base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { AjaxConstant } from './../../../shared/constants/AjaxConstants';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { PostCodeSearchComponent } from './../../internal/search/iCABSBPostcodeSearch.component';
import { InternalSearchModuleRoutes } from './../../base/PageRoutes';
import { QueryParams } from '@shared/services/http-params-wrapper';
@Component({
    templateUrl: 'iCABSBBranchPcodeServiceGrpEntryMaintenance.html'
})

export class BranchPcodeServiceGrpEntryMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private queryParams: any = {
        operation: 'Business/iCABSBBranchPcodeServiceGrpEntryMaintenance',
        module: 'validation',
        method: 'contract-management/admin'
    };
    private getData: any = [];
    private isRowClick: any;
    private isSaveClicked: boolean;
    public pageId: string = '';
    public promptConfirmContent: any;
    public isAddHide: boolean = true;
    public isDeleteHide: boolean = true;
    public isHide: boolean = false;
    public controls = [
        { name: 'BranchServiceAreaCode', disabled: true, required: true },
        { name: 'BranchServiceAreaDesc', disabled: true, required: true },
        { name: 'Postcode', required: true },
        { name: 'State', disabled: true },
        { name: 'Town', disabled: true },
        { name: 'SortOrder' },
        { name: 'ROWID' }
    ];
    public postCodeSearch: any = {
        autoOpen: false,
        showCloseButton: true,
        childConfigParams: {
            parentMode: 'LookUp-Service',
            showAddNew: false,
            PostCode: '',
            State: '',
            Town: ''
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        },
        contentComponent: PostCodeSearchComponent,
        showHeader: true,
        searchModalRoute: '',
        disabled: true
    };
    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHPCODESERVICEGRPENTRYMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Postcode Group Entry Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnLoad();
    }
    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
    private windowOnLoad(): void {
        this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
        this.setControlValue('BranchServiceAreaDesc', this.riExchange.getParentHTMLValue('BranchServiceAreaDesc'));
        this.setControlValue('ROWID', this.riExchange.getParentHTMLValue('ROWID'));
        this.isRowClick = this.riExchange.getParentHTMLValue('isRowClick');
        if (this.isRowClick) {
            this.pageParams.mode = 'update';
            this.isAddHide = false;
            this.isDeleteHide = false;
            this.isSaveClicked = false;
            this.getData.push(this.riExchange.getParentHTMLValue('Postcode'));
            this.getData.push(this.riExchange.getParentHTMLValue('State'));
            this.getData.push(this.riExchange.getParentHTMLValue('Town'));
            this.getData.push('0');
            this.setControlValue('Postcode', this.riExchange.getParentHTMLValue('Postcode'));
            this.setControlValue('State', this.riExchange.getParentHTMLValue('State'));
            this.setControlValue('Town', this.riExchange.getParentHTMLValue('Town'));
            this.setControlValue('SortOrder', '0');
            this.disableControl('Postcode', true);
            this.postCodeSearch.disabled = true;
            this.setPostCodeInputParams(this.riExchange.getParentHTMLValue('Postcode'), this.riExchange.getParentHTMLValue('State'), this.riExchange.getParentHTMLValue('Town'));
        }
        else {
            this.pageParams.mode = 'add';
            this.isDeleteHide = true;
            this.setControlValue('SortOrder', '');
            this.disableControl('Postcode', false);
            this.postCodeSearch.disabled = false;
        }
    }
    private onChangeinitialExpenseCode(data: any, fromCancel?: boolean): void {
        let pageParams: QueryParams = this.getURLSearchParamObject();
        pageParams.set(this.serviceConstants.Action, '0');
        pageParams.set('Postcode', this.getControlValue('Postcode'));
        pageParams.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, pageParams)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.findResult === '<Multi>') {
                    this.formPristine();
                    this.navigate(this.parentMode, InternalSearchModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH);
                }
                else if (data.hasError) {
                    let modalVO: ICabsModalVO = new ICabsModalVO(data.errorMessage, data.fullError);
                    modalVO.closeCallback = this.navigateBack.bind(this);
                    this.modalAdvService.emitMessage(modalVO);
                }
                else {
                    this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                    this.setControlValue('Postcode', data.Postcode);
                    this.setControlValue('Town', data.Town);
                    this.setControlValue('State', data.State);
                    if (fromCancel) {
                        this.setControlValue('SortOrder', data.SortOrder);
                    } else {
                        this.setControlValue('SortOrder', '0');
                    }
                    this.setPostCodeInputParams(data.Postcode, data.State, data.Town);
                    this.cloneData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }

    private navigateBack(): void {
        this.formPristine();
        this.navigate(this.parentMode, InternalSearchModuleRoutes.ICABSBBRANCHPCODESERVICEGRPENTRYSEARCH);
    }

    private cloneData(): void {
        this.getData = [];
        this.getData.push(this.getControlValue('Postcode'));
        this.getData.push(this.getControlValue('State'));
        this.getData.push(this.getControlValue('Town'));
        this.getData.push(this.getControlValue('SortOrder'));
        this.isRowClick = true;
    }

    private confirmCallBack(): void {
        this.saveUpdatePCode();
    }
    private deleteCallBack(): void {
        this.deletePCode();
    }
    private setPostCodeInputParams(postCode: String, state: String, town: String): void {
        this.postCodeSearch['childConfigParams']['PostCode'] = postCode;
        this.postCodeSearch['childConfigParams']['State'] = state;
        this.postCodeSearch['childConfigParams']['Town'] = town;
    }
    public showSaveConfirm(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.confirmCallBack.bind(this)));
        }
    }
    public saveUpdatePCode(): void {
        let postSearchParams = this.getURLSearchParamObject();
        let postParams: any = {};
        if (this.pageParams.mode === 'add') {
            postSearchParams.set(this.serviceConstants.Action, '1');
            postParams = {
                'BranchServiceAreaCode': this.getControlValue('BranchServiceAreaCode'),
                'Postcode': this.getControlValue('Postcode'),
                'BranchNumber': this.utils.getBranchCode(),
                'Town': this.getControlValue('Town'),
                'State': this.getControlValue('State'),
                'SortOrder': this.getControlValue('SortOrder')
            };
        } else {
            postSearchParams.set(this.serviceConstants.Action, '2');
            postParams = {
                'ROWID': this.getControlValue('ROWID'),
                'SortOrder': this.getControlValue('SortOrder')
            };
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe(
            (e) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e['errorMessage'], e['fullError']));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                    this.setControlValue('ROWID', e.ttBranchPcodeServiceGrpEntry);
                    this.cloneData();
                    this.isSaveClicked = true;
                    this.pageParams.mode = 'update';
                    this.isDeleteHide = false;
                    this.isAddHide = false;
                    this.formPristine();
                    this.disableControl('Postcode', true);
                    this.postCodeSearch.disabled = true;
                }
            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }

    //delete mode
    public deletePCode(): void {
        this.pageParams.mode = 'delete';
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '3');
        let postParams: any = {
            'ROWID': this.getControlValue('ROWID')
        };
        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, postParams)
            .subscribe(
            (data) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                }
                else {
                    this.isAddHide = false;
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeleted));
                    this.resetField();
                    this.setControlValue('SortOrder', '');
                    this.disableControl('Postcode', true);
                    this.disableControl('SortOrder', true);
                    this.postCodeSearch.disabled = true;
                    this.isHide = true;
                    this.isDeleteHide = true;
                    this.getData = [];
                    this.setPostCodeInputParams('', '', '');
                    this.isRowClick = false;
                }
            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }
    public cancel(data: any): void {
        this.formPristine();
        if (this.pageParams.mode === 'update') {
            if (this.isSaveClicked) {
                this.onChangeinitialExpenseCode(data, true);
            } else {
                this.onChangeinitialExpenseCode(data);
            }
        }
        else if (this.pageParams.mode === 'add') {
            if (this.isRowClick) {
                this.pageParams.mode = 'update';
                this.isAddHide = false;
                this.isDeleteHide = false;
                this.setControlValue('Postcode', this.getData[0]);
                this.setControlValue('State', this.getData[1]);
                this.setControlValue('Town', this.getData[2]);
                this.setControlValue('SortOrder', this.getData[3]);
                this.setPostCodeInputParams(this.getData[0], this.getData[1], this.getData[2]);
                this.disableControl('Postcode', true);
                this.postCodeSearch.disabled = true;
            }
            else {
                this.resetField();
                this.setControlValue('SortOrder', '');
            }
        }
    }
    public addMode(): void {
        this.setControlValue('SortOrder', '');
        this.pageParams.mode = 'add';
        this.disableControl('Postcode', false);
        this.disableControl('SortOrder', false);
        this.postCodeSearch.disabled = false;
        this.resetField();
        this.isAddHide = true;
        this.isDeleteHide = true;
        this.isHide = false;
    }
    public resetField(): void {
        this.setControlValue('Postcode', '');
        this.setControlValue('State', '');
        this.setControlValue('Town', '');
        this.setPostCodeInputParams('', '', '');
    }
    public onPostCodeChange(data: any): void {
        this.setControlValue('Postcode', data.Postcode);
        this.setControlValue('State', data.State);
        this.setControlValue('Town', data.Town);
        this.setPostCodeInputParams(data.Postcode, data.State, data.Town);
        this.uiForm.controls['Postcode'].markAsDirty();
        if (this.pageParams.mode === 'update') {
            this.onChangeinitialExpenseCode(data);
        }
    }
    public onPostChangeEvent(): void {
        this.setPostCodeInputParams(this.getControlValue('Postcode'), this.getControlValue('State'), this.getControlValue('Town'));
    }
    public onWarnPortFolio(): void {
        this.pageParams.mode = 'delete';
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '3');
        let postParams: any = {};
        postParams[this.serviceConstants.Function] = 'WarnPortfolioRelatedToPcode';
        postParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        postParams['Town'] = this.getControlValue('Town');
        postParams['State'] = this.getControlValue('State');
        postParams['Postcode'] = this.getControlValue('Postcode');
        postParams['BranchNumber'] = this.utils.getBranchCode();
        this.ajaxSource.next(AjaxConstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, postParams)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage']));
                }
                else {
                    this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteCallBack.bind(this)));
                }
            },
            (error) => {
                this.ajaxSource.next(AjaxConstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }
}
