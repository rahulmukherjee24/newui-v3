import { Component, OnInit, OnDestroy, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractSearchComponent } from '../search/iCABSAContractSearch';
import { GlobalizeService } from '@shared/services/globalize.service';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { PremiseSearchComponent } from '../search/iCABSAPremiseSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { InternalGridSearchModuleRoutes } from '@app/base/PageRoutes';
import { InternalMaintenanceServiceModuleRoutes, InternalGridSearchServiceModuleRoutes } from '@base/PageRoutes';
import { ContractActionTypes } from './../../actions/contract';

@Component({
    templateUrl: 'iCABSSEUnreturnedConsignmentNotesDetailGrid.html',
    providers: [CommonLookUpUtilsService]
})


export class UnreturnedConsignmentNotesDetailGridComponent extends LightBaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public bCode: string = this.businessCode();
    public commonGrifFunction: CommonGridFunction;
    public hasGridData: boolean = false;
    public pageId: string;
    public isContractDisabled: boolean = true;
    public isPremiseDisabled: boolean = true;

    public gridConfig: Record<string, number> = {
        itemsPerPage: 10,
        totalItem: 1
    };
    private xhrParams: Record<string, string> = {
        method: 'bi/reports',
        module: 'reports',
        operation: 'Service/iCABSSEUnreturnedConsignmentNotesDetailGrid'
    };

    public visitStatus: Array<Object> = [
        { key: 'All', value: 'All' },
        { key: 'Visited', value: 'V' },
        { key: 'Not Visited', value: 'NV' }
    ];

    public controls: IControls[] = [
        { name: 'ContractName', disabled: true, type: MntConst.eTypeText },
        { name: 'ContractTypePrefix', type: MntConst.eTypeText },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'PremiseName', disabled: true, type: MntConst.eTypeText },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'SystemUniqueNumber', type: MntConst.eTypeInteger },
        { name: 'VisitStatus', type: MntConst.eTypeText, value: 'All' },
        { name: 'WasteConsignmentNoteNumber', disabled: true, type: MntConst.eTypeText }
    ];

    public ellipsis: Record<string, Record<string, Object>> = {
        contract: {
            childConfigParams: {
                'parentMode': 'LookUp',
                'showCountry': false,
                'showBusiness': false,
                'showAddNew': false
            },
            component: ContractSearchComponent
        },
        premises: {
            childConfigParams: {
                parentMode: 'LookUp',
                ContractNumber: '',
                ContractName: '',
                showAddNew: false
            },
            component: PremiseSearchComponent
        }
    };

    constructor(injector: Injector, public globalize: GlobalizeService, private lookupUtils: CommonLookUpUtilsService, private store: Store<any>) {
        super(injector);
        this.commonGrifFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSSEMANUALWASTECONSIGNMENTNOTESDETAIL;
        this.pageTitle = this.browserTitle = 'Unreturned Consignment Notes Detail';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    public ngAfterContentInit(): void {
        super.ngAfterContentInit();
        if (!this.isReturning()) {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.gridCacheRefresh = true;

            this.controls.forEach(control => {
                this.setControlValue(control.name, this.riExchange.getParentAttributeValue(control.name));
            });

            this.setControlValue('VisitStatus', 'All');
        }

        this.isContractDisabled = !!this.getControlValue('ContractNumber');
        this.isPremiseDisabled = !!this.getControlValue('PremiseNumber');
        this.disableControl('ContractNumber', this.isContractDisabled);
        this.disableControl('PremiseNumber', this.isPremiseDisabled);
        this.buildGrid();
        this.populateGrid();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        if (this.getControlValue('ContractNumber') === '') {
            this.riGrid.AddColumn('ContractNumber', 'WasteConsignmentNoteDetail', 'ContractNumber', MntConst.eTypeCode, 8, false);
            this.riGrid.AddColumnAlign('ContractNumber', MntConst.eAlignmentCenter);
            this.riGrid.AddColumnOrderable('ContractNumber', true);
        }

        if (this.getControlValue('PremiseNumber') === '') {
            this.riGrid.AddColumn('PremiseNumber', 'WasteConsignmentNoteDetail', 'PremiseNumber', MntConst.eTypeCode, 3, false);
            this.riGrid.AddColumnAlign('PremiseNumber', MntConst.eAlignmentCenter);
        }

        this.riGrid.AddColumn('WCNProductCode', 'WasteConsignmentNoteDetail', 'WCNProductCode', MntConst.eTypeCode, 6, false);
        this.riGrid.AddColumnAlign('WCNProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('WCNProductCode', true);

        this.riGrid.AddColumn('WCNProductDesc', 'WasteConsignmentNoteDetail', 'WCNProductDesc', MntConst.eTypeText, 40);

        this.riGrid.AddColumn('EWCCode', 'WasteConsignmentNoteDetail', 'EWCCode', MntConst.eTypeCode, 50, false);
        this.riGrid.AddColumnAlign('EWCCode', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('VisitTypeCode', 'WasteConsignmentNoteDetail', 'VisitTypeCode', MntConst.eTypeCode, 6, true);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('VisitTypeCode', true);

        this.riGrid.AddColumn('VisitDueDate', 'WasteConsignmentNoteDetail', 'VisitDueDate', MntConst.eTypeDate, 10, true);
        this.riGrid.AddColumnAlign('VisitDueDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('VisitDueDate', true);

        this.riGrid.AddColumn('ActualVisitDate', 'WasteConsignmentNoteDetail', 'ActualVisitDate', MntConst.eTypeDate, 6, false);
        this.riGrid.AddColumnAlign('ActualVisitDate', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('ActualVisitDate', true);

        this.riGrid.AddColumn('PlannedQuantity', 'WasteConsignmentNoteDetail', 'PlannedQuantity', MntConst.eTypeInteger, 7);
        this.riGrid.AddColumnAlign('PlannedQuantity', MntConst.eAlignmentCenter);

        this.riGrid.AddColumn('ServicedQuantity', 'WasteConsignmentNoteDetail', 'ServicedQuantity', MntConst.eTypeInteger, 7);
        this.riGrid.AddColumnAlign('ServicedQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ServicedQuantity', true);

        this.riGrid.FunctionUpdateSupport = true;

        this.riGrid.Complete();
    }

    private populateGrid(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Record<string, any> = {};

        gridQuery.set(this.serviceConstants.Action, '2');

        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData['SystemUniqueNumber'] = this.getControlValue('SystemUniqueNumber');
        formData['VisitStatus'] = this.getControlValue('VisitStatus');

        formData[this.serviceConstants.GridMode] = '0';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.hasGridData = false;
                this.gridConfig.totalItem = 1;
                this.displayMessage(data);
            } else {
                this.hasGridData = true;
                this.riGrid.RefreshRequired();
                this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                this.riGrid.UpdateHeader = true;
                this.riGrid.UpdateBody = true;
                this.riGrid.UpdateFooter = true;
                this.riGrid.Execute(data);
                setTimeout(() => {
                    this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                }, 100);
            }
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }

    public onContractDataReceived(data: Object): void {
        if (data) {
            this.setControlValue('ContractName', data['ContractName']);
            this.setControlValue('ContractNumber', data['ContractNumber']);
        }
        this.updateEllipsisConfig();
    }

    public onPremiseDataReceived(data: Object): void {
        if (data) {
            this.setControlValue('PremiseNumber', data['PremiseNumber']);
            this.setControlValue('PremiseName', data['PremiseName']);
        }
    }

    private updateEllipsisConfig(): void {
        this.ellipsis.premises.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber') || '';
        this.ellipsis.premises.childConfigParams['ContractName'] = this.getControlValue('ContractName') || '';
    }

    public onControlChange(controlId: any): void {
        let contractNumber: string = this.getControlValue('ContractNumber');
        switch (controlId) {
            case 'ContractNumber':
                this.setControlValue('ContractName', '');
                if (!contractNumber) {
                    this.setControlValue('ContractNumber', '');
                    this.updateEllipsisConfig();
                    return;
                }
                this.lookupUtils.getContractName(contractNumber).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('ContractName', data[0][0].ContractName);
                    } else {
                        this.setControlValue('ContractName', '');
                        this.setControlValue('ContractNumber', '');
                    }
                    this.updateEllipsisConfig();
                },
                    (error) => {
                        this.displayMessage(error.fullError);
                    });
                break;

            case 'PremiseNumber':
                let premiseNumber: string = this.getControlValue('PremiseNumber');
                if (!contractNumber || !premiseNumber) {
                    this.setControlValue('PremiseName', '');
                    this.setControlValue('PremiseNumber', '');
                    return;
                }
                this.setControlValue('PremiseName', '');
                this.lookupUtils.getPremiseName(premiseNumber, this.businessCode(), contractNumber).then((data) => {
                    if (data && data[0] && data[0][0]) {
                        this.setControlValue('PremiseName', data[0][0].PremiseName);
                    } else {
                        this.setControlValue('PremiseName', '');
                        this.setControlValue('PremiseNumber', '');
                    }
                },
                    (error) => {
                        this.displayMessage(error.fullError);
                    });
                break;
        }
    }

    public goWasteConsDetail(): void {
        this.navigate('UnreturnedConsignmentGrid', InternalGridSearchModuleRoutes.ICABSSEWASTECONSIGNMENTNOTEGRID);
    }

    public onRiGridRefresh(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.commonGrifFunction.onRefreshClick();
        }
    }

    public onHeaderClick(): void {
        this.onRiGridRefresh();
    }

    public onGridBodyDoubleClick(): void {
        let contractType: string = this.riGrid.Details.GetAttribute('WCNProductCode', 'additionalproperty');
        let contractTypeURLParameter: string = '';
        if (contractType === 'J') {
            contractTypeURLParameter = '<job>';
        } else if (contractType === 'J') {
            contractTypeURLParameter = '<product>';
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'VisitDueDate':
                this.store.dispatch({
                    type: ContractActionTypes.SAVE_DATA,
                    payload: {
                        'ContractNumber': this.getControlValue('ContractNumber'),
                        'ContractName': this.getControlValue('ContractName'),
                        'PremiseNumber': this.getControlValue('PremiseNumber'),
                        'PremiseName': this.getControlValue('PremiseName'),
                        'ProductCode': this.getControlValue('ProductCode'),
                        'ProductDesc': this.getControlValue('ProductDesc'),
                        'PremiseRowID': this.getControlValue('Premise'),
                        'CurrentContractTypeURLParameter': contractTypeURLParameter,
                        'currentContractType': contractType
                    }
                });
                this.navigate('UnreturnedConsignmentGrid', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITSUMMARY);
                break;
            case 'VisitTypeCode':
                if (this.riGrid.Details.GetValue('VisitTypeCode')) {
                    this.navigate('Summary', InternalMaintenanceServiceModuleRoutes.ICABSSESERVICEVISITMAINTENANCE, {
                        ProductCode: this.riGrid.Details.GetValue('WCNProductCode'),
                        ServiceVisitRowID: this.riGrid.Details.GetAttribute('VisitTypeCode', 'rowid'),
                        'CurrentContractTypeURLParameter': contractTypeURLParameter,
                        'currentContractType': contractType
                    });
                }
                break;
        }
    }

    public onBodyColumnBlur(_event: any): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }
        let gridQuery: QueryParams = this.getURLSearchParamObject();
        let formData: Record<string, any> = {};

        gridQuery.set(this.serviceConstants.Action, 6);

        formData[this.serviceConstants.BusinessCode] = this.businessCode();
        formData['WCNProductCode'] = this.riGrid.Details.GetValue('WCNProductCode');
        formData['WCNProductDesc'] = this.riGrid.Details.GetValue('WCNProductDesc');
        formData['EWCCode'] = this.riGrid.Details.GetValue('EWCCode');
        formData['VisitTypeCodeRowID'] = this.riGrid.Details.GetAttribute('VisitTypeCode', 'rowid');
        formData['VisitTypeCode'] = this.riGrid.Details.GetValue('VisitTypeCode');
        formData['VisitDueDateRowID'] = this.riGrid.Details.GetAttribute('VisitDueDate', 'rowid');
        formData['VisitDueDate'] = this.riGrid.Details.GetValue('VisitDueDate');
        formData['ActualVisitDate'] = this.riGrid.Details.GetValue('ActualVisitDate');
        formData['PlannedQuantity'] = this.riGrid.Details.GetValue('PlannedQuantity');
        formData['ServicedQuantityRowID'] = this.riGrid.Details.GetAttribute('ServicedQuantity', 'rowid');
        formData['ServicedQuantity'] = this.riGrid.Details.GetValue('ServicedQuantity');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData['SystemUniqueNumber'] = this.getControlValue('SystemUniqueNumber');
        formData['VisitStatus'] = this.getControlValue('VisitStatus');

        formData[this.serviceConstants.GridMode] = '3';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = 10;
        formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
        formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
        formData[this.serviceConstants.GridHeaderClickedColumn] = '';
        formData['riSortOrder'] = 'Ascending';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, gridQuery, formData).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (this.hasError(data)) {
                this.displayMessage(data);
            }
            this.riGrid.Mode = MntConst.eModeNormal;
        }).catch(error => {
            this.hasGridData = false;
            this.displayMessage(error);
        });
    }
}
