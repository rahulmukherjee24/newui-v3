import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { PostCodeUtils } from './../../../../shared/services/postCode-utility';
import { Injector } from '@angular/core';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';

export class ServiceCoverMaintenance1 {

    private context: ServiceCoverMaintenanceComponent;
    private PostCodeDefaultingFunctions: PostCodeUtils;

    constructor(private parent: ServiceCoverMaintenanceComponent, injector: Injector) {
        this.context = parent;
        this.PostCodeDefaultingFunctions = injector.get(PostCodeUtils);
    }

    public init(): void {
        this.PostCodeDefaultingFunctions.PostCodeList().subscribe(
            (res) => {
                this.context.pageParams.vEnablePostcodeDefaulting = res;
            },
            (error) => {
                this.context.pageParams.vEnablePostcodeDefaulting = false;
            });
        this.context.iCABSAServiceCoverMaintenanceLoad.window_onload();
    }

    /************************************************************************************
     * BEFORE SELECT (Hide fields)      *
     ************************************************************************************/

    public riMaintenance_BeforeSelect(): void {
        this.context.pageParams.uiDisplay.trInstallationValue = false;
        this.context.pageParams.uiDisplay.trRemovalValue = false;
        this.context.pageParams.uiDisplay.trServiceAnnualValue = false;
        this.context.pageParams.uiDisplay.cmdValue = false;
        this.context.pageParams.uiDisplay.labelInactiveEffectDate = false;
        this.context.pageParams.uiDisplay.InactiveEffectDate = false;
        this.context.pageParams.uiDisplay.tdReasonLab = false;
        this.context.pageParams.uiDisplay.tdReason = false;
        this.context.pageParams.uiDisplay.trAverageUnitValue = false;
        this.context.pageParams.uiDisplay.trServiceVisitFrequencyCopy = false;
        this.context.pageParams.uiDisplay.trServiceVisitCycleFields1 = false;
        this.context.pageParams.uiDisplay.trServiceVisitCycleFields2 = false;
        this.context.pageParams.uiDisplay.trServiceVisitCycleFields3 = false;
        this.context.pageParams.uiDisplay.trStaticVisit = false;
        this.context.pageParams.uiDisplay.trDOWSentricon = false;
        this.context.pageParams.uiDisplay.trDOWInstall = false;
        this.context.pageParams.uiDisplay.trDOWProduct = false;
        this.context.pageParams.uiDisplay.trDOWPerimeter = false;
        this.context.pageParams.uiDisplay.trDOWRenewalDate = false;
        this.context.pageParams.uiDisplay.tdLineOfService_innerhtml = '';
        this.context.pageParams.uiDisplay.tdLineOfService = false;
        this.context.pageParams.uiDisplay.trUnitValue = false;



        this.context.setControlValue('selTaxCode', '');

        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered')) {
            this.context.pageParams.uiDisplay.trUnitValue = false;
        }

    }

    public CalAverageUnitValue(): void {
        if (!this.context || !this.context.utils || this.context.pageParams.hasFetchedAverageUnitValue || !this.context.getControlValue('ServiceCoverROWID')) {
            return;
        }
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';

        this.context.riMaintenance.PostDataAddFunction('CalculateAverageUnitValue');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.getControlValue('ServiceCoverROWID'), MntConst.eTypeText);

        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.pageParams.hasFetchedAverageUnitValue = true;
            this.context.setControlValue('AverageUnitValue', data['AverageUnitValue']);
        }, 'POST', 6, false);
    }

    public CalDisplayValues(): void {

        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {

            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';

            if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
                this.context.riMaintenance.PostDataAddFunction('ReCalcDisplayValues');
                this.context.PostDataAddFromField('LastChangeEffectDate', MntConst.eTypeDate);
            } else {
                this.context.riMaintenance.PostDataAddFunction('CalcDisplayValues');
            }

            this.context.riMaintenance.PostDataAddBusiness();
            this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.pageParams.ServiceCoverRowID, MntConst.eTypeText);

            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('WedValue', data['WEDValue']);
                this.context.setControlValue('PricePerWED', data['PricePerWED']);
                this.context.setControlValue('MaterialsValue', data['MaterialsValue']);
                this.context.setControlValue('MaterialsCost', data['MaterialsCost']);
                this.context.setControlValue('LabourValue', data['LabourValue']);
                this.context.setControlValue('LabourCost', data['LabourCost']);
                this.context.setControlValue('ReplacementValue', data['ReplacementValue']);
                this.context.setControlValue('ReplacementCost', data['ReplacementCost']);

                switch (data['ReplacementIncludeInd']) {
                    case 'yes':
                        this.context.pageParams.rsPlantReplaceIndyes = true;
                        break;
                    case 'no':
                        this.context.pageParams.rsPlantReplaceIndno = true;
                        break;
                    case 'Mixed':
                        this.context.pageParams.rsPlantReplaceIndmixed = true;
                        break;
                }
            }, 'POST');

        }
    }

    public ServiceCoverWasteReq(): void {

        this.context.pageParams.blnServiceCoverWasteReq = false;
        this.context.iCABSAServiceCoverMaintenance8.GetServiceCoverWasteRequired();
    }

    /************************************************************************************
     * AFTER FETCH (Hide/Show fields)   *
     ************************************************************************************/
    public riMaintenance_AfterFetch(): void {
        let subjectToUplift: string = this.context.getRawControlValue('SubjectToUplift');

        if (!this.context || !this.context.utils) {
            return;
        }
        this.context.iCABSAServiceCoverMaintenance4.ShowHideBudgetBilling();
        this.context.iCABSAServiceCoverMaintenance6.ShowHideTermiteRelatedFields();
        this.context.attributes.DefaultEffectiveDate = null;
        this.context.attributes.DefaultEffectiveDateProduct = null;

        //this.context.iCABSAServiceCoverMaintenance3.SetQuickWindowSetValue('C');
        this.context.setRawControlValueFromField('SelServiceBasis', 'ServiceBasis');
        if (this.context.getRawControlValue('AutoAllocation')) {
            this.context.setRawControlValueFromField('SelAutoAllocation', 'AutoAllocation');
        }
        this.context.setRawControlValueFromField('SelAutoPattern', 'AutoPattern');

        this.context.setControlValue('SelSubjectToUplift', subjectToUplift || 'N');

        this.context.setRawControlValueFromField('SelUpliftVisitPosition', 'UpliftVisitPosition');

        this.context.iCABSAServiceCoverMaintenance2.SelServiceBasis_OnChange();
        this.context.iCABSAServiceCoverMaintenance8.SelSubjectToUplift_onChange();
        this.context.iCABSAServiceCoverMaintenance7.SelUpliftVisitPosition_onChange();

        if (this.context.pageParams.vbEnableWeeklyVisitPattern &&
            (this.context.getRawControlValue('ServiceBasis') === 'S' ||
                this.context.getRawControlValue('ServiceBasis') === 'T')) {
            this.context.pageParams.blnUseVisitCycleValues = false;
        }

        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeSelect) {
            this.context.pageParams.uiDisplay.tdLineOfService_innerhtml = this.context.getRawControlValue('LOSName');
            this.context.pageParams.uiDisplay.tdLineOfService = true;
            this.context.iCABSAServiceCoverMaintenance3.BuildTaxCodeCombo();

        }

        this.context.iCABSAServiceCoverMaintenance4.ShowFields();

        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
            this.context.iCABSAServiceCoverMaintenance1.CalAverageUnitValue();
        }
        this.context.iCABSAServiceCoverMaintenance6.CalculateUnitValue();

        if (this.context.pageParams.blnUseVisitCycleValues) {
            this.context.pageParams.SavedVisitCycleInWeeksOverrideNote = this.context.getRawControlValue('VisitCycleInWeeksOverrideNote');
            this.context.pageParams.SavedCalculatedVisits = this.context.getRawControlValue('CalculatedVisits');
            this.context.pageParams.SavedVisitCycleInWeeks = this.context.getRawControlValue('VisitCycleInWeeks');
            this.context.pageParams.SavedVisitsPerCycle = this.context.getRawControlValue('VisitsPerCycle');

            this.context.setControlValue('ServiceVisitFrequencyCopy', this.context.getControlValue('ServiceVisitFrequency'));
            if (!this.context.getRawControlValue('VisitFrequencyWarningMessage')) {
                this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning = true;
                this.context.pageParams.uiDisplay.VisitFrequencyWarningColour = this.context.getRawControlValue('VisitFrequencyWarningColour');
                this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning_innerText = this.context.getRawControlValue('VisitFrequencyWarningMessage');
            } else {
                this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning = false;
            }
        }

        //if ( user has Full Access || logged in neg || service branch then show value fields;
        this.context.pageParams.blnAccess = (this.context.pageParams.FullAccess === 'Full' || this.context.utils.getBranchCode().toString() === this.context.getControlValueAsString('ServiceBranchNumber')
            || this.context.utils.getBranchCode().toString() === this.context.getControlValueAsString('NegBranchNumber'));

        this.context.pageParams.blnValueRequired = (this.context.getRawControlValue('FieldHideList') &&
            this.context.getRawControlValue('FieldHideList').indexOf('ServiceAnnualValue') === -1);

        if (this.context.getRawControlValue('FieldHideList') &&
            this.context.getRawControlValue('FieldHideList').indexOf('MultipleTaxRates') === -1
            && this.context.pageParams.vbEnableMultipleTaxRates) {

            let isMultipleTaxRates: boolean = this.context.isControlChecked('MultipleTaxRates');
            this.context.pageParams.uiDisplay.trTaxHeadings = isMultipleTaxRates;
            this.context.pageParams.uiDisplay.trTaxMaterials = isMultipleTaxRates;
            this.context.pageParams.uiDisplay.trTaxLabour = isMultipleTaxRates;
            this.context.pageParams.uiDisplay.trTaxReplacement = isMultipleTaxRates;
            this.context.pageParams.uiDisplay.trConsolidateEqualTaxRates = isMultipleTaxRates;
        }

        if (this.context.isControlChecked('ValueRequiredInd')) {
            this.context.pageParams.uiDisplay.trZeroValueIncInvoice = true;
        }

        //Show Invoice Info;
        this.context.pageParams.uiDisplay.trInvoiceType = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceStartDate = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceEndDate = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceValue = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trForwardDateChangeInd = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceSuspend = this.context.pageParams.blnValueRequired;
        this.context.pageParams.uiDisplay.trInvoiceReleased = this.context.pageParams.blnValueRequired;

        this.context.pageParams.uiDisplay.trAnnualTime = this.context.isControlChecked('RequireAnnualTimeInd');

        this.context.iCABSAServiceCoverMaintenance7.BuildInvoiceTypeSelect();

        switch (this.context.pageParams.currentContractType) {

            case 'C':
                //if ( blnAccess && blnValueRequired && Not vbEnableServiceCoverDispLev ) {
                if (this.context.pageParams.blnAccess && this.context.pageParams.blnValueRequired) {
                    //if ( Contract show AnnualValueChange field (Amendments to value will be added as the change rather than actual value);
                    //Value Change will only be displayed if ( Service Cover Displays not switched on;

                    if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                        (this.context.isControlChecked('DisplayLevelInd') ||
                            this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                        this.context.isControlChecked('VisitTriggered')) {
                        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                            this.context.pageParams.uiDisplay.tdUnitValueChangeLab = true;
                            this.context.pageParams.uiDisplay.UnitValueChange = true;
                        }
                        this.context.pageParams.uiDisplay.trUnitValue = true;
                    }
                    this.context.pageParams.uiDisplay.tdAnnualValueChangeLab = true;
                    this.context.pageParams.uiDisplay.AnnualValueChange = true;
                    this.context.pageParams.uiDisplay.trInvoiceValue = true;
                    this.context.pageParams.uiDisplay.trServiceAnnualValue = true;
                } else {
                    if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                        (this.context.isControlChecked('DisplayLevelInd') ||
                            this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                        this.context.isControlChecked('VisitTriggered')) {
                        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                            this.context.pageParams.uiDisplay.tdUnitValueChangeLab = true;
                            this.context.pageParams.uiDisplay.UnitValueChange = true;
                        }
                        this.context.pageParams.uiDisplay.trUnitValue = false;
                    }
                    if (this.context.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                        this.context.pageParams.uiDisplay.tdAnnualValueChangeLab = this.context.pageParams.blnValueRequired;
                        this.context.pageParams.uiDisplay.AnnualValueChange = this.context.pageParams.blnValueRequired;
                    }
                    this.context.pageParams.uiDisplay.trInvoiceValue = false;
                    this.context.pageParams.uiDisplay.trServiceAnnualValue = false;
                }

                //#36525 Display Initial Value field for update of a Service cover if ( Initial Value SysChar is enabled;
                this.context.pageParams.uiDisplay.trInitialValue = (this.context.pageParams.vEnableInitialCharge && !this.context.pageParams.vEnableInitialChargeorInstall);
                this.context.setControlValue('AnnualTimeChange', '');
                this.context.setControlValue('AnnualValueChange', '0');

                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.setControlValue('UnitValueChange', '0');
                    this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'UnitValueChange');
                }
                this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'AnnualValueChange');

                if (this.context.pageParams.blnValueRequired && !this.context.riExchange.URLParameterContains('PendingReduction')) {

                    //Show Reneg Indicator && if ( set to true, show all Reneg associated fields;

                    this.context.pageParams.uiDisplay.trChkRenegContract = true;

                    this.context.pageParams.uiDisplay.tdRenegOldContract = this.context.isControlChecked('chkRenegContract');

                } else {
                    this.context.pageParams.uiDisplay.trChkRenegContract = false;
                }
                if (this.context.riMaintenance.CurrentMode !== MntConst.eModeUpdate) {
                    this.context.iCABSAServiceCoverMaintenance7.ToggleSeasonalDates();
                }

                if (this.context.pageParams.vbEnableEntitlement) {
                    this.context.iCABSAServiceCoverMaintenance3.ToggleEntitlementRequired();
                }

                if (this.context.pageParams.vbEnableTrialPeriodServices) {

                    if (this.context.isControlChecked('ContractTrialPeriodInd')) {

                        this.context.pageParams.uiDisplay.tdTrialPeriodInd = false;
                        this.context.pageParams.uiDisplay.tdContractTrialPeriodInd = true;
                        this.context.setControlValue('ContractTrialPeriodInd', true);
                        this.context.riMaintenance.DisableInput('ContractTrialPeriodInd');
                    } else {

                        this.context.pageParams.dtTrialPeriodEndDateRecordValue = this.context.getRawControlValue('TrialPeriodEndDate');
                        this.context.pageParams.deTrialPeriodChargeRecordValue = this.context.getRawControlValue('TrialPeriodChargeValue');
                        this.context.pageParams.uiDisplay.tdTrialPeriodInd = true;
                        this.context.pageParams.uiDisplay.tdContractTrialPeriodInd = false;
                        this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
                    }

                }

                let isFOCChecked: boolean = this.context.isControlChecked('chkFOC');
                this.context.pageParams.uiDisplay.tdFOCInvoiceStartDate = isFOCChecked;
                this.context.pageParams.uiDisplay.tdFOCProposedAnnualValue = isFOCChecked;
                break;
            case 'J':

                //if ( Job hide AnnualValueChange field (enter the full annual value not change in value);
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    (this.context.isControlChecked('DisplayLevelInd') ||
                        this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.pageParams.uiDisplay.tdUnitValueChangeLab = false;
                    this.context.pageParams.uiDisplay.UnitValueChange = false;
                }
                this.context.pageParams.uiDisplay.tdAnnualValueChangeLab = false;
                this.context.pageParams.uiDisplay.AnnualValueChange = false;

                //Hide all Reneg associated fields;
                this.context.pageParams.uiDisplay.trChkRenegContract = false;

                //if ( Contract.FirstInvoicedDate is NE ? Then hide invoice fields;
                if (this.context.getRawControlValue('FirstInvoicedDate') === 'yes') {
                    this.context.pageParams.uiDisplay.trInvoiceType = false;
                    this.context.pageParams.uiDisplay.trInvoiceStartDate = false;
                    this.context.pageParams.uiDisplay.trInvoiceEndDate = false;
                    this.context.pageParams.uiDisplay.trInvoiceValue = false;
                    this.context.pageParams.uiDisplay.trForwardDateChangeInd = false;
                }

                //Only users with full access || logged in as neg || service branch can view values;
                if (this.context.pageParams.blnAccess && this.context.pageParams.blnValueRequired) {
                    if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                        (this.context.isControlChecked('DisplayLevelInd') ||
                            this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                        this.context.isControlChecked('VisitTriggered')) {
                        this.context.pageParams.uiDisplay.trUnitValue = true;
                    }
                    this.context.pageParams.uiDisplay.trServiceAnnualValue = true;
                } else {
                    if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                        (this.context.isControlChecked('DisplayLevelInd') ||
                            this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                        this.context.isControlChecked('VisitTriggered')) {
                        this.context.pageParams.uiDisplay.trUnitValue = false;
                    }
                    this.context.pageParams.uiDisplay.trServiceAnnualValue = false;
                    this.context.pageParams.uiDisplay.trInvoiceValue = false;
                }

        }

        //Hide/Show Term/Delete Date;
        if (this.context.getRawControlValue('InactiveEffectDate')) {
            this.context.pageParams.uiDisplay.labelInactiveEffectDate = true;
            this.context.pageParams.uiDisplay.InactiveEffectDate = true;

            if (this.context.getRawControlValue('SCLostBusinessDesc')) {
                this.context.pageParams.uiDisplay.tdReasonLab = true;
                this.context.pageParams.SCLostBusinessDesc_title = this.context.getRawControlValue('SCLostBusinessDesc2') +
                    '\n' + this.context.getRawControlValue('SCLostBusinessDesc3');
                this.context.pageParams.uiDisplay.tdReason = true;
            } else {
                this.context.pageParams.uiDisplay.tdReasonLab = false;
                this.context.pageParams.uiDisplay.tdReason = false;
            }

        } else {
            this.context.pageParams.uiDisplay.labelInactiveEffectDate = false;
            this.context.pageParams.uiDisplay.InactiveEffectDate = false;
            this.context.pageParams.uiDisplay.tdReasonLab = false;
            this.context.pageParams.uiDisplay.tdReason = false;
        }

        //Hide/Show Future Change button;
        this.context.pageParams.uiDisplay.cmdValue = (this.context.pageParams.blnValueRequired && this.context.isControlChecked('ShowValueButton'));
        this.context.disableControl('cmdValue', !this.context.pageParams.uiDisplay.cmdValue);

        //if ( come from Client Retention then default the LastChangeEffectDate from OutcomeEffectDate on Contact;
        if (this.context.parentMode === 'ContactUpdate') {
            this.context.setControlValue('OutcomeEffectDate',
                this.context.riExchange.getParentHTMLValue('OutcomeEffectDate'));
            this.context.setControlValue('LastChangeEffectDate',
                this.context.riExchange.getParentHTMLValue('LastChangeEffectDate'));
        }

        //Hide Outstanding Installation details
        this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
        this.context.pageParams.uiDisplay.trInstallationEmployee = false;
        this.context.pageParams.uiDisplay.trInstallationValue = false;
        this.context.setControlValue('OutstandingInstallations', this.context.getControlValue('ServiceQuantity') || '0');

        this.context.pageParams.uiDisplay.trChkStockOrder = (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd && (this.context.getRawControlValue('StockOrderAllowed') === 'yes'));
        this.context.pageParams.vbEnableRouteOptimisation = this.context.pageParams.vEnableRouteOptimisation;

        if (this.context.pageParams.vbEnableRouteOptimisation !== true) {
            this.context.pageParams.uiDisplay.trAutoRouteProductInd = false;
        }

        if (this.context.isControlChecked('RequireAnnualTimeInd')) {
            this.context.pageParams.uiDisplay.trAnnualTime = true;
            this.context.pageParams.uiDisplay.trStandardTreatmentTime = false;
            this.context.setRequiredStatus('StandardTreatmentTime', false);
            //StandardTreatmentTime.value = FormatDateTime(TimeSerial(0, 0, 0), vbShortTime);
        } else {
            this.context.pageParams.uiDisplay.trAnnualTime = false;

            if (this.context.pageParams.vbEnableStandardTreatmentTime) {
                this.context.pageParams.uiDisplay.trStandardTreatmentTime = true;
                this.context.setRequiredStatus('StandardTreatmentTime', true);
            }
        }

        //Store saved values to be used as comparison to determine what qty's have been changed.;
        let serviceVisitFrequency: number = parseInt(this.context.getRawControlValue('ServiceVisitFrequency'), 10);
        this.context.setControlValue('SavedServiceVisitFrequency', !isNaN(serviceVisitFrequency) ? serviceVisitFrequency : 0);

        this.context.pageParams.SavedServiceVisitFrequency = this.context.getRawControlValue('ServiceVisitFrequency');

        let serviceQuantity: number = parseInt(this.context.riExchange.riInputElement.GetValue(this.context.uiForm, 'ServiceQuantity'), 10);
        this.context.setControlValue('SavedServiceQuantity', !isNaN(serviceQuantity) ? serviceQuantity : 0);

        let serviceAnnualValue: number = parseInt(this.context.riExchange.riInputElement.GetValue(this.context.uiForm, 'ServiceAnnualValue'), 10);
        this.context.pageParams.SavedServiceAnnualValue = !isNaN(serviceAnnualValue) ? serviceAnnualValue : '0.00';

        this.context.pageParams.SavedInitialTreatmentTime = this.context.getRawControlValue('InitialTreatmentTime');

        this.context.pageParams.SavedServiceAnnualTime = this.context.getRawControlValue('ServiceAnnualTime');

        this.context.pageParams.SavedWasteTransferType = this.context.getRawControlValue('WasteTransferTypeCode');

        this.context.pageParams.uiDisplay.tdNationalAccount = (this.context.isControlChecked('NationalAccountChecked') &&
            this.context.isControlChecked('NationalAccount'));

        this.context.pageParams.uiDisplay.tdPNOL = this.context.isControlChecked('PNOL');

        this.context.pageParams.uiDisplay.tdCustomerInfo = this.context.isControlChecked('CustomerInfoAvailable');

        //Format SeqNo Display;
        if (this.context.getRawControlValue('BranchServiceAreaSeqNo')) {
            if ((this.context.getRawControlValue('BranchServiceAreaSeqNo')).length < 4) {
                this.context.setControlValue('BranchServiceAreaSeqNo',
                    this.context.utils.Format(this.context.getRawControlValue('BranchServiceAreaSeqNo'), '0000'));
            } else {
                if ((this.context.getRawControlValue('BranchServiceAreaSeqNo')).length > 4) {
                    this.context.setControlValue('BranchServiceAreaSeqNo',
                        this.context.utils.Format(this.context.getRawControlValue('BranchServiceAreaSeqNo'), '000000'));
                }
            }
        }

        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'InvTypeSel');

        this.context.pageParams.uiDisplay.tdContractHasExpired = (this.context.getControlValue('ContractHasExpired') === 'Yes');

        this.context.pageParams.vbServiceQuantity = this.context.getControlValueAsString('ServiceQuantity') ? this.context.getControlValueAsString('ServiceQuantity') : '';
        this.context.pageParams.vbServiceVisitFrequency = this.context.getControlValueAsString('ServiceVisitFrequency') ? this.context.getControlValueAsString('ServiceVisitFrequency') : '0';
        this.context.pageParams.vbPriceChangeOnlyInd = false;

        if (this.context.pageParams.currentContractType !== 'P') {
            this.context.iCABSAServiceCoverMaintenance7.DOWSentriconToggle();
        }

        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
        this.context.iCABSAServiceCoverMaintenance8.EnableRMMFields();
        this.context.iCABSAServiceCoverMaintenance2.HardSlotType_OnChange();
        this.context.iCABSAServiceCoverMaintenance5.AutoRouteProductInd_onClick();
        this.context.iCABSAServiceCoverMaintenance2.ServiceQuantityLabel();

        if (subjectToUplift !== '' && !this.context.getRawControlValue('RequireAnnualTimeInd')) {
            this.context.pageParams.uiDisplay.trUplift = true;
            this.context.iCABSAServiceCoverMaintenance8.GetUpliftStatus();
            this.context.pageParams.uiDisplay.trUpliftCalendar = (subjectToUplift === 'U');
        }

        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.iCABSAServiceCoverMaintenance1.riMaintenance_BeforeAdd();
        }
        this.context.iCABSAServiceCoverMaintenance2.riMaintenanceAfterEvent();
    }


    /************************************************************************************
     * BEFORE ADD MODE (Default values/Hide/Show fields)*
     ************************************************************************************/

    public riMaintenance_BeforeAddMode(): void {
        // Change for IUI-25174
        this.context.disableControl('ProductCode', false);
        this.context.riMaintenance.CurrentMode = MntConst.eModeAdd;
        this.context.setControlValue('ComponentGridCacheTime', this.context.utils.Time());
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdComponentSelAll');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdComponentDesAll');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'selTaxCode');

        this.context.setControlValue('SelAutoPattern', 'D');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelAutoPattern');
        this.context.setControlValue('AutoPattern', this.context.getRawControlValue('SelAutoPattern'));

        //Blank Line of Service value && hide field on new records;
        //this.context.pageParams.tdLineofService_innerhtml = '';
        //this.context.pageParams.uiDisplay.tdLineOfService = false;

        this.context.iCABSAServiceCoverMaintenance7.HideQuickWindowSet(false);

        if (this.context.pageParams.vbDefaultTaxCodeOnServiceCoverMaintLog === true) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        }

        this.context.setControlValue('selTaxCode', true);
        this.context.setControlValue('DepositCanAmend', 'Y');

        this.context.pageParams.SavedIncreaseQuantity = 0;

        this.context.pageParams.uiDisplay.tdUnitValueChangeLab = false;
        this.context.pageParams.uiDisplay.UnitValueChange = false;

        //Show Invoice Start/End/Val fields;
        this.context.pageParams.uiDisplay.trInvoiceType = true;
        this.context.pageParams.uiDisplay.trInvoiceStartDate = true;
        this.context.pageParams.uiDisplay.trInvoiceEndDate = true;
        this.context.pageParams.uiDisplay.trInvoiceValue = true;
        this.context.pageParams.uiDisplay.trForwardDateChangeInd = true;
        this.context.pageParams.tdAnnualTimeChange = false;
        this.context.pageParams.tdAnnualTimeChangeLab = false;

        //SH 03/07/2003 - do not show inv.suspend details when in 'add' mode;
        this.context.pageParams.uiDisplay.trInvoiceSuspend = false;
        this.context.pageParams.uiDisplay.trInvoiceReleased = false;
        this.context.setControlValue('InstallByBranchInd', true);

        //Hide Delete/Term Date (not applicable);
        this.context.pageParams.uiDisplay.labelInactiveEffectDate = false;
        this.context.pageParams.uiDisplay.InactiveEffectDate = false;
        this.context.pageParams.uiDisplay.tdReasonLab = false;
        this.context.pageParams.uiDisplay.tdReason = false;
        this.context.pageParams.uiDisplay.trEffectiveDate = false;

        //Hide value button/Waste Transfer details;
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered')) {
            this.context.pageParams.uiDisplay.trUnitValue = false;
        }
        //this.context.pageParams.uiDisplay.trServiceAnnualValue = false;
        this.context.pageParams.uiDisplay.cmdValue = false;
        this.context.pageParams.uiDisplay.tdWasteTransfer = false;
        //trAnnualValueChange.height = '1';

        this.context.pageParams.uiDisplay.trDeliveryConfirmation = false;

        //Enable Copy Service Cover button;
        if (this.context.getRawControlValue('ContractNumber') !== ''
            && this.context.getRawControlValue('PremiseNumber') !== '') {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdCopyServiceCover');
        } else {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdCopyServiceCover');
        }

        this.context.pageParams.uiDisplay.trDepositLineAdd = false;
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DepositAmount');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DepositDate');

        if (this.context.pageParams.currentContractType === 'C') {
            //Only show Renegotitated checkbox if ( Contract;
            this.context.pageParams.uiDisplay.trChkRenegContract = true;
            //SRS 41 C012260 Hide FOC fields until indicator checked;
            this.context.pageParams.uiDisplay.tdFOCInvoiceStartDate = false;
            this.context.pageParams.uiDisplay.tdFOCProposedAnnualValue = false;
        } else {
            //SH 13/05/2003 QRS0576 - ensure that 'reneg' option is not shown for JOBS;
            this.context.pageParams.uiDisplay.trChkRenegContract = false;
        }

        //And hide the AnnualValueChange field (when in add mode just enter the actual annual value);
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered')) {
            this.context.pageParams.uiDisplay.tdUnitValueChangeLab = false;
            this.context.pageParams.uiDisplay.UnitValueChange = false;
        }

        this.context.pageParams.uiDisplay.tdAnnualValueChangeLab = false;
        this.context.pageParams.uiDisplay.AnnualValueChange = false;
        //Default Service Cover information from the Premise record;
        if (this.context.getRawControlValue('ContractNumber') !== ''
            && this.context.getRawControlValue('PremiseNumber') !== ''
            && (this.context.hasControlChanged('ContractNumber')
                || this.context.hasControlChanged('PremiseNumber'))) {

            let isNewPremise: string = (this.context.parentMode === 'Premise-Add' || this.context.pageParams.currentContractType === 'J') ? 'yes' : 'no';
            this.context.setControlValue('NewPremise', isNewPremise);

            this.context.riMaintenance.CBORequestClear();
            this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=GetServiceDefaultValues,GetPremiseWindows';
            this.context.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
            this.context.riMaintenance.CBORequestAdd('ContractNumber');
            this.context.riMaintenance.CBORequestAdd('PremiseNumber');
            this.context.riMaintenance.CBORequestAdd('NewPremise');
            this.context.riMaintenance.CBORequestExecute(this.context, function (data: any): any {
                /**
                 * @todo - Check this loop
                 */
                if (!this.context.copyMode && data) {
                    for (let id in data) {
                        if (id && id !== 'NextInvoiceStartDate' && id !== 'NextInvoiceEndDate') {
                            let respData = data[id];
                            let ctrlType = this.context.riMaintenance.getControlType(this.context.controls, id, 'type');
                            if (ctrlType) {
                                respData = this.context.riMaintenance.respDataFormatUI(ctrlType, respData);
                                if (ctrlType === MntConst.eTypeDate) {
                                    this.context.riMaintenance.updateDateField(this.context, respData, id);
                                }
                                this.context.riExchange.updateCtrl(this.context.controls, id, 'value', respData);
                                this.context.setControlValue(id, respData);
                            } else {
                                this.context.riExchange.updateCtrl(this.context.controls, id, 'value', respData);
                                this.context.setControlValue(id, respData);
                            }
                        }
                    }
                    this.context.setControlValue('ClosedTemplateName', this.context.getControlValue('TemplateName'));
                    this.context.setControlValue('TemplateName', '');
                }
                this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strEntryProcedure;
                if (this.context.isControlChecked('PremiseDefaultTimesInd')) {
                    this.context.iCABSAServiceCoverMaintenance3.SetQuickWindowSetValue('P');
                    this.context.iCABSAServiceCoverMaintenance2.UpdateDefaultTimes('Premise');
                } else {
                    this.context.iCABSAServiceCoverMaintenance3.SetQuickWindowSetValue('D');
                    this.context.iCABSAServiceCoverMaintenance2.UpdateDefaultTimes('Default');
                }
            });
        }


        //Arrived from Premise maintenance after selecting a Renegotiated Premise;
        //MSA 15/09/2004 - Changed slightly to fix problem with lazy evaluation;
        if (this.context.parentMode === 'Premise-Add') {
            if (this.context.riExchange.getParentAttributeValue('RenegContract') !== null
                && this.context.riExchange.getParentAttributeValue('RenegContract') === 'true') {

                this.context.setControlValue('chkRenegContract', true);
                this.context.iCABSAServiceCoverMaintenance3.chkRenegContract_onclick();

                this.context.riMaintenance.DisableInput('chkRenegContract');
                this.context.riMaintenance.DisableInput('RenegOldContract');
                this.context.riMaintenance.DisableInput('RenegOldPremise');

                this.context.setControlValue('RenegOldContract', this.context.riExchange.getParentAttributeValue('ContractNumber'));
                this.context.setControlValue('RenegOldPremise', this.context.riExchange.getParentAttributeValue('PremiseNumber'));

            }
        }

        //MSA - Seasonal Service;
        this.context.riMaintenance.DisableInput('SeasonalBranchUpdate');

        //MSA - Reset tabs to default;
        this.context.iCABSAServiceCoverMaintenance7.CreateTabs();
        //05/01/06 PG #14138 Prevent fields && tabs going red due to moving the focus;
        this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'ServiceCommenceDate', false);
        this.context.setRequiredStatus('ServiceCommenceDate', true);
        this.context.riExchange.updateCtrl(this.context.controls, 'ServiceCommenceDate', 'required', true);
        this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'ContractNumber', false);
        this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'PremiseNumber', false);
        this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'ProductCode', false);
        //Call riTab.TabRefreshStatus();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddFunction('GetDefaultServiceType');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            if (!(this.context.copyMode && this.context.fieldHasValue('ServiceTypeCode'))) {
                this.context.setControlValue('ServiceTypeCode', data['ServiceTypeCode']);
                this.context.setControlValue('ServiceTypeDesc', data['ServiceTypeDesc']);
                this.context.setControlValue('ServiceBasis', 'N');
                this.context.setDropDownComponentValue('ServiceTypeCode', 'ServiceTypeDesc');
            }
            this.context.iCABSAServiceCoverMaintenance2.ServiceBasis_OnChange();

            this.context.iCABSAServiceCoverMaintenance2.WindowPreferredIndChanged();
            if (!(this.context.copyMode && this.context.fieldHasValue('TaxCode'))) {
                this.context.iCABSAServiceCoverMaintenance3.GetDefaultTaxCode();
            }
            if (this.context.parentMode === 'Premise-Add') {
                this.context.iCABSAServiceCoverMaintenance3.DefaultFromProspect();
                this.context.iCABSAServiceCoverMaintenance3.LeadEmployeeDisplay();
                if (this.context.isControlChecked('LeadInd')) {
                    this.context.riExchange.riInputElement.focus('LeadEmployee');
                }
                this.context.iCABSAServiceCoverMaintenance3.BusinessOriginDetailDisplay();
            }
            this.context.iCABSAServiceCoverMaintenance4.ShowFields();

            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd && this.context.pageParams.vbEnableProductServiceType) {
                this.context.iCABSAServiceCoverMaintenance1.GetProductServiceType();
            }
            if (this.context.fieldHasValue('ProductCode') && !this.context.riMaintenance.processingFlag) {
                this.context.utils.getFirstFocusableFieldForTab(1);
            } else {
                setTimeout(() => {
                    if (this.context.fieldHasValue('ProductCode') && !this.context.riMaintenance.processingFlag) {
                        this.context.utils.getFirstFocusableFieldForTab(1);
                    }
                }, 500);
            }
        }, 'POST');
    }


    /************************************************************************************
     * BEFORE ADD (Default values/Hide/Show fields)
     ************************************************************************************/

    public riMaintenance_BeforeAdd(): void {


        this.context.pageParams.uiDisplay.trEffectiveDate = false;
        this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
        this.context.pageParams.uiDisplay.trRemovalEmployee = false;
        this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
        this.context.pageParams.uiDisplay.trInstallationEmployee = false;
        this.context.pageParams.uiDisplay.trRefreshDisplayVal = false;
        this.context.setControlValue('BranchServiceAreaSeqNo', '0');
        this.context.pageParams.uiDisplay.trUplift = false;
        this.context.pageParams.uiDisplay.trUpliftCalendar = false;
        this.context.setControlValue('RMMJobVisitValue', '');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'ServiceCommenceDate');

        this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning_innerText = true;
        this.context.pageParams.uiDisplay.tdNumberOfVisitsWarning = false;
        this.context.setRawControlValueFromField('SelServiceBasis', 'ServiceBasis');
        this.context.setRawControlValueFromField('SelSubjectToUplift', 'SubjectToUplift');
        this.context.setRawControlValueFromField('SelUpliftVisitPosition', 'UpliftVisitPositIon');
        this.context.setRawControlValueFromField('SelAutoPattern', 'AutoPattern');
        if (this.context.initialLoad) {
            this.context.setRawControlValueFromField('SelAutoAllocation', 'AutoAllocation');
        }
        this.context.iCABSAServiceCoverMaintenance2.SelServiceBasis_OnChange();

        if (this.context.pageParams.currentContractType === 'J') {

            //Disable ServiceCommenceDate after defaulting it from the Contract Commence Date
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ServiceCommenceDate');

        } else if (this.context.pageParams.currentContractType === 'C') {

            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SeasonalServiceInd');

            if (this.context.pageParams.vbEnableEntitlement) {
                this.context.iCABSAServiceCoverMaintenance3.ToggleEntitlementRequired();
            }
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelServiceBasis');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'selHardSlotType');
            //this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdHardSlotCalendar');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdDiaryView');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelSubjectToUplift');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelUpliftVisitPosition');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelAutoPattern');
            if (this.context.initialLoad) {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'SelAutoAllocation');
            }
        }
        //SRS 17 Build initial invoice type select;
        this.context.riMaintenance.CBORequestClear();
        this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=BuildInvoiceTypeSelect' + '&ContractTypeCode=' + this.context.pageParams.currentContractType;
        this.context.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
        this.context.riMaintenance.CBORequestExecute(this.context, function (data: any): any {
            if (data) {
                this.context.riMaintenance.renderResponseForCtrl(this.context, data);
            }
            this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.iCABSAServiceCoverMaintenance7.BuildInvoiceTypeSelect();
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'InvTypeSel');

            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                this.context.isControlChecked('DisplayLevelInd')) {
                //this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'ExpiryDate');

                if (!this.context.pageParams.vbEnableServiceCoverDepreciation) {
                    this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepreciationPeriod');
                }
            }
            this.context.iCABSAServiceCoverMaintenance4.ShowHideBudgetBilling();
            this.context.iCABSAServiceCoverMaintenance1.riMaintenance_BeforeAddMode();
        });
    }


    //*************** S P E E D S C R I P T   L O G I C ************************************************************************;

    public SetHTMLPageSettings(): void {

        //Hide/Show fields && set variables depending on whether System Chars are required || not;
        this.context.pageParams.vbEnablePostcodeDefaulting = this.context.pageParams.vEnablePostcodeDefaulting;
        this.context.pageParams.vbEnableServiceCoverDetail = this.context.pageParams.vEnableServiceCoverDetail;
        this.context.pageParams.vbEnableInstallsRemovals = this.context.pageParams.vEnableInstallsRemovals;
        this.context.pageParams.vbEnableLocations = this.context.pageParams.vEnableLocations;
        this.context.pageParams.vbEnableDetailLocations = this.context.pageParams.vEnableDetailLocations;
        this.context.pageParams.vbLocationsSingleEntry = this.context.pageParams.vLocationsSingleEntry;
        this.context.pageParams.vbEnableEntitlement = this.context.pageParams.vEnableEntitlement;
        this.context.pageParams.vbEnableEntitlementForWashroom = this.context.pageParams.vEnableEntitlementForWashroom;
        this.context.pageParams.vbEnableJobsToInvoiceAfterVisit = this.context.pageParams.vEnableJobsToInvoiceAfterVisit;
        this.context.setControlValue('NationalAccountChecked', this.context.pageParams.vEnableNationalAccountWarning);
        this.context.pageParams.vbEnableFreeOfChargeServices = this.context.pageParams.vEnableFreeOfChargeServices;
        this.context.pageParams.vbEnableTrialPeriodServices = this.context.pageParams.vEnableTrialPeriodServices;
        this.context.pageParams.vbEnableStandardTreatmentTime = this.context.pageParams.vEnableSTTEntry;
        this.context.pageParams.vbEnableInitialTreatmentTime = this.context.pageParams.vEnableInitialTreatmentTime;
        this.context.pageParams.vbEnableAPICodeEntry = this.context.pageParams.vEnableAPICodeEntry;
        this.context.pageParams.vbEnableWorkLoadIndex = this.context.pageParams.vEnableWorkLoadIndex;
        this.context.pageParams.vbEnableMonthlyUnitPrice = this.context.pageParams.vEnableMonthlyUnitPrice;
        this.context.pageParams.vbSuspendSalesStatPortFig = this.context.pageParams.vSuspendSalesStatPortFig;
        this.context.pageParams.vbDefaultStockReplenishment = this.context.pageParams.vDefaultStockReplenishment;
        this.context.pageParams.vbEnableProductLinking = this.context.pageParams.vEnableProductLinking;

        this.context.pageParams.vbSuspendSalesStatPortFig = this.context.pageParams.vSuspendSalesStatPortFig;
        this.context.pageParams.vbEnableWeeklyVisitPattern = this.context.pageParams.vEnableWeeklyVisitPattern;
        this.context.pageParams.vbEnableTimePlanning = this.context.pageParams.vEnableTimePlanning;
        this.context.pageParams.vbShowInspectionPoint = this.context.pageParams.vShowInspectionPoint;
        this.context.pageParams.vbShowPremiseWasteTab = this.context.pageParams.vShowPremiseWasteTab;
        this.context.pageParams.vbEnableServiceCoverAvgWeightReq = this.context.pageParams.vEnableServiceCoverAvgWeightReq;
        this.context.pageParams.vbEnableServiceCoverDispLev = this.context.pageParams.vEnableServiceCoverDispLev;
        this.context.pageParams.vbEnableServiceCoverDepreciation = this.context.pageParams.vEnableServiceCoverDepreciation;
        this.context.pageParams.vbDisplayLevelInstall = this.context.pageParams.vDisplayLevelInstall;
        this.context.pageParams.vbEnableSpecificVisitDays = this.context.pageParams.vbEnableSpecificVisitDays;
        this.context.pageParams.vbEnableDeliveryRelease = this.context.pageParams.vEnableDeliveryRelease;
        this.context.pageParams.vbEnableMultipleTaxRates = this.context.pageParams.vEnableMultipleTaxRates;
        this.context.pageParams.vbOverrideMultipleTaxRates = this.context.pageParams.vOverrideMultipleTaxRates;

        this.context.pageParams.vbDefaultTaxCodeOnServiceCoverMaintReq = this.context.pageParams.vDefaultTaxCodeProductExpenseReq;
        this.context.pageParams.vbDefaultTaxCodeOnServiceCoverMaintLog = this.context.pageParams.vDefaultTaxCodeProductExpenseLog;
        this.context.pageParams.vWasteTransferReq = this.context.pageParams.vSCEnableWasteTransfer;
        this.context.pageParams.vbEnableProductServiceType = this.context.pageParams.vSCEnableProductServiceType;
        this.context.pageParams.vbEnableDepositProcessing = this.context.pageParams.vEnableDepositProcessing;
        this.context.pageParams.vbEnableRMM = this.context.pageParams.vEnableRMM;

        if (this.context.pageParams.vbEnableServiceCoverAvgWeightReq) {
            this.context.pageParams.vbEnableServiceCoverAvgWeightText = this.context.pageParams.vEnableServiceCoverAvgWeightText;
        }

        if (this.context.pageParams.currentContractType === 'J') {
            this.context.pageParams.vbEnableWeeklyVisitPattern = false;
        }

        if (this.context.pageParams.vbEnableJobsToInvoiceAfterVisit) {
            this.context.pageParams.JobInvoiceFirstVisitValue = this.context.pageParams.vJobInvoiceFirstVisitValue;
        }

        this.context.pageParams.uiDisplay.trRetainServiceWeekday = this.context.pageParams.vEnableRetentionOfServiceWeekDay;
        this.context.pageParams.uiDisplay.trInitialValue = this.context.pageParams.vEnableInitialCharge;
        this.context.pageParams.uiDisplay.trWorkLoadIndex = this.context.pageParams.vEnableWorkLoadIndex;
        this.context.pageParams.uiDisplay.trMonthlyUnitPrice = this.context.pageParams.vEnableMonthlyUnitPrice;
        this.context.pageParams.uiDisplay.trServiceDepot = this.context.pageParams.vShowServiceDepot;

        if (this.context.pageParams.vbEnableServiceCoverDispLev) {
            this.context.pageParams.uiDisplay.trWEDValue = this.context.pageParams.vEnableWED;
            this.context.pageParams.uiDisplay.trPricePerWED = this.context.pageParams.vEnableWED;
            this.context.pageParams.uiDisplay.trMinimumDuration = true;

        } else {
            this.context.pageParams.uiDisplay.trMinimumDuration = false;
        }

        this.context.pageParams.uiDisplay.trUnitValue = (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered'));

        this.context.pageParams.uiDisplay.trAPICode = (this.context.pageParams.vbEnableAPICodeEntry && this.context.pageParams.currentContractType === 'C');

        this.context.pageParams.uiDisplay.InactiveEffectDate = false;

        //Disable Copy Service Cover button;
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdCopyServiceCover');
        this.context.pageParams.uiDisplay.trWasteTransferType = this.context.pageParams.vShowPremiseWasteTab;

        this.context.iCABSAServiceCoverMaintenance8.EnableRMMFields();
    }

    public ProductCode_OnChange(): void {
        this.context.initialLoad = true;
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.iCABSAServiceCoverMaintenance6.TermiteServiceCheck();
            this.context.iCABSAServiceCoverMaintenance3.GetDefaultTaxCode();
        }

        this.context.iCABSAServiceCoverMaintenance1.GetProductDescription();

        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd && this.context.pageParams.vbEnableProductServiceType) {
            this.context.iCABSAServiceCoverMaintenance1.GetProductServiceType();
        }
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.iCABSAServiceCoverMaintenance7.CheckServiceNotifyInd();
        }
        if (this.context.pageParams.boolPropertyCareInd === 'Y' && this.context.pageParams.boolUserWriteAccess === 'yes') {
            this.context.iCABSAServiceCoverMaintenance1.CheckGuaranteeRequiredInd();
        }
        this.context.iCABSAServiceCoverMaintenance8.EnableRMMFields();
        this.context.iCABSAServiceCoverMaintenance7.APTChangedAccordingToQuantity();
        this.context.linkedServiceCoverSearchParams.ProductCode = this.context.getRawControlValue('ProductCode');
    }

    public GetProductServiceType(): void {
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddFunction('GetProductServiceType');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('ServiceTypeCode', data['ServiceTypeCode']);
            this.context.setControlValue('ServiceTypeDesc', data['ServiceTypeDesc']);
            this.context.setDropDownComponentValue('ServiceTypeCode', 'ServiceTypeDesc');
        }, 'POST');
    }

    public GetProductExpenseTaxCodeDefault(): void {
        if (this.context.pageParams.vbDefaultTaxCodeProductExpenseReq === true) {
            this.context.ajaxSource.next(this.context.ajaxconstant.START);
            let query = this.context.getURLSearchParamObject();
            query.set(this.context.serviceConstants.Action, '6');
            let formData = {
                'Function': 'GetProductExpenseDefaultTaxCode',
                'ContractTypeCode': this.context.pageParams.currentContractType,
                'ProductCode': this.context.getRawControlValue('ProductCode')
            };
            //formData = this.context.utils.cleanForm(formData);
            this.context.httpService.makePostRequest(this.context.xhrParams.method, this.context.xhrParams.module,
                this.context.xhrParams.operation, query, formData)
                .subscribe(
                    (data) => {
                        this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                        if (data.errorMessage) {
                            this.context.errorService.emitError(data.errorMessage);
                            this.context.showAlert(data.errorMessage, 0, data.fullError);
                        } else {
                            this.context.setControlValue('TaxCode', data['TaxCode']);
                            this.context.setControlValue('TaxDesc', data['TaxDesc']);
                            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
                            this.context.pageParams.selTaxCode = [];
                            let obj = {
                                text: data['TaxCode'] + ' - ' + data['TaxDesc'],
                                value: data['TaxCode']
                            };
                            this.context.pageParams.selTaxCode.push(obj);
                        }
                    },
                    (error) => {
                        this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
                        this.context.errorService.emitError('Record not found');
                    }
                );
        }
    }

    public GetProductDescription(): void {
        if (this.context.isReturning()) {
            return;
        }
        this.context.riMaintenance.BusinessObject = 'iCABSMassPriceChangeGrid.p';
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddFunction('GetProductDescription');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('ProductDesc', data['ProductDesc']);
            if (this.riMaintenance.CurrentMode !== MntConst.eModeAdd) {
                this.riMaintenance.CurrentMode = MntConst.eModeUpdate;
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeFetch();
            }
        }, 'POST');

    }

    public CheckGuaranteeRequiredInd(): void {
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.PostDataAddFunction('CheckGuaranteeRequiredInd');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            if (data['GuaranteeRequiredInd'] === 'y') {
                this.context.setControlValue('GuaranteeRequired', true);
                this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
            } else {
                this.context.setControlValue('GuaranteeRequired', false);
            }
        }, 'POST');
    }
}
