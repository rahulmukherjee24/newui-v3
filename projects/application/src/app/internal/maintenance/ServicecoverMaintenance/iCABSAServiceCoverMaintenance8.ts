import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { InternalGridSearchApplicationModuleRoutes } from './../../../base/PageRoutes';

export class ServiceCoverMaintenance8 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent) {
        this.context = parent;
    }

    public GetUpliftStatus(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.riMaintenance.GetRowID('ServiceCoverROWID'), MntConst.eTypeTextFree);
        this.context.riMaintenance.PostDataAddFunction('GetUpliftStatus');
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.pageParams.tdUpliftStatus_InnerText = data['UpliftStatus'];
        }, 'POST');
    }

    public UpliftTemplateNumber_onchange(): void {
        this.context.setControlValue('UpliftTemplateName', '');
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
    }

    public SelSubjectToUplift_onChange(): void {
        let selSubjectToUpliftValue: string = this.context.getRawControlValue('SelSubjectToUplift');
        this.context.setControlValue('SubjectToUplift', selSubjectToUpliftValue === 'N' ? '' : selSubjectToUpliftValue);
        this.context.setRequiredStatus('ClosedCalendarTemplateNumber', this.context.getRawControlValue('SubjectToUplift') === 'C');
        if (this.context.getRawControlValue('SubjectToUplift') === 'U') {
            this.context.setRequiredStatus('UpliftTemplateNumber', true);
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd ||
                this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate
                && this.context.isControlChecked('CapableOfUplift')) {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'UpliftTemplateNumber');
            }
            this.context.pageParams.uiDisplay.trUpliftCalendar = true;
        } else {
            this.context.setRequiredStatus('UpliftTemplateNumber', false);
            this.context.setControlValue('UpliftTemplateNumber', '');
            this.context.setControlValue('UpliftTemplateName', '');
            this.context.pageParams.uiDisplay.trUpliftCalendar = false;
        }
        if (!this.context.fieldHasValue('SubjectToUplift')) {
            this.context.pageParams.UpliftVisitPosLabel = false;
            this.context.pageParams.uiDisplay.selUpliftVisitPosLabel = false;
            this.context.setControlValue('UpliftVisitPosition', '');
            //tdUpliftStatus.style.backgroundcolor = true;
            this.context.pageParams.tdUpliftStatus_InnerText = '';
        } else {
            this.context.pageParams.UpliftVisitPosLabel = true;
            this.context.pageParams.uiDisplay.selUpliftVisitPosLabel = true;
            let selUpliftVisitPositionValue: string = this.context.getRawControlValue('UpliftVisitPosition');
            this.context.setControlValue('SelUpliftVisitPosition', this.context.riMaintenance.CurrentMode === MntConst.eModeAdd &&
                !this.context.pageParams.vbAbandon &&
                !selUpliftVisitPositionValue ? 'AU' : selUpliftVisitPositionValue);
            this.context.iCABSAServiceCoverMaintenance7.SelUpliftVisitPosition_onChange();
        }
    }

    public EnableRMMFields(): void {
        if (this.context.pageParams.vbEnableRMM) {
            if (!this.context.getRawControlValue('RMMCategoryCode') ||
                this.context.riExchange.riInputElement.isError(this.context.uiForm, 'RMMCategoryCode')) {
                this.context.pageParams.uiDisplay.tdRMMJobVisitValueLabel = false;
                this.context.pageParams.uiDisplay.tdRMMJobVisitValue = false;
            } else if (this.context.getRawControlValue('RMMCategoryCode')) {
                this.context.iCABSAServiceCoverMaintenance8.EnableDisableRMMJobVisitValue();
            }
        }
    }

    public EnableDisableRMMJobVisitValue(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddFunction('GetRMMCategoryDesc');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAdd('LanguageCode', this.context.riExchange.LanguageCode(), MntConst.eTypeCode);
        this.context.PostDataAddFromField('RMMCategoryCode', MntConst.eTypeCode);
        this.context.riMaintenance.ReturnDataAdd('ShowFreeCallouts', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context['pageParams'].RMMCategoryCodeSelected = {
                id: this.context.getControlValue('RMMCategoryCode'),
                text: data['RMMCategoryDesc']
            };
            this.context.setControlValue('RMMCategoryDesc', data['RMMCategoryDesc']);
            this.context.pageParams.vbNumVisits = data['NumVisits'];
            this.context.setControlValue('ShowFreeCallouts', data['ShowFreeCallouts']);
            if (data.hasError) {
                //this.context.errorModal.show(data, true);
                this.context.messageType = 'error';
                this.context.alertMessage = {
                    msg: 'ERROR: ' + data.errorMessage + ' ' + data.fullError,
                    timestamp: (new Date()).getMilliseconds()
                };
                return;
            }
            if (this.context.pageParams.vbNumVisits === '0') {
                this.context.pageParams.uiDisplay.tdRMMJobVisitValueLabel = false;
                this.context.pageParams.uiDisplay.tdRMMJobVisitValue = false;
                this.context.setControlValue('RMMJobVisitValue', '0.00');
            } else {
                this.context.pageParams.uiDisplay.tdRMMJobVisitValueLabel = true;
                this.context.pageParams.uiDisplay.tdRMMJobVisitValue = true;
            }
            if (!this.context.isControlChecked('ShowFreeCallouts')) {
                this.context.pageParams.uiDisplay.tdTotalFreeAdditionalVisitsAllowedLabel = false;
                this.context.pageParams.uiDisplay.tdTotalFreeAdditionalVisitsAllowed = false;
                this.context.pageParams.uiDisplay.tdCurrentAddnlVisitCountLabel = false;
                this.context.pageParams.uiDisplay.tdCurrentAddnlVisitCount = false;
                this.context.setControlValue('TotalFreeAddnlVisits', '0');
                this.context.setControlValue('CurrentAddnlVisitCount', '0');
            } else {
                this.context.pageParams.uiDisplay.tdTotalFreeAdditionalVisitsAllowedLabel = true;
                this.context.pageParams.uiDisplay.tdTotalFreeAdditionalVisitsAllowed = true;
                this.context.pageParams.uiDisplay.tdCurrentAddnlVisitCountLabel = true;
                this.context.pageParams.uiDisplay.tdCurrentAddnlVisitCount = true;
            }
        }, 'POST', 6, false);
    }

    public RMMCategoryCode_onChange(): void {
        this.context.iCABSAServiceCoverMaintenance8.EnableDisableRMMJobVisitValue();
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
    }

    public ResetPriceChangeVariable(): void {
        this.context.pageParams.uiDisplay.trPriceChangeOnly = false;
        this.context.pageParams.vbPriceChangeOnlyInd = false;
    }

    public GetServiceCoverWasteRequired(): void {
        //Determineif( Service Cover Waste Screen should be displayed based on Waste Transfer Type && Regulatory Authority;
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverFunctions.p';
        this.context.riMaintenance.PostDataAddFunction('GetServiceCoverWasteRequired');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
        this.context.PostDataAddFromField('PremiseNumber', MntConst.eTypeCode);
        this.context.PostDataAddFromField('WasteTransferTypeCode', MntConst.eTypeCode);
        this.context.riMaintenance.PostDataAdd('ServiceCoverRowID', this.context.riMaintenance.GetRowID('ServiceCoverROWID'), MntConst.eTypeText);
        this.context.riMaintenance.ReturnDataAdd('ServiceCoverWasteRequired', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.pageParams.blnServiceCoverWasteReq = data['ServiceCoverWasteRequired'];
            if (this.context.pageParams.blnServiceCoverWasteReq) {
                this.context.pageParams.saveReturnCallback = true;
                if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                    this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_1;
                } else {
                    this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_UPDATE_1;
                }
                this.context.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSASERVICECOVERWASTEGRID);
            }
        }, 'POST');
    }

    public UpdateSPT(): void {
        // This will only get run when EnabledTimePlanning is switched on;
        if (this.context.getRawControlValue('ServiceQuantity') && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
            this.context.riMaintenance.PostDataAddFunction('SetSPTValue');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
            this.context.PostDataAddFromField('ServiceQuantity', MntConst.eTypeCode);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('SalesPlannedTime', data['SalesPlannedTime']);
                if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                    this.context.setControlValue('ActualPlannedTime',
                        this.context.getRawControlValue('SalesPlannedTime'));
                }
            }, 'POST');
        }
    }

    public SeasonalFromDate_onchange(id: string): void {
        this.context.iCABSAServiceCoverMaintenance7.SeasonalDateChange('SeasonalFromDate' + id, 'SeasonalFromWeek' + id, 'SeasonalFromYear' + id);
    }

    public SeasonalFromWeek_onchange(id: string): void {
        this.context.iCABSAServiceCoverMaintenance3.SeasonalFromWeekChange('SeasonalFromWeek' + id, 'SeasonalFromYear' + id, 'SeasonalFromDate' + id);
    }

    public SeasonalFromYear_onchange(id: string): void {
        this.context.iCABSAServiceCoverMaintenance3.SeasonalFromWeekChange('SeasonalFromWeek' + id, 'SeasonalFromYear' + id, 'SeasonalFromDate' + id);
    }

    public SeasonalToDate_onchange(id: string): void {
        this.context.iCABSAServiceCoverMaintenance7.SeasonalDateChange('SeasonalToDate' + id, 'SeasonalToWeek' + id, 'SeasonalToYear' + id);
    }

    public SeasonalToWeek_onchange(id: string): void {
        this.context.iCABSAServiceCoverMaintenance3.SeasonalToWeekChange('SeasonalToWeek' + id, 'SeasonalToYear' + id, 'SeasonalToDate' + id);
    }

    public SeasonalToYear_onchange(id: string): void {
        this.context.iCABSAServiceCoverMaintenance3.SeasonalToWeekChange('SeasonalToWeek' + id, 'SeasonalToYear' + id, 'SeasonalToDate' + id);
    }

}
