import { InternalGridSearchApplicationModuleRoutes, InternalGridSearchServiceModuleRoutes } from './../../../base/PageRoutes';
import { ContractActionTypes } from './../../../actions/contract';
import { Injector } from '@angular/core';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from '../../../../shared/constants/message.constant';

export class ServiceCoverMaintenance6 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent, injector: Injector) {
        this.context = parent;
    }

    public ServiceQuantity_onchange(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            (this.context.isControlChecked('DisplayLevelInd') ||
                this.context.isControlChecked('InvoiceUnitValueRequired')) &&
            this.context.isControlChecked('VisitTriggered')) {
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                if (this.context.getRawControlValue('UnitValue') &&
                    this.context.getRawControlValue('UnitValue') !== '0') {
                    this.context.iCABSAServiceCoverMaintenance6.CalculateAnnualValue();
                }
            } else {
                if (this.context.getRawControlValue('UnitValueChange') &&
                    this.context.getRawControlValue('UnitValueChange') !== '0') {
                    this.context.iCABSAServiceCoverMaintenance6.CalculateAnnualValueChange();
                }
            }
        }
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
        this.context.iCABSAServiceCoverMaintenance7.APTChangedAccordingToQuantity();
    }

    public UnitValue_OnChange(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev && (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired')) && this.context.isControlChecked('VisitTriggered')) {
            this.context.iCABSAServiceCoverMaintenance6.CalculateAnnualValue();
        }
    }

    public ServiceAnnualValue_OnChange(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev && (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired')) && this.context.isControlChecked('VisitTriggered')) {
            this.context.iCABSAServiceCoverMaintenance6.CalculateUnitValue();
        }
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest(false);
    }

    public CalculateAnnualValue(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev && (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired')) && this.context.isControlChecked('VisitTriggered')) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('CalculateAnnualValue');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
            this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('ServiceQuantity', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('UnitValue', MntConst.eTypeCurrency);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('ServiceAnnualValue', data['CalcAnnualValue']);
            }, 'POST');
        }
    }

    public UnitValueChange_OnChange(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev && (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired')) && this.context.isControlChecked('VisitTriggered') === true) {
            this.context.iCABSAServiceCoverMaintenance6.CalculateAnnualValueChange();
        }
    }

    public CalculateUnitValue(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev && (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired'))) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('CalculateUnitValue');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
            this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('ServiceQuantity', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('ServiceAnnualValue', MntConst.eTypeCurrency);
            this.context.PostDataAddFromField('UnitValueChange', MntConst.eTypeCurrency);
            this.context.PostDataAddFromField('AnnualValueChange', MntConst.eTypeCurrency);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('UnitValue', data['UnitValue']);
            }, 'POST');
        }
    }

    public CalculateAnnualValueChange(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev && (this.context.isControlChecked('DisplayLevelInd') || this.context.isControlChecked('InvoiceUnitValueRequired'))) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('CalculateAnnualValueChange');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
            this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('ServiceQuantity', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('ServiceAnnualValue', MntConst.eTypeCurrency);
            this.context.PostDataAddFromField('UnitValueChange', MntConst.eTypeCurrency);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue('AnnualValueChange', this.context.riExchange.globalize.formatCurrencyToLocaleFormat(data['CalcAnnualValueChange']));
            }, 'POST');
        }
    }

    public IsVisitTriggered(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('GetInvoiceType');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
        this.context.PostDataAddFromField('InvoiceTypeNumber', MntConst.eTypeInteger);
        this.context.riMaintenance.ReturnDataAdd('VisitTriggered', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('VisitTriggered', data['VisitTriggered'] && (this.context.utils.lcase(data['VisitTriggered'].toString()) === 'yes'
                || this.context.utils.lcase(data['VisitTriggered'].toString()) === 'true'));
        }, 'POST');
    }

    public GetDefaultMultipleTaxRates(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = 'iCABSServiceCoverEntryRequests.p';
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('DefaultMultipleTaxRates');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.ReturnDataAdd('ConsolidateEqualTaxRates', MntConst.eTypeCheckBox);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('TaxCodeMaterials', data['TaxCodeMaterials']);
            this.context.setControlValue('TaxCodeLabour', data['TaxCodeLabour']);
            this.context.setControlValue('TaxCodeReplacement', data['TaxCodeReplacement']);
            this.context.setControlValue('InvoiceTextMaterials', data['InvoiceTextMaterials']);
            this.context.setControlValue('InvoiceTextLabour', data['InvoiceTextLabour']);
            this.context.setControlValue('InvoiceTextReplacement', data['InvoiceTextReplacement']);
            this.context.setControlValue('ConsolidateEqualTaxRates', this.context.LCase(data['ConsolidateEqualTaxRates']) === 'yes'
                || this.context.LCase(data['ConsolidateEqualTaxRates']) === 'true');
            this.context.iCABSAServiceCoverMaintenance3.TaxCodeMaterials_onChange();
            this.context.iCABSAServiceCoverMaintenance3.TaxCodeLabour_onChange();
            this.context.iCABSAServiceCoverMaintenance3.TaxCodeReplacement_onChange();
        }, 'POST');
    }

    public TermiteServiceCheck(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('TermiteServiceCheck');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeCode);
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('IsTermiteProduct', data['IsTermiteProduct']);
            if (data['TermiteWarningDesc']) {
                this.context.isTermiteContract = true;
                this.context.showAlert(data['TermiteWarningDesc']);
            }
            this.context.iCABSAServiceCoverMaintenance6.ShowHideTermiteRelatedFields();
        }, 'POST');
    }

    public ShowHideTermiteRelatedFields(): void {
        this.context.pageParams.uiDisplay.trTermiteWarrantyLine1 = false;
        this.context.pageParams.uiDisplay.trTermiteWarrantyLine2 = false;
        this.context.pageParams.uiDisplay.trTermiteWarrantyLine3 = false;
        this.context.pageParams.uiDisplay.trGraphNumber = false;
        if (this.context.getRawControlValue('IsTermiteProduct')) {
            if (this.context.getRawControlValue('IsTermiteProduct') === 'Y') {
                this.context.pageParams.uiDisplay.trTermiteWarrantyLine1 = true;
                this.context.pageParams.uiDisplay.trTermiteWarrantyLine2 = true;
                this.context.pageParams.uiDisplay.trTermiteWarrantyLine3 = true;
                this.context.pageParams.uiDisplay.trGraphNumber = true;
            }
        }
    }

    public GetBudgetInstalmentDetails(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddAction('6');
        this.context.riMaintenance.PostDataAddFunction('GetBudgetInstalmentDetails');
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.PostDataAddFromField('ProductCode', MntConst.eTypeCode);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            this.context.setControlValue('BudgetValidInstalments', data['BudgetValidInstalments']);
        });
    }

    public cmdVisitTolerances_onclick(): void {
        this.context.store.dispatch({
            type: ContractActionTypes.SAVE_DATA,
            payload: {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'CurrentServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
                'currentContractType': this.context.riExchange.getCurrentContractType()
            }
        });
        this.context.navigate('ServiceCoverVisitTolerance', InternalGridSearchServiceModuleRoutes.ICABSSVISITTOLERANCEGRID);
    }

    public cmdInfestationTolerances_onclick(): void {
        /*this.context.store.dispatch({
            type: ContractActionTypes.SAVE_SENT_FROM_PARENT,
            payload: {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'CurrentServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'currentContractType': this.context.riExchange.getCurrentContractType(),
                'parentMode': 'ServiceCoverInfestationTolerance'
            }
        });*/
        this.context.navigate('ServiceCoverInfestationTolerance', InternalGridSearchServiceModuleRoutes.ICABSSINFESTATIONTOLERANCEGRID,
            {
                'AccountNumber': this.context.getControlValue('AccountNumber'),
                'AccountName': this.context.getControlValue('AccountName'),
                'ContractNumber': this.context.getControlValue('ContractNumber'),
                'ContractName': this.context.getControlValue('ContractName'),
                'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                'PremiseName': this.context.getControlValue('PremiseName'),
                'ProductCode': this.context.getControlValue('ProductCode'),
                'ProductDesc': this.context.getControlValue('ProductDesc'),
                'ServiceCoverNumber': this.context.getControlValue('LinkedServiceCoverNumber'),
                'CurrentServiceCoverRowID': this.context.getControlValue('ServiceCoverROWID'),
                'CurrentContractTypeURLParameter': this.context.riExchange.getCurrentContractTypeUrlParam(),
                'parentMode': 'ServiceCoverInfestationTolerance'
            });
    }

    public cmdTeleSalesOrderLine_onclick(): void {
        //riExchange.Mode = 'ServiceCoverTeleSalesOrderLine';
        // window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSATeleSalesOrderLineGrid.htm';
        this.context.navigate('ServiceCoverTeleSalesOrderLine', InternalGridSearchApplicationModuleRoutes.ICABSATELESALESORDERLINEGRID);
    }

    public cmdServiceCoverDisplays_onclick(): void {
        this.context.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSSASERVICECOVERDISPLAYGRID, {
            'ServiceCover': this.context.getControlValue('ServiceCoverROWID')
        });
    }

    public cmdQualifications_onclick(): void {
        //riExchange.Mode = 'ServiceCoverQualifications';
        // window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSAServiceCoverQualificationGrid.htm';
        this.context.showAlert(MessageConstant.Message.PageNotCovered);
    }

    public riExchange_UnloadHTMLDocument(): void {
        this.context.setControlValue('WindowClosingName', this.context.getRawControlValue('ClosedWithChanges') === 'Y' ? 'AmendmentsMade' : 'true');
        this.context.riExchange.setParentHTMLValue('WindowClosingName', this.context.getRawControlValue('WindowClosingName'));
        this.context.riExchange.setParentHTMLValue('ClosedWithChanges', this.context.getRawControlValue('ClosedWithChanges'));
        //Call riExchange.UpdateParentHTMLDocument();
    }

}
