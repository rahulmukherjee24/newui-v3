import { InternalGridSearchSalesModuleRoutes, InternalMaintenanceApplicationModuleRoutes, InternalGridSearchApplicationModuleRoutes, InternalGridSearchServiceModuleRoutes } from './../../../base/PageRoutes';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ServiceCoverMaintenanceComponent } from './iCABSAServiceCoverMaintenance.component';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ServiceCoverSearchComponent } from './../../search/iCABSAServiceCoverSearch';

export class ServiceCoverMaintenance7 {

    private context: ServiceCoverMaintenanceComponent;

    constructor(private parent: ServiceCoverMaintenanceComponent) {
        this.context = parent;
    }

    public CreateTabs(): void {
        this.context.uiDisplay.tab.tab1.visible = true;

        this.context.uiDisplay.tab.tab2.visible = this.context.isControlChecked('CompositeProductInd');
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            this.context.uiDisplay.tab.tab3.visible = this.context.riExchange.URLParameterContains('PendingReduction');
        }
        this.context.uiDisplay.tab.tab4.visible = true;
        this.context.uiDisplay.tab.tab5.visible = true;

        this.context.uiDisplay.tab.tab6.visible = !(this.context.getRawControlValue('FieldShowList') &&
            this.context.getRawControlValue('FieldShowList').indexOf('VisitCycleInWeeks') === 0 ||
            this.context.riExchange.getCurrentContractType() === 'J' || !this.context.pageParams.blnUseVisitCycleValues);
        //APH Removed property care && user write access calls, as already exist in Window_OnLoad
        this.context.uiDisplay.tab.tab7.visible = (this.context.pageParams.boolPropertyCareInd === 'Y' && this.context.pageParams.boolUserWriteAccess === 'yes');

        this.context.uiDisplay.tab.tab8.visible = (this.context.isControlChecked('SeasonalServiceInd')
            && this.context.riExchange.getCurrentContractType() === 'C');

        this.context.uiDisplay.tab.tab9.visible = true;
        this.context.uiDisplay.tab.tab10.visible = true;

        this.context.uiDisplay.tab.tab11.visible = (this.context.pageParams.vbEnableEntitlement &&
            this.context.riExchange.getCurrentContractType() === 'C' &&
            this.context.isControlChecked('EntitlementRequiredInd'));

        if (this.context.pageParams.vEnableEntitlementForWashroom) { //ITA-196 [if we have 4340 true then hide it else show entitlement tab]
            this.context.uiDisplay.tab.tab11.visible = false;
            this.context.controls.map((item) => { // UAT547- bug fix for currency
                if (item.name.toLowerCase() === 'entitlementpriceperunit') {
                    item.type = MntConst.eTypeTextFree;
                    return;
                }
            });
        }

        // Only add this tab if( Trial Period indicator is ticked, && this is a contract
        this.context.uiDisplay.tab.tab12.visible = ((this.context.isControlChecked('TrialPeriodInd') ||
            this.context.isControlChecked('ContractTrialPeriodInd')) &&
            this.context.riExchange.getCurrentContractType() === 'C');
        this.context.uiDisplay.tab.tab13.visible = true;
        // MDP - 07/11/08 #35611: UK HY - Survey Details (SIP)
        this.context.uiDisplay.tab.tab14.visible = this.context.pageParams.vEnableSurveyDetail;

        this.context.uiDisplay.tab.tab15.visible = (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd'));
        /* 09/17/20 - IK - #IUI_25456 Stops validation of the required fields.
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'ServiceCommenceDate', false);
            this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'ServiceVisitFrequency', false);
        }*/
    }

    public riMaintenance_BeforeSave(): void {
        this.context.pageParams.verror = false;
        this.context.setControlValue('CompositeProductCode', this.context.getRawControlValue('SelectCompositeProductCode'));

        if (!this.context.pageParams.uiDisplay.trServiceAnnualValue
            && this.context.riExchange.URLParameterContains('PendingReduction')) {
            this.context.showAlert(MessageConstant.PageSpecificMessage.scvCoversReducedvalReqd);
            this.context.pageParams.verror = true;
            this.context.saveClicked = false;
            return;
        }

        if (!this.context.getRawControlValue('selTaxCode') &&
            !this.context.getRawControlValue('TaxCode')) {
            this.context.showAlert(MessageConstant.PageSpecificMessage.reqdTaxCode);
            this.context.pageParams.verror = true;
            this.context.saveClicked = false;
            return;
        }

        if (this.context.pageParams.blnUseVisitCycleValues) {
            if (this.context.getRawControlValue('VisitCycleInWeeks') &&
                this.context.getRawControlValue('VisitCycleInWeeks') === '0') {
                this.context.setControlValue('VisitCycleInWeeks', '');
                this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'VisitCycleInWeeks');
                this.context.saveClicked = false;
                return;
            } else if (this.context.getRawControlValue('VisitsPerCycle') &&
                this.context.getRawControlValue('VisitsPerCycle') === '0') {
                this.context.setControlValue('VisitsPerCycle', '');
                this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'VisitsPerCycle');
                this.context.saveClicked = false;
                return;
            }
            /**
             * IUI-20701
             * Code Commented Out Since The Logic Needs To Be Refurbished
             */
            /* if ((this.context.getControlValueAsString('VFPNumberOfWeeks') !== this.context.getControlValueAsString('VisitCycleInWeeks')) &&
                this.context.utils.len(this.context.getRawControlValue('VisitCycleInWeeksOverrideNote')) < 10) {
                this.context.showAlert(MessageConstant.PageSpecificMessage.PatternOutSync);
                this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'VisitCycleInWeeksOverrideNote');
                this.context.saveClicked = false;
                return;
            } */
        }

        if (this.context.getRawControlValue('HardSlotVisitTime') &&
            this.context.riExchange.riInputElement.isError(this.context.uiForm, 'HardSlotVisitTime')) {
            let iHour = this.context.utils.mid(this.context.getRawControlValue('HardSlotVisitTime'), 1,
                this.context.utils.len(this.context.getRawControlValue('HardSlotVisitTime')) - 3);
            if (parseInt(iHour, 10) > 23) {
                this.context.showAlert(MessageConstant.PageSpecificMessage.hardSlotAcpt);
                this.context.riExchange.riInputElement.markAsError(this.context.uiForm, 'HardSlotVisitTime');
                this.context.pageParams.verror = true;
                this.context.saveClicked = false;
                return;
            }
        }

        this.context.setControlValue('CallLogID', this.context.riExchange.getParentHTMLValue('CurrentCallLogID'));
        this.context.setControlValue('ClosedWithChanges', 'Y');
        if (this.context.pageParams.vEnableInsEmpCodeValidation) {
            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
                if (this.context.InStr('FieldShowList', 'InstallationValue') !== -1) {
                    if (this.context.getRawControlValue('OutstandingInstallations') === '0') {
                        if (!this.context.getRawControlValue('InstallationEmployeeCode')) {
                            this.context.showAlert(MessageConstant.PageSpecificMessage.reqdInstEmp);
                            this.context.pageParams.uiDisplay.trInstallationEmployee = true;
                            this.context.renderTab(5);
                            //InstallationEmployeeCode.focus();
                            //this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'InstallationEmployeeCode', true);
                            this.context.pageParams.verror = true;
                            this.context.saveClicked = false;
                            return;
                        }
                    }
                }
            }
        }

        if (!this.context.pageParams.verror) {
            if (this.context.pageParams.blnServiceVisitAnnivDateChange
                && this.context.getRawControlValue('LastChangeEffectDate')) {
                if (this.context.utils.year(this.context.getRawControlValue('LastChangeEffectDate'))
                    !== this.context.utils.year(new Date())) {
                    this.context.showDialog('Effective Date Is Not In Current Year - Do You Wish To Continue ',
                        this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd, this.context.iCABSAServiceCoverMaintenance7.handleSaveDecline);
                } else {
                    this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd();
                }
            } else {
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd();
            }
        }
    }

    public riMaintenance_BeforeSaveContd(): void {
        this.context.setControlValue('PatternWarningString', '');
        if (this.context.getRawControlValue('ServiceSpecialInstructions')) {
            this.context.pageParams.blnServiceSpecialInstructions = true;
        }
        this.context.pageParams.strFunctions = 'CheckLength,';
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd && this.context.riExchange.getCurrentContractType() === 'C') {
            this.context.pageParams.strFunctions = this.context.pageParams.strFunctions + 'CheckVisitPattern,';
            this.context.riMaintenance.CBORequestAdd('ContractNumber');
            this.context.riMaintenance.CBORequestAdd('PremiseNumber');
        }
        this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=' + this.context.utils.Left(this.context.pageParams.strFunctions, this.context.utils.len(this.context.pageParams.strFunctions) - 1);
        this.context.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
        if (this.context.getRawControlValue('ServiceSpecialInstructions')) {
            this.context.riMaintenance.CBORequestAdd('ServiceSpecialInstructions');
        }
        this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.CBORequestExecute(this.context, function (data: any): any {
            if (data) {
                if (data.hasError) {
                    this.context.showAlert(data.errorMessage, 0, data.fullError);
                } else {
                    this.context.riMaintenance.renderResponseForCtrl(this.context, data);
                    this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveFirstCallback();
                }
            }
        });

    }

    public handlesaveClick(): void {
        this.context.saveClicked = true;
        this.context.setRequiredStatus('ServiceCommenceDate', true);
        this.context.riExchange.updateCtrl(this.context.controls, 'ServiceCommenceDate', 'required', true);
        this.context.setRequiredStatus('LastChangeEffectDate', true);
        this.context.riExchange.updateCtrl(this.context.controls, 'LastChangeEffectDate', 'required', true);

        if ((this.context.reasonCodeDropDown || this.context.detailCodeDropDown) && this.context.getControlValue('AnnualValueChange') < 0) {
            if (!this.context.getRawControlValue('LostBusinessCode')
                || !this.context.getRawControlValue('LostBusinessDetailCode')) {
                if (!this.context.getRawControlValue('LostBusinessCode')) {
                    this.context.setRequiredStatus('LostBusinessCode', true);
                    this.context.uiForm.controls['LostBusinessCode'].markAsTouched();
                    if (this.context.reasonCodeDropDown) {
                        this.context.reasonCodeDropDown.isTriggerValidate = true;
                    }
                }
                if (!this.context.getRawControlValue('LostBusinessDetailCode')) {
                    this.context.setRequiredStatus('LostBusinessDetailCode', true);
                    if (this.context.detailCodeDropDown) {
                        this.context.detailCodeDropDown.isTriggerValidate = true;
                    }
                }
                this.context.pageParams.verror = true;
                this.context.saveClicked = false;
                this.context.renderTab(1, true);
                setTimeout(() => {
                    this.context.utils.makeTabsRedById('grdGeneral');
                }, 100);
            }
        } else {
            this.context.setRequiredStatus('LostBusinessCode', false);
            this.context.setRequiredStatus('LostBusinessDetailCode', false);
        }

        if (this.context.riExchange.validateForm(this.context.uiForm)) {
            /*this.context.renderTab(1);
            this.context.showDialog(MessageConstant.Message.ConfirmRecord,
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSave);*/
            this.context.utils.makeTabsNormal();
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSave();
        } else {
            if (!status) {
                for (let control in this.context.uiForm.controls) {
                    if (this.context.uiForm.controls[control].invalid) {
                        if (this.context.pageParams['dt' + control]) {
                            this.context.pageParams['dt' + control].validate = true;
                        }
                    }
                }
            }
            this.context.utils.highlightTabsByIds();
            this.context.renderTab(1);
            this.context.saveClicked = false;
        }
    }

    public handleSaveDecline(): void {
        this.context.saveClicked = false;
    }

    /**
     * Before Save Callbacks
     */

    public riMaintenance_BeforeSaveFirstCallback(): void {
        if (this.context.getRawControlValue('PatternWarningString')) {
            let msgTextR: string = this.context.getControlValue('PatternWarningString');
            // let msg: string = 'Visit Patterns Exist : ' +
            //     msgTextR.replace('/*NL*/', String.fromCharCode(10) + String.fromCharCode(13)) +
            //     String.fromCharCode(10) + String.fromCharCode(13) +
            //     'Do You Wish To Continue ?';
            let msgArray = [];
            msgArray.push('Visit Patterns Exist : ');
            msgArray = msgArray.concat(msgTextR.split('/*NL*/'));
            msgArray.push('Do You Wish To Continue ?');
            this.context.showDialog(msgArray,
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd1, this.context.iCABSAServiceCoverMaintenance7.handleSaveDecline);
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd1();
        }
    }

    public riMaintenance_BeforeSaveContd1(): void {
        if (this.context.pageParams.vSCValidateInvoiceTypeOnNewSC && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
            this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=ValidateInvoiceType';
            this.context.riMaintenance.CBORequestAdd('ContractNumber');
            this.context.riMaintenance.CBORequestAdd('InvoiceTypeNumber');
            this.context.riMaintenance.CBORequestExecute(this.context, function (data: any): any {
                if (data) {
                    this.context.riMaintenance.renderResponseForCtrl(this.context, data);
                }
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveSecondCallback();
            });
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveSecondCallback();
        }
    }

    public riMaintenance_BeforeSaveSecondCallback(): void {
        if (this.context.getRawControlValue('ServiceCoverInvTypeString')) {
            let msgTextR: string = this.context.getControlValue('ServiceCoverInvTypeString');
            // let msg: string = 'Visit Patterns Exist : ' +
            //     msgTextR.replace('/*NL*/', String.fromCharCode(10) + String.fromCharCode(13)) +
            //     String.fromCharCode(10) + String.fromCharCode(13) +
            //     'Do You Wish To Continue ?';
            let msgArray = [];
            msgArray.push('Other Invoice Types Exist On Service Covers For This Contract : ');
            msgArray = msgArray.concat(msgTextR.split('/*NL*/'));
            msgArray.push('Do You Wish To Continue ?');
            this.context.showDialog(msgArray,
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd2, this.context.iCABSAServiceCoverMaintenance7.handleSaveDecline);
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd2();
        }
    }

    public riMaintenance_BeforeSaveContd2(): void {
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
            this.context.getRawControlValue('BranchServiceAreaCode')
            !== this.context.getRawControlValue('oriBranchServiceAreaCode')) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.PostDataAddFunction('GetTechRetentionInd');
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddParamFromField('ServiceCoverRowID', 'CurrentServiceCoverRowID', MntConst.eTypeText);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                if (data['MessageDisplay'] !== '') {
                    this.context.setControlValue('MessageDisplay', data['MessageDisplay']);
                    this.context.showDialog(data['MessageDisplay'],
                        this.context.iCABSAServiceCoverMaintenance7.continueBeforeSave, this.context.iCABSAServiceCoverMaintenance7.handleSaveDecline);
                } else {
                    this.context.iCABSAServiceCoverMaintenance7.continueBeforeSave();
                }
            }, 'POST');
        } else {
            this.context.iCABSAServiceCoverMaintenance7.continueBeforeSave();
        }
    }

    public continueBeforeSave(): void {
        this.context.iCABSAServiceCoverMaintenance5.GetServiceAreaRequired();
        this.context.iCABSAServiceCoverMaintenance5.ValidateServiceArea();
        this.context.iCABSAServiceCoverMaintenance5.ValidateVisitPattern();

        if (this.context.pageParams.uiDisplay.trServiceVisitFrequency &&
            this.context.pageParams.uiDisplay.trServiceAnnualValue &&
            this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
            this.context.riExchange.getCurrentContractType() === 'J') {
            if ((this.context.utils.CInt(this.context.pageParams.SavedServiceVisitFrequency) !== this.context.CInt('ServiceVisitFrequency'))
                || (this.context.utils.cCur(this.context.pageParams.SavedServiceAnnualValue) !== this.context.cCur('ServiceAnnualValue'))) {
                this.context.riMaintenance.clear();
                this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
                this.context.riMaintenance.PostDataAddFunction('JobServiceVisitCheck');
                this.context.riMaintenance.PostDataAddBusiness();
                this.context.PostDataAddParamFromField('ServiceCoverRowID', 'CurrentServiceCoverRowID', MntConst.eTypeText);
                this.context.PostDataAddFromField('InvoiceTypeNumber', MntConst.eTypeInteger);
                this.context.riMaintenance.Execute(this.context, function (data: any): any {
                    if (data['MessageDisplay'] !== '') {
                        this.context.setControlValue('MessageDisplay', data['MessageDisplay']);
                        this.context.showDialog(data['MessageDisplay'],
                            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd3, this.context.iCABSAServiceCoverMaintenance7.handleSaveDecline);
                    } else {
                        this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd3();
                    }
                }, 'POST');
            } else {
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd3();
            }
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_BeforeSaveContd3();
        }
    }

    public riMaintenance_BeforeSaveContd3(): void {
        this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strEntryProcedure;
        if (this.context.getRawControlValue('ErrorMessage')) {
            this.context.showAlert(this.context.getRawControlValue('ErrorMessage'));
            this.context.setControlValue('ErrorMessage', '');
        }
        if (this.context.parentMode === 'ContactAdd') {
            this.context.setControlValue('LostBusinessRequestNumber', this.context.riExchange.getParentHTMLValue('LostBusinessRequestNumber'));
        }
        this.context.setControlValue('PendingReduction', this.context.riExchange.URLParameterContains('PendingReduction') ? 'PendingReduction' : '');
        this.context.setControlValue('PendingDeletion', this.context.riExchange.URLParameterContains('PendingDeletion') ? 'PendingDeletion' : '');
        this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'ContractTypeCode=' + this.context.riExchange.getCurrentContractType();
        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
        if (this.context.getRawControlValue('selServiceBasis') === 'H') {
            if (!this.context.getRawControlValue('HardSlotTemplateNumber')) {
                this.context.showAlert(MessageConstant.PageSpecificMessage.hardSlotTempNoAssigned);
                return;
            } else if (this.context.getRawControlValue('HardSlotType') === 'S'
                && !this.context.getRawControlValue('HardSlotVisitTime')) {
                this.context.showAlert(MessageConstant.PageSpecificMessage.hardSlotVistTimeReqd);
                return;
            } else {
                this.context.riMaintenance.clear();
                this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
                this.context.riMaintenance.PostDataAddFunction('ValidateHardSlot');
                this.context.riMaintenance.PostDataAddBusiness();
                this.context.PostDataAddFromField('LastChangeEffectDate', MntConst.eTypeDate);
                this.context.PostDataAddFromField('ServiceVisitAnnivDate', MntConst.eTypeDate);
                this.context.PostDataAddFromField('ServiceCommenceDate', MntConst.eTypeDate);
                this.context.PostDataAddFromField('ServiceVisitFrequency', MntConst.eTypeInteger);
                this.context.PostDataAddFromField('HardSlotTemplateNumber', MntConst.eTypeInteger);
                this.context.PostDataAddFromField('ServiceBranchNumber', MntConst.eTypeInteger);
                this.context.PostDataAddFromField('HardSlotVisitTime', MntConst.eTypeTime);
                for (let idx = 1; idx < 15; idx++) {
                    let suffix: string = idx < 10 ? '0' + idx : idx.toString();
                    this.context.PostDataAddFromField('WindowStart' + suffix, MntConst.eTypeTime);
                    this.context.PostDataAddFromField('WindowEnd' + suffix, MntConst.eTypeTime);
                }
                this.context.PostDataAddFromField('HardSlotType', MntConst.eTypeText);
                this.context.PostDataAddFromField('ContractNumber', MntConst.eTypeText);
                this.context.PostDataAddFromField('PremiseNumber', MntConst.eTypeInteger);
                this.context.PostDataAddParamFromField('ServiceCoverRowID', 'CurrentServiceCoverRowID', MntConst.eTypeText);
                this.context.PostDataAddFromField('ActualPlannedTime', MntConst.eTypeTime);
                this.context.PostDataAddFromField('StandardTreatmentTime', MntConst.eTypeTime);
                this.context.PostDataAddFromField('BranchServiceAreaCode', MntConst.eTypeCode);
                this.context.riMaintenance.Execute(this.context, function (data: any): any {
                    if (data['MessageDisplay'] !== '') {
                        this.context.setControlValue('MessageDisplay', data['MessageDisplay']);
                        this.context.showDialog(data['MessageDisplay'],
                            this.context.iCABSAServiceCoverMaintenance7.riExchange_CBORequest, this.context.iCABSAServiceCoverMaintenance7.handleSaveDecline);
                    }
                    this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
                });
            }
        } else {
            this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
        }
    }

    public saveRecord(): void {
        let action: number = 2;
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            action = 1;
        }
        let fieldsArr = this.context.riExchange.getAllCtrl(this.context.controls);
        let ignoreSubmitEntitlements: boolean = !this.context.uiDisplay.tab.tab11.visible;
        if (this.context.pageParams.vEnableEntitlementForWashroom) { ignoreSubmitEntitlements = false; }

        this.context.riMaintenance.clear();
        let isPendingReduction: boolean = this.context.riExchange.URLParameterContains('PendingReduction');
        this.context.setControlValue('PendingReduction', isPendingReduction ? 'PendingReduction' : '');
        this.context.riExchange.updateCtrl(this.context.controls, 'riGridHandle', 'ignoreSubmit', !isPendingReduction);

        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementAnnivDate', 'ignoreSubmit', ignoreSubmitEntitlements); //ITA-663 [UAT bug]
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementAnnualQuantity', 'ignoreSubmit', ignoreSubmitEntitlements);
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementNextAnnualQuantity', 'ignoreSubmit', ignoreSubmitEntitlements);
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementOrderedQuantity', 'ignoreSubmit', ignoreSubmitEntitlements);
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementPricePerUnit', 'ignoreSubmit', ignoreSubmitEntitlements);
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementRequiredInd', 'ignoreSubmit', ignoreSubmitEntitlements); //ITA-663 [UAT bug]
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementServiceQuantity', 'ignoreSubmit', ignoreSubmitEntitlements);
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementYTDQuantity', 'ignoreSubmit', ignoreSubmitEntitlements);
        this.context.riExchange.updateCtrl(this.context.controls, 'EntitlementInvoiceTypeCode', 'ignoreSubmit', this.context.getControlValue('EntitlementInvoiceTypeCode') === '');
        this.context.riExchange.updateCtrl(this.context.controls, 'MultipleTaxRates', 'ignoreSubmit', !this.context.pageParams.vbEnableMultipleTaxRates);
        this.context.riExchange.updateCtrl(this.context.controls, 'ServiceCoverNumber', 'ignoreSubmit', 'false');
        this.context.riMaintenance.PostDataAdd('ServiceCoverNumber', this.context.getControlValue('ServiceCoverNumber'), this.context.riMaintenance.getControlType(this.context.controls, 'ServiceCoverNumber', 'type'));

        if (this.context.pageParams.vDisplayVisitSharing) {
            this.context.setControlValue('EnableVisitSharing',
                this.context.pageParams.ShareEmplTot !== this.context.getControlValue('ShareEmplTot') ||
                this.context.pageParams.ShareStatus !== this.context.getControlValue('ShareStatus')
            );
        }

        for (let i = 0; i < fieldsArr.length; i++) {
            let id = fieldsArr[i];
            let dataType = this.context.riMaintenance.getControlType(this.context.controls, id, 'type');
            let ignore = this.context.riMaintenance.getControlType(this.context.controls, id, 'ignoreSubmit');
            if (!ignore) {
                let value = this.context.getRawControlValue(id);
                this.context.riMaintenance.PostDataAdd(id, value, dataType);
            }
        }
        if (this.context.fieldHasValue('ClosedCalendarTemplateNumber')) {
            this.context.riMaintenance.PostDataAdd('TemplateName', this.context.getControlValue('ClosedTemplateName'), MntConst.eTypeText);
        }

        if (this.context.riExchange.getCurrentContractType() === 'J') {
            this.context.setControlValue('LastChangeEffectDate', this.context.getControlValue('ServiceCommenceDate'));
        }

        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            if (data.hasError) {
                if (data.errorMessage && data.errorMessage.trim() !== '') {
                    this.logger.log('Post data Error', data);
                    // this.context.errorModal.show(data, true);
                    this.context.messageType = 'error';
                    this.context.alertMessage = {
                        msg: 'ERROR: ' + data.errorMessage + ' ' + data.fullError,
                        timestamp: (new Date()).getMilliseconds()
                    };
                }
            } else {
                this.context.riExchange.updateCtrl(this.context.controls, 'riGridHandle', 'ignoreSubmit', true);
                this.context.pageParams.riGridHandlevalue = '';
                this.context.setControlValue('riGridHandle', '');
                this.context.riMaintenance.renderResponseForCtrl(this.context, data, true);
                this.context.setControlValue('ServiceCoverROWID', this.context.getControlValue('CurrentServiceCoverRowID'));
                this.context.pageParams.ServiceCoverRowID = this.context.getControlValue('CurrentServiceCoverRowID');
                if (this.getControlValue('CurrentServiceCoverRowID')) {
                    this.iCABSAServiceCoverMaintenance1.init();
                }
                this.context.iCABSAServiceCoverMaintenance7.postSave();
            }
        }, 'POST', action);
    }

    public postSave(): void {
        this.context.pageParams.initialForm = this.context.createControlObjectFromForm();
        this.context.storePageParams();
        this.context.formIsDirty = false;
        this.context.serviceCoverSearchParams.parentMode = 'Search';
        this.context.serviceCoverSearchComponent = ServiceCoverSearchComponent;
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd();
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave();
        }
    }

    //******************
    //* AFTER SAVE ADD *
    //******************
    public riMaintenance_AfterSaveAdd(): void {
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelectCompositeProductcode');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        if (this.context.pageParams.vbDefaultTaxCodeProductExpenseReq && this.context.pageParams.vbDefaultTaxCodeProductExpenseLog) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        }
        this.context.setControlValue('ComponentGridCacheTime', this.context.utils.Time()); // Unique Key used for Component Grid Rebuilding;
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdComponentSelAll');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdComponentDesAll');
        if (this.context.parentMode === 'Premise-Add') {
            this.context.pageParams.ServiceCoverAdded = true;
        }
        //If user has Full Access || logged in neg || service branch ) { show value flds;
        this.context.pageParams.blnAccess = (this.context.pageParams.FullAccess === 'Full'
            || this.context.utils.getBranchCode().toString() === this.context.getControlValueAsString('ServiceBranchNumber')
            || this.context.utils.getBranchCode().toString() === this.context.getControlValueAsString('NegBranchNumber'));
        if (this.context.getRawControlValue('WasteTransferTypeCode')) {
            this.context.iCABSAServiceCoverMaintenance1.ServiceCoverWasteReq();
        } else {
            this.context.saved = true;
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd6();
        }
    }

    public riMaintenance_AfterSaveAdd1(): void {
        if (this.context.isControlChecked('DetailRequired')) {
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_3;
            this.context.navigate('ServiceCover', this.context.pageParams.vbLocationsSingleEntry ? InternalMaintenanceApplicationModuleRoutes.ICABSASERVICECOVERDETAILGROUPMAINTENANCE : InternalMaintenanceApplicationModuleRoutes.ICABSASERVICECOVERDETAILMAINTENANCE);
            // Should prevent location entryif( freq is not req;
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd2();
        }
    }

    public riMaintenance_AfterSaveAdd2(): void {
        if (this.context.pageParams.vbEnableLocations
            && this.context.isControlChecked('LocationsEnabled')
            && this.context.InStr('FieldShowList', 'ServiceQuantity') !== -1) {
            //Store the RowID of the ServiceCover record to an attribute, so the Location Grid can know;
            //which record has just been updtd;
            this.context.attributes.ServiceCoverRowID = this.context.getRawControlValue('CurrentServiceCoverRowID');
            this.context.attributes.OutstandingInstallations = this.context.getRawControlValue('OutstandingInstallations');
            this.context.attributes.QuantityChange = this.context.getRawControlValue('QuantityChange');
            this.context.attributes.DefaultEffectiveDate = this.context.getRawControlValue('ServiceVisitAnnivDate');
            this.context.attributes.DefaultEffectiveDateProduct = this.context.getRawControlValue('ProductCode');
            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                this.context.isControlChecked('DisplayLevelInd')) {
                this.context.navigate('ServiceCover-Increase', InternalGridSearchSalesModuleRoutes.ICABSAEMPTYPREMISELOCATIONSEARCHGRID);
                this.context.pageParams.saveReturnCallback = true;
                this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_3;
                return;
            } else {
                this.context.navigate('ServiceCover-Increase', 'application/premiseLocationAllocation');
                this.context.pageParams.saveReturnCallback = true;
                this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_3;
                return;
            }
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd3();
        }
    }

    public riMaintenance_AfterSaveAdd3(): void {
        if (this.context.isControlChecked('chkStockOrder')) {
            this.context.setControlValue('GenContractNumber', '');
            ////WindowPath = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSAStockOrderFieldsMaintenance.htm<maxwidthproduct>';
            ////window.location = WindowPath;
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd3A();
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd4();
        }
    }

    public riMaintenance_AfterSaveAdd3A(): void {
        if (this.context.getRawControlValue('GenContractNumber')) {
            this.context.navigate('GeneratedStockOrder', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_1);
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_4;
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd4();
        }
    }

    public riMaintenance_AfterSaveAdd4(): void {
        if (this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            this.context.attributes.Mode = 'New';
            this.context.riMaintenance.CurrentMode = MntConst.eModeUpdate;
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_5;
            ////riExchange.Mode = 'ServiceCover';
            ////window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSAServiceVisitPlanningGrid.htm<maxwidth>';
            this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITPLANNINGGRID,
                {
                    'ContractNumber': this.context.getControlValue('ContractNumber'),
                    'ContractName': this.context.getControlValue('ContractName'),
                    'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                    'PremiseName': this.context.getControlValue('PremiseName'),
                    'ProductCode': this.context.getControlValue('ProductCode'),
                    'ProductDesc': this.context.getControlValue('ProductDesc'),
                    'CalendarUpdateAllowed': this.context.getControlValue('CalendarUpdateAllowed'),
                    'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
                    'ServiceAnnualValue': this.context.getControlValue('ServiceAnnualValue'),
                    'LastChangeEffectDate': this.context.getControlValue('LastChangeEffectDate'),
                    'ServiceCover': this.context.getControlValue('ServiceCoverROWID')
                });
            //this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd5();
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSaveAdd5();
        }
    }


    public riMaintenance_AfterSaveAdd5(): void {
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            this.context.navigate('ServiceCoverAdd', InternalGridSearchApplicationModuleRoutes.ICABSSASERVICECOVERDISPLAYGRID, {
                'ServiceCover': this.context.getControlValue('ServiceCoverROWID')
            });
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.reloadOnReturn = true;
            this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_ADD_6;
        } else if (!this.context.isReturning()) {
            this.context.showSuccessMessageDialog();
        }
    }

    public riMaintenance_AfterSaveAdd6(): void {
        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'InvTypeSel');
        this.context.riMaintenance.CurrentMode = MntConst.eModeUpdate;
        this.context.disableControl('ContractNumber', true);
        this.context.disableControl('PremiseNumber', true);
        this.context.disableControl('ProductCode', true);
        this.context.disableControl('menu', false);
        this.context.initMode(this.context.Mode.UPDATE, true);
        this.context.initialLoad = true;
        /* BUGFIX-24500 As ProductCode remains disabled after save - IK 05/10/2020
        this.context.iCABSAServiceCoverMaintenance1.ProductCode_OnChange();
        */
        if (this.context.pageParams.vbEnableProductServiceType) {
            this.context.iCABSAServiceCoverMaintenance1.GetProductServiceType();
        }
        this.context.iCABSAServiceCoverMaintenance7.CheckServiceNotifyInd();
        if (this.context.parentMode === 'Premise-Add'
            || this.context.parentMode === 'SearchAdd'
            || this.context.parentMode === 'ContactAdd') {
            this.context.afterSaveNavigate = true;
        }
        this.context.iCABSAServiceCoverMaintenance1.init();
    }

    public riMaintenance_AfterSave(): void {
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelectCompositeProductcode');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        if (this.context.pageParams.vbDefaultTaxCodeProductExpenseReq && this.context.pageParams.vbDefaultTaxCodeProductExpenseLog) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        }
        this.context.setControlValue('ComponentGridCacheTime', this.context.utils.Time()); // Unique Key So I Can Control When The Component Grid Needs Rebuilding e.g. change of product;
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdComponentSelAll');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdComponentDesAll');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdHardSlotCalendar');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdDiaryView');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelServiceBasis');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selHardSlotType');
        this.context.pageParams.vbUpdateServiceVisit = false;
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelSubjectToUplift');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelUpliftVisitPosition');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelAutoPattern');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelAutoAllocation');
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            this.context.setControlValue('origTotalValue', '');
            this.context.setControlValue('NewTotalValue', '');
            this.context.setControlValue('riGridHandle', '');
        }
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
            this.context.getRawControlValue('WasteTransferTypeCode') !== this.context.getRawControlValue('SavedWasteTransferType')
            && this.context.getRawControlValue('WasteTransferTypeCode')) {
            this.context.iCABSAServiceCoverMaintenance1.ServiceCoverWasteReq();
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave1();
        }
    }

    public riMaintenance_AfterSave1(): void {
        if (this.context.pageParams.vbEnableLocations
            && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate
            && this.context.isControlChecked('LocationsEnabled')
            && this.context.InStr('FieldShowList', 'ServiceQuantity') !== -1
            && (this.context.pageParams.blnQuantityChange || this.context.parentMode === 'ServiceCoverGrid')) {
            //Store the RowID of the ServiceCover record to an attribute, so that the Location Grid can know which record has just been updated;
            this.context.attributes.ServiceCoverRowID = this.context.getRawControlValue('CurrentServiceCoverRowID');
            this.context.attributes.OutstandingInstallations = this.context.getRawControlValue('OutstandingInstallations');
            this.context.attributes.QuantityChange = this.context.getRawControlValue('QuantityChange');
            this.context.attributes.DefaultEffectiveDate = this.context.getRawControlValue('LastChangeEffectDate');
            this.context.attributes.DefaultEffectiveDateProduct = this.context.getRawControlValue('ProductCode');
            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                this.context.isControlChecked('DisplayLevelInd')) {
                if (!this.context.riExchange.URLParameterContains('PendingReduction')) { //Do not Display Location Information on the Reducing ServiceCover screenif( Display Level is enabled;
                    this.context.navigate('ServiceCover-Increase', InternalGridSearchSalesModuleRoutes.ICABSAEMPTYPREMISELOCATIONSEARCHGRID);
                    this.context.pageParams.saveReturnCallback = true;
                    this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_UPDATE_2;
                    return;
                } else {
                    this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave2();
                }
            } else {
                this.context.navigate('ServiceCover-Increase', 'application/premiseLocationAllocation');
                this.context.pageParams.saveReturnCallback = true;
                this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_UPDATE_2;
            }
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave2();
        }
    }

    public riMaintenance_AfterSave2(): void {
        if (this.context.isControlChecked('chkStockOrder')
            && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.context.setControlValue('GenContractNumber', '');
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave3();
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave4();
        }
    }

    public riMaintenance_AfterSave3(): void {
        if (this.context.getRawControlValue('GenContractNumber')) {
            this.context.navigate('GeneratedStockOrder', InternalGridSearchServiceModuleRoutes.ICABSAPRODUCTSALESSCENTRYGRID.URL_1);
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_UPDATE_4;
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave4();
        }
    }

    public riMaintenance_AfterSave4(): void {
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate && this.context.isControlChecked('RequiresManualVisitPlanningInd')) {
            this.context.pageParams.vbAnnualTimeChange = this.context.getRawControlValue('AnnualTimeChange').replace(this.context.pageParams.vbTimeSeparator, '');
            this.context.setControlValue('AnnualTimeChange', '');
            if (this.context.IsNumeric('ServiceVisitFrequency')) {
                if (this.context.CInt('ServiceVisitFrequency').toString() !== this.context.pageParams.SavedServiceVisitFrequency.toString()) {
                    this.context.pageParams.vbUpdateServiceVisit = true;
                }
            }
            if (!this.context.pageParams.vbUpdateServiceVisit && this.context.IsNumeric(this.context.pageParams.vbAnnualTimeChange)) {
                if (this.context.pageParams.vbAnnualTimeChange !== 0) {
                    this.context.pageParams.vbUpdateServiceVisit = true;
                }
            }
            if (this.context.pageParams.blnServiceVisitAnnivDateChange && !this.context.pageParams.vbUpdateServiceVisit) {
                this.context.pageParams.vbUpdateServiceVisit = true;
            }
            if (!this.context.pageParams.vbUpdateServiceVisit
                && this.context.getRawControlValue('InitialTreatmentTime') !== this.context.pageParams.SavedInitialTreatmentTime) {
                this.context.pageParams.vbUpdateServiceVisit = true;
            }
            //Only open Service Visit Calendarif( freq has changed || Annual Time has changed;
            if (this.context.pageParams.vbUpdateServiceVisit) {
                this.context.attributes.Mode = 'Update';
                ////riExchange.Mode = 'ServiceCover';
                ////window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSAServiceVisitPlanningGrid.htm<maxwidth>';
                this.context.navigate('ServiceCover', InternalGridSearchServiceModuleRoutes.ICABSASERVICEVISITPLANNINGGRID,
                    {
                        'ContractNumber': this.context.getControlValue('ContractNumber'),
                        'ContractName': this.context.getControlValue('ContractName'),
                        'PremiseNumber': this.context.getControlValue('PremiseNumber'),
                        'PremiseName': this.context.getControlValue('PremiseName'),
                        'ProductCode': this.context.getControlValue('ProductCode'),
                        'ProductDesc': this.context.getControlValue('ProductDesc'),
                        'CalendarUpdateAllowed': this.context.getControlValue('CalendarUpdateAllowed'),
                        'ServiceVisitAnnivDate': this.context.getControlValue('ServiceVisitAnnivDate'),
                        'ServiceAnnualValue': this.context.getControlValue('ServiceAnnualValue'),
                        'LastChangeEffectDate': this.context.getControlValue('LastChangeEffectDate'),
                        'ServiceCover': this.context.getControlValue('ServiceCoverROWID')
                    });
                this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave5();
            }
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave5();
        }
    }

    public riMaintenance_AfterSave5(): void {
        this.context.pageParams.uiDisplay.cmdValue = this.context.pageParams.blnValueRequired && this.context.isControlChecked('ShowValueButton');
        this.context.disableControl('cmdValue', !this.context.pageParams.uiDisplay.cmdValue);
        this.context.setControlValue('RenegOldValue', '0');
        if (this.context.parentMode !== 'Premise-Add'
            || !this.context.riExchange.getParentAttributeValue('RenegContract')) {
            this.context.setControlValue('chkRenegContract', false);
            this.context.iCABSAServiceCoverMaintenance3.chkRenegContract_onclick();
        }
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd') &&
            this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate &&
            this.context.InStr('FieldShowList', 'ServiceQuantity') !== -1 &&
            !this.context.riExchange.URLParameterContains('PendingReduction') &&
            (this.context.pageParams.blnQuantityChange || this.context.pageParams.blnValueChange)) {
            this.context.pageParams.reloadOnReturn = true;
            this.context.navigate('ServiceCoverUpdate', InternalGridSearchApplicationModuleRoutes.ICABSSASERVICECOVERDISPLAYGRID, {
                'ServiceCover': this.context.getControlValue('ServiceCoverROWID')
            });
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.saveReturnMethod = this.context.postSaveMethodType.POST_SAVE_UPDATE_6;
        } else {
            this.context.iCABSAServiceCoverMaintenance7.riMaintenance_AfterSave6();
        }
    }

    public riMaintenance_AfterSave6(): void {
        this.context.iCABSAServiceCoverMaintenance8.ResetPriceChangeVariable();
        //this.context.iCABSAServiceCoverMaintenance1.riMaintenance_AfterFetch();
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdCopyServiceCover');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'InvTypeSel');
        if (this.context.pageParams.uiDisplay.trEntitlementServiceQuantity) {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'btnDefaultServiceQuantity');
        }
        this.context.iCABSAServiceCoverMaintenance7.HideQuickWindowSet(true);
        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
        this.context.pageParams.dtLastChangeEffectDate.value = null;
        if (this.context.pageParams['updateMatchedDisplayValues']) {
            this.context.pageParams['updateMatchedDisplayValues'] = false;
            this.context.initialLoad = false;
            this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
        }
        this.context.showSuccessMessageDialog();
        this.context.iCABSAServiceCoverMaintenance2.riMaintenanceBeforeUpdate();
    }

    public btnDepositAdd_OnClick(): void {
        this.context.setControlValue('DepositAddAdditional', 'Y');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DepositDate');
        this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DepositAmount');
        this.context.setControlValue('DepositDate', '');
        this.context.pageParams.dtDepositDate.value = null;
        this.context.setControlValue('DepositAmount', '');
        this.context.setControlValue('DepositAmountApplied', '');
        this.context.setControlValue('DepositPostedDate', '');
    }

    //*****************
    //* AFTER ABANDON *
    //*****************
    public riMaintenance_AfterAbandon(): void {
        this.context.pageParams.uiDisplay.trDepositLineAdd = false;
        this.context.setControlValue('DepositAddAdditional', 'N');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepositAmount');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DepositDate');
        this.context.iCABSAServiceCoverMaintenance8.EnableRMMFields();
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdCalcInstalment');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelectCompositeProductcode');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        if (this.context.pageParams.vbDefaultTaxCodeProductExpenseReq && this.context.pageParams.vbDefaultTaxCodeProductExpenseLog) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selTaxCode');
        }
        this.context.iCABSAServiceCoverMaintenance2.HardSlotType_OnChange();
        this.context.setControlValue('selTaxCode',
            this.context.getRawControlValue('TaxCode'));
        this.context.setControlValue('ComponentGridCacheTime', this.context.utils.Time());
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdComponentSelAll');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdComponentDesAll');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdDiaryView');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelServiceBasis');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'selHardSlotType');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelAutoPattern');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelAutoAllocation');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelSubjectToUplift');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'SelUpliftVisitPosition');
        this.context.pageParams.vbAbandon = true;
        if (this.context.getRawControlValue('selServiceBasis') === 'H') {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'cmdHardSlotCalendar');
        }
        //rebuild display gridif( service cover reduction && display tab enabled;
        if (this.context.pageParams.vbEnableServiceCoverDispLev &&
            this.context.isControlChecked('DisplayLevelInd')) {
            if (this.context.riExchange.URLParameterContains('PendingReduction')) {
                this.context.iCABSAServiceCoverMaintenance3.BuildDisplayGrid();
                this.context.iCABSAServiceCoverMaintenance3.riDisplayGrid_BeforeExecute();
                do {
                    //wait for response from screen
                } while (this.context.riExchange.Busy);
            }
        }
        this.context.iCABSAServiceCoverMaintenance4.ShowFields();
        this.context.pageParams.vbAbandon = false;
        this.context.pageParams.uiDisplay.labelInactiveEffectDate = false;
        this.context.pageParams.uiDisplay.InactiveEffectDate = false;
        this.context.pageParams.uiDisplay.tdReasonLab = false;
        this.context.pageParams.uiDisplay.tdReason = false;
        this.context.iCABSAServiceCoverMaintenance8.ResetPriceChangeVariable();
        if (this.context.getRawControlValue('ContractNumber')) {
            if (this.context.getRawControlValue('InactiveEffectDate')) {
                this.context.pageParams.uiDisplay.labelInactiveEffectDate = true;
                this.context.pageParams.uiDisplay.InactiveEffectDate = true;
                this.context.pageParams.uiDisplay.tdReasonLab = false;
                this.context.pageParams.uiDisplay.tdReason = false;
                if (this.context.getRawControlValue('SCLostBusinessDesc')) {
                    this.context.pageParams.uiDisplay.tdReasonLab = true;
                    this.context.pageParams.uiDisplay.tdReason = true;
                    this.context.pageParams.SCLostBusinessDesc_title =
                        this.context.getRawControlValue('scLostBusinessDesc2') + '10' +
                        this.context.getRawControlValue('scLostBusinessDesc3');
                }
            }
            this.context.pageParams.uiDisplay.cmdValue = this.context.pageParams.blnValueRequired && this.context.isControlChecked('ShowValueButton');
            this.context.disableControl('cmdValue', !this.context.pageParams.uiDisplay.cmdValue);
            //Reset the AnnualTimeChange back to blank;
            this.context.setControlValue('AnnualTimeChange', '');
            //Reset the AnnualValueChange back to 0;
            this.context.setControlValue('AnnualValueChange', '0');
            this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'AnnualValueChange');
            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                (this.context.isControlChecked('DisplayLevelInd') ||
                    this.context.isControlChecked('InvoiceUnitValueRequired')) &&
                this.context.isControlChecked('VisitTriggered')) {
                this.context.setControlValue('UnitValueChange', '0');
                this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'UnitValueChange');
                this.context.pageParams.uiDisplay.trUnitValue = true;
            }
            this.context.pageParams.uiDisplay.trServiceAnnualValue = this.context.pageParams.blnAccess && this.context.pageParams.blnValueRequired;
            let chkFOC: boolean = this.context.isControlChecked('chkFOC');
            this.context.pageParams.uiDisplay.tdFOCInvoiceStartDate = chkFOC;
            this.context.pageParams.uiDisplay.tdFOCProposedAnnualValue = chkFOC;
        }

        this.context.pageParams.uiDisplay.trOutstandingInstallations = false;
        this.context.pageParams.uiDisplay.trInstallationEmployee = false;
        this.context.pageParams.uiDisplay.trOutstandingRemovals = false;
        this.context.pageParams.uiDisplay.trRemovalEmployee = false;
        this.context.pageParams.uiDisplay.trEffectiveDate = false;
        this.context.pageParams.uiDisplay.trInstallationValue = false;
        this.context.pageParams.uiDisplay.trRefreshDisplayVal = true;
        if (!this.context.isControlChecked('chkRenegContract')) {
            this.context.pageParams.uiDisplay.tdRenegOldContract = false;
        }

        this.context.iCABSAServiceCoverMaintenance7.HideQuickWindowSet(true);
        this.context.pageParams.uiDisplay.trInvoiceSuspend = this.context.riMaintenance.RecordSelected(false);

        this.context.iCABSAServiceCoverMaintenance3.ToggleEntitlementRequired();
        this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
        this.context.iCABSAServiceCoverMaintenance7.BuildInvoiceTypeSelect();   // restore correct value;
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'InvTypeSel');
        this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'cmdCopyServiceCover');
        if (this.context.pageParams.uiDisplay.trEntitlementServiceQuantity) {
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'btnDefaultServiceQuantity');
        }
        if (this.context.riExchange.getCurrentContractType() === 'C') {
            this.context.iCABSAServiceCoverMaintenance2.ServiceBasis_OnChange();
        }
        this.context.iCABSAServiceCoverMaintenance5.AutoPattern_OnChange();
        this.context.iCABSAServiceCoverMaintenance5.AutoAllocation_OnChange();
        if (this.context.pageParams.uiDisplay.trMultipleTaxRates) {
            this.context.iCABSAServiceCoverMaintenance3.MultipleTaxRates_onClick();
        }
        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
        //this.context.riMaintenance.Focus();
    }

    public riMaintenance_BeforeAbandon(): void {
        if (this.context.getRawControlValue('HardSlotUpdate') === 'Yes') {
            this.context.iCABSAServiceCoverMaintenance7.HardSlotTemplateRemove();
        }
    }

    //******************************
    //* Query Unload HTML Document *
    //******************************
    public riExchange_QueryUnloadHTMLDocument(): void {
        if (this.context.parentMode === 'Premise-Add') {
            if (!this.context.pageParams.ServiceCoverAdded) {
                //Msgbox '<%=riT('No Service Cover Records Have Been Added For This Premises') ;
            }
        }
    }

    //***********
    //* Search  *
    //***********
    public riMaintenance_Search(): void {
        if (!this.context.getRawControlValue('ContractNumber')) {
            //this.context.iCABSAServiceCoverMaintenance7.ContractNumberSelection(riMaintenance_Search_Callback1);
            this.context.iCABSAServiceCoverMaintenance7.ContractNumberSelection();
        }

        function riMaintenance_Search_Callback1(): void {
            if (this.context.getRawControlValue('ContractNumber') &&
                !this.context.getRawControlValue('PremiseNumber')) {
                this.context.iCABSAServiceCoverMaintenance7.PremiseNumberSelection(riMaintenance_Search_Callback2);
            }
        }
        function riMaintenance_Search_Callback2(): void {
            if (this.context.getRawControlValue('ContractNumber') &&
                this.context.getRawControlValue('PremiseNumber')) {
                this.context.iCABSAServiceCoverMaintenance7.ProductCodeSelection();
            }
        }
    }

    //*************
    //* Functions *
    //*************
    public BuildInvoiceTypeSelect(): void {
        let ValArray;
        let DescArray;
        if (!this.context.fieldHasValue('InvoiceTypeVal')
            || !this.context.fieldHasValue('InvoiceTypeDesc')) { // no valid options - hide select;
            this.context.pageParams.uiDisplay.trInvoiceType = false;
        } else {
            ValArray = this.context.getRawControlValue('InvoiceTypeVal').toString().split('|');
            DescArray = this.context.getRawControlValue('InvoiceTypeDesc').toString().split('|');
            if (ValArray && DescArray) {
                this.context.pageParams.InvTypeSel = [];
                for (let i = 0; i < ValArray.length; i++) {
                    let obj = {
                        text: DescArray[i],
                        value: ValArray[i]
                    };
                    this.context.pageParams.InvTypeSel.push(obj);
                }
            }
            this.context.setControlValue('InvTypeSel', this.context.getControlValue('InvoiceTypeNumber') || this.context.pageParams.InvTypeSel[0].value);
            this.context.pageParams.uiDisplay.trInvoiceType = this.context.pageParams.blnValueRequired;
        }
        if (this.context.pageParams.vbEnableServiceCoverDispLev) {
            this.context.iCABSAServiceCoverMaintenance6.IsVisitTriggered();
        }
    }

    //********
    //* Menu *
    //********
    public BuildMenuOptions(): void {

        let cEmployeeLimitChildDrillOptions = this.context.riExchange.getParentHTMLValue('EmployeeLimitChildDrillOptions');
        this.context.setControlValue('EmployeeLimitChildDrillOptions', this.context.riExchange.getParentHTMLValue('EmployeeLimitChildDrillOptions'));

        let objPortfolioGroup = [];
        let objHistoryGroup = [];
        let objInvoicingGroup = [];
        let objServiceGroup = [];
        let objCustomerGroup = [];

        if (this.context.pageParams.cEmployeeLimitChildDrillOptions !== 'Y') {
            objPortfolioGroup.push({ value: 'Contract', label: 'Contract' });
            objPortfolioGroup.push({ value: 'Premise', label: 'Premises' });
        }

        if (this.context.pageParams.vbEnableServiceCoverDetail) {
            objPortfolioGroup.push({ value: 'ServiceDetail', label: 'Service Detail' });
        }
        if (this.context.pageParams.uiDisplay.trLinkedProduct === true) {
            objPortfolioGroup.push({ value: 'LinkedProducts', label: 'Linked Products' });
        }
        objPortfolioGroup.push({ value: 'TeleSalesOrderLine', label: 'Telesales Order Line' });

        //History Options
        objHistoryGroup.push({ value: 'History', label: 'Service Cover History' });
        objHistoryGroup.push({ value: 'EventHistory', label: 'Event History' });
        objHistoryGroup.push({ value: 'ServiceValue', label: 'Service Value' });

        if (this.context.pageParams.blnEnableDOWSentricon) {
            objHistoryGroup.push({ value: 'DOWServiceValue', label: 'DOW Service Value' });
        }
        objHistoryGroup.push({ value: 'SalesStatsAdjustment', label: 'Adjust Sales Stats' });


        //Invoicing Options
        objInvoicingGroup.push({ value: 'ProRata', label: 'Pro Rata Charge' });
        objInvoicingGroup.push({ value: 'InvoiceHistory', label: 'Invoice History' });

        //Service Options
        if (this.context.pageParams.vEnableTabularView) {
            objServiceGroup.push({ value: 'PlanVisit', label: 'Planned Visits Grid' });
            objServiceGroup.push({ value: 'PlanVisitTabular', label: 'Planned Visits Table' });
        } else {
            objServiceGroup.push({ value: 'PlanVisit', label: 'Planned Visits' });
        }

        objServiceGroup.push({ value: 'StaticVisit', label: 'Static Visits (SOS)' });
        objServiceGroup.push({ value: 'VisitHistory', label: 'Visit History' });

        if (this.context.riExchange.getCurrentContractType() === 'C' || this.context.riExchange.getCurrentContractType() === 'J') {
            objServiceGroup.push({ value: 'ServiceVisitPlanning', label: 'Service Visit Planning' });
        }

        if (this.context.pageParams.vbEnableLocations) {
            objServiceGroup.push({ value: 'Location', label: 'Service Cover Locations' });
        }

        if (this.context.riExchange.getCurrentContractType() === 'C') {
            objServiceGroup.push({ value: 'SeasonalService', label: 'Service Seasons' });
            objServiceGroup.push({ value: 'ServiceCalendar', label: 'Service Calendar' });
            objServiceGroup.push({ value: 'ClosedCalendar', label: 'Service Closed Calendar' });
        }

        objServiceGroup.push({ value: 'Service Recommendations', label: 'Service Recommendations' });
        objServiceGroup.push({ value: 'StateOfService', label: 'State Of Service' });
        objServiceGroup.push({ value: 'ServiceCoverWaste', label: 'Service Cover Waste' });
        //objServiceGroup.push({ value: 'TreatmentPlan', label: 'Treatment Plan' });

        if (this.context.pageParams.vShowWasteHistory) {
            objServiceGroup.push({ value: 'WasteConsignmentNoteHistory', label: 'Waste Consignment Note History' });
        }
        if (this.context.pageParams.vEnableServiceCoverDispLev) {
            objServiceGroup.push({ value: 'ServiceCoverDisplays', label: 'Service Cover Displays' });
        }
        if (this.context.pageParams.vSCVisitTolerances) {
            objServiceGroup.push({ value: 'VisitTolerances', label: 'Visit Tolerances' });
        }
        if (this.context.pageParams.vSCInfestationTolerances) {
            objServiceGroup.push({ value: 'InfestationTolerances', label: 'Infestation Tolerances' });
        }
        if (this.context.pageParams.lEnableQualificationOption) {
            objServiceGroup.push({ value: 'Qualifications', label: 'Qualifications' });
        }
        if (this.context.pageParams.vEnableEntitlementForWashroom) {
            objServiceGroup.push({ value: 'Entitlement', label: 'Entitlement' });  //todo : ITA-196
        }

        objCustomerGroup.push({ value: 'ContactManagement', label: 'Contact Management' });
        objCustomerGroup.push({ value: 'ContactManagementSearch', label: 'Contact Centre Search' });
        objCustomerGroup.push({ value: 'CustomerInformation', label: 'Customer Information' });

        //Add Groups to menu
        this.context.pageParams.menu = [
            { OptionGrp: 'Portfolio', Options: objPortfolioGroup },
            { OptionGrp: 'History', Options: objHistoryGroup },
            { OptionGrp: 'Invoicing', Options: objInvoicingGroup },
            { OptionGrp: 'Servicing', Options: objServiceGroup },
            { OptionGrp: 'Customer Relations', Options: objCustomerGroup }
        ];
        this.context.setControlValue('menu', 'Options');
    }

    //****************************************************
    //*Menu options/Buttons/Check boxes (onclick actions)*
    //****************************************************

    //***********
    //* LookUps *
    //***********
    public ContractNumberSelection(callback?: any): void {
        if (this.context.contractSearch) {
            this.context.openSearchModal(this.context.contractSearch, callback);
        }
    }

    public PremiseNumberSelection(callback?: any): void {
        if (this.context.premiseSearch) {
            this.context.openSearchModal(this.context.premiseSearch, callback);
        }
    }

    public APTChangedAccordingToQuantity(): void {

        if (this.context.pageParams.vSCEnableAPTByServiceType) {
            let APTFound;
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSExchangeFunctions.p';
            this.context.riMaintenance.PostDataAdd('PostDesc', 'NewAPTValue', MntConst.eTypeText);
            this.context.riMaintenance.PostDataAddBusiness();
            this.context.PostDataAddFromField('ProductCode', MntConst.eTypeText);
            this.context.PostDataAddFromField('ServiceQuantity', MntConst.eTypeInteger);
            this.context.PostDataAddFromField('LastChangeEffectDate', MntConst.eTypeDate);
            this.context.PostDataAddFromField('ServiceTypeCode', MntConst.eTypeText);
            this.context.riMaintenance.ReturnDataAdd('ActualPlannedTime', MntConst.eTypeTime);
            this.context.riMaintenance.ReturnDataAdd('APTFound', MntConst.eTypeCheckBox);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                APTFound = data['APTFound'];
                if (APTFound) {
                    this.context.setControlValue('ActualPlannedTime', data['ActualPlannedTime']);
                }
            }, 'POST');
        }
    }

    //*****************
    //* BEFORE FETCH  *
    //*****************
    public riMaintenance_BeforeFetch(): void {
        this.context.pageParams.strFunctions = '';
        if (this.context.getRawControlValue('PremiseNumber') &&
            this.context.getRawControlValue('ContractNumber') && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.pageParams.strFunctions = 'GetServiceDetails,GetShowFields,';
            this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strRequestProcedure;
            this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'Function=' + this.context.utils.Left(this.context.pageParams.strFunctions,
            this.context.utils.len(this.context.pageParams.strFunctions));
            this.context.riMaintenance.CBORequestAddCS('BusinessCode', MntConst.eTypeCode);
            this.context.riMaintenance.CBORequestAdd('ContractNumber');
            this.context.riMaintenance.CBORequestAdd('PremiseNumber');
            this.context.riMaintenance.CBORequestAdd('ProductCode');
            this.context.riMaintenance.CBORequestAdd('ServiceBranchNumber');
            this.context.riMaintenance.CBORequestAdd('BranchServiceAreaCode');
            this.context.riMaintenance.CBORequestAdd('BranchServiceAreaDesc');
            this.context.riMaintenance.CBORequestExecute(this.context, function (data: any): any {
                if (data) {
                    const currentEntitlementType: any = this.context.getControlValue('EntitlementInvoiceTypeCode');
                    this.context.riMaintenance.renderResponseForCtrl(this.context, data, true);
                    this.context.setControlValue('EntitlementInvoiceTypeCode', currentEntitlementType);
                }
                this.context.riMaintenance.CustomBusinessObject = this.context.pageParams.strEntryProcedure;
                if (!this.context.getRawControlValue('ErrorMessage')) {
                    this.context.iCABSAServiceCoverMaintenance4.ShowFields();
                    //this.context.riMaintenance.GetVirtuals();
                }
            });
        }
        this.context.riMaintenance.CustomBusinessObjectAdditionalPostData = 'ContractTypeCode=' + this.context.riExchange.getCurrentContractType();
        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
    }

    public ProductCodeSelection(): void {
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            //riExchange.Mode = 'ServiceCover-' + CurrentContractType;
            //window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Business/iCABSBProductSearch.htm';
            if (this.context.pageParams.vbEnableProductServiceType) {
                this.context.iCABSAServiceCoverMaintenance1.GetProductServiceType();
            }
            this.context.iCABSAServiceCoverMaintenance7.CheckServiceNotifyInd();
        } else {
            this.context.serviceCoverSearch.openModal();
        }
    }

    public ServiceTypeCode_ondeactivate(): void {
        if (this.context.getRawControlValue('ServiceTypeCode')) {
            this.context.setControlValue('ServiceTypeDesc', '');
        }
    }

    public ServiceTypeCode_onChange(): void {
        if (!this.context.fieldHasValue('ServiceTypeCode')) {
            this.context.setControlValue('ServiceTypeDesc', '');
        } else {
            this.context.iCABSAServiceCoverMaintenance7.APTChangedAccordingToQuantity();
            this.context.iCABSAServiceCoverMaintenance7.CheckServiceNotifyInd();
        }
    }

    public CheckServiceNotifyInd(): void {
        this.context.setControlValue('ServiceNotifyInd', true);
    }

    public GuaranteeRequired_onclick(): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'GuaranteeRequired')) {
            this.context.iCABSAServiceCoverMaintenance7.CreateTabs();
            this.context.renderTab(4);
        }
        this.context.iCABSAServiceCoverMaintenance7.GuaranteeToggle();
    }

    public GuaranteeToggle(): void {
        if (this.context.pageParams.boolPropertyCareInd === 'Y') {
            let isGuaranteeRequired = this.context.isControlChecked('GuaranteeRequired');
            this.context.pageParams.uiDisplay.trGuaranteeExpiry15 = isGuaranteeRequired;
            this.context.pageParams.uiDisplay.trGuaranteeCommence15 = isGuaranteeRequired;
            this.context.pageParams.uiDisplay.trNoGuaranteeReason = !isGuaranteeRequired;
            this.context.setRequiredStatus('NoGuaranteeCode', isGuaranteeRequired);
            this.context.pageParams.uiDisplay.trNumberBedrooms = isGuaranteeRequired;
            this.context.pageParams.uiDisplay.trAgeOfPropertyLabel = isGuaranteeRequired;
            this.context.pageParams.uiDisplay.trListedBuilding = isGuaranteeRequired;
            if (isGuaranteeRequired) {
                this.context.riExchange.riInputElement.SetErrorStatus(this.context.uiForm, 'NoGuaranteeCode', false);
            } else {
                this.context.setControlValue('GuaranteeCommence', '');
                this.context.setControlValue('GuaranteeExpiry', '');
            }
        }
    }

    public SeasonalServiceInd_onclick(): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'SeasonalServiceInd')) {
            this.context.iCABSAServiceCoverMaintenance7.ToggleSeasonalDates();
        }
    }

    public DOWSentriconInd_onclick(): void {
        if (!this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'DOWSentriconInd')) {
            this.context.iCABSAServiceCoverMaintenance7.DOWSentriconToggle();
        }
    }

    public DOWSentriconToggle(): void {
        this.context.pageParams.uiDisplay.trDOWInstall = false;
        this.context.pageParams.uiDisplay.trDOWProduct = false;
        this.context.pageParams.uiDisplay.trDOWPerimeter = false;
        this.context.pageParams.uiDisplay.trDOWRenewalDate = false;
        if (this.context.InStr('FieldShowList', 'PerimeterValue') !== -1) {
            this.context.pageParams.uiDisplay.trPerimeterValue = true;
            this.context.setRequiredStatus('PerimeterValue', true);
        }
        if (this.context.isControlChecked('DOWSentriconInd')) {
            this.context.pageParams.uiDisplay.trDOWInstall = true;
            this.context.pageParams.uiDisplay.trDOWProduct = true;
            this.context.pageParams.uiDisplay.trDOWPerimeter = true;
            this.context.pageParams.uiDisplay.trDOWRenewalDate = this.context.riExchange.getCurrentContractType() === 'C' &&
                this.context.getRawControlValue('DOWRenewalDate');
            if (this.context.InStr('FieldShowList', 'PerimeterValue') !== -1) {
                this.context.pageParams.uiDisplay.trPerimeterValue = false;
                this.context.setRequiredStatus('PerimeterValue', false);
            }
        }
        this.context.setRequiredStatus('DOWInstallTypeCode',
            this.context.isControlChecked('DOWSentriconInd'));
        this.context.setRequiredStatus('DOWProductCode',
            this.context.isControlChecked('DOWSentriconInd'));
        this.context.setRequiredStatus('DOWPerimeterValue',
            this.context.isControlChecked('DOWSentriconInd'));

        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DOWSentriconInd');
            if (this.context.getRawControlValue('DOWRenewalDate')) {
                this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DOWInstallTypeCode');
            } else {
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DOWInstallTypeCode');
            }
        } else if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'DOWRenewalDate');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DOWSentriconInd');
            this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'DOWInstallTypeCode');
        }
    }

    public ToggleSeasonalDates(): void {
        this.context.iCABSAServiceCoverMaintenance7.CreateTabs();
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            //this.context.renderTab(5);
        }
        if (this.context.isControlChecked('SeasonalServiceInd')) {
            this.context.iCABSAServiceCoverMaintenance7.InitialiseSeasonalService();
        } else {
            // Hide (and make not required) Season 1 data;
            this.context.iCABSAServiceCoverMaintenance7.ShowHideSeasonRow(1, false);
        }
        this.context.setRequiredStatus('NumberOfSeasons',
            this.context.isControlChecked('SeasonalServiceInd'));
    }

    public cmdDOWServiceValue_onclick(): void {
        this.context.showAlert(MessageConstant.Message.PageNotCovered);
        //window.location = '/wsscripts/riHTMLWrapper.p?riFileName=Application/iCABSADOWServiceValueGrid.htm<maxwidth>' + CurrentContractTypeURLParameter;
    }

    public MinCommitQty_onclick(): void {
        this.context.iCABSAServiceCoverMaintenance3.MinCommitQtyToggle();
    }

    public TrialPeriodInd_onClick(): void {
        if (this.context.pageParams.uiDisplay.tdTrialPeriodInd && !this.context.riExchange.riInputElement.isDisabled(this.context.uiForm, 'TrialPeriodInd')) {
            this.context.iCABSAServiceCoverMaintenance7.ToggleTrialPeriodStatus();
        }
    }

    public ToggleTrialPeriodStatus(): void {
        this.context.pageParams.blnRequired = this.context.isControlChecked('TrialPeriodInd');
        this.context.pageParams.blnTrialReleased = this.context.isControlChecked('TrialPeriodReleasedInd');
        this.context.iCABSAServiceCoverMaintenance7.CreateTabs();
        this.context.riMaintenance.SetRequiredStatus('TrialPeriodEndDate', this.context.pageParams.blnRequired);
        this.context.riMaintenance.SetRequiredStatus('TrialPeriodChargeValue', this.context.pageParams.blnRequired);
        this.context.riMaintenance.SetRequiredStatus('ProposedAnnualValue', this.context.pageParams.blnRequired);

        if (!this.context.pageParams.blnRequired) {
            this.context.setControlValue('TrialPeriodEndDate', '');
            this.context.setControlValue('TrialPeriodChargeValue', '');
            this.context.setControlValue('ProposedAnnualValue', '');
            this.context.riMaintenance.SetErrorStatus('TrialPeriodEndDate', false);
            this.context.riMaintenance.SetErrorStatus('TrialPeriodChargeValue', false);
            this.context.riMaintenance.SetErrorStatus('ProposedAnnualValue', false);

            if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd &&
                !this.context.isControlChecked('chkFOC')) {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd') &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.riMaintenance.EnableInput('UnitValue');
                }
                this.context.riMaintenance.EnableInput('ServiceAnnualValue');
                this.context.riExchange.riInputElement.Enable(this.context.uiForm, 'chkFOC');
            }
        }
        if (this.context.pageParams.blnRequired && this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            // 17/08/05 PG #12560 Add Trial Period Start Dt;
            // 23/08/05 Ensure dt is formatted before copying;
            if (this.context.getRawControlValue('ServiceCommenceDate') &&
                this.context.riExchange.riInputElement.isCorrect(this.context.uiForm, 'ServiceCommenceDate')) {
                this.context.setControlValue('TrialPeriodStartDate', this.context.getRawControlValue('ServiceCommenceDate'));
            }
            // Copy ServiceAnnualValue to ProposedAnnualValueif( it//s not blank;
            // #12561 value being reset after save;
            if (this.context.getRawControlValue('ServiceAnnualValue') &&
                !this.context.getRawControlValue('ProposedAnnualValue')) {
                this.context.setControlValue('ProposedAnnualValue',
                    this.context.getRawControlValue('ServiceAnnualValue'));
            } else {
                // #12561 value being reset after save;
                if (!this.context.getRawControlValue('ProposedAnnualValue')) {
                    this.context.setControlValue('ProposedAnnualValue', '0');
                }
            }
            this.context.setControlValue('ServiceAnnualValue', '0');
            // #12561 value being reset after save;
            if (!this.context.getRawControlValue('TrialPeriodChargeValue')) {
                this.context.setControlValue('TrialPeriodChargeValue', '0');
            }
            // Call 'isError' on chngd flds to cause formatting to occur;
            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                this.context.isControlChecked('DisplayLevelInd') &&
                this.context.isControlChecked('VisitTriggered')) {
                this.context.riExchange.riInputElement.isError(this.context.uiForm, 'UnitValue');
            }
            this.context.riExchange.riInputElement.isError(this.context.uiForm, 'ServiceAnnualValue');
            this.context.riExchange.riInputElement.isError(this.context.uiForm, 'ProposedAnnualValue');
            this.context.riExchange.riInputElement.isError(this.context.uiForm, 'TrialPeriodChargeValue');

            if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                this.context.isControlChecked('DisplayLevelInd') &&
                this.context.isControlChecked('VisitTriggered')) {
                this.context.riMaintenance.DisableInput('UnitValue');
            }
            this.context.riMaintenance.DisableInput('ServiceAnnualValue');
            this.context.riExchange.riInputElement.Disable(this.context.uiForm, 'chkFOC');
        } else if (this.context.pageParams.blnRequired && this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            //if( was Trial Period, && we are now updating, set access to flds;
            // Always disable this on updt - it may be enabled later;
            this.context.riMaintenance.DisableInput('TrialPeriodChargeValue');
            if (this.context.pageParams.blnTrialReleased) {
                this.context.riMaintenance.DisableInput('TrialPeriodEndDate');
                this.context.riMaintenance.DisableInput('ProposedAnnualValue');
                this.context.setRequiredStatus('ProposedAnnualValue', false);
            } else {
                if (this.context.pageParams.vbEnableServiceCoverDispLev &&
                    this.context.isControlChecked('DisplayLevelInd') &&
                    this.context.isControlChecked('VisitTriggered')) {
                    this.context.riMaintenance.DisableInput('UnitValue');
                    this.context.riMaintenance.DisableInput('UnitValueChange');
                }
                this.context.riMaintenance.DisableInput('ServiceAnnualValue');
                this.context.riMaintenance.DisableInput('AnnualValueChange');
            }
        }
    }

    public InitialiseSeasonalService(): void {
        // Set no of seasons to 1;
        if (this.context.riMaintenance.CurrentMode !== MntConst.eModeSelect
            && (this.context.getRawControlValue('NumberOfSeasons') === '')) {
            this.context.setControlValue('NumberOfSeasons', '1');
            this.context.setControlValue('FixedNumberOfSeasons', '1');
        }
        this.context.iCABSAServiceCoverMaintenance7.ShowSeasonalRows();
    }

    public HardSlotTemplateRemove(): void {
        this.context.riMaintenance.clear();
        this.context.riMaintenance.BusinessObject = this.context.pageParams.strRequestProcedure;
        this.context.riMaintenance.PostDataAddBusiness();
        this.context.riMaintenance.PostDataAddFunction('HardSlotTemplateRemove');
        this.context.PostDataAddFromField('HardSlotTemplateNumber', MntConst.eTypeInteger);
        this.context.PostDataAddFromField('HardSlotVersionNumber', MntConst.eTypeInteger);
        this.context.riMaintenance.Execute(this.context, function (data: any): any {
            //Empty block
        });
    }

    public SelUpliftVisitPosition_onChange(): void {
        this.context.setControlValue('UpliftVisitPosition',
            this.context.getRawControlValue('SelUpliftVisitPosition'));
    }

    public ShowSeasonalRows(event?: any): void {
        let arrSeasonsVisible = [false, false, false, false, false, false, false, false];
        let arrSeasonsEnabled = [false, false, false, false, false, false, false, false];
        let arrVisitsEnabled = [true, true, true, true, true, true, true, true];
        let iNumberOfSeasons = -1;
        let iFixedNumberOfSeasons = -1;
        let iVisitCount = 0;

        // Convert value of input to int;
        iNumberOfSeasons = this.context.CInt('NumberOfSeasons');
        iFixedNumberOfSeasons = this.context.CInt('FixedNumberOfSeasons');

        //if( int conversion failed, ) { resolve problems;
        if (iNumberOfSeasons === -1 || iNumberOfSeasons <= 0) {
            this.context.setControlValue('NumberOfSeasons', '1');
            iNumberOfSeasons = 1;
        }
        if (iFixedNumberOfSeasons === -1) {
            this.context.setControlValue('FixedNumberOfSeasons', '0');
            iFixedNumberOfSeasons = 0;
        }

        if (iNumberOfSeasons > 8) {
            iNumberOfSeasons = 8;
            this.context.setControlValue('NumberOfSeasons', '8');
        }
        //if( current mode is 'update';
        if (this.context.riMaintenance.CurrentMode === MntConst.eModeUpdate) {
            //if( the no of existing seasons is less than the new no of seasons, ) { need to allow;
            // updts to the new season. Otherwise, we should show all seasons, && only allow updts to;
            // the no of visits.;
            if (iNumberOfSeasons > iFixedNumberOfSeasons) {
                for (let i = 0; i < 8; i++) {
                    arrSeasonsEnabled[i] = !(iNumberOfSeasons >= (i + 1) && iFixedNumberOfSeasons >= (i + 1));
                }
            } else {
                // Need to show the existing no of seasons;
                iNumberOfSeasons = iFixedNumberOfSeasons;
                for (let i = 0; i < 8; i++) {
                    arrSeasonsEnabled[i] = false;
                }
            }

            //If Updating, && the effective dt is before all season start dates - disable every visit pattern field;
            this.context.pageParams.blnDisableVisitPattern = false;
            if (!this.context.getRawControlValue('LastChangeEffectDate')) {
                this.context.pageParams.blnDisableVisitPattern = true;
            } else if (this.context.getRawControlValue('LastChangeEffectDate') &&
                !this.context.getRawControlValue('FirstSeasonStartDate')) {
                this.context.pageParams.blnDisableVisitPattern = false;
            } else if (this.context.CDate('LastChangeEffectDate') < this.context.CDate('FirstSeasonStartDate')) {
                this.context.pageParams.blnDisableVisitPattern = true;
            }

            if (this.context.pageParams.blnDisableVisitPattern) {
                for (let i = 0; i < arrVisitsEnabled.length; i++) {
                    arrVisitsEnabled[i] = false;
                }
            }
        } else if (this.context.riMaintenance.CurrentMode === MntConst.eModeAdd) {
            for (let i = 0; i < 8; i++) {
                arrSeasonsEnabled[i] = true;
                arrVisitsEnabled[i] = true;
            }
        }
        //Show the correct no of seasons;
        for (let i = 8; i > 0; i--) {
            arrSeasonsVisible[i - 1] = (iNumberOfSeasons >= i);
        }
        //Populate values from arrays through to screen fields;
        //Enabled;
        for (let i = 0; i < arrSeasonsEnabled.length; i++) {
            this.context.iCABSAServiceCoverMaintenance7.EnableSeasonalRow(arrSeasonsEnabled[i], (i + 1));
        }
        //No of Visits Enabled;
        //For i = LBound(arrVisitsEnabled) To UBound(arrVisitsEnabled);
        //Call EnableSeasonalVisitRow(arrVisitsEnabled(i), CStr(i + 1));
        //Next;
        // Visibility;
        for (let i = arrSeasonsVisible.length - 1; i >= 0; i--) {
            this.context.iCABSAServiceCoverMaintenance7.ShowHideSeasonRow((i + 1), arrSeasonsVisible[i]);
        }
        this.context.iCABSAServiceCoverMaintenance4.riExchange_CBORequest();
    }

    public ShowHideSeasonRow(ipSeason: number, ipVisible: boolean): void {
        let trRow;
        // Show/hide row;
        this.context.pageParams.uiDisplay.Seasons[ipSeason - 1].trRow = ipVisible;
        // Set required status of input fields;
        this.context.setRequiredStatus('SeasonNumber' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonalFromDate' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonalFromWeek' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonalFromYear' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonalToDate' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonalToWeek' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonalToYear' + ipSeason, ipVisible);
        this.context.setRequiredStatus('SeasonNoOfVisits' + ipSeason, ipVisible);
    }

    public EnableSeasonalRow(ipEnable: boolean, ipRow: number): void {
        this.context.disableControl('SeasonNumber' + ipRow, !ipEnable);
        this.context.disableControl('SeasonalFromDate' + ipRow, !ipEnable);
        this.context.disableControl('SeasonalFromWeek' + ipRow, !ipEnable);
        this.context.disableControl('SeasonalFromYear' + ipRow, !ipEnable);
        this.context.disableControl('SeasonalToDate' + ipRow, !ipEnable);
        this.context.disableControl('SeasonalToWeek' + ipRow, !ipEnable);
        this.context.disableControl('SeasonalToYear' + ipRow, !ipEnable);
    }

    public HideQuickWindowSet(ipHide: boolean): void {
        for (let i = 1; i < 8; i++) {
            this.context.pageParams.uiDisplay['selQuickWindowSet' + i] = !ipHide;
        }
    }

    public EnableSeasonalVisitRow(ipEnable: boolean, ipRow: number): void {
        this.context.disableControl('SeasonNoOfVisits' + ipRow, !ipEnable);
    }

    public EnableSeasonalChanges(ipEnable: boolean): void {

        for (let i = 1; i < 8; i++) {
            this.context.iCABSAServiceCoverMaintenance7.EnableSeasonalRow(ipEnable, i);
        }
        // Do not allow user to update number of seasonsif( following template;
        this.context.riMaintenance.EnableInput('NumberOfSeasons');
        if (this.context.getRawControlValue('SeasonalTemplateNumber')) {
            this.context.riMaintenance.DisableInput('NumberOfSeasons');
        }
    }

    public SeasonalDateChange(ipFromDate: string, opFromWeek: string, opFromYear: string): void {
        // Call 'isError', in order to get the formatting correct, && to check that the user has not;
        // entered an incorrect value;
        if (this.context.getRawControlValue(ipFromDate)
            && !this.context.riExchange.riInputElement.isError(this.context.uiForm, ipFromDate)) {
            this.context.riMaintenance.clear();
            this.context.riMaintenance.BusinessObject = 'iCABSDateTimeRequests.p';
            this.context.riMaintenance.PostDataAddFunction('GetWeekFromDate');
            this.context.PostDataAddParamFromField('WeekCalcDate', ipFromDate, MntConst.eTypeDate);
            this.context.riMaintenance.ReturnDataAdd('WeekCalcNumber', MntConst.eTypeInteger);
            this.context.riMaintenance.ReturnDataAdd('WeekCalcYear', MntConst.eTypeInteger);
            this.context.riMaintenance.Execute(this.context, function (data: any): any {
                this.context.setControlValue(opFromWeek, data['WeekCalcNumber']);
                this.context.setControlValue(opFromYear, data['WeekCalcYear']);
            }, 'POST');

        } else {
            this.context.setControlValue(opFromWeek, '');
            this.context.setControlValue(opFromYear, '');
        }
    }

    public SeasonalWeekChange(ipRequestFunction: string, ipFromWeek: string, ipFromYear: string, opFromDate: string): void {
        if (this.context.getRawControlValue(ipFromWeek) &&
            this.context.getRawControlValue(ipFromYear)) {
            // Call 'isError', in order to get the formatting correct, && to check that the user has not;
            // entered an incorrect value;
            if (!this.context.riExchange.riInputElement.isError(this.context.uiForm, ipFromWeek)
                && !!this.context.riExchange.riInputElement.isError(this.context.uiForm, ipFromYear)) {

                this.context.riMaintenance.clear();
                this.context.riMaintenance.BusinessObject = 'iCABSDateTimeRequests.p';
                this.context.riMaintenance.PostDataAddFunction(ipRequestFunction);
                this.context.PostDataAddParamFromField('WeekCalcNumber', 'ipFromWeek', MntConst.eTypeInteger);
                this.context.PostDataAddParamFromField('WeekCalcYear', 'ipFromYear', MntConst.eTypeInteger);
                this.context.riMaintenance.ReturnDataAdd('WeekCalcDate', MntConst.eTypeDate);
                this.context.riMaintenance.ReturnDataAdd('WeekCalcNumber', MntConst.eTypeInteger);
                this.context.riMaintenance.ReturnDataAdd('WeekCalcYear', MntConst.eTypeInteger);
                this.context.riMaintenance.Execute(this.context, function (data: any): any {
                    this.context.setControlValue(opFromDate, data['WeekCalcDate']);
                    this.context.setControlValue(ipFromWeek, data['WeekCalcNumber']);
                    this.context.setControlValue(ipFromYear, data['WeekCalcYear']);
                }, 'POST');

            } else {
                this.context.setControlValue(opFromDate, '');
                if (this.context.pageParams['dt' + opFromDate].value !== null) {
                    this.context.pageParams['dt' + opFromDate].value = null;
                } else {
                    this.context.pageParams['dt' + opFromDate].value = void 0;
                }
            }
        } else {
            this.context.setControlValue(opFromDate, '');
            if (this.context.pageParams['dt' + opFromDate].value !== null) {
                this.context.pageParams['dt' + opFromDate].value = null;
            } else {
                this.context.pageParams['dt' + opFromDate].value = void 0;
            }
        }
    }

}
