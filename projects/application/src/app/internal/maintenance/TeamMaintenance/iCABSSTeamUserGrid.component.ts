import { Component, Injector, OnInit, AfterContentInit, ViewChild } from '@angular/core';

import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { InternalMaintenanceServiceModuleRoutes } from '@app/base/PageRoutes';

@Component({
    templateUrl: 'iCABSSTeamUserGrid.html'
})
export class TeamUserGridComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;

    public controls: IControls[] = [
        { name: 'RunStyle' },
        { name: 'TeamID', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'TeamSystemName', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'UserCode', disabled: true, required: false, type: MntConst.eTypeCode },
        { name: 'UserName', disabled: true, required: false, type: MntConst.eTypeText }
    ];
    public pageId: string = '';
    public pageTitle: string;
    public commonGridFunction: CommonGridFunction;

    constructor(injector: Injector) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
        this.pageId = PageIdentifier.ICABSSTEAMUSERGRID;
        this.browserTitle = this.pageTitle = `Team Details - Maintenance Grid`;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();
        this.pageParams.gridCacheRefresh = true;
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString(),
                pageSize: 10
            };
            this.pageParams.gridCurrentPage = 1;
            if (this.parentMode === 'AddTeam') {
                this.riExchange.getParentHTMLValue('TeamID');
                this.riExchange.getParentHTMLValue('TeamSystemName');
            } else {
                this.riExchange.getParentHTMLValue('UserCode');
                this.riExchange.getParentHTMLValue('UserName');
            }
            this.setControlValue('RunStyle', this.parentMode);

        }
        this.buildGrid();
        this.populateGrid();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('UserCode', 'TeamUser', 'UserCode', MntConst.eTypeCode, 12, false, '');
        this.riGrid.AddColumn('UserName', 'TeamUser', 'UserName', MntConst.eTypeTextFree, 30, false, '');
        this.riGrid.AddColumn('AdministratorInd', 'TeamUser', 'AdministratorInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('DefaultTeamInd', 'TeamUser', 'DefaultTeamInd', MntConst.eTypeImage, 1);
        this.riGrid.AddColumn('TeamClosureSecurityLDesc', 'TeamUser', 'TeamClosureSecurityLDesc', MntConst.eTypeImage, 1);

        this.riGrid.AddColumnOrderable('UserCode', true);
        this.riGrid.AddColumnOrderable('AdministratorInd', true);
        this.riGrid.AddColumnOrderable('DefaultTeamInd', true);
        this.riGrid.AddColumnOrderable('TeamClosureSecurityLDesc', true);
        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {};

            Object.keys(this.uiForm.controls).forEach(control => {
                formData = {
                    ...formData,
                    [control]: this.getControlValue(control)
                };
            });


            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
            formData[this.serviceConstants.PageSize] = this.pageParams.gridConfig.pageSize.toString();
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage.toString();
            formData[this.serviceConstants.GridCacheRefresh] = this.pageParams.gridCacheRefresh;
            formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
            formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('people/grid', 'team', 'System/iCABSSTeamUserGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.displayMessage(data);
                    } else {
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                        if (this.isReturning()) {
                            setTimeout(() => {
                                this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                            }, 500);
                        }
                        this.riGrid.Execute(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public cacheRefreshOnHeaderClick(): void {
        this.pageParams.gridCacheRefresh = false;
    }

    public onGridBodyDoubleClick(): void {
        if (this.riGrid.CurrentColumnName === 'UserCode') {
            this.setAttribute('UserCodeRowID', this.riGrid.Details.GetAttribute('UserCode', 'ROWID'));
            this.navigate('UpdateUserToTeam', InternalMaintenanceServiceModuleRoutes.ICABSSTEAMUSERMAINTENANCE);
        }

    }

    public onAddClick(): void {
        this.navigate('AddUserToTeam', InternalMaintenanceServiceModuleRoutes.ICABSSTEAMUSERMAINTENANCE);
    }
}
