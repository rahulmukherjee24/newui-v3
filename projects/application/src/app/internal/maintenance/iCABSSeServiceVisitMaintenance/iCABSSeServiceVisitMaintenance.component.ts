import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Injector } from '@angular/core';
import { QueryParams } from './../../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';

import { UiControls } from './uiControls';
import { ObjEllipsis } from './objEllipsis';
import { ObjLookUps } from './objLookUps';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { InternalGridSearchSalesModuleRoutes, ContractManagementModuleRoutes, InternalMaintenanceServiceModuleRoutes, InternalSearchModuleRoutes, InternalGridSearchApplicationModuleRoutes } from './../../../base/PageRoutes';
import { QueryParametersCallback } from './../../../base/Callback';
import { BaseComponent } from '../../../../app/base/BaseComponent';
import { VariableService } from './../../../../shared/services/variable.service';
import { HttpService } from './../../../../shared/services/http-service';
import { MntConst, RiTab } from './../../../../shared/services/riMaintenancehelper';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from './../../../../shared/components/ellipsis/ellipsis';
import { CommonDropdownComponent } from './../../../../shared/components/common-dropdown/common-dropdown.component';

@Component({
    selector: 'icabs-service-visit-maintenance',
    templateUrl: 'iCABSSeServiceVisitMaintenance.html'
})
export class ServiceVisitMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy, QueryParametersCallback {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    //Dropdowns
    @ViewChild('visitTypeSearch') public visitTypeSearch;
    @ViewChild('visitNarrativeSearch') visitNarrativeSearch: CommonDropdownComponent;
    @ViewChild('manualVisitReasonSearch') manualVisitReasonSearch: CommonDropdownComponent;
    @ViewChild('noServiceReasonSearch') noServiceReasonSearch: CommonDropdownComponent;
    @ViewChild('noSignatureReasonSearch') noSignatureReasonSearch: CommonDropdownComponent;

    //Ellipsis
    @ViewChild('PlanVisitNumber') PlanVisitNumber: EllipsisComponent;
    @ViewChild('PlanVisitNumber2') PlanVisitNumber2: EllipsisComponent;
    @ViewChild('EmployeeCode') EmployeeCode: EllipsisComponent;
    @ViewChild('AdditionalEmployee') AdditionalEmployee: EllipsisComponent;
    @ViewChild('ProductComponentCode') ProductComponentCode: EllipsisComponent;

    private isLookupNecessary: boolean = true;
    private tempServiceVisitRowID: string = '';
    public ellipsis: any = {};
    public lookUpRefs: any = {};

    public isInit: boolean = false;
    public isUpdateEnabled: boolean = true;
    public queryParams: any;
    public pageMode: string = '';
    public promptContent: string = MessageConstant.Message.ConfirmRecord;
    public promptContentDel: string = MessageConstant.Message.DeleteRecord;
    public modalConfig: any = { backdrop: 'static', keyboard: true };

    public labelStartDate: string = '';
    public labelStartTime: string = '';
    public arrAdditionalEmployees: Array<any> = [];
    public pageTitle = 'Service Visit Maintenance';

    public uiDisplay: any = {
        menu: true,
        trPremiseContactName: false,
        trPremiseContactPrintedName: false,
        trPremiseContactSignature: false,
        tdContactName: false,
        tdReviewDisplays: false,
        tdVisitReference: false,
        tdVisitReference2: false,
        trAdditionalChargeValue: false,
        trCustVisitReference: false,
        trDeliveryNoteNumber: false,
        trDeliveryQuantity: false,
        trInfestation: false,
        trInvoiceUnitValue: false,
        trManualVisitReason: false,
        trOrderQuantity: false,
        trPremiseLocationNumber: false,
        trPrepUsed: false,
        trProductComponentCode: false,
        trProductComponentRemoved: false,
        trReasonDesc: false,
        trReasonNumber: false,
        trRemovalQuantity: false,
        trServiceCoverItemNumber: false,
        trServicedQuantity: false,
        trServiceEnd: false,
        trServiceStart: false,
        trSignature: false,
        trUnDeliveredQuantity: false,
        trWasteCollected: false,
        trWasteConsignmentNoteNumber: false,
        trWEDSQty: false,
        trWorkLoadIndex: false
    };
    public dropDown: any = {
        menu: [],
        VisitTypeCode: {
            isRequired: true,
            isDisabled: false,
            triggerValidate: false,
            active: { id: '', text: '' },
            arrData: [],
            inputParams: { parentMode: 'LookUp' },
            displayFields: ['VisitTypeCode', 'VisitTypeDesc']
        },
        visitNarrativeSearch: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBVisitNarrativeSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['VisitNarrativeCode', 'VisitNarrativeDesc']
        },
        noSignatureReasonSearch: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                module: 'service',
                method: 'service-delivery/search',
                operation: 'Business/iCABSBNoSignatureReasonSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['ReasonNumber', 'ReasonDesc']
        },
        manualVisitReasonSearch: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                method: 'service-delivery/search',
                module: 'manual-service',
                operation: 'Business/iCABSBManualVisitReasonSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['ManualVisitReasonCode', 'ManualVisitReasonDesc']
        },
        noServiceReasonSearch: {
            isRequired: false,
            isDisabled: false,
            isTriggerValidate: false,
            params: {
                method: 'service-delivery/search',
                module: 'service',
                operation: 'Business/iCABSBNoServiceReasonSearch'
            },
            active: { id: '', text: '' },
            displayFields: ['NoServiceReasonCode', 'NoServiceReasonDesc'],
            columnType: [MntConst.eTypeCode]
        }
    };

    public xhr: HttpService;
    public xhrParams: any = {
        module: 'service',
        method: 'service-delivery/maintenance',
        operation: 'Service/iCABSSeServiceVisitMaintenance'
    };

    public pageId: string = '';
    public controls: Array<any>;

    public riTab: RiTab;
    public tab: any = {
        tab1: { id: 'grdMain', name: 'Main', visible: true, active: true },
        tab2: { id: 'grdAdditional', name: 'Additional', visible: true, active: false },
        tab3: { id: 'grdSiteRiskAssessment', name: 'Site Risk Assessment', visible: false, active: false } //Out of Scope
    };

    public lookUpObj: any = {};

    public subUserAccess: Subscription;
    public subLoggedInBranch: Subscription;

    ngOnInit(): void {
        super.ngOnInit();
        this.genCtrlMap();
    }

    ngAfterViewInit(): void {
        this.isInit = true;
        this.isRequesting = true;
        this.init();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subUserAccess) this.subUserAccess.unsubscribe();
        if (this.subLoggedInBranch) this.subLoggedInBranch.unsubscribe();
    }

    constructor(injector: Injector, private variableService: VariableService) {
        super(injector);

        this.controls = new UiControls().controls;
        this.ellipsis = new ObjEllipsis().ellipsis;
        this.lookUpRefs = new ObjLookUps().LookUpRefs;

        this.pageId = PageIdentifier.ICABSSESERVICEVISITMAINTENANCE;
        this.setURLQueryParameters(this);
    }

    public getURLQueryParameters(param: any): void {
        this.queryParams = param;
        this.isRequesting = true;
        if (this.isInit) { this.init(); }
    }

    public init(): void {
        this.riTab = new RiTab(this.tab, this.utils);
        this.tab = this.riTab.tabObject;
        this.riTab.TabAdd('Main');
        this.riTab.TabAdd('Additional');

        this.utils.setTitle(this.pageTitle);

        this.pageParams.vBusinessCode = this.utils.getBusinessCode();
        this.pageParams.vCountryCode = this.utils.getCountryCode();
        this.parentMode = this.riExchange.getParentMode();

        //Init Dropdowns
        this.visitTypeSearch.triggerDataFetch(this.dropDown.VisitTypeCode.inputParams);

        this.getSysCharDtetails();
    }

    //SpeedScript
    public getSysCharDtetails(): any {
        this.pageParams.vSCEnableVisitRef = false;
        this.pageParams.vSCEnableSharedTeamVisits = false;
        this.pageParams.vSCEnableVisitDetail = false;
        this.pageParams.vSCEnableCustomerVisitRef = false;
        this.pageParams.vSCShowConsNote = false;
        this.pageParams.vSCShowWED = false;
        this.pageParams.vSCEnableServiceCoverDispLev = false;
        this.pageParams.vSCEnableServiceTime = false;
        this.pageParams.vSCEnableManualVisitReasonCode = false;
        this.pageParams.vLoop = 0;
        this.pageParams.vDeletionWarning = MessageConstant.PageSpecificMessage.msg2668;

        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharEnableEntryOfVisitRefInVisitEntry, //Required
            this.sysCharConstants.SystemCharEnableSharedTeamVisits, //Required
            this.sysCharConstants.SystemCharEnableVisitDetail, //Required
            this.sysCharConstants.SystemCharEnableEntryOfCustomerRefInVisitEntry, //Required
            this.sysCharConstants.SystemCharShowWasteConsNumInVisitEntry, //Required
            this.sysCharConstants.SystemCharEnableWED, //Required
            this.sysCharConstants.SystemCharEnableServiceCoverDisplayLevel, //Required
            this.sysCharConstants.SystemCharEnableServiceStartAndEndTime, //Required
            this.sysCharConstants.SystemCharEnableManualVisitReasonCode //Required
        ];

        let sysCharIP = {
            module: this.xhrParams.module,
            operation: this.xhrParams.operation,
            action: 0,
            businessCode: this.utils.getBusinessCode(),
            countryCode: this.utils.getCountryCode(),
            SysCharList: sysCharList.toString()
        };

        this.speedScript.sysCharPromise(sysCharIP).then((data) => {
            let records = data.records;
            this.pageParams.vSCEnableVisitRef = records[0].Required;
            this.pageParams.vSCEnableSharedTeamVisits = records[1].Required;
            this.pageParams.vSCEnableVisitDetail = records[2].Required;
            this.pageParams.vSCEnableCustomerVisitRef = records[3].Required;
            this.pageParams.vSCShowConsNote = records[4].Required;
            this.pageParams.vSCShowWED = records[5].Required;
            this.pageParams.vSCEnableServiceCoverDispLev = records[6].Required;
            this.pageParams.vSCEnableServiceTime = records[7].Required;
            this.pageParams.vSCEnableManualVisitReasonCode = records[8].Required;
            this.onLoad();
        });
    }

    public onLoad(): any {
        this.isRequesting = false;
        this.pageParams.ServiceDateStartChanged = false;
        this.riExchange.setCurrentContractType();
        this.fnBuildMenuAddOption();

        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));

        this.setControlValue('ServiceVisitRowID', this.riExchange.getParentHTMLValue('ServiceVisitRowID'));
        this.setControlValue('ServiceCoverRowID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
        this.setControlValue('PlanVisitRowID', this.riExchange.getParentHTMLValue('PlanVisitRowID'));
        this.setControlValue('riCacheRefresh', false);

        if (this.isReturning()) {
            if (this.pageParams.isGridHandleRequired) {
                if (this.pageParams.isFormDirty) { this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'VisitTypeCode'); }
            }
            this.setControlValue('riGridHandle', this.formData.riGridHandle);
        } else {
            this.setControlValue('riGridHandle', this.utils.gridHandle);
            this.setControlValue('riCacheRefresh', true);
        }

        this.isLookupNecessary = true;
        if (this.isReturning() && this.pageParams.openPrepUsedMnt) {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.isLookupNecessary = false;
            setTimeout(() => {
                this.pageParams.openPrepUsedMnt = 0; //Temp state
                this.navigate('ServiceVisit', InternalMaintenanceServiceModuleRoutes.ICABSSEPREPUSEDMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.riExchange.getCurrentContractTypeUrlParam(),
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode'),
                    ServiceVisitRowID: this.getControlValue('ServiceVisitRowID')
                });
            }, 1000);
        }
        else if (this.isReturning() && this.pageParams.openPrepUsedMnt === 0) {
            this.pageParams.openPrepUsedMnt = false;
        }
        if (!this.isLookupNecessary) return;

        if (this.pageParams.vSCEnableVisitRef) {
            this.uiDisplay.tdVisitReference = true;
            this.uiDisplay.tdVisitReference2 = true;
        }

        if (this.pageParams.vSCEnableCustomerVisitRef) {
            this.uiDisplay.trCustVisitReference = true;
        }

        if (this.pageParams.vSCEnableServiceTime) {
            this.uiDisplay.trServiceStart = true;
        } else {
            this.uiDisplay.trServiceStart = false;
        }

        let labelStartText: string = 'Start';

        if (this.parentMode === 'ProductSalesDelivery') {
            this.uiDisplay.trOrderQuantity = true;
            this.uiDisplay.trDeliveryQuantity = true;
            this.uiDisplay.trUnDeliveredQuantity = true;
            this.uiDisplay.trServiceEnd = false;

            Observable.forkJoin(
                this.utils.getTranslatedval('Delivery'),
                this.utils.getTranslatedval('Visit Maintenance'),
                this.utils.getTranslatedval('Date'),
                this.utils.getTranslatedval(' Start Time'),
                this.utils.getTranslatedval('Service')
            ).subscribe((e) => {
                this.browserTitle = this.pageTitle = e[0] + ' ' + e[1];
                this.utils.setTitle(this.pageTitle);
                this.labelStartDate = e[0] + ' ' + e[2];
                this.labelStartTime = e[4] + e[3];
            });
        } else {
            this.uiDisplay.trServicedQuantity = true;
            Observable.forkJoin(
                this.utils.getTranslatedval('Service'),
                this.utils.getTranslatedval('Visit Maintenance'),
                this.utils.getTranslatedval('Date'),
                this.utils.getTranslatedval(' Start Time')
            ).subscribe((e) => {
                this.browserTitle = this.pageTitle = e[0] + ' ' + e[1];
                this.utils.setTitle(this.pageTitle);
                this.labelStartDate = e[0] + ' ' + e[2];
                this.labelStartTime = e[0] + e[3];
            });
        }

        this.pageParams.vWasteConsignmentNoteNumber = this.riExchange.getParentHTMLValue('WasteConsignmentNoteNumber');
        if (!this.pageParams.vWasteConsignmentNoteNumber) this.pageParams.vWasteConsignmentNoteNumber = '';
        this.setControlValue('WasteConsignmentNoteNumber', '');

        this.executeGetReq();
    }

    public executeGetReq(): void {
        if (this.parentMode !== 'ProductSalesDelivery') {
            // this.setRequiredStatus('ServicedQuantity', true);
            // this.setRequiredStatus('ServiceDateEnd', true);
        }

        if (this.pageParams.vSCEnableServiceTime) {
            // this.setRequiredStatus('ServiceTimeEnd', true);
        }

        if (this.pageParams.vSCShowWED) {
            this.riExchange.riInputElement.Disable(this.uiForm, 'WEDSQty');
        }

        if (this.pageParams.vSCEnableVisitRef) {
            this.setRequiredStatus('VisitReference', true);
        } else {
            this.setRequiredStatus('VisitReference', false);
        }

        if (this.pageParams.vSCEnableManualVisitReasonCode && (this.parentMode === 'SearchAdd' || this.parentMode === 'ServiceVisitEntryGrid')) {
            this.setRequiredStatus('ManualVisitReasonCode', true);
        } else {
            this.setRequiredStatus('ManualVisitReasonCode', false);
        }

        if (this.parentMode === 'ProductSalesDelivery') {
            this.setRequiredStatus('DeliveryQuantity', true);
            this.riExchange.riInputElement.Disable(this.uiForm, 'UndeliveredQuantity');
        }
        if (this.parentMode === 'ProductSalesDelivery' || this.pageParams.vProdReplacement) {
            this.riExchange.riInputElement.Disable(this.uiForm, 'ServicedQuantity');
        }

        switch (this.parentMode) {
            case 'GroupServiceVisit':
            case 'ConfirmedPlan':
            case 'DespatchGrid':
                if (!this.getControlValue('ServiceVisitRowID')) {
                    this.pageMode = MntConst.eModeAdd;
                } else {
                    this.pageMode = MntConst.eModeUpdate;
                }
                break;
            case 'SearchAdd':
            case 'ProductSalesDelivery':
            case 'CreditMissedService':
            case 'ServiceVisitEntryGrid':
            case 'CallOut':
                this.pageMode = MntConst.eModeAdd;
                break;
            case 'ContactHistory':
                this.pageMode = MntConst.eModeUpdate;
                break;
            default:
                this.pageMode = MntConst.eModeUpdate;
        }

        if (this.pageMode !== MntConst.eModeAdd) {
            //Service Call
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '0');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
            search.set('ContractNumber', this.getControlValue('ContractNumber'));
            search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
            search.set('ProductCode', this.getControlValue('ProductCode'));
            search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
            search.set('ServiceVisitRowID', this.getControlValue('ServiceVisitRowID'));

            this.httpService.xhrGet(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            let tempObj: any = this.controls[this.controlsMap[i]];
                            let isPristine: boolean = true;
                            if (tempObj && tempObj.type === MntConst.eTypeDate) {
                                isPristine = this.uiForm.controls[i].pristine;
                            }

                            this.setControlValue(i, data[i]);

                            if (tempObj && tempObj.type === MntConst.eTypeDate) {
                                if (isPristine) { this.uiForm.controls[i].markAsPristine(); }
                            }
                        }
                    }

                    this.updateCustomDropdowns();
                    if (data.hasOwnProperty('ServiceVisit')) {
                        this.setControlValue('ServiceVisitRowID', data.ServiceVisit);
                        this.tempServiceVisitRowID = data.ServiceVisit;
                    }
                    this.doLookup();
                    this.riMntAfterFetch();
                }
            });
        } else {
            this.doLookup();
        }

        if (this.parentMode === 'ProductSalesDelivery') {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'GetProductSaleDetails';
            formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    this.setControlValue('VisitTypeCode', data['VisitTypeCode']);
                    this.setControlValue('VisitTypeDesc', data['VisitTypeDesc']);
                    this.setControlValue('ServiceTimeStart', data['ServiceTimeStart']);

                    let isPristine: boolean = this.uiForm.controls['ServiceDateStart'].pristine;
                    this.setControlValue('ServiceDateStart', data['ServiceDateStart']);
                    if (isPristine) { this.uiForm.controls['ServiceDateStart'].markAsPristine(); }
                }
            });
        }

        this.riTab.TabsToNormal();

        //RiMaintenance Mode execution
        setTimeout(() => {
            this.riMntBeforeMode();
            if (this.pageMode === MntConst.eModeUpdate) {
                this.fnSetVariables();
                //Update or Delete
                this.riExchange.riInputElement.Enable(this.uiForm, 'menu');
                this.riMntBeforeUpdate();
                this.riExchangeCBORequest();
            } else {
                //Add
                this.riExchange.riInputElement.Disable(this.uiForm, 'menu');
                this.riMntBeforeAddMode();
            }
        }, 1000);
    }

    public doLookup(): any {
        let lookupIP: Array<any> = [];
        this.lookUpRefs.Contract.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.Contract.query.ContractNumber = this.getControlValue('ContractNumber');
        if (this.lookUpRefs.Contract.query.ContractNumber) lookupIP.push(this.lookUpRefs.Contract);

        this.lookUpRefs.Premise.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.Premise.query.ContractNumber = this.getControlValue('ContractNumber');
        this.lookUpRefs.Premise.query.PremiseNumber = this.getControlValue('PremiseNumber');
        if (this.lookUpRefs.Premise.query.PremiseNumber) lookupIP.push(this.lookUpRefs.Premise);

        this.lookUpRefs.Product.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.Product.query.ProductCode = this.getControlValue('ProductCode');
        if (this.lookUpRefs.Product.query.ProductCode) lookupIP.push(this.lookUpRefs.Product);

        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            if (data) {
                this.utils.setLookupValue(this, this.lookUpRefs.Contract.fields, data[0]);
                this.utils.setLookupValue(this, this.lookUpRefs.Premise.fields, data[1]);
                this.utils.setLookupValue(this, this.lookUpRefs.Product.fields, data[2]);
                this.updateEllipsisParams();
            }
        });

        let lookupIP1: Array<any> = [];
        this.lookUpRefs.VisitType.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.VisitType.query.VisitTypeCode = this.getControlValue('VisitTypeCode');
        lookupIP1.push(this.lookUpRefs.VisitType);

        this.lookUpRefs.VisitNarrative.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.VisitNarrative.query.VisitNarrativeCode = this.getControlValue('VisitNarrativeCode');
        lookupIP1.push(this.lookUpRefs.VisitNarrative);

        this.lookUpRefs.Employee.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.Employee.query.EmployeeCode = this.getControlValue('EmployeeCode');
        lookupIP1.push(this.lookUpRefs.Employee);

        this.lookUpRefs.ManualVisitReason.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.ManualVisitReason.query.ManualVisitReasonCode = this.getControlValue('ManualVisitReasonCode');
        lookupIP1.push(this.lookUpRefs.ManualVisitReason);

        this.LookUp.lookUpPromise(lookupIP1).then((data) => {
            if (data) {
                let tempVisitTypeDesc: string = this.utils.setLookupValue(this, this.lookUpRefs.VisitType.fields, data[0]);
                if (tempVisitTypeDesc) {
                    this.dropDown.VisitTypeCode.active = {
                        id: this.lookUpRefs.VisitType.query.VisitTypeCode,
                        text: this.lookUpRefs.VisitType.query.VisitTypeCode + ' - ' + tempVisitTypeDesc
                    };
                }

                let tempVisitNarrativeDesc: string = this.utils.setLookupValue(this, this.lookUpRefs.VisitNarrative.fields, data[1]);
                if (tempVisitNarrativeDesc) {
                    this.dropDown.visitNarrativeSearch.active = {
                        id: this.lookUpRefs.VisitNarrative.query.VisitNarrativeCode,
                        text: this.lookUpRefs.VisitNarrative.query.VisitNarrativeCode + ' - ' + tempVisitNarrativeDesc
                    };
                }

                if (this.lookUpRefs.Employee.query.EmployeeCode) this.utils.setLookupValue(this, this.lookUpRefs.Employee.fields, data[2]);

                let tempManualVisitReasonDesc: string = this.utils.setLookupValue(this, this.lookUpRefs.ManualVisitReason.fields, data[3]);
                if (tempManualVisitReasonDesc) {
                    this.dropDown.manualVisitReasonSearch.active = {
                        id: this.lookUpRefs.ManualVisitReason.query.ManualVisitReasonCode,
                        text: this.lookUpRefs.ManualVisitReason.query.ManualVisitReasonCode + ' - ' + tempManualVisitReasonDesc
                    };
                }
            }
        });

        let lookupIP2: Array<any> = [];
        this.lookUpRefs.NoSignatureReason.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.NoSignatureReason.query.ReasonNumber = this.getControlValue('ReasonNumber');
        lookupIP2.push(this.lookUpRefs.NoSignatureReason);

        this.lookUpRefs.NoServiceReason.query.BusinessCode = this.utils.getBusinessCode();
        this.lookUpRefs.NoServiceReason.query.NoServiceReasonCode = this.getControlValue('NoServiceReasonCode');
        lookupIP2.push(this.lookUpRefs.NoServiceReason);

        this.LookUp.lookUpPromise(lookupIP2).then((data) => {
            if (data) {
                let tempReasonNumberDesc: string = this.utils.setLookupValue(this, this.lookUpRefs.NoSignatureReason.fields, data[0]);
                if (tempReasonNumberDesc) {
                    this.dropDown.noSignatureReasonSearch.active = {
                        id: this.lookUpRefs.NoSignatureReason.query.ReasonNumber,
                        text: this.lookUpRefs.NoSignatureReason.query.ReasonNumber + ' - ' + tempReasonNumberDesc
                    };
                }

                let tempNoServiceReasonDesc: string = this.utils.setLookupValue(this, this.lookUpRefs.NoServiceReason.fields, data[1]);
                if (tempNoServiceReasonDesc) {
                    this.dropDown.noServiceReasonSearch.active = {
                        id: this.lookUpRefs.NoServiceReason.query.NoServiceReasonCode,
                        text: this.lookUpRefs.NoServiceReason.query.NoServiceReasonCode + ' - ' + tempNoServiceReasonDesc
                    };
                }
            }

            this.updateEllipsisParams();
        });
    }

    public fnSetVariables(): any {
        // Check to see if Quantites/Shared Ind/Preps/Infestations are required for this Business/Product;
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'GetVariables';
        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                this.pageParams.vReqQuantity = (data['ReqQuantity'] === 'yes') ? true : false;
                this.pageParams.vReqPreps = (data['ReqPreps'] === 'yes') ? true : false;
                this.pageParams.vReqInfestations = (data['ReqInfestations'] === 'yes') ? true : false;
                this.pageParams.vReqDeliveryNoteNumber = (data['ReqDeliveryNoteNumber'] === 'yes') ? true : false;
                this.pageParams.vWasteCollected = (data['WasteCollected'] === 'yes') ? true : false;
                this.pageParams.vVisitTypeNarrative = (data['VisitTypeNarrative'] === 'yes') ? true : false;
                this.pageParams.vReqWorkLoadIndex = (data['ReqWorkLoadIndex'] === 'yes') ? true : false;
                this.pageParams.vProdReplacement = (data['ProdReplacement'] === 'yes') ? true : false;
                this.pageParams.vRoutineVisit = (data['RoutineVisit'] === 'yes') ? true : false;
                this.pageParams.vReviewDisplays = (data['ReviewDisplays'] === 'yes') ? true : false;
                this.setControlValue('ServiceCoverRowID', data['ServiceCoverRowID']);

                this.enableAllCustomDropdowns();
                if (this.pageParams.vProdReplacement && this.pageMode === MntConst.eModeUpdate) {
                    this.isUpdateEnabled = false;
                    setTimeout(() => { this.disableCtrlsforUpdate(); }, 500);
                } else {
                    this.isUpdateEnabled = true;
                }

                if (this.pageParams.vReqQuantity || this.pageParams.vProdReplacement) {
                    this.uiDisplay.trServicedQuantity = true;
                    if (this.pageParams.vSCEnableServiceCoverDispLev && this.pageParams.vReviewDisplays) {
                        this.uiDisplay.tdReviewDisplays = true;
                    } else {
                        this.uiDisplay.tdReviewDisplays = false;
                    }
                } else {
                    this.uiDisplay.trServicedQuantity = false;
                    this.uiDisplay.tdReviewDisplays = false;
                }

                if (this.pageParams.vWasteCollected) {
                    this.uiDisplay.trWasteCollected = true;
                } else {
                    this.uiDisplay.trWasteCollected = false;
                }

                if (this.pageParams.vReqPreps && this.getControlValue('PrepUsedInd', true)) {
                    this.uiDisplay.trPrepUsed = true;
                    this.fnBuildMenuAddOption();
                } else if (this.pageParams.vReqPreps) {
                    this.uiDisplay.trPrepUsed = true;
                } else {
                    this.uiDisplay.trPrepUsed = false;
                }

                if (this.pageParams.vReqInfestations && this.getControlValue('InfestationInd', true)) {
                    this.uiDisplay.trInfestation = true;
                    this.fnBuildMenuAddOption();
                } else if (this.pageParams.vReqInfestations) {
                    this.uiDisplay.trInfestation = true;
                } else {
                    this.uiDisplay.trInfestation = false;
                }

                if (this.pageParams.vReqDeliveryNoteNumber) {
                    this.uiDisplay.trDeliveryNoteNumber = true;
                } else {
                    this.uiDisplay.trDeliveryNoteNumber = false;
                }

                if (this.pageParams.vVisitTypeNarrative) {
                    this.setRequiredStatus('VisitNarrativeCode', true);
                } else {
                    this.setRequiredStatus('VisitNarrativeCode', false);
                }

                if (this.pageParams.vReqWorkLoadIndex) {
                    this.uiDisplay.trWorkLoadIndex = true;
                } else {
                    this.uiDisplay.trWorkLoadIndex = false;
                }

                if (this.pageParams.vProdReplacement) {
                    this.uiDisplay.trServiceCoverItemNumber = true;
                    this.uiDisplay.trPremiseLocationNumber = true;
                    this.uiDisplay.trProductComponentCode = true;
                    this.uiDisplay.trProductComponentRemoved = true;
                    this.uiDisplay.trRemovalQuantity = true;
                    this.uiDisplay.trInvoiceUnitValue = true;
                    this.uiDisplay.trAdditionalChargeValue = true;
                } else {
                    this.uiDisplay.trServiceCoverItemNumber = false;
                    this.uiDisplay.trPremiseLocationNumber = false;
                    this.uiDisplay.trProductComponentCode = false;
                    this.uiDisplay.trProductComponentRemoved = false;
                    this.uiDisplay.trRemovalQuantity = false;
                    this.uiDisplay.trInvoiceUnitValue = false;
                    this.uiDisplay.trAdditionalChargeValue = false;
                }

                if (this.pageParams.vSCShowWED && this.pageParams.vRoutineVisit) {
                    this.uiDisplay.trWEDSQty = true;
                } else {
                    this.uiDisplay.trWEDSQty = false;
                }

                ///default to zero;
                this.setControlValue('AdditionalEmployees', 0);
                if (this.pageParams.vProdReplacement) {
                    ///Get List of dummy product codes  &&  Single Qty Components;
                    this.ajaxSource.next(this.ajaxconstant.START);
                    search = new QueryParams();
                    search.set(this.serviceConstants.Action, '6');
                    search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
                    search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

                    let formData: any = {};
                    formData['Function'] = 'DummyProductCodeList';
                    formData['ProductCode'] = this.getControlValue('ProductCode');
                    this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.pageParams.vbDummyProductCodes = data['DummyProductCodes'].split('|');

                        let tempProductComponentCode: string = this.getControlValue('ProductComponentCode');
                        if (this.getControlValue('ProductComponentCode')) {
                            this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
                            for (let vbProductCode in this.pageParams.vbDummyProductCodes) {
                                if (this.pageParams.vbDummyProductCodes.hasOwnProperty(vbProductCode)) {
                                    if (tempProductComponentCode.toUpperCase() === this.pageParams.vbDummyProductCodes[vbProductCode].toUpperCase()) {
                                        this.riExchange.riInputElement.Enable(this.uiForm, 'ProductComponentDesc');
                                        break;
                                    }
                                }
                            }
                        } else {
                            this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
                            this.setControlValue('ProductComponentDesc', '');
                        }
                    });
                }
            }
        });
    }

    public fnCheckServiceVisitValid(): void {
        if (this.getControlValue('VisitTypeCode') !== '' && this.getControlValue('ServiceDateStart') !== '') {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'CheckServiceVisitsValid';
            formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
            formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
            formData['PlanVisitNumber'] = this.getControlValue('PlanVisitNumber');
            formData['ServiceDateStart'] = this.getControlValue('ServiceDateStart');
            formData['ProductComponentCode'] = this.getControlValue('ProductComponentCode');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                }
            });
        }
    }

    public fnValidateVisitDate(): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'ValidateVisitDate';
        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        formData['ServiceDateStart'] = this.getControlValue('ServiceDateStart');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                if (data.MessageDesc) {
                    this.globalNotifications.displayMessage(data.MessageDesc,
                        CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                }
            }
        });
    }

    public fnGetDefaultVisitReference(callBkFn?: any): any {
        if (this.pageParams.ServiceDateStartChanged && this.getControlValue('ServiceDateStart') !== '') {
            this.fnValidateVisitDate();
            this.fnCheckServiceVisitValid();
            if (this.pageParams.vSCEnableVisitRef && this.getControlValue('ServiceDateStart') !== '') {
                this.ajaxSource.next(this.ajaxconstant.START);
                let search: QueryParams = new QueryParams();
                search.set(this.serviceConstants.Action, '6');
                search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
                search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

                let formData: any = {};
                formData['Function'] = 'GetDefaultVisitReference';
                formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
                formData['ServiceDateStart'] = this.getControlValue('ServiceDateStart');
                formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
                this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.globalNotifications.displayMessage(data);
                    } else {
                        // this.riExchange.riInputElement.Enable(this.uiForm, 'VisitReference');
                        if (data['VisitReference'] !== '') {
                            this.setControlValue('VisitReference', data['VisitReference']);
                            // this.riExchange.riInputElement.Disable(this.uiForm, 'VisitReference');
                        }

                        if (callBkFn && typeof callBkFn === 'function') {
                            callBkFn.call(this);
                        }
                    }
                });
            } else {
                if (callBkFn && typeof callBkFn === 'function') {
                    callBkFn.call(this);
                }
            }
        } else {
            if (callBkFn && typeof callBkFn === 'function') {
                callBkFn.call(this);
            }
        }
    }

    public fnCheckComponentExchange(callBkFn?: any): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'CheckComponentExchange';
        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            }

            if (callBkFn && typeof callBkFn === 'function') {
                callBkFn.call(this);
            }
        });
    }

    public fnCheckExistingServiceVisit(callBkFn: any): any {
        if (this.pageParams.vSCEnableServiceTime) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'CheckExistingServiceVisit';
            formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
            formData['EmployeeCode'] = this.getControlValue('EmployeeCode');
            formData['ServiceDateStart'] = this.getControlValue('ServiceDateStart');
            formData['ServiceTimeStart'] = this.getControlValue('ServiceTimeStart');
            formData['ServiceTimeEnd'] = this.getControlValue('ServiceTimeEnd');
            if (this.pageMode === MntConst.eModeUpdate) {
                formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
            }

            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    if (callBkFn && typeof callBkFn === 'function') {
                        if (data.MessageDesc) {
                            this.globalNotifications.displayMessage(data.MessageDesc,
                                CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                        }
                        callBkFn.call(this);
                    }
                }
            });
        } else {
            if (callBkFn && typeof callBkFn === 'function') {
                callBkFn.call(this);
            }
        }
    }

    public fnCheckWasteConsignmentNumberRequired(): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'CheckWasteConsignmentNumberRequired';
        formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                this.setControlValue('WasteConsNoteIsMandatory', (data['WasteConsignmentNumberRequired'] === 'yes') ? true : false);
                this.fnSetEdittableFields();
            }
        });
    }

    public fnFindRelatedPlanVisit(): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'FindRelatedPlanVisit';
        formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        if (this.pageMode === MntConst.eModeUpdate) {
            formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
        }
        if (this.parentMode === 'SearchAdd' && this.pageParams.vWasteConsignmentNoteNumber !== '') {
            formData['WasteConsignmentNoteNumber'] = this.pageParams.vWasteConsignmentNoteNumber;
        }
        if (this.getControlValue('WasteConsignmentNoteNumber') !== '' && this.pageParams.vWasteConsignmentNoteNumber === '') {
            formData['WasteConsignmentNoteNumber'] = this.getControlValue('WasteConsignmentNoteNumber');
        }
        formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                for (let i in data) {
                    if (data.hasOwnProperty(i)) {
                        this.setControlValue(i, data[i]);
                    }
                }
                this.updateCustomDropdowns();
            }
        });
    }

    public fnBuildMenuAddOption(): void {
        let menuOptions: any = [];
        menuOptions.push({ value: 'PlanVisits', label: 'Planned Visits' });
        if (this.pageParams.vSCEnableVisitDetail) {
            menuOptions.push({ value: 'VisitDetail', label: 'Visit Details' });
        }

        if (this.getControlValue('PrepUsedInd', true) && this.pageMode !== MntConst.eModeAdd) {
            menuOptions.push({ value: 'PrepUsed', label: 'Preps Used' });
        }
        if (this.getControlValue('InfestationInd', true) && this.pageMode !== MntConst.eModeAdd) {
            menuOptions.push({ value: 'Infestation', label: 'Infestations' });
        }

        //Issue with ForkJoin on these particular functions and hence applying alternate solution
        this.subUserAccess = this.utils.getUserAccessType().subscribe(data => {
            if (data === 'Full') {
                menuOptions.push({ value: 'ProRataCharges', label: 'Pro Rata Charges' });
            } else {
                this.subLoggedInBranch = this.utils.getLoggedInBranch().subscribe(data => {
                    if (data && data.results[0]) {
                        let branchNumber: number = data.results[0][0]['BranchNumber'];
                        if (this.getControlValue('ServiceBranchNumber') === branchNumber || this.getControlValue('NegBranchNumber') === branchNumber) {
                            menuOptions.push({ value: 'ProRataCharges', label: 'Pro Rata Charges' });
                        }
                    }
                });
            }
        });

        this.dropDown.menu = menuOptions;
        this.setControlValue('menu', 'Options');
    }

    public fnCheckServiceVisitQty(): any {
        if (this.pageParams.vSCEnableServiceCoverDispLev && this.pageParams.vReviewDisplays) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let vServiceVisitMode: string = '';
            if (this.pageMode === MntConst.eModeAdd) {
                vServiceVisitMode = 'AddServiceVisit';
            } else if (this.pageMode === MntConst.eModeUpdate) {
                vServiceVisitMode = 'UpdateServiceVisit';
            }

            let formData: any = {};
            formData['Function'] = 'CheckServiceVisitQty';
            formData['ServiceVisitQty'] = this.getControlValue('ServicedQuantity');
            formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
            formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
            formData['riGridHandle'] = this.getControlValue('riGridHandle');
            formData['ServiceVisitMode'] = vServiceVisitMode;
            if (this.getControlValue('ServiceVisitRowID') !== '') {
                formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
            }
            // check the service visit qty matches the number of displays assigned to the visit
            return this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    //TODO this.ServicedQuantity.focus();
                    this.modalAdvService.emitPrompt(
                        new ICabsModalVO(this.promptContent, null, this.riMntAfterSave.bind(this), null)
                            .setShiftTop(true));
                }
            });
        } else {
            // this.modalAdvService.emitPrompt( new ICabsModalVO( this.promptContent, null, this.confirmed.bind(this), this.promptCancel.bind(this)));
            this.modalAdvService.emitPrompt(
                new ICabsModalVO(this.promptContent, null, this.riMntAfterSave.bind(this), null)
                    .setShiftTop(true)
            );
        }
    }

    public fnSetDisableServiceQty(): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'SetDisableServiceQty';
        formData['ProductCode'] = this.getControlValue('ProductCode');

        return this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                if (data['ProductDisplayLevel'] === 'yes') {
                    this.riExchange.riInputElement.Disable(this.uiForm, 'ServicedQuantity');
                }
            }
        });
    }

    public generateAdditionAlEmpObj(): void {
        let len = this.getControlValue('AdditionalEmployees');
        if (len > 10) {
            len = 10;
            this.setControlValue('AdditionalEmployees', 10);
        }
        if (len < 0) {
            len = 0;
            this.setControlValue('AdditionalEmployees', 0);
        }
        this.arrAdditionalEmployees = [];
        for (let i = 0; i < 10; i++) {
            this.setRequiredStatus('AddEmployeeCode' + (i + 1), false);
        }
        if (len > 0) {
            this.setControlValue('SharedInd', true);
        }
        for (let i = 0; i < len; i++) {
            this.arrAdditionalEmployees.push({ code: 'AddEmployeeCode' + (i + 1), desc: 'AddEmployeeSurname' + (i + 1), index: (i + 1) });
            this.setRequiredStatus('AddEmployeeCode' + (i + 1), true);
        }
    }

    //No reference found
    public fnChangeStandardDuration(): any {
        let iStartTime: number = 0;
        let iEndTime: number = 0;
        let iDuration: number = 0;
        if (!this.riExchange.riInputElement.isError(this.uiForm, 'ServiceTimeStart') && !this.riExchange.riInputElement.isError(this.uiForm, 'ServiceTimeEnd')) {
            iStartTime = this.getControlValue('ServiceTimeStart');
            iEndTime = this.getControlValue('ServiceTimeEnd');
            if (iStartTime !== 0 && iEndTime !== 0) {
                iDuration = iEndTime - iStartTime;
                this.setControlValue('StandardDuration', iDuration);
                // this.riExchange.riInputElement.isErrorEx('StandardDuration', false, false);
            }
        }
    }

    public fnCheckServiceVisitProRataCharge(): any {
        this.ajaxSource.next(this.ajaxconstant.START);
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};
        formData['Function'] = 'CheckServiceVisitProRataCharge';
        formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisit');

        return this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                if (data.DisplayMessage) {
                    this.modalAdvService.emitPrompt(
                        new ICabsModalVO(MessageConstant.PageSpecificMessage.msgVisitMnt, null, this.fnDeleteRecord.bind(this), null)
                            .setShiftTop(true));
                } else {
                    this.fnDeleteRecord();
                }
            }
        });
    }

    public fnDeleteRecord(): any {
        if (this.pageMode === MntConst.eModeDelete) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '3');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Table'] = 'ServiceVisit';
            formData['ROWID'] = this.getControlValue('ServiceVisitRowID');

            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.markAsPristine();
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    this.variableService.setBackClick(true);
                    this.location.back();
                }
            });
        }
    }

    public fnSetEdittableFields(): any {
        this.setRequiredStatus('WasteConsignmentNoteNumber', false);
        if (this.pageParams.vSCShowConsNote) {
            if (this.getControlValue('WasteConsNoteIsMandatory', true)) {
                this.uiDisplay.trWasteConsignmentNoteNumber = true;
                this.setRequiredStatus('WasteConsignmentNoteNumber', true);
                if (this.parentMode === 'SearchAdd') {
                    this.setControlValue('WasteConsignmentNoteNumber', this.riExchange.getParentHTMLValue('WasteConsignmentNoteNumber'));
                    if (this.getControlValue('WasteConsignmentNoteNumber')) {
                        this.riExchange.riInputElement.Disable(this.uiForm, 'WasteConsignmentNoteNumber');
                    } else {
                        this.riExchange.riInputElement.Enable(this.uiForm, 'WasteConsignmentNoteNumber');
                    }
                }
            }
            else {
                this.uiDisplay.trWasteConsignmentNoteNumber = false;
            }
        }

        if (this.pageParams.vSCEnableManualVisitReasonCode &&
            (this.parentMode === 'SearchAdd' || this.parentMode === 'ServiceVisitEntryGrid' || this.getControlValue('ManualVisitInd', true))) {
            this.setRequiredStatus('ManualVisitReasonCode', true);
            this.uiDisplay.trManualVisitReason = true;
        } else {
            this.setRequiredStatus('ManualVisitReasonCode', false);
            this.uiDisplay.trManualVisitReason = false;
        }
        this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactReason');
        /// Based on the Visit Detail checkbox value, set the properties of various fields;

        let flag: boolean = this.getControlValue('ServiceVisitDetail', true);
        if (flag) {
            this.riExchange.riInputElement.Disable(this.uiForm, 'EmployeeCode');
            this.setRequiredStatus('EmployeeCode', false);

            this.riExchange.riInputElement.Disable(this.uiForm, 'StandardDuration');
            this.setRequiredStatus('StandardDuration', false);

            this.riExchange.riInputElement.Disable(this.uiForm, 'OvertimeDuration');
            this.setRequiredStatus('OvertimeDuration', false);

            let isPristineA: boolean = this.uiForm.controls['ServiceDateStart'].pristine;
            this.riExchange.riInputElement.Disable(this.uiForm, 'ServiceDateStart');
            this.setRequiredStatus('ServiceDateStart', false);
            if (isPristineA) { this.uiForm.controls['ServiceDateStart'].markAsPristine(); }

            let isPristineA1: boolean = this.uiForm.controls['ServiceDateEnd'].pristine;
            this.riExchange.riInputElement.Disable(this.uiForm, 'ServiceDateEnd');
            if (isPristineA1) { this.uiForm.controls['ServiceDateEnd'].markAsPristine(); }

            this.riExchange.riInputElement.Disable(this.uiForm, 'ServicedQuantity');
            this.riExchange.riInputElement.Disable(this.uiForm, 'AdditionalEmployees');
            this.riExchange.riInputElement.Disable(this.uiForm, 'ServiceQuantity');
            this.riExchange.riInputElement.Disable(this.uiForm, 'UndeliveredQuantity');
            this.riExchange.riInputElement.Disable(this.uiForm, 'DeliveryQuantity');
            this.riExchange.riInputElement.Disable(this.uiForm, 'WorkLoadIndexTotal');
            this.riExchange.riInputElement.Disable(this.uiForm, 'ReasonNumber');
            this.riExchange.riInputElement.Disable(this.uiForm, 'PremiseContactName');

            let len = this.getControlValue('AdditionalEmployees');
            for (let i = 0; i < len; i++) {
                this.riExchange.riInputElement.Disable(this.uiForm, 'AddEmployeeCode' + (i + 1));
            }
        } else {
            this.riExchange.riInputElement.Enable(this.uiForm, 'EmployeeCode');
            this.setRequiredStatus('EmployeeCode', true);

            this.riExchange.riInputElement.Enable(this.uiForm, 'StandardDuration');
            this.setRequiredStatus('StandardDuration', true);

            this.riExchange.riInputElement.Enable(this.uiForm, 'OvertimeDuration');
            this.setRequiredStatus('OvertimeDuration', true);

            let isPristineB: boolean = this.uiForm.controls['ServiceDateStart'].pristine;
            this.riExchange.riInputElement.Enable(this.uiForm, 'ServiceDateStart');
            this.setRequiredStatus('ServiceDateStart', true);
            if (isPristineB) { this.uiForm.controls['ServiceDateStart'].markAsPristine(); }

            let isPristineB1: boolean = this.uiForm.controls['ServiceDateEnd'].pristine;
            this.riExchange.riInputElement.Enable(this.uiForm, 'ServiceDateEnd');
            if (isPristineB1) { this.uiForm.controls['ServiceDateEnd'].markAsPristine(); }

            this.riExchange.riInputElement.Enable(this.uiForm, 'ProductComponentCode');
            this.riExchange.riInputElement.Enable(this.uiForm, 'AlternateProductCode');
            this.riExchange.riInputElement.Enable(this.uiForm, 'AdditionalEmployees');
            this.riExchange.riInputElement.Enable(this.uiForm, 'ServicedQuantity');
            this.riExchange.riInputElement.Enable(this.uiForm, 'UndeliveredQuantity');
            this.riExchange.riInputElement.Enable(this.uiForm, 'DeliveryQuantity');
            this.riExchange.riInputElement.Enable(this.uiForm, 'ServiceTimeStart');
            this.riExchange.riInputElement.Enable(this.uiForm, 'ServiceTimeEnd');
            this.riExchange.riInputElement.Enable(this.uiForm, 'WorkLoadIndexTotal');

            let len = this.getControlValue('AdditionalEmployees');
            for (let i = 0; i < len; i++) {
                this.riExchange.riInputElement.Enable(this.uiForm, 'AddEmployeeCode' + (i + 1));
            }
        }
        if (this.pageMode === MntConst.eModeUpdate) {
            if (this.pageParams.vSCEnableServiceCoverDispLev) {
                this.fnSetDisableServiceQty();
            }
        }
    }

    public onloadPremiseContactSignature(): any {
        if (this.getControlValue('PremiseContactSignatureURL') !== '') {
            this.uiDisplay.trSignature = true;
        }
    }

    public defaultToUpdateMode(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.markAsPristine();
        if (!this.getControlValue('ServiceVisitRowID')) this.setControlValue('ServiceVisitRowID', this.tempServiceVisitRowID);
        if (!this.getControlValue('ServiceVisitRowID')) {
            this.variableService.setBackClick(true);
            this.location.back();
            return;
        }
        //Page mode converted to Update mode after Add is Successful
        this.pageMode = MntConst.eModeUpdate;
        this.parentMode = 'Summary';

        let urlStrArr: any = this.location.path().split('?');
        let urlStr: any = urlStrArr[0];
        let qParams: any = 'parentMode=' + this.parentMode
            + '&ContractNumber=' + this.getControlValue('ContractNumber')
            + '&PremiseNumber=' + this.getControlValue('PremiseNumber')
            + '&ProductCode=' + this.getControlValue('ProductCode')
            + '&ServiceCoverRowID=' + this.getControlValue('ServiceCoverRowID')
            + '&ServiceVisitRowID=' + this.getControlValue('ServiceVisitRowID');
        this.location.replaceState(urlStr, qParams);

        this.executeGetReq();
    }

    public riMntBeforeDelete(): any {
        if (this.getControlValue('SharedInd', true)) {
            setTimeout(() => {
                this.globalNotifications.displayMessage(MessageConstant.PageSpecificMessage.msgSharedVisit,
                    CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
            }, 500);
        }
        this.fnCheckComponentExchange(this.fnCheckServiceVisitProRataCharge);
    }

    public riMntBeforeSaveAdd(): any {
        this.fnGetDefaultVisitReference(this.riMntBeforeConfirm);
    }

    public riExchangeCBORequest(): any {
        if (this.pageParams.vVisitTypeNarrative) {
            this.setRequiredStatus('VisitNarrativeCode', true);
        } else {
            this.setRequiredStatus('VisitNarrativeCode', false);
        }
        if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'VisitTypeCode') && this.getControlValue('VisitTypeCode') !== '') {
            this.fnFindRelatedPlanVisit();
            this.fnSetVariables();
            let iAdditionalEmployees: number = 0;
            this.fnCheckServiceVisitValid();
            this.fnCheckWasteConsignmentNumberRequired();
        }
        if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'PlanVisitNumber') && this.getControlValue('PlanVisitNumber') !== '') {
            this.onChangePlanVisitNumber();
        }
        if (this.riExchange.riInputElement.HasChanged(this.uiForm, 'WasteConsignmentNoteNumber')) {
            this.fnFindRelatedPlanVisit();
        }
    }
    public onChangePlanVisitNumber(): void {
        if (this.getControlValue('PlanVisitNumber') !== '') {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'GetDetailsFromPlanVisit';
            formData['Mode'] = this.parentMode;
            formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
            formData['PlanVisitNumber'] = this.getControlValue('PlanVisitNumber');
            formData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data,
                        CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                }
            });
        }
    }

    public riMntBeforeUpdate(): any {
        this.setControlValue('ServiceVisitDetail', this.pageParams.vSCEnableVisitDetail);
        let tempProductComponentCode: string = this.getControlValue('ProductComponentCode');
        if (tempProductComponentCode !== '') {
            this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
            for (let vbProductCode in this.pageParams.vbDummyProductCodes) {
                if (this.pageParams.vbDummyProductCodes.hasOwnProperty(vbProductCode)) {
                    if (tempProductComponentCode.toUpperCase() === this.pageParams.vbDummyProductCodes[vbProductCode].toUpperCase()) {
                        this.riExchange.riInputElement.Enable(this.uiForm, 'ProductComponentDesc');
                        break;
                    }
                }
            }
        } else {
            this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
            this.setControlValue('ProductComponentDesc', '');
        }
        this.fnSetEdittableFields();
    }

    public riMntBeforeAddMode(): any {
        this.setControlValue('ServiceTimeStart', 0);
        this.setControlValue('ServiceTimeEnd', 0);
        this.setControlValue('StandardDuration', 60);
        this.setControlValue('OvertimeDuration', 0);

        /// If adding, no ServiceVisitDetail will be present;
        this.setControlValue('ServiceVisitDetail', false);

        /// If adding, Service End Date will always be blank,  &&  so hide.;
        this.uiDisplay.trServiceEnd = false;

        ///hide the Review Displays button until a visit type code has been entered;
        this.uiDisplay.tdReviewDisplays = false;

        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

        let formData: any = {};

        switch (this.parentMode) {
            case 'ProductSalesDelivery':
                this.setControlValue('VisitTypeCode', this.getControlValue('VisitTypeCode'));
                this.setControlValue('VisitTypeDesc', this.getControlValue('VisitTypeDesc'));
                this.setControlValue('ServiceTimeStart', this.getControlValue('ServiceTimeStart'));

                let isPristine: boolean = this.uiForm.controls['ServiceDateStart'].pristine;
                this.setControlValue('ServiceDateStart', this.getControlValue('ServiceDateStart'));
                if (isPristine) { this.uiForm.controls['ServiceDateStart'].markAsPristine(); }
                break;
            case 'CreditMissedService':
                formData['Function'] = 'GetCreditMissedServiceDefaults';
                formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
                formData['ProRataChargeRowID'] = this.getControlValue('ProRataChargeRowID');
                break;
            case 'GroupServiceVisit':
            case 'DespatchGrid':
            case 'ConfirmedPlan':
                formData['Function'] = 'GetPlanVisitDefaultsNoDates';
                formData['PlanVisitRowID'] = this.getControlValue('PlanVisitRowID');
                break;
            case 'ServiceVisitEntryGrid':
                formData['Function'] = 'GetServiceCoverDetails';
                formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
                break;
        }
        this.logger.log('Debug >>', this.parentMode, this.getControlValue('ServiceBranchNumber'), formData.hasOwnProperty('Function'), formData);
        if (!this.getControlValue('ServiceBranchNumber')) {
            this.fnGetDataFromServiceCover();
        }
        if (formData.hasOwnProperty('Function')) {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            let tempObj: any = this.controls[this.controlsMap[i]];
                            let isPristine: boolean = true;
                            if (tempObj && tempObj.type === MntConst.eTypeDate) {
                                isPristine = this.uiForm.controls[i].pristine;
                            }

                            this.setControlValue(i, data[i]);

                            if (tempObj && tempObj.type === MntConst.eTypeDate) {
                                if (isPristine) { this.uiForm.controls[i].markAsPristine(); }
                            }
                        }
                    }
                    this.updateControls();
                    this.updateCustomDropdowns();
                    this.fnSetEdittableFields();
                    this.fnSetVariables();
                }
            });
        } else {
            this.fnSetVariables();
        }
    }

    public updateControls(): void {
        for (let i in this.pageParams.cachedData) {
            if (this.pageParams.cachedData.hasOwnProperty(i)) {
                let currVal: any = this.uiForm.controls[i].value;
                let oldVal: any = this.pageParams.cachedData[i].value;
                if (currVal !== oldVal) {
                    let fields = `ServicedQuantity, riCacheRefresh`;
                    fields = fields.replace(/\\t/g, '').replace(/\r?\n|\r/g, '').replace(/ /g, '');
                    let skipIDs = fields.split(',');

                    if (i && skipIDs.indexOf(i) === -1) {
                        this.setControlValue(i, oldVal);
                    }
                    this.riExchange.riInputElement.MarkAsDirty(this.uiForm, i);
                }
            }
        }
        this.pageParams.cachedData = {};
    }

    public fnGetDataFromServiceCover(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let formData: any = {};
        let search: QueryParams = new QueryParams();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        formData['Function'] = 'GetServiceCoverDetails';
        formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (data.hasError) {
                this.globalNotifications.displayMessage(data);
            } else {
                for (let i in data) {
                    if (data.hasOwnProperty(i)) {
                        let tempObj: any = this.controls[this.controlsMap[i]];
                        let isPristine: boolean = true;
                        if (tempObj && tempObj.type === MntConst.eTypeDate) {
                            isPristine = this.uiForm.controls[i].pristine;
                        }

                        this.setControlValue(i, data[i]);

                        if (tempObj && tempObj.type === MntConst.eTypeDate) {
                            if (isPristine) { this.uiForm.controls[i].markAsPristine(); }
                        }
                    }
                }
                this.updateControls();
                this.updateCustomDropdowns();
                this.fnSetEdittableFields();
                this.fnSetVariables();
            }
        });
    }

    public riMntBeforeConfirm(): any {
        setTimeout(() => {
            this.fnCheckServiceVisitQty();
        }, 500);
    }

    public riMntBeforeSave(callBkFn?: any): any {
        this.fnCheckExistingServiceVisit(() => {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Mode'] = this.parentMode;
            formData['Pattern'] = 'all';

            formData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
            if (this.pageMode === MntConst.eModeUpdate) {
                formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');
            }
            if (this.parentMode === 'ProductSalesDelivery') {
                formData['DeliveryQuantity'] = this.getControlValue('DeliveryQuantity');
            }

            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    if (callBkFn && typeof callBkFn === 'function') {
                        callBkFn.call(this);
                    }
                }
            });
        });
    }

    public riMntBeforeMode(): any {
        if (this.pageMode === MntConst.eModeDelete) {
            //Condition commented as TC not present
            // if (this.getControlValue('ServiceVisitDetail', true)) {
            //     setTimeout(() => {
            //         let tempModal: ICabsModalVO = new ICabsModalVO(this.pageParams.vDeletionWarning);
            //         tempModal.closeCallback = this.riMntBeforeDelete.bind(this);
            //         this.modalAdvService.emitMessage(tempModal);
            //     }, 500);
            // } else {
            //     this.riMntBeforeDelete();
            // }
            this.riMntBeforeDelete();
        }
    }

    public riMntAfterSave(callBkFn?: any): any {
        //UPDATE
        this.pageMode = this.pageMode === MntConst.eModeDelete ? MntConst.eModeUpdate : this.pageMode;
        if (this.pageMode === MntConst.eModeUpdate) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '2');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            let len = this.controls.length;
            for (let i = 0; i < len; i++) {
                let ctrlName: string = this.controls[i].name;
                let ctrlValue: any = this.getControlValue(this.controls[i].name, true);
                if (this.controls[i].type === MntConst.eTypeCheckBox) {
                    ctrlValue = this.utils.convertCheckboxValueToRequestValue(ctrlValue);
                }
                formData[ctrlName] = ctrlValue;
            }
            // formData['usercode'] = this.utils.getUserCode().toLowerCase(); //FOR SIT & DEV
            formData['usercode'] = this.utils.getUserCode();
            if (!this.pageParams.isGridHandleRequired) { formData['riGridHandle'] = ''; }

            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            this.setControlValue(i, data[i]);
                        }
                    }
                    this.markAsPristine();
                    if (this.pageParams.vSCEnableVisitRef) {
                        if (this.getControlValue('VisitReferenceWarning') !== '') {
                            this.globalNotifications.displayMessage(this.getControlValue('VisitReferenceWarning'),
                                CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                        }
                    }
                    if (this.parentMode === 'GroupServiceVisit' || this.parentMode === 'ConfirmedPlan' || this.parentMode === 'DespatchGrid') {
                        this.variableService.setBackClick(true);
                        this.location.back();
                    } else {
                        this.globalNotifications.displayMessage(MessageConstant.Message.SavedSuccessfully,
                            CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    }
                    this.setControlValue('riGridHandle', this.utils.gridHandle);
                    this.setControlValue('riCacheRefresh', true);
                    this.pageParams.isGridHandleRequired = false;
                }
            });
        }
        //ADD
        if (this.pageMode === MntConst.eModeAdd) {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '1');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            let len = this.controls.length;
            for (let i = 0; i < len; i++) {
                let ctrlName: string = this.controls[i].name;
                let ctrlValue: any = this.getControlValue(this.controls[i].name, true);
                if (this.controls[i].type === MntConst.eTypeCheckBox) {
                    ctrlValue = this.utils.convertCheckboxValueToRequestValue(ctrlValue);
                }
                formData[ctrlName] = ctrlValue;
            }
            // formData['usercode'] = this.utils.getUserCode().toLowerCase(); //FOR SIT & DEV
            formData['usercode'] = this.utils.getUserCode();
            if (!this.pageParams.isGridHandleRequired) { formData['riGridHandle'] = ''; }

            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    for (let i in data) {
                        if (data.hasOwnProperty(i)) {
                            if (i === 'ServiceVisit') {
                                this.setControlValue('ServiceVisitRowID', data[i]);
                                this.tempServiceVisitRowID = data[i];
                            } else {
                                this.setControlValue(i, data[i]);
                            }
                        }
                    }
                    this.setControlValue('riGridHandle', this.utils.gridHandle);
                    this.setControlValue('riCacheRefresh', true);
                    this.pageParams.isGridHandleRequired = false;
                    this.markAsPristine();
                    this.riMntAfterSaveAdd(data);
                }
            });
        }
    }

    public riMntAfterSaveAdd(data: any): any {
        if (this.getControlValue('InfestationInd', true)) {
            let parentMode: string = '';
            if (this.getControlValue('PrepUsedInd', true)) {
                parentMode = 'ServiceVisitPrep';
                this.pageParams.openPrepUsedMnt = this.getControlValue('PrepUsedInd', true);
            } else {
                parentMode = 'ServiceVisit';
            }

            if (data && data.hasOwnProperty('ServiceVisit')) {
                this.setControlValue('ServiceVisitRowID', data.ServiceVisit);
                let urlStrArr: any = this.location.path().split('?');
                let urlStr: any = urlStrArr[0];
                let qParams: any = 'parentMode=Summary'
                    + '&ContractNumber=' + this.getControlValue('ContractNumber')
                    + '&PremiseNumber=' + this.getControlValue('PremiseNumber')
                    + '&ProductCode=' + this.getControlValue('ProductCode')
                    + '&ServiceCoverRowID=' + this.getControlValue('ServiceCoverRowID')
                    + '&ServiceVisitRowID=' + this.getControlValue('ServiceVisitRowID');
                this.location.replaceState(urlStr, qParams);
            }

            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'UpdateSLAVisitCycle';
            formData['ServiceVisitRowID'] = this.getControlValue('ServiceVisitRowID');

            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    this.navigate(parentMode, InternalMaintenanceServiceModuleRoutes.ICABSSEINFESTATIONMAINTENANCE, {
                        currentContractTypeURLParameter: this.riExchange.getCurrentContractTypeUrlParam()
                    });
                }
            });
        }
        else if (this.getControlValue('PrepUsedInd', true) && !this.getControlValue('InfestationInd', true)) {
            let urlStrArr: any = this.location.path().split('?');
            let urlStr: any = urlStrArr[0];
            let qParams: any = 'parentMode=Summary'
                + '&ContractNumber=' + this.getControlValue('ContractNumber')
                + '&PremiseNumber=' + this.getControlValue('PremiseNumber')
                + '&ProductCode=' + this.getControlValue('ProductCode')
                + '&ServiceCoverRowID=' + this.getControlValue('ServiceCoverRowID')
                + '&ServiceVisitRowID=' + data.ServiceVisit;
            this.location.replaceState(urlStr, qParams);

            this.navigate('ServiceVisit', InternalMaintenanceServiceModuleRoutes.ICABSSEPREPUSEDMAINTENANCE, {
                CurrentContractTypeURLParameter: this.riExchange.getCurrentContractTypeUrlParam(),
                ContractNumber: this.getControlValue('ContractNumber'),
                PremiseNumber: this.getControlValue('PremiseNumber'),
                ProductCode: this.getControlValue('ProductCode'),
                ServiceVisitRowID: data.ServiceVisit
            });
        } else if (this.parentMode === 'GroupServiceVisit' || this.parentMode === 'ConfirmedPlan' || this.parentMode === 'DespatchGrid') {
            this.variableService.setBackClick(true);
            this.location.back();
        } else {
            this.globalNotifications.displayMessage(MessageConstant.Message.SavedSuccessfully,
                CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            this.defaultToUpdateMode();
        }
    }

    public riMntAfterFetch(): any {
        this.fnCheckWasteConsignmentNumberRequired();
        if (this.getControlValue('ServiceVisitDetail', true)) {
            this.uiDisplay.trServiceEnd = true;
        } else {
            this.uiDisplay.trServiceEnd = false;
        }
        if (this.getControlValue('GotSignature', true)) {
            this.uiDisplay.trSignature = true;
            this.uiDisplay.trPremiseContactSignature = false;
            this.uiDisplay.trReasonNumber = false;
            this.uiDisplay.trReasonDesc = false;
            this.uiDisplay.tdContactName = true;
            if (this.getControlValue('NameCapturedInd', true)) {
                this.uiDisplay.trPremiseContactPrintedName = true;
            } else {
                this.uiDisplay.trPremiseContactName = true;
            }
        } else {
            this.uiDisplay.trSignature = false;
            if (this.getControlValue('ReasonNumber') !== '') {
                this.uiDisplay.trReasonNumber = true;
                this.uiDisplay.trReasonDesc = false;
            } else {
                this.uiDisplay.trReasonNumber = false;
                this.uiDisplay.trReasonDesc = true;
            }
        }
        this.onloadPremiseContactSignature();
    }

    //OnChange functionalities - Start
    public onChangeProductComponentCode(): any {
        if (this.getControlValue('ProductComponentCode') !== '') {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'GetProductValues';
            formData['ProductComponentCode'] = this.getControlValue('ProductComponentCode');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    this.setControlValue('AlternateProductCode', data['AlternateProductCode']);
                    this.setControlValue('ProductComponentDesc', data['ProductComponentDesc']);
                    this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
                    let tempProductComponentCode: string = this.getControlValue('ProductComponentCode');
                    for (let vbProductCode in this.pageParams.vbDummyProductCodes) {
                        if (this.pageParams.vbDummyProductCodes.hasOwnProperty(vbProductCode)) {
                            if (tempProductComponentCode.toUpperCase() === this.pageParams.vbDummyProductCodes[vbProductCode].toUpperCase()) {
                                this.riExchange.riInputElement.Enable(this.uiForm, 'ProductComponentDesc');
                            }
                        }
                    }
                }
            });
        } else {
            this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
            this.setControlValue('AlternateProductCode', '');
            this.setControlValue('ProductComponentDesc', '');
        }
    }
    public onChangeAlternateProductCode(): any {
        if (this.getControlValue('AlternateProductCode') !== '') {
            this.ajaxSource.next(this.ajaxconstant.START);
            let search: QueryParams = new QueryParams();
            search.set(this.serviceConstants.Action, '6');
            search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
            search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());

            let formData: any = {};
            formData['Function'] = 'GetAlternateProductValues';
            formData['AlternateProductCode'] = this.getControlValue('AlternateProductCode');
            this.httpService.xhrPost(this.xhrParams.method, this.xhrParams.module, this.xhrParams.operation, search, formData).then((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.globalNotifications.displayMessage(data);
                } else {
                    this.setControlValue('ProductComponentCode', data['ProductComponentCode']);
                    this.setControlValue('ProductComponentDesc', data['ProductComponentDesc']);
                    this.riExchange.riInputElement.Disable(this.uiForm, 'ProductComponentDesc');
                    let tempProductComponentCode: string = this.getControlValue('ProductComponentCode');
                    for (let vbProductCode in this.pageParams.vbDummyProductCodes) {
                        if (this.pageParams.vbDummyProductCodes.hasOwnProperty(vbProductCode)) {
                            if (tempProductComponentCode.toUpperCase() === this.pageParams.vbDummyProductCodes[vbProductCode].toUpperCase()) {
                                this.riExchange.riInputElement.Enable(this.uiForm, 'ProductComponentDesc');
                            }
                        }
                    }
                }
            });
        }
    }

    public onChangeServiceDateStart(event: any): void {
        this.pageParams.ServiceDateStartChanged = true;
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ServiceDateStart');
    }

    public onChangeServiceDateEnd(event: any): void {
        this.logger.log('Datepicker - onChangeServiceDateEnd', event);
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ServiceDateEnd');
    }

    public onChangeMenu(): any {
        this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'menu');
        switch (this.getControlValue('menu')) {
            case 'ServiceStatsAdjust':
                this.globalNotifications.displayMessage(MessageConstant.Message.PageNotDeveloped,
                    CustomAlertConstants.c_s_MESSAGE_TYPE_WARNING);
                // this.navigate('ServiceVisit', 'Service/iCABSSeServiceStatsAdjustmentSearch.htm', {
                //     CurrentContractTypeURLParameter: this.riExchange.getCurrentContractTypeUrlParam()
                // });
                break;
            case 'PrepUsed':
                this.navigate('ServiceVisit', InternalSearchModuleRoutes.ICABSSEPREPUSEDSEARCH, {
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode'),
                    ServiceVisit: this.getControlValue('ServiceVisitRowID')
                });
                break;
            case 'Infestation':
                this.navigate('ServiceVisit', InternalSearchModuleRoutes.ICABSSEINFESTATIONSEARCH, {
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode'),
                    ServiceVisit: this.getControlValue('ServiceVisitRowID')
                });
                break;
            case 'PlanVisits':
                this.navigate('ServiceVisitMaintenance', InternalGridSearchSalesModuleRoutes.ICABSAPLANVISITGRIDYEAR, {
                    CurrentContractTypeURLParameter: this.riExchange.getCurrentContractTypeUrlParam(),
                    ServiceCoverRowID: this.getControlValue('ServiceCoverRowID'),
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode')
                });
                break;
            case 'ProRataCharges':
                this.navigate('ServiceVisitMaint', InternalGridSearchSalesModuleRoutes.ICABSAPRORATACHARGESUMMARY, {
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode'),
                    ServiceCoverRowID: this.getControlValue('ServiceCoverRowID'),
                    ServiceVisitRowID: this.getControlValue('ServiceVisitRowID')
                });
                break;
            case 'VisitDetail':
                this.navigate('ServiceVisit', ContractManagementModuleRoutes.ICABSASERVICEVISITDETAILSUMMARY, {
                    ContractNumber: this.getControlValue('ContractNumber'),
                    PremiseNumber: this.getControlValue('PremiseNumber'),
                    ProductCode: this.getControlValue('ProductCode'),
                    ServiceCoverRowID: this.getControlValue('ServiceCoverRowID'),
                    ServiceVisit: this.getControlValue('ServiceVisitRowID')
                });
                break;
        }
        this.setControlValue('menu', 0);
    }

    public onChangeFn(event: any, id?: string): void {
        let target = event.target || event.srcElement || event.currentTarget;
        let idAttr = target.attributes.id.nodeValue;
        let value = target.value;

        let lookupIP: Array<any> = [{
            table: 'Employee',
            query: { 'BusinessCode': this.utils.getBusinessCode(), 'EmployeeCode': value.toString() },
            fields: ['EmployeeCode', 'EmployeeSurname']
        }];
        this.LookUp.lookUpPromise(lookupIP).then((data) => {
            if (data) {
                switch (idAttr) {
                    case 'EmployeeCode':
                        this.utils.setLookupValue(this, ['EmployeeSurname'], data[0]);
                        break;
                    default:
                        if (idAttr.indexOf('AddEmployeeCode') === 0) {
                            let desc = this.utils.setLookupValue(this, ['EmployeeSurname'], data[0], [idAttr.replace('AddEmployeeCode', 'AddEmployeeSurname')]);
                        }
                }
            }
        });
    }
    //OnChange functionalities - End

    //Click functionalities - Start
    public onClickReviewDisplays(): any {
        let tempObj: any = {};
        for (let i in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(i)) {
                tempObj[i] = {};
                tempObj[i]['value'] = this.uiForm.controls[i].value;
                tempObj[i]['pristine'] = this.uiForm.controls[i].pristine;
            }
        }
        this.pageParams.cachedData = tempObj;

        this.isRequesting = true;
        this.pageParams.isFormDirty = (this.pageMode === MntConst.eModeUpdate) ? false : this.uiForm.dirty;
        this.uiForm.markAsPristine();
        this.pageParams.isGridHandleRequired = true;
        let parentMode: string = '';
        if (this.pageMode === MntConst.eModeAdd || this.pageMode === MntConst.eModeUpdate) {
            parentMode = 'ServiceVisitUpdate';
        } else {
            parentMode = 'ServiceVisitView';
        }
        this.navigate(parentMode, InternalGridSearchApplicationModuleRoutes.ICABSASERVICEVISITDISPLAYGRID, {
            currentContractTypeURLParameter: this.riExchange.getCurrentContractTypeUrlParam()
        });
    }
    //Click functionalities - End

    //Ellipsis Functionalities
    public updateEllipsisParams(): void {
        this.ellipsis.PlanVisitNumber.childparams.parentMode = (this.parentMode === 'CreditMissedService') ? 'LookUp-CreditMissedService' : 'LookUp';
        this.ellipsis.PlanVisitNumber.childparams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.PlanVisitNumber.childparams.ContractName = this.getControlValue('ContractName');
        this.ellipsis.PlanVisitNumber.childparams.PremiseNumber = this.getControlValue('PremiseNumber');
        this.ellipsis.PlanVisitNumber.childparams.PremiseName = this.getControlValue('PremiseName');
        this.ellipsis.PlanVisitNumber.childparams.ProductCode = this.getControlValue('ProductCode');
        this.ellipsis.PlanVisitNumber.childparams.ProductDesc = this.getControlValue('ProductDesc');
        this.ellipsis.PlanVisitNumber.childparams.ServiceCoverRowID = this.getControlValue('ServiceCoverRowID');

        this.ellipsis.PlanVisitNumber2.childparams.parentMode = 'LookUp';
        this.ellipsis.PlanVisitNumber2.childparams.ContractNumber = this.getControlValue('ContractNumber');
        this.ellipsis.PlanVisitNumber2.childparams.ContractName = this.getControlValue('ContractName');
        this.ellipsis.PlanVisitNumber2.childparams.PremiseNumber = this.getControlValue('PremiseNumber');
        this.ellipsis.PlanVisitNumber2.childparams.PremiseName = this.getControlValue('PremiseName');
        this.ellipsis.PlanVisitNumber2.childparams.ProductCode = this.getControlValue('ProductCode');
        this.ellipsis.PlanVisitNumber2.childparams.ProductDesc = this.getControlValue('ProductDesc');
        this.ellipsis.PlanVisitNumber2.childparams.ServiceCoverRowID = this.getControlValue('ServiceCoverRowID');
    }
    public ellipsisRespHandler(data: any, ellipsisID: string): any {
        switch (ellipsisID) {
            case 'PlanVisitNumber':
            case 'PlanVisitNumber2':
                for (let i in data) {
                    if (data.hasOwnProperty(i)) {
                        this.setControlValue(i, data[i]);
                        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, i);
                    }
                }
                this.onChangePlanVisitNumber();
                break;
            case 'EmployeeCode':
                this.setControlValue('EmployeeCode', data.EmployeeCode);
                this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                break;
            case 'ProductComponentCode':
                this.setControlValue('ProductComponentCode', data.ProductCode);
                this.setControlValue('AlternateProductCode', data.PrimaryAlternateCode);
                this.setControlValue('ProductComponentDesc', data.ProductDesc);
                break;
            default:
                let ctrlId: string = ellipsisID;
                if (ctrlId.indexOf('AddEmployeeCode') === 0) {
                    this.setControlValue(ctrlId, data.AddEmployeeCode);
                    this.setControlValue(ctrlId.replace('Code', 'Surname'), data.AddEmployeeSurname);
                }
                break;
        }
    }

    //Dropdown Component Handling Start
    public updateCustomDropdowns(): void {
        let len = this.controls.length;
        for (let i = 0; i < len; i++) {
            if (this.controls[i].hasOwnProperty('isCutomDropdown')) {
                if (this.controls[i].isCutomDropdown && this.dropDown[this.controls[i].name]) {
                    this.utils.setCustomDropdownValue(this.dropDown[this.controls[i].name], this.getControlValue(this.controls[i].name));
                } else {
                    if (this.controls[i].hasOwnProperty('ref')) {
                        let tempID: string = this.getControlValue(this.dropDown[this.controls[i].ref].displayFields[0]);
                        let tempDesc: string = this.getControlValue(this.dropDown[this.controls[i].ref].displayFields[1]);
                        if (tempID && tempDesc) {
                            this.dropDown[this.controls[i].ref].active = { id: tempID, text: tempID + ' - ' + tempDesc };
                        }
                    }
                }
            }
        }
    }
    public visitTypeSelectedValue(event: any): any {
        this.setControlValue('VisitTypeCode', event.VisitTypeCode || event.value);
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'VisitTypeCode');
        this.dropDown.VisitTypeCode.triggerValidate = false;
        this.riExchangeCBORequest();
    }
    public visitTypeDataRecieved(event: any): any {
        let len = (event) ? event.length : 0;
        this.dropDown.VisitTypeCode.arrData = [];
        for (let i = 0; i < len; i++) {
            this.dropDown.VisitTypeCode.arrData.push({ code: event[i]['VisitType.VisitTypeCode'], desc: event[i]['VisitType.VisitTypeDesc'] });
        }
    }
    public onManualVisitReceived(event: any): any {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ManualVisitReasonCode');
        if (event) {
            this.setControlValue('ManualVisitReasonCode', event.ManualVisitReasonCode);
            this.setControlValue('ManualVisitReasonDesc', event.ManualVisitReasonDesc);
            this.dropDown.manualVisitReasonSearch.isTriggerValidate = false;
        } else {
            this.setControlValue('ManualVisitReasonCode', '');
            this.setControlValue('ManualVisitReasonDesc', '');
        }
    }
    public onNoServiceSearchDropdownDataRecieved(event: any): any {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'NoServiceReasonCode');
        if (event) {
            this.setControlValue('NoServiceReasonCode', event.NoServiceReasonCode);
            this.setControlValue('NoServiceReasonDesc', event.NoServiceReasonDesc);
            this.dropDown.noServiceReasonSearch.isTriggerValidate = false;
        } else {
            this.setControlValue('NoServiceReasonCode', '');
            this.setControlValue('NoServiceReasonDesc', '');
        }
    }
    public onNoSignatureReasonSearchDataRecieved(event: any): any {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'ReasonNumber');
        if (event) {
            this.setControlValue('ReasonNumber', event.ReasonNumber);
            this.setControlValue('ReasonDesc', event.ReasonDesc);
            this.dropDown.noSignatureReasonSearch.isTriggerValidate = false;
        } else {
            this.setControlValue('ReasonNumber', '');
            this.setControlValue('ReasonDesc', '');
        }
    }
    public onVisitNarrativeSearchDataReceived(event: any): any {
        this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'VisitNarrativeCode');
        if (event) {
            this.setControlValue('VisitNarrativeCode', event.VisitNarrativeCode);
            this.setControlValue('VisitNarrativeDesc', event.VisitNarrativeDesc);
            this.dropDown.visitNarrativeSearch.isTriggerValidate = false;
        } else {
            this.setControlValue('VisitNarrativeCode', '');
            this.setControlValue('VisitNarrativeDesc', '');
        }
    }
    //Dropdown Component Handling End

    //Maintenance Event
    public save(): void {
        this.utils.highlightTabs();
        this.riExchange.validateForm(this.uiForm);

        for (let i in this.dropDown) {
            if (this.dropDown.hasOwnProperty(i)) {
                if (this.dropDown[i].hasOwnProperty('isRequired') && this.dropDown[i].isRequired) {
                    this.dropDown[i].triggerValidate = true;
                }
            }
        }

        if (this.uiForm.status === 'VALID') {
            this.riTab.TabFocus(1);
            if (this.pageMode === 'eModeAdd') {
                this.add();
            } else {
                this.update();
            }
        } else {
            for (let control in this.uiForm.controls) {
                if (this.uiForm.controls[control].invalid) {
                    this.logger.log('   >> Invalid:', control, this.uiForm.controls[control].errors, this.uiForm.controls[control]);
                    if (control === 'ManualVisitReasonCode') {
                        this.dropDown.manualVisitReasonSearch.isTriggerValidate = true;
                    }
                }
            }
        }
    }
    public add(): void {
        this.riMntBeforeSave(this.riMntBeforeSaveAdd);
    }
    public update(): void {
        this.riMntBeforeSave(this.riMntBeforeConfirm);
    }
    public delete(): void {
        this.pageParams.isGridHandleRequired = false;
        function deleteCanceled(): void { this.pageMode = MntConst.eModeUpdate; }
        this.pageMode = MntConst.eModeDelete;
        this.modalAdvService.emitPrompt(
            new ICabsModalVO(this.promptContentDel, null, this.riMntBeforeMode.bind(this), deleteCanceled.bind(this))
                .setShiftTop(true));
    }
    public cancel(): void {
        this.pageParams.isGridHandleRequired = false;
        if (this.pageMode === MntConst.eModeAdd) {
            if (!this.uiForm.dirty) {
                this.defaultToUpdateMode();
            } else {
                this.verifyFormSatate('C');
                this.modalAdvService.emitPrompt(
                    new ICabsModalVO(MessageConstant.Message.RouteAway, null, this.defaultToUpdateMode.bind(this), null)
                        .setShiftTop(true));
            }
        } else {
            this.variableService.setBackClick(true);
            this.location.back();
        }
    }
    public checkFormDirty(): void {
        if (!this.uiForm.dirty) {
            this.switchToAddMode();
        } else {
            this.verifyFormSatate('B');
            this.modalAdvService.emitPrompt(
                new ICabsModalVO(MessageConstant.Message.RouteAway, null, this.switchToAddMode.bind(this), null)
                    .setShiftTop(true));
        }
    }
    public verifyFormSatate(obj?: any): void {
        for (let control in this.uiForm.controls) {
            if (this.uiForm.controls[control].dirty) {
                this.logger.log('DIRTY:', obj, control + ' >> ' + this.uiForm.controls[control].dirty);
            }
        }
    }
    public switchToAddMode(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.tempServiceVisitRowID = this.getControlValue('ServiceVisitRowID');
        this.uiForm.reset();
        this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        this.setControlValue('ServiceCoverRowID', this.riExchange.getParentAttributeValue('ServiceCoverRowID'));
        this.setControlValue('menu', 'Options');

        this.setControlValue('riGridHandle', this.utils.gridHandle);
        this.setControlValue('riCacheRefresh', true);
        this.pageParams.isGridHandleRequired = false;
        for (let i in this.dropDown) {
            if (this.dropDown.hasOwnProperty(i)) {
                if (this.dropDown[i].hasOwnProperty('active')) {
                    this.dropDown[i].active = { id: '', text: '' };

                    if (this.dropDown[i].hasOwnProperty('triggerValidate')) { this.dropDown[i].triggerValidate = false; }
                    if (this.dropDown[i].hasOwnProperty('isTriggerValidate')) { this.dropDown[i].isTriggerValidate = false; }
                }
            }
        }

        //Page mode converted to Add  mode
        this.parentMode = 'SearchAdd';
        this.pageMode = MntConst.eModeAdd;

        let urlStrArr: any = this.location.path().split('?');
        let urlStr: any = urlStrArr[0];
        let qParams: any = 'parentMode=' + this.parentMode
            + '&ContractNumber=' + this.getControlValue('ContractNumber')
            + '&PremiseNumber=' + this.getControlValue('PremiseNumber')
            + '&ProductCode=' + this.getControlValue('ProductCode')
            + '&ServiceCoverRowID=' + this.getControlValue('ServiceCoverRowID');
        this.location.replaceState(urlStr, qParams);

        this.executeGetReq();
    }

    public canDeactivate(): Observable<boolean> {
        this.verifyFormSatate('A');
        return super.canDeactivate();
    }

    public disableCtrlsforUpdate(): void {
        this.disableControls(['menu']);
        this.disableAllCustomDropdowns();
    }
    public disableAllCustomDropdowns(): void {
        let len = this.controls.length;
        for (let i = 0; i < len; i++) {
            if (this.controls[i].hasOwnProperty('isCutomDropdown')) {
                if (this.controls[i].hasOwnProperty('ref')) {
                    this.dropDown[this.controls[i].ref].isDisabled = true;
                }
            }
        }
    }
    public enableAllCustomDropdowns(): void {
        let len = this.controls.length;
        for (let i = 0; i < len; i++) {
            if (this.controls[i].hasOwnProperty('isCutomDropdown')) {
                if (this.controls[i].hasOwnProperty('ref')) {
                    this.dropDown[this.controls[i].ref].isDisabled = false;
                }
            }
        }
    }

    //Generic function
    public focusSave(obj: any): void {
        this.riTab.focusNextTab(obj);
    }
    public markAsPristine(): void {
        this.formPristine();
    }
}
