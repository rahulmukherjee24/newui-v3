import { CustomAlertConstants } from './../../../shared/components/alert/customalert.constants';
import { Component, OnInit, Injector, ViewChild, AfterContentInit } from '@angular/core';

import { AccountSearchComponent } from './../search/iCABSASAccountSearch';
import { CommonGridFunction } from '@app/base/CommonGridFunction';
import { CommonLookUpUtilsService } from '@shared/services/commonLookupUtils.service';
import { ContractManagementModuleRoutes, InternalMaintenanceSalesModuleRoutes } from './../../base/PageRoutes';
import { ContractSearchComponent } from '@app/internal/search/iCABSAContractSearch';
import { GridAdvancedComponent } from '@shared/components/grid-advanced/grid-advanced';
import { GroupAccountNumberComponent } from './../search/iCABSSGroupAccountNumberSearch';
import { LightBaseComponent, IGridHandlers } from '@app/base/BaseComponentLight';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from './../../base/PageIdentifier';
import { PaginationComponent } from '@shared/components/pagination/pagination';
import { PremiseSearchComponent } from '@app/internal/search/iCABSAPremiseSearch';
import { ProductSearchGridComponent } from './../search/iCABSBProductSearch';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAMassServiceSuspendMaintenance.html',
    providers: [CommonLookUpUtilsService]
})

export class MassServiceSuspendMaintenanceComponent extends LightBaseComponent implements OnInit, AfterContentInit, IGridHandlers {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('promptModalForSave') public promptModalForSave;
    @ViewChild('errorModal') public errorModal;
    @ViewChild('messageModal') public messageModal;
    @ViewChild('promptModal') public promptModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    constructor(injector: Injector, private commonUtils: CommonLookUpUtilsService) {
        super(injector);
        this.commonGridFunction = new CommonGridFunction(this);
    }
    public pageId: string = '';
    public pageType: string = '';
    public commonGridFunction: CommonGridFunction;
    public contractSearchComponent: any;
    public premiseSearchComponent: any;
    public isPremisesEllipsisDisabled: boolean = true;
    public isContractEllipsisDisabled: boolean = false;
    public isSelectAllDisabled: boolean = true;
    public isSaveDisabled: boolean = true;
    public isSuspendDatesRequired: boolean = false;
    public isExcludingProductEllipsisDisabled: boolean = false;
    public isOnlyWithProductEllipsisDisabled: boolean = false;
    public hasGridData: boolean = false;
    public promptConfirmContent: string;
    public gridConfig: Record<string, number> = {
        itemsPerPage: 10,
        totalItem: 1
    };
    public suspendReasonSearchColumns: Array<string> = ['SuspendReasonCode', 'SuspendReasonDesc'];
    public suspendReasonSearchInputParams: any = {
        operation: 'Business/iCABSBSuspendReasonSearch',
        module: 'suspension',
        method: 'contract-management/search'
    };
    public queryParams: any = {
        operation: 'Application/iCABSAPremiseServiceSuspendMaintenance',
        module: 'suspension',
        method: 'contract-management/grid'
    };
    public isSuspendReasonSearchDisabled: boolean = true;
    public controls: Array<any> = [
        { name: 'GroupAccountNumber', type: MntConst.eTypeInteger },
        { name: 'GroupName', type: MntConst.eTypeText, disabled: true },
        { name: 'AccountNumber', type: MntConst.eTypeCode },
        { name: 'AccountName', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'Show', type: MntConst.eTypeText, value: 'all' },
        { name: 'Lines', type: MntConst.eTypeText, required: true, value: 10 },
        { name: 'SuspensionStartDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'SuspensionEndDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'OnlyWithProductCode', type: MntConst.eTypeText },
        { name: 'ExcludingProductCode', type: MntConst.eTypeText },
        { name: 'IssueCredit', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'ResetAnnivDateChk', type: MntConst.eTypeCheckBox, disabled: true },
        { name: 'ResetAnnivDate', type: MntConst.eTypeDate, disabled: true },
        { name: 'SuspendReasonCode', type: MntConst.eTypeCode }
    ];
    public ellipsis: Record<string, Record<string, Object>> = {
        commonProperties: {
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'Lookup-UpdateParent',
                'showAddNewDisplay': false
            },
            showHeader: true
        },
        groupAccount: {
            contentComponent: GroupAccountNumberComponent,
            showHeader: true,
            disabled: false
        },
        account: {
            contentComponent: AccountSearchComponent,
            childConfigParams: {
                'parentMode': 'Lookup-UpdateParent',
                'showAddNewDisplay': false
            },
            disabled: false
        },
        contract: {
            childConfigParams: {
                'parentMode': 'ContractSearch',
                'showAddNew': false,
                'shouldClear': true,
                'currentContractType': 'C',
                'currentContractTypeURLParameter': '<contract>'
            }
        },
        premise: {
            childConfigParams: {
                'parentMode': 'LookUp-All',
                'showAddNew': false,
                'shouldClear': true
            }
        },
        product: {
            childParams: {
                parentMode: 'ContractJobReport:Include',
                IncludeProducts: ''
            },
            component: ProductSearchGridComponent
        },
        excludeProduct: {
            childParams: {
                parentMode: 'ContractJobReport:Exclude',
                ExcludeProducts: ''
            },
            component: ProductSearchGridComponent
        }
    };

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        this.pageId = this.riExchange.getParentHTMLValue('parentMode') ? PageIdentifier.ICABSAMASSSERVICECOVERSUSPENDMAINTENANCE : PageIdentifier.ICABSAMASSSERVICESUSPENDMAINTENANCE;
        this.pageType = this.riExchange.getParentHTMLValue('parentMode') ? 'ServiceCoverSuspend' : 'PremiseSuspend';
        this.browserTitle = this.pageTitle = this.pageType === 'PremiseSuspend' ? 'Mass Suspend Service' : 'Service Cover Mass Suspend';
        super.ngAfterContentInit();
        this.onWindowLoad();
    }

    private onWindowLoad(): void {
        this.contractSearchComponent = ContractSearchComponent;
        this.premiseSearchComponent = PremiseSearchComponent;
        if (this.isReturning() && this.pageType === 'PremiseSuspend') {
            if (this.getControlValue('ContractNumber')) {
                this.isSelectAllDisabled = false;
            }
            if (this.getControlValue('ExcludingProductCode')) {
                this.disableControl('OnlyWithProductCode', true);
                this.isOnlyWithProductEllipsisDisabled = true;
            } else if (this.getControlValue('OnlyWithProductCode')) {
                this.disableControl('ExcludingProductCode', true);
                this.isExcludingProductEllipsisDisabled = true;
            }
            this.disableControl('ResetAnnivDateChk', !this.getControlValue('ResetAnnivDateChk'));
            this.buildGrid();
            this.onRiGridRefresh();
        } else {
            this.pageParams.gridHandle = this.utils.randomSixDigitString();
            this.pageParams.gridCacheRefresh = true;
            this.pageParams.gridCurrentPage = 1;
            this.pageParams.taggedArray = [];
            this.pageParams.suspendReasonSelected = {
                id: '',
                text: ''
            };
            if (this.pageType === 'ServiceCoverSuspend') {
                this.setControlValue('AccountNumber', this.riExchange.getParentHTMLValue('AccountNumber'));
                this.commonUtils.getAccountName(this.getControlValue('AccountNumber'))
                    .then(data => {
                        this.setControlValue('AccountName', data[0][0].AccountName);
                    });
                this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
                this.commonUtils.getContractName(this.getControlValue('ContractNumber'))
                    .then(data => {
                        this.setControlValue('ContractName', data[0][0].ContractName);
                    });
                this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
                this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
                this.disableControl('AccountNumber', true);
                this.disableControl('ContractNumber', true);
                this.disableControl('PremiseNumber', true);
                this.isContractEllipsisDisabled = true;
                this.ellipsis.account.disabled = true;
                this.onRiGridRefresh();
            }
            this.buildGrid();
        }
    }

    private buildGrid(): void {
        this.riGrid.Clear();

        if (this.pageType === 'PremiseSuspend') {
            this.riGrid.AddColumn('AccountNumber', 'MassSuspend', 'AccountNumber', MntConst.eTypeText, 8);
            this.riGrid.AddColumn('ContractNumber', 'MassSuspend', 'ContractNumber', MntConst.eTypeText, 8);
            this.riGrid.AddColumn('PremiseNumber', 'MassSuspend', 'PremiseNumber', MntConst.eTypeInteger, 4);
            this.riGrid.AddColumn('PremiseName', 'MassSuspend', 'PremiseName', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('AddressLine1', 'MassSuspend', 'AddressLine1', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('Town', 'MassSuspend', 'Town', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('PostCode', 'MassSuspend', 'PostCode', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('PremisesCommenceDate', 'MassSuspend', 'PremisesCommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('ServicingBranch', 'MassSuspend', 'ServicingBranch', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('Value', 'MassSuspend', 'Value', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('SuspensionStartDate', 'MassSuspend', 'SuspensionStartDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('SuspensionEndDate', 'MassSuspend', 'SuspensionEndDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('Status', 'MassSuspend', 'Status', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('ProductCode', 'MassSuspend', 'ProductCode', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('Suspend', 'MassSuspend', 'Suspend', MntConst.eTypeImage, 1, false, '');
            this.riGrid.AddColumn('PartSuspend', 'MassSuspend', 'PartSuspend', MntConst.eTypeImage, 10);
        } else {
            this.riGrid.AddColumn('ProductCode', 'MassSuspend', 'ProductCode', MntConst.eTypeText, 8);
            this.riGrid.AddColumn('ProductDescription', 'MassSuspend', 'ProductDescription', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('CommenceDate', 'MassSuspend', 'CommenceDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('ServiceArea', 'MassSuspend', 'ServiceArea', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('Qty', 'MassSuspend', 'Qty', MntConst.eTypeInteger, 10);
            this.riGrid.AddColumn('Freq', 'MassSuspend', 'Freq', MntConst.eTypeInteger, 8);
            this.riGrid.AddColumn('Value', 'MassSuspend', 'Value', MntConst.eTypeText, 20);
            this.riGrid.AddColumn('NextInvoiceStartDate', 'MassSuspend', 'NextInvoiceStartDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('NextInvoiceEndDate', 'MassSuspend', 'NextInvoiceEndDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('VisitAnnivDate', 'MassSuspend', 'VisitAnnivDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('SuspensionStartDate', 'MassSuspend', 'SuspensionStartDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('SuspensionEndDate', 'MassSuspend', 'SuspensionEndDate', MntConst.eTypeDate, 10);
            this.riGrid.AddColumn('Status', 'MassSuspend', 'Status', MntConst.eTypeText, 20);
        }

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.getControlValue('Lines')) {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            if (this.pageType === 'PremiseSuspend') {
                formData['GroupAccountNumber'] = this.getControlValue('GroupAccountNumber');
                formData['Show'] = this.getControlValue('Show');
                formData['IncludeProducts'] = this.getControlValue('OnlyWithProductCode');
                formData['ExcludeProducts'] = this.getControlValue('ExcludingProductCode');
                formData['IssueCredit'] = this.getControlValue('IssueCredit');
                formData['ServiceVisitAnnivDateInd'] = this.getControlValue('ResetAnnivDateChk') ? 'yes' : 'no';
                formData['UpdatedAnnivDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ResetAnnivDate'));
            }
            if (this.pageParams.selectAll) {
                formData['selectallchosen'] = true;
            } else {
                formData['selectallchosen'] = false;
            }
            formData['FullAccess'] = 'Full';
            formData['LoggedInBranch'] = this.utils.getBranchCode();
            formData['AccountNumber'] = this.getControlValue('AccountNumber');
            formData['ContractNumber'] = this.getControlValue('ContractNumber');
            formData['PremiseNumber'] = this.getControlValue('PremiseNumber');
            formData['ServiceCoverNumber'] = '';
            formData['SuspensionStartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SuspensionStartDate'));
            formData['SuspensionEndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SuspensionEndDate'));
            formData['CreditSuspendServiceInd'] = false;
            formData['fn'] = 'AnnivDate';
            if (!this.pageParams.selectAll) {
                formData['TaggedList'] = this.pageParams.taggedList || '';
            }
            formData[this.serviceConstants.GridMode] = '0';
            formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
            formData[this.serviceConstants.PageSize] = this.getControlValue('Lines');
            formData[this.serviceConstants.PageCurrent] = this.pageParams.gridCurrentPage;
            formData[this.serviceConstants.GridHeaderClickedColumn] = '';
            formData[this.serviceConstants.GridSortOrder] = 'Ascending';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.xhrPost('mass/suspend', 'mass-suspend', this.pageType === 'PremiseSuspend' ? 'Application/iCABSAMassServiceSuspendMaintenance' : 'Application/iCABSServiceCoverMassSuspendGrid', search, formData).then(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (this.hasError(data)) {
                        this.hasGridData = false;
                        this.gridConfig.totalItem = 1;
                        this.displayMessage(data);
                    } else {
                        this.hasGridData = true;
                        this.riGrid.RefreshRequired();
                        this.pageParams.gridCurrentPage = data.pageData ? data.pageData.pageNumber : 1;
                        this.gridConfig.totalItem = data.pageData && data.pageData.lastPageNumber ? data.pageData.lastPageNumber * this.gridConfig.itemsPerPage : 1;
                        if (this.pageParams.selectAll) {
                            this.pageParams.taggedList = data.footer.rows[0].text.replace(/,/g, '');
                            this.pageParams.taggedArray = data.footer.rows[0].text.split(',');
                        }
                        this.isSaveDisabled = this.pageParams.taggedList ? false : true;
                        if (this.pageParams.taggedList) {
                            this.disableControl('IssueCredit', false);
                            this.disableControl('SuspensionStartDate', false);
                            this.disableControl('SuspensionEndDate', false);
                            this.isSuspendReasonSearchDisabled = false;
                            this.isSuspendDatesRequired = true;
                            this.setRequiredStatus('SuspensionStartDate', true);
                            this.setRequiredStatus('SuspensionEndDate', true);
                            this.setRequiredStatus('SuspendReasonCode', true);
                        } else {
                            this.disableControl('ResetAnnivDateChk', true);
                            this.disableControl('IssueCredit', true);
                            this.disableControl('SuspensionStartDate', true);
                            this.disableControl('SuspensionEndDate', true);
                            this.isSuspendReasonSearchDisabled = true;
                            this.setControlValue('SuspensionStartDate', '');
                            this.setControlValue('SuspensionEndDate', '');
                            this.setControlValue('ResetAnnivDate', '');
                            this.setControlValue('ResetAnnivDateChk', false);
                            this.setControlValue('IssueCredit', false);
                            this.isSuspendDatesRequired = false;
                            this.setRequiredStatus('SuspensionStartDate', false);
                            this.setRequiredStatus('SuspensionEndDate', false);
                            this.setRequiredStatus('SuspendReasonCode', false);
                            this.setControlValue('SuspendReasonCode', '');
                            this.pageParams.suspendReasonSelected = {
                                id: '',
                                text: ''
                            };
                        }
                        this.riGrid.UpdateFooter = false;
                        this.riGrid.Execute(data);
                        setTimeout(() => {
                            this.riGridPagination.setPage(this.pageParams.gridCurrentPage);
                        }, 100);
                        data.body.cells.map(elements => {
                            if (elements.additionalData.toLowerCase() === 't' && this.pageType === 'PremiseSuspend') {
                                this.isSaveDisabled = false;
                            }
                        });
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.hasGridData = false;
                    this.isRequesting = false;
                    this.displayMessage(error);
                });
        }
    }

    public onEllipsisDataRecieved(data: any, type: string): void {
        let newValue: string = '';
        this.onChangeCommon();
        switch (type) {
            case 'GroupAccount':
                this.setControlValue('GroupAccountNumber', data.GroupAccountNumber);
                this.setControlValue('GroupName', data.GroupName);
                this.resetValues('GroupAccount');
                this.pageParams.isMandatory = true;
                this.disableControl('AccountNumber', !data.GroupAccountNumber);
                this.disableControl('ContractNumber', data.GroupAccountNumber);
                this.isContractEllipsisDisabled = data.GroupAccountNumber;
                this.ellipsis.account.childConfigParams['groupAccountNumber'] = this.getControlValue('GroupAccountNumber');
                this.ellipsis.account.childConfigParams['groupName'] = this.getControlValue('GroupName');
                break;
            case 'Account':
                this.setControlValue('AccountNumber', data.AccountNumber);
                this.setControlValue('AccountName', data.AccountName);
                this.resetValues('Account');
                this.disableControl('ContractNumber', false);
                this.isContractEllipsisDisabled = false;
                this.ellipsis.contract.childConfigParams['accountNumber'] = this.getControlValue('AccountNumber');
                this.ellipsis.contract.childConfigParams['accountName'] = this.getControlValue('AccountName');
                break;
            case 'Contract':
                this.setControlValue('ContractNumber', data.ContractNumber);
                this.setControlValue('ContractName', data.ContractName);
                this.disableControl('PremiseNumber', !data.ContractNumber);
                this.isPremisesEllipsisDisabled = false;
                this.isSelectAllDisabled = !data.ContractName;
                this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                break;
            case 'Premise':
                this.setControlValue('PremiseNumber', data.PremiseNumber);
                this.setControlValue('PremiseName', data.PremiseName);
                this.ellipsis.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                this.ellipsis.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                this.ellipsis.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                this.ellipsis.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
                break;
            case 'Product':
                newValue = data['IncludeProducts'];
                this.ellipsis.product['childParams']['IncludeProducts'] = newValue;
                this.setControlValue('OnlyWithProductCode', newValue);
                break;
            case 'ExcludingProductCode':
                newValue = data['ExcludeProducts'];
                this.ellipsis.excludeProduct['childParams']['ExcludeProducts'] = newValue;
                this.setControlValue('ExcludingProductCode', newValue);
                break;
        }
    }

    public onChangeControls(type: string): void {
        if (type !== 'ResetAnnivDateChk' && type !== 'SuspensionEndDate') {
            this.onChangeCommon();
        }
        switch (type) {
            case 'GroupAccount':
                this.setControlValue('GroupName', '');
                this.disableControl('ContractNumber', false);
                this.isContractEllipsisDisabled = false;
                this.disableControl('PremiseNumber', true);
                this.isPremisesEllipsisDisabled = true;
                this.isSelectAllDisabled = true;
                this.resetValues('GroupAccount');
                this.pageParams.isMandatory = false;
                this.commonUtils.getGroupAccountName(this.getControlValue('GroupAccountNumber'))
                    .then(data => {
                        if (data[0][0]) {
                            this.setControlValue('GroupName', data[0][0].GroupName);
                            this.pageParams.isMandatory = true;
                            this.disableControl('AccountNumber', false);
                            this.disableControl('ContractNumber', true);
                            this.isContractEllipsisDisabled = true;
                        } else {
                            if (this.getControlValue('GroupAccountNumber')) {
                                this.displayMessage(MessageConstant.Message.RecordNotFound);
                            }
                        }
                    });
                this.ellipsis.account.childConfigParams['groupAccountNumber'] = this.getControlValue('GroupAccountNumber');
                break;
            case 'Account':
                this.setControlValue('AccountName', '');
                this.resetValues('Account');
                this.disableControl('PremiseNumber', true);
                this.isPremisesEllipsisDisabled = true;
                this.isSelectAllDisabled = true;
                if (this.getControlValue('AccountNumber')) {
                    this.commonUtils.getAccountName(this.getControlValue('AccountNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setControlValue('AccountName', data[0][0].AccountName);
                            } else {
                                this.displayMessage(MessageConstant.Message.RecordNotFound);
                            }
                        });
                }
                this.ellipsis.contract.childConfigParams['accountNumber'] = this.getControlValue('AccountNumber');
                break;
            case 'Contract':
                this.setControlValue('ContractName', '');
                this.resetValues('Contract');
                this.disableControl('PremiseNumber', true);
                this.isPremisesEllipsisDisabled = true;
                this.isSelectAllDisabled = true;
                if (this.getControlValue('ContractNumber')) {
                    this.commonUtils.getContractName(this.getControlValue('ContractNumber'), this.utils.getBusinessCode(), this.getControlValue('AccountNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setControlValue('ContractName', data[0][0].ContractName);
                                this.ellipsis.premise.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                                this.ellipsis.premise.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                                this.disableControl('PremiseNumber', false);
                                this.isPremisesEllipsisDisabled = false;
                                this.isSelectAllDisabled = false;
                            } else {
                                this.setControlValue('OnlyWithProductCode', '');
                                this.displayMessage(MessageConstant.Message.RecordNotFound);
                            }
                        });
                } else {
                    this.setControlValue('OnlyWithProuctCode', '');
                    this.setControlValue('ExcludingProductCode', '');
                    this.setControlValue('ContractName', '');
                }
                break;
            case 'Premise':
                this.setControlValue('PremiseName', '');
                if (this.getControlValue('PremiseNumber')) {
                    this.commonUtils.getPremiseName(this.getControlValue('PremiseNumber'), this.utils.getBusinessCode(), this.getControlValue('ContractNumber'))
                        .then(data => {
                            if (data[0][0]) {
                                this.setControlValue('PremiseName', data[0][0].PremiseName);
                                this.ellipsis.product.childConfigParams['ContractNumber'] = this.getControlValue('ContractNumber');
                                this.ellipsis.product.childConfigParams['ContractName'] = this.getControlValue('ContractName');
                                this.ellipsis.product.childConfigParams['PremiseNumber'] = this.getControlValue('PremiseNumber');
                                this.ellipsis.product.childConfigParams['PremiseName'] = this.getControlValue('PremiseName');
                            } else {
                                this.displayMessage(MessageConstant.Message.RecordNotFound);
                            }
                        });
                }
                break;
            case 'OnlyWithProductCode':
                this.setControlValue('ExcludingProductCode', '');
                this.disableControl('ExcludingProductCode', this.getControlValue('OnlyWithProductCode'));
                this.isExcludingProductEllipsisDisabled = this.getControlValue('OnlyWithProductCode');
                this.ellipsis.product['childParams']['IncludeProducts'] = this.getControlValue('OnlyWithProductCode');
                break;
            case 'ExcludingProductCode':
                this.setControlValue('OnlyWithProductCode', '');
                this.disableControl('OnlyWithProductCode', this.getControlValue('ExcludingProductCode'));
                this.isOnlyWithProductEllipsisDisabled = this.getControlValue('ExcludingProductCode');
                this.ellipsis.excludeProduct['childParams']['ExcludeProducts'] = this.getControlValue('ExcludingProductCode');
                break;
            case 'ResetAnnivDateChk':
                if (this.getControlValue('SuspensionEndDate')) {
                    let endDate = new Date(this.getControlValue('SuspensionEndDate'));
                    this.setControlValue('ResetAnnivDate', new Date(endDate.getFullYear(), endDate.getMonth(), new Date(endDate).getDate() + 1));
                }
                if (!this.getControlValue('ResetAnnivDateChk')) {
                    this.setControlValue('ResetAnnivDate', '');
                }
                break;
            case 'SuspensionEndDate':
                if (this.getControlValue('SuspensionEndDate')) {
                    this.disableControl('ResetAnnivDateChk', false);
                    if (this.getControlValue('ResetAnnivDateChk')) {
                        let endDate = new Date(this.getControlValue('SuspensionEndDate'));
                        this.setControlValue('ResetAnnivDate', new Date(endDate.getFullYear(), endDate.getMonth(), new Date(endDate).getDate() + 1));
                    }
                } else {
                    this.disableControl('ResetAnnivDateChk', true);
                    this.setControlValue('ResetAnnivDate', '');
                    this.setControlValue('ResetAnnivDateChk', false);
                }
                break;
        }
    }

    public resetValues(type: string): void {
        switch (type) {
            case 'GroupAccount':
                this.setControlValue('AccountNumber', '');
                this.setControlValue('AccountName', '');
                this.setControlValue('ContractNumber', '');
                this.setControlValue('ContractName', '');
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                break;
            case 'Account':
                this.setControlValue('ContractNumber', '');
                this.setControlValue('ContractName', '');
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                break;
            case 'Contract':
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                break;
        }
    }

    public suspendReasonSearchDataRecieved(event: any): void {
        if (event.SuspendReasonCode) {
            this.setControlValue('SuspendReasonCode', event.SuspendReasonCode);
            this.pageParams.suspendReasonSelected = {
                id: event.SuspendReasonCode,
                text: event.SuspendReasonCode + ' - ' + event.SuspendReasonDesc
            };
        } else {
            this.setControlValue('SuspendReasonCode', '');
            this.pageParams.suspendReasonSelected = {
                id: '',
                text: ''
            };
        }
    }

    public getSuspendResonDesc(suspendReasonCode: string): void {
        if (suspendReasonCode) {
            let formData: Object = {};
            let search = this.getURLSearchParamObject();

            search.set(this.serviceConstants.Action, '6');

            formData['Function'] = 'GetSuspendReasonDesc';
            formData['SuspendReasonCode'] = suspendReasonCode;

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
                .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.displayMessage(data.fullError);
                        } else {
                            this.pageParams.suspendReasonSelected = {
                                id: suspendReasonCode,
                                text: suspendReasonCode + ' - ' + data.SuspendResonDesc
                            };
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.displayMessage(error.fullError);
                    });
        } else {
            this.pageParams.suspendReasonSelected = {
                id: '',
                text: ''
            };
            this.setControlValue('SuspendReasonCode', '');
        }
    }

    public onRiGridRefresh(): void {
        this.commonGridFunction.onRefreshClick();
    }

    public onBtnClick(type: string): void {
        switch (type) {
            case 'SelectAll':
                this.pageParams.selectAll = true;
                this.onRiGridRefresh();
                break;
            case 'ClearAll':
                this.pageParams.taggedList = '';
                this.pageParams.selectAll = false;
                this.isSuspendDatesRequired = false;
                this.setControlValue('SuspendReasonCode', '');
                this.pageParams.suspendReasonSelected = {
                    id: '',
                    text: ''
                };
                this.pageParams.taggedArray = [];
                this.isSaveDisabled = true;
                this.onRiGridRefresh();
                break;
            case 'CancelSuspension':
                this.saveCancelSuspension('cancel');
                break;
            case 'Save':
                if (this.riExchange.validateForm(this.uiForm) && this.getControlValue('SuspendReasonCode')) {
                    this.promptModal.show();
                    if (this.getControlValue('OnlyWithProductCode') || this.getControlValue('ExcludingProductCode')) {
                        this.promptConfirmContent = MessageConstant.PageSpecificMessage.massSuspendSave;
                    } else {
                        this.promptConfirmContent = MessageConstant.Message.ConfirmRecord;
                    }
                }
                break;
            case 'Cancel':
                this.setControlValue('SuspensionStartDate', '');
                this.setControlValue('SuspensionEndDate', '');
                this.pageParams.suspendReasonSelected = {
                    id: '',
                    text: ''
                };
                this.setControlValue('ResetAnnivDateChk', false);
                this.setControlValue('ResetAnnivDate', '');
                this.disableControl('ResetAnnivDateChk', true);
                this.disableControl('ResetAnnivDate', true);
                break;
        }
    }

    public confirmed(obj: any): void {
        if (this.getControlValue('SuspendReasonCode')) {
            this.saveCancelSuspension('save');
        }
    }

    private saveCancelSuspension(type: string): void {
        if ((type === 'save' && this.riExchange.validateForm(this.uiForm)) || type === 'cancel') {
            this.isRequesting = true;
            let search: QueryParams = this.getURLSearchParamObject();
            search.set(this.serviceConstants.Action, '2');
            let formData: Object = {};

            formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
            formData['TaggedList'] = this.pageParams.taggedList;
            formData['SuspendStartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SuspensionStartDate'));
            formData['SuspendEndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SuspensionEndDate'));
            formData['Level'] = 'MassLevel';
            formData['IncludeProducts'] = this.getControlValue('OnlyWithProductCode');
            formData['ExcludeProducts'] = this.getControlValue('ExcludingProductCode');
            if (type === 'save') {
                formData['FullAccess'] = 'Full';
                formData['LoggedInBranch'] = this.utils.getBranchCode();
                formData['CreditSuspendServiceInd'] = this.getControlValue('IssueCredit');
                formData['ServiceVisitAnnivDateInd'] = this.getControlValue('ResetAnnivDateChk') ? 'yes' : 'no';
                formData['UpdatedAnnivDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ResetAnnivDate'));
                formData['SuspendReasonCode'] = this.getControlValue('SuspendReasonCode');
                formData['SuspendReasonDesc'] = this.pageParams.suspendReasonSelected.text;
            } else {
                formData[this.serviceConstants.BusinessCode] = this.utils.getBusinessCode();
                formData['Function'] = 'CancelSuspension';
                formData['TaggedList'] = this.pageParams.taggedList;
                formData['Mode'] = '';
            }
            this.httpService.xhrPost('mass/suspend', 'mass-suspend', 'Application/iCABSMassServiceCoverServiceSuspendEntry', search, formData)
                .then(data => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.isRequesting = false;
                    if (data.fullError) {
                        this.displayMessage(data, CustomAlertConstants.c_s_MESSAGE_TYPE_ERROR);
                    } else {
                        this.displayMessage(type === 'save' ? MessageConstant.Message.RecordSavedSuccessfully : MessageConstant.PageSpecificMessage.serviceCoverServiceSuspendmaintenance.cancelSuspension, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
                    }
                    this.pageParams.taggedList = '';
                    this.pageParams.taggedArray = [];
                    this.onRiGridRefresh();
                });
        }
    }

    public onGridBodyDoubleClick(): void {
        switch (this.riGrid.CurrentColumnName) {
            case 'Suspend':
                this.pageParams.selectAll = false;
                this.isSaveDisabled = true;
                let cellValue = this.riGrid.Details.GetValue('Suspend');
                let rowId = this.riGrid.Details.GetAttribute('Suspend', 'rowid');
                this.pageParams.taggedList = cellValue.toLowerCase() === 't' ?
                    this.pageParams.taggedList.replace(rowId, '') : this.pageParams.taggedList + rowId;

                if (cellValue.toLowerCase() !== 't') {
                    this.pageParams.taggedArray.push(rowId);
                } else {
                    for (let i = 0; i < this.pageParams.taggedArray.length; i++) {
                        if (this.pageParams.taggedArray[i] === rowId) {
                            this.pageParams.taggedArray.splice(i, 1);
                        }
                    }
                }
                this.onRiGridRefresh();
                break;
            case 'AccountNumber':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAACCOUNTMAINTENANCE, {
                    'parentMode': 'DailyTransactions',
                    'AccountNumber': this.riGrid.Details.GetValue('AccountNumber')
                });
                break;
            case 'ContractNumber':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSACONTRACTMAINTENANCE, {
                    'parentMode': 'DailyTransactions',
                    'ContractNumber': this.riGrid.Details.GetValue('ContractNumber')
                });
                break;
            case 'PremiseNumber':
                this.navigate('DailyTransactions', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    'parentMode': 'DailyTransactions',
                    'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                    'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber')
                });
                break;
            case 'PartSuspend':
                this.navigate('serviceCoverMassSuspend', InternalMaintenanceSalesModuleRoutes.ICABSAMASSSERVICECOVERSUSPENDMAINTENANCE, {
                    'parentMode': 'ServiceCoverMassSuspend',
                    'ContractNumber': this.riGrid.Details.GetValue('ContractNumber'),
                    'PremiseNumber': this.riGrid.Details.GetValue('PremiseNumber'),
                    'PremiseName': this.riGrid.Details.GetValue('PremiseName'),
                    'AccountNumber': this.riGrid.Details.GetValue('AccountNumber')
                });
                break;
            case 'ProductCode':
                if (this.pageType === 'ServiceCoverSuspend') {
                    this.navigate('LostBusinessAnalysis', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                        ContractNumber: this.getControlValue('ContractNumber'),
                        ContractName: this.getControlValue('ContractName'),
                        PremiseNumber: this.getControlValue('PremiseNumber'),
                        ProductCode: this.riGrid.Details.GetValue('ProductCode'),
                        ServiceCoverRowID: this.riGrid.Details.GetAttribute('ProductCode', 'rowid'),
                        currentContractType: 'C'
                    });
                }
                break;
        }
    }

    public onChangeCommon(): void {
        this.pageParams.taggedList = '';
        this.isSaveDisabled = true;
        this.gridConfig.totalItem = 0;
        this.pageParams.gridCurrentPage = 1;
        this.pageParams.taggedList = '';
        this.pageParams.taggedArray = [];
        this.pageParams.selectAll = false;
        this.setRequiredStatus('SuspensionStartDate', false);
        this.setRequiredStatus('SuspensionEndDate', false);
        this.setRequiredStatus('SuspendReasonCode', false);
        this.isSuspendDatesRequired = false;
        this.setControlValue('SuspendReasonCode', '');
        this.pageParams.suspendReasonSelected = {
            id: '',
            text: ''
        };
        this.riGrid.Clear();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }

}
