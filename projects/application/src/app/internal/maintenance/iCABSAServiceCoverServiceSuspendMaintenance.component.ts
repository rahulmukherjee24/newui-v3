import { Component, OnInit, OnDestroy, Injector, ViewChild, EventEmitter, AfterViewInit } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../base/BaseComponent';
import { PageIdentifier } from '../../base/PageIdentifier';
import { InternalGridSearchSalesModuleRoutes } from './../../base/PageRoutes';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from './../../../shared/components/ellipsis/ellipsis';
import { ContractSearchComponent } from './../search/iCABSAContractSearch';
import { PremiseSearchComponent } from './../search/iCABSAPremiseSearch';
import { ServiceCoverSearchComponent } from './../search/iCABSAServiceCoverSearch';

@Component({
    templateUrl: 'iCABSAServiceCoverServiceSuspendMaintenance.html'
})

export class ServiceSuspendMaintenanceComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('contractSearchEllipsis') public contractSearchEllipsis: EllipsisComponent;
    @ViewChild('sCSearchEllipsis') public sCSearchEllipsis: EllipsisComponent;
    private vbEnableCreditSuspendService: boolean = false;
    private strSuspendStartDate: string;
    private strSuspendEndDate: string;
    private currentContractType: string;
    private queryParams: any = {
        operation: 'Application/iCABSAServiceCoverServiceSuspendMaintenance',
        module: 'suspension',
        method: 'contract-management/maintenance'
    };
    public pageId: string = '';
    public isShowHeader: boolean = true;
    public isCmdSuspend: boolean = true;
    public isSaveDisable: boolean = true;
    public isCancelDisable: boolean = true;
    public isCmdCancel: boolean = true;
    public setFocusOnUpdatedAnnivDate = new EventEmitter<boolean>();
    public setFocusOnSuspendQuantity = new EventEmitter<boolean>();
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, required: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, required: true, commonValidator: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, required: true, commonValidator: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ServiceVisitFrequency', type: MntConst.eTypeInteger },
        { name: 'ServiceAnnualValue', type: MntConst.eTypeCurrency },
        { name: 'ServiceQuantity', type: MntConst.eTypeInteger },
        { name: 'ServiceCommenceDate', type: MntConst.eTypeDate },
        { name: 'ServiceVisitAnnivDate', type: MntConst.eTypeDate },
        { name: 'SuspendStartDate', type: MntConst.eTypeDate },
        { name: 'SuspendEndDate', type: MntConst.eTypeDate },
        { name: 'SuspendLocation', type: MntConst.eTypeText, commonValidator: true },
        { name: 'SuspendQuantity', type: MntConst.eTypeInteger },
        { name: 'SuspendReasonCode', type: MntConst.eTypeCode },
        { name: 'SuspendReasonDesc', type: MntConst.eTypeTextFree },
        { name: 'CreditSuspendServiceInd', type: MntConst.eTypeCheckBox },
        { name: 'ServiceVisitAnnivDateInd', type: MntConst.eTypeCheckBox },
        { name: 'UpdatedAnnivDate', type: MntConst.eTypeDate },
        { name: 'Status', type: MntConst.eTypeText },
        { name: 'ErrorMessageDesc', type: MntConst.eTypeText },
        { name: 'AccountBalance', type: MntConst.eTypeCurrency },
        { name: 'QuantityRequired', type: MntConst.eTypeText },
        { name: 'ServiceCoverNumber', type: MntConst.eTypeInteger },
        { name: 'Menu' },
        { name: 'ServiceCoverRowID' }
    ];
    public inputParams: any = {
        contractParams: {
            parentMode: 'LookUp',
            showAddNew: false,
            currentContractType: '',
            contractSearchComponent: ContractSearchComponent
        },
        premiseParams: {
            parentMode: 'LookUp',
            ContractNumber: '',
            ContractName: '',
            currentContractType: '',
            premiseSearchComponent: PremiseSearchComponent
        },
        serviceCoverSearchParams: {
            parentMode: 'Search',
            ContractNumber: '',
            ContractName: '',
            PremiseNumber: '',
            PremiseName: '',
            ProductCode: '',
            ProductDesc: '',
            ContractTypeCode: '',
            CurrentContractTypeURLParameter: '<contract>',
            serviceCoverSearchComponent: ServiceCoverSearchComponent
        }
    };
    public suspendReasonSearchColumns: Array<string> = ['SuspendReasonCode', 'SuspendReasonDesc'];
    public suspendReasonSearchInputParams: any = {
        operation: 'Business/iCABSBSuspendReasonSearch',
        module: 'suspension',
        method: 'contract-management/search'
    };
    public isSuspendReasonSearchDisabled: boolean = false;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSASERVICECOVERSERVICESUSPENDMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Service Cover Invoice and Service Suspension Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.currentContractType = this.riExchange.setCurrentContractType();
        this.inputParams.contractParams.currentContractType = this.inputParams.premiseParams.currentContractType = this.currentContractType;
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            if (this.getControlValue('SuspendReasonCode')) {
                this.pageParams.suspendReasonSelected = {
                    id: this.getControlValue('SuspendReasonCode'),
                    text: this.getControlValue('SuspendReasonCode') + ' - ' + this.getControlValue('SuspendReasonDesc')
                };
            }
            this.inputParams = this.pageParams.inputParams;
            this.vbEnableCreditSuspendService = this.pageParams.vbEnableCreditSuspendService;
            this.strSuspendStartDate = this.pageParams.strSuspendStartDate;
            this.strSuspendEndDate = this.pageParams.strSuspendEndDate;
            this.initialMode();
            this.checkContractType();
        } else {
            this.getSysCharDtetails();
            this.windowOnLoad();
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private getSysCharDtetails(): void {
        let sysCharNumbers = [this.sysCharConstants.SystemCharCreditSuspendService];
        let sysCharIp = {
            module: this.queryParams.module,
            operation: this.queryParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharNumbers.toString()
        };
        this.speedScript.sysChar(sysCharIp).subscribe((data) => {
            this.vbEnableCreditSuspendService = data.records[0].Logical;
        });
    }

    private windowOnLoad(): void {
        this.contractSearchEllipsis.openModal();
        this.initialMode();
    }

    //Page initial mode with fields in disabled mode except primary fields
    private initialMode(): void {
        let disbleIgnoreControls: Array<string> = [
            'ContractNumber',
            'PremiseNumber',
            'ProductCode'
        ];
        this.pageParams.suspendReasonSelected = {
            id: '',
            text: ''
        };
        this.isSuspendReasonSearchDisabled = true;
        this.isCmdSuspend = true;
        this.isSaveDisable = true;
        this.isCancelDisable = true;
        this.isCmdCancel = true;
        this.pageParams.isCmdSuspendShow = false;
        this.pageParams.isCmdCancelShow = false;
        this.pageParams.isAddMode = false;
        this.setControlValue('Menu', 'options');
        this.disableControls(disbleIgnoreControls);
        this.disableControl('ContractNumber', false);
        this.disableControl('PremiseNumber', false);
        this.disableControl('ProductCode', false);
    }

    // after fetch the data disable primary fields and only editable fields will be enabled
    private enableFormToEditMode(): void {
        let enableIgnoreControls: Array<string> = [
            'ContractName',
            'PremiseName',
            'ProductDesc',
            'Status',
            'ServiceVisitFrequency',
            'ServiceAnnualValue',
            'ServiceQuantity',
            'ServiceCommenceDate',
            'ServiceVisitAnnivDate',
            'AccountBalance',
            'UpdatedAnnivDate'
        ];
        this.enableControls(enableIgnoreControls);
        if (this.getControlValue('SuspendReasonCode')) {
            this.pageParams.suspendReasonSelected = {
                id: this.getControlValue('SuspendReasonCode'),
                text: this.getControlValue('SuspendReasonCode') + ' - ' + this.getControlValue('SuspendReasonDesc')
            };
        }
        this.isSuspendReasonSearchDisabled = false;
        this.isCmdSuspend = false;
        this.isSaveDisable = false;
        this.isCancelDisable = false;
        this.isCmdCancel = false;
        this.setControlValue('Menu', 'options');
        this.setControlValue('ServiceVisitAnnivDateInd', false);
        this.setControlValue('UpdatedAnnivDate', '');
        this.disableControl('ContractNumber', true);
        this.disableControl('PremiseNumber', true);
        this.disableControl('ProductCode', true);
        this.disableControl('SuspendQuantity', this.getControlValue('SuspendQuantity') === 'yes' ? false : true);
    }

    // before update event
    private beforeUpdate(): void {
        this.enableFormToEditMode();
        if (this.parentMode === 'CancelSuspension') {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SuspendStartDate', false);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SuspendEndDate', false);
            this.pageParams.isSuspendStartDateReq = false;
            this.pageParams.isSuspendEndDateReq = false;
            this.disableControl('SuspendStartDate', true);
            this.disableControl('SuspendEndDate', true);
            this.disableControl('ServiceVisitAnnivDateInd', true);
            this.setFocusOnUpdatedAnnivDate.emit(true);
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SuspendStartDate', true);
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SuspendEndDate', true);
            this.pageParams.isSuspendStartDateReq = true;
            this.pageParams.isSuspendEndDateReq = true;
            this.disableControl('UpdatedAnnivDate', true);
            this.disableControl('SuspendQuantity', true);
        }
        this.pageParams.isCmdSuspendShow = false;
        this.pageParams.isCmdCancelShow = false;
        if ((this.getControlValue('QuantityRequired') === 'yes') && (((this.getControlValue('SuspendStartDate') === '' && this.getControlValue('SuspendEndDate')) === '') || this.parentMode === 'AddSuspension')) {
            this.disableControl('SuspendQuantity', false);
            this.setFocusOnSuspendQuantity.emit(true);
            this.setControlValue('SuspendQuantity', '0');
            if (this.vbEnableCreditSuspendService) {
                this.setControlValue('CreditSuspendServiceInd', true);
            }
        }
        if (!this.pageParams.isAddMode)
            this.showButtons();
    }

    //show or hide add and cancel button
    private showButtons(): void {
        let suspendStartDate: Date = new Date(this.getControlValue('SuspendStartDate'));
        let suspendEndDate: Date = new Date(this.getControlValue('SuspendEndDate'));
        let today: Date = new Date();
        if ((this.getControlValue('SuspendStartDate') === '') || (this.getControlValue('SuspendEndDate') === ''))
            this.pageParams.isCmdSuspendShow = true;
        //todo date comparison
        else if ((suspendStartDate <= today) && (suspendEndDate >= today))
            this.pageParams.isCmdSuspendShow = false;
        else
            this.pageParams.isCmdSuspendShow = true;
        if (this.getControlValue('SuspendStartDate') === '')
            this.pageParams.isCmdCancelShow = false;
        else
            this.pageParams.isCmdCancelShow = true;
    }

    //After mandatory fields check for contract type
    private checkContractType(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        search.set('ContractTypeCode', this.currentContractType);
        formData[this.serviceConstants.Function] = 'CheckContractType';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else
                    this.populateData();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //populate data in the form
    private populateData(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        formData['ServiceCoverROWID'] = this.getControlValue('ServiceCoverRowID');
        formData['Level'] = 'ServiceCover';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.uiForm.patchValue(data);
                    this.setControlValue('CreditSuspendServiceInd', this.utils.convertResponseValueToCheckboxInput(data.CreditSuspendServiceInd));
                    if (this.getControlValue('SuspendReasonCode')) {
                        this.pageParams.suspendReasonSelected = {
                            id: this.getControlValue('SuspendReasonCode'),
                            text: this.getControlValue('SuspendReasonCode') + ' - ' + this.getControlValue('SuspendReasonDesc')
                        };
                    } else {
                        this.pageParams.suspendReasonSelected = {
                            id: '',
                            text: ''
                        };
                    }
                    this.lookUpData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //Lookup to get all data
    private lookUpData(): void {
        let lookupIP: Array<any> = [{
            'table': 'Contract',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'BusinessCode ': this.businessCode()
            },
            'fields': ['ContractName']
        },
        {
            'table': 'Premise',
            'query': {
                'ContractNumber': this.getControlValue('ContractNumber'),
                'PremiseNumber': this.getControlValue('PremiseNumber'),
                'BusinessCode ': this.businessCode()
            },
            'fields': ['PremiseName']
        },
        {
            'table': 'Product',
            'query': {
                'ProductCode': this.getControlValue('ProductCode'),
                'BusinessCode ': this.businessCode()
            },
            'fields': ['ProductDesc', 'QuantityRequired']
        }];
        this.LookUp.lookUpPromise(lookupIP).then(
            (data) => {
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data && data.length > 0) {
                        if (data[0] && data[0].length > 0) {
                            this.setControlValue('ContractName', data[0][0].ContractName || '');
                            this.inputParams.premiseParams.ContractName = this.getControlValue('ContractName');
                            this.inputParams.serviceCoverSearchParams.ContractName = this.getControlValue('ContractName');
                        }
                        if (data[1] && data[1].length > 0) {
                            this.setControlValue('PremiseName', data[1][0].PremiseName || '');
                            this.inputParams.serviceCoverSearchParams.PremiseName = this.getControlValue('PremiseName');
                        }
                        if (data[2] && data[2].length > 0) {
                            this.setControlValue('ProductDesc', data[2][0].ProductDesc || '');
                            this.inputParams.serviceCoverSearchParams.ProductDesc = this.getControlValue('ProductDesc');
                            this.setControlValue('QuantityRequired', data[2][0].QuantityRequired ? 'yes' : 'no' || '');
                            this.getStatus();
                        }
                    }
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //function to get status
    private getStatus(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
        formData[this.serviceConstants.Function] = 'GetStatus';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.setControlValue('Status', data.Status);
                    this.strSuspendStartDate = this.getControlValue('SuspendStartDate');
                    this.strSuspendEndDate = this.getControlValue('SuspendEndDate');
                    this.beforeUpdate();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //CheckPlanVisit function
    private checkPlanVisit(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
        search.set('OrigSuspendStartDate', this.globalize.parseDateToFixedFormat(this.strSuspendStartDate).toString());
        search.set('OrigSuspendEndDate', this.globalize.parseDateToFixedFormat(this.strSuspendEndDate).toString());
        search.set('SuspendStartDate', this.globalize.parseDateToFixedFormat(this.getControlValue('SuspendStartDate')).toString());
        search.set('SuspendEndDate', this.globalize.parseDateToFixedFormat(this.getControlValue('SuspendEndDate')).toString());
        formData[this.serviceConstants.Function] = 'CheckPlanVisit';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data.DisplayMessage !== '') {
                        let modalVO: ICabsModalVO = new ICabsModalVO();
                        modalVO.msg = data.DisplayMessage + MessageConstant.PageSpecificMessage.doYouWishToContinue;
                        modalVO.confirmCallback = this.validateSuspendQuantity.bind(this);
                        this.modalAdvService.emitPrompt(modalVO);
                    } else {
                        this.validateSuspendQuantity();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //ValidateSuspendQuantity function
    private validateSuspendQuantity(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
        search.set('SuspendStartDate', this.globalize.parseDateToFixedFormat(this.getControlValue('SuspendStartDate')).toString());
        search.set('SuspendEndDate', this.globalize.parseDateToFixedFormat(this.getControlValue('SuspendEndDate')).toString());
        formData[this.serviceConstants.Function] = 'ValidateSuspendQuantity';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data.DisplayMessage !== '') {
                        this.modalAdvService.emitMessage(data.DisplayMessage);
                    } else {
                        this.validateData();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // on confirm to save data
    private promptConfirm(data: any): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        formData['ServiceVisitAnnivDateInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('ServiceVisitAnnivDateInd'));
        formData['UpdatedAnnivDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('UpdatedAnnivDate'));
        formData['ErrorMessageDesc'] = this.getControlValue('ErrorMessageDesc');
        formData['AccountBalance'] = this.globalize.parseCurrencyToFixedFormat(this.getControlValue('AccountBalance'));
        formData['ServiceCoverROWID'] = this.getControlValue('ServiceCoverRowID');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        formData['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        formData['ServiceVisitFrequency'] = this.getControlValue('ServiceVisitFrequency');
        formData['ServiceQuantity'] = this.getControlValue('ServiceQuantity');
        formData['ServiceAnnualValue'] = this.globalize.parseCurrencyToFixedFormat(this.getControlValue('ServiceAnnualValue'));
        formData['ServiceCommenceDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceCommenceDate'));
        formData['ServiceVisitAnnivDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ServiceVisitAnnivDate'));
        formData['SuspendStartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SuspendStartDate'));
        formData['SuspendEndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('SuspendEndDate'));
        formData['SuspendLocation'] = this.getControlValue('SuspendLocation');
        formData['SuspendQuantity'] = this.getControlValue('SuspendQuantity');
        formData['SuspendReasonCode'] = this.getControlValue('SuspendReasonCode');
        formData['SuspendReasonDesc'] = this.getControlValue('SuspendReasonDesc');
        formData['CreditSuspendServiceInd'] = this.utils.convertCheckboxValueToRequestValue(this.getControlValue('CreditSuspendServiceInd'));
        formData['Mode'] = this.pageParams.isAddMode ? 'AddSuspension' : 'LookUp';
        formData['Level'] = 'ServiceCover';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data && data.ServiceVisitAnnivDate) {
                        this.formPristine();
                        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                        this.setControlValue('ServiceVisitAnnivDate', data.ServiceVisitAnnivDate);
                        this.setControlValue('UpdatedAnnivDate', '');
                        this.setControlValue('ServiceVisitAnnivDateInd', false);
                        this.strSuspendStartDate = this.getControlValue('SuspendStartDate');
                        this.strSuspendEndDate = this.getControlValue('SuspendEndDate');
                        this.parentMode = 'Normal';
                        this.showButtons();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //warn cancel
    private warnCancel(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
        formData[this.serviceConstants.Function] = 'WarnCancel';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError && data.ErrorMessageDesc) {
                    this.setControlValue('ErrorMessageDesc', data.ErrorMessageDesc);
                    let modalVO: ICabsModalVO = new ICabsModalVO();
                    modalVO.msg = data['ErrorMessageDesc'];
                    modalVO.title = MessageConstant.PageSpecificMessage.serviceCoverServiceSuspendmaintenance.cancelSuspension;
                    modalVO.confirmCallback = this.cancelSuspension.bind(this);
                    this.modalAdvService.emitPrompt(modalVO);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //cancel suspension
    private cancelSuspension(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID'));
        search.set('CreditSuspendServiceInd', this.utils.convertCheckboxValueToRequestValue(this.getControlValue('CreditSuspendServiceInd')));
        search.set('Level', 'ServiceCover');
        formData[this.serviceConstants.Function] = 'CancelSuspension';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data) {
                        this.uiForm.controls['SuspendStartDate'].markAsUntouched();
                        this.uiForm.controls['SuspendEndDate'].markAsUntouched();
                        this.uiForm.patchValue(data);
                        if (this.getControlValue('SuspendReasonCode') === '') {
                            this.pageParams.suspendReasonSelected = {
                                id: '',
                                text: ''
                            };
                        } else {
                            this.pageParams.suspendReasonSelected = {
                                id: this.getControlValue('SuspendReasonCode'),
                                text: this.getControlValue('SuspendReasonCode') + ' - ' + this.getControlValue('SuspendReasonDesc')
                            };
                        }
                        this.setControlValue('CreditSuspendServiceInd', this.utils.convertResponseValueToCheckboxInput(data.CreditSuspendServiceInd));
                        this.setControlValue('ServiceVisitAnnivDateInd', this.utils.convertResponseValueToCheckboxInput(data.ServiceVisitAnnivDateInd));
                        this.formPristine();
                        this.parentMode = '';
                        this.showButtons();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //public check for end dates
    private suspendEndDateCallBack(): void {
        let modalVO: ICabsModalVO = new ICabsModalVO();
        modalVO.msg = MessageConstant.PageSpecificMessage.serviceCoverServiceSuspendmaintenance.endDateHasChanged;
        modalVO.title = MessageConstant.PageSpecificMessage.serviceCoverServiceSuspendmaintenance.suspendDatesChange;
        if (this.getControlValue('SuspendQuantity') !== 0) {
            modalVO.closeCallback = this.checkPlanVisit.bind(this);
        } else {
            modalVO.closeCallback = this.validateData.bind(this);
        }
        setTimeout(function (): void {
            this.modalAdvService.emitMessage(modalVO);
        }.bind(this), 0);
    }

    //function to validate form and confirm popup
    private validateData(): void {
        if (this.riExchange.validateForm(this.uiForm)) {
            setTimeout(function (): void {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.promptConfirm.bind(this)));
            }.bind(this), 0);
        }
    }

    //getservicecoverrowid on product change
    private getServiceCoverRowID(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        search.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        search.set(this.serviceConstants.ProductCode, this.getControlValue('ProductCode'));
        formData[this.serviceConstants.Function] = 'GETSERVICECOVERROWID';
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data && data.ServiceCoverNumber) {
                        if (data.ServiceCoverNumber === '-1') {
                            this.sCSearchEllipsis.openModal();
                        } else {
                            this.setControlValue('ServiceCoverNumber', data.ServiceCoverNumber);
                            this.setControlValue('ServiceCoverRowID', data.ServiceCoverRowID);
                            this.checkContractType();
                        }
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    // modify the input params
    private assignInputParams(ctrlName: string): void {
        switch (ctrlName) {
            case 'ContractNumber':
                this.inputParams.premiseParams.ContractNumber = this.getControlValue('ContractNumber');
                this.inputParams.premiseParams.ContractName = this.getControlValue('ContractName');
                this.inputParams.serviceCoverSearchParams.ContractNumber = this.getControlValue('ContractNumber');
                this.inputParams.serviceCoverSearchParams.ContractName = this.getControlValue('ContractName');
                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                this.setControlValue('ProductCode', '');
                this.setControlValue('ProductDesc', '');
                this.setControlValue('ServiceCoverRowID', '');
                break;
            case 'PremiseNumber':
                this.inputParams.serviceCoverSearchParams.PremiseNumber = this.getControlValue('PremiseNumber');
                this.inputParams.serviceCoverSearchParams.PremiseName = this.getControlValue('PremiseName');
                this.setControlValue('ProductCode', '');
                this.setControlValue('ProductDesc', '');
                this.setControlValue('ServiceCoverRowID', '');
                break;
            case 'ProductCode':
                this.inputParams.serviceCoverSearchParams.ProductCode = this.getControlValue('ProductCode');
                this.inputParams.serviceCoverSearchParams.ProductDesc = this.getControlValue('ProductDesc');
                break;
        }
    }

    //Save button event
    public beforeSave(): void {
        let modalVO: ICabsModalVO = new ICabsModalVO();
        if (this.getControlValue('SuspendStartDate') !== this.strSuspendStartDate) {
            modalVO.msg = MessageConstant.PageSpecificMessage.serviceCoverServiceSuspendmaintenance.startDateHasChanged;
            modalVO.title = MessageConstant.PageSpecificMessage.serviceCoverServiceSuspendmaintenance.suspendDatesChange;
            if (this.getControlValue('SuspendEndDate') !== this.strSuspendEndDate)
                modalVO.closeCallback = this.suspendEndDateCallBack.bind(this);
            else {
                if (this.getControlValue('SuspendQuantity') !== 0) {
                    modalVO.closeCallback = this.checkPlanVisit.bind(this);
                } else {
                    modalVO.closeCallback = this.validateData.bind(this);
                }
            }
            this.modalAdvService.emitMessage(modalVO);
        } else if (this.getControlValue('SuspendEndDate') !== this.strSuspendEndDate) {
            this.suspendEndDateCallBack();
        } else {
            if (this.getControlValue('SuspendQuantity') !== 0) {
                this.riExchange.validateForm(this.uiForm);
                this.checkPlanVisit();
            } else {
                this.validateData();
            }
        }
    }

    //addtional cancel event
    public onCmdCancelClick(): void {
        if (this.getControlValue('SuspendStartDate') !== '') {
            this.warnCancel();
        }
    }

    //Add button event
    public onAddClick(): void {
        this.formPristine();
        this.uiForm.controls['SuspendStartDate'].markAsUntouched();
        this.uiForm.controls['SuspendEndDate'].markAsUntouched();
        this.parentMode = 'AddSuspension';
        this.pageParams.isAddMode = true;
        this.pageParams.isCmdSuspendShow = false;
        this.disableControl('Menu', true);
        if (this.vbEnableCreditSuspendService)
            this.setControlValue('CreditSuspendServiceInd', true);
        this.checkContractType();
    }

    //cancel button click
    public onCancelClick(): void {
        this.formPristine();
        this.uiForm.controls['SuspendStartDate'].markAsUntouched();
        this.uiForm.controls['SuspendEndDate'].markAsUntouched();
        this.parentMode = 'Normal';
        this.checkContractType();
        if (this.pageParams.isAddMode) {
            this.pageParams.isAddMode = false;
            this.disableControl('Menu', false);
        }
    }

    //End date change event
    public onSuspendEndDateChange(data: any): void {
        this.onServiceVisitAnnivDateIndCheck();
        this.uiForm.controls['SuspendEndDate'].markAsDirty();
    }

    //start date change event
    public onSuspendStartDateChange(data: any): void {
        this.uiForm.controls['SuspendStartDate'].markAsDirty();
    }

    //ServiceVisitAnnivDateInd checkbox change event
    public onServiceVisitAnnivDateIndCheck(): void {
        if (this.parentMode !== 'CancelSuspension') {
            if (this.getControlValue('ServiceVisitAnnivDateInd') && (this.getControlValue('SuspendEndDate') !== '')) {
                let updatedDate: Date = this.utils.addDays(this.getControlValue('SuspendEndDate'), 1);
                this.setControlValue('UpdatedAnnivDate', updatedDate);
            }
            else {
                this.setControlValue('UpdatedAnnivDate', '');
            }
        }
    }

    //event on ellipsis data select
    public ellipsisData(data: any, cntrlName: string): void {
        let cnumber, cname, pname: string;
        let pnumber: number;
        cnumber = this.getControlValue('ContractNumber');
        cname = this.getControlValue('ContractName');
        pnumber = this.getControlValue('PremiseNumber');
        pname = this.getControlValue('PremiseName');
        if (data) {
            switch (cntrlName) {
                case 'ContractNumber':
                    this.uiForm.reset();
                    this.setControlValue('ContractNumber', data.ContractNumber);
                    this.setControlValue('ContractName', data.ContractName);
                    this.assignInputParams('ContractNumber');
                    this.assignInputParams('PremiseNumber');
                    this.assignInputParams('ProductCode');
                    this.initialMode();
                    break;
                case 'PremiseNumber':
                    this.uiForm.reset();
                    this.setControlValue('ContractNumber', cnumber);
                    this.setControlValue('ContractName', cname);
                    this.setControlValue('PremiseNumber', data.PremiseNumber);
                    this.setControlValue('PremiseName', data.PremiseName);
                    this.assignInputParams('PremiseNumber');
                    this.assignInputParams('ProductCode');
                    this.initialMode();
                    break;
                case 'ProductCode':
                    this.uiForm.reset();
                    this.setControlValue('ContractNumber', cnumber);
                    this.setControlValue('ContractName', cname);
                    this.setControlValue('PremiseNumber', pnumber);
                    this.setControlValue('PremiseName', pname);
                    this.setControlValue('ProductCode', data.ProductCode);
                    this.setControlValue('ProductDesc', data.ProductDesc);
                    this.setControlValue('ServiceCoverRowID', data.row.ttServiceCover);
                    this.assignInputParams('ProductCode');
                    this.initialMode();
                    this.checkContractType();
                    break;
            }
        }
    }

    //events for onchange
    public onContractChange(): void {
        this.uiForm.controls['ContractNumber'].markAsPristine();
        this.uiForm.controls['PremiseNumber'].markAsUntouched();
        this.uiForm.controls['ProductCode'].markAsUntouched();
        this.assignInputParams('ContractNumber');
        this.assignInputParams('PremiseNumber');
        this.assignInputParams('ProductCode');
    }

    public onPremiseChange(): void {
        this.uiForm.controls['PremiseNumber'].markAsPristine();
        this.uiForm.controls['ProductCode'].markAsUntouched();
        this.assignInputParams('PremiseNumber');
        this.assignInputParams('ProductCode');
    }

    public onProductChange(): void {
        this.uiForm.controls['ProductCode'].markAsPristine();
        this.assignInputParams('ProductCode');
        if (this.getControlValue('ProductCode') !== '') {
            if (this.riExchange.validateForm(this.uiForm))
                this.getServiceCoverRowID();
        }
    }

    // suspendReason dropdown change event
    public suspendReasonSearchDataRecieved(event: any): void {
        this.uiForm.controls['SuspendReasonCode'].markAsDirty();
        this.uiForm.controls['SuspendReasonDesc'].markAsDirty();
        if (event.SuspendReasonCode) {
            this.setControlValue('SuspendReasonCode', event.SuspendReasonCode);
            this.setControlValue('SuspendReasonDesc', event.SuspendReasonDesc);
            this.pageParams.suspendReasonSelected = {
                id: event.SuspendReasonCode,
                text: event.SuspendReasonCode + ' - ' + event.SuspendReasonDesc
            };
        } else {
            this.setControlValue('SuspendReasonCode', '');
            this.setControlValue('SuspendReasonDesc', '');
            this.pageParams.suspendReasonSelected = {
                id: '',
                text: ''
            };
        }
    }

    // option menu change event
    public onMenuChange(): void {
        this.uiForm.controls['Menu'].markAsPristine();
        let menuSelected: string = this.getControlValue('Menu');
        this.pageParams.inputParams = this.inputParams;
        this.pageParams.vbEnableCreditSuspendService = this.vbEnableCreditSuspendService;
        this.pageParams.strSuspendStartDate = this.strSuspendStartDate;
        this.pageParams.strSuspendEndDate = this.strSuspendEndDate;
        switch (menuSelected) {
            case 'PlanVisit':
                this.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSAPLANVISITGRIDYEAR, {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'ProductCode': this.getControlValue('ProductCode'),
                    'CurrentContractTypeURLParameter': '<contract>',
                    'ServiceCoverRowID': this.getControlValue('ServiceCoverRowID')
                });
                break;
            case 'StaticVisit':
                this.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSASTATICVISITGRIDYEAR, {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'ContractName': this.getControlValue('ContractName'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'PremiseName': this.getControlValue('PremiseName'),
                    'ProductCode': this.getControlValue('ProductCode'),
                    'ProductDesc': this.getControlValue('ProductDesc'),
                    'ServiceCoverRowID': this.getControlValue('ServiceCoverRowID'),
                    'currentContractTypeURLParameter': this.currentContractType
                });
                break;
        }
    }
}
