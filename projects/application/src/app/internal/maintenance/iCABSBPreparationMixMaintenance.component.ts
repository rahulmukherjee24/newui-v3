import { Component, Injector, OnInit, ViewChild, Renderer } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { PageIdentifier } from '../../base/PageIdentifier';
import { BaseComponent } from '../../base/BaseComponent';
import { PreparationSearchComponent } from '../../internal/search/iCABSBPreparationSearch.component';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { ErrorConstant } from '../../../shared/constants/error.constant';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { GlobalConstant } from '../../../shared/constants/global.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBPreparationMixMaintenance.html'
})

export class PreparationMixMaintenanceComponent extends BaseComponent implements OnInit {
    private requestParams: Object = {
        operation: 'Business/iCABSBPreparationMixMaintenance',
        module: 'preps',
        method: 'service-delivery/admin'
    };
    private queryParams: Object;
    private search: QueryParams = new QueryParams();
    private queryLookUp: QueryParams = new QueryParams();
    private querySysChar: QueryParams = new QueryParams();
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('prepMixCode') public prepMixCode;
    @ViewChild('prepCode') public prepCode;
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'PrepCode', readonly: false, disabled: false, required: false, type: MntConst.eTypeCode },
        { name: 'PrepDesc', readonly: false, disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PrepMixCode', readonly: false, disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'PrepMixLabel', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'MeasureBy', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'Formulation', readonly: false, disabled: false, required: true, type: MntConst.eTypeText },
        { name: 'MixRatio', readonly: false, disabled: false, required: true, type: MntConst.eTypeDecimal5 },
        { name: 'Save', readonly: false, exclude: true, disabled: false, required: false },
        { name: 'Cancel', readonly: false, exclude: true, disabled: false, required: false },
        { name: 'Add', readonly: false, exclude: true, disabled: false, required: false },
        { name: 'Delete', readonly: false, exclude: true, disabled: false, required: false }
    ];
    public pageParams: Object = {
        fieldVisibility: {
            trMiniumumLevyValue: false
        },
        fieldRequired: {
            PrepMixCode: true,
            PrepMixLabel: true,
            MeasureBy: true,
            Formulation: true,
            MixRatio: true
        },
        functionAdd: true
    };
    public modalConfig: Object = {
        backdrop: 'static',
        keyboard: true
    };
    public ellipsis: any = {
        prepCodeEllipsis: {
            autoOpen: false,
            inputParams: {
                parentMode: 'LookUp',
                showAddNew: false
            },
            contentComponent: PreparationSearchComponent,
            showHeader: true,
            isEllipsisDisabled: false
        }
    };
    constructor(injector: Injector, private renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPREPARATIONMIXMAINTENANCE;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.parentMode = this.riExchange.getParentMode();
        this.utils.setTitle('Preparation Mix Maintenance');
        this.fetchTranslationContent();
        this.postInit();
    }

    /**
     * This function is called when initialisation completes, it loads default data and process modes
     */
    private postInit(): void {
        if (this.parentMode === 'PrepMixAdd') {
            this.formMode = this.c_s_MODE_ADD;
            this.processMode();
            this.setControlValue('PrepCode', this.riExchange.getParentHTMLValue('PrepCode'));
            this.setControlValue('PrepDesc', this.riExchange.getParentHTMLValue('PrepDesc'));
        } else if (this.parentMode === 'PrepMixUpdate') {
            this.pageParams['RowID'] = this.riExchange.getParentHTMLValue('RowID') || this.riExchange.getParentHTMLValue('ROWID');
            this.pageParams['functionAdd'] = false;
            this.triggerUpdateMode();
        }
    }
    // This function triggers select mode
    private triggerSelectMode(): void {
        this.formMode = this.c_s_MODE_SELECT;
        this.processMode();
    }

    // This function triggers update mode
    private triggerUpdateMode(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxGet('', { ROWID: this.pageParams['RowID'], action: '0' }).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (!data['hasError']) {
                this.updateModeInternals(data);
            } else {
                this.triggerSelectMode();
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }

    // This function triggers update mode
    private triggerUpdateModeFromPrepMix(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxGet('', { PrepMixCode: this.getControlValue('PrepMixCode'), ROWID: '~~ignore~~', action: '0' }).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (!data['hasError']) {
                if (data['ttPrepMix']) {
                    this.updateModeInternals(data);
                    this.pageParams['RowID'] = data['ttPrepMix'];
                }
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    // This functions implements update mode
    private updateModeInternals(data: any): void {
        this.formMode = this.c_s_MODE_UPDATE;
        this.processMode();
        this.formPristine();
        this.populateFormAndParams(data);
        this.populateDescription();
    }

    /**
     * This function triggers when mode is changed
     */
    private processMode(): void {
        this.formReset();
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                this.enableControls(['PrepDesc']);
                this.ellipsis['prepCodeEllipsis'].isEllipsisDisabled = false;
                this.fieldFocus(this.prepMixCode);
                break;

            case this.c_s_MODE_UPDATE:
                this.enableControls(['PrepMixCode', 'PrepDesc']);
                this.uiForm.controls['PrepMixCode'].disable();
                this.ellipsis['prepCodeEllipsis'].isEllipsisDisabled = false;
                this.fieldFocus(this.prepCode);
                break;
            case this.c_s_MODE_SELECT:
                this.disableControls([]);
                this.ellipsis['prepCodeEllipsis'].isEllipsisDisabled = true;
                if (this.pageParams['functionAdd']) {
                    this.uiForm.controls['PrepMixCode'].enable();
                    this.uiForm.controls['Add'].enable();
                } else {
                    this.uiForm.controls['PrepMixCode'].disable();
                    this.uiForm.controls['Add'].disable();
                }
                this.fieldFocus(this.prepMixCode);
                break;
        }
    }
    // This function brings focus to the passed input field
    private fieldFocus(input: any): void {
        setTimeout(() => {
            let focus = new CustomEvent('focus', { bubbles: true });
            this.renderer.invokeElementMethod(input.nativeElement, 'focus', [focus]);
        }, 0);
    }

    /**
     * This function resets the form to empty
     */
    private formReset(): void {
        this.uiForm.reset();
        this.formPristine();
        this.setTranslatedValues();
    }

    /**
     * This function toggles disable/enable attribute of action buttons
     */
    private toggleeActionBtn(isDisable: boolean): void {
        if (isDisable) {
            this.uiForm.controls['Save'].disable();
            this.uiForm.controls['Cancel'].disable();
            this.uiForm.controls['Add'].disable();
            this.uiForm.controls['Delete'].disable();
        } else {
            this.uiForm.controls['Save'].enable();
            this.uiForm.controls['Cancel'].enable();
            this.uiForm.controls['Add'].enable();
            this.uiForm.controls['Delete'].enable();
        }
    }

    /**
     * This function sets the translated values of the action buttons
     */
    private setTranslatedValues(): void {
        this.setControlValue('Save', this.pageParams['Save']);
        this.setControlValue('Add', this.pageParams['Add']);
        this.setControlValue('Delete', this.pageParams['Delete']);
        this.setControlValue('Cancel', this.pageParams['Cancel']);
    }
    /**
     * This function fetches the description of prep code
     */
    private populateDescription(returnObservable?: boolean): any {
        if (returnObservable) {
            return this.ajaxGet('', { PrepCode: this.getControlValue('PrepCode'), action: '0' });
        }
        this.ajaxGet('', { PrepCode: this.getControlValue('PrepCode'), action: '0' }).subscribe((data) => {
            this.populateDescriptionInternals(data);
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function processes the data recived from prep service
     */
    private populateDescriptionInternals(data: any): void {
        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
        if (this.getControlValue('PrepCode')) {
            if (!data['hasError']) {
                this.setControlValue('PrepDesc', data['PrepDesc']);
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PrepCode', false);
                this.uiForm.controls['PrepCode'].updateValueAndValidity();
            } else {
                this.setControlValue('PrepDesc', '');
                this.uiForm.controls['PrepCode'].setErrors({});
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PrepCode', true);
            }
        } else {
            this.setControlValue('PrepDesc', '');
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PrepCode', false);
            this.uiForm.controls['PrepCode'].updateValueAndValidity();
        }
    }

    /**
     * This function populates form data and page params with data recieved from connector
     * @param data
     */
    private populateFormAndParams(data: Object): void {
        if (data && !data['hasError']) {
            for (let i in data) {
                if (data.hasOwnProperty(i)) {
                    data[i] = !this.utils.isFalsy(data[i]) ? data[i].toString().trim() : '';
                    if (this.uiForm.controls[i] !== undefined && this.uiForm.controls[i] !== null) {
                        this.setControlValue(i, data[i].trim());
                        if (data[i].toUpperCase() === GlobalConstant.Configuration.Yes || data[i].toUpperCase() === GlobalConstant.Configuration.True) {
                            this.setControlValue(i, true);
                        } else if (data[i].toUpperCase() === GlobalConstant.Configuration.No || data[i].toUpperCase() === GlobalConstant.Configuration.False) {
                            this.setControlValue(i, false);
                        }
                    }
                    if (typeof this.pageParams[i] !== 'undefined') {
                        this.pageParams[i] = data[i];
                    }
                }
            }
        }
    }

    /**
     * This is an API function for GET requests triggered in the screen
     * @param functionName
     * @param params
     */
    private ajaxGet(functionName: string, params: Object): Observable<any> {
        let requestParams = new QueryParams();
        requestParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        requestParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            requestParams.set(this.serviceConstants.Action, '6');
            requestParams.set('Function', functionName);
        }
        for (let key in params) {
            if (key) {
                requestParams.set(key, params[key]);
            }
        }
        return this.httpService.makeGetRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], requestParams);
    }
    /**
     * This is an API function for POST requests triggered in the screen
     * @param functionName
     * @param params
     * @param formData
     */
    private ajaxPost(functionName: string, params: Object, formData: Object): Observable<any> {
        let requestParams = new QueryParams();
        requestParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        requestParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            requestParams.set(this.serviceConstants.Action, '6');
            formData['Function'] = functionName;
        }
        for (let key in params) {
            if (key) {
                requestParams.set(key, params[key]);
            }
        }
        return this.httpService.makePostRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], requestParams, formData);
    }
    /**
     * This is an API function for Look-up requests triggered in the screen
     * @param data
     * @param maxresults
     */
    private lookUpRecord(data: Object, maxresults?: number): Observable<any> {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        } else {
            this.queryLookUp.set(this.serviceConstants.MaxResults, '100');
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }

    // This function will be called after delete operation is successful
    private afterDelete(): void {
        this.triggerSelectMode();
    }

    /**
     * This is batch request to fetch translation
     */
    private fetchTranslationContent(): void {
        this.getTranslatedValuesBatch((data: any) => {
            if (data) {
                this.pageParams['Save'] = data[0];
                this.pageParams['Add'] = data[1];
                this.pageParams['Cancel'] = data[2];
                this.pageParams['Delete'] = data[3];
            }
        },
            ['Save'], ['Add'], ['Cancel'], ['Delete']);
    }

    /**
     * This function triggers before save operation
     */
    private beforeSave(): boolean {
        for (let i in this.uiForm.controls) {
            if (this.uiForm.controls.hasOwnProperty(i)) {
                this.uiForm.controls[i].markAsTouched();
                if (!this.uiForm.controls[i].enabled) {
                    this.uiForm.controls[i].clearValidators();
                }
                this.uiForm.controls[i].updateValueAndValidity();
            }
        }
        this.uiForm.updateValueAndValidity();
        return this.uiForm.valid;
    }

    /**
     * This function triggers after Save service returns
     */
    private afterSave(): void {
        if (this.formMode === this.c_s_MODE_ADD) {
            this.triggerUpdateMode();
        }
    }

    /**
     * This function triggers when add button is clicked
     */
    public addOnClick(): void {
        this.formMode = this.c_s_MODE_ADD;
        this.processMode();
    }
    /**
     * This function triggers when delete button is clicked
     */
    public deleteOnClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteOperation.bind(this)));
    }
    /**
     * This function has the core delete functionality
     */
    public deleteOperation(): void {
        let formdata: Object = {
            ROWID: this.pageParams['RowID']
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxPost('', { action: 3 }, formdata).subscribe((data) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (!data['hasError']) {
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully);
                modalVO.closeCallback = this.afterDelete.bind(this);
                this.modalAdvService.emitMessage(modalVO);
                this.pageParams['RowID'] = '';
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function triggers when save button is clicked
     */
    public saveOnClick(): void {
        let valid: boolean = true;
        valid = this.beforeSave();
        if (valid) {
            if (this.getControlValue('PrepCode')) {
                this.populateDescription(true).subscribe((data) => {
                    this.populateDescriptionInternals(data);
                    if (this.uiForm.valid) {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveOperation.bind(this)));
                    }
                }, (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                });
            } else {
                this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveOperation.bind(this)));
            }
        }
    }

    /**
     * This function is the save operation that gets triggerred when save is clicked
     */
    public saveOperation(): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        let formData: Object = {};
        let requestParam: Object = {};
        let controlsLength = this.controls.length;
        for (let i = 0; i < controlsLength; i++) {
            if (!this.controls[i]['exclude']) {
                formData[this.controls[i].name] = this.getControlValue(this.controls[i].name);
            }
        }
        if (!this.getControlValue('PrepCode')) {
            formData['PrepCode'] = '?';
        }
        if (this.formMode === this.c_s_MODE_ADD) {
            requestParam = {
                action: 1
            };
        } else {
            requestParam = {
                action: 2
            };
            formData['ROWID'] = this.pageParams['RowID'];
        }
        this.ajaxPost('', requestParam, formData).subscribe((data) => {
            if (data['status'] === GlobalConstant.Configuration.Failure) {
                this.modalAdvService.emitError(new ICabsModalVO(data['oResponse']));
            } else {
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.formPristine();
                    this.pageParams['RowID'] = data['ttPrepMix'];
                    let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully);
                    modalVO.closeCallback = this.afterSave.bind(this);
                    this.modalAdvService.emitMessage(modalVO);
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            }
        }, (error) => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }

    /**
     * This function triggers when an option is selected in business dropdown
     */
    public cancelOnClick(): void {
        switch (this.formMode) {
            case this.c_s_MODE_ADD:
                if (this.pageParams['RowID']) {
                    this.triggerUpdateMode();
                } else {
                    this.triggerSelectMode();
                }
                break;

            case this.c_s_MODE_UPDATE:
                this.triggerUpdateMode();
                break;
        }
    }

    /* Rreceiving data from PrepCode Ellipsis */
    public onPrepCodeDataReceived(data: Object): void {
        if (data && this.formMode !== this.c_s_MODE_UPDATE) {
            this.setControlValue('PrepCode', data['PrepCode']);
            this.setControlValue('PrepDesc', data['PrepDesc']);
        }
    }
    /**
     * This function is called when prep code changes
     */
    public onPrepCodeChange(): void {
        if (this.getControlValue('PrepCode')) {
            this.populateDescription();
        } else {
            this.setControlValue('PrepDesc', '');
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'PrepCode', false);
            this.uiForm.controls['PrepCode'].updateValueAndValidity();
        }
    }

    /**
     * This function is called when prep mix code changes
     */
    public onPrepMixCodeChange(): void {
        if (this.getControlValue('PrepMixCode') && this.formMode === this.c_s_MODE_SELECT) {
            this.triggerUpdateModeFromPrepMix();
        }
    }

    /**
     * This function triggers candeactive modal
     */
    public canDeactivate(): Observable<boolean> {
        this.routeAwayGlobals.setSaveEnabledFlag(this.uiForm.dirty);
        return this.routeAwayComponent.canDeactivate();
    }
}
