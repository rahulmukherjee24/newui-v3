/**
 * The iCABSSNotificationTemplateDetailMaintenance  to manage email templates
 * @author icabs ui team
 * @version 1.0
 * @since   01/30/2017
 */
import { Component, OnInit, Injector, ViewChild, OnDestroy, ElementRef } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { LanguageSearchComponent } from './../../internal/search/riMGLanguageSearch.component';
import { CommonDropdownComponent } from '../../../shared/components/common-dropdown/common-dropdown.component';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { QueryParams } from '@shared/services/http-params-wrapper';

// Class definition starts here
@Component({
    templateUrl: 'iCABSSNotificationTemplateDetailMaintenance.html'
})

export class NotificationTemplateDetailMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('templateCodeDropDown') public templateCodeDropDown: CommonDropdownComponent;

    private queryParams: any = {
        operation: 'System/iCABSSNotificationTemplateDetailMaintenance',
        module: 'notification',
        method: 'ccm/admin'
    };
    public pageId: string = '';
    public isDisplaySubjectText: boolean = true;
    public controls: Array<Object> = [
        { name: 'NotifyTemplateCode', required: true, value: '', type: MntConst.eTypeCode },
        { name: 'NotifyTemplateSystemDesc', required: false, value: '', type: MntConst.eTypeText },
        { name: 'LanguageCode', required: true, value: '', type: MntConst.eTypeCode },
        { name: 'LanguageDescription', required: false, value: '', type: MntConst.eTypeText },
        { name: 'NotifySubjectText', required: false, value: '', type: MntConst.eTypeTextFree },
        { name: 'NotifyBodyText', required: false, value: '', type: MntConst.eTypeTextFree },
        { name: 'CustomerContactNumber', required: false, value: '', type: MntConst.eTypeInteger },
        { name: 'rowid', required: false, value: '' },
        { name: 'NotifyTemplateType', value: '' }
    ];
    public isTestDisabled: boolean = false;
    public dropdown: any = {
        NotificationTemplateSearch: {
            isRequired: true,
            isDisabled: false,
            params: {
                operation: 'System/iCABSSNotificationTemplateSearch',
                module: 'notification',
                method: 'ccm/search'
            },
            displayFields: ['NotifyTemplateCode', 'NotifyTemplateSystemDesc'],
            isActive: {
                id: '',
                text: ''
            },
            isTriggerValidate: false
        }
    };

    public isShowButtons: any = {
        Add: true,
        Delete: true,
        Save: true,
        Cancel: true
    };
    public ellipsis: any = {
        LanguageSearch: {
            childConfigParams: {
                parentMode: 'LookUp'
            },
            component: LanguageSearchComponent,
            isHide: false,
            isDisabled: false
        }
    };

    //Lifecycle/Constructor methods definition
    constructor(injector: Injector, private el: ElementRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSNOTIFICATIONTEMPLATEDETAILMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Notification Template Detail Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.initData();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * This method is used to initialize all parameters during page load
     * @param void
     * @return void
     */
    private initData(): void {
        if (this.riExchange.getParentMode() === 'Add') {
            this.setFormMode(this.c_s_MODE_ADD);
            this.setControlValue('NotifyTemplateCode', this.riExchange.getParentHTMLValue('NotifyTemplateCode'));
            this.setControlValue('NotifyTemplateSystemDesc', this.riExchange.getParentHTMLValue('NotifyTemplateSystemDesc'));
            this.pageParams['isActive'] = {
                id: this.getControlValue('NotifyTemplateCode'),
                text: this.getControlValue('NotifyTemplateCode') + ' - ' + this.getControlValue('NotifyTemplateSystemDesc')
            };
            this.dropdown.NotificationTemplateSearch.isActive = {
                id: this.getControlValue('NotifyTemplateCode'),
                text: this.getControlValue('NotifyTemplateCode') + ' - ' + this.getControlValue('NotifyTemplateSystemDesc')
            };
            this.dropdown.NotificationTemplateSearch.isTriggerValidate = true;
            this.disableControl('NotifyTemplateCode', true);
            this.showButtons();
        } else {
            this.setFormMode(this.c_s_MODE_UPDATE);
            if (!this.getControlValue('rowid')) {
                this.setControlValue('rowid', this.riExchange.getParentHTMLValue('rowid'));
            }
            this.disableControl('LanguageCode', true);
            this.ellipsis.LanguageSearch.isDisabled = true;
            this.fetchTemplate();
        }
        this.setControlValue('NotifyTemplateType', this.riExchange.getParentHTMLValue('NotifyTemplateType'));
        if (this.getControlValue('NotifyTemplateType') === '1') {
            this.isDisplaySubjectText = false;
        }
        this.disableControl('LanguageDescription', true);
        this.dropdown.NotificationTemplateSearch.isDisabled = true;
    }

    /**
     * This method used to fetch template details
     * @param void
     * @return void
     */
    private fetchTemplate(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('ROWID', this.getControlValue('rowid'));
        search.set(this.serviceConstants.Action, '0');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.setControlValue('NotifyTemplateCode', data.NotifyTemplateCode);
                this.setControlValue('LanguageCode', data.LanguageCode);
                this.setControlValue('NotifySubjectText', data.NotifySubjectText);
                this.setControlValue('NotifyBodyText', data.NotifyBodyText);
                this.setLanguageDescription(data.LanguageCode);
                this.setTemplateDescription(data.NotifyTemplateCode);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    /**
     * This method used to reset form
     * @param void
     * @return void
     */
    private resetForm(): void {
        let cname: string = '';
        for (let cntrl of this.controls) {
            cname = cntrl['name'];
            if (this.uiForm.controls[cname] && cname !== 'rowid' && cname !== 'CustomerContactNumber') {
                this.uiForm.controls[cname].reset();
            }
        }
        this.dropdown.NotificationTemplateSearch.isActive = { id: '', text: '' };
    }

    /**
     * This method is called after click on save button
     * @param void
     * @return void
     */
    private saveTemplate(): void {
        switch (this.getFormMode()) {
            case this.c_s_MODE_UPDATE:
                this.updateTemplate();
                break;
            case this.c_s_MODE_ADD:
                this.addTemplate();
                break;
        }
    }

    /**
     * This method to service call for look up table data
     * @param any
     * @return any observable to subsribe table data response
     */
    private lookUpRecord(data: any, maxresults: any): any {
        let queryLookUp: QueryParams = this.getURLSearchParamObject();
        queryLookUp.set(this.serviceConstants.Action, '0');
        if (maxresults) {
            queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        }
        return this.httpService.lookUpRequest(queryLookUp, data);
    }

    /**
     * This method is used to disable all controls after deletion
     * @param void
     * @return void
     */
    private disableAllControls(): void {
        this.ellipsis.LanguageSearch.isDisabled = true;
        this.dropdown.NotificationTemplateSearch.isDisabled = true;
        this.disableControls([]);
        this.isShowButtons['Delete'] = false;
        this.isShowButtons['Save'] = false;
        this.isShowButtons['Cancel'] = false;
        this.isShowButtons['Add'] = true;
        this.isTestDisabled = true;
    }

    /**
     * This method is used to enable all controls
     * @param void
     * @return void
     */
    private enableAllControls(): void {
        this.ellipsis.LanguageSearch.isDisabled = false;
        this.dropdown.NotificationTemplateSearch.isDisabled = false;
        this.isTestDisabled = false;
        this.enableControls(['LanguageDescription']);
        this.showButtons();
    }

    /**
     * This method is used to show buttons based on mode
     * @param void
     * @return void
     */
    private showButtons(): void {
        switch (this.getFormMode()) {
            case this.c_s_MODE_ADD:
                this.isShowButtons['Add'] = false;
                this.isShowButtons['Delete'] = false;
                this.isShowButtons['Save'] = true;
                this.isShowButtons['Cancel'] = true;
                this.isTestDisabled = false;
                this.dropdown.NotificationTemplateSearch.isDisabled = false;
                break;
            case this.c_s_MODE_UPDATE:
                this.isShowButtons['Add'] = true;
                this.isShowButtons['Delete'] = true;
                this.isShowButtons['Save'] = true;
                this.isShowButtons['Cancel'] = true;
                this.isTestDisabled = false;
                this.ellipsis.LanguageSearch.isDisabled = true;
                this.disableControl('LanguageCode', true);
                this.dropdown.NotificationTemplateSearch.isDisabled = true;
                break;
        }
    }

    /**
     * This method is used to set Language data
     * @param void
     * @return data -language details Object
     */
    public onLanguageSearchRecieved(data: any): void {
        this.setControlValue('LanguageCode', data.LanguageCode);
        this.setControlValue('LanguageDescription', data.LanguageDescription);
        this.uiForm.controls['LanguageCode'].markAsDirty();
    }

    /**
     * This method is used to set template details
     * @param void
     * @return data -templates details Object
     */
    public onNotificationTemplateReceived(data: any): void {
        this.setControlValue('NotifyTemplateCode', data.NotifyTemplateCode);
        this.setControlValue('NotifyTemplateSystemDesc', data.NotifyTemplateSystemDesc);
        this.uiForm.controls['NotifyTemplateCode'].markAsDirty();
    }

    /**
     * This method is used to set LanguageDescription
     * @param void
     * @return void
     */
    public setLanguageDescription(lcode: string, from: string = ''): void {
        let dataParam: Array<Object> = [{
            'table': 'Language',
            'query': { 'LanguageCode': lcode },
            'fields': ['LanguageDescription']
        }];
        this.lookUpRecord(dataParam, 0).subscribe((data) => {
            if (data.results) {
                if (data.results[0][0]) {
                    this.setControlValue('LanguageDescription', data.results[0][0]['LanguageDescription']);
                    if (from === 'save') {
                        let msgObj: any = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveTemplate.bind(this));
                        this.modalAdvService.emitPrompt(msgObj);
                    }
                } else {
                    this.riExchange.riInputElement.markAsError(this.uiForm, 'LanguageCode');
                }
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            }
        });
    }

    /**
     * This method is used to set NotifyTemplateSystemDesc
     * @param void
     * @return void
     */
    public setTemplateDescription(tcode: string): void {
        let dataParam: Array<Object> = [{
            'table': 'NotificationTemplate',
            'query': { 'NotifyTemplateCode': tcode },
            'fields': ['NotifyTemplateSystemDesc']
        }];
        this.lookUpRecord(dataParam, 0).subscribe((data) => {
            if (data.results) {
                this.setControlValue('NotifyTemplateSystemDesc', data.results[0][0]['NotifyTemplateSystemDesc']);
                this.dropdown.NotificationTemplateSearch.isActive = {
                    id: tcode,
                    text: tcode + ' - ' + this.getControlValue('NotifyTemplateSystemDesc')
                };
                this.formPristine();
                this.dropdown.NotificationTemplateSearch.isTriggerValidate = true;
                this.pageParams['isActive'] = {
                    id: this.getControlValue('NotifyTemplateCode'),
                    text: this.getControlValue('NotifyTemplateCode') + ' - ' + this.getControlValue('NotifyTemplateSystemDesc')
                };
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.RecordNotFound));
            }
        });
    }

    /**
     * This method is for update service call
     * @param void
     * @return void
     */
    public updateTemplate(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {};
        postData['NotifySubjectText'] = this.getControlValue('NotifySubjectText');
        postData['NotifyBodyText'] = this.getControlValue('NotifyBodyText');
        postData['ROWID'] = this.getControlValue('rowid');
        search.set(this.serviceConstants.Action, '2');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    this.setFormMode(this.c_s_MODE_UPDATE);
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is for add service call
     * @param void
     * @return void
     */
    public addTemplate(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {};
        postData['LanguageCode'] = this.getControlValue('LanguageCode');
        postData['NotifyTemplateCode'] = this.getControlValue('NotifyTemplateCode');
        postData['NotifySubjectText'] = this.getControlValue('NotifySubjectText');
        postData['NotifyBodyText'] = this.getControlValue('NotifyBodyText');
        search.set(this.serviceConstants.Action, '1');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.setControlValue('rowid', data.ttNotificationTemplateDetail);
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    this.setFormMode(this.c_s_MODE_UPDATE);
                    this.showButtons();
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is for delete service call
     * @param void
     * @return void
     */
    public deleteTemplate(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        let postData: Object = {};
        postData['ROWID'] = this.getControlValue('rowid');
        search.set(this.serviceConstants.Action, '3');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                    this.setFormMode(this.c_s_MODE_SELECT);
                    this.resetForm();
                    this.disableAllControls();
                    this.setControlValue('rowid', '');
                    this.formPristine();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
            }
        );
    }

    /**
     * This method is being called after clicking add button
     * @param void
     * @return void
     */
    public addMode(): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.resetForm();
        this.enableAllControls();
        this.showButtons();
        this.dropdown.NotificationTemplateSearch.isActive = this.pageParams['isActive'];
        this.setControlValue('NotifyTemplateCode', this.pageParams['isActive']['id']);
        this.el.nativeElement.querySelector('#LanguageCode').focus();
    }

    /**
     * This method is being called after clicking save button
     * @param void
     * @return void
     */
    public saveData(): void {
        this.dropdown.NotificationTemplateSearch.isTriggerValidate = true;
        if (this.uiForm.valid && this.getControlValue('NotifyTemplateCode')) {
            this.setLanguageDescription(this.getControlValue('LanguageCode'), 'save');
        } else {
            this.uiForm.controls['LanguageCode'].markAsTouched();
            this.uiForm.controls['NotifyTemplateCode'].markAsTouched();
        }
    }

    /**
     * This method is being called after clicking delete button
     * @param void
     * @return void
     */
    public deleteData(): void {
        let msgObj: any = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteTemplate.bind(this));
        this.modalAdvService.emitPrompt(msgObj);
    }



    public cancelButtonClick(): void {
        if (this.riExchange.getParentMode() === 'Add') {
            this.formPristine();
            this.location.back();
        } else if (this.riExchange.getParentMode() === 'Update' && !this.getControlValue('rowid')) {
            this.resetForm();
            this.disableAllControls();
        } else {
            this.resetForm();
            this.initData();
            this.showButtons();
        }
    }

    public testTemplateClick(): void {
        if (this.uiForm.controls['CustomerContactNumber'].valid) {
            let search: QueryParams = this.getURLSearchParamObject();
            let postData: Object = {};
            postData['LanguageCode'] = this.getControlValue('LanguageCode');
            postData['NotifyTemplateCode'] = this.getControlValue('NotifyTemplateCode');
            postData[this.serviceConstants.Function] = 'TestNotificationTemplate';
            postData['CustomerContactNumber'] = this.getControlValue('CustomerContactNumber');
            search.set(this.serviceConstants.Action, '6');
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullError']));
                    } else {
                        this.modalAdvService.emitMessage(new ICabsModalVO(data.InfoMessage));

                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'], error['fullError']));
                }
            );
        }
    }
}
