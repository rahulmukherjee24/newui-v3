import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { InternalMaintenanceServiceModuleRoutes } from './../../../base/PageRoutes';

export class TelesalesEntryOrderLine {

    private context: any;

    constructor(private parent: any) {
        this.context = parent;
    }

    public riGridOrderLineBeforeExecute(): void {
        this.context.search = this.context.getURLSearchParamObject();
        this.context.search.set(this.context.serviceConstants.Action, '2');
        //set parameters
        this.context.search.set('TelesalesOrderNumber', this.context.riExchange.riInputElement.GetValue(this.context.uiForm, 'TelesalesOrderNumber', MntConst.eTypeInteger));
        this.context.search.set('BulkDiscountPerc', this.context.riExchange.riInputElement.GetValue(this.context.uiForm, 'BulkDiscountPerc'));
        this.context.search.set('CacheTime', this.context.pageParams.OrderLineCacheTime);
        if (this.context.getAttribute('SelectedOrderLineRowID')) {
            this.context.search.set('RowID', this.context.getAttribute('SelectedOrderLineRowID'));
        }
        this.context.search.set('HeaderClickedColumn', this.context.riGridOrderLine.HeaderClickedColumn);
        this.context.search.set('riSortOrder', this.context.riGridOrderLine.SortOrder);
        // set grid building parameters
        this.context.search.set(this.context.serviceConstants.PageSize, this.context.getItemsPerPage('riGridOrderLine'));
        this.context.search.set(this.context.serviceConstants.PageCurrent, this.context.getCurrentPageForGrid('riGridOrderLine'));
        this.context.search.set(this.context.serviceConstants.GridMode, '0');
        this.context.search.set(this.context.serviceConstants.GridHandle, '2294694');
        this.context.queryParams.search = this.context.search;
        this.context.httpService.makeGetRequest(this.context.queryParams.method, this.context.queryParams.module, this.context.queryParams.operation, this.context.search)
            .subscribe(
            (data) => {
                if (data) {
                    this.context.gridConfig.riGridOrderLine.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.context.gridConfig.riGridOrderLine.totalItems = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    this.context.setAttribute('SelectedOrderLineRowID', '');
                    //this.riGrid.Update = true;
                    this.context.riGridOrderLine.UpdateBody = true;
                    this.context.riGridOrderLine.UpdateFooter = true;
                    this.context.riGridOrderLine.UpdateHeader = true;
                    if (data.hasError) {
                        this.context.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.context.riGridOrderLine.Execute(data);
                    }
                }
                this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
            },
            error => {
                this.context.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                this.context.gridConfig.riGridOrderLine.totalItems = 1;
                this.context.ajaxSource.next(this.context.ajaxconstant.COMPLETE);
            });
        if (this.context.riGridOrderLine.Update) {
            this.context.riGridOrderLine.StartRow = this.context.getAttribute('SelectedOrderLineRow');
            this.context.riGridOrderLine.StartColumn = 0;
            this.context.riGridOrderLine.RowID = this.context.getAttribute('SelectedOrderLineRowID');
            this.context.riGridOrderLine.UpdateHeader = false;
            this.context.riGridOrderLine.UpdateBody = true;
            this.context.riGridOrderLine.UpdateFooter = false;
        }
    }

    public riGridOrderLineAfterExecute(): void {
        this.context.pageParams.lRefreshOrderLineGrid = false;
    }

    public riGridOrderLineBodyOnClick(event: any): void {
        this.selectedRowFocusOrderLine(event.srcElement.parentElement.parentElement.parentElement.children[0].children[0].children[0]);
    }

    public selectedRowFocusOrderLine(rsrcElement: any): void {
        rsrcElement.select();
        this.context.setAttribute('SelectedOrderLineRow', rsrcElement.parentElement.parentElement.parentElement.sectionRowIndex);
        this.context.setAttribute('SelectedOrderLineCell', rsrcElement.parentElement.parentElement.cellIndex);
        this.context.setAttribute('SelectedOrderLineRowID', rsrcElement.getAttribute('RowID'));
        this.context.setControlValue('SelectedOrderLine', this.context.riGridOrderLine.Details.GetAttribute('OrdLineProductCode', 'AdditionalProperty'));
        rsrcElement.focus();
    }

    public riGridOrderLineBodyOnDblClick(event: any): void {
        if (this.context.riGridOrderLine.CurrentColumnName === 'OrdLineProductDesc' && this.context.riGridOrderLine.Details.GetAttribute('OrdLineProductCode', 'AdditionalProperty')) {
            this.context.setControlValue('SelectedOrderLine', this.context.riGridOrderLine.Details.GetAttribute('OrdLineProductCode', 'AdditionalProperty'));
            this.context.setControlValue('TelesalesOrderLineNumber', this.context.getControlValue('SelectedOrderLine'));
            this.context.navigate('', InternalMaintenanceServiceModuleRoutes.ICABSCMTELESALESENTRYORDERLINEMAINTENANCE, {
                TelesalesOrderNumber: this.context.getControlValue('TelesalesOrderNumber'),
                TelesalesOrderLineNumber: this.context.getControlValue('TelesalesOrderLineNumber'),
                CurrentCallLogID: this.context.getControlValue('CurrentCallLogID'),
                ContractNumber: this.context.getControlValue('ContractNumber'),
                AccountNumber: this.context.getControlValue('AccountNumber')
            });
            this.context.pageParams.saveReturnCallback = true;
            this.context.pageParams.returnState = this.context.createControlObjectFromForm();
            this.context.pageParams.returnMethod = this.context.postReturnMethodType.POST_RETURN_2;
        }
    }

    public processOrderGridOnReturn(): void {
        this.context.pageParams.lRefreshOrderHistoryGrid = true;
        this.context.pageParams.lRefreshOrderLineGrid = true;
        this.riGridOrderLineBeforeExecute();
    }
}
