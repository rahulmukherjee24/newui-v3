import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { DatepickerComponent } from '../../../shared/components/datepicker/datepicker';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBReturnedPaperworkMaintenance.html'
})

export class ReturnedPaperworkMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('promptModal') public promptModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('ReturnedPaperworkReceivedDate') ReturnedPaperworkReceivedDate: DatepickerComponent;
    @ViewChild('ReturnedPaperworkRejectedDate') ReturnedPaperworkRejectedDate: DatepickerComponent;

    private queryParams: any = {
        operation: 'Business/iCABSBReturnedPaperworkMaintenance',
        module: 'waste',
        method: 'service-delivery/admin'
    };
    private fetchSubscription: Subscription;
    private updateSubscription: Subscription;
    private defaultDate: Date = new Date();
    private cloneFetchedData: Object;

    public isReturnedPaperworkReceivedDateVisible: boolean = false;
    public isReturnedPaperworkSatisfactoryINDVisible: boolean = false;
    public isReturnedPaperworkUserCodeVisible: boolean = false;
    public isReturnedPaperworkRejectedDateVisible: boolean = false;
    public pageId: string = '';
    public datePickerConfig = {
        ReturnedPaperworkReceivedDate: {
            isRequired: false
        }
    };
    public controls = [
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true },
        { name: 'ContractName', type: MntConst.eTypeText, disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PremiseName', type: MntConst.eTypeText, disabled: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ProductDesc', type: MntConst.eTypeText, disabled: true },
        { name: 'ReturnedPaperworkReceivedIND', type: MntConst.eTypeCheckBox },
        { name: 'ReturnedPaperworkSatisfactoryIND', type: MntConst.eTypeCheckBox },
        { name: 'ReturnedPaperworkUserCode', type: MntConst.eTypeCode, disabled: true },
        { name: 'ReturnedPaperworkNotes', type: MntConst.eTypeTextFree },
        { name: 'ReturnedPaperworkReceivedDate', type: MntConst.eTypeDate },
        { name: 'ReturnedPaperworkRejectedDate', type: MntConst.eTypeDate }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBRETURNEDPAPERWORKMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Returned Paperwork Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.fetchSubscription) {
            this.fetchSubscription.unsubscribe();
        }
        if (this.updateSubscription) {
            this.updateSubscription.unsubscribe();
        }
    }

    private windowOnload(): void {
        this.fetchReturnedPaperWorkDetails();
    }

    private fetchReturnedPaperWorkDetails(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('ROWID', this.riExchange.getParentHTMLValue('LetterRowID'));
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.fetchSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.cloneFetchedData = data;
                    this.setControlValue('ContractNumber', data.ContractNumber);
                    this.setControlValue('ContractName', data.ContractName);
                    if (data.PremiseNumber !== '0') {
                        this.setControlValue('PremiseNumber', data.PremiseNumber);
                    }
                    else {
                        this.setControlValue('PremiseNumber', '');
                    }
                    this.setControlValue('ProductCode', data.ProductCode || '');
                    this.dolookUpCallForDesc(data);
                    this.setControlValue('ReturnedPaperworkNotes', data.ReturnedPaperworkNotes);
                    this.setFormControl(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private setFormControl(receivedData: Object): void {
        this.setControlValue('ReturnedPaperworkReceivedIND', receivedData['ReturnedPaperworkReceivedIND']);
        if (receivedData['ReturnedPaperworkReceivedIND'] as boolean) {
            this.isReturnedPaperworkReceivedDateVisible = true;
            this.isReturnedPaperworkSatisfactoryINDVisible = true;
            this.isReturnedPaperworkUserCodeVisible = true;
            this.setControlValue('ReturnedPaperworkUserCode', this.utils.getUserCode());
            this.setControlValue('ReturnedPaperworkSatisfactoryIND', receivedData['ReturnedPaperworkSatisfactoryIND']);
            this.setControlValue('ReturnedPaperworkReceivedDate', receivedData['ReturnedPaperworkReceivedDate']);
        }
        if (!(receivedData['ReturnedPaperworkSatisfactoryIND'] as boolean) && (receivedData['ReturnedPaperworkReceivedIND'] as boolean)) {
            this.isReturnedPaperworkRejectedDateVisible = true;
            this.setControlValue('ReturnedPaperworkRejectedDate', receivedData['ReturnedPaperworkRejectedDate']);
        }
    }

    private dolookUpCallForDesc(receivedData: Object): void {
        let lookupDetails: Array<any> = [{
            'table': 'Contract',
            'query': {
                'ContractNumber': receivedData['ContractNumber'],
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['ContractName']
        },
        {
            'table': 'Premise',
            'query': {
                'ContractNumber': receivedData['ContractNumber'],
                'BusinessCode': this.utils.getBusinessCode(),
                'PremiseNumber': receivedData['PremiseNumber']
            },
            'fields': ['PremiseName']
        },
        {
            'table': 'Product',
            'query': {
                'ProductCode': receivedData['ProductCode'],
                'BusinessCode': this.utils.getBusinessCode()
            },
            'fields': ['ProductDesc']
        }];

        this.LookUp.lookUpRecord(lookupDetails).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.length) {
                    let contract = data[0];
                    let premise = data[1];
                    let product = data[2];
                    if (contract.length) {
                        this.cloneFetchedData['ContractName'] = contract[0].ContractName;
                        this.setControlValue('ContractName', contract[0].ContractName);
                    }
                    if (premise.length) {
                        this.cloneFetchedData['PremiseName'] = contract[0].PremiseName;
                        this.setControlValue('PremiseName', premise[0].PremiseName);
                    }

                    if (product.length) {
                        this.cloneFetchedData['ProductDesc'] = contract[0].ProductDesc;
                        this.setControlValue('ProductDesc', product[0].ProductDesc);
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });

    }

    private proceedWithSaveConfirmation(): void {
        if (this['uiForm'].valid) {
            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveUpdatedData.bind(this));
            this.modalAdvService.emitPrompt(modalVO);
        }
    }

    private saveUpdatedData(): void {
        let postSearchParams = this.getURLSearchParamObject();
        let postParams: any = {};
        postSearchParams.set(this.serviceConstants.Action, '2');
        postParams['Table'] = 'CustomerLetter';
        postParams['ROWID'] = this.riExchange.getParentHTMLValue('LetterRowID');
        postParams['LetterTypeCode'] = this.cloneFetchedData['LetterTypeCode'];
        postParams['ContractNumber'] = this.cloneFetchedData['ContractNumber'];
        if (this.cloneFetchedData['PremiseNumber']) {
            postParams['PremiseNumber'] = this.cloneFetchedData['PremiseNumber'];
        }
        else {
            postParams['PremiseNumber'] = '?';
        }
        postParams['ProductCode'] = this.cloneFetchedData['ProductCode'];
        postParams['ServiceCoverNumber'] = this.cloneFetchedData['LetterTypeCode'];
        postParams['ReturnedPaperworkReceivedIND'] = this.getControlValue('ReturnedPaperworkReceivedIND');
        postParams['ReturnedPaperworkSatisfactoryIND'] = this.getControlValue('ReturnedPaperworkSatisfactoryIND');
        postParams['ReturnedPaperworkReceivedDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ReturnedPaperworkReceivedDate'));
        if (!(this.getControlValue('ReturnedPaperworkSatisfactoryIND') as boolean)) {
            postParams['ReturnedPaperworkRejectedDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('ReturnedPaperworkRejectedDate'));
        }
        else {
            postParams['ReturnedPaperworkRejectedDate'] = '';
        }
        postParams['ReturnedPaperworkUserCode'] = this.getControlValue('ReturnedPaperworkUserCode');
        postParams['ReturnedPaperworkNotes'] = this.getControlValue('ReturnedPaperworkNotes');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.updateSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    this.cloneFetchedData['ReturnedPaperworkReceivedIND'] = postParams['ReturnedPaperworkReceivedIND'];
                    this.cloneFetchedData['ReturnedPaperworkSatisfactoryIND'] = postParams['ReturnedPaperworkSatisfactoryIND'];
                    this.cloneFetchedData['ReturnedPaperworkReceivedDate'] = postParams['ReturnedPaperworkReceivedDate'];
                    this.cloneFetchedData['ReturnedPaperworkRejectedDate'] = postParams['ReturnedPaperworkRejectedDate'];
                    this.cloneFetchedData['ReturnedPaperworkNotes'] = postParams['ReturnedPaperworkNotes'];
                    this.formPristine();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    private validateSelectedRejectionDate(): void {
        if (this.globalize.parseDateStringToDate(this.getControlValue('ReturnedPaperworkRejectedDate')) > this.defaultDate) {
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ReturnedPaperworkRejectedDate', true);
            this.ReturnedPaperworkRejectedDate.markAsError(true);
            setTimeout(function (): void {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.PageSpecificMessage.bReturnedPaperworkMaintenance.rejectedPaperworkDateError));
            }.bind(this), 0);
        }
    }

    //On change of Rejected Paperwork Satisfactory Checkbox
    public onReturnedPaperworkSatisfactoryINDChange(): void {
        let isChecked: boolean = this.getControlValue('ReturnedPaperworkSatisfactoryIND');
        if (!isChecked) {
            this.isReturnedPaperworkRejectedDateVisible = true;
            this.setControlValue('ReturnedPaperworkRejectedDate', this.defaultDate);
        }
        else {
            this.isReturnedPaperworkRejectedDateVisible = false;
            this.setControlValue('ReturnedPaperworkRejectedDate', '');
        }
    }

    //On change of Returned Paperwork Received Checkbox
    public onReturnedPaperworkReceivedINDChange(): void {
        this.setControlValue('ReturnedPaperworkUserCode', this.utils.getUserCode());
        let isChecked: boolean = this.getControlValue('ReturnedPaperworkReceivedIND');
        if (isChecked) {
            if (!this.isReturnedPaperworkReceivedDateVisible || !this.isReturnedPaperworkSatisfactoryINDVisible || !this.isReturnedPaperworkUserCodeVisible) {
                this.isReturnedPaperworkReceivedDateVisible = true;
                this.isReturnedPaperworkSatisfactoryINDVisible = true;
                this.isReturnedPaperworkUserCodeVisible = true;
            }
            this.setControlValue('ReturnedPaperworkReceivedDate', this.defaultDate);
            this.setControlValue('ReturnedPaperworkSatisfactoryIND', true);
        }
        else {
            this.setControlValue('ReturnedPaperworkReceivedDate', '');
            this.setControlValue('ReturnedPaperworkRejectedDate', '');
            this.setControlValue('ReturnedPaperworkSatisfactoryIND', false);
        }
    }

    //On Cancel Button Click
    public onCancel(): void {
        this.setControlValue('ReturnedPaperworkReceivedIND', this.cloneFetchedData['ReturnedPaperworkReceivedIND']);
        this.setControlValue('ReturnedPaperworkNotes', this.cloneFetchedData['ReturnedPaperworkNotes']);
        if (this.cloneFetchedData['ReturnedPaperworkUserCode']) {
            this.setControlValue('ReturnedPaperworkUserCode', this.utils.getUserCode());
        }
        else {
            this.setControlValue('ReturnedPaperworkUserCode', this.cloneFetchedData['ReturnedPaperworkUserCode']);
        }
        if ((this.cloneFetchedData['ReturnedPaperworkReceivedIND'] as boolean)) {
            this.setControlValue('ReturnedPaperworkReceivedDate', this.cloneFetchedData['ReturnedPaperworkReceivedDate']);
            this.setControlValue('ReturnedPaperworkSatisfactoryIND', this.cloneFetchedData['ReturnedPaperworkSatisfactoryIND']);
            if (!(this.cloneFetchedData['ReturnedPaperworkSatisfactoryIND'] as boolean)) {
                this.isReturnedPaperworkRejectedDateVisible = true;
                this.setControlValue('ReturnedPaperworkRejectedDate', this.cloneFetchedData['ReturnedPaperworkRejectedDate']);
            }
            else {
                this.isReturnedPaperworkRejectedDateVisible = false;
                this.setControlValue('ReturnedPaperworkRejectedDate', '');
            }
        }
        else {
            this.setControlValue('ReturnedPaperworkReceivedDate', '');
            this.setControlValue('ReturnedPaperworkSatisfactoryIND', false);
            this.setControlValue('ReturnedPaperworkRejectedDate', '');
        }
        this.formPristine();
    }

    //On Received Paperwork Date Change
    public onSelectedReceivedDateValue(event: any): void {
        if (this.globalize.parseDateStringToDate(event.value)) {
            this.setControlValue('ReturnedPaperworkReceivedDate', event.value);
            this.uiForm.markAsDirty();
        }
    }

    //On Rejected Paperwork Date Change
    public onSelectedRejectedDateValue(event: any): void {
        if (this.globalize.parseDateStringToDate(event.value)) {
            this.setControlValue('ReturnedPaperworkRejectedDate', event.value);
            this.uiForm.markAsDirty();
        }
    }

    //On Save Button Click
    public showSaveConfirm(): void {
        if ((this.getControlValue('ReturnedPaperworkReceivedIND') as boolean) && !this.fieldHasValue('ReturnedPaperworkReceivedDate')) {
            this.datePickerConfig.ReturnedPaperworkReceivedDate.isRequired = true;
        }
        else {
            if ((this.getControlValue('ReturnedPaperworkReceivedIND') as boolean) && (this.globalize.parseDateStringToDate(this.getControlValue('ReturnedPaperworkReceivedDate')) > this.defaultDate || this.globalize.parseDateStringToDate(this.getControlValue('ReturnedPaperworkRejectedDate')) > this.defaultDate)) {
                if (this.globalize.parseDateStringToDate(this.getControlValue('ReturnedPaperworkReceivedDate')) > this.defaultDate) {
                    this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'ReturnedPaperworkReceivedDate', true);
                    this.ReturnedPaperworkReceivedDate.markAsError(true);
                    let promptVO: ICabsModalVO = new ICabsModalVO();
                    promptVO.msg = MessageConstant.PageSpecificMessage.bReturnedPaperworkMaintenance.receivedPaperworkDateError;
                    promptVO.closeCallback = this.validateSelectedRejectionDate.bind(this);
                    this.modalAdvService.emitError(promptVO);
                }
                else {
                    this.validateSelectedRejectionDate();
                }
            }
            else {
                this.proceedWithSaveConfirmation();
            }
        }
    }
}
