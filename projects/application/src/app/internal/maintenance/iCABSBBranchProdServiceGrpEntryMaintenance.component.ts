import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { EllipsisComponent } from '../../../shared/components/ellipsis/ellipsis';
import { ProductServiceGroupSearchComponent } from '../search/iCABSBProductServiceGroupSearch.component';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { BranchProdServiceGrpEntrySearchComponent } from '../search/iCABSBBranchProdServiceGrpEntrySearch.component';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSBBranchProdServiceGrpEntryMaintenance.html'
})

export class BranchProdServiceGrpEntryMaintenanceComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
    @ViewChild('productservice') productservice: ProductServiceGroupSearchComponent;
    @ViewChild('prodServiCeGrpEntry') prodServiCeGrpEntry: EllipsisComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private branchProdServiceGrpEntrySearchComponent: any = BranchProdServiceGrpEntrySearchComponent;
    private inputParams: any = {
        method: 'service-delivery/admin',
        module: 'product',
        operation: 'Business/iCABSBBranchProdServiceGrpEntryMaintenance'
    };
    public pageId: string = '';
    public controls = [
        { name: 'BranchServiceAreaCode', disabled: true },
        { name: 'BranchServiceAreaDesc', disabled: true },
        { name: 'ProductServiceGroupCode', value: '', required: true },
        { name: 'ProductServiceGroupDesc', value: '' },
        { name: 'ROWID' }
    ];
    public isSearchEnabled: boolean;
    public isDeleteEnabled: boolean;
    public isSaveEnabled: boolean;
    public isCancelEnabled: boolean;
    public dropDown: any = {
        productServiceGroup: {
            active: {
                id: '',
                text: ''
            },
            inputParams: {
                params: {
                    parentMode: 'LookUp'
                }
            }
        }
    };
    public ellipsis = {
        prodServiCeGrpEntry: {
            childConfigParams: {
                parentMode: 'Search',
                BranchServiceAreaCode: '',
                BranchServiceAreaDesc: ''
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: this.branchProdServiceGrpEntrySearchComponent,
            disabled: false,
            autoOpen: false
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBRANCHPRODSERVICEGRPENTRYMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Product Group Entry Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.productservice.isTriggerValidate = false;
        this.productservice.isRequired = true;
        this.pageParams.ProductServiceGroupCode = this.getControlValue('ProductServiceGroupCode');
        this.pageParams.ProductServiceGroupDesc = this.getControlValue('ProductServiceGroupDesc');
        this.riExchange.getParentHTMLValue('BranchServiceAreaCode');
        this.riExchange.getParentHTMLValue('BranchServiceAreaDesc');
        this.ellipsis.prodServiCeGrpEntry.childConfigParams['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        this.ellipsis.prodServiCeGrpEntry.childConfigParams['BranchServiceAreaDesc'] = this.getControlValue('BranchServiceAreaDesc');
        this.windowOnLoad();
        if (this.parentMode !== 'AddProduct') {
            this.riExchange.getParentHTMLValue('ProductServiceGroupCode');
            this.getProductServiceDetails();
        }
    }

    ngAfterContentInit(): void {
        this.ellipsis.prodServiCeGrpEntry.autoOpen = true;
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        if (this.parentMode === 'AddProduct') {
            this.productservice.isDisabled = false;
            this.isSearchEnabled = true;
            this.isDeleteEnabled = false;
            this.isSaveEnabled = true;
            this.isCancelEnabled = true;
        } else {
            this.productservice.isDisabled = true;
            this.isSearchEnabled = true;
            this.isDeleteEnabled = true;
            this.isSaveEnabled = false;
            this.isCancelEnabled = false;

        }
    }

  /**
   * fetch productservice details
   * @param void
   * @return void
   */

    private getProductServiceDetails(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set('TABLE', 'ProductServiceGroup');
        search.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroupCode'));
        search.set('Mode', 'ProductServiceGroup');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('ProductServiceGroupDesc', data.ProductServiceGroupDesc);
                    this.setControlValue('ROWID', data.ttProductServiceGroup);
                    this.dropDown.productServiceGroup.active = {
                        id: this.getControlValue('ProductServiceGroupCode'),
                        text: this.getControlValue('ProductServiceGroupCode') + ' - ' + this.getControlValue('ProductServiceGroupDesc')
                    };
                }

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    /**
     * delete product group
     * @param void
     * @return void
     */
    private callWarnDeleteService(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '3');
        let formdata: Object = {};
        formdata['Function'] = 'WarnPortfolioRelatedToProd';
        formdata['BranchNumber'] = this.utils.getBranchCode();
        formdata['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formdata['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                this.dropDown.productServiceGroup.active = {
                    id: '',
                    text: ''
                };
                this.isDeleteEnabled = false;
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    /**
     * add productservice
     * @param void
     * @return void
     */

    private addProduct(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '1');
        let formdata: Object = {};
        formdata['TABLE'] = 'BranchProdServiceGrpEntry';
        formdata['BranchNumber'] = this.utils.getBranchCode();
        formdata['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formdata['ProductServiceGroupCode'] = this.getControlValue('ProductServiceGroupCode');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.inputParams.method, this.inputParams.module, this.inputParams.operation, search, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.formPristine();
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                this.productservice.isTriggerValidate = false;
                this.productservice.isRequired = true;
                this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.SavedSuccessfully));
                this.parentMode = '';
                this.windowOnLoad();
                this.pageParams.ProductServiceGroupCode = this.getControlValue('ProductServiceGroupCode');
                this.pageParams.ProductServiceGroupDesc = this.getControlValue('ProductServiceGroupDesc');

            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    public onProductSearchReceived(data: any): void {
        this.setControlValue('ProductServiceGroupCode', data.ProductServiceGroupCode);
        this.setControlValue('ProductServiceGroupDesc', data.ProductServiceGroupDesc);
        this.uiForm.markAsDirty();
    }

    public onDeleteClick(): void {
        let promptVO: ICabsModalVO = new ICabsModalVO();
        promptVO.msg = MessageConstant.Message.DeleteRecord;
        promptVO.confirmCallback = this.callWarnDeleteService.bind(this);
        this.modalAdvService.emitPrompt(promptVO);
    }

    public onAddClick(): void {
        if (this.uiForm.valid) {
        let promptVO: ICabsModalVO = new ICabsModalVO();
        promptVO.msg = MessageConstant.Message.ConfirmRecord;
        promptVO.confirmCallback = this.addProduct.bind(this);
        this.modalAdvService.emitPrompt(promptVO);
    }
        this.productservice.isTriggerValidate = true;
        this.productservice.isRequired = true;
    }

    public onCancelClick(): void {
        if (this.parentMode === 'AddProduct') {
            if (this.pageParams.ProductServiceGroupCode) {
                this.dropDown.productServiceGroup.active = {
                    id: this.pageParams.ProductServiceGroupCode,
                    text: this.pageParams.ProductServiceGroupCode + ' - ' + this.pageParams.ProductServiceGroupDesc
                };
            } else {
                this.dropDown.productServiceGroup.active = {
                    id: '',
                    text: ''
                };
            }
            this.formPristine();
        }
    }

    public onProdServiceGroupSelect(event: any): void {
        if (event.addNew) {
        this.setControlValue('BranchServiceAreaCode',event.branchServiceAreaCode);
        this.setControlValue('BranchServiceAreaDesc',event.branchServiceAreaDesc);
        this.ellipsis.prodServiCeGrpEntry.childConfigParams.BranchServiceAreaCode = this.getControlValue('BranchServiceAreaCode');
        this.ellipsis.prodServiCeGrpEntry.childConfigParams.BranchServiceAreaDesc = this.getControlValue('BranchServiceAreaDesc');
        this.parentMode = 'AddProduct';
        this.dropDown.productServiceGroup.active = {
            id: '',
            text: ''
        };
        } else {
        this.parentMode = '';
        this.dropDown.productServiceGroup.active = {
            id: event.productDetails.ProductServiceGroupCode,
            text: event.productDetails.ProductServiceGroupCode + ' - ' + event.productDetails.ProductServiceGroupDesc
        };
        this.setControlValue('ProductServiceGroupCode', event.productDetails.ProductServiceGroupCode);
        this.setControlValue('ProductServiceGroupDesc', event.productDetails.ProductServiceGroupDesc);
    }
        this.windowOnLoad();
    }

    public onSearchClick(): void {
        this.prodServiCeGrpEntry.openModal();
        if ( this.parentMode === 'AddProduct') {
             this.dropDown.productServiceGroup.active = {
            id: '',
            text: ''
        };
        }
    }

}
