import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSSeServiceVisitRecommendationMaintenance.html'
})

export class ServiceVisitRecommendationMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private queryParams: any = {
        operation: 'Service/iCABSSeServiceVisitRecommendationMaintenance',
        module: 'pda',
        method: 'service-delivery/maintenance'
    };

    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'RecommendationText', type: MntConst.eTypeTextFree, disabled: true }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEVISITRECOMMENDATIONMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Service Visit Recommendation';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    private windowOnload(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('ROWID', this.riExchange.getParentHTMLValue('currentRowID'));
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('RecommendationText', data.RecommendationText);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }
}
