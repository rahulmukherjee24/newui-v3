import { Component, OnInit, Injector, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { QueryParams } from '../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MntConst } from './../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';
import { EmployeeSearchComponent } from './../../internal/search/iCABSBEmployeeSearch';
import { MessageConstant } from './../../../shared/constants/message.constant';
import { BranchSearchComponent } from '../search/iCABSBBranchSearch';

@Component({
    templateUrl: 'iCABSSeProductivityAdjustmentMaintenance.html'
})

export class ProductivityAdjustmentMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('promptModal') public promptModal;
    @ViewChild('promptConfirmModalDelete') public promptConfirmModalDelete;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('branchsearchDropDown') branchsearchDropDown: BranchSearchComponent;

    private isAllowProductiveTimeAdj: boolean = false;
    private deleteSubscription: Subscription;
    private updateSubscription: Subscription;
    private fetchSubscription: Subscription;
    private queryParams: any = {
        operation: 'Service/iCABSSeProductivityAdjustmentMaintenance',
        module: 'productivity',
        method: 'service-delivery/maintenance'
    };
    private onCancelRestControls: Object;
    private isFetchTrue: boolean = false;
    public isbtnAdd: boolean = false;
    public isbtnSave: boolean = false;
    public isbtnDelete: boolean = false;
    public isbtnCancel: boolean = false;
    public isBranchSearchDisabled: boolean = false;
    public isAdjustmentTimeVisible: boolean = true;
    public isAdjustmentTimeNegVisible: boolean = true;
    public isBranchNumberValidate: boolean = false;
    public branchNumberSelected: Object = {
        id: '',
        text: ''
    };
    public datePickerConfig = {
        AdjustmentDate: {
            isDisabled: true,
            isRequired: true,
            value: null
        }
    };
    public pageId: string = '';
    public controls = [
        { name: 'EmployeeCode', type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'AdjustmentNumber', type: MntConst.eTypeAutoNumber },
        { name: 'AdjustmentValue', type: MntConst.eTypeCurrency },
        { name: 'AdjustmentTime', type: MntConst.eTypeTime },
        { name: 'AdjustmentTimeNeg', type: MntConst.eTypeCheckBox },
        { name: 'AdjustmentText', type: MntConst.eTypeTextFree },
        { name: 'AdjustmentDate', required: true, type: MntConst.eTypeDate }
    ];
    public ellipsisParams: any = {
        Employee: {
            isShowCloseButton: true,
            isShowHeader: true,
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp'
            },
            contentComponent: EmployeeSearchComponent
        },
        common: {
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            }
        }
    };
    public inputParamsBranchSearch: any = { 'parentMode': 'LookUp', 'ContractTypeCode': '', 'businessCode': this.utils.getBusinessCode(), 'countryCode': this.utils.getCountryCode(), action: 0 };

    constructor(injector: Injector, private ref: ChangeDetectorRef) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPRODUCTIVITYADJUSTMENTMAINTENANCE;
        this.pageTitle = this.browserTitle = 'Productivity Adjustment Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getSysCharDetails();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.deleteSubscription) {
            this.deleteSubscription.unsubscribe();
        }
        if (this.fetchSubscription) {
            this.fetchSubscription.unsubscribe();
        }
        if (this.updateSubscription) {
            this.updateSubscription.unsubscribe();
        }
    }

    private getSysCharDetails(): void {
        let sysCharList: number[] = [
            this.sysCharConstants.SystemCharAllowProductiveTimeAdjustment];
        let sysCharIP = {
            module: this.queryParams.module,
            operation: this.queryParams.operation,
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharList.toString()
        };
        this.speedScript.sysChar(sysCharIP).subscribe((data) => {
            if (data.records && data.records.length > 0) {
                this.isAllowProductiveTimeAdj = data.records[0].Required;
            }
            this.windowOnLoad();
        });
    }

    private windowOnLoad(): void {
        if (!this.isAllowProductiveTimeAdj) {
            this.isAdjustmentTimeNegVisible = false;
            this.isAdjustmentTimeVisible = false;
        }
        this.formResetToNormalMode([]);
    }

    private formResetToNormalMode(ignore: Array<string>): void {
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EmployeeCode', true);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'EmployeeSurname', true);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'AdjustmentValue', true);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'AdjustmentText', true);
        this.formModeButtonController(this.formMode = this.c_s_MODE_SELECT);
        this.disableControls(['EmployeeCode', 'AdjustmentNumber']);
        this.disableControl('EmployeeCode', false);
        this.disableControl('AdjustmentNumber', false);
        this.clearControls(ignore);
        this.isBranchSearchDisabled = true;
        this.branchNumberSelected = {
            id: '',
            text: ''
        };
        this.datePickerConfig.AdjustmentDate.isDisabled = true;
        this.formPristine();
    }

    private dolookUpCallForDesc(isLookingUp: boolean, callback?: any): void {
        let lookupIP: any = [
            {
                'table': 'Employee',
                'query': {
                    'EmployeeCode': this.getControlValue('EmployeeCode')
                },
                'fields': ['EmployeeSurname']
            }
        ];
        this.lookupDetails(isLookingUp, lookupIP, callback);
    }

    private lookupDetails(isLookingUp: boolean, query: any, callback?: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            if (isLookingUp) {
                if (data[0] && data[0].length > 0) {
                    this.setControlValue('EmployeeSurname', (data[0] && data[0].length ? data[0][0].EmployeeSurname : ''));
                } else {
                    this.setControlValue('EmployeeSurname', '');
                    this.riExchange.riInputElement.markAsError(this.uiForm, 'EmployeeCode');
                }
            }
            else {
                callback.call(this);
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error));
        });
    }

    private formModeButtonController(modeForm: string): void {
        this.formMode = modeForm;
        if (this.formMode === this.c_s_MODE_UPDATE) {
            this.isbtnAdd = false;
            this.isbtnSave = false;
            this.isbtnCancel = false;
            this.isFetchTrue === true ? this.isbtnDelete = false : this.isbtnDelete = true;
        } else if (this.formMode === this.c_s_MODE_ADD) {
            this.isbtnAdd = true;
            this.isbtnSave = false;
            this.isbtnCancel = false;
            this.isbtnDelete = true;
        } else if (this.formMode === this.c_s_MODE_SELECT) {
            this.isbtnAdd = false;
            this.isbtnSave = true;
            this.isbtnCancel = true;
            this.isbtnDelete = true;
        }
    }

    private populateAdjustmentProductivity(): void {
        this.isFetchTrue = false;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set('EmployeeCode', (this.getControlValue('EmployeeCode').toString().trim().length === 0 ? '' : this.getControlValue('EmployeeCode')));
        search.set('AdjustmentNumber', (this.getControlValue('AdjustmentNumber').toString().trim().length === 0 ? '' : this.getControlValue('AdjustmentNumber')));
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.fetchSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    if (data.BranchNumber === '0') {
                        let promptVO: ICabsModalVO = new ICabsModalVO();
                        promptVO.msg = MessageConstant.Message.RecordNotFound;
                        promptVO.closeCallback = this.formResetToNormalMode.bind(this, []);
                        this.modalAdvService.emitError(promptVO);
                    }
                    else {
                        this.isFetchTrue = true;
                        this.setControlsOnDataReceive(data);
                        this.resetControlsOnCancle(data);
                    }
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    private resetControlsOnCancle(data: Object): void {
        this.onCancelRestControls = data;
    }

    private setControlsOnDataReceive(data: Object): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.setControlValue('AdjustmentDate', data['AdjustmentDate']);
            this.setControlValue('AdjustmentTime', data['AdjustmentTime']);
            this.setControlValue('AdjustmentText', data['AdjustmentText']);
            this.setControlValue('AdjustmentValue', data['AdjustmentValue']);
            this.setControlValue('AdjustmentTimeNeg', data['AdjustmentTimeNeg'] as boolean);
            if (data['BranchNumber'] === '0') {
                this.branchNumberSelected = {
                    id: '',
                    text: ''
                };
            }
            else {
                this.branchNumberSelected = {
                    id: data['BranchNumber'],
                    text: this.utils.getBranchText(data['BranchNumber'])
                };
            }

            this.formModeButtonController(this.formMode = this.c_s_MODE_UPDATE);
            this.formPristine();
        }
        else {
            this.setRequiredStatus('AdjustmentNumber', true);
        }

        this.enableControls(['EmployeeCode', 'EmployeeSurname']);
        this.disableControl('EmployeeCode', true);
        this.isBranchSearchDisabled = true;
        this.datePickerConfig.AdjustmentDate.isDisabled = false;
    }

    private proceedWithSaveUpdate(): void {
        let postSearchParams = this.getURLSearchParamObject();
        let postParams: any = {};
        if (this.formMode === this.c_s_MODE_ADD) {
            postSearchParams.set(this.serviceConstants.Action, '1');
        }
        else if (this.formMode === this.c_s_MODE_UPDATE) {
            postSearchParams.set(this.serviceConstants.Action, '2');
            postParams['AdjustmentNumber'] = this.getControlValue('AdjustmentNumber');
        }
        postParams['EmployeeCode'] = this.getControlValue('EmployeeCode');
        postParams['AdjustmentTime'] = this.globalize.parseTimeToFixedFormat(this.getControlValue('AdjustmentTime'));
        postParams['AdjustmentTimeNeg'] = (this.getControlValue('AdjustmentTimeNeg')) ? 'yes' : 'no';
        postParams['BranchNumber'] = this.branchNumberSelected['id'];
        postParams['AdjustmentValue'] = this.globalize.parseCurrencyToFixedFormat(this.getControlValue('AdjustmentValue'));
        postParams['AdjustmentDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('AdjustmentDate'));
        postParams['AdjustmentText'] = this.getControlValue('AdjustmentText');
        this.resetControlsOnCancle(postParams);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.updateSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, postSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    return;
                }
                else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    if (this.formMode === this.c_s_MODE_ADD) {
                        this.setControlValue('AdjustmentNumber', data.AdjustmentNumber);
                        this.setControlsOnDataReceive(data);
                    }
                    this.isFetchTrue = true;
                    this.formModeButtonController(this.formMode = this.c_s_MODE_UPDATE);
                    this.formPristine();
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }

    private proceedWithSaveConfirmation(): void {
        if (this['uiForm'].valid) {
            let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveUpdatedData.bind(this));
            this.modalAdvService.emitPrompt(modalVO);
        }
    }

    public deleteSavedData(): void {
        let delSearchParams = this.getURLSearchParamObject();
        delSearchParams.set(this.serviceConstants.Action, '3');
        let postParams: any = {};
        postParams['EmployeeCode'] = this.getControlValue('EmployeeCode');
        postParams['AdjustmentNumber'] = this.getControlValue('AdjustmentNumber');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.deleteSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, delSearchParams, postParams)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data['hasError']) {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'], data['fullerror']));
                    return;
                }
                let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.RecordDeleted);
                this.modalAdvService.emitMessage(modalVO);
                this.formResetToNormalMode([]);
                this.onCancelRestControls = {};
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.errorService.emitError(error);
            });
    }

    public onEllipsisDataReceived(type: string, data: any): void {
        switch (type) {
            case 'Employee':
                this.setControlValue('EmployeeCode', data['EmployeeCode'] || '');
                this.setControlValue('EmployeeSurname', data['EmployeeSurname'] || '');
                if (this.formMode !== this.c_s_MODE_ADD) {
                    this.formResetToNormalMode(['EmployeeCode', 'EmployeeSurname']);
                }
                break;
            default:
        }
    }

    public employeeCodeOnChange(): void {
        if (this.formMode !== this.c_s_MODE_ADD) {
            this.uiForm.controls['EmployeeCode'].markAsPristine();
        }
        if (this.fieldHasValue('EmployeeCode')) {
            this.dolookUpCallForDesc(true);
        }
        else {
            this.setControlValue('EmployeeSurname', '');
        }
    }

    //On Adjustment Number Change
    public adjustmentNumberOnChange(): void {
        if (this.globalize.parseIntegerToFixedFormat(this.uiForm.controls['AdjustmentNumber'].value)) {
            this.dolookUpCallForDesc(false, this.populateAdjustmentProductivity);
        }
        else {
            this.riExchange.riInputElement.markAsError(this.uiForm, 'AdjustmentNumber');
            let promptVO: ICabsModalVO = new ICabsModalVO();
            promptVO.msg = MessageConstant.Message.RecordNotFound;
            promptVO.closeCallback = this.formResetToNormalMode.bind(this, []);
            this.modalAdvService.emitError(promptVO);
        }
    }

    //On Save Button Click
    public showSaveConfirm(): void {
        if (!this.riExchange.validateForm(this.uiForm) || (!(this.fieldHasValue('AdjustmentNumber')) && (this.formMode !== this.c_s_MODE_ADD))
            || !this.fieldHasValue('AdjustmentText') || (this.branchNumberSelected['id'] === '') || !(this.fieldHasValue('AdjustmentDate')) || !this.fieldHasValue('AdjustmentValue')) {
            if (this.branchNumberSelected['id'] === '') {
                this.isBranchNumberValidate = true;
            }
            return;
        } else {
            this.isBranchNumberValidate = false;
            this.dolookUpCallForDesc(false, this.proceedWithSaveConfirmation);
            this.formPristine();
        }
    }

    //On Delete Button Click
    public showDeleteConfirm(): void {
        if (!this.riExchange.validateForm(this.uiForm) || !this.fieldHasValue('AdjustmentNumber')
            || !this.fieldHasValue('AdjustmentText') || (this.branchNumberSelected['id'] === '') || !(this.fieldHasValue('AdjustmentDate')) || !this.fieldHasValue('AdjustmentValue')) {
            return;
        }
        let modalVO: ICabsModalVO = new ICabsModalVO(MessageConstant.Message.DeleteRecord, null, this.deleteSavedData.bind(this));
        this.modalAdvService.emitPrompt(modalVO);
        this.isbtnCancel = false;
    }

    public onSelectedDateValue(event: any): void {
        if (this.globalize.parseDateStringToDate(event.value)) {
            this.setControlValue('AdjustmentDate', event.value);
            this.uiForm.markAsDirty();
        }
    }

    public saveUpdatedData(): void {
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        } else {
            this.dolookUpCallForDesc(false, this.proceedWithSaveUpdate);
        }
    }

    public onBranchDataReceived(obj: any): void {
        if (obj) {
            if (obj.BranchNumber) {
                this.branchNumberSelected = {
                    id: obj.BranchNumber,
                    text: obj.BranchNumber + ' - ' + obj.BranchName
                };
                this.uiForm.markAsDirty();
            }
        }
    }

    //On Cancel Button cick
    public onCancel(): void {
        if (this.isFetchTrue && this.formMode === this.c_s_MODE_UPDATE) {
            this.setControlsOnDataReceive(this.onCancelRestControls);
        } else {
            if (this.formMode === this.c_s_MODE_ADD) {
                this.formResetToNormalMode([]);
                this.onCancelRestControls = {};
            }
            else {
                this.formModeButtonController(this.formMode = this.c_s_MODE_UPDATE);
            }
        }
        this.formPristine();
    }

    public addMode(): void {
        this.formModeButtonController(this.formMode = this.c_s_MODE_ADD);
        this.setControlsOnAddMode();
        this.isFetchTrue = false;
    }

    private setControlsOnAddMode(): void {
        this.isBranchSearchDisabled = false;
        this.datePickerConfig.AdjustmentDate.isDisabled = false;
        this.datePickerConfig.AdjustmentDate.isRequired = true;
        this.enableControls(['AdjustmentNumber', 'EmployeeSurname']);
        this.disableControl('AdjustmentNumber', true);
        this.setRequiredStatus('AdjustmentNumber', false);

        this.clearControls([]);
        this.branchNumberSelected = {
            id: '',
            text: ''
        };
    }
}
