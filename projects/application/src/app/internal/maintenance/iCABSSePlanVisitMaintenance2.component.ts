import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { QueryParams } from '@shared/services/http-params-wrapper';

import { Subscription } from 'rxjs';

import { BaseComponent } from '@app/base/BaseComponent';
import { BranchSearchComponent } from './../search/iCABSBBranchSearch';
import { BranchServiceAreaSearchComponent } from './../search/iCABSBBranchServiceAreaSearch';
import { ICabsModalVO } from '@shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '@shared/constants/message.constant';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@base/PageIdentifier';

@Component({
    templateUrl: 'iCABSSePlanVisitMaintenance2.html'
})

export class PlanVisitMaintenance2Component extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    // URL Query Parameters
    private queryParams: Object = {
        operation: 'Service/iCABSSePlanVisitMaintenance2',
        module: 'plan-visits',
        method: 'service-planning/maintenance'
    };
    public parentMode: string;
    public pageId: string = '';
    public controls: any[] = [
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true },
        { name: 'BranchNumber', type: MntConst.eTypeInteger, required: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, required: true },
        { name: 'BranchServiceAreaDesc', disabled: true },
        { name: 'ContractName', disabled: true },
        { name: 'ContractNumber', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'CustomerVisitRef', disabled: true, required: false, type: MntConst.eTypeTextFree },
        { name: 'EndDate' },
        { name: 'OriginalVisitDueDate', type: MntConst.eTypeDate, required: true },
        { name: 'PlanQuantity', type: MntConst.eTypeInteger, required: true },
        { name: 'PlanVisitNumber', type: MntConst.eTypeInteger, disabled: true },
        { name: 'PlanVisitRowID' },
        { name: 'PremiseName', disabled: true },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger, disabled: true, required: true },
        { name: 'ProductCode', type: MntConst.eTypeCode, disabled: true, required: true },
        { name: 'ProductDesc', disabled: true },
        { name: 'SelectedDate' },
        { name: 'ServiceCoverNumber' },
        { name: 'ServiceCoverRowID' },
        { name: 'ServiceQuantity' },
        { name: 'StartDate' },
        { name: 'VisitDurationDefault', type: MntConst.eTypeText, disabled: true },
        { name: 'VisitTypeCode', type: MntConst.eTypeCode, required: true },
        { name: 'VisitTypeDesc', type: MntConst.eTypeText, required: true }
    ];

    //Visit type Search
    public dropDown: any = {
        menu: [],
        VisitTypeCode: {
            isRequired: true,
            triggerValidate: false,
            isDisabled: false,
            active: { id: '', text: '' },
            arrData: [],
            inputParams: {
                parentMode: 'LookUp'
            }
        }
    };

    //BranchService Area search
    public searchConfigs: any = {
        branchServiceAreaSearch: {
            isDisabled: true,
            params: {
                parentMode: 'LookUp-PlanVisit',
                BranchNumberServiceBranchNumber: '',
                ServiceBranchNumber: '',
                BranchName: ''
            },
            component: BranchServiceAreaSearchComponent
        },
        modalConfig: {
            backdrop: 'static',
            keyboard: true
        }
    };

    //Branch Search
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        }
    };
    public branchSelected: Object = {
        id: '',
        text: ''
    };

    //Global variables
    public isThInformationDisplayed: boolean = false;
    public lookUpSubscription: Subscription;
    public isDeleteButton: boolean = false;
    public trCustomerVisitRef: boolean = false; //ITA-984 syschar: 4400

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPLANVISITMAINTENANCE2;
        this.browserTitle = this.pageTitle = 'Plan Visit Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.windowOnLoad();
    }

    ngAfterViewInit(): void {
        this.setControlValue('PlanQuantity', '0');
        let firstDay: any;
        firstDay = this.globalize.parseDateToFixedFormat(new Date(this.riExchange.getParentHTMLValue('SelectedDate'))).toString();
        this.setControlValue('OriginalVisitDueDate', this.globalize.parseDateStringToDate(firstDay) || '');
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnLoad(): void {
        if (this.parentMode === 'Search' || this.parentMode === 'SearchAdd') {
            this.setControlValue('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
            this.setControlValue('ContractName', this.riExchange.getParentHTMLValue('ContractName'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
            this.setControlValue('PremiseName', this.riExchange.getParentHTMLValue('PremiseName'));
            this.setControlValue('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
            this.setControlValue('ProductDesc', this.riExchange.getParentHTMLValue('ProductDesc'));
            this.setControlValue('ServiceCoverRowID', this.riExchange.getParentHTMLValue('ServiceCoverRowID'));
            if (this.parentMode === 'Search') {
                this.isDeleteButton = true;
                this.setControlValue('PlanVisitRowID', this.riExchange.getParentHTMLValue('PlanVisitRowID'));
                this.planVisitLookUp();
            }
        }
        else if (this.parentMode === 'SearchAdd2') {
            this.setControlValue('ContractNumber', this.riExchange.getParentAttributeValue('ContractNumber'));
            this.setControlValue('ContractName', this.riExchange.getParentAttributeValue('ContractName'));
            this.setControlValue('PremiseNumber', this.riExchange.getParentAttributeValue('PremiseNumber'));
            this.setControlValue('PremiseName', this.riExchange.getParentAttributeValue('PremiseName'));
            this.setControlValue('ProductCode', this.riExchange.getParentAttributeValue('ProductCode'));
            this.setControlValue('ProductDesc', this.riExchange.getParentAttributeValue('ProductDesc'));
            this.setControlValue('ServiceCoverRowID', this.riExchange.getParentAttributeValue('ServiceCoverRowID'));
        }
        this.getSysCharDetails();
    }

    private getSysCharDetails(): any {
        let sysCharNumbers: number[] = [
            this.sysCharConstants.SystemCharEnableTimePlanning,
            this.sysCharConstants.SystemCharEnableCustomerVisitRef
        ];
        let sysCharIp: Object = {
            module: this.queryParams['module'],
            operation: this.queryParams['operation'],
            action: 0,
            businessCode: this.businessCode(),
            countryCode: this.countryCode(),
            SysCharList: sysCharNumbers.toString()
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.speedScript.sysChar(sysCharIp).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    let record: any = data.records;
                    this.pageParams.vEnableTimePlanning = record[0]['Required'];

                    this.trCustomerVisitRef = record[1].Required;  // ITA-984
                    if (this.trCustomerVisitRef) {
                        this.fetchCustomerVisitRefStatus();
                    }
                    if (this.pageParams.vEnableTimePlanning) {
                        this.isThInformationDisplayed = true;
                    }
                    if (this.parentMode !== 'Search') {
                        this.fetchRecord();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private fetchCustomerVisitRefStatus(): void {  //ITA-984
        let search: QueryParams = new QueryParams();
        search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');
        search.set('PlanVisitROWID', this.getControlValue('PlanVisitRowID') ? this.getControlValue('PlanVisitRowID') : this.getControlValue('ServiceCoverRowID'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], 'Application/iCABSAPlanVisitMaintenance', search)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data) {
                        if (data && data['hasError']) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));

                        } else {
                            this.setControlValue('CustomerVisitRef', data['CustomerVisitRef']);
                        }
                    }
                },
                error => {
                    this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.setControlValue('CustomerVisitRef', '');
                });
    }

    private fetchRecord(): void {
        let searchParams: QueryParams;
        let postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        postData['Function'] = 'GetServiceArea,GetServiceCoverDetails';
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                        this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
                        this.setControlValue('PlanQuantity', data.ServiceQuantity);
                        this.setControlValue('ServiceQuantity', data.ServiceQuantity);
                        this.branchSearchDropDown.active = {
                            id: data.BranchNumber,
                            text: data.BranchNumber + ' - ' + data.BranchName
                        };
                        this.setControlValue('BranchNumber', data.BranchNumber);
                        this.setControlValue('BranchName', data.BranchName);
                        this.searchConfigs.branchServiceAreaSearch.params.BranchNumberServiceBranchNumber = this.getControlValue('BranchNumber');
                        this.searchConfigs.branchServiceAreaSearch.params.ServiceBranchNumber = this.getControlValue('BranchNumber');
                        this.searchConfigs.branchServiceAreaSearch.params.BranchName = this.getControlValue('BranchName');
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });

    }

    private planVisitLookUp(): void {
        let lookupIP = [
            {
                'table': 'PlanVisit',
                'query': {
                    'BusinessCode': this.utils.getBusinessCode(),
                    'ROWID': this.getControlValue('PlanVisitRowID')
                },
                'fields': ['ContractNumber', 'PremiseNumber', 'ProductCode', 'ServiceCoverNumber', 'PlanVisitNumber', 'VisitTypeCode', 'BranchNumber', 'BranchServiceAreaCode', 'OriginalVisitDueDate', 'PlanQuantity', 'VisitDurationDefault']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe(
            (data) => {
                let responseData = data[0][0];
                this.setControlValue('BranchNumber', responseData.BranchNumber);
                this.setControlValue('BranchServiceAreaCode', responseData.BranchServiceAreaCode);
                this.setControlValue('PlanQuantity', responseData.PlanQuantity);
                this.setControlValue('PlanVisitNumber', responseData.PlanVisitNumber);
                this.setControlValue('ServiceCoverNumber', responseData.ServiceCoverNumber);
                this.setControlValue('VisitDurationDefault', responseData.VisitDurationDefault);
                this.setControlValue('VisitTypeCode', responseData.VisitTypeCode);
                let firstDay: any;
                firstDay = this.globalize.parseDateToFixedFormat(new Date(responseData.OriginalVisitDueDate)).toString();
                this.setControlValue('OriginalVisitDueDate', this.globalize.parseDateStringToDate(firstDay));
                this.doPlanVisitlookUp();
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private doPlanVisitlookUp(): void {
        let lookupIP = [
            {
                'table': 'Branch',
                'query': {
                    'BranchNumber': this.getControlValue('BranchNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['BranchName']
            },
            {
                'table': 'BranchServiceArea',
                'query': {
                    'BranchNumber': this.getControlValue('BranchNumber'),
                    'BranchServiceAreaCode': this.getControlValue('BranchServiceAreaCode'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['BranchServiceAreaDesc']
            },
            {
                'table': 'VisitType',
                'query': {
                    'VisitTypeCode': this.getControlValue('VisitTypeCode'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['VisitTypeDesc']
            }
        ];
        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe(
            (data) => {
                let branchData = data[0][0];
                let branchServiceAreaData = data[1][0];
                let visitTypeData = data[2][0];
                this.setControlValue('BranchName', branchData.BranchName);
                this.setControlValue('BranchServiceAreaDesc', branchServiceAreaData.BranchServiceAreaDesc);
                this.setControlValue('VisitTypeDesc', visitTypeData.VisitTypeDesc);
                this.branchSelected = {
                    id: this.getControlValue('BranchNumber'),
                    text: this.getControlValue('BranchNumber') + ' - ' + this.getControlValue('BranchName')
                };
                this.searchConfigs.branchServiceAreaSearch.params.BranchNumberServiceBranchNumber = this.getControlValue('BranchNumber');
                this.searchConfigs.branchServiceAreaSearch.params.ServiceBranchNumber = this.getControlValue('BranchNumber');
                this.searchConfigs.branchServiceAreaSearch.params.BranchName = this.getControlValue('BranchName');
                this.dropDown.VisitTypeCode.active = {
                    id: this.getControlValue('VisitTypeCode'),
                    text: this.getControlValue('VisitTypeCode') + ' - ' + this.getControlValue('VisitTypeDesc')
                };
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private doLookupformData(): void {
        let lookupIP = [
            {
                'table': 'Contract',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['ContractName']
            },
            {
                'table': 'Premise',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['PremiseName']
            },
            {
                'table': 'Product',
                'query': {
                    'ProductCode': this.getControlValue('ProductCode'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['ProductDesc']
            },
            {
                'table': 'Branch',
                'query': {
                    'BranchNumber': this.getControlValue('BranchNumber'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['BranchName']
            },
            {
                'table': 'BranchServiceArea',
                'query': {
                    'BranchNumber': this.getControlValue('BranchNumber'),
                    'BranchServiceAreaCode': this.getControlValue('BranchServiceAreaCode'),
                    'BusinessCode': this.utils.getBusinessCode()
                },
                'fields': ['BranchServiceAreaDesc']
            }
        ];

        this.lookUpSubscription = this.LookUp.lookUpRecord(lookupIP).subscribe(
            (data) => {
                if (data.length > 0) {
                    let contractData = data[0][0];
                    let premiseData = data[1][0];
                    let productData = data[2][0];
                    let branchData = data[3][0];
                    let branchServiceAreaData = data[4][0];
                    if (data[4][0]) {
                        this.setControlValue('BranchServiceAreaDesc', branchServiceAreaData.BranchServiceAreaDesc);
                    }
                    else {
                        this.setControlValue('BranchServiceAreaDesc', '');
                        this.setControlValue('BranchServiceAreaCode', '');
                    }
                    if (this.riExchange.validateForm(this.uiForm) && this.getControlValue('VisitTypeCode')) {
                        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, '', this.promptConfirmSave.bind(this)));
                    }
                    else {
                        this.dropDown.VisitTypeCode.triggerValidate = true;
                    }
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private fetchVisitDuration(): void {
        let searchParams: QueryParams;
        let postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['ServiceCoverRowID'] = this.getControlValue('ServiceCoverRowID');
        postData['Function'] = 'GetDefaultDuration';
        postData['BranchNumber'] = this.getControlValue('BranchNumber');
        postData['VisitTypeCode'] = this.getControlValue('VisitTypeCode');
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    }
                    else {
                        this.setControlValue('VisitDurationDefault', data.VisitDurationString);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
    }

    private openBranchSearch(): void {
        this.searchConfigs.branchServiceAreaSearch.params.BranchNumberServiceBranchNumber = this.getControlValue('BranchNumber');
        this.searchConfigs.branchServiceAreaSearch.params.ServiceBranchNumber = this.getControlValue('BranchNumber');
        this.searchConfigs.branchServiceAreaSearch.params.BranchName = this.getControlValue('BranchName');
    }

    private promptConfirmSave(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams = this.getURLSearchParamObject();
        let formdata: any;
        formdata = {
            PlanVisitRowID: this.getControlValue('PlanVisitRowID') || '',
            BusinessCode: this.businessCode(),
            ContractNumber: this.getControlValue('ContractNumber'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            ProductCode: this.getControlValue('ProductCode'),
            ServiceCoverNumber: this.getControlValue('ServiceCoverNumber'),
            PlanVisitNumber: this.getControlValue('PlanVisitNumber'),
            VisitTypeCode: this.getControlValue('VisitTypeCode'),
            BranchNumber: this.getControlValue('BranchNumber'),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            OriginalVisitDueDate: this.getControlValue('OriginalVisitDueDate'),
            PlanQuantity: this.getControlValue('PlanQuantity'),
            VisitDurationDefault: this.getControlValue('VisitDurationDefault'),
            StartDate: this.getControlValue('StartDate'),
            EndDate: this.getControlValue('EndDate'),
            ServiceQuantity: this.getControlValue('ServiceQuantity'),
            ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
        };
        searchParams.set(this.serviceConstants.Action, (this.parentMode === 'Search') ? '2' : '1');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.formPristine();
                    this.location.back();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    private promptConfirmDelete(): void {
        let searchParams: QueryParams = new QueryParams();
        searchParams = this.getURLSearchParamObject();
        let formdata: any;
        formdata = {
            ROWID: this.getControlValue('PlanVisitRowID')
        };
        searchParams.set(this.serviceConstants.Action, '3');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams, formdata).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    this.location.back();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }
        );
    }

    public visitTypeSelectedValue(value: any): void {
        if (value.VisitTypeCode !== '' && value.VisitTypeDesc !== '') {
            this.setControlValue('VisitTypeCode', value.VisitTypeCode || value.value);
            this.setControlValue('VisitTypeDesc', value.VisitTypeDesc || value.value);
            if (this.parentMode !== 'Search') {
                this.fetchVisitDuration();
            }
            this.uiForm.controls['VisitTypeCode'].markAsDirty();
        }
        else {
            this.dropDown.VisitTypeCode.triggerValidate = true;
        }
    }

    public visitTypeDataRecieved(event: any): any {
        let len: number;
        len = event.length || 0;
        this.dropDown.VisitTypeCode.arrData = [];
        for (let i = 0; i < len; i++) {
            this.dropDown.VisitTypeCode.arrData.push({ code: event[i]['VisitType.VisitTypeCode'], desc: event[i]['VisitType.VisitTypeDesc'] });
        }
    }

    public onBranchDataReceived(data: any): void {
        if (data['BranchNumber']) {
            this.setControlValue('BranchNumber', data['BranchNumber']);
            this.setControlValue('BranchName', data['BranchName']);
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
        this.uiForm.controls['BranchNumber'].markAsDirty();
        this.openBranchSearch();
    }

    public setBranchServiceArea(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('BranchServiceAreaDesc', data.BranchServiceAreaDesc);
            this.uiForm.controls['BranchServiceAreaCode'].markAsDirty();
        }
    }

    public datePickerSelectedValue(value: any): void {
        if (value && value.value)
            this.setControlValue('OriginalVisitDueDate', value.value);
        this.uiForm.controls['OriginalVisitDueDate'].markAsDirty();
    }

    // Clicking on Save button
    public saveOnClick(): void {
        this.doLookupformData();
    }

    // Clicking on Cancel button
    public cancelOnClick(): void {
        this.location.back();
    }

    // Clicking on Delete button
    public deleteOnClick(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, '', this.promptConfirmDelete.bind(this)));
    }
}
