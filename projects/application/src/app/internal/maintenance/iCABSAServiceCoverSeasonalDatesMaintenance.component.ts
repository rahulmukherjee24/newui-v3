import { Component, OnInit, AfterViewInit, OnDestroy, Injector, ViewChild, EventEmitter } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { InternalGridSearchSalesModuleRoutes, InternalGridSearchApplicationModuleRoutes } from './../../base/PageRoutes';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst, RiTab } from '../../../shared/services/riMaintenancehelper';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { ContractSearchComponent } from '../../internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from '../../internal/search/iCABSAPremiseSearch';
import { ServiceCoverSearchComponent } from '../../internal/search/iCABSAServiceCoverSearch';
import { SeasonalTemplateSearchComponent } from '../../internal/search/iCABSBSeasonalTemplateSearch';

@Component({
    templateUrl: 'iCABSAServiceCoverSeasonalDatesMaintenance.html'
})

export class ServiceCoverSeasonalDatesMaintenanceComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private queryParams: any = {
        operation: 'Application/iCABSAServiceCoverSeasonalDatesMaintenance',
        module: 'template',
        method: 'service-planning/maintenance'
    };

    public setFocusSeasonalServiceInd = new EventEmitter<boolean>();
    public pageId: string = '';
    public controls: Array<any> = [
        { name: 'ContractNumber', type: MntConst.eTypeCode },
        { name: 'ContractName', type: MntConst.eTypeText },
        { name: 'PremiseNumber', type: MntConst.eTypeInteger },
        { name: 'PremiseName', type: MntConst.eTypeText },
        { name: 'ProductCode', type: MntConst.eTypeCode },
        { name: 'ProductDesc', type: MntConst.eTypeText },
        { name: 'Status', type: MntConst.eTypeText },
        { name: 'ServiceCommenceDate', type: MntConst.eTypeDate },
        { name: 'ServiceVisitFrequency', type: MntConst.eTypeInteger },
        { name: 'ServiceQuantity', type: MntConst.eTypeInteger },
        { name: 'ServiceVisitAnnivDate', type: MntConst.eTypeDate },
        { name: 'SeasonalServiceInd', type: MntConst.eTypeCheckBox },
        { name: 'LastChangeEffectDate', type: MntConst.eTypeDate, required: true },
        { name: 'NumberOfSeasons', type: MntConst.eTypeInteger },
        { name: 'SeasonalTemplateNumber', type: MntConst.eTypeInteger },
        { name: 'TemplateName', type: MntConst.eTypeText },
        { name: 'SeasonalBranchUpdate', type: MntConst.eTypeCheckBox },
        { name: 'Menu', type: MntConst.eTypeText },
        { name: 'SeasonNumber1', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate1', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek1', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear1', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate1', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek1', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear1', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits1', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber2', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate2', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek2', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear2', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate2', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek2', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear2', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits2', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber3', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate3', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek3', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear3', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate3', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek3', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear3', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits3', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber4', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate4', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek4', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear4', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate4', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek4', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear4', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits4', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber5', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate5', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek5', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear5', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate5', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek5', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear5', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits5', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber6', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate6', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek6', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear6', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate6', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek6', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear6', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits6', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber7', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate7', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek7', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear7', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate7', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek7', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear7', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits7', type: MntConst.eTypeInteger },
        { name: 'SeasonNumber8', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromDate8', type: MntConst.eTypeDate },
        { name: 'SeasonalFromWeek8', type: MntConst.eTypeInteger },
        { name: 'SeasonalFromYear8', type: MntConst.eTypeInteger },
        { name: 'SeasonalToDate8', type: MntConst.eTypeDate },
        { name: 'SeasonalToWeek8', type: MntConst.eTypeInteger },
        { name: 'SeasonalToYear8', type: MntConst.eTypeInteger },
        { name: 'SeasonNoOfVisits8', type: MntConst.eTypeInteger },
        // Hidden field
        { name: 'ServiceCoverNumber', type: MntConst.eTypeInteger },
        { name: 'ServiceCoverRowID', type: MntConst.eTypeText }
    ];
    public riTab: RiTab;
    public tab: any = {
        tab1: { id: 'grdGeneral', name: 'General', active: true },
        tab2: { id: 'grdSeasonal', name: 'Seasonal', active: false }
    };
    public isAutoOpenContractSearch: boolean = false;
    public isAutoOpenProductSearch: boolean = false;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSASERVICECOVERSEASONALDATESMAINTENANCE;

        this.pageParams.isDisableSave = false;
        this.pageParams.isDisableCancel = false;
    }

    ngOnInit(): void {
        super.ngOnInit();
        if (!this.isReturning()) {
            this.onWindowLoad();
            this.enableDisablePageControls(false);
        } else {
            this.onCancelClick();
        }

        this.disableControl('ContractName', true);
        this.disableControl('PremiseName', true);
        this.disableControl('ProductDesc', true);
        this.disableControl('Status', true);
        this.disableControl('ServiceVisitFrequency', true);
        this.disableControl('ServiceQuantity', true);
        this.disableControl('ServiceCommenceDate', true);
        this.disableControl('ServiceVisitAnnivDate', true);
        this.disableControl('TemplateName', true);
        this.setControlValue('Menu', 'Options');

        switch (this.riExchange.setCurrentContractType()) {
            case 'C':
                this.pageParams.contractLabel = 'Contract Number';
                this.pageTitle = this.browserTitle = 'Contract Service Cover Seasonal Dates Maintenance';
                break;
            case 'J':
                this.pageParams.contractLabel = 'Job Number';
                this.pageTitle = this.browserTitle = 'Job Service Cover Seasonal Dates Maintenance';
                break;
            case 'P':
                this.pageParams.contractLabel = 'Product Sale Number';
                this.pageTitle = this.browserTitle = 'Product Sale Service Cover Seasonal Dates Maintenance';
                break;
        }
        this.utils.setTitle(this.browserTitle);
    }

    ngAfterViewInit(): void {
        this.isAutoOpenContractSearch = this.getControlValue('ContractNumber') ? false : true;

        this.riTab = new RiTab(this.tab, this.utils);
        this.tab = this.riTab.tabObject;
        this.riTab.TabAdd('General');
        this.riTab.TabAdd('Seasonal');
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private onWindowLoad(): void {
        this.setPageMasterData();
        this.pageParams.currentContractTypeURLParameter = this.riExchange.getCurrentContractTypeUrlParam();
        this.pageParams.ellipsisParams.contractNumber.childConfigParams.currentContractTypeURLParameter = this.pageParams.currentContractTypeURLParameter;
        this.pageParams.ellipsisParams.premiseNumber.childConfigParams.currentContractTypeURLParameter = this.pageParams.currentContractTypeURLParameter;
        this.pageParams.ellipsisParams.productCode.childConfigParams.currentContractTypeURLParameter = this.pageParams.currentContractTypeURLParameter;

        this.pageParams.seasonMaxLength = (this.pageParams.seasonalMaxNumberSeasons > 9) ? 2 : 1;

        if (this.parentMode === 'ServiceCover') {
            let contractNumber: string = this.riExchange.getParentHTMLValue('ContractNumber');
            let contractName: string = this.riExchange.getParentHTMLValue('ContractName');
            let premiseNumber: string = this.riExchange.getParentHTMLValue('PremiseNumber');
            let premiseName: string = this.riExchange.getParentHTMLValue('PremiseName');
            let productCode: string = this.riExchange.getParentHTMLValue('ProductCode');
            let productDesc: string = this.riExchange.getParentHTMLValue('ProductDesc');

            this.setControlValue('ContractNumber', contractNumber);
            this.setControlValue('ContractName', contractName);
            this.setControlValue('PremiseNumber', premiseNumber);
            this.setControlValue('PremiseName', premiseName);
            this.setControlValue('ProductCode', productCode);
            this.setControlValue('ProductDesc', productDesc);

            this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractNumber = contractNumber;
            this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractName = contractName;
            this.pageParams.ellipsisParams.productCode.childConfigParams.ContractNumber = contractNumber;
            this.pageParams.ellipsisParams.productCode.childConfigParams.ContractName = contractName;
            this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseNumber = premiseNumber;
            this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseName = premiseName;
            this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = productCode;
            this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = productDesc;

            this.fetchRecord();
        }
    }

    private setPageMasterData(): void {
        this.pageParams.ellipsisParams = {
            contractNumber: {
                isShowCloseButton: true,
                isShowHeader: true,
                isDisabled: false,
                childConfigParams: {
                    parentMode: 'LookUp',
                    currentContractTypeURLParameter: ''
                },
                contentComponent: ContractSearchComponent,
                isShowEllipsis: true
            },
            premiseNumber: {
                isShowCloseButton: true,
                isShowHeader: true,
                isDisabled: false,
                childConfigParams: {
                    parentMode: 'LookUp',
                    currentContractTypeURLParameter: '',
                    ContractNumber: '',
                    ContractName: ''
                },
                contentComponent: PremiseSearchComponent,
                isShowEllipsis: true
            },
            productCode: {
                isShowCloseButton: true,
                isShowHeader: true,
                isDisabled: false,
                childConfigParams: {
                    parentMode: 'LookUp',
                    currentContractTypeURLParameter: '',
                    ContractNumber: '',
                    ContractName: '',
                    PremiseNumber: '',
                    PremiseName: '',
                    ProductCode: '',
                    ProductDesc: ''
                },
                contentComponent: ServiceCoverSearchComponent,
                isShowEllipsis: true
            },
            seasonalTemplate: {
                isShowCloseButton: true,
                isShowHeader: true,
                isDisabled: false,
                childConfigParams: {
                    parentMode: 'LookUp'
                },
                contentComponent: SeasonalTemplateSearchComponent,
                isShowEllipsis: true
            },
            common: {
                modalConfig: {
                    backdrop: 'static',
                    keyboard: true
                }
            }
        };
        this.pageParams.seasonalMaxNumberSeasons = 8;
        this.pageParams.seasonMaxLength = 1;
        this.pageParams.isExistingSeasonalInd = false;
        this.pageParams.isCurrentSeasonalInd = false;
        this.pageParams.currentContractTypeURLParameter = '';
        this.pageParams.annualSeasonLayoutArr = [];
        this.pageParams.contractLabel = '';
        this.pageParams.isMandatoryNoOfVisits = false;
        this.pageParams.isMandatorySeason = false;
    }

    private fetchRecord(): void {
        let search = this.getURLSearchParamObject();
        search.set(this.serviceConstants.ContractNumber, this.getControlValue('ContractNumber'));
        search.set(this.serviceConstants.PremiseNumber, this.getControlValue('PremiseNumber'));
        search.set(this.serviceConstants.ProductCode, this.getControlValue('ProductCode'));
        if (this.getControlValue('ServiceCoverRowID')) { search.set('ServiceCoverRowID', this.getControlValue('ServiceCoverRowID')); }
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.enableDisablePageControls(false);
                } else {
                    if (data.findResult === '<Single>') {
                        this.clearPageControls();
                        this.enableDisablePageControls(true);
                        this.setControlValue('SeasonalServiceInd', data.SeasonalServiceInd === 'yes');
                        this.setControlValue('SeasonalBranchUpdate', data.SeasonalBranchUpdate === 'yes');
                        this.setControlValue('ServiceCoverRowID', data.ServiceCover);

                        for (let key in data) {
                            if (key && data[key].length > 0) {
                                if (!(key === 'SeasonalServiceInd' || key === 'SeasonalBranchUpdate' || key === 'ServiceCoverRowID')) {
                                    this.setControlValue(key, data[key]);
                                }
                            }
                        }

                        this.pageParams.isDisableSave = false;
                        this.pageParams.isDisableCancel = false;
                        this.pageParams.isExistingSeasonalInd = this.getControlValue('SeasonalServiceInd');
                        this.pageParams.isCurrentSeasonalInd = this.pageParams.isExistingSeasonalInd;
                        this.showSeasonRows();
                        this.enableDisableSeasonInputs();
                        this.setControlValue('LastChangeEffectDate', '');
                        setTimeout(() => {
                            this.setFocusSeasonalServiceInd.emit(true);
                        }, 0);
                        this.riTab.TabsToNormal();
                        this.formPristine();
                    } else if (data.findResult === '<Multi>') {
                        this.isAutoOpenProductSearch = true;
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private getLookupData(callType: string): void {
        let lookupIPDetails: Array<any> = [];
        let contractIndex: number = -1;
        let premiseIndex: number = -1;
        let productIndex: number = -1;
        let seasonalTemplateIndex: number = -1;

        if ((callType === 'A' || callType === 'C') && this.getControlValue('ContractNumber')) {
            lookupIPDetails.push({
                'table': 'Contract',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['ContractNumber', 'ContractName']
            });
            contractIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'P') && this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            lookupIPDetails.push({
                'table': 'Premise',
                'query': {
                    'ContractNumber': this.getControlValue('ContractNumber'),
                    'PremiseNumber': this.getControlValue('PremiseNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['PremiseNumber', 'PremiseName']
            });
            premiseIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'S') && this.getControlValue('ProductCode')) {
            lookupIPDetails.push({
                'table': 'Product',
                'query': {
                    'ProductCode': this.getControlValue('ProductCode'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['ProductCode', 'ProductDesc']
            });
            productIndex = lookupIPDetails.length - 1;
        }

        if ((callType === 'A' || callType === 'T') && this.getControlValue('SeasonalTemplateNumber')) {
            lookupIPDetails.push({
                'table': 'SeasonalTemplate',
                'query': {
                    'SeasonalTemplateNumber': this.getControlValue('SeasonalTemplateNumber'),
                    'BusinessCode': this.businessCode()
                },
                'fields': ['SeasonalTemplateNumber', 'TemplateName']
            });
            seasonalTemplateIndex = lookupIPDetails.length - 1;
        }

        if (lookupIPDetails.length > 0) {
            this.LookUp.lookUpPromise(lookupIPDetails).then((data) => {
                let contractDetail: any = null;
                let premiseDetail: any = null;
                let productDetail: any = null;
                let seasonalTemplateDetail: any = null;

                if ((callType === 'A' || callType === 'C') && contractIndex > -1 && data && data.length > 0 && data[contractIndex].length > 0) { contractDetail = data[contractIndex][0]; }
                if ((callType === 'A' || callType === 'P') && premiseIndex > -1 && data && data.length > 0 && data[premiseIndex].length > 0) { premiseDetail = data[premiseIndex][0]; }
                if ((callType === 'A' || callType === 'S') && productIndex > -1 && data && data.length > 0 && data[productIndex].length > 0) { productDetail = data[productIndex][0]; }
                if ((callType === 'A' || callType === 'T') && seasonalTemplateIndex > -1 && data && data.length > 0 && data[seasonalTemplateIndex].length > 0) { seasonalTemplateDetail = data[seasonalTemplateIndex][0]; }

                if (contractDetail && contractDetail.ContractName) {
                    this.setControlValue('ContractName', contractDetail.ContractName);

                    this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractNumber = contractDetail.ContractNumber;
                    this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractName = contractDetail.ContractName;

                    this.pageParams.ellipsisParams.productCode.childConfigParams.ContractNumber = contractDetail.ContractNumber;
                    this.pageParams.ellipsisParams.productCode.childConfigParams.ContractName = contractDetail.ContractName;
                }

                if (premiseDetail && premiseDetail.PremiseName) {
                    this.setControlValue('PremiseName', premiseDetail.PremiseName);

                    this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseNumber = premiseDetail.PremiseNumber;
                    this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseName = premiseDetail.PremiseName;
                }

                if (productDetail && productDetail.ProductDesc) {
                    this.setControlValue('ProductDesc', productDetail.ProductDesc);

                    this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = productDetail.ProductDesc;
                }

                if (seasonalTemplateDetail && seasonalTemplateDetail.TemplateName) {
                    this.setControlValue('TemplateName', seasonalTemplateDetail.TemplateName);
                }
            });
        }
    }

    private showSeasonRows(): void {
        let iNumberOfSeasons: number;

        if (this.getControlValue('NumberOfSeasons') === '') {
            iNumberOfSeasons = 1;
        } else {
            iNumberOfSeasons = this.getControlValue('NumberOfSeasons');
        }

        // Check value of NumberOfSeasons
        if (iNumberOfSeasons < 1) {
            iNumberOfSeasons = 1;
            this.setControlValue('NumberOfSeasons', 1);
        } else if (iNumberOfSeasons > this.pageParams.seasonalMaxNumberSeasons) {
            iNumberOfSeasons = this.pageParams.seasonalMaxNumberSeasons;
            this.setControlValue('NumberOfSeasons', this.pageParams.seasonalMaxNumberSeasons);
        }

        this.pageParams.annualSeasonLayoutArr = [];

        for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
            this.showHideRow(i, (i <= iNumberOfSeasons));
        }
    }

    private showHideRow(ipSeason: number, isVisible: boolean): void {
        let isDisableVisits: boolean = true;
        let isEnableRow: boolean = false;
        let isReEnableVisits: boolean = false;

        if (isVisible) {
            this.pageParams.annualSeasonLayoutArr.push(ipSeason);
        }

        // Set required status of input fields
        if (!this.pageParams.isExistingSeasonalInd &&
            !(this.getControlValue('SeasonalTemplateNumber') !== '' && this.getControlValue('SeasonalBranchUpdate', false))) {
            isDisableVisits = true;
            isEnableRow = isVisible;
            isReEnableVisits = false;
        } else {
            isEnableRow = false;
            if (!this.pageParams.isExistingSeasonalInd) {
                isDisableVisits = !isVisible;
                isReEnableVisits = true;
            } else {
                isDisableVisits = true;
                isReEnableVisits = false;
            }
        }

        this.enableSeasonRow(ipSeason, isEnableRow, isDisableVisits);

        if (isReEnableVisits) { this.disableControl('SeasonNoOfVisits' + ipSeason, false); }
    }

    private enableDisablePageControls(isEnable: boolean): void {
        this.pageParams.isDisableSave = !isEnable;
        this.pageParams.isDisableCancel = !isEnable;
        this.pageParams.ellipsisParams.seasonalTemplate.isDisabled = !isEnable;

        if (isEnable) {
            this.disableControl('SeasonalServiceInd', false);
        } else {
            this.uiForm.disable();
        }

        this.disableControl('ContractNumber', false);
        this.disableControl('PremiseNumber', false);
        this.disableControl('ProductCode', false);
        this.disableControl('Menu', false);
    }

    private enableSeasonRow(ipRow: number, isEnable: boolean, isIncludeDisableVisits: boolean): void {
        this.disableControl('SeasonNumber' + ipRow, !isEnable);
        this.disableControl('SeasonalFromDate' + ipRow, !isEnable);
        this.disableControl('SeasonalFromWeek' + ipRow, !isEnable);
        this.disableControl('SeasonalFromYear' + ipRow, !isEnable);
        this.disableControl('SeasonalToDate' + ipRow, !isEnable);
        this.disableControl('SeasonalToWeek' + ipRow, !isEnable);
        this.disableControl('SeasonalToYear' + ipRow, !isEnable);

        if (isEnable) {
            this.disableControl('SeasonNoOfVisits' + ipRow, !isEnable);
        } else {
            if (isIncludeDisableVisits) { this.disableControl('SeasonNoOfVisits' + ipRow, true); }
        }

        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonNumber' + ipRow, isEnable);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonalFromDate' + ipRow, isEnable);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonalFromWeek' + ipRow, isEnable);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonalFromYear' + ipRow, isEnable);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonalToDate' + ipRow, isEnable);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonalToWeek' + ipRow, isEnable);
        this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonalToYear' + ipRow, isEnable);

        if (isEnable || isIncludeDisableVisits) {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonNoOfVisits' + ipRow, isEnable);
        } else {
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'SeasonNoOfVisits' + ipRow, true);
        }

        this.pageParams.isMandatorySeason = !this.riExchange.riInputElement.isDisabled(this.uiForm, 'SeasonNumber1');
        this.pageParams.isMandatoryNoOfVisits = !this.riExchange.riInputElement.isDisabled(this.uiForm, 'SeasonNoOfVisits1');
    }

    private getTemplateSeasons(): void {
        let formData: Object = {};
        let search = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '6');

        formData['SeasonalTemplateNumber'] = this.getControlValue('SeasonalTemplateNumber');
        if (this.getControlValue('LastChangeEffectDate')) { formData['LastChangeEffectDate'] = this.getControlValue('LastChangeEffectDate'); }
        formData[this.serviceConstants.Function] = 'GetSeasonalTemplateDetails,GetSeasonalTemplateDates';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('TemplateName', data.TemplateName);
                    this.setControlValue('NumberOfSeasons', data.NumberOfSeasons);
                    this.setControlValue('SeasonalBranchUpdate', data.SeasonalBranchUpdate);
                    for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
                        this.setControlValue('SeasonNumber' + i, data['SeasonNumber' + i]);
                        this.setControlValue('SeasonalFromDate' + i, data['SeasonalFromDate' + i]);
                        this.setControlValue('SeasonalFromWeek' + i, data['SeasonalFromWeek' + i]);
                        this.setControlValue('SeasonalFromYear' + i, data['SeasonalFromYear' + i]);
                        this.setControlValue('SeasonalToDate' + i, data['SeasonalToDate' + i]);
                        this.setControlValue('SeasonalToWeek' + i, data['SeasonalToWeek' + i]);
                        this.setControlValue('SeasonalToYear' + i, data['SeasonalToYear' + i]);
                        this.setControlValue('SeasonNoOfVisits' + i, data['SeasonNoOfVisits' + i]);
                    }
                    this.showSeasonRows();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private getTemplateDates(): void {
        let formData: Object = {};
        let search = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '6');

        formData['SeasonalTemplateNumber'] = this.getControlValue('SeasonalTemplateNumber');
        if (this.getControlValue('LastChangeEffectDate')) { formData['LastChangeEffectDate'] = this.getControlValue('LastChangeEffectDate'); }
        formData[this.serviceConstants.Function] = 'GetSeasonalTemplateDates';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('SeasonalBranchUpdate', data.SeasonalBranchUpdate);
                    for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
                        this.setControlValue('SeasonNumber' + i, data['SeasonNumber' + i]);
                        this.setControlValue('SeasonalFromDate' + i, data['SeasonalFromDate' + i]);
                        this.setControlValue('SeasonalFromWeek' + i, data['SeasonalFromWeek' + i]);
                        this.setControlValue('SeasonalFromYear' + i, data['SeasonalFromYear' + i]);
                        this.setControlValue('SeasonalToDate' + i, data['SeasonalToDate' + i]);
                        this.setControlValue('SeasonalToWeek' + i, data['SeasonalToWeek' + i]);
                        this.setControlValue('SeasonalToYear' + i, data['SeasonalToYear' + i]);
                        this.setControlValue('SeasonNoOfVisits' + i, data['SeasonNoOfVisits' + i]);
                    }
                    this.showSeasonRows();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private getServiceSeasons(): void {
        let formData: Object = {};
        let search = this.getURLSearchParamObject();

        search.set(this.serviceConstants.Action, '6');

        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        formData['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        if (this.getControlValue('LastChangeEffectDate')) { formData['LastChangeEffectDate'] = this.getControlValue('LastChangeEffectDate'); }
        formData[this.serviceConstants.Function] = 'GetSeasonalServiceDates';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.setControlValue('SeasonalBranchUpdate', data.SeasonalBranchUpdate);
                    this.setControlValue('NumberOfSeasons', data.NumberOfSeasons);
                    for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
                        this.setControlValue('SeasonNumber' + i, data['SeasonNumber' + i]);
                        this.setControlValue('SeasonalFromDate' + i, data['SeasonalFromDate' + i]);
                        this.setControlValue('SeasonalFromWeek' + i, data['SeasonalFromWeek' + i]);
                        this.setControlValue('SeasonalFromYear' + i, data['SeasonalFromYear' + i]);
                        this.setControlValue('SeasonalToDate' + i, data['SeasonalToDate' + i]);
                        this.setControlValue('SeasonalToWeek' + i, data['SeasonalToWeek' + i]);
                        this.setControlValue('SeasonalToYear' + i, data['SeasonalToYear' + i]);
                        this.setControlValue('SeasonNoOfVisits' + i, data['SeasonNoOfVisits' + i]);
                    }
                    this.showSeasonRows();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private enableDisableSeasonInputs(): void {
        let isEnableSeasonRows: boolean = false;
        let isShowSeasonRowsBeforeEnable: boolean = false;
        let isShowSeasonRowsAfterEnable: boolean = false;
        let isEnableEffectiveDate: boolean = false;
        let isDisableVisitInputs: boolean = true;

        if (this.pageParams.isExistingSeasonalInd) {
            this.disableControl('NumberOfSeasons', true);
            this.disableControl('SeasonalTemplateNumber', true);
            this.pageParams.ellipsisParams.seasonalTemplate.isDisabled = true;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'NumberOfSeasons', true);
            isEnableSeasonRows = false;
            isEnableEffectiveDate = !this.getControlValue('SeasonalServiceInd');
        } else {
            isEnableSeasonRows = isEnableEffectiveDate = this.getControlValue('SeasonalServiceInd');
            if (this.getControlValue('SeasonalServiceInd')) {
                // If no template is specified, or if template specified but should not follow it, disable "Number Of Seasons" input
                if (this.getControlValue('SeasonalTemplateNumber') === '' || this.getControlValue('SeasonalBranchUpdate') === false) {
                    this.disableControl('NumberOfSeasons', false);
                } else {
                    this.disableControl('NumberOfSeasons', true);
                    isEnableSeasonRows = false;
                    isDisableVisitInputs = false;
                }

                this.disableControl('SeasonalTemplateNumber', false);
                this.pageParams.ellipsisParams.seasonalTemplate.isDisabled = false;
                isShowSeasonRowsAfterEnable = true;
            } else {
                this.disableControl('NumberOfSeasons', true);
                this.disableControl('SeasonalTemplateNumber', true);
                this.pageParams.ellipsisParams.seasonalTemplate.isDisabled = true;

                // Show correct number of season rows
                this.setControlValue('NumberOfSeasons', '');
                isShowSeasonRowsBeforeEnable = true;
            }
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'NumberOfSeasons', this.getControlValue('SeasonalServiceInd'));
        }

        // If the SC is changed to/already Seasonal Service, and it follows a template and the user is updating,
        // then allow the "Update Branch" indicator to be changed. Otherwise, do not allow changes to this field.
        if (this.getControlValue('SeasonalServiceInd') && this.getControlValue('SeasonalTemplateNumber')) {
            this.disableControl('SeasonalBranchUpdate', false);
        } else {
            this.disableControl('SeasonalBranchUpdate', true);
        }

        this.disableControl('LastChangeEffectDate', false);

        // Show the season rows before we enable them
        if (isShowSeasonRowsBeforeEnable) {
            this.showSeasonRows();
        }

        // Enable/Disable all season rows
        for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
            this.enableSeasonRow(i, isEnableSeasonRows, isDisableVisitInputs);
        }

        // Show the season rows after we enable them
        if (isShowSeasonRowsAfterEnable) {
            this.showSeasonRows();
        }
    }

    private seasonalDateChange(ipFromDate: string, opFromWeek: string, opFromYear: string): void {
        // Call "isError", in order to get the formatting correct, and to check that the user has not entered an incorrect value
        if (this.getControlValue(ipFromDate) && !this.riExchange.riInputElement.isError(this.uiForm, ipFromDate)) {
            let formData: Object = {};
            let search = this.getURLSearchParamObject();

            search.set(this.serviceConstants.Action, '6');

            formData['WeekCalcDate'] = this.getControlValue(ipFromDate);
            formData[this.serviceConstants.Function] = 'GetWeekFromDate';

            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
                .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        this.setControlValue(opFromWeek, data.WeekCalcNumber);
                        this.setControlValue(opFromYear, data.WeekCalcYear);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue(opFromWeek, '');
            this.setControlValue(opFromYear, '');
        }
    }

    private seasonalWeekChange(ipRequestFunction: string, ipFromWeek: string, ipFromYear: string, opFromDate: string): void {
        if (this.getControlValue(ipFromWeek) && this.getControlValue(ipFromYear)) {
            // Call "isError", in order to get the formatting correct, and to check that the user has not entered an incorrect value
            if (!this.riExchange.riInputElement.isError(this.uiForm, ipFromWeek) && !this.riExchange.riInputElement.isError(this.uiForm, ipFromYear)) {
                let formData: Object = {};
                let search = this.getURLSearchParamObject();

                search.set(this.serviceConstants.Action, '6');

                formData['WeekCalcNumber'] = this.getControlValue(ipFromWeek);
                formData['WeekCalcYear'] = this.getControlValue(ipFromYear);
                formData[this.serviceConstants.Function] = ipRequestFunction;

                this.ajaxSource.next(this.ajaxconstant.START);
                this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
                    .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        } else {
                            this.setControlValue(opFromDate, data.WeekCalcDate);
                            this.setControlValue(ipFromWeek, data.WeekCalcNumber);
                            this.setControlValue(ipFromYear, data.WeekCalcYear);
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
            } else {
                this.setControlValue(opFromDate, '');
            }
        } else {
            this.setControlValue(opFromDate, '');
        }
    }

    private seasonalFromWeekChange(ipFromWeek: string, ipFromYear: string, opFromDate: string): void {
        this.seasonalWeekChange('GetWeekStartFromWeek', ipFromWeek, ipFromYear, opFromDate);
    }

    private seasonalToWeekChange(ipFromWeek: string, ipFromYear: string, opFromDate: string): void {
        this.seasonalWeekChange('GetWeekEndFromWeek', ipFromWeek, ipFromYear, opFromDate);
    }

    private clearPageControls(): void {
        this.setControlValue('NumberOfSeasons', '');
        this.setControlValue('SeasonalBranchUpdate', false);
        this.setControlValue('SeasonalServiceInd', false);
        this.setControlValue('SeasonalTemplateNumber', '');
        this.setControlValue('ServiceCommenceDate', '');
        this.setControlValue('ServiceCoverRowID', '');
        this.setControlValue('ServiceCoverNumber', '');
        this.setControlValue('ServiceQuantity', '');
        this.setControlValue('ServiceVisitAnnivDate', '');
        this.setControlValue('ServiceVisitFrequency', '');
        this.setControlValue('Status', '');
        this.setControlValue('TemplateName', '');

        for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
            this.setControlValue('SeasonNumber' + i, '');
            this.setControlValue('SeasonalFromDate' + i, '');
            this.setControlValue('SeasonalFromWeek' + i, '');
            this.setControlValue('SeasonalFromYear' + i, '');
            this.setControlValue('SeasonalToDate' + i, '');
            this.setControlValue('SeasonalToWeek' + i, '');
            this.setControlValue('SeasonalToYear' + i, '');
            this.setControlValue('SeasonNoOfVisits' + i, '');
        }
    }

    private saveRecord(): void {
        let formData: Object = {};
        let search = this.getURLSearchParamObject();

        formData['ServiceCoverROWID'] = this.getControlValue('ServiceCoverRowID');
        formData[this.serviceConstants.ContractNumber] = this.getControlValue('ContractNumber');
        formData[this.serviceConstants.PremiseNumber] = this.getControlValue('PremiseNumber');
        formData[this.serviceConstants.ProductCode] = this.getControlValue('ProductCode');
        formData['ServiceCoverNumber'] = this.getControlValue('ServiceCoverNumber');
        formData['ServiceVisitFrequency'] = this.getControlValue('ServiceVisitFrequency');
        formData['ServiceQuantity'] = this.getControlValue('ServiceQuantity');
        formData['ServiceCommenceDate'] = this.getControlValue('ServiceCommenceDate');
        formData['ServiceVisitAnnivDate'] = this.getControlValue('ServiceVisitAnnivDate');
        formData['SeasonalServiceInd'] = this.getControlValue('SeasonalServiceInd') ? 'yes' : 'no';
        formData['LastChangeEffectDate'] = this.getControlValue('LastChangeEffectDate');
        formData['NumberOfSeasons'] = this.getControlValue('NumberOfSeasons');
        formData['SeasonalTemplateNumber'] = this.getControlValue('SeasonalTemplateNumber');
        formData['SeasonalBranchUpdate'] = this.getControlValue('SeasonalBranchUpdate') ? 'yes' : 'no';
        formData[this.serviceConstants.Function] = 'GetSeasonalTemplateDetails,GetSeasonalTemplateDates';

        for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
            formData['SeasonNumber' + i] = this.getControlValue('SeasonNumber' + i);
            formData['SeasonalFromDate' + i] = this.getControlValue('SeasonalFromDate' + i);
            formData['SeasonalFromWeek' + i] = this.getControlValue('SeasonalFromWeek' + i);
            formData['SeasonalFromYear' + i] = this.getControlValue('SeasonalFromYear' + i);
            formData['SeasonalToDate' + i] = this.getControlValue('SeasonalToDate' + i);
            formData['SeasonalToWeek' + i] = this.getControlValue('SeasonalToWeek' + i);
            formData['SeasonalToYear' + i] = this.getControlValue('SeasonalToYear' + i);
            formData['SeasonNoOfVisits' + i] = this.getControlValue('SeasonNoOfVisits' + i);
        }

        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, search, formData)
            .subscribe((data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.fetchRecord();
                    this.formPristine();
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onEllipsisDataReceived(type: string, data: any): void {
        switch (type) {
            case 'ContractNumber':
                this.isAutoOpenContractSearch = false;
                this.setControlValue('ContractNumber', data.ContractNumber || '');
                this.setControlValue('ContractName', data.ContractName || '');
                this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractNumber = data.ContractNumber || '';
                this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractName = data.ContractName || '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.ContractNumber = data.ContractNumber || '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.ContractName = data.ContractName || '';

                this.setControlValue('PremiseNumber', '');
                this.setControlValue('PremiseName', '');
                this.setControlValue('ProductCode', '');
                this.setControlValue('ProductDesc', '');
                this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseNumber = '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseName = '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = '';

                this.clearPageControls();
                this.enableDisablePageControls(false);
                this.formPristine();
                break;
            case 'PremiseNumber':
                this.setControlValue('PremiseNumber', data.PremiseNumber || '');
                this.setControlValue('PremiseName', data.PremiseName || '');
                this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseNumber = data.PremiseNumber || '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseName = data.PremiseName || '';

                this.setControlValue('ProductCode', '');
                this.setControlValue('ProductDesc', '');
                this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = '';

                this.setControlValue('ProductDesc', '');

                this.clearPageControls();
                this.enableDisablePageControls(false);
                this.formPristine();
                break;
            case 'ProductCode':
                this.isAutoOpenProductSearch = false;
                this.clearPageControls();
                this.setControlValue('ProductCode', data.ProductCode || '');
                this.setControlValue('ProductDesc', data.ProductDesc || '');
                if (data.row && data.row.ttServiceCover) {
                    this.setControlValue('ServiceCoverRowID', data.row.ttServiceCover || '');
                }
                this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = data.ProductCode || '';
                this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = data.ProductDesc || '';

                this.enableDisablePageControls(false);
                this.formPristine();

                if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber') && this.getControlValue('ProductCode')) {
                    this.fetchRecord();
                }
                break;
            case 'SeasonalTemplate':
                this.riExchange.riInputElement.MarkAsDirty(this.uiForm, 'SeasonalTemplateNumber');
                this.setControlValue('SeasonalTemplateNumber', data.TemplateNumber || '');
                this.setControlValue('TemplateName', data.TemplateName || '');
                this.onSeasonalTemplateNumberChange();
                break;
            default:
        }
    }

    public onContractNumberChange(event: Event): void {
        this.setControlValue('ContractName', '');
        this.setControlValue('PremiseNumber', '');
        this.setControlValue('PremiseName', '');
        this.setControlValue('ProductCode', '');
        this.setControlValue('ProductDesc', '');

        this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractNumber = this.pageParams.ellipsisParams.productCode.childConfigParams.ContractNumber = this.getControlValue('ContractNumber');
        this.pageParams.ellipsisParams.premiseNumber.childConfigParams.ContractName = this.pageParams.ellipsisParams.productCode.childConfigParams.ContractName = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseNumber = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseName = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = '';

        if (this.getControlValue('ContractNumber')) {
            this.getLookupData('C');
        }

        this.clearPageControls();
        this.enableDisablePageControls(false);
        this.formPristine();
    }

    public onPremiseNumberChange(event: Event): void {
        this.setControlValue('PremiseName', '');
        this.setControlValue('ProductCode', '');
        this.setControlValue('ProductDesc', '');

        this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseNumber = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.PremiseName = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = '';

        if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber')) {
            this.getLookupData('P');
        }

        this.clearPageControls();
        this.enableDisablePageControls(false);
        this.formPristine();
    }

    public onProductCodeChange(event: Event): void {
        this.setControlValue('ProductDesc', '');
        this.pageParams.ellipsisParams.productCode.childConfigParams.ProductDesc = '';
        this.pageParams.ellipsisParams.productCode.childConfigParams.ProductCode = this.getControlValue('ProductCode');

        if (this.getControlValue('ProductCode')) {
            this.getLookupData('S');
        }

        this.clearPageControls();
        this.enableDisablePageControls(false);

        if (this.getControlValue('ContractNumber') && this.getControlValue('PremiseNumber') && this.getControlValue('ProductCode')) {
            this.fetchRecord();
        }
        this.formPristine();
    }

    public onNumberOfSeasonsChange(): void {
        this.showSeasonRows();
    }

    public onSeasonalTemplateNumberChange(): void {
        if (this.getControlValue('SeasonalTemplateNumber')) {
            this.onSeasonalBranchUpdateChange();
            this.disableControl('SeasonalBranchUpdate', false);
            this.disableControl('NumberOfSeasons', true);
            this.getTemplateSeasons();
            this.onNumberOfSeasonsChange();

            // Enable Season inputs
            for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
                this.enableSeasonRow(i, false, i > this.getControlValue('NumberOfSeasons'));
            }
        } else {
            this.disableControl('SeasonalBranchUpdate', true);
            this.disableControl('NumberOfSeasons', false);
            this.setControlValue('TemplateName', '');
            this.setControlValue('SeasonalBranchUpdate', false);

            // Enable Season inputs
            for (let i: number = 1; i <= this.pageParams.seasonalMaxNumberSeasons; i++) {
                this.enableSeasonRow(i, true, false);
            }
        }
    }

    public onSeasonalBranchUpdateChange(): void {
        if (!this.riExchange.riInputElement.isDisabled(this.uiForm, 'SeasonalBranchUpdate')) {
            this.enableDisableSeasonInputs();
            if (this.getControlValue('SeasonalBranchUpdate')) { this.getTemplateSeasons(); }
        }
    }

    public onSeasonalServiceIndChange(): void {
        this.pageParams.isCurrentSeasonalInd = this.getControlValue('SeasonalServiceInd');
        if (!this.riExchange.riInputElement.isDisabled(this.uiForm, 'SeasonalServiceInd')) {
            this.enableDisableSeasonInputs();

            // If setting this to seasonal and number of seasons is blank, set number to 1
            if (this.getControlValue('SeasonalServiceInd') && !this.pageParams.isExistingSeasonalInd && this.getControlValue('NumberOfSeasons') === '') {
                this.setControlValue('NumberOfSeasons', '1');
                this.showSeasonRows();
            }
        }
    }

    public onLastChangeEffectDateChange(): void {
        if (this.pageParams.isExistingSeasonalInd) {
            this.getServiceSeasons();
        } else if (this.getControlValue('SeasonalServiceInd') && this.getControlValue('SeasonalTemplateNumber')) {
            this.getTemplateDates();
        }
    }

    public onSeasonalFromDateChange(seasonNumber: number): void {
        this.seasonalDateChange('SeasonalFromDate' + seasonNumber, 'SeasonalFromWeek' + seasonNumber, 'SeasonalFromYear' + seasonNumber);
    }

    public onSeasonalFromWeekChange(seasonNumber: number): void {
        this.seasonalFromWeekChange('SeasonalFromWeek' + seasonNumber, 'SeasonalFromYear' + seasonNumber, 'SeasonalFromDate' + seasonNumber);
    }

    public onSeasonalFromYearChange(seasonNumber: number): void {
        this.seasonalFromWeekChange('SeasonalFromWeek' + seasonNumber, 'SeasonalFromYear' + seasonNumber, 'SeasonalFromDate' + seasonNumber);
    }

    public onSeasonalToDateChange(seasonNumber: number): void {
        this.seasonalDateChange('SeasonalToDate' + seasonNumber, 'SeasonalToWeek' + seasonNumber, 'SeasonalToYear' + seasonNumber);
    }

    public onSeasonalToWeekChange(seasonNumber: number): void {
        this.seasonalToWeekChange('SeasonalToWeek' + seasonNumber, 'SeasonalToYear' + seasonNumber, 'SeasonalToDate' + seasonNumber);
    }

    public onSeasonalToYearChange(seasonNumber: number): void {
        this.seasonalToWeekChange('SeasonalToWeek' + seasonNumber, 'SeasonalToYear' + seasonNumber, 'SeasonalToDate' + seasonNumber);
    }

    public onMenuChange(selectedValue: string): void {
        this.setControlValue('Menu', 'Options');
        this.riExchange.riInputElement.MarkAsPristine(this.uiForm, 'Menu');

        if (this.getControlValue('ServiceCoverRowID')) {
            switch (selectedValue) {
                case 'PlanVisit':
                    this.navigate('ServiceCover', InternalGridSearchSalesModuleRoutes.ICABSAPLANVISITGRIDYEAR,
                        {
                            CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                            ContractNumber: this.getControlValue('ContractNumber'),
                            PremiseNumber: this.getControlValue('PremiseNumber'),
                            ProductCode: this.getControlValue('ProductCode'),
                            ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
                        });
                    break;
                case 'StateOfService':
                    // this.navigate('SOS', Service/iCABSSeStateOfServiceNatAccountGrid.htm);
                    this.modalAdvService.emitMessage(new ICabsModalVO('iCABSSeStateOfServiceNatAccountGrid.htm - This screen is not yet developed!'));
                    break;
                case 'ServiceSeasons':
                    this.navigate('ServiceCover', InternalGridSearchApplicationModuleRoutes.ICABSASERVICECOVERSEASONGRID,
                        {
                            CurrentContractTypeURLParameter: this.pageParams.currentContractTypeURLParameter,
                            ContractNumber: this.getControlValue('ContractNumber'),
                            PremiseNumber: this.getControlValue('PremiseNumber'),
                            ProductCode: this.getControlValue('ProductCode'),
                            ServiceCoverRowID: this.getControlValue('ServiceCoverRowID')
                        });
                    break;
            }
        } else {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.NoRecordSelected));
        }
    }

    public onCancelClick(): void {
        this.clearPageControls();
        this.fetchRecord();
        this.formPristine();
    }

    // Form submit event
    public onSubmit(event: any): void {
        this.utils.highlightTabs();
        if (this.riExchange.validateForm(this.uiForm)) {
            this.riTab.TabsToNormal();
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null, this.saveRecord.bind(this)));
        }
    }

    // Generic function
    public focusSave(obj: any): void {
        this.riTab.focusNextTab(obj);
    }
}
