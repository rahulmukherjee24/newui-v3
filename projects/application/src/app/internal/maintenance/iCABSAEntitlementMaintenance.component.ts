import { Component, Injector, OnInit, ViewChild } from '@angular/core';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { ICabsModalVO } from '../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { PageIdentifier } from '../../base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';

@Component({
    templateUrl: 'iCABSAEntitlementMaintenance.html',
    styles: [`
        .marked-in-red{border: 1px solid red;}
    `]
})

export class EntitlementMaintenanceComponent extends BaseComponent implements OnInit {
    @ViewChild('successModal') public successModal;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private masterServiceCoverDetailsElement;
    private static readonly ENTITLEMENTINVOICETYPECODECHECK: string = 'e';
    private queryParams: Object = {
        operation: 'Application/iCABSAEntitlementMaintenance',
        module: 'service-cover',
        method: 'contract-management/maintenance'
    };
    private shouldCall: boolean = false;
    private isMasterServiceCover: boolean = false;
    private updateApiPayload: Object = {};

    public pageId: string = '';
    public pageTitle: string = 'Service Cover Entitlement Maintenance';
    public controls: Array<any> = [
        { name: 'BtnDefaultValue', type: '', value: '', ignoreSubmit: true },
        { name: 'ContractName', disabled: true, type: '', value: '', ignoreSubmit: true },
        { name: 'ContractNumber', disabled: true, type: '', value: '', ignoreSubmit: true },
        { name: 'EntitlementAnnivDate', value: '', required: true, type: MntConst.eTypeDate },
        { name: 'EntitlementAnnualQuantity', value: '', required: true },
        { name: 'EntitlementNextAnnualQuantity', value: '', required: true },
        { name: 'EntitlementOrderedQuantity', value: '' },
        { name: 'EntitlementPricePerUnit', disabled: true, required: true, type: MntConst.eTypeCurrency },
        { name: 'EntitlementServiceQuantity', value: '' },
        { name: 'EntitlementYTDQuantity', disabled: true, required: false },
        { name: 'MasterServiceCoverQty', type: '', value: '', ignoreSubmit: true },
        { name: 'MinCommitQty', disabled: false, type: MntConst.eTypeCheckBox, required: false, value: '' },
        { name: 'PremiseName', disabled: true, type: '', value: '', ignoreSubmit: true },
        { name: 'PremiseNumber', disabled: true, type: '', value: '', ignoreSubmit: true },
        { name: 'ProductCode', disabled: true, type: '', value: '', ignoreSubmit: true },
        { name: 'ProductDesc', disabled: true, type: '', value: '', ignoreSubmit: true },
        { name: 'ServiceCoverRowID', value: '' },
        { name: 'UnitDesc', disabled: true },

        { name: 'EntitlementApprovalReqdInd', disabled: true, type: MntConst.eTypeCheckBox, ignoreSubmit: false },
        { name: 'EntitlementCalculationLevel', disabled: true, ignoreSubmit: false, required: true, value: '' },
        { name: 'EntitlementCalculationPeriod', disabled: true, ignoreSubmit: false, required: true },
        { name: 'EntitlementInvoiceFrequencyCode', disabled: true, ignoreSubmit: false, required: true },
        { name: 'EntitlementPRCInd', disabled: true, type: MntConst.eTypeCheckBox, ignoreSubmit: false },
        { name: 'LastCalculationDate', disabled: true, required: true, value: '', ignoreSubmit: false, type: MntConst.eTypeDate },
        { name: 'MasterPRCUnitDesc', disabled: true, ignoreSubmit: true },
        { name: 'MasterServiceCoverDetailsText', disabled: true, ignoreSubmit: true, required: false },
        { name: 'PeriodTDQuantity', disabled: true, value: '', ignoreSubmit: false, required: true }
    ];
    public ContractName: any;
    public ContractNumber: any;
    public entitlmentBusinessRegistryData: Array<any> = [
        {
            'EntitlementCalculationLevelOptions': []
        },
        {
            'EntitlementCalculationPeriodOptions': []
        },
        {
            'EntitlementInvoiceFrequencyCodeOptions': []
        }
    ];
    public isMarkedInRed: boolean = false;
    public PremiseName: any;
    public PremiseNumber: any;
    public ProductCode: any;
    public ProductDesc: any;
    public extendedEntitlementFields: boolean = false;
    public showPricePerUnit: boolean = false;

    constructor(
        injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSAENTITLEMENTMAINTENENCE;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageParams.spanEntitlementAnnivDateLab_innerText = 'Entitlement Anniversary Date';
        this.pageParams.spanEntitlementAnnualQuantityLab_innerText = 'Annual Entitlement Quantity';
        this.pageParams.spanEntitlementNextAnnualQuantityLab_innerText = 'Next Year\'s Entitlement Quantity';
        this.getregistryValues();
        this.fetchEntitlementPageData();
    }

    private fetchEntitlementPageData(): void {
        this.riExchange.getParentHTMLValue('ContractName');
        this.riExchange.getParentHTMLValue('ContractNumber');
        this.riExchange.getParentHTMLValue('PremiseName');
        this.riExchange.getParentHTMLValue('PremiseNumber');
        this.riExchange.getParentHTMLValue('ProductCode');
        this.riExchange.getParentHTMLValue('ProductDesc');

        let searchParams: QueryParams = new QueryParams();
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '0');
        searchParams.set('ProductCode', this.getControlValue('ProductCode'));
        searchParams.set('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        searchParams.set('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        searchParams.set('ServiceCoverNumber', this.riExchange.getParentHTMLValue('ServiceCoverNumber'));

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams)
            .subscribe(
                (data) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    } else {
                        for (let item in data) {
                            if (item) {
                                this.updateApiPayload[item] = data[item];
                                this.updateApiPayload['ServiceCoverNumber'] = this.riExchange.getParentHTMLValue('ServiceCoverNumber');
                            }
                        }
                        this.setFieldControlValues(data);
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    private getregistryValues(): void {
        let lookupIP: Array<any> = [
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Entitlement',
                    'RegKey': 'ValidCalculationLevel'
                },
                'fields': [
                    'RegValue'
                ]
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Entitlement',
                    'RegKey': 'ValidCalculationPeriod'
                },
                'fields': [
                    'RegValue'
                ]
            },
            {
                'table': 'BusinessRegistry',
                'query': {
                    'BusinessCode': this.businessCode(),
                    'RegSection': 'Entitlement',
                    'RegKey': 'ValidInvoiceFrequency'
                },
                'fields': ['RegValue']
            }
        ];
        this.LookUp.lookUpRecord(lookupIP).subscribe((data) => {
            if (data) {
                data.map((item, k) => {
                    this.parseBusinessRegistryData(item, this.entitlmentBusinessRegistryData[k]);
                });
            }
        },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private parseBusinessRegistryData(data: any, obj: Object): void {
        let key: string = Object.keys(obj).toString();
        let dataValue: any = data[0].RegValue.split('|');
        let title: any = dataValue[1].split(',');
        dataValue[0].split(',').map((item, k) => {
            obj[key].push({
                value: item,
                title: title[k]
            });
        });
    }

    private setDateToFields(fieldName: string, value: string): void {
        if (!(fieldName && (value || value.length))) {
            this.riExchange.riInputElement.SetMarkedAsTouched(this['uiForm'], fieldName, true);
            return;
        }
        let tempDateString = this.globalize.parseDateToFixedFormat(value).toString();
        this.pageParams['dt' + fieldName] = new Date(tempDateString);
        this.setControlValue(fieldName, tempDateString);
        this.updateApiPayload[fieldName] = tempDateString;
    }

    private setSpannerFieldControlState(data: any): void {
        if (data['EntitlementInvoiceTypeCode'].toLowerCase() === EntitlementMaintenanceComponent.ENTITLEMENTINVOICETYPECODECHECK) {
            this.extendedEntitlementFields = true;

            this.disableControl('EntitlementCalculationLevel', false);
            this.setControlValue('EntitlementCalculationLevel', data.EntitlementCalculationLevel);

            this.disableControl('EntitlementCalculationPeriod', false);
            this.setControlValue('EntitlementCalculationPeriod', data.EntitlementCalculationPeriod);

            this.setControlValue('EntitlementInvoiceFrequencyCode', data.EntitlementInvoiceFrequencyCode);
            this.disableControl('EntitlementInvoiceFrequencyCode', false);

            this.setControlValue('EntitlementNextAnnualQuantity', data.EntitlementNextAnnualQuantity);

            this.disableControl('PeriodTDQuantity', false);
            this.setControlValue('PeriodTDQuantity', data.PeriodTDQuantity);

            this.setDateToFields('LastCalculationDate', data.LastCalculationDate);

            this.disableControl('EntitlementPRCInd', false);
            this.disableControl('MasterServiceCoverDetailsText', false);
            this.setControlValue('EntitlementApprovalReqdInd', data.EntitlementApprovalReqdInd);
            this.setControlValue('EntitlementApprovalReqdInd', data.EntitlementApprovalReqdInd === 'true' ? true : false);

            /*
                checking for true or blank value from API response
            */
            if (data['EntitlementPRCInd'] === 'true') {
                this.isMasterServiceCover = true;
                this.setControlValue('EntitlementPRCInd', true);
                this.disableControl('EntitlementApprovalReqdInd', false);
                this.disableControl('LastCalculationDate', false);
                this.disableControl('EntitlementPricePerUnit', false);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'MasterServiceCoverDetailsText', '');
                this.setControlValue('MasterServiceCoverDetailsText', '');

            } else {
                this.isMasterServiceCover = false;
                this.setControlValue('EntitlementPRCInd', false);
                this.disableControl('EntitlementApprovalReqdInd', true);
                this.disableControl('LastCalculationDate', true);
                this.disableControl('EntitlementPricePerUnit', true);
                this.riExchange.riInputElement.SetValue(this.uiForm, 'MasterServiceCoverDetailsText', data.MasterServiceCoverDetailsText);
                this.setControlValue('MasterServiceCoverDetailsText', data.MasterServiceCoverDetailsText);
                this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'MasterServiceCoverDetailsText', false);
                this.isMarkedInRed = false;
            }
            /*
             as per tech document on page 16 : Show the text box in red if MasterServiceCoverCurrentInd is FALSE.
            */
            if (data['MasterServiceCoverCurrentInd'] && data['MasterServiceCoverCurrentInd'] === 'false') {
                this.isMarkedInRed = true;
                this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'MasterServiceCoverDetailsText', true);
            }

        } else { // if other then 'e' hide them
            this.extendedEntitlementFields = false;
        }

        /*
        Acceptence Criteria
         EntitlementPricePerUnit visible only if EntitlementInvoiceTypeCode !== e
         EntitlementPricePerUnit updateable only if EntitlementInvoiceTypeCode !== e  or EntitlementPRCInd == true
        */
        if (data['EntitlementInvoiceTypeCode'].toLowerCase() !== EntitlementMaintenanceComponent.ENTITLEMENTINVOICETYPECODECHECK || data['EntitlementPRCInd'] === true) {
            this.disableControl('EntitlementPricePerUnit', false); // updatable
        }
    }

    private setFieldControlValues(data: any): void {
        if (data) {
            this.setDateToFields('EntitlementAnnivDate', data.EntitlementAnnivDate ? data.EntitlementAnnivDate : this.riExchange.getParentHTMLValue('EntitlementAnnivDate'));
            this.setControlValue('EntitlementAnnualQuantity', data.EntitlementAnnualQuantity);
            this.setControlValue('EntitlementNextAnnualQuantity', data.EntitlementNextAnnualQuantity);
            this.setControlValue('EntitlementPricePerUnit', data.EntitlementPricePerUnit);
            this.setControlValue('EntitlementServiceQuantity', data.EntitlementServiceQuantity);
            this.setControlValue('EntitlementYTDQuantity', data.EntitlementYTDQuantity);
            this.setControlValue('UnitDesc', data.UnitDesc);

            // set spanner fields state
            this.setSpannerFieldControlState(data);
        }
    }

    public dateToSelectedValue(val: any, field: string): void {
        if (!field || val.valid === 'invalid') {
            return;
        }
        if ((val && val.value)) {
            let tempDateString = this.globalize.parseDateToFixedFormat(val.value).toString();
            this.setControlValue(field, tempDateString);
            this.updateApiPayload[field] = tempDateString;

        } else if (!val.value) {
            let tempDateString = this.globalize.parseDateToFixedFormat(val).toString();
            this.setControlValue(field, tempDateString);
            this.updateApiPayload[field] = tempDateString;
        }
    }

    private setSearch(): QueryParams {
        let search: QueryParams;

        search = this.getURLSearchParamObject();
        search.set('action', '6');
        return search;
    }

    public handleCancel(): void {
        this.location.back();
    }

    public btnDefaultServiceQuantity_onClick(): void {
        let operation: string = 'Application/iCABSAServiceCoverMaintenance';
        let form_data: Object = {
            'action': '6',
            'BusinessCode': this.utils.getBusinessCode(),
            'EntitlementAnnualQuantity': this.getControlValue('EntitlementAnnualQuantity'),
            'Function': 'GetServiceVisitQuantity',
            'ServiceVisitFrequency': this.riExchange.getParentHTMLValue('ServiceVisitFrequency')
        };

        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
            operation, this.setSearch(), form_data).subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, response.fullError));
                } else {
                    if ((response['ErrorMessage'] && response['ErrorMessage'].length) || response.fullError) {
                        let errorMessage = response['ErrorMessage'] || response.fullError;
                        this.setControlValue('EntitlementAnnualQuantity', 0);
                        this.setControlValue('EntitlementNextAnnualQuantity', 0);
                        this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, errorMessage));
                    } else {
                        this.riExchange.riInputElement.SetValue(this.uiForm, 'EntitlementServiceQuantity', response.EntitlementServiceVisitQuantity);
                        this.setControlValue('EntitlementServiceQuantity', response.EntitlementServiceVisitQuantity);
                        this.updateFormFieldOnChange(response.EntitlementServiceVisitQuantity, 'EntitlementServiceQuantity');
                    }
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    public EntitlementAnnualQuantity_OnChange(val: any, field: any): void {
        this.updateFormFieldOnChange(val, field);
        this.riExchange.riInputElement.SetValue(this.uiForm, 'EntitlementNextAnnualQuantity', this.riExchange.riInputElement.GetValue(this.uiForm, 'EntitlementAnnualQuantity'));
        this.updateFormFieldOnChange(val, 'EntitlementNextAnnualQuantity');

        let form_data: Object = {
            'ContractNumber': this.riExchange.getParentHTMLValue('ContractNumber'),
            'ContractPeriodTrialInd': '',
            'EntitlementAnnualQuantity': this.getControlValue('EntitlementAnnualQuantity'),
            'EntitlementNextAnnualQuantity': this.getControlValue('EntitlementNextAnnualQuantity'),
            'Function': 'WarnEntitlementQuantity',
            'PremiseNumber': this.riExchange.getParentHTMLValue('PremiseNumber')
        };
        let operation: string = 'Application/iCABSAServiceCoverMaintenance';
        let queryParams: QueryParams = new QueryParams();

        queryParams.set(this.serviceConstants.Action, '6');
        queryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.ajaxSource.next(this.ajaxconstant.START);

        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
            operation, this.setSearch(), form_data).subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, response.fullError));
                } else {
                    if ((response['ErrorMessage'] && response['ErrorMessage'].length) || response.fullError) {
                        let errorMessage = response['ErrorMessage'] || response.fullError;
                        this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, errorMessage));
                    }
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'EntitlementAnnualQuantity', response.EntitlementAnnualQuantity);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'EntitlementNextAnnualQuantity', response.EntitlementNextAnnualQuantity);
                    this.setControlValue('EntitlementAnnualQuantity', response.EntitlementAnnualQuantity);
                    this.setControlValue('EntitlementNextAnnualQuantity', response.EntitlementNextAnnualQuantity);
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    public EntitlementNextAnnualQuantity_OnChange(val: any, field: any): void {
        this.updateFormFieldOnChange(val, field);

        let form_data: Object = {
            'ContractNumber': this.riExchange.getParentHTMLValue('ContractNumber'),
            'ContractPeriodTrialInd': '',
            'EntitlementAnnualQuantity': this.getControlValue('EntitlementAnnualQuantity'),
            'EntitlementNextAnnualQuantity': this.getControlValue('EntitlementNextAnnualQuantity'),
            'Function': 'WarnEntitlementQuantity',
            'PremiseNumber': this.riExchange.getParentHTMLValue('PremiseNumber')
        };
        let operation: string = 'Application/iCABSAServiceCoverMaintenance';
        let queryParams: QueryParams = new QueryParams();

        queryParams.set(this.serviceConstants.Action, '6');
        queryParams.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryParams.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
            operation, this.setSearch(), form_data).subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, response.fullError));
                } else {
                    if ((response['ErrorMessage'] && response['ErrorMessage'].length) || response.fullError) {
                        let errorMessage = response['ErrorMessage'] || response.fullError;
                        this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, errorMessage));
                    }
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'EntitlementAnnualQuantity', response.EntitlementAnnualQuantity);
                    this.riExchange.riInputElement.SetValue(this.uiForm, 'EntitlementNextAnnualQuantity', response.EntitlementNextAnnualQuantity);
                    this.setControlValue('EntitlementAnnualQuantity', response.EntitlementAnnualQuantity);
                    this.setControlValue('EntitlementNextAnnualQuantity', response.EntitlementNextAnnualQuantity);
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    public handleSaveClick(): void {
        this.updateApiPayload['action'] = '2';
        this.updateApiPayload['BusinessCode'] = this.utils.getBusinessCode();
        this.updateApiPayload['ContractNumber'] = this.riExchange.getParentHTMLValue('ContractNumber');
        this.updateApiPayload['CountryCode'] = this.utils.getCountryCode();
        this.updateApiPayload['PremiseNumber'] = this.riExchange.getParentHTMLValue('PremiseNumber');
        this.updateApiPayload['ProductCode'] = this.riExchange.getParentHTMLValue('ProductCode');
        this.minCommitQtyToggle();

        let searchPost: QueryParams;
        searchPost = this.getURLSearchParamObject();
        searchPost.set('BusinessCode', this.riExchange.getParentHTMLValue('BusinessCode'));
        searchPost.set('CountryCode', this.riExchange.getParentHTMLValue('CountryCode'));
        searchPost.set('ContractNumber', this.riExchange.getParentHTMLValue('ContractNumber'));
        searchPost.set('PremiseNumber', this.riExchange.getParentHTMLValue('PremiseNumber'));
        searchPost.set('ProductCode', this.riExchange.getParentHTMLValue('ProductCode'));
        searchPost.set('ServiceCoverNumber', this.riExchange.getParentHTMLValue('ServiceCoverNumber'));
        searchPost.set(this.serviceConstants.Action, '2');

        let invoice = Number(this.riExchange.riInputElement.GetValue(this.uiForm, 'EntitlementInvoiceFrequencyCode'));
        let freq = Number(this.riExchange.riInputElement.GetValue(this.uiForm, 'EntitlementCalculationPeriod'));

        if (this.riExchange.validateForm(this['uiForm'])) {
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
                this.queryParams['operation'], searchPost, this.updateApiPayload).subscribe((response) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (response.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, response.fullError));
                    } else {
                        this.formPristine();
                        this.showSuccessMessageDialog();
                    }
                },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    }
                );
        }
    }

    public minCommitQtyToggle(): void {
        if (this.riExchange.riInputElement.checked(this.uiForm, 'MinCommitQty')) {
            this.pageParams.spanEntitlementAnnivDateLab_innerText = 'Minimum Commitment Anniversary Date';
            this.pageParams.spanEntitlementAnnualQuantityLab_innerText = 'Minimum Commitment Quantity';
            this.pageParams.spanEntitlementNextAnnualQuantityLab_innerText = 'Next Year\'s Minimum Commitment Quantity';
            this.setControlValue('MinCommitQty', true);
            this.updateFormFieldOnChange(true, 'MinCommitQty');

        } else {
            this.pageParams.spanEntitlementAnnivDateLab_innerText = 'Entitlement Anniversary Date';
            this.pageParams.spanEntitlementAnnualQuantityLab_innerText = 'Annual Entitlement Quantity';
            this.pageParams.spanEntitlementNextAnnualQuantityLab_innerText = 'Next Year\'s Entitlement Quantity';
            this.setControlValue('MinCommitQty', false);
            this.updateFormFieldOnChange(false, 'MinCommitQty');
        }
    }

    public onEntitlementPricePerUnitChange(event: any, field: any): void {
        this.updateFormFieldOnChange(this.getControlValue('EntitlementPricePerUnit'), field);
        let form_data: Object = {
            'ContractNumber': this.riExchange.getParentHTMLValue('ContractNumber'),
            'ContractPeriodTrialInd': '',
            'EntitlementPricePerUnit': this.getControlValue('EntitlementPricePerUnit'),
            'Function': 'WarnEntitlementPrice',
            'PremiseNumber': this.riExchange.getParentHTMLValue('PremiseNumber')
        };

        let operation: string = this.queryParams['operation'];   //'Application/iCABSAEntitlementMaintenance';

        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
            operation, this.setSearch(), form_data).subscribe((response) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (response.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, response.fullError));
                } else {
                    if ((response['ErrorMessage'] && response['ErrorMessage'].length) || response.fullError) {
                        let errorMessage = response['ErrorMessage'] || response.fullError;
                        this.modalAdvService.emitError(new ICabsModalVO(response.errorMessage, errorMessage));
                    }
                }
            },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                }
            );
    }

    public onMasterServiceCoverChange(val: any): void {
        this.updateFormFieldOnChange(val ? true : false, 'EntitlementPRCInd');
        if (val) {
            this.disableControl('EntitlementPricePerUnit', false);
            this.disableControl('LastCalculationDate', false);
            this.disableControl('EntitlementApprovalReqdInd', false);
            this.isMarkedInRed = false;
            this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'MasterServiceCoverDetailsText', false);
            if (!this.isMasterServiceCover) {
                this.riExchange.riInputElement.SetValue(this.uiForm, 'MasterServiceCoverDetailsText', '');
            }

        } else {
            this.disableControl('EntitlementPricePerUnit', true);
            this.disableControl('LastCalculationDate', true);
            this.disableControl('EntitlementApprovalReqdInd', true);
            this.setControlValue('EntitlementApprovalReqdInd', false);

            /* If user blank this field and disabled it then restore previous value */
            if (this.riExchange.riInputElement.GetValue(this.uiForm, 'LastCalculationDate') === '') {
                this.riExchange.riInputElement.SetValue(this.uiForm, 'LastCalculationDate', this.updateApiPayload['LastCalculationDate']);
            }

            if (this.isMasterServiceCover) {
                this.isMarkedInRed = true;
                this.riExchange.riInputElement.SetMarkedAsTouched(this.uiForm, 'MasterServiceCoverDetailsText', true);
            } else {
                this.riExchange.riInputElement.SetValue(this.uiForm, 'MasterServiceCoverDetailsText', this.updateApiPayload['MasterServiceCoverDetailsText']);
            }
        }
    }

    public showSuccessMessageDialog(): void {
        this.shouldCall = true;
        this.successModal.show({ msg: MessageConstant.Message.RecordSavedSuccessfully, title: MessageConstant.Message.MessageTitle }, false);
    }

    public successModalClose(event: Event): void {
        if (this.shouldCall) {
            this.shouldCall = false;
            this.handleCancel();
        }
    }

    public updateFormFieldOnChange(val: any, field: any): void {
        this.updateApiPayload[field] = val;
    }
}
