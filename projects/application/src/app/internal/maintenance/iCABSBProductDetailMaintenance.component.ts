import { Component, AfterViewInit, Injector, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { QueryParams } from './../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../app/base/BaseComponent';
import { PageIdentifier } from './../../base/PageIdentifier';
import { MessageConstant } from '../../../shared/constants/message.constant';
import { MntConst } from '../../../shared/services/riMaintenancehelper';
import { ProductDetailSearchComponent } from '../search/iCABSBProductDetailSearch.component';
import { ICabsModalVO } from './../../../shared/components/modal-adv/modal-adv-vo';

@Component({
    templateUrl: 'iCABSBProductDetailMaintenance.html'
})

export class ProductDetailMaintenanceComponent extends BaseComponent implements AfterViewInit, OnInit, OnDestroy {
    @ViewChild('messageModal') public messageModal;
    @ViewChild('errorModal') public errorModal;
    @ViewChild('promptModalForSave') public promptModalForSave;
    @ViewChild('promptModalForDelete') public promptModalForDelete;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    public pageId: string = '';
    public queryPost: QueryParams = this.getURLSearchParamObject();
    public rowID: string = '';
    public recordID: string = '';
    public isSaveDisabled: boolean = true;
    public isCancelDisabled: boolean = true;
    public isDeleteDisabled: boolean = true;
    public isAutoOpen: boolean = false;
    public modalConfig: any = {
        backdrop: 'static',
        keyboard: true
    };

    public productDetailSearchComponent = ProductDetailSearchComponent;
    public inputParams = {
        parentMode: 'Search',
        isShowAddNew: true
    };
    public controls = [
        { name: 'ProductDetailCode', disabled: false, required: true, type: MntConst.eTypeCode },
        { name: 'ProductDetailDesc', disabled: true, required: true, type: MntConst.eTypeText },
        { name: 'InfestationGroupCode', disabled: true, required: true, type: MntConst.eTypeCode },
        { name: 'InfestationGroupDesc', disabled: true, required: false, type: MntConst.eTypeText },
        { name: 'PassToPDAInd', disabled: true, required: false },
        { name: 'SortOrder', disabled: true, required: false, type: MntConst.eTypeInteger }
    ];

    public muleConfig = {
        method: 'contract-management/admin',
        module: 'contract-admin',
        operation: 'Business/iCABSBProductDetailMaintenance'
    };
    public infestationGroupSearch: any = {
        isRequired: true,
        isDisabled: true,
        isTriggerValidate: false,
        params: {
            operation: 'Business/iCABSBInfestationGroupSearch',
            module: 'service',
            method: 'service-delivery/search'
        },
        displayFields: ['InfestationGroupCode', 'InfestationGroupDesc'],
        active: {
            id: '',
            text: ''
        }
    };

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBPRODUCTDETAILMAINTENANCE;
        this.browserTitle = this.pageTitle = 'Product Detail Maintenance';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public ngAfterViewInit(): void {
        this.setFormMode(this.c_s_MODE_SELECT);
        this.setControlValue('PassToPDAInd', false);
        this.isAutoOpen = true;
        this.disableFeilds({
            Save: true,
            Cancel: true,
            Delete: true
        });
        this.pageParams = {
            ProductDetailCode: '',
            ProductDetailDesc: '',
            InfestationGroupCode: '',
            InfestationGroupDesc: '',
            PassToPDAInd: '',
            SortOrder: ''
        };
    }

    public showErrorModal(data: any): void {
        this.errorModal.show({ msg: data.msg, title: 'Error' }, false);
    }

    public showMessageModal(data: any): void {
        this.messageModal.show({ msg: data.msg, title: 'Message' }, false);
    }

    // Product Detail Code/Description Search - Tabout
    public productDetailCodeTabout(): void {
        if (this.formMode === this.c_s_MODE_ADD) {
            return;
        }
        if (this.getControlValue('ProductDetailCode')) {
            this.setControlValue('ProductDetailCode', this.getControlValue('ProductDetailCode').toUpperCase());
            let queryPost: QueryParams = this.getURLSearchParamObject();
            queryPost.set(this.serviceConstants.Action, '0');
            queryPost.set(this.serviceConstants.BusinessCode, this.businessCode());
            queryPost.set('ProductDetailCode', this.getControlValue('ProductDetailCode'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makeGetRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, queryPost)
                .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.riExchange.riInputElement.markAsError(this.uiForm, 'ProductDetailCode');
                    } else {
                        this.isAutoOpen = false;
                        this.setFormMode(this.c_s_MODE_UPDATE);
                        this.setControlValue('ProductDetailDesc', data.ProductDetailDesc);
                        this.setControlValue('PassToPDAInd', data.PassToPDAInd);
                        this.setControlValue('SortOrder', data.SortOrder);
                        this.infestationGroupCodeTabout(data.InfestationGroupCode);
                        this.rowID = data.ttProductDetail;
                        this.recordID = data.RecordID;
                        this.disableFeilds({
                            Save: false,
                            Cancel: false,
                            Delete: false,
                            ProductDetailCode: true,
                            ProductDetailDesc: false,
                            PassToPDAInd: false,
                            SortOrder: false,
                            Infestation: false
                        });
                        this.setPageParams();
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
    }

    // Infestation Group Search - Tabout
    public infestationGroupCodeTabout(InfestationGroupCode: any): void {
        if (InfestationGroupCode) {
            this.queryPost.set(this.serviceConstants.Action, '0');
            let formdata: Object = {
                BusinessCode: this.businessCode(),
                InfestationGroupCode: InfestationGroupCode
            };
            this.ajaxSource.next(this.ajaxconstant.START);
            this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
                .subscribe(
                (data) => {
                    if (data.hasError) {
                        this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                        this.riExchange.riInputElement.markAsError(this.uiForm, 'InfestationGroupCode');
                    } else {
                        this.setControlValue('InfestationGroupDesc', data.InfestationGroupDesc);
                        this.infestationGroupSearch['active'] = {
                            id: InfestationGroupCode,
                            text: InfestationGroupCode + ' - ' + data.InfestationGroupDesc
                        };
                        this.pageParams.InfestationGroupDesc = data.InfestationGroupDesc;
                    }
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                },
                (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                });
        }
    }

    public setProductDetailCode(data: any): void {
        this.isAutoOpen = false;
        if (data.addMode) {
            this.setFormMode(this.c_s_MODE_ADD);
            this.disableFeilds({
                Save: false,
                Cancel: false,
                Delete: true,
                ProductDetailCode: false,
                ProductDetailDesc: false,
                PassToPDAInd: false,
                SortOrder: false,
                Infestation: false
            });
            this.clearControls([]);
            this.infestationGroupSearch['active'] = {
                id: '',
                text: ''
            };
            this.infestationGroupSearch['isRequired'] = false;
            this.infestationGroupSearch['isTriggerValidate'] = false;
        }
        else {
            this.setFormMode(this.c_s_MODE_UPDATE);
            this.disableFeilds({
                Save: false,
                Cancel: false,
                Delete: false,
                ProductDetailCode: true,
                ProductDetailDesc: false,
                PassToPDAInd: false,
                SortOrder: false,
                Infestation: false
            });
            this.infestationGroupSearch['isRequired'] = true;
            this.infestationGroupSearch['isTriggerValidate'] = true;
            this.setControlValue('ProductDetailCode', data.ProductDetailCode);
            this.setControlValue('ProductDetailDesc', data.ProductDetailDesc);
            this.setControlValue('InfestationGroupCode', data.InfestationGroupCode);
            this.setControlValue('PassToPDAInd', data.PassToPDAInd);
            this.setControlValue('SortOrder', data.SortOrder);
            this.infestationGroupCodeTabout(data.InfestationGroupCode);
            this.rowID = data.ttProductDetail;
            this.recordID = data.RecordID;
            this.riExchange.riInputElement.SetRequiredStatus(this.uiForm, 'InfestationGroupCode', true);
            this.riExchange.riInputElement.Enable(this.uiForm, 'InfestationGroupCode');
            this.setPageParams();
        }
    }

    public setPageParams(): void {
        this.pageParams.ProductDetailCode = this.getControlValue('ProductDetailCode');
        this.pageParams.ProductDetailDesc = this.getControlValue('ProductDetailDesc');
        this.pageParams.InfestationGroupCode = this.getControlValue('InfestationGroupCode');
        this.pageParams.PassToPDAInd = this.getControlValue('PassToPDAInd');
        this.pageParams.SortOrder = this.getControlValue('SortOrder');
    }

    // Add Product Detail
    public addProductDetail(mode: any): void {
        this.setFormMode(this.c_s_MODE_ADD);
        this.uiForm.reset();
    }

    // Promt for Saving Product Detail
    public promptForSaveProductDetail(): void {

        if (this.uiForm.valid) {
            this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.ConfirmRecord, null,
                this.saveProductDetail.bind(this)));
        } else {
            this.riExchange.riInputElement.isError(this.uiForm, 'ProductDetailCode');
            this.riExchange.riInputElement.isError(this.uiForm, 'ProductDetailDesc');
            this.riExchange.riInputElement.isError(this.uiForm, 'InfestationGroupCode');
            this.infestationGroupSearch['isRequired'] = true;
            this.infestationGroupSearch['isTriggerValidate'] = true;
        }
    }

    public saveProductDetail(): void {
        let formdata: any;
        if (this.formMode === this.c_s_MODE_ADD) {
            this.queryPost.set(this.serviceConstants.Action, '1');
            formdata = {
                BusinessCode: this.businessCode(),
                ProductDetailCode: this.getControlValue('ProductDetailCode'),
                ProductDetailDesc: this.getControlValue('ProductDetailDesc'),
                InfestationGroupCode: this.getControlValue('InfestationGroupCode'),
                PassToPDAInd: this.getControlValue('PassToPDAInd') ? 'true' : 'false',
                SortOrder: this.getControlValue('SortOrder')
            };
        } else {
            this.queryPost.set(this.serviceConstants.Action, '2');
            formdata = {
                BusinessCode: this.businessCode(),
                ProductDetailCode: this.getControlValue('ProductDetailCode'),
                ProductDetailDesc: this.getControlValue('ProductDetailDesc'),
                InfestationGroupCode: this.getControlValue('InfestationGroupCode'),
                PassToPDAInd: this.getControlValue('PassToPDAInd') ? 'true' : 'false',
                SortOrder: this.getControlValue('SortOrder'),
                ROWID: this.rowID
            };
        }
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
            (data) => {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordSavedSuccessfully));
                    this.rowID = data.ttProductDetail;
                    this.recordID = data.RecordID;
                    this.setFormMode(this.c_s_MODE_UPDATE);
                    this.setPageParams();
                    this.pageParams.InfestationGroupDesc = this.getControlValue('InfestationGroupDesc');
                    this.disableFeilds({
                        Save: false,
                        Cancel: false,
                        Delete: false,
                        ProductDetailCode: true,
                        ProductDetailDesc: false,
                        Infestation: false
                    });
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }

    public cancelProductDetail(): void {
        if (this.formMode !== this.c_s_MODE_SELECT)
            if (this.formMode === this.c_s_MODE_ADD) {
                this.setFormMode(this.c_s_MODE_SELECT);
                this.isAutoOpen = true;
                this.disableFeilds({
                    Save: true,
                    Cancel: true,
                    Delete: true,
                    ProductDetailCode: false,
                    ProductDetailDesc: true,
                    InfestationGroupCode: true,
                    InfestationGroupDesc: true,
                    PassToPDAInd: true,
                    SortOrder: true,
                    Infestation: true
                });
                this.uiForm.reset();
                this.infestationGroupSearch['active'] = {
                    id: '',
                    text: ''
                };
            } else {
                this.setFormMode(this.c_s_MODE_UPDATE);
                this.setControlValue('ProductDetailCode', this.pageParams.ProductDetailCode);
                this.setControlValue('ProductDetailDesc', this.pageParams.ProductDetailDesc);
                this.setControlValue('InfestationGroupCode', this.pageParams.InfestationGroupCode);
                this.setControlValue('InfestationGroupDesc', this.pageParams.InfestationGroupDesc);
                this.setControlValue('PassToPDAInd', this.pageParams.PassToPDAInd);
                this.setControlValue('SortOrder', this.pageParams.SortOrder);
                this.infestationGroupSearch['active'] = {
                    id: this.pageParams.InfestationGroupCode,
                    text: this.pageParams.InfestationGroupCode + ' - ' + this.pageParams.InfestationGroupDesc
                };
            }
        this.formPristine();
    }

    // Promt for Deleting Product Detail
    public promptForDeleteProductDetail(): void {
        this.modalAdvService.emitPrompt(new ICabsModalVO(MessageConstant.Message.DeleteRecord, null,
            this.deleteProductDetail.bind(this)));
    }

    public deleteProductDetail(): void {
        this.queryPost.set(this.serviceConstants.Action, '3');
        let formdata: any = {
            BusinessCode: this.businessCode(),
            ROWID: this.rowID
        };
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.muleConfig.method, this.muleConfig.module, this.muleConfig.operation, this.queryPost, formdata)
            .subscribe(
            (data) => {
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.RecordDeletedSuccessfully));
                    this.uiForm.reset();
                    this.disableFeilds({
                        Save: true,
                        Cancel: true,
                        Delete: true,
                        ProductDetailCode: false,
                        ProductDetailDesc: true,
                        InfestationGroupCode: true,
                        Infestation: true,
                        PassToPDAInd: true,
                        SortOrder: true
                    });
                }
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }


    public disableFeilds(fields: any): void {
        for (let field in fields) {
            if (fields.hasOwnProperty(field)) {
                let fieldBoolean = fields[field];
                switch (field) {
                    case 'Save':
                        this.isSaveDisabled = fieldBoolean;
                        break;
                    case 'Cancel':
                        this.isCancelDisabled = fieldBoolean;
                        break;
                    case 'Delete':
                        this.isDeleteDisabled = fieldBoolean;
                        break;
                    case 'Infestation':
                        this.infestationGroupSearch['isDisabled'] = fieldBoolean;
                        break;
                    default:
                        this.disableControl(field, fieldBoolean);
                        break;
                }
            }
        }
    }
    public onInfestationSearch(data: any): void {
        if (data.InfestationGroupCode) {
            this.setControlValue('InfestationGroupCode', data.InfestationGroupCode);
        } else {
            this.setControlValue('InfestationGroupCode', '');
        }
        this.setControlValue('InfestationGroupDesc', data.InfestationGroupDesc);
    }
}
