import { QueryParams } from './../../../../shared/services/http-params-wrapper';
import { Component, OnInit, Injector, ViewChild, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from './../../../base/PageIdentifier';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../../shared/constants/message.constant';


@Component({
    templateUrl: 'iCABSBBusinessRegistryGrid.html'
})

export class BusinessRegistryGridComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    private subscription: Subscription;
    private queryParams: any;
    private selectedModule: string = '';
    public pageId: string = '';
    public moduleValue: Array<any> = [];
    public controls = [
        { name: 'LatestInd', type: MntConst.eTypeCheckBox, disabled: false }
    ];

    ngOnInit(): void {
        super.ngOnInit();
        this.onWindowLoad();
    }

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSBBUSINESSREGISTRYGRID;
        this.browserTitle = 'Business Registry';
        this.pageTitle = 'Business Registry Maintenance';
        this.subscription = new Subscription();
        this.initiateQueryParams();

    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    // Initializes data
    private onWindowLoad(): void {
        this.loadModuleDropDownValue();
        this.buildGrid();
        if (!this.isReturning()) {
            this.pageParams.gridConfig = {
                pageSize: 10,
                currentPage: 1,
                totalRecords: 1,
                gridHandle: this.utils.randomSixDigitString()
            };
        }
        else {
            this.populateGrid();
        }

    }

    //This method used to initiate queryParams
    private initiateQueryParams(): void {
        this.queryParams = {
            save: {
                operation: 'Business/iCABSBBusinessRegistryGrid',
                module: 'configuration',
                method: 'it-functions/ri-model'
            },
            fetchOne: {
                operation: 'Business/iCABSBBusinessRegistryGrid',
                module: 'configuration',
                action: 0,
                method: 'it-functions/ri-model'
            },
            fetchTwo: {
                operation: 'Business/iCABSBBusinessRegistryGrid',
                module: 'configuration',
                action: 0,
                method: 'it-functions/ri-model'
            }
        };
    }

    // Builds the structure of the grid
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.AddColumn('RegSection', 'BusinessRegistryGrid', 'RegSection', MntConst.eTypeTextFree, 20, false, 'Module');
        this.riGrid.AddColumn('EffectiveDate', 'BusinessRegistryGrid', 'EffectiveDate', MntConst.eTypeDate, 10, false, 'Effective Date');
        this.riGrid.AddColumnTabSupport('EffectiveDate', true);
        this.riGrid.AddColumnOrderable('EffectiveDate', true, true);
        this.riGrid.AddColumn('RegKey', 'BusinessRegistryGrid', 'RegKey', MntConst.eTypeTextFree, 30, false, 'Key');
        this.riGrid.AddColumnTabSupport('RegKey', true);
        this.riGrid.AddColumnOrderable('RegKey', true, true);
        this.riGrid.SetColumnMaxLength('RegKey', 50);
        this.riGrid.AddColumn('RegValue', 'BusinessRegistryGrid', 'RegValue', MntConst.eTypeTextFree, 100, false, 'Value');
        this.riGrid.AddColumnTabSupport('RegValue', true);

        this.riGrid.AddColumnUpdateSupport('EffectiveDate', false);
        this.riGrid.AddColumnUpdateSupport('RegKey', false);
        this.riGrid.AddColumnUpdateSupport('RegValue', true);
        this.riGrid.SetColumnMaxLength('RegKey', 100);

        this.riGrid.Complete();
    }

    // Populate data into the grid
    private populateGrid(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        // set grid building parameters
        search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        search.set('riCacheRefresh', 'True');
        search.set(this.serviceConstants.GridMode, '0');
        search.set(this.serviceConstants.GridHandle, this.pageParams.gridConfig.gridHandle);
        search.set(this.serviceConstants.PageSize, this.pageParams.gridConfig.pageSize.toString());
        search.set(this.serviceConstants.PageCurrent, this.pageParams.gridConfig.currentPage.toString());
        let SortOrder = 'Descending';
        if (!this.riGrid.DescendingSort) {
            SortOrder = 'Ascending';
        }
        search.set('riSortOrder', this.riGrid.SortOrder);
        search.set('HeaderClickedColumn', this.riGrid.HeaderClickedColumn);
        search.set('Module', this.selectedModule);
        search.set('LatestInd', (this.getControlValue('LatestInd')) ? this.getControlValue('LatestInd') : false);
        search.set(this.serviceConstants.Action, '2');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makeGetRequest(this.queryParams.fetchOne.method, this.queryParams.fetchOne.module, this.queryParams.fetchOne.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.riGrid.RefreshRequired();
                    this.pageParams.gridConfig.currentPage = data.pageData ? data.pageData.pageNumber : 1;
                    this.pageParams.gridConfig.totalRecords = data.pageData ? data.pageData.lastPageNumber * this.pageParams.gridConfig.pageSize : 1;
                    this.riGrid.Execute(data);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }));
    }

    // Updates the pagelevel attributes on grid row activity
    private onGridFocus(): void {
        this.setAttribute('Row', this.riGrid.CurrentRow);
    }

    //This method used to fetch module details
    private loadModuleDropDownValue(): void {
        let search: QueryParams = this.getURLSearchParamObject();
        // set grid building parameters
        search.set('fn', 'BuildModuleCombo');
        search.set(this.serviceConstants.Action, '0');

        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makeGetRequest(this.queryParams.fetchTwo.method, this.queryParams.fetchTwo.module, this.queryParams.fetchTwo.operation, search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                } else {
                    this.getDropDownValue(data.ValidList);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }));
    }

    //This method used to prepare modules data
    private getDropDownValue(validList: string): void {
        if (validList) {
            let toArray: Array<any> = validList.split(',');
            let index: number = 0;
            if (toArray.length > 0) {
                toArray.forEach(item => {
                    if (index === 0) {
                        this.selectedModule = item.split('|')[0];
                    }
                    this.moduleValue.push({ 'value': item.split('|')[0], 'text': item.split('|')[1] });
                    index++;
                });
            }
        }
    }

    // Refresh the grid data on user click
    public onRiGridRefresh(event: any): void {
        if (this.pageParams.gridConfig.currentPage <= 0) {
            this.pageParams.gridConfig.currentPage = 1;
        }
        this.populateGrid();
    }

    //This method used to sort grid data
    public riGridSort(event: Object): void {
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    // Callback to retrieve the current page on user clicks
    public onGetCurrentPage(currentPage: any): void {
        this.pageParams.gridConfig.currentPage = currentPage.value;
        this.onRiGridRefresh(null);
    }

    //This method get execcuted on double click event fire of grid
    public onGridBodyDbClick(event: any): void {
        this.onGridFocus();
        //this.navigate('Update', Business/iCABSBBusinessRegistryMaint.htm);
        if (this.riGrid.CurrentColumnName === 'EffectiveDate') {
            this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
        }
    }

    //This method used to navigate user when Add button clicked
    public onAddBtnClicked(event: any): void {
        //this.navigate('Add', Business/iCABSBBusinessRegistryMaint.htm);
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.PageNotCovered));
    }

    //This method used to update Business regidtry information
    public onUpdateBusinessRegistry(event: any): void {
        let formData: any = {};
        formData['RegSection'] = this.riGrid.Details.GetValue('RegSection');
        formData['EffectiveDateRowID'] = this.riGrid.Details.GetAttribute('EffectiveDate', 'rowID');
        formData['EffectiveDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('EffectiveDate'));
        formData['RegKey'] = this.riGrid.Details.GetValue('RegKey');
        formData['RegValueRowID'] = this.riGrid.Details.GetAttribute('RegValue', 'RowID');
        formData['RegValue'] = this.riGrid.Details.GetValue('RegValue');
        formData['Module'] = this.selectedModule;
        formData['LatestInd'] = (this.getControlValue('LatestInd')) ? this.getControlValue('LatestInd') : false;
        formData[this.serviceConstants.GridMode] = '3';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridConfig.gridHandle;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;

        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.subscription.add(this.httpService.makePostRequest(this.queryParams.save.method, this.queryParams.save.module, this.queryParams.save.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.riGrid.Mode = MntConst.eModeNormal;
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            }));
    }

    //This method get executed when user change any module value from dropdown
    public onModuleSelectionChange(value: string): void {
        this.selectedModule = value;
    }
}
