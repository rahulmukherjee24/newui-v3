import { LocalStorageService } from 'ngx-webstorage';
import { HttpService } from './../../../../shared/services/http-service';
import { GlobalizeService } from './../../../../shared/services/globalize.service';
import { CBBService } from '@shared/services/cbb.service';
import { ServiceConstants } from '@shared/constants/service.constants';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { CBBConstants } from '@shared/constants/cbb.constants';

@Component({
    templateUrl: 'riAddUserAllServers.html'
})

export class AddUserComponent {
    public userForm: FormGroup;
    public msgs: Array<String> = [];

    constructor(
        private formBuilder: FormBuilder,
        private serviceConstants: ServiceConstants,
        private cbb: CBBService,
        private globalize: GlobalizeService,
        private http: HttpService,
        private ls: LocalStorageService
    ) {
        this.userForm = this.formBuilder.group({
            UserCode: [],
            UserName: [],
            Email: [],
            Password: [],
            LanguageCode: [{ value: 'ENG', disabled: true }]
        });
    }

    public onCreateUser(): void {
        this.msgs = [];
        if (!this.validate()) {
            return;
        }

        let access: any = this.cbb.getCBBList()[CBBConstants.c_s_COUNTRIES];

        access.forEach(cntItem => {
            let country: any = cntItem[CBBConstants.c_s_BUSINESSES];

            country.forEach((busItem, index) => {
                let business: any = busItem[CBBConstants.c_s_BRANCHES];
                let cCur: string = cntItem[CBBConstants.c_s_VALUE];
                let cBus: any = busItem[CBBConstants.c_s_VALUE];

                let queryParam: QueryParams = new QueryParams();

                queryParam.set(this.serviceConstants.BusinessCode, cBus);
                queryParam.set(this.serviceConstants.CountryCode, cCur);
                queryParam.set(this.serviceConstants.Action, '2');
                let userData = [{
                    'table': 'UserInformation',
                    'query': { 'UserCode': this.ls.retrieve('RIUserCode') },
                    'fields': ['UserCode', 'riDeveloper']
                }];

                this.http.lookUpPromise(queryParam, userData).then(data => {
                    let rec: any = data.results;
                    if (rec && rec[0] && rec[0][0] && rec[0][0].riDeveloper) {
                        let userQueryParam: QueryParams = new QueryParams();
                        let formData: any = {};

                        userQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                        userQueryParam.set(this.serviceConstants.CountryCode, cCur);
                        userQueryParam.set(this.serviceConstants.Action, '1');

                        formData['UserCode'] = this.userForm.controls['UserCode'].value;
                        formData['UserName'] = this.userForm.controls['UserName'].value;
                        formData['Email'] = this.userForm.controls['Email'].value;
                        formData['Password'] = this.userForm.controls['Password'].value;
                        formData['LanguageCode'] = this.userForm.controls['LanguageCode'].value;
                        formData['PhoneNumber'] = '.';
                        formData['FaxNumber'] = '.';
                        formData['PasswordlastChangedDate'] = this.globalize.parseDateToFixedFormat(new Date());
                        formData['PasswordRetries'] = 0;
                        formData['DateLastLoggedOn'] = this.globalize.parseDateToFixedFormat(new Date());
                        formData['LocalDocumentServer'] = '.';
                        formData['ReportServer'] = '.';
                        formData['ApprovalLevelCode'] = 0;
                        formData['UserTypeCode'] = '099';
                        formData['riDeveloper'] = true;

                        this.http.makePostRequest('it-functions/ri-model', 'user', 'Model/riMUserInformationMaintenance', userQueryParam, formData).subscribe(data => {
                            if (data.errorMessage) {
                                this.msgs.push('Error In User Entry For: ' + cntItem[CBBConstants.c_s_VALUE] + ' - ' + busItem[CBBConstants.c_s_VALUE] + ' - ' + data.errorMessage);
                            }
                            let authQueryParam: QueryParams = new QueryParams();
                            let formData: any = {};

                            authQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                            authQueryParam.set(this.serviceConstants.CountryCode, cCur);
                            authQueryParam.set(this.serviceConstants.Action, '1');

                            formData['Table'] = 'UserAuthority';
                            formData['UserCode'] = this.userForm.controls['UserCode'].value;
                            formData['DefaultBusinessInd'] = index === 0;
                            formData['FullAccessInd'] = true;
                            formData['AllowViewOfSensitiveInfoInd'] = true;
                            formData['AllowUpdateOfContractInfoInd'] = true;
                            formData['AllowExportOfInformationInd'] = true;
                            formData['ContactCreateSecurityLevel'] = 600;
                            formData['ClosureProcessInd'] = true;
                            formData['ContactTypeDetailAmendInd'] = true;
                            formData['CreateCallLogInCCMInd'] = true;

                            this.http.makePostRequest('it-functions/admin', 'user', 'Business/iCABSBUserAuthorityMaintenance', userQueryParam, formData).subscribe(data => {
                                if (data.errorMessage) {
                                    this.msgs.push('Error In User Authority For: ' + cCur + ' - ' + cBus + ' - ' + data.errorMessage);
                                }


                                let query: QueryParams = new QueryParams();
                                query.set(this.serviceConstants.BusinessCode, cBus);
                                query.set(this.serviceConstants.CountryCode, cCur);
                                query.set(this.serviceConstants.Action, '6');
                                query.set('AuthorityUserCode', this.userForm.controls['UserCode'].value);

                                this.http.makePostRequest('people/grid', 'user', 'Business/iCABSBUserAuthorityBranchGrid', query, { 'Function': 'SetBranchAuthorityGroup' }).subscribe(data => {
                                    if (data.errorMessage) {
                                        this.msgs.push('Cannot Create Branch Authority For : ' + cCur + ' - ' + cBus + ' - ' + data.errorMessage);
                                    } else {
                                        business.forEach((branch, index) => {
                                            let writeQueryParam: QueryParams = new QueryParams();
                                            let formData: any = {};

                                            writeQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                                            writeQueryParam.set(this.serviceConstants.CountryCode, cCur);
                                            writeQueryParam.set(this.serviceConstants.Action, '6');
                                            writeQueryParam.set('AuthorityUserCode', this.userForm.controls['UserCode'].value);
                                            writeQueryParam.set('BranchNumber', branch[CBBConstants.c_s_VALUE]);
                                            writeQueryParam.set('SetWriteAccess', 'Yes');

                                            formData['Function'] = 'SetWriteAccess';

                                            this.http.makePostRequest('people/grid', 'user', 'Business/iCABSBUserAuthorityBranchGrid', writeQueryParam, formData).subscribe(data => {
                                                if (data.errorMessage) {
                                                    this.msgs.push('Error In User Branch Write For: ' + cCur + ' - ' + cBus + ' - ' + data.errorMessage);
                                                } else {
                                                    if (index === 0) {
                                                        let defaultQueryParam: QueryParams = new QueryParams();
                                                        let formData: any = {};

                                                        defaultQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                                                        defaultQueryParam.set(this.serviceConstants.CountryCode, cCur);
                                                        defaultQueryParam.set(this.serviceConstants.Action, '6');
                                                        defaultQueryParam.set('AuthorityUserCode', this.userForm.controls['UserCode'].value);
                                                        defaultQueryParam.set('BranchNumber', branch[CBBConstants.c_s_VALUE]);
                                                        defaultQueryParam.set('SetDefaultBranch', 'Yes');
                                                        this.http.makePostRequest('people/grid', 'user', 'Business/iCABSBUserAuthorityBranchGrid', writeQueryParam, formData).subscribe(data => {
                                                            this.msgs.push('User Default Branch Set To: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE]);
                                                        }, error => {
                                                            this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE] + ' - ' + JSON.stringify(error));
                                                        });
                                                    }
                                                    this.msgs.push('Successfully Added For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE]);
                                                }
                                            }, error => {
                                                this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE] + ' - ' + JSON.stringify(error));
                                            });
                                        });
                                    }
                                }, error => {
                                    this.msgs.push('Cannot Create Branch Authority For : ' + cCur + ' - ' + cBus + ' - ' + JSON.stringify(error));
                                });
                            }, error => {
                                this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + JSON.stringify(error));
                            });

                        }, error => {
                            this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + JSON.stringify(error));
                        });
                    } else {
                        this.msgs.push('Error You Need riDeveloper Ticked To Perform This Action: ' + cCur + ' - ' + cBus);
                    }
                }, error => {
                    this.msgs.push('Error For Checking Access Level: ' + cCur + ' - ' + cBus + ' - ' + JSON.stringify(error));
                });
            });
        });
    }

    public addUser(): void {

    }

    public createUser(): void {
        this.msgs = [];
        if (!this.validate()) {
            return;
        }
        let access: any = this.cbb.getCBBList()[CBBConstants.c_s_COUNTRIES];

        access.forEach(cntItem => {
            let country: any = cntItem[CBBConstants.c_s_BUSINESSES];
            country.forEach(busItem => {
                let business: any = busItem[CBBConstants.c_s_BRANCHES];
                let cCur: string = cntItem[CBBConstants.c_s_VALUE];
                let cBus: any = busItem[CBBConstants.c_s_VALUE];

                let userQueryParam: QueryParams = new QueryParams();
                let formData: any = {};

                userQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                userQueryParam.set(this.serviceConstants.CountryCode, cCur);
                userQueryParam.set(this.serviceConstants.Action, '1');

                formData['UserCode'] = this.userForm.controls['UserCode'].value;
                formData['UserName'] = this.userForm.controls['UserName'].value;
                formData['Email'] = this.userForm.controls['Email'].value;
                formData['Password'] = this.userForm.controls['Password'].value;
                formData['LanguageCode'] = this.userForm.controls['LanguageCode'].value;
                formData['PhoneNumber'] = '.';
                formData['FaxNumber'] = '.';
                formData['PasswordlastChangedDate'] = this.globalize.parseDateToFixedFormat(new Date());
                formData['PasswordRetries'] = 0;
                formData['DateLastLoggedOn'] = this.globalize.parseDateToFixedFormat(new Date());
                formData['LocalDocumentServer'] = '.';
                formData['ReportServer'] = '.';
                formData['ApprovalLevelCode'] = 0;
                formData['UserTypeCode'] = '099';
                formData['riDeveloper'] = true;

                this.msgs.push('Inserting For: ' + cntItem[CBBConstants.c_s_VALUE] + ' - ' + busItem[CBBConstants.c_s_VALUE]);
                this.http.makePostRequest('it-functions/ri-model', 'user', 'Model/riMUserInformationMaintenance', userQueryParam, formData).subscribe(data => {
                    if (data.errorMessage) {
                        this.msgs.push('Error In User Entry For: ' + cntItem[CBBConstants.c_s_VALUE] + ' - ' + busItem[CBBConstants.c_s_VALUE] + ' - ' + data.errorMessage);
                    } else {
                        let authQueryParam: QueryParams = new QueryParams();
                        let formData: any = {};

                        authQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                        authQueryParam.set(this.serviceConstants.CountryCode, cCur);
                        authQueryParam.set(this.serviceConstants.Action, '1');

                        formData['Table'] = 'UserAuthority';
                        formData['UserCode'] = this.userForm.controls['UserCode'].value;
                        formData['DefaultBusinessInd'] = true;
                        formData['FullAccessInd'] = true;
                        formData['AllowViewOfSensitiveInfoInd'] = true;
                        formData['AllowUpdateOfContractInfoInd'] = true;
                        formData['AllowExportOfInformationInd'] = true;
                        formData['ContactCreateSecurityLevel'] = 600;
                        formData['ClosureProcessInd'] = true;
                        formData['ContactTypeDetailAmendInd'] = true;
                        formData['CreateCallLogInCCMInd'] = true;

                        this.http.makePostRequest('it-functions/admin', 'user', 'Business/iCABSBUserAuthorityMaintenance', userQueryParam, formData).subscribe(data => {
                            if (data.errorMessage) {
                                this.msgs.push('Error In User Authority For: ' + cCur + ' - ' + cBus + ' - ' + data.errorMessage);
                            } else {
                                business.forEach((branch, index) => {
                                    this.msgs.push('Will Insert For ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE]);

                                    let query: QueryParams = new QueryParams();
                                    query.set(this.serviceConstants.BusinessCode, cBus);
                                    query.set(this.serviceConstants.CountryCode, cCur);
                                    query.set(this.serviceConstants.Action, '6');
                                    query.set('AuthorityUserCode', this.userForm.controls['UserCode'].value);

                                    this.http.makePostRequest('people/grid', 'user', 'Business/iCABSBUserAuthorityBranchGrid', query, { 'Function': 'SetBranchAuthorityGroup' }).subscribe(data => {
                                        if (data.errorMessage) {
                                            this.msgs.push('Cannot Create Branch Authority For : ' + cCur + ' - ' + cBus + ' - ' + data.errorMessage);
                                        } else {
                                            let writeQueryParam: QueryParams = new QueryParams();
                                            let formData: any = {};

                                            writeQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                                            writeQueryParam.set(this.serviceConstants.CountryCode, cCur);
                                            writeQueryParam.set(this.serviceConstants.Action, '6');
                                            writeQueryParam.set('AuthorityUserCode', this.userForm.controls['UserCode'].value);
                                            writeQueryParam.set('BranchNumber', branch[CBBConstants.c_s_VALUE]);
                                            writeQueryParam.set('SetWriteAccess', 'Yes');

                                            formData['Function'] = 'SetWriteAccess';

                                            this.http.makePostRequest('people/grid', 'user', 'Business/iCABSBUserAuthorityBranchGrid', writeQueryParam, formData).subscribe(data => {
                                                if (data.errorMessage) {
                                                    this.msgs.push('Error In User Branch Write For: ' + cCur + ' - ' + cBus + ' - ' + data.errorMessage);
                                                } else {
                                                    if (index === 0) {
                                                        let defaultQueryParam: QueryParams = new QueryParams();
                                                        let formData: any = {};

                                                        defaultQueryParam.set(this.serviceConstants.BusinessCode, cBus);
                                                        defaultQueryParam.set(this.serviceConstants.CountryCode, cCur);
                                                        defaultQueryParam.set(this.serviceConstants.Action, '6');
                                                        defaultQueryParam.set('AuthorityUserCode', this.userForm.controls['UserCode'].value);
                                                        defaultQueryParam.set('BranchNumber', branch[CBBConstants.c_s_VALUE]);
                                                        defaultQueryParam.set('SetDefaultBranch', 'Yes');
                                                        this.http.makePostRequest('people/grid', 'user', 'Business/iCABSBUserAuthorityBranchGrid', writeQueryParam, formData).subscribe(data => {
                                                            this.msgs.push('User Default Branch Set To: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE]);
                                                        }, error => {
                                                            this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE] + ' - ' + JSON.stringify(error));
                                                        });
                                                    }
                                                    this.msgs.push('Successfully Added For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE]);
                                                }
                                            }, error => {
                                                this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE] + ' - ' + JSON.stringify(error));
                                            });
                                        }
                                    }, error => {
                                        this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + branch[CBBConstants.c_s_VALUE] + ' - ' + JSON.stringify(error));
                                    });
                                });
                            }
                        }, error => {
                            this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + JSON.stringify(error));
                        });
                    }
                }, error => {
                    this.msgs.push('Error For: ' + cCur + ' - ' + cBus + ' - ' + JSON.stringify(error));
                });
            });
        });
    }

    private validate(): boolean {
        let isValid: boolean = true;
        for (let control in this.userForm.controls) {
            if (this.userForm.controls[control].invalid) {
                isValid = false;
                this.userForm.controls[control].markAsTouched();
            }
        }

        return isValid;
    }
}
