import { OnInit, Injector, ViewChild, Component } from '@angular/core';
import { QueryParams } from './../../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { GoogleMapPagesModuleRoutes } from '../../../base/PageRoutes';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { MessageConstant } from './../../../../shared/constants/message.constant';
import { EllipsisComponent } from './../../../../shared/components/ellipsis/ellipsis';
import { DropdownStaticComponent } from './../../../../shared/components/dropdown-static/dropdownstatic';
import { BranchServiceAreaSearchComponent } from '../../../internal/search/iCABSBBranchServiceAreaSearch';
import { DatepickerComponent } from './../../../../shared/components/datepicker/datepicker';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';

@Component({
    templateUrl: 'iCABSSePlanningDiary.html'
})

export class SePlanningDiaryComponent extends BaseComponent implements OnInit {
    @ViewChild('StatusSelectDropdown') StatusSelectDropdown: DropdownStaticComponent;
    @ViewChild('PlanningDiaryEllipsis') PlanningDiaryEllipsis: EllipsisComponent;
    @ViewChild('PickerFrom') PickerFrom: DatepickerComponent;
    @ViewChild('PickerTo') PickerTo: DatepickerComponent;

    public isDisable = true;
    public borderRed: boolean = false;
    public validateStartEndDate: boolean = true;
    public pageId: string = '';

    private leadDateFromString: string;
    private leadDateToString: string;
    private branchServicearray: Array<any> = [];
    private employeeSurnamearray: Array<any> = [];
    private employeeCodes: Array<any> = [];

    private queryParams: any = {
        operation: 'Service/iCABSSePlanningDiary',
        module: 'diary',
        method: 'service-planning/maintenance'
    };

    public ellipsisQueryParams: any = {
        branchServiceArea: {
            autoOpen: false,
            showCloseButton: true,
            childConfigParams: {
                'parentMode': 'LookUp-PlanningDiary',
                'showAddNew': true,
                'EmployeeCode': ''
            },
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            },
            contentComponent: BranchServiceAreaSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        }
    };
    public controls: Array<any> = [
        { name: 'SupervisorEmployeeCode', disabled: false, required: true },
        { name: 'BranchServiceAreaCode', required: true, type: MntConst.eTypeCode },
        { name: 'EmployeeSurname', disabled: true, type: MntConst.eTypeText },
        { name: 'tdStartDate', required: true, type: MntConst.eTypeDate },
        { name: 'tdDateTo', required: true, type: MntConst.eTypeDate },
        { name: 'EmployeeCodes' }
    ];
    public planningDiary: Array<any> = [];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEPLANNINGDIARY;
        this.browserTitle = this.pageTitle = 'Planning Diary';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.supervisorEmployeeCodeOnChange();
        this.StatusSelectDropdown.selectedItem = 'None';
        if (this.isReturning()) {
            this.dateFromSelectedValue(event);
            this.ellipsisQueryParams.branchServiceArea.childConfigParams.EmployeeCode = this.pageParams.supervisorName;
            this.StatusSelectDropdown.selectedItem = this.pageParams.supervisorName;
            this.branchServicearray = this.getControlValue('BranchServiceAreaCode');
            this.employeeSurnamearray = this.getControlValue('EmployeeSurname');
            this.employeeCodes = this.getControlValue('EmployeeCodes');
        }
        else { this.fetchDate(event); }
        this.setControlValue('SupervisorEmployeeCode', this.StatusSelectDropdown.selectedItem);
        this.pageParams.supervisorName = this.StatusSelectDropdown.selectedItem;
    }

    public branchServiceAreaCodeOnkeydown(data: any): void {
        this.branchServicearray.push(data.BranchServiceAreaCode);
        this.employeeSurnamearray.push(data.EmployeeSurname);
        this.employeeCodes.push(data.EmployeeCode);
        this.setControlValue('BranchServiceAreaCode', this.branchServicearray);
        this.setControlValue('EmployeeSurname', this.employeeSurnamearray);
        this.setControlValue('EmployeeCodes', this.employeeCodes);
        if (this.branchServicearray.length > 3) {
            this.setControlValue('BranchServiceAreaCode', '');
            this.setControlValue('EmployeeSurname', '');
            this.setControlValue('EmployeeCodes', '');
            this.branchServicearray = [];
            this.employeeSurnamearray = [];
            this.employeeCodes = [];
            this.borderRed = true;
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.PageSpecificMessage.planningDiary.RecordLengthCheck));
        }
        if (data.EmployeeSurname === '') {
            this.modalAdvService.emitError(new ICabsModalVO(data.recordNotFound));
            this.setControlValue('EmployeeCodes', data.EmployeeCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.branchServicearray.pop();
            this.employeeSurnamearray.pop();
            this.employeeCodes.pop();
        } else {
            this.isDisable = this.getControlValue('BranchServiceAreaCode') ? false : true;
        }
    }

    public populateDropdown(data: any): void {
        this.planningDiary.push({ value: 'None', text: 'None' });
        let dropdownarray: Array<any> = data.SupervisorList.split('|');
        dropdownarray.forEach(element => {
            let dropdownele = element.split(':');
            let obj: any = {
                text: dropdownele[1],
                value: dropdownele[0]
            };
            this.planningDiary.push(obj);
        });
    }
    public supervisorEmployeeCodeOnChange(): void {
        let searchPost: QueryParams;
        searchPost = this.getURLSearchParamObject();
        searchPost.set(this.serviceConstants.Action, '6');
        searchPost.set(this.serviceConstants.ActionType, 'GetSupervisorEmployee');
        let postData: Object = {};
        postData['BranchNumber'] = this.utils.getBranchCode();
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchPost, postData)
            .subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) { this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage)); }
                else {
                    this.populateDropdown(data);
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }
    //date set to and from
    public startEndDateValidation(): void {
        let toDate: any = this.getControlValue('tdDateTo');
        let fromDate: any = this.getControlValue('tdStartDate');
        if (toDate <= fromDate) {
            this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.dateSelectionWarning));
            this.validateStartEndDate = false;
        }
        else {
            this.validateStartEndDate = true;
            if (toDate === '') {
                toDate = fromDate;
            }
        }
    }
    public dateFromSelectedValue(event: any): void {
        if (this.getControlValue('BranchServiceAreaCode') && this.getControlValue('tdStartDate') && this.getControlValue('tdDateTo')) {
            this.isDisable = false;
        }
        if (event && event.value) {
            this.setControlValue('tdStartDate', event.value);
        }
    }
    public dateToSelectedValue(event: any): void {
        if (this.getControlValue('BranchServiceAreaCode') && this.getControlValue('tdStartDate') && this.getControlValue('tdDateTo')) {
            this.isDisable = false;
        }
        if (event && event.value) {
            this.setControlValue('tdDateTo', event.value);
        }
    }
    //continue click
    public cmdContinueOnClick(): void {
        let serviceArea: string | Array<string> = this.getControlValue('BranchServiceAreaCode');
        this.checkErrorStatus();
        this.startEndDateValidation();
        if (typeof serviceArea === 'string') {
            this.branchServicearray = serviceArea.split(',');
        }
        if (this.uiForm.valid && this.validateStartEndDate) {
            this.parentMode = 'ServicePlanning';
            if (this.branchServicearray.length === 1 || typeof this.branchServicearray === 'string') {
                this.navigate(this.parentMode, GoogleMapPagesModuleRoutes.ICABSATECHNICIANVISITDIARYGRID, {
                    EmployeeCode: this.getControlValue('EmployeeCodes'),
                    EmployeeSurname0: this.getControlValue('EmployeeSurname'),
                    StartDate: this.getControlValue('tdStartDate'),
                    EndDate: this.getControlValue('tdDateTo')
                });
            }
            else {
                this.navigate('ServicePlanning', GoogleMapPagesModuleRoutes.ICABSAMULTITECHVISITDIARYGRID, {
                    StartDate: this.getControlValue('tdStartDate'),
                    EndDate: this.getControlValue('tdDateTo')
                });
            }
        }
    }
    //check validation
    public checkErrorStatus(): boolean {
        this.PickerFrom.validateDateField();
        this.PickerTo.validateDateField();
        this.riExchange.validateForm(this.uiForm);
        if (this.uiForm.valid) {
            return true;
        } else {
            return false;
        }
    }
    public employeeCodeOnChange(event: any): void {
        this.branchServicearray = [];
        this.employeeSurnamearray = [];
        this.employeeCodes = [];
        this.setControlValue('BranchServiceAreaCode', '');
        this.setControlValue('EmployeeSurname', '');
        this.setControlValue('SupervisorEmployeeCode', event);
        this.ellipsisQueryParams.branchServiceArea.childConfigParams.EmployeeCode = this.getControlValue('SupervisorEmployeeCode');
        this.pageParams.supervisorName = this.getControlValue('SupervisorEmployeeCode');
    }
    public branchServiceAreaCodeOnchange(): void {
        let searchPost: QueryParams;
        searchPost = this.getURLSearchParamObject();
        searchPost.set(this.serviceConstants.Action, '6');
        searchPost.set(this.serviceConstants.ActionType, 'GetEmployeeSurname');
        let postData: Object = {};
        postData['BranchNumber'] = this.utils.getBranchCode();
        postData['SupervisorEmployeeCode'] = this.getControlValue('SupervisorEmployeeCode');
        postData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        this.branchServicearray = [];
        this.employeeSurnamearray = [];
        this.employeeCodes = [];
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchPost, postData)
            .subscribe(data => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.ErrorMessageDesc) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                    this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                }
                else {
                    this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                    this.setControlValue('EmployeeCodes', data.EmployeeCodes);
                    this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                    this.branchServicearray.push(data.BranchServiceAreaCode);
                    this.employeeSurnamearray.push(data.EmployeeSurname);
                    this.employeeCodes.push(data.EmployeeCodes);
                    if (this.branchServicearray.length > 3) {
                        this.setControlValue('BranchServiceAreaCode', '');
                        this.setControlValue('EmployeeSurname', '');
                        this.setControlValue('EmployeeCodes', '');
                        this.branchServicearray = [];
                        this.employeeSurnamearray = [];
                        this.employeeCodes = [];
                        this.riExchange.riInputElement.isError(this.uiForm, 'BranchServiceAreaCode');
                        this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.PageSpecificMessage.planningDiary.RecordLengthCheck));
                    }
                    if (!data.EmployeeSurname) {
                        this.isDisable = true;
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
                        this.branchServicearray.pop();
                        this.employeeSurnamearray.pop();
                        this.employeeCodes.pop();
                    } else {
                        this.isDisable = this.getControlValue('BranchServiceAreaCode') ? false : true;
                        this.branchServicearray = this.getControlValue('BranchServiceAreaCode');
                    }
                }
            }, (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error));
            });
    }
    public fetchDate(e: any): void {
        let lookupIP: any = [
            {
                'table': 'SystemParameter',
                'query': {},
                'fields': ['SystemParameterEndOfWeekDay']
            }
        ];
        this.lookupDetails(lookupIP);
    }
    public lookupDetails(query: any): void {
        this.ajaxSource.next(this.ajaxconstant.START);
        this.LookUp.lookUpPromise(query).then(data => {
            if (data[0] && data[0].length > 0) {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                let tdStartDate: Date = new Date();
                let tdDateTo: Date = new Date();
                let vEndofWeekDay: number;
                let vEndofWeekDate: number;
                vEndofWeekDay = data[0][0].SystemParameterEndOfWeekDay < 7 ? vEndofWeekDay = data[0][0].SystemParameterEndOfWeekDay : 1;
                vEndofWeekDate = ((7 - tdStartDate.getDay()) + vEndofWeekDay) + tdStartDate.getDate();
                let updatedDateFrom = (new Date()).setDate(vEndofWeekDate);
                let newDateFrom = new Date(updatedDateFrom);
                let updatedDateTo = tdStartDate.setDate(vEndofWeekDate + 6);
                let newDateTo = new Date(updatedDateTo);
                this.setControlValue('tdStartDate', newDateFrom);
                this.setControlValue('tdDateTo', newDateTo);
            }
            else {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.recordNotFound));
            }
        }).catch(error => {
            this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            this.modalAdvService.emitError(new ICabsModalVO(error));
        });
    }
}
