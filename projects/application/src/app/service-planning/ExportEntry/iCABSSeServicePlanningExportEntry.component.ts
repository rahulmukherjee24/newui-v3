import { OnInit, Injector, Component, AfterContentInit } from '@angular/core';

import { CustomAlertConstants } from '@shared/components/alert/customalert.constants';
import { IControls } from '@app/base/ControlsType';
import { LightBaseComponent } from '@app/base/BaseComponentLight';
import { MntConst } from '@shared/services/riMaintenancehelper';
import { PageIdentifier } from '@app/base/PageIdentifier';
import { QueryParams } from '@shared/services/http-params-wrapper';
import { StaticUtils } from '@shared/services/static.utility';

@Component({
    templateUrl: 'iCABSSeServicePlanningExportEntry.html'
})

export class ServicePlanningExportEntryComponent extends LightBaseComponent implements OnInit, AfterContentInit {
    public controls: IControls[] = [
        { name: 'BusinessCode', type: MntConst.eTypeCode, disabled: true, value: this.utils.getBusinessCode() },
        { name: 'BusinessDesc', type: MntConst.eTypeText, disabled: true, value: this.utils.getBusinessText() },
        { name: 'BusinessCodeImport', type: MntConst.eTypeCode, disabled: true, value: this.utils.getBusinessCode() },
        { name: 'BusinessDescImport', type: MntConst.eTypeText, disabled: true, value: this.utils.getBusinessText() },
        { name: 'BranchNumber', type: MntConst.eTypeCode, disabled: true, value: this.utils.getBranchCode() },
        { name: 'BranchName', type: MntConst.eTypeText, disabled: true, value: this.utils.getBranchTextOnly() },
        { name: 'BranchNumberImport', type: MntConst.eTypeCode, disabled: true, value: this.utils.getBranchCode() },
        { name: 'BranchNameImport', type: MntConst.eTypeText, disabled: true, value: this.utils.getBranchTextOnly() },
        { name: 'DateFrom', type: MntConst.eTypeDate },
        { name: 'DateTo', type: MntConst.eTypeDate }
    ];

    public pageId: string;

    public headerParams: Record<string, string> = {
        operation: 'CreateBatchProcessEntry',
        module: 'create-batch',
        method: 'create/batch'
    };

    public isExportDisabled: boolean;
    public isImportDisabled: boolean;

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEPLANNINGEXPORTENTRY;
        this.browserTitle = this.pageTitle = 'Service+ Planning Request';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterContentInit(): void {
        super.ngAfterContentInit();

        let date = new Date();
        this.setControlValue('DateFrom', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth(), 1)));
        this.setControlValue('DateTo', this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth() + 1, 0)));
    }

    public onBtnClick(control: string): void {
        this.setRequiredStatus('DateTo', control === 'Export');
        if (!this.riExchange.validateForm(this.uiForm)) {
            return;
        }

        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '0');

        let arrParameterName: Array<string> = [], arrParameterValue: Array<string> = [];
        let strDescription: any;
        let strProgramName: any;

        if (control === 'Export') {

            strDescription = 'Weekly Service Extract';
            strProgramName = 'iCABSServicingExports.p';

            arrParameterName.push('DateFrom'); arrParameterValue.push(this.getControlValue('DateFrom'));
            arrParameterName.push('DateTo'); arrParameterValue.push(this.getControlValue('DateTo'));
        } else {
            strDescription = 'Weekly Service Import';
            strProgramName = 'iCABSServicingImports.p';
        }

        arrParameterName.push('BusinessCode'); arrParameterValue.push(this.businessCode());
        arrParameterName.push('BranchNumber'); arrParameterValue.push(this.utils.getBranchCode());
        arrParameterName.push('Mode'); arrParameterValue.push('SPPL');

        let formData = {
            'Description': strDescription,
            'ProgramName': strProgramName,
            'StartDate': this.globalize.parseDateToFixedFormat(new Date()).toString(),
            'StartTime': this.utils.hmsToSeconds(this.utils.Time()),
            'ParameterName': arrParameterName.join(StaticUtils.RI_SEPARATOR_VALUE_LIST),
            'ParameterValue': arrParameterValue.join(StaticUtils.RI_SEPARATOR_VALUE_LIST)
        };

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.xhrPost(this.headerParams.method, this.headerParams.module, this.headerParams.operation, search, formData).then(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this['is' + control + 'Disabled'] = true;
                this.displayMessage(data.fullError, CustomAlertConstants.c_s_MESSAGE_TYPE_SUCCESS);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.displayMessage(error);
            });
    }

}
