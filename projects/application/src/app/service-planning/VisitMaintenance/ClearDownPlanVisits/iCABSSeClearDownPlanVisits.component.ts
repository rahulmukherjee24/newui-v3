import { Component, OnInit, Injector, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../../app/base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { BranchSearchComponent } from '../../../../app/internal/search/iCABSBBranchSearch';
import { DropdownStaticComponent } from '../../../../shared/components/dropdown-static/dropdownstatic';
import { BusinessSearchComponent } from '../../../../app/internal/search/iCABSSBusinessSearch.component';
import { MessageConstant } from '../../../../shared/constants/message.constant';

@Component({
    templateUrl: 'iCABSSeClearDownPlanVisits.html'
})

export class ClearDownPlanVisitsComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('routeAwayComponent') public routeAwayComponent;
    @ViewChild('branchSearchDropDown') branchSearchDropDown: BranchSearchComponent;
    @ViewChild('businessSearchDropDown') businessSearchDropDown: BusinessSearchComponent;
    @ViewChild('visitTypeSearch') public visitTypeSearch;
    @ViewChild('cancellationReasonDropdown') cancellationReasonDropdown: DropdownStaticComponent;
    // URL Query Parameters
    private queryParams: Object = {
        operation: 'Service/iCABSSeClearDownPlanVisits',
        module: 'plan-visits',
        method: 'service-planning/maintenance'
    };
    public pageId: string = '';
    public controls: Array<Object> = [
        { name: 'BusinessCode', type: MntConst.eTypeCode },
        { name: 'BusinessDesc', type: MntConst.eTypeText },
        { name: 'BranchNumber', type: MntConst.eTypeInteger },
        { name: 'BranchName', type: MntConst.eTypeText },
        { name: 'CancelCode', type: MntConst.eTypeCode },
        { name: 'Condition' },
        { name: 'ServiceVisitFrequency', type: MntConst.eTypeInteger, required: true },
        { name: 'ClearContracts' },
        { name: 'ClearJobs' },
        { name: 'ClearProductSales' },
        { name: 'CompleteClearDown' },
        { name: 'Condition', required: true },
        { name: 'ClearDateTo', required: true, type: MntConst.eTypeDate },
        { name: 'VisitTypeCode', type: MntConst.eTypeCode, readonly: false, disabled: false, required: true },
        { name: 'VisitTypeDesc', type: MntConst.eTypeText, readonly: false, disabled: false, required: true }
    ];
    public vErrorMessageCode: number = 2887;
    public strNoDetailErrorMessage: string = '';
    public languageCode: string;
    public thInformation: any;
    public isThInformationDisplayed: boolean = false;
    public inputParams: any = {
        branchParams: {
            'parentMode': 'LookUp'
        },
        businessParams: {
            'parentMode': 'LookUp'
        }
    };
    public dropDown: any = {
        menu: [],
        VisitTypeCode: {
            isRequired: true,
            triggerValidate: false,
            active: { id: '', text: '' },
            arrData: [],
            inputParams: {
                parentMode: 'LookUp'
            }
        }
    };
    public cancellationReasonList: Array<Object> = [];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSECLEARDOWNPLANVISITS;
        this.browserTitle = this.pageTitle = 'Clear Down Plan Visits';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.languageCode = this.riExchange.LanguageCode();
        this.windowOnLoad();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    ngAfterViewInit(): void {
        this.branchSearchDropDown.active = {
            id: this.utils.getBranchCode(),
            text: this.utils.getBranchText()
        };
        this.businessSearchDropDown.active = {
            id: this.utils.getBusinessCode(),
            text: this.utils.getBusinessCode() + ' - ' + this.utils.getBusinessText()
        };
        this.setControlValue('BranchNumber', this.branchSearchDropDown.active.id);
        this.setControlValue('BranchName', this.branchSearchDropDown.active.text);
        this.setControlValue('BusinessCode', this.businessSearchDropDown.active.id);
        this.setControlValue('BusinessDesc', this.businessSearchDropDown.active.text);
        this.visitTypeSearch.triggerDataFetch(this.dropDown.VisitTypeCode.inputParams);
        this.setControlValue('ServiceVisitFrequency', '4');
        this.setControlValue('Condition', 'GE');
        this.setControlValue('ClearContracts', false);
        this.setControlValue('ClearJobs', false);
        this.setControlValue('ClearProductSales', false);
        this.setControlValue('CompleteClearDown', 'C');
    }

    // errormessage function to get value of strNoDetailErrorMessage
    private errorMessageLookUp(): void {
        let lookupIP = [
            {
                'table': 'ErrorMessageLanguage',
                'query': {
                    'LanguageCode': this.languageCode,
                    'ErrorMessageCode': this.vErrorMessageCode
                },
                'fields': ['ErrorMessageDisplayDescription']
            }
        ];

        this.LookUp.lookUpRecord(lookupIP).subscribe(
            (data) => {
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.strNoDetailErrorMessage = data[0][0].ErrorMessageDisplayDescription;
                    this.modalAdvService.emitError(new ICabsModalVO(this.strNoDetailErrorMessage));
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(MessageConstant.Message.GeneralError));
            });
    }

    private windowOnLoad(): void {
        let firstDay: any;
        let date = new Date();
        firstDay = this.globalize.parseDateToFixedFormat(new Date(date.getFullYear(), date.getMonth() - 1, 0)).toString();
        this.setControlValue('ClearDateTo', this.globalize.parseDateStringToDate(firstDay));
        this.fetchCancellationReason();
    }

    private fetchCancellationReason(): void {
        let searchParams: QueryParams, postData: Object = {}, reasonDescSplit: Array<any>, reasonCodeSplit: Array<any>;
        let reasonCodeLength: number;
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['Function'] = 'LoadCancelPage';
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    reasonDescSplit = data.ReasonDesc.split(';');
                    reasonCodeSplit = data.ReasonCode.split(';');
                    reasonCodeLength = reasonCodeSplit.length;
                    if (reasonCodeLength > 0) {
                        for (let i = 0; i < reasonCodeLength; i++) {
                            this.cancellationReasonList.push({
                                text: reasonDescSplit[i],
                                value: reasonCodeSplit[i]
                            });
                        }
                        this.cancellationReasonDropdown.selectedItem = this.cancellationReasonList[0]['value'];
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    private branchName(): void {
        let searchParams: QueryParams, postData: Object = {};
        searchParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        postData['PostDesc'] = 'Branch';
        postData['BranchNumber'] = this.getControlValue('BranchNumber');
        //making post request
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'],
            searchParams, postData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else
                    this.setControlValue('BranchName', data.BranchName);
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onBranchDataReceived(data: any): void {
        if (data['BranchNumber']) {
            this.setControlValue('BranchNumber', data['BranchNumber']);
            this.setControlValue('BranchName', data['BranchName']);
        } else {
            this.setControlValue('BranchNumber', '');
            this.setControlValue('BranchName', '');
        }
        this.uiForm.markAsDirty();
        if (this.getControlValue('BranchNumber')) {
            this.branchName();
        }
    }

    public visitTypeSelectedValue(value: any): void {
        if (value) {
            this.setControlValue('VisitTypeCode', value.VisitTypeCode || value.value);
            this.setControlValue('VisitTypeDesc', value.VisitTypeDesc || value.value);
        }
        else
            this.dropDown.VisitTypeCode.triggerValidate = true;
        this.uiForm.markAsDirty();
    }

    public datePickerSelectedValue(value: any): void {
        if (value && value.value)
            this.setControlValue('ClearDateTo', value.value);
    }

    public visitTypeDataRecieved(event: any): any {
        let len: number;
        len = event.length || 0;
        this.dropDown.VisitTypeCode.arrData = [];
        for (let i = 0; i < len; i++) {
            this.dropDown.VisitTypeCode.arrData.push({ code: event[i]['VisitType.VisitTypeCode'], desc: event[i]['VisitType.VisitTypeDesc'] });
        }
    }

    public cancellationReasonOnChange(event: any): void {
        this.setControlValue('CancelCode', event);
        this.cancellationReasonDropdown.selectedItem = event;
        this.uiForm.markAsDirty();
    }

    public visitFrequency(event: string): void {
        this.setControlValue('Condition', event);
        this.uiForm.markAsDirty();
    }

    public onClickSubmit(): void {
        if (this.riExchange.validateForm(this.uiForm) && this.getControlValue('VisitTypeCode')) {
            let todayDate = this.globalize.parseDateToFixedFormat(new Date()).toString();
            if (this.getControlValue('ClearDateTo') > todayDate) {
                this.errorMessageLookUp();
            }
            else {
                let searchParams: QueryParams;
                let date = new Date();
                searchParams = this.getURLSearchParamObject();
                searchParams.set(this.serviceConstants.Action, '0');
                searchParams.set('Description', 'Clear Down Plan Visits');
                searchParams.set('ProgramName', 'iCABSClearDownPlanVisits.p');
                searchParams.set('StartDate', this.globalize.parseDateToFixedFormat(date).toString());
                searchParams.set('StartTime', ((date.getHours() * 60 + date.getMinutes()) * 60) + date.getSeconds().toString());
                searchParams.set('Report', 'report');
                searchParams.set('ParameterName', 'BusinessCodeBranchNumberVisitTypeCodeConditionServiceVisitFrequencyClearDateToClearContractsClearJobsClearProductSalesCompleteClearDownCancelCode');
                searchParams.set('ParameterValue', this.businessCode() + '' + this.getControlValue('BranchNumber') + '' + this.getControlValue('VisitTypeCode') + '' + this.getControlValue('Condition') + '' + this.getControlValue('ServiceVisitFrequency') + '' + this.getControlValue('ClearDateTo') + '' + this.getControlValue('ClearContracts') + '' + this.getControlValue('ClearJobs') + '' + this.getControlValue('ClearProductSales') + '' +
                    this.getControlValue('CompleteClearDown') + '' + this.cancellationReasonDropdown.selectedItem);
                this.ajaxSource.next(this.ajaxconstant.START);
                this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], searchParams)
                    .subscribe(
                    (data) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (data.hasError) {
                            this.thInformation = data.fullError;
                            this.isThInformationDisplayed = true;
                        }
                        this.formPristine();
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    }
                    );
            }
        } else
            this.dropDown.VisitTypeCode.triggerValidate = true;
    }
}
