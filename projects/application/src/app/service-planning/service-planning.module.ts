import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../shared/shared.module';
import { InternalSearchModule } from '../internal/search.module';
import { InternalSearchEllipsisModule } from '../internal/search-ellipsis.module';
import { SearchEllipsisDropdownModule } from '../internal/search-ellipsis-dropdown.module';
import { SearchEllipsisBusinessModule }from '../internal/search-ellipsis-business.module';
import { ServicePlanningRootComponent } from './service-planning.component';
import { ServicePlanningRouteDefinitions } from './service-planning.route';
import { AwaitingVerificationGridComponent } from './Workload/ServiceVerification/iCABSSeAwaitingVerificationGrid.component';
import { ServiceMoveGridComponent } from './Workload/BumpWorkload/iCABSSeServiceMoveGrid.component';
import { ClosedServiceGridComponent } from './Templates/HolidayClosedTemplateUse/iCABSAClosedServiceGrid.component';
import { CalendarHistoryGridComponent } from './Templates/iCABSACalendarHistoryGrid.component';
import { CalendarServiceGridComponent } from './Templates/CalendarTemplateUse/iCABSACalendarServiceGrid.component';
import { ClosedTemplateMaintenanceComponent } from './Templates/HolidayClosedTemplateMaintenance/iCABSAClosedTemplateMaintenance.component';
import { SeServicePlanningGridHgComponent } from './PDA/iCABSSeServicePlanningGridHg.component';
import { ServiceCoverCalendarDatesMaintenanceGridComponent } from './CalendarAndSeasons/iCABSAServiceCoverCalendarDatesMaintenanceGrid';
import { ServicePlanningSplitServiceMaintenanceComponent } from './VisitMaintenance/iCABSSeServicePlanningSplitServiceMaintenanceHg.component';
import { ServicePlanningCalendarComponent } from './CalendarAndSeasons/iCABSServicePlanningCalendar.component';
import { ClearDownPlanVisitsComponent } from './VisitMaintenance/ClearDownPlanVisits/iCABSSeClearDownPlanVisits.component';
import { SePlanningDiaryComponent } from './Plans/PlanningDiary/iCABSSePlanningDiary.component';
import { ServiceAreaReSequencingComponent } from './Area/ServiceRe-Sequencing/iCABSSeServiceAreaReSequencing.component';
import { AreaReallocationGridComponent } from './Area/AreaReallocation/iCABSSeAreaReallocationGrid.component';
import { ServiceAreaPostcodeSequenceGridComponent } from './Area/AreaSeqByPostcode/iCABSSeServiceAreaPostcodeSequenceGrid.component';
import { ServicePlanningExportEntryComponent } from './ExportEntry/iCABSSeServicePlanningExportEntry.component';
@NgModule({
    imports: [
        HttpClientModule,
        SharedModule,
        InternalSearchModule,
        InternalSearchEllipsisModule,
        SearchEllipsisDropdownModule,
        SearchEllipsisBusinessModule,
        ServicePlanningRouteDefinitions
    ],
    declarations: [
        ServicePlanningRootComponent,
        AwaitingVerificationGridComponent,
        ServiceMoveGridComponent,
        ClosedServiceGridComponent,
        CalendarHistoryGridComponent,
        CalendarServiceGridComponent,
        ClosedTemplateMaintenanceComponent,
        ServiceAreaReSequencingComponent,
        SeServicePlanningGridHgComponent,
        ServiceCoverCalendarDatesMaintenanceGridComponent,
        ServicePlanningSplitServiceMaintenanceComponent,
        ServicePlanningCalendarComponent,
        ClearDownPlanVisitsComponent,
        SePlanningDiaryComponent,
        AreaReallocationGridComponent,
        ServiceAreaPostcodeSequenceGridComponent,
        ServicePlanningExportEntryComponent
    ]
})

export class ServicePlanningModule { }
