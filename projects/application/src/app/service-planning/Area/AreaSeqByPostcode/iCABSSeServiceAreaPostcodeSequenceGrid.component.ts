import { Component, OnInit, Injector, OnDestroy, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';

import { BaseComponent } from '../../../base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { ContractManagementModuleRoutes } from '../../../base/PageRoutes';
import { MntConst } from './../../../../shared/services/riMaintenancehelper';
import { GridAdvancedComponent } from './../../../../shared/components/grid-advanced/grid-advanced';
import { ICabsModalVO } from './../../../../shared/components/modal-adv/modal-adv-vo';
import { PaginationComponent } from './../../../../shared/components/pagination/pagination';
import { DropdownStaticComponent } from './../../../../shared/components/dropdown-static/dropdownstatic';
import { BranchServiceAreaSearchComponent } from './../../../internal/search/iCABSBBranchServiceAreaSearch';

@Component({
    templateUrl: 'iCABSSeServiceAreaPostcodeSequenceGrid.html'
})

export class ServiceAreaPostcodeSequenceGridComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('riGrid') public riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') public riGridPagination: PaginationComponent;
    @ViewChild('ctypeSelectDropdown') public ctypeSelectDropdown: DropdownStaticComponent;

    private focusElement: any;
    private isRefreshClick: boolean = false;
    //query params
    private queryParams: Object = {
        operation: 'Service/iCABSSeServiceAreaPostcodeSequenceGrid',
        module: 'structure',
        method: 'service-planning/maintenance'
    };
    //grid params
    public gridParams: any = {
        totalRecords: 0,
        itemsPerPage: 10,
        currentPage: 1,
        pageCurrent: 1,
        riGridMode: 0
    };
    //branch service area ellipsis params
    public branchServiceAreaParams: any = {
        parentMode: 'LookUp-Emp',
        component: BranchServiceAreaSearchComponent
    };
    public isHidePagination: boolean = true;
    public pageId: string = '';
    public setFocusOnBranchServiceAC = new EventEmitter<boolean>();
    public contractTypeCodeArr: Array<any> = [{ text: 'All', value: 'All' }];
    //form controls
    public controls = [
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode, required: true, commonValidator: true },
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'ContractTypeCode' },
        { name: 'PortfolioStatusType' },
        { name: 'IncludeUnsequenced' },
        { name: 'SequenceGap', type: MntConst.eTypeInteger, required: true }
    ];

    constructor(injector: Injector) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEAREAPOSTCODESEQUENCEGRID;
        this.browserTitle = this.pageTitle = 'Service Area Sequence By Postcode';
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        if (this.isReturning()) {
            this.populateUIFromFormData();
            this.contractTypeCodeArr = this.pageParams.contractTypeCodeArr;
            setTimeout(() => {
                this.ctypeSelectDropdown.selectedItem = this.pageParams.cType;
                this.buildGrid();
                this.loadGridData();
            }, 0);
        } else
            this.windowOnload();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private windowOnload(): void {
        this.setFocusOnBranchServiceAC.emit(true);
        this.pageParams.istdSequence1 = true;
        this.pageParams.isResequenceNumDisable = false;
        this.setControlValue('SequenceGap', 10);
        this.setControlValue('IncludeUnsequenced', false);
        this.setControlValue('PortfolioStatusType', 'Current');
        this.getBusinessContractTypes();
        this.buildGrid();
    }

    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceCover', 'BranchServiceAreaSeqNo', MntConst.eTypeInteger, 6, false);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentRight);
        this.riGrid.AddColumnUpdateSupport('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumn('Postcode', 'ServiceCover', 'Postcode', MntConst.eTypeCode, 9, true);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ContractPremise', 'ServiceCover', 'ContractPremise', MntConst.eTypeText, 16);
        this.riGrid.AddColumnAlign('ContractPremise', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'ServiceCover', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PremiseAddressLine1', 'ServiceCover', 'PremiseAddressLine1', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseAddressLine1', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ProductCode', 'ServiceCover', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('PortfolioStatusDesc', 'ServiceVisit', 'PortfolioStatusDesc', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PortfolioStatusDesc', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCover', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceQuantity', 'ServiceCover', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('BranchServiceAreaSeqNo', true);
        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        this.riGrid.Complete();
    }

    private loadGridData(): void {
        this.riGrid.UpdateBody = true;
        this.riGrid.UpdateFooter = true;
        this.riGrid.UpdateHeader = true;
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        search.set(this.serviceConstants.PageSize, this.gridParams['itemsPerPage'].toString());
        search.set(this.serviceConstants.PageCurrent, this.gridParams['pageCurrent'].toString());
        search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
        search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        search.set(this.serviceConstants.GridMode, this.gridParams['riGridMode']);
        search.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle ? this.pageParams.gridHandle : this.pageParams.gridHandle = this.utils.randomSixDigitString());
        if (this.isRefreshClick) {
            this.isRefreshClick = false;
            search.set(this.serviceConstants.GridCacheRefresh, 'True');
        }
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('ShowType', 'All');
        search.set('ContractTypeCode', this.ctypeSelectDropdown.selectedItem);
        search.set('PortfolioStatusType', this.getControlValue('PortfolioStatusType'));
        this.queryParams['search'] = search;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.queryParams['method'], this.queryParams['module'],
            this.queryParams['operation'], this.queryParams['search'])
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.gridParams['pageCurrent'] = data.pageData ? data.pageData.pageNumber : 1;
                    this.gridParams['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                    this.riGrid.UpdateBody = true;
                    this.riGrid.UpdateHeader = true;
                    this.riGrid.UpdateFooter = true;
                    this.riGrid.Execute(data);
                    if (data.pageData && (data.pageData.lastPageNumber * 10) > 0)
                        this.isHidePagination = false;
                    else
                        this.isHidePagination = true;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.gridParams['totalRecords'] = 1;
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //Api to get contract types
    private getBusinessContractTypes(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        this.queryParams['search'] = search;
        formData[this.serviceConstants.Function] = 'GetBusinessContractTypes';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    if (data && data.BusinessContractTypes)
                        this.buildMenuOptions(data.BusinessContractTypes);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //Function to build dropdown with contract types
    private buildMenuOptions(cTypes: any): void {
        let cTypeArr: Array<any> = cTypes.split(',');
        let cTypelen, i: number;
        cTypelen = cTypeArr.length;
        for (i = 0; i < cTypelen; i++) {
            let cType = cTypeArr[i];
            switch (cType) {
                case 'C':
                    this.contractTypeCodeArr.push({ text: 'Contracts', value: 'C' });
                    this.ctypeSelectDropdown.selectedItem = 'C';
                    break;
                case 'J':
                    this.contractTypeCodeArr.push({ text: 'Jobs', value: 'J' });
                    break;
                case 'P':
                    this.contractTypeCodeArr.push({ text: 'Product Sales', value: 'P' });
                    break;
            }
        }
    }

    //Grid Double click
    public onGridDblClick(): void {
        this.pageParams.contractTypeCodeArr = this.contractTypeCodeArr;
        this.pageParams.cType = this.ctypeSelectDropdown.selectedItem;
        let CurrentContractTypeURLParameter: string;
        this.attributes.PremiseRowID = this.riGrid.Details.GetAttribute('Postcode', 'rowID');
        this.attributes.ServiceCoverRowID = this.riGrid.Details.GetAttribute('ProductCode', 'rowID');
        this.attributes.ContractTypeCode = this.riGrid.Details.GetAttribute('Postcode', 'AdditionalProperty');
        switch (this.attributes.ContractTypeCode) {
            case 'C':
                CurrentContractTypeURLParameter = '';
                break;
            case 'J':
                CurrentContractTypeURLParameter = '<job>';
                break;
            case 'P':
                CurrentContractTypeURLParameter = '<product>';
                break;
        }
        switch (this.riGrid.CurrentColumnName) {
            case 'Postcode':
                this.navigate('ServiceAreaSequence', ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    RowID: this.attributes.PremiseRowID,
                    contracttypecode: this.attributes.ContractTypeCode
                });
                break;
            case 'ProductCode':
                this.navigate('ServiceAreaSequence', ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    RowID: this.attributes.ServiceCoverRowID,
                    currentContractType: this.attributes.ContractTypeCode
                });
                break;
        }
    }

    public onCellBlur(data: any): void {
        this.focusElement = data.srcElement;
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '2');
        this.queryParams['search'] = search;
        formData['BranchServiceAreaSeqNoRowID'] = '1';
        formData['BranchServiceAreaSeqNo'] = this.riGrid.Details.GetValue('BranchServiceAreaSeqNo');
        formData['PostcodeRowID'] = this.riGrid.Details.GetAttribute('Postcode', 'rowID');
        formData['Postcode'] = this.riGrid.Details.GetValue('Postcode');
        formData['ContractPremise'] = this.riGrid.Details.GetValue('ContractPremise');
        formData['PremiseName'] = this.riGrid.Details.GetValue('PremiseName');
        formData['PremiseAddressLine1'] = this.riGrid.Details.GetValue('PremiseAddressLine1');
        formData['ProductCodeRowID'] = this.riGrid.Details.GetAttribute('ProductCode', 'rowID');
        formData['ProductCode'] = this.riGrid.Details.GetValue('ProductCode');
        formData['PortfolioStatusDesc'] = this.riGrid.Details.GetValue('PortfolioStatusDesc');
        formData['ServiceVisitFrequency'] = this.riGrid.Details.GetValue('ServiceVisitFrequency');
        formData['ServiceQuantity'] = this.riGrid.Details.GetValue('ServiceQuantity');
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['ShowType'] = 'All';
        formData['ContractTypeCode'] = this.ctypeSelectDropdown.selectedItem;
        formData['PortfolioStatusType'] = this.getControlValue('PortfolioStatusType');
        formData[this.serviceConstants.GridMode] = '3';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.PageSize] = this.gridParams['itemsPerPage'].toString();
        formData[this.serviceConstants.PageCurrent] = this.gridParams['pageCurrent'].toString();
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'],
            this.queryParams['operation'], this.queryParams['search'], formData)
            .subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    let modalVO: ICabsModalVO = new ICabsModalVO(data.errorMessage, data.fullError);
                    modalVO.closeCallback = this.onErrorCloseCallback.bind(this);
                    this.modalAdvService.emitError(modalVO);
                } else {
                    this.riGrid.Mode = MntConst.eModeNormal;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onErrorCloseCallback(): void {
        this.riGrid.setFocusBack(this.focusElement);
    }

    //Pagination click
    public getCurrentPage(currentPage: any): void {
        if (this.getControlValue('BranchServiceAreaCode') !== '') {
            this.gridParams['pageCurrent'] = currentPage.value;
            this.riGrid.RefreshRequired();
            this.loadGridData();
        }
    }

    //Refresh click Function
    public refreshGrid(): void {
        if (this.getControlValue('BranchServiceAreaCode') !== '') {
            this.isRefreshClick = true;
            this.riGrid.RefreshRequired();
            this.loadGridData();
        }
    }

    //Grid Sort Function
    public riGridSort(): void {
        if (this.getControlValue('BranchServiceAreaCode') !== '') {
            this.riGrid.RefreshRequired();
            this.riGrid.DescendingSort = false;
            this.loadGridData();
        }
    }

    //BranchServiceArea ellipsis click
    public ellipsisData(data: any): void {
        this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
    }

    //BranchServiceArea on change
    public onbranchServiceAreaChange(): void {
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.queryParams['search'] = search;
        formData[this.serviceConstants.Function] = 'GetBranchServiceArea';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                }
                else {
                    if (data && data.EmployeeSurname)
                        this.setControlValue('EmployeeSurname', data.EmployeeSurname);
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.setControlValue('EmployeeSurname', '');
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
        if (this.getControlValue('BranchServiceAreaCode') === '')
            this.setControlValue('EmployeeSurname', '');
    }

    //Resequence By Postcode button click
    public onCmdResequenceNumbers(data: any): void {
        let cmdResequenceNumbersValue: string = data.target.value;
        let formData: Object = {};
        let search: QueryParams = this.getURLSearchParamObject();
        search.set(this.serviceConstants.Action, '6');
        search.set('BranchNumber', this.utils.getBranchCode());
        search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        search.set('ResequenceNumbers', cmdResequenceNumbersValue);
        search.set('IncludeUnsequenced', this.getControlValue('IncludeUnsequenced'));
        search.set('SequenceGap', this.getControlValue('SequenceGap'));
        this.queryParams['search'] = search;
        formData[this.serviceConstants.Function] = 'ResequenceByPostcode';

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makePostRequest(this.queryParams['method'], this.queryParams['module'], this.queryParams['operation'], this.queryParams['search'], formData).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data.hasError)
                    this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
                else {
                    this.pageParams.isResequenceNumDisable = true;
                    this.buildGrid();
                    this.loadGridData();
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    //PortfolioStatusType dropdown
    public onPortfolioStatusTypeChange(): void {
        if (this.getControlValue('PortfolioStatusType') === 'All' || this.getControlValue('PortfolioStatusType') === 'NonCurrent')
            this.pageParams.istdSequence1 = false;
        else
            this.pageParams.istdSequence1 = true;
    }

    //ContractTypeCode dropdown
    public onContractTypeCodeSelect(data: any): void {
        this.setControlValue('ContractTypeCode', this.ctypeSelectDropdown.selectedItem);
    }
}
