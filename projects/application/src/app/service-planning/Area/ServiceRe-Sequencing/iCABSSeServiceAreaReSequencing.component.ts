import { Component, OnInit, Injector, ViewChild, OnDestroy, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs';

import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { PaginationComponent } from '../../../../shared/components/pagination/pagination';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { BaseComponent } from '../../../base/BaseComponent';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { BranchServiceAreaSearchComponent } from '../../../internal/search/iCABSBBranchServiceAreaSearch';
import { MessageConstant } from '../../../../shared/constants/message.constant';
import { DropdownStaticComponent } from '../../../../shared/components/dropdown-static/dropdownstatic';
import { VariableService } from '../../../../shared/services/variable.service';

@Component({
    templateUrl: 'iCABSSeServiceAreaReSequencing.html',
    styles: [`
    .message-box.info-label {
        color: #000;
        background-color: #FF8000;
    }
  `]
})

export class ServiceAreaReSequencingComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('riGrid') riGrid: GridAdvancedComponent;
    @ViewChild('riGridPagination') riGridPagination: PaginationComponent;
    @ViewChild('dateSelector') dateSelector: DropdownStaticComponent;
    @ViewChild('routeAwayComponent') public routeAwayComponent;

    private vEndofWeekDay: number;
    private vEndofWeekDate: Date;
    private afterSave: boolean = false;
    private queryParams: any = {
        operation: 'Service/iCABSSeServiceAreaReSequencing',
        module: 'structure',
        method: 'service-planning/maintenance'
    };
    public pageId: string = '';
    public controls: any = [
        { name: 'EmployeeSurname', type: MntConst.eTypeText, disabled: true },
        { name: 'DateSelection' },
        { name: 'GridPageSize', type: MntConst.eTypeInteger, required: true },
        { name: 'LocationStartPostcode', type: MntConst.eTypeText, disabled: true },
        { name: 'LocationEndPostcode', type: MntConst.eTypeText, disabled: true },
        { name: 'StartDate', type: MntConst.eTypeDate, required: true },
        { name: 'EndDate', type: MntConst.eTypeDate, required: true },
        { name: 'BranchServiceAreaCode', type: MntConst.eTypeCode },
        { name: 'FileUploaded', type: MntConst.eTypeText }
    ];
    public setFocusBranchServiceAreaCode = new EventEmitter<boolean>();
    public isDisablePagination: boolean = false;
    public canDeactivateObservable: Observable<boolean>;
    public isVisible: any = {
        isDateVisible: false,
        isPlanVisible: true,
        isMsgBoxVisible: false
    };
    public gridConfig: any = {
        pageSize: 11,
        currentPage: 1,
        totalRecords: 1,
        action: '2'
    };
    public ellipsisConfig: any = {
        serviceArea: {
            isDisabled: false,
            childConfigParams: {
                parentMode: 'LookUp-Emp'
            },
            contentComponent: BranchServiceAreaSearchComponent
        }
    };
    public pageSize: number = 11;
    public dateSelectorDropdown: any = [];
    private shouldRefreshCache: boolean = true;

    constructor(injector: Injector, private ref: ChangeDetectorRef, private variableService: VariableService) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSESERVICEAREARESEQUENCING;
        this.browserTitle = this.pageTitle = 'Service Planning Re-Sequence';
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.getSystemParameters();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    private getSystemParameters(): void {
        let lookUpSys: any = [{
            'table': 'SystemParameter',
            'query': {},
            'fields': ['SystemParameterEndOfWeekDay']
        }];
        this.LookUp.lookUpRecord(lookUpSys).subscribe((data) => {
            if (data.hasError) {
                this.modalAdvService.emitError(new ICabsModalVO(data.errorMessage, data.fullError));
            } else {
                if (data[0][0].SystemParameterEndOfWeekDay < 7) {
                    this.vEndofWeekDay = data[0][0].SystemParameterEndOfWeekDay;
                } else {
                    this.vEndofWeekDay = 1;
                }
                let today: any = new Date();
                this.vEndofWeekDate = this.utils.addDays(today, ((7 - today.getDay()) + this.vEndofWeekDay - 1));
                let tempDate: any = this.vEndofWeekDate.toString();
                tempDate = (this.globalize.parseDateStringToDate(tempDate)).toString();
                this.vEndofWeekDate = new Date(tempDate);
                if (this.isReturning()) {
                    this.buildGrid();
                } else {
                    this.onWindowLoad();
                }
            }
        });
    }

    private onWindowLoad(): void {
        this.setControlValue('GridPageSize', 11);
        this.pageSize = 11;
        setTimeout(() => {
            this.setFocusBranchServiceAreaCode.emit(true);
        }, 0);
        this.buildGrid();
        if (this.parentMode === 'Summary') {
            this.isVisible.isDateVisible = true;
            this.isVisible.isPlanVisible = false;
            this.disableControl('BranchServiceAreaCode', true);
            this.setControlValue('BranchServiceAreaCode', this.riExchange.getParentHTMLValue('BranchServiceAreaCode'));
            let toOptomiseDates: string = this.riExchange.getParentHTMLValue('ToOptomiseDates');
            let vcOptomiseDatesNum: number = toOptomiseDates.split(';').length;
            let vcOptomiseDates: any = toOptomiseDates.split(';');
            this.buildDateSelection(vcOptomiseDates, vcOptomiseDatesNum);
            this.populateGrid();
        } else {
            let tempDtFrom: Date = this.utils.addDays(this.vEndofWeekDate, 1);
            this.setControlValue('StartDate', tempDtFrom);
            let tempDtTo: Date = this.utils.addDays(tempDtFrom, 6);
            this.setControlValue('EndDate', tempDtTo);
        }
    }

    private promptConfirm(data: any): void {
        this.afterSave = false;
        this.isVisible.isMsgBoxVisible = false;
        this.onRiGridRefresh();
    }

    public buildGrid(): void {
        this.pageSize = this.getControlValue('GridPageSize');
        this.riGrid.Clear();
        this.riGrid.FunctionUpdateSupport = true;
        this.riGrid.AddColumn('UpdateServicePlanSequenceNumber', 'ReSequencing', 'UpdateServicePlanSequenceNumber', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('UpdateServicePlanSequenceNumber', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('UpdateServicePlanSequenceNumber', true);

        this.riGrid.AddColumn('UniqueReference', 'ReSequencing', 'UniqueReference', MntConst.eTypeText, 10);
        this.riGrid.AddColumnScreen('UniqueReference', false);
        this.riGrid.AddColumnUpdateSupport('UniqueReference', false);

        this.riGrid.AddColumn('VisitDate', 'ReSequencing', 'VisitDate', MntConst.eTypeDate, 15);
        this.riGrid.AddColumnAlign('VisitDate', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnNoWrap('VisitDate', true);
        this.riGrid.AddColumnUpdateSupport('VisitDate', false);

        this.riGrid.AddColumn('ProdCode', 'ReSequencing', 'ProdCode', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ProdCode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('ProdCode', false);

        this.riGrid.AddColumn('ServiceVisitFrequency', 'ServiceCover', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ServiceVisitFrequency', false);

        this.riGrid.AddColumn('ContractNum', 'ReSequencing', 'ContractNum', MntConst.eTypeCode, 10);
        this.riGrid.AddColumnAlign('ContractNum', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('ContractNum', false);

        this.riGrid.AddColumn('PremiseNum', 'ReSequencing', 'PremiseNum', MntConst.eTypeText, 5);
        this.riGrid.AddColumnAlign('PremiseNum', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('PremiseNum', false);

        this.riGrid.AddColumn('PremiseName', 'ReSequencing', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('PremiseName', false);

        this.riGrid.AddColumn('PremiseAddress1', 'ReSequencing', 'PremiseAddress1', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('PremiseAddress1', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('PremiseAddress1', false);

        this.riGrid.AddColumn('PremiseAddress2', 'ReSequencing', 'PremiseAddress2', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('PremiseAddress2', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('PremiseAddress2', false);

        this.riGrid.AddColumn('PremiseAddress3', 'ReSequencing', 'PremiseAddress3', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('PremiseAddress3', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('PremiseAddress3', false);

        this.riGrid.AddColumn('PremiseTown', 'ReSequencing', 'PremiseTown', MntConst.eTypeText, 15);
        this.riGrid.AddColumnAlign('PremiseTown', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('PremiseTown', false);

        this.riGrid.AddColumn('PremisePostcode', 'ReSequencing', 'PremisePostcode', MntConst.eTypeCode, 5);
        this.riGrid.AddColumnAlign('PremisePostcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnUpdateSupport('PremisePostcode', false);

        this.riGrid.AddColumn('VisitStartTime', 'ReSequencing', 'VisitStartTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('VisitStartTime', MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('VisitStartTime', false);
        this.riGrid.AddColumnUpdateSupport('VisitStartTime', false);

        this.riGrid.AddColumn('VisitEndTime', 'ReSequencing', 'VisitEndTime', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('VisitEndTime', MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('VisitEndTime', false);
        this.riGrid.AddColumnUpdateSupport('VisitEndTime', false);

        this.riGrid.AddColumn('VisitDuration', 'ReSequencing', 'VisitDuration', MntConst.eTypeTime, 5);
        this.riGrid.AddColumnAlign('VisitDuration', MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('VisitDuration', false);
        this.riGrid.AddColumnUpdateSupport('VisitDuration', false);

        this.riGrid.AddColumn('PremiseContact', 'ReSequencing', 'PremiseContact', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremiseContact', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnScreen('PremiseContact', false);
        this.riGrid.AddColumnUpdateSupport('PremiseContact', false);

        this.riGrid.AddColumn('PremisePhone', 'ReSequencing', 'PremisePhone', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremisePhone', MntConst.eAlignmentLeft);
        this.riGrid.AddColumnScreen('PremisePhone', false);
        this.riGrid.AddColumnUpdateSupport('PremisePhone', false);

        this.riGrid.AddColumn('PremiseGPSCordX', 'ReSequencing', 'PremiseGPSCordX', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremiseGPSCordX', MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('PremiseGPSCordX', false);
        this.riGrid.AddColumnUpdateSupport('PremiseGPSCordX', false);

        this.riGrid.AddColumn('PremiseGPSCordY', 'ReSequencing', 'PremiseGPSCordY', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('PremiseGPSCordY', MntConst.eAlignmentRight);
        this.riGrid.AddColumnScreen('PremiseGPSCordY', false);
        this.riGrid.AddColumnUpdateSupport('PremiseGPSCordY', false);

        this.riGrid.AddColumn('PremiseOpeningTime01', 'ReSequencing', 'PremiseOpeningTime01', MntConst.eTypeTime, 10);
        this.riGrid.AddColumnAlign('PremiseOpeningTime01', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('PremiseOpeningTime01', false);
        this.riGrid.AddColumnUpdateSupport('PremiseOpeningTime01', false);

        this.riGrid.AddColumn('PremiseClosingTime01', 'ReSequencing', 'PremiseClosingTime01', MntConst.eTypeTime, 10);
        this.riGrid.AddColumnAlign('PremiseClosingTime01', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('PremiseClosingTime01', false);
        this.riGrid.AddColumnUpdateSupport('PremiseClosingTime01', false);

        this.riGrid.AddColumn('PremiseOpeningTime02', 'ReSequencing', 'PremiseOpeningTime02', MntConst.eTypeTime, 10);
        this.riGrid.AddColumnAlign('PremiseOpeningTime02', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('PremiseOpeningTime02', false);
        this.riGrid.AddColumnUpdateSupport('PremiseOpeningTime02', false);

        this.riGrid.AddColumn('PremiseClosingTime02', 'ReSequencing', 'PremiseClosingTime02', MntConst.eTypeTime, 10);
        this.riGrid.AddColumnAlign('PremiseClosingTime02', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnScreen('PremiseClosingTime02', false);
        this.riGrid.AddColumnUpdateSupport('PremiseClosingTime02', false);

        this.riGrid.AddColumn('VisitTypeCode', 'ReSequencing', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnUpdateSupport('VisitTypeCode', false);

        this.riGrid.AddColumn('PlanVisitStatus', 'ReSequencing', 'PlanVisitStatus', MntConst.eTypeText, 10);
        this.riGrid.AddColumnAlign('PlanVisitStatus', MntConst.eAlignmentLeft);

        this.riGrid.AddColumn('Uploaded', 'ReSequencing', 'Uploaded', MntConst.eTypeImage, 1);
        this.riGrid.AddColumnAlign('Uploaded', MntConst.eAlignmentCenter);

        this.riGrid.Complete();
    }

    public populateGrid(): void {
        if (this.riGrid.Mode === MntConst.eModeUpdate) {
            this.isVisible.isMsgBoxVisible = true;
        }
        if (this.getControlValue('FileUploaded')) {
            this.isVisible.isMsgBoxVisible = true;
        }

        if (!this.afterSave) {
            let locationDetails: string = 'Location Start:' + this.getControlValue('LocationStartPostcode') + 'End:' + this.getControlValue('LocationEndPostcode');
            let gridSearch: QueryParams = this.getURLSearchParamObject();
            gridSearch.set('BranchNumber', this.utils.getBranchCode());
            gridSearch.set('StartDate', this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate')).toString());
            gridSearch.set('EndDate', this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate')).toString());
            gridSearch.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
            gridSearch.set('FileUploaded', this.getControlValue('FileUploaded'));
            gridSearch.set('SrcMode', this.parentMode);
            gridSearch.set('LocationDetails', locationDetails);
            this.setControlValue('FileUploaded', '');

            gridSearch.set(this.serviceConstants.GridMode, '0');
            gridSearch.set(this.serviceConstants.GridHandle, this.pageParams.gridHandle ? this.pageParams.gridHandle : this.pageParams.gridHandle = this.utils.randomSixDigitString());
            gridSearch.set(this.serviceConstants.PageSize, this.gridConfig.pageSize.toString());
            gridSearch.set(this.serviceConstants.PageCurrent, this.gridConfig.currentPage.toString());
            gridSearch.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);
            gridSearch.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
            gridSearch.set(this.serviceConstants.Action, this.gridConfig.action.toString());
            gridSearch.set(this.serviceConstants.GridCacheRefresh, this.shouldRefreshCache ? 'True' : 'False');

            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makeGetRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, gridSearch)
                .subscribe(
                    (e) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        if (e.hasError) {
                            this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                        } else {
                            this.riGrid.RefreshRequired();
                            this.riGridPagination.currentPage = this.gridConfig.currentPage = e.pageData ? e.pageData.pageNumber : 1;
                            this.riGridPagination.totalItems = this.gridConfig.totalRecords = e.pageData ? e.pageData.lastPageNumber * this.pageSize : 1;
                            this.riGrid.UpdateBody = true;
                            this.riGrid.UpdateFooter = true;
                            this.riGrid.UpdateHeader = true;
                            this.riGrid.Execute(e);
                            this.ref.detectChanges();
                            let isBlnGridHasData: any = (this.riGrid.HTMLGridBody && this.riGrid.HTMLGridBody.children.length > 0);
                            if (isBlnGridHasData) {
                                this.pageParams.gridHandle = this.riGrid.Details.GetAttribute('UpdateServicePlanSequenceNumber', 'AdditionalProperty');
                            }
                        }
                    },
                    (error) => {
                        this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                        this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                    });
        }
    }

    public onRiGridRefresh(): void {
        if (this.isVisible.isMsgBoxVisible) {
            let msgObj: any = new ICabsModalVO(MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.confirmWarning, null, this.promptConfirm.bind(this));
            msgObj.title = MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.confirmTitle;
            this.modalAdvService.emitPrompt(msgObj);
        }
        if (this.riGrid.currentPage <= 0) {
            this.riGrid.currentPage = 1;
        }
        this.shouldRefreshCache = true;
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public getCurrentPage(currentPage: any): void {
        this.gridConfig.currentPage = currentPage.value;
        this.shouldRefreshCache = false;
        this.afterSave = false;
        this.riGrid.RefreshRequired();
        this.populateGrid();
    }

    public onBranchServiceAreaDataReceived(data: any): void {
        if (data) {
            this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode);
            this.setControlValue('EmployeeSurname', data.EmployeeSurname);
        }
        this.onBranchServiceAreaCodeChange();
        this.buildGrid();
        this.riGrid.RefreshRequired();
    }

    public onBranchServiceAreaCodeChange(): void {
        if (this.getControlValue('BranchServiceAreaCode')) {
            let searchParams: QueryParams = this.getURLSearchParamObject();
            searchParams.set(this.serviceConstants.Action, '6');
            let formData: any = {};
            formData[this.serviceConstants.Function] = 'GetEmployeeSurname';
            formData['BranchNumber'] = this.utils.getBranchCode();
            formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
            formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
            this.ajaxSource.next(this.ajaxconstant.START);
            this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
                (e) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    if (e.hasError) {
                        this.setControlValue('EmployeeSurname', '');
                        this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                    } else {
                        this.setControlValue('EmployeeSurname', e.EmployeeSurname);
                        if (e.ErrorMessageDesc) {
                            this.modalAdvService.emitMessage(new ICabsModalVO(e.ErrorMessageDesc));
                        } else {
                            this.buildGrid();
                            this.riGrid.RefreshRequired();
                            this.gridConfig.totalRecords = 1;
                        }
                    }
                },
                (error) => {
                    this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                    this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
                });
        } else {
            this.setControlValue('EmployeeSurname', '');
        }
        this.buildGrid();
        this.riGrid.RefreshRequired();
        this.riGridPagination.currentPage = 1;
    }

    public onGridPageSizeChange(): void {
        if (this.getControlValue('GridPageSize')) {
            this.gridConfig.pageSize = this.getControlValue('GridPageSize');
        }
    }

    public buildDateSelection(dateString: any, length: any): void {
        let count: number = 0;
        while (count < length) {
            let data: any = {};
            data = { text: dateString[count], value: dateString[count] };
            this.dateSelectorDropdown.push(data);
            count++;
        }
        if (this.isVisible.isDateVisible) {
            this.dateSelector.updateSelectedItem();
        }
    }

    public onDateSelectorChange(): void {
        this.setControlValue('StartDate', this.dateSelector.selectedItem);
        this.setControlValue('EndDate', this.dateSelector.selectedItem);
        this.buildGrid();
        this.riGrid.RefreshRequired();

        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'GetLocationStartEnd';
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['StartDate'] = this.getControlValue('StartDate');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    if (e.ErrorMessageDesc) {
                        this.modalAdvService.emitMessage(new ICabsModalVO(e.ErrorMessageDesc));
                    } else {
                        this.setControlValue('EmployeeSurname', e.EmployeeSurname);
                        this.setControlValue('LocationStart', e.LocationStart);
                        this.setControlValue('LocationStartPostcode', e.LocationStartPostcode);
                        this.setControlValue('LocationEndPostcode', e.LocationEndPostcode);
                        this.setControlValue('LocationEnd', e.LocationEnd);
                        this.buildGrid();
                    }
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onSequenceNumberChange(data: any): void {
        let formData: any = {};
        formData['UpdateServicePlanSequenceNumberRowID'] = this.riGrid.Details.GetAttribute('UpdateServicePlanSequenceNumber', 'rowID');
        formData['UpdateServicePlanSequenceNumber'] = data.target.value;
        formData['VisitDate'] = this.globalize.parseDateToFixedFormat(this.riGrid.Details.GetValue('VisitDate'));
        formData['ProdCode'] = this.riGrid.Details.GetValue('ProdCode');
        formData['ServiceVisitFrequency'] = this.riGrid.Details.GetValue('ServiceVisitFrequency');
        formData['ContractNum'] = this.riGrid.Details.GetValue('ContractNum');
        formData['PremiseNum'] = this.riGrid.Details.GetValue('PremiseNum');
        formData['PremiseName'] = this.riGrid.Details.GetValue('PremiseName');
        formData['PremiseAddress1'] = this.riGrid.Details.GetValue('PremiseAddress1');
        formData['PremiseAddress2'] = this.riGrid.Details.GetValue('PremiseAddress2');
        formData['PremiseAddress3'] = this.riGrid.Details.GetValue('PremiseAddress3');
        formData['PremiseTown'] = this.riGrid.Details.GetValue('PremiseTown');
        formData['PremisePostcode'] = this.riGrid.Details.GetValue('PremisePostcode');
        formData['VisitTypeCode'] = this.riGrid.Details.GetValue('VisitTypeCode');
        formData['PlanVisitStatus'] = this.riGrid.Details.GetValue('PlanVisitStatus');
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['FileUploaded'] = this.getControlValue('FileUploaded');
        formData['SrcMode'] = this.parentMode;
        let locationDetails: string = 'Location Start:' + (this.getControlValue('LocationStartPostcode') ? this.getControlValue('LocationStartPostcode') : ' ') + 'End:' + (this.getControlValue('LocationEndPostcode') ? this.getControlValue('LocationEndPostcode') : ' ');
        formData['LocationDetails'] = locationDetails;
        formData[this.serviceConstants.GridMode] = '3';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData[this.serviceConstants.GridHeaderClickedColumn] = this.riGrid.HeaderClickedColumn;
        formData[this.serviceConstants.GridSortOrder] = this.riGrid.SortOrder;
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '2');
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.riGrid.Mode = MntConst.eModeNormal;

                if (e.hasError) {
                    this.isDisablePagination = true;
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.isDisablePagination = false;
                    this.isVisible.isMsgBoxVisible = true;
                    this.afterSave = true;
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    public onStartDateChange(): void {
        if (this.getControlValue('StartDate')) {
            this.onBranchServiceAreaCodeChange();
        } else {
            this.buildGrid();
            this.riGrid.RefreshRequired();
        }
    }

    public onEndDateChange(): void {
        if (this.getControlValue('EndDate')) {
            this.onBranchServiceAreaCodeChange();
        } else {
            this.buildGrid();
            this.riGrid.RefreshRequired();
        }
    }

    public onApplyChangesClick(): void {
        let searchParams: QueryParams = this.getURLSearchParamObject();
        searchParams.set(this.serviceConstants.Action, '6');
        let formData: any = {};
        formData[this.serviceConstants.Function] = 'ApplyChanges';
        formData[this.serviceConstants.GridHandle] = this.pageParams.gridHandle;
        formData['BranchNumber'] = this.utils.getBranchCode();
        formData['BranchServiceAreaCode'] = this.getControlValue('BranchServiceAreaCode');
        formData['StartDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('StartDate'));
        formData['EndDate'] = this.globalize.parseDateToFixedFormat(this.getControlValue('EndDate'));
        this.ajaxSource.next(this.ajaxconstant.START);
        this.ajaxSubscription = this.httpService.makePostRequest(this.queryParams.method, this.queryParams.module, this.queryParams.operation, searchParams, formData).subscribe(
            (e) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (e.hasError) {
                    this.modalAdvService.emitError(new ICabsModalVO(e.errorMessage, e.fullError));
                } else {
                    this.isVisible.isMsgBoxVisible = false;
                    this.afterSave = false;
                    this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.PageSpecificMessage.sequenceNumberChanged));
                }
            },
            (error) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                this.modalAdvService.emitError(new ICabsModalVO(error.errorMessage, error.fullError));
            });
    }

    /**
     * This method is called when leaving the screen
     * @param void
     * @return Observable containing current state
     */
    public canDeactivate(): Observable<boolean> {
        let isShownWarning: boolean = this.afterSave;
        this.routeAwayGlobals.setSaveEnabledFlag(isShownWarning);
        let messageText: string = MessageConstant.PageSpecificMessage.confirmExit;
        this.canDeactivateObservable = new Observable((observer) => {
            let modalVO: ICabsModalVO = new ICabsModalVO(messageText, null);
            modalVO.title = MessageConstant.PageSpecificMessage.serviceAreaDetailGrid.warningTitle;
            if (isShownWarning) {
                modalVO.cancelCallback = () => {
                    this.cancelEvent({}, observer);
                };
                modalVO.confirmCallback = () => {
                    observer.next(true);
                };
                this.modalAdvService.emitPrompt(modalVO);
                return;
            }
            observer.next(true);
        });
        return this.canDeactivateObservable;
    }

    public cancelEvent(event: any, observer: any): void {
        if (this.variableService.getBackClick() === true) {
            this.variableService.setBackClick(false);
            this.location.go(this.utils.getCurrentUrl());
        }
        observer.next(false);
        setTimeout(() => {
            this.router.navigate([], { skipLocationChange: true, preserveQueryParams: true });
        }, 0);
    }

    public onImportSequenceClick(): void {
        this.modalAdvService.emitMessage(new ICabsModalVO(MessageConstant.Message.ScreenNotReady));
    }
}
