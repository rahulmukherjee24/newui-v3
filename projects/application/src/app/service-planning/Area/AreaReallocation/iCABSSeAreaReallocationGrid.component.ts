import { Component, OnInit, OnDestroy, Injector, ViewChild, Renderer, ElementRef } from '@angular/core';
import { QueryParams } from '../../../../shared/services/http-params-wrapper';

import { Observable } from 'rxjs/Rx';
import * as moment from 'moment';

import { GridAdvancedComponent } from '../../../../shared/components/grid-advanced/grid-advanced';
import { BaseComponent } from '../../../base/BaseComponent';
import { PaginationComponent } from '../../../../shared/components/pagination/pagination';
import { PageIdentifier } from '../../../base/PageIdentifier';
import { ICabsModalVO } from '../../../../shared/components/modal-adv/modal-adv-vo';
import { BranchServiceAreaSearchComponent } from '../../../internal/search/iCABSBBranchServiceAreaSearch';
import { ContractSearchComponent } from '../../../internal/search/iCABSAContractSearch';
import { PremiseSearchComponent } from '../../../internal/search/iCABSAPremiseSearch';
import { ProductSearchGridComponent } from '../../../internal/search/iCABSBProductSearch';
import { MntConst } from '../../../../shared/services/riMaintenancehelper';
import { ErrorConstant } from '../../../../shared/constants/error.constant';
import { MessageConstant } from '../../../../shared/constants/message.constant';


@Component({
    templateUrl: 'iCABSSeAreaReallocationGrid.html'
})

export class AreaReallocationGridComponent extends BaseComponent implements OnInit, OnDestroy {
    private requestParams: Object = {
        operation: 'Service/iCABSSeAreaReallocationGrid',
        module: 'smoothing',
        method: 'service-planning/maintenance'
    };
    private queryParams: any;
    private search: QueryParams = new QueryParams();
    private queryLookUp: QueryParams = new QueryParams();
    @ViewChild('gridPagination') public gridPagination: PaginationComponent;
    @ViewChild('riGrid') public riGrid: GridAdvancedComponent;
    @ViewChild('SearchType') public searchType: ElementRef;
    public pageId: string = '';
    public controls = [
        { name: 'SearchType', readonly: false, disabled: false, required: false },
        { name: 'BranchServiceAreaCode', readonly: false, disabled: false, required: false },
        { name: 'BranchServiceAreaPlan', readonly: false, disabled: false, required: false },
        { name: 'FilterType', readonly: false, disabled: false, required: false },
        { name: 'ContractNumber', readonly: false, disabled: false, required: false },
        { name: 'ContractName', readonly: false, disabled: false, required: false },
        { name: 'ProductCode', readonly: false, disabled: false, required: false },
        { name: 'ProductCodeDesc', readonly: false, disabled: false, required: false },
        { name: 'PremiseNumber', readonly: false, disabled: false, required: false },
        { name: 'PremiseName', readonly: false, disabled: false, required: false },
        { name: 'DayFilter', readonly: false, disabled: false, required: false },
        { name: 'PremiseName', readonly: false, disabled: false, required: false },
        { name: 'ServiceFrequency', readonly: false, disabled: false, required: false },
        { name: 'SequenceNumber', readonly: false, disabled: false, required: false },
        { name: 'ProductServiceGroupCode', readonly: false, disabled: false, required: false },
        { name: 'ProductServiceGroupDesc', readonly: false, disabled: false, required: false },
        { name: 'StartDate', readonly: false, disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'EndDate', readonly: false, disabled: false, required: true, type: MntConst.eTypeDate },
        { name: 'cmdReverse', readonly: false, disabled: true, required: false },
        { name: 'cmdUpdateAll', readonly: false, disabled: true, required: false }
    ];
    public pageVariables: any = {
        vEndofWeekDay: '',
        vEndofWeekDate: '',
        cCacheKey: '',
        cCacheTime: '',
        recordProcess: '',
        lGridRefreshed: false,
        vbEndofWeekDate: '',
        Row: '',
        PremiseRowID: '',
        ServiceCoverRowID: '',
        PlanVisitRowID: '',
        CurrentContractTypeURLParameter: '',
        riGridHandle: '',
        fieldVisibility: {

        },
        fieldRequired: {
            StartDate: false,
            EndDate: false,
            BranchServiceAreaCode: false,
            BranchServiceAreaPlan: false
        },
        gridProperties: {
            itemsPerPage: 10,
            currentPage: 1,
            totalRecords: 0,
            paginationCurrentPage: 1,
            headerClicked: '',
            sortType: 'Descending',
            inputParams: {}
        }
    };
    public ellipsis: any = {
        branchServiceAreaFrom: {
            showCloseButton: true,
            showHeader: true,
            disabled: false,
            childConfigParams: {
                parentMode: 'LookUp-Emp'
            },
            contentComponent: BranchServiceAreaSearchComponent
        },
        branchServiceAreaTo: {
            showCloseButton: true,
            showHeader: true,
            disabled: false,
            childConfigParams: {
                parentMode: 'LookUp-PlanEmp'
            },
            contentComponent: BranchServiceAreaSearchComponent
        },
        contractSearch: {
            disabled: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'Search',
                currentContractType: '',
                currentContractTypeURLParameter: '',
                showAddNew: false,
                contractNumber: '',
                showCountry: false,
                showBusiness: false,
                accountNumber: '',
                accountName: ''
            },
            showHeader: true,
            showAddNew: false,
            autoOpenSearch: false,
            setFocus: false,
            contentComponent: ContractSearchComponent
        },
        premiseSearch: {
            autoOpenSearch: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'Search',
                ContractNumber: '',
                ContractName: '',
                currentContractType: '',
                currentContractTypeURLParameter: '',
                showAddNew: false
            },
            contentComponent: PremiseSearchComponent,
            showHeader: true,
            searchModalRoute: '',
            disabled: false
        },
        productCode: {
            autoOpenSearch: false,
            showCloseButton: true,
            childConfigParams: {
                parentMode: 'Search',
                negativeBranchNumber: ''
            },
            contentComponent: ProductSearchGridComponent,
            showHeader: true,
            disabled: false
        },
        common: {
            modalConfig: {
                backdrop: 'static',
                keyboard: true
            }
        }
    };
    public dropdown: any = {
        ProductServiceGroupSearch: {
            params: {
                parentMode: 'Search',
                ProductServiceGroupString: '',
                SearchValue3: '',
                ProductServiceGroupCode: ''
            },
            active: { id: '', text: '' },
            isDisabled: false,
            isRequired: false,
            isTriggerValidate: false
        }
    };

    constructor(injector: Injector, private renderer: Renderer) {
        super(injector);
        this.pageId = PageIdentifier.ICABSSEAREAREALLOCATIONGRID;
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.pageTitle = 'Service Area Reallocation';
        this.utils.setTitle('Service Area Reallocation');
        this.pageVariables['cCacheTime'] = moment(new Date()).format('HH:mm:ss');
        this.queryParams = this.riExchange.getRouterParams();
        this.pageVariables['riGridHandle'] = this.utils.randomSixDigitString();
        this.setControlValue('SearchType', 'PlanVisit');
        this.setControlValue('DayFilter', 'All');
        this.setControlValue('FilterType', 'All');
        this.fetchTranslationContent();
        this.ellipsis['contractSearch']['childConfigParams']['currentContractType'] = this.riExchange.getCurrentContractType();
        this.ellipsis['contractSearch']['childConfigParams']['currentContractTypeURLParameter'] = this.riExchange.getCurrentContractTypeLabel();
        this.ellipsis['premiseSearch']['childConfigParams']['currentContractType'] = this.riExchange.getCurrentContractType();
        this.ellipsis['premiseSearch']['childConfigParams']['currentContractTypeURLParameter'] = this.riExchange.getCurrentContractTypeLabel();
        this.postInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    /**
     * This function is called when initialisation completes, it loads default data
     */
    private postInit(): void {
        let lookupdata: Array<any> = [
            {
                table: 'SystemParameter',
                query: {},
                fields: ['SystemParameterEndOfWeekDay']
            }
        ];

        this.lookUpRecord(lookupdata, 1).subscribe((e) => {
            if (e['results'] && e['results'].length > 0) {
                let today: Date = new Date();
                if (e['results'][0].length > 0 && e['results'][0][0]['SystemParameterEndOfWeekDay'] < 7) {
                    this.pageVariables['vEndofWeekDay'] = e['results'][0][0]['SystemParameterEndOfWeekDay'];
                } else {
                    this.pageVariables['vEndofWeekDay'] = 1;
                }
                this.pageVariables['vEndofWeekDate'] = new Date(today.setDate((7 - (today.getDay() + 1) + this.pageVariables['vEndofWeekDay']) + today.getDate()));
                this.pageVariables['vbEndofWeekDate'] = this.pageVariables['vEndofWeekDate'];
                this.windowOnLoad();
            }
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function is called post initialisation, it also checks for isReturning flag
     */
    private windowOnLoad(): void {
        this.buildGrid();
        let startDate = new Date(this.pageVariables['vbEndofWeekDate']), endDate = new Date(this.pageVariables['vbEndofWeekDate']);
        startDate.setDate(this.pageVariables['vbEndofWeekDate'].getDate() + 1);
        endDate.setDate(this.pageVariables['vbEndofWeekDate'].getDate() + 7);
        this.setControlValue('StartDate', this.globalize.parseDateToFixedFormat(startDate));
        this.setControlValue('EndDate', this.globalize.parseDateToFixedFormat(endDate));
        let focus = new CustomEvent('focus', { bubbles: true });
        this.renderer.invokeElementMethod(this.searchType.nativeElement, 'focus', [focus]);
        if (this.isReturning()) {
            this.pageVariables = this.pageParams;
            this.populateUIFromFormData();
            if (this.getControlValue('ProductServiceGroupCode')) {
                this.dropdown['ProductServiceGroupSearch']['active'] = {
                    id: this.getControlValue('ProductServiceGroupCode'),
                    text: this.getControlValue('ProductServiceGroupCode') + ' - ' + this.getControlValue('ProductServiceGroupDesc')
                };
            }
            this.riGridBeforeExecute();
        }
    }

    /**
     * This is an API function for GET requests triggered in the screen
     * @param functionName
     * @param params
     */
    private areaGridGet(functionName: string, params: Object): Observable<any> {
        let queryWorkOrder = new QueryParams();
        queryWorkOrder.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryWorkOrder.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            queryWorkOrder.set(this.serviceConstants.Action, '6');
            queryWorkOrder.set('Function', functionName);
        }
        for (let key in params) {
            if (key) {
                queryWorkOrder.set(key, params[key]);
            }
        }
        return this.httpService.makeGetRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], queryWorkOrder);
    }
    /**
     * This is an API function for POST requests triggered in the screen
     * @param functionName
     * @param params
     * @param formData
     */
    private areaGridPost(functionName: string, params: Object, formData: Object): Observable<any> {
        let queryWorkOrder = new QueryParams();
        queryWorkOrder.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        queryWorkOrder.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (functionName !== '') {
            queryWorkOrder.set(this.serviceConstants.Action, '6');
            formData['Function'] = functionName;
        }
        for (let key in params) {
            if (key) {
                queryWorkOrder.set(key, params[key]);
            }
        }
        return this.httpService.makePostRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], queryWorkOrder, formData);
    }
    /**
     * This is an API function for Look-up requests triggered in the screen
     * @param data
     * @param maxresults
     */
    private lookUpRecord(data: Object, maxresults?: number): Observable<any> {
        this.queryLookUp.set(this.serviceConstants.Action, '0');
        this.queryLookUp.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.queryLookUp.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        if (maxresults) {
            this.queryLookUp.set(this.serviceConstants.MaxResults, maxresults.toString());
        } else {
            this.queryLookUp.set(this.serviceConstants.MaxResults, '100');
        }
        return this.httpService.lookUpRequest(this.queryLookUp, data);
    }

    /**
     * This is batch request to fetch translation
     */
    private fetchTranslationContent(): void {
        this.getTranslatedValuesBatch((data: any) => {
            if (data) {
                this.pageVariables['cmdReverse'] = data[0];
                this.pageVariables['cmdUpdateAll'] = data[1];
                this.pageVariables['recordProcess'] = data[2];
            }
        },
            ['Reverse'], ['Update All'], ['Records To Process']);
    }
    /**
     * This function builds the structure of grid
     */
    private buildGrid(): void {
        this.riGrid.Clear();
        this.riGrid.AddColumn('Contract', 'PlanVisit', 'Contract', MntConst.eTypeCode, 15);
        this.riGrid.AddColumnAlign('Contract', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('Town', 'PlanVisit', 'Town', MntConst.eTypeText, 25);
        this.riGrid.AddColumnAlign('Town', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Postcode', 'PlanVisit', 'Postcode', MntConst.eTypeCode, 9, true);
        this.riGrid.AddColumnAlign('Postcode', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('NextServiceVisitDate', 'PlanVisit', 'NextServiceVisitDate', MntConst.eTypeDate, 10);
        this.riGrid.AddColumnAlign('Status', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('NextServiceVisitDay', 'PlanVisit', 'NextServiceVisitDay', MntConst.eTypeTextFree, 3);
        this.riGrid.AddColumnAlign('NextServiceVisitDay', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('PremiseName', 'PlanVisit', 'PremiseName', MntConst.eTypeText, 14);
        this.riGrid.AddColumnAlign('PremiseName', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('ProductCode', 'PlanVisit', 'ProductCode', MntConst.eTypeCode, 10, true);
        this.riGrid.AddColumnAlign('ProductCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceVisitFrequency', 'PlanVisit', 'ServiceVisitFrequency', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceVisitFrequency', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceQuantity', 'PlanVisit', 'ServiceQuantity', MntConst.eTypeInteger, 5);
        this.riGrid.AddColumnAlign('ServiceQuantity', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('ServiceTypeCode', 'PlanVisit', 'ServiceTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('ServiceTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('VisitTypeCode', 'PlanVisit', 'VisitTypeCode', MntConst.eTypeCode, 2);
        this.riGrid.AddColumnAlign('VisitTypeCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('SCBranchServiceAreaCode', 'PlanVisit', 'SCBranchServiceAreaCode', MntConst.eTypeCode, 4);
        this.riGrid.AddColumnAlign('SCBranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaCode', 'PlanVisit', 'BranchServiceAreaCode', MntConst.eTypeCode, 4);
        this.riGrid.AddColumnAlign('BranchServiceAreaCode', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('BranchServiceAreaSeqNo', 'ServiceCover', 'BranchServiceAreaSeqNo', MntConst.eTypeText, 6);
        this.riGrid.AddColumnAlign('BranchServiceAreaSeqNo', MntConst.eAlignmentCenter);
        this.riGrid.AddColumn('gridEmployeeSurname', 'PlanVisit', 'gridEmployeeSurname', MntConst.eTypeText, 20);
        this.riGrid.AddColumnAlign('gridEmployeeSurname', MntConst.eAlignmentLeft);
        this.riGrid.AddColumn('Reallocated', 'PlanVisit', 'Reallocated', MntConst.eTypeImage, 1, false, 'Click here to reallocate');
        this.riGrid.AddColumnAlign('Reallocated', MntConst.eAlignmentCenter);
        this.riGrid.AddColumnOrderable('Contract', true);
        this.riGrid.AddColumnOrderable('Postcode', true);
        this.riGrid.AddColumnOrderable('ServiceVisitFrequency', true);
        this.riGrid.AddColumnOrderable('NextServiceVisitDate', true);
        this.riGrid.AddColumnOrderable('NextServiceVisitDay', true);
        this.riGrid.AddColumnOrderable('Town', true);
        this.riGrid.AddColumnOrderable('PremiseName', true);
        this.riGrid.AddColumnOrderable('ProductCode', true);
        this.riGrid.Complete();
    }
    /**
     * This function builds request parameters for the grid service
     */
    private riGridBeforeExecute(): void {
        this.pageVariables['cCacheKey'] = this.utils.getBusinessCode() + '|' + this.utils.getBranchCode() + '|' + this.pageVariables['cCacheTime'] + '|' + this.getControlValue('StartDate') + '|' + this.getControlValue('EndDate') + '|' + this.getControlValue('BranchServiceAreaCode') + '|' + this.getControlValue('BranchServiceAreaPlan') + '|' + this.getControlValue('ContractNumber') + '|' + this.getControlValue('PremiseNumber') + '|' + this.getControlValue('ProductCode') + '|' + this.getControlValue('DayFilter') + '|' + this.getControlValue('ProductServiceGroupCode') + '|' + this.getControlValue('FilterType') + '|' + this.getControlValue('SearchType') + '|' + this.getControlValue('ServiceFrequency') + '|' + this.getControlValue('SequenceNumber') + '|' + this.getControlValue('PremiseName');
        this.riGrid.RefreshRequired();
        this.search.set(this.serviceConstants.Action, '2');
        this.search.set(this.serviceConstants.CountryCode, this.utils.getCountryCode());
        this.search.set(this.serviceConstants.BusinessCode, this.utils.getBusinessCode());
        this.search.set('BranchNumber', this.utils.getBranchCode());
        this.search.set(this.serviceConstants.GridMode, '0');
        this.search.set(this.serviceConstants.GridHandle, this.pageVariables['riGridHandle']);
        this.search.set(this.serviceConstants.GridCacheRefresh, 'true');
        this.search.set(this.serviceConstants.PageSize, this.pageVariables['gridProperties']['itemsPerPage'].toString());
        this.search.set(this.serviceConstants.PageCurrent, this.pageVariables['gridProperties']['paginationCurrentPage'].toString());
        this.search.set('CacheKey', this.pageVariables['cCacheKey']);
        this.search.set('StartDate', this.getControlValue('StartDate'));
        this.search.set('EndDate', this.getControlValue('EndDate'));
        this.search.set('BranchServiceAreaCode', this.getControlValue('BranchServiceAreaCode'));
        this.search.set('BranchServiceAreaPlan', this.getControlValue('BranchServiceAreaPlan'));
        this.search.set('ContractNumber', this.getControlValue('ContractNumber'));
        this.search.set('PremiseNumber', this.getControlValue('PremiseNumber'));
        this.search.set('ProductCode', this.getControlValue('ProductCode'));
        this.search.set('DayFilter', this.getControlValue('DayFilter'));
        this.search.set('ProductServiceGroupCode', this.getControlValue('ProductServiceGroupCode'));
        this.search.set('FilterType', this.getControlValue('FilterType'));
        this.search.set('SearchType', this.getControlValue('SearchType'));
        this.search.set('ServiceFrequency', this.getControlValue('ServiceFrequency'));
        this.search.set('SequenceNumber', this.getControlValue('SequenceNumber'));
        this.search.set('PremiseName', this.getControlValue('PremiseName'));
        this.search.set(this.serviceConstants.GridSortOrder, this.riGrid.SortOrder);
        if (this.riGrid.Update) {
            this.riGrid.StartColumn = 0;
            this.riGrid.StartRow = this.riGrid.CurrentRow;
            this.riGrid.RowID = this.riGrid.Details.GetAttribute('Reallocated', 'RowID');
            this.search.set('RowID', this.riGrid.RowID);
        } else {
            this.search.delete('RowID');
        }
        this.search.set(this.serviceConstants.GridHeaderClickedColumn, this.riGrid.HeaderClickedColumn);

        this.ajaxSource.next(this.ajaxconstant.START);
        this.httpService.makeGetRequest(this.requestParams['method'], this.requestParams['module'], this.requestParams['operation'], this.search).subscribe(
            (data) => {
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
                if (data) {
                    if (!data['hasError']) {
                        if (this.riGrid.Update) {
                            this.riGrid.UpdateHeader = false;
                            this.riGrid.UpdateFooter = false;
                        } else {
                            this.riGrid.UpdateHeader = true;
                            this.riGrid.UpdateFooter = true;
                            this.pageVariables['gridProperties']['paginationCurrentPage'] = data.pageData ? data.pageData.pageNumber : 1;
                            this.pageVariables['gridProperties']['totalRecords'] = data.pageData ? data.pageData.lastPageNumber * 10 : 1;
                        }
                        this.riGrid.UpdateBody = true;
                        this.riGrid.Execute(data);
                        this.riGrid.Update = false;
                    } else {
                        this.resetPagination();
                        this.riGrid.Execute(data);
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'] || ErrorConstant.Message.UnexpectedError, data['fullError']));
                    }
                }
            },
            (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                this.resetPagination();
                this.ajaxSource.next(this.ajaxconstant.COMPLETE);
            });
    }
    /**
     * This function resets the pagination to default
     */
    private resetPagination(): void {
        this.pageVariables['gridProperties']['pageCurrent'] = 1;
        this.pageVariables['gridProperties']['paginationCurrentPage'] = 1;
        this.pageVariables['gridProperties']['totalRecords'] = 0;
    }

    /**
     * This function disables the Update All button if filters have changed
     */
    private filtersHaveChanged(): void {
        this.uiForm.controls['cmdUpdateAll'].disable();
        this.riGrid.RefreshRequired();
    }
    /**
     * This function is callback for Update All confirm process
     */
    private processRecordCallback(): void {
        let formdata: Object = {
            BranchNumber: this.utils.getBranchCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            BranchServiceAreaPlan: this.getControlValue('BranchServiceAreaPlan'),
            CacheKey: this.pageVariables['cCacheKey'],
            StartDate: this.getControlValue('StartDate'),
            EndDate: this.getControlValue('EndDate')
        };
        this.areaGridPost('UpdateAll', {}, formdata).subscribe((data) => {
            if (!data['hasError']) {
                this.pageVariables['cCacheTime'] = moment(new Date()).format('HH:mm:ss');
                this.riGrid.DescendingSort = !this.riGrid.DescendingSort;
                this.buildGrid();
                this.riGridBeforeExecute();
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'] || ErrorConstant.Message.UnexpectedError, data['fullError']));
            }
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function sets page params when grid row is focussed
     */
    private planVisitFocus(srcElement: any): void {
        this.pageVariables['Row'] = this.riGrid.Details.GetAttribute('Postcode', 'RowID');
        this.pageVariables['PremiseRowID'] = this.riGrid.Details.GetAttribute('Postcode', 'RowID');
        this.pageVariables['ServiceCoverRowID'] = this.riGrid.Details.GetAttribute('ProductCode', 'RowID');
        this.pageVariables['PlanVisitRowID'] = this.riGrid.Details.GetAttribute('Reallocated', 'RowID');
        let contractType: string = this.riGrid.Details.GetAttribute('Town', 'AdditionalProperty');
        switch (contractType) {
            case 'C':
                this.pageVariables['CurrentContractTypeURLParameter'] = '';
                break;
            case 'J':
                this.pageVariables['CurrentContractTypeURLParameter'] = '<job>';
                break;
            case 'P':
                this.pageVariables['CurrentContractTypeURLParameter'] = '<product>';
                break;
        }
    }
    /**
     * This function gets triggered when pagination changes
     */
    public getCurrentPage(currentPage: any): void {
        this.pageVariables['gridProperties']['paginationCurrentPage'] = currentPage.value;
        this.search.set(this.serviceConstants.PageCurrent, this.pageVariables['gridProperties']['paginationCurrentPage'].toString());
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    /**
     * This function gets triggered when grid is refreshed
     */
    public gridRefresh(): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    /**
     * This function gets triggered when grid column is clicked to sort the rows
     */
    public riGridSort(event: any): void {
        this.riGrid.RefreshRequired();
        this.riGridBeforeExecute();
    }
    /**
     * This function gets triggered when grid is populated with data
     */
    public riGridAfterExecute(): void {
        this.pageVariables['lGridRefreshed'] = true;
        this.uiForm.controls['cmdUpdateAll'].enable();
        if (!this.getControlValue('BranchServiceAreaCode')) {
            this.uiForm.controls['cmdReverse'].disable();
        } else {
            this.uiForm.controls['cmdReverse'].enable();
        }
    }
    /**
     * This function gets triggered when grid row is double clicked
     */
    public gridBodyOnDblClick(event: any): void {
        this.planVisitFocus(event['srcElement']);
        switch (this.riGrid.CurrentColumnName) {
            case 'ProductCode':
                this.pageParams = this.pageVariables;
                this.navigate('AreaReallocation', this.ContractManagementModuleRoutes.ICABSASERVICECOVERMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageVariables['CurrentContractTypeURLParameter'],
                    currentContractType: this.riGrid.Details.GetAttribute('Town', 'AdditionalProperty'),
                    ServiceCoverRowID: this.pageVariables['ServiceCoverRowID']
                });
                break;
            case 'Postcode':
                this.pageParams = this.pageVariables;
                this.navigate('AreaReallocation', this.ContractManagementModuleRoutes.ICABSAPREMISEMAINTENANCE, {
                    CurrentContractTypeURLParameter: this.pageVariables['CurrentContractTypeURLParameter'],
                    ContractTypeCode: this.riGrid.Details.GetAttribute('Town', 'AdditionalProperty'),
                    PremiseRowID: this.pageVariables['PremiseRowID']
                });
                break;
            default:
                this.gridBodyOnClick(event);
        }
    }
    /**
     * This function gets triggered when grid row is clicked
     */
    public gridBodyOnClick(event: any): void {
        if (this.riGrid.CurrentColumnName === 'Reallocated') {
            if (!this.getControlValue('BranchServiceAreaPlan')) {
                this.uiForm.controls['BranchServiceAreaPlan'].setErrors({});
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaPlan', true);
            } else {
                this.planVisitFocus(event['srcElement']);
                let formdata: Object = {
                    BranchNumber: this.utils.getBranchCode(),
                    BranchServiceAreaPlan: this.getControlValue('BranchServiceAreaPlan'),
                    PlanVisitRowID: this.pageVariables['PlanVisitRowID']
                };
                this.areaGridPost('Update', {}, formdata).subscribe((data) => {
                    if (!data['hasError']) {
                        this.riGrid.Update = true;
                        this.riGridBeforeExecute();
                    } else {
                        this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'] || ErrorConstant.Message.UnexpectedError, data['fullError']));
                    }
                }, (error) => {
                    this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
                });
            }
        }
    }
    /**
     * This function gets triggered when grid row recieves key event
     */
    public gridBodyOnKeyDown(event: any): void {
        try {
            let cellindex = event.srcElement.parentElement.parentElement.cellIndex;
            let rowIndex = event.srcElement.parentElement.parentElement.parentElement.sectionRowIndex;
            switch (event.keyCode) {
                case 13:
                    break;
                case 38:
                    event.returnValue = 0;
                    if ((rowIndex > 0) && (rowIndex < 10)) {
                        this.planVisitFocus(this.riGrid.CurrentHTMLRow.previousSibling.children[cellindex].children[0].children[0]);
                    }
                    break;
                case 40:
                case 9:
                    event.returnValue = 0;
                    if ((rowIndex >= 0) && (rowIndex < 9)) {
                        this.planVisitFocus(this.riGrid.CurrentHTMLRow.nextSibling.children[cellindex].children[0].children[0]);
                    }
                    break;
                default:
                    break;
            }
        } catch (e) {
            // error
        }
    }
    /**
     * This function gets triggered when cmdReverse button is clicked
     */
    public cmdReverseOnClick(): void {
        let formdata: any = {
            BranchNumber: this.utils.getBranchCode(),
            BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
            StartDate: this.getControlValue('StartDate'),
            EndDate: this.getControlValue('EndDate'),
            SearchType: this.getControlValue('SearchType'),
            FilterType: this.getControlValue('FilterType'),
            PremiseName: this.getControlValue('PremiseName'),
            ContractNumber: this.getControlValue('ContractNumber'),
            PremiseNumber: this.getControlValue('PremiseNumber'),
            ProductCode: this.getControlValue('ProductCode'),
            ServiceFrequency: this.getControlValue('ServiceFrequency'),
            ProductServiceGroupCode: this.getControlValue('ProductServiceGroupCode'),
            DayFilter: this.getControlValue('DayFilter'),
            SequenceNumber: this.getControlValue('SequenceNumber')
        };
        this.areaGridPost('Reverse', {}, formdata).subscribe((data) => {
            if (!data['hasError']) {
                this.pageVariables['cCacheTime'] = moment(new Date()).format('HH:mm:ss');
                this.riGrid.DescendingSort = !this.riGrid.DescendingSort;
                this.buildGrid();
                this.riGridBeforeExecute();
            } else {
                this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'] || ErrorConstant.Message.UnexpectedError, data['fullError']));
            }
        }, (error) => {
            this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
        });
    }
    /**
     * This function gets triggered when cmdUpdateAll button is clicked
     */
    public cmdUpdateAllOnClick(): void {
        let iNumberOfRecords, cRecordCountText;
        if (!this.getControlValue('BranchServiceAreaPlan')) {
            this.uiForm.controls['BranchServiceAreaPlan'].setErrors({});
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaPlan', true);
        } else {
            let formdata: Object = {
                BranchNumber: this.utils.getBranchCode(),
                BranchServiceAreaCode: this.getControlValue('BranchServiceAreaCode'),
                CacheKey: this.pageVariables['cCacheKey'],
                StartDate: this.getControlValue('StartDate'),
                EndDate: this.getControlValue('EndDate')
            };
            this.areaGridPost('UpdateAllCount', {}, formdata).subscribe((data) => {
                if (!data['hasError']) {
                    cRecordCountText = data['NumberOfRecords'] + ' ' + this.pageVariables['recordProcess'];
                    this.modalAdvService.emitPrompt(new ICabsModalVO(cRecordCountText, MessageConstant.Message.ContinueMessage, this.processRecordCallback.bind(this)));
                } else {
                    this.modalAdvService.emitError(new ICabsModalVO(data['errorMessage'] || ErrorConstant.Message.UnexpectedError, data['fullError']));
                }
            }, (error) => {
                this.modalAdvService.emitError(new ICabsModalVO(error['errorMessage'] || ErrorConstant.Message.UnexpectedError));
            });
        }
    }
    /**
     * This function gets triggered when branchServiceAreaCode is changed
     */
    public branchServiceAreaCodeOnChange(): void {
        if (this.pageVariables['lGridRefreshed']) {
            if (!this.getControlValue('BranchServiceAreaCode')) {
                this.uiForm.controls['cmdReverse'].disable();
            } else {
                this.uiForm.controls['cmdReverse'].enable();
            }
            this.filtersHaveChanged();
        }
    }
    /**
     * This function gets triggered when branchServiceAreaPlan is changed
     */
    public branchServiceAreaPlanOnChange(): void {
        this.riGrid.RefreshRequired();
        if (this.getControlValue('BranchServiceAreaPlan')) {
            this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaPlan', false);
            this.uiForm.controls['BranchServiceAreaPlan'].updateValueAndValidity();
        }
    }
    /**
     * This function gets triggered when premise number is changed
     */
    public premiseNumberOnChange(): void {
        this.filtersHaveChanged();
    }
    /**
     * This function gets triggered when product code is changed
     */
    public productCodeOnChange(): void {
        this.filtersHaveChanged();
    }
    /**
     * This function gets triggered when service frequency is changed
     */
    public serviceFrequencyOnChange(): void {
        this.filtersHaveChanged();
    }
    /**
     * This function gets triggered when contract number is changed
     */
    public onContractChange(): void {
        let cNum = this.getControlValue('ContractNumber');
        if (cNum) {
            this.setControlValue('ContractNumber', this.utils.fillLeadingZeros(cNum, 8));
            this.ellipsis['premiseSearch']['childConfigParams']['ContractNumber'] = this.getControlValue('ContractNumber');
        } else {
            this.ellipsis['premiseSearch']['childConfigParams']['ContractNumber'] = cNum;
        }
        this.ellipsis['premiseSearch']['childConfigParams']['ContractName'] = '';
        this.filtersHaveChanged();
    }
    /**
     * This function gets triggered on ServiceArea ellipsis data event
     */
    public onServiceAreaDataReceived(type: string, data: any): void {
        switch (type) {
            case 'BranchServiceAreaCode':
                this.setControlValue('BranchServiceAreaCode', data.BranchServiceAreaCode || '');
                this.branchServiceAreaCodeOnChange();
                break;
            case 'BranchServiceAreaPlan':
                this.setControlValue('BranchServiceAreaPlan', data.BranchServiceAreaPlan || '');
                this.riExchange.riInputElement.SetErrorStatus(this.uiForm, 'BranchServiceAreaPlan', false);
                this.uiForm.controls['BranchServiceAreaPlan'].updateValueAndValidity();
                break;
            default:
        }
    }
    /**
     * This function gets triggered on contract number ellipsis data event
     */
    public onContractDataReceived(data: Object): void {
        if (data) {
            this.setControlValue('ContractNumber', data['ContractNumber']);
            this.ellipsis['premiseSearch']['childConfigParams']['ContractNumber'] = data['ContractNumber'];
            this.ellipsis['premiseSearch']['childConfigParams']['ContractName'] = data['ContractName'];
            this.filtersHaveChanged();
        }
    }
    /**
     * This function gets triggered on premise number ellipsis data event
     */
    public onPremiseDataReceived(data: Object): void {
        if (data) {
            this.setControlValue('PremiseNumber', data['PremiseNumber']);
            this.filtersHaveChanged();
        }
    }
    /**
     * This function gets triggered on product code ellipsis data event
     */
    public onProductCodeReceived(data: Object): void {
        if (data) {
            this.setControlValue('ProductCode', data['ProductCode']);
            this.filtersHaveChanged();
        }
    }
    /**
     * This function gets triggered on product service group data selection
     */
    public onProductServiceGroupReceived(data: Object): void {
        if (data) {
            this.setControlValue('ProductServiceGroupCode', data['ProductServiceGroupCode']);
            this.setControlValue('ProductServiceGroupDesc', data['ProductServiceGroupDesc']);
            this.filtersHaveChanged();
        }
    }
}
