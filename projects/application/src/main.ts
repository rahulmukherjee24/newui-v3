import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

window['gKeys'] = {
  GA_TRACK_CODE: environment['GA_TRACK_CODE'],
  GMAP_API_KEY: environment['GMAP_API_KEY']
};

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
