@echo off

REM Location of the scanner program. Change this as required.
set SCANNER=sonar-scanner.bat

REM Sonar Scanner Options - In this case, tell Java to use system Proxy settings
set SONAR_SCANNER_OPTS=-Djava.net.useSystemProxies=true

REM Check to see if an environment variable is configured which contains the login key
IF NOT DEFINED SONAR_LOGIN (
	echo Login key for Sonar is not defined in environment variable
	echo Define an environment variable called 'SONAR_LOGIN' with the appropriate login key - e.g.:
	echo "set SONAR_LOGIN=<login-key>"
	goto end
) ELSE (
	REM Format the login parameter correctly
	set SONAR_LOGIN_PARAM=-Dsonar.login=%SONAR_LOGIN%
)

REM Get Git branch name
for /f %%i in ('git branch --show-current') do set GIT_BRANCH_NAME=%%i

REM If the branch is not "master" then ensure that the Sonar branch parameter is specified
set SONAR_BRANCH_PARAM=""

IF "%GIT_BRANCH_NAME%" == "master" (
	set SONAR_BRANCH_PARAM=
) ELSE (
	set SONAR_BRANCH_PARAM=-Dsonar.branch.name=%GIT_BRANCH_NAME%
)

REM Set a target branch name if not on master, RC_STAGING or RC_SIT
REM Assume parent branch is RC_STAGING unless already defined
set SONAR_TARGET_BRANCH_PARAM=
IF NOT "%GIT_BRANCH_NAME%" == "master" IF NOT "%GIT_BRANCH_NAME%" == "RC_STAGING" IF NOT "%GIT_BRANCH_NAME%" == "RC_SIT" (
	REM Check if already defined
	IF "%SONAR_TARGET_BRANCH%" == "" (
		set SONAR_TARGET_BRANCH_PARAM=-Dsonar.branch.target=RC_STAGING
	) ELSE (
		set SONAR_TARGET_BRANCH_PARAM=-Dsonar.branch.target=%SONAR_TARGET_BRANCH%
	)
)

echo SONAR_SCANNER_OPTS: %SONAR_SCANNER_OPTS%
echo %SCANNER% %SONAR_LOGIN_PARAM% %SONAR_BRANCH_PARAM% %SONAR_TARGET_BRANCH_PARAM%
%SCANNER% %SONAR_LOGIN_PARAM% %SONAR_BRANCH_PARAM% %SONAR_TARGET_BRANCH_PARAM%

:end